# -*- Mode: Makefile; -*-
MAKEFILE_NAME:=$(lastword $(MAKEFILE_LIST))
# include project configuration
#include guiprt_config

NAME=gui_server
TYPE=program

# set to 1 to add JS support
JS_SUPPORT?=0
vos2_IMPORTS=C:/REPO/ADK-4.7.8-969/adk-full-ext-4.7.8-969/vos

SOURCE= src/main.cpp
	
	
vos_LIBS=-lvfisysinfo -lvfictls -lrt -lpthread -lvfiplatforminfo -ldl $(TECMSRLIBS) -lvfiguiprt -lvfiipc -lTLV_Util\
          -lsdiclient -lEMV_CT_Link -lEMV_CTLS_Link -llog -lvfiguiprt -lsecins -lcom  -linf -lssl -lexpat \
		  -lEMV_CT_Client -lEMV_CT_Framework -lEMV_CTLS_Link -lEMV_CTLS_Framework -lEMV_CTLS_Client -lmsr -lmsrclient \
		  -lsvc_led -lsvc_security -lsvc_netloader -ljsproc -lvfiguiprt -lvfiipc -ljsproc \
		  -pthread -lvfisec -lrt -ldl -lssl -lcrypto -lvfisyspm -lsec2 -lsqlite \
		  -LC:/REPO/ADK-4.7.8-969/adk-full-ext-4.7.8-969/vos/lib
#		  -Lsrc/GlobalPlatform/

vos_CFLAGS=-Wall -pthread -IC:/REPO/ADK-4.7.8-969/adk-full-ext-4.7.8-969/vos/include \
			-Wno-unused-parameter -Wno-missing-field-initializers -Isrc/ -Isrc/utility \
			-IC:/REPO/ADK-4.7.8-969/vos-sdk-winx86-release-31341100/Mx9xxSDK-1.2.7-DTK400.341100/usr/include \
			-IC:/REPO/ADK-4.7.8-969/adk-full-ext-4.7.8-969/vos/include 

default: vos-debug

all: x86-debug x86-release vos-debug vos-release 

clean:
	-rm -rf obj 2>/dev/null

############### VOS/Raptor packing (common) ###############

vos_PACK_SCRIPT="import/make_rules/script/makepackage"
STRIPNEW="import/make_rules/script/stripnew"

vos_PACKAGE_NAME=dl.$(NAME)
vos_APP_NAME=dl.$(NAME)-app
vos_FONTPKG_NAME=dl.$(vos_DEMOFONTS)
vos_START_CMDLINE:=$(NAME)
VERSION = 0.0.1


############### Raptor packing ###############

# create package after debug build
vos-debug: vos-debug-build
	$(STRIPNEW) $(vos_STRIP) vos-debug/$(NAME)
	$(vos_PACK_SCRIPT) --compress --dlname $(vos_PACKAGE_NAME) --version $(VERSION) --start "$(vos_START_CMDLINE)" vos-debug/$(NAME) 
	mv $(vos_PACKAGE_NAME).tar vos-debug

# create package after debug build
vos-release: vos-release-build
	$(STRIPNEW) $(vos_STRIP) vos-release/$(NAME)
	$(vos_PACK_SCRIPT) --compress --dlname $(vos_PACKAGE_NAME) --version $(VERSION) --start "$(vos_START_CMDLINE)" vos-release/$(NAME) 
	mv $(vos_PACKAGE_NAME).tar vos-release	

include import/make_rules/x86_rules
include import/make_rules/vos_rules
include import/make_rules/vos2_rules
#include guiprt_common
