
This is a minimal version of DEV-ADK-BUILD to reduce both download and disk size for compilation.
For this it omits documentation and executables.

Documentation can still be found in the DEV-ADK-BUILD repository
ssh://git@bitbucket.verifone.com:7999/ifadk/dev-adk-build.git

Executables no longer are part of the repository but are considered to be found in the
search path $PATH. By this they just need to be installed once instead of downloading
them over and over again. The executables can also be obtained from the DEV-ADK-BUILD repository.
On Linux it should be sufficient to install them in /home/<user>/bin.

The following executables are affected by this change:

  - vfisigner / vfisigner_win32.exe
  - FileSign.exe
  - mfc90.dll
  - 7z.exe
  - cmactool/cmactool_win32.exe
  - paxctl.exe
  
