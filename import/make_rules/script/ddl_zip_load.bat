@echo OFF

setlocal enabledelayedexpansion

set script_path=%~dp0

REM add FST and DDL to path
path %PATH%;"c:\Program Files (x86)\VeriFone\FST";"c:\Program Files\VeriFone\FST";%VRXSDK%\bin;!script_path!..\bin

set clearall=
set port=
set zip=
set add_para=
set signing=y
set loading=y
set silent=n
set outdir=
set bundle=
set ziplist=
set fstfile=sign.fst
set isbundle=n
set use7zip=n

set zip_cnt=0
set outdir_created=0

set 7z_exe="!script_path!7z.exe"
if not exist "!7z_exe!" set 7z_exe="7z.exe"

REM handle parameters
:Loop
if "%1"=="" goto continue
   set para=%~1
   if "!para:~0,2!"=="--" (
     if "!para:~0,11!"=="--clearall" (
       REM '=' is ignored by WinShell and stored to next parameter
       set clearall=%~2
       if "!clearall!"=="" goto usage
       shift
     ) else (
     if "!para:~0,11!"=="--ddl_port" (
       REM '=' is ignored by WinShell and stored to next parameter
       set port=%~2
       if "!port!"=="" goto usage
       shift
     ) else (
     if "!para:~0,11!"=="--add_para" (
       REM '=' is ignored by WinShell and stored to next parameter
       set add_para=%~2
       if "!add_para!"=="" goto usage
       shift
     ) else (
     if "!para:~0,10!"=="--signing" (
       REM '=' is ignored by WinShell and stored to next parameter
       set signing=%~2
       if "!signing!"=="" goto usage
       shift
     ) else (
     if "!para:~0,10!"=="--loading" (
       REM '=' is ignored by WinShell and stored to next parameter
       set loading=%~2
       if "!loading!"=="" goto usage
       shift
     ) else (
     if "!para:~0,9!"=="--silent" (
       REM '=' is ignored by WinShell and stored to next parameter
       set silent=%~2
       if "!silent!"=="" goto usage
       shift
     ) else (
     if "!para:~0,9!"=="--outdir" (
       REM '=' is ignored by WinShell and stored to next parameter
       set outdir=%~2
       if "!outdir!"=="" goto usage
       shift
     ) else (
     if "!para:~0,9!"=="--bundle" (
       REM '=' is ignored by WinShell and stored to next parameter
       set bundle=%~2
       for %%F in (!bundle!) do set bundle_ext=%%~xF
       if not "!bundle_ext!"==".zip" goto usage
       shift
     ) else (
     if "!para:~0,10!"=="--fstfile" (
       REM '=' is ignored by WinShell and stored to next parameter
       set fstfile=%~2
       if "!fstfile!"=="" goto usage
       shift
     ) else (
     if "!para:~0,11!"=="--isbundle" (
       REM '=' is ignored by WinShell and stored to next parameter
        set isbundle=%~2
       if "!isbundle!"=="" goto usage
       shift
     ) else (
     if "!para:~0,10!"=="--use7zip" (
       REM '=' is ignored by WinShell and stored to next parameter
        set use7zip=%~2
       if "!use7zip!"=="" goto usage
       shift
     )))))))))))
   ) else (
     for %%F in (!para!) do set zip_ext=%%~xF
     if not "!zip_ext!"==".zip" goto usage
     set ziplist=!ziplist! !para!
     set /a zip_cnt+=1
   )
shift
goto Loop
:continue

if "!ziplist!"=="" goto usage

if "!signing!"=="Y" set signing=y
if "!loading!"=="Y" set loading=y
if "!isbundle!"=="Y" set isbundle=y
if "!use7zip!"=="Y" set use7zip=y

REM at least one option --signing or --loading must be set to 'y'
if not "!signing!"=="y" (
  if not "!loading!"=="y" (
    goto usage
  )
)

rd /S /Q __bdldir > NUL 2>&1
rd /S /Q __bdltmp > NUL 2>&1

if "!isbundle!"=="y" (
  if !zip_cnt! GTR 1 (
    goto usage
  )
  
  mkdir __bdldir
  if not "!errorlevel!"=="0" (
    goto create_bundledir_failed
  )
  for /f "tokens=* delims= " %%a in ("!ziplist!") do set input_bundle=%%a
  if "!use7zip!"=="y" (
    "!7z_exe!" x "!input_bundle!" -o__bdldir > NUL 2>&1
  ) else (
    CScript "!script_path!unzip.vbs" !input_bundle! __bdldir > NUL 2>&1
  )
  if not "!errorlevel!"=="0" goto invalid_input_bundle_file
  REM if no outdir is specified for ZIP files
  if "!outdir!"=="" (
    REM use directory of input bundle, if no bundle should be created
    if "!bundle!"=="" (
      for /f %%i in ("!input_bundle!") do (
        set outdir=%%~di%%~pi
      )
    REM all files are added to bundle, so use __bdltmp, which is removed later
    ) else (
      mkdir __bdltmp
      if not "!errorlevel!"=="0" (
        goto create_bundledir_failed
      )
      set outdir=__bdltmp
    )
  )
  
  set ziplist=
  for /f "tokens=*" %%f in ('dir /s /b __bdldir\*.zip') do (
    REM remove current dir prefix from the path to get the relative path
    set p=%%f
    call set rel_path=%%p:!CD!\=%%
    set ziplist=!ziplist! !rel_path!
  )
  if "!ziplist!"=="" goto invalid_input_bundle_file
)

REM create/init temporary bundle dir
if not "!bundle!"=="" (
  if not exist __bdldir (
    mkdir __bdldir
    if not "!errorlevel!"=="0" (
      goto create_bundledir_failed
    )
  )
  REM add VFI installer tools
  if not exist "__bdldir\pkg_inst.sdm" copy pkg_inst.sdm __bdldir > NUL 2>&1
  if not exist "__bdldir\vfi.p7s" copy vfi.p7s __bdldir > NUL 2>&1
  if not exist "__bdldir\vfi.ped" copy vfi.ped __bdldir > NUL 2>&1
  if not exist "__bdldir\vxossign.crt" copy vxossign.crt __bdldir > NUL 2>&1
  REM remove exisiting package list (if any)
  del "__bdldir\pkg_inst.man" > NUL 2>&1
)

REM these lines are just for displaying screen information
set title_action=signing
if "!loading!"=="y" (
  set title_action=download
)
title ZIP file !title_action! for Verix eVo
echo.

if "!loading!"=="y" (
  if "!port!"=="" (
    if "!silent!"=="y" goto no_port_number
    cls
    if "!signing!"=="y" (
      echo Please start FST signing service and
      set /p port=enter COM port number for download: 
    ) else (
      set /p port=Please enter COM port number for download: 
    )
    if "!port!"=="" goto no_port_number
  )
) else (
  if not "!silent!"=="y" (
    cls
    echo Please start FST signing service and
    set /p dummy=press ENTER to continue with signing.
  )
)

if "!loading!"=="y" (
  if "!clearall!"=="" (
    if "!silent!"=="y" goto no_clearall_set
    set /p clearall=Clear all terminal files before download [y/n]?
  )
  if "!clearall!"=="Y" set clearall=y
)


REM list of ZIP files for DDL download
set loadzips=
REM list of ZIP files for DDL *UNZIP variable (without path)
set unzips=

cls
set /a first_file=1

REM Process the ZIP file list
for %%z in (!ziplist!) do (
  set zip=%%z
  
  REM zip file name without path
  for %%F in (!zip!) do set zip_name=%%~nxF
  REM init ZIP file variables
  set zip_file=!zip!
  set zip_file_name=!zip_name!
  
  REM sign the file
  if "!signing!"=="y" (
    REM specify ZIP output file extract filename from path and add suffix _signed.zip
    set zip_file=!zip_name:~0,-4%!_signed.zip
    REM store the ZIP file name without path before adding outdir below
    set zip_file_name=!zip_file!
    if not "!outdir!"=="" (
      if not exist "!outdir!" ( 
        mkdir "!outdir!"
        set outdir_created=1
      )
      set zip_file=!outdir!\!zip_file!
    )
    del "!zip_file!" > NUL 2>&1
    if "!first_file!"=="1" (
      set /a first_file=0
    ) else (
      echo.
    )
    echo Processing !zip_name!...
    set result=1
    call:signfile "!zip!" "!zip_file!" "!fstfile!" result
    if !result! equ 0 (
      echo Created !zip_file! successfully.
    ) else (
      if !result! equ 2 (
        REM files that were not signed will get suffix _unsigned.zip
        for %%F in (!zip!) do set zip_name=%%~nxF
        set zip_file=!zip_name:~0,-4%!_unsigned.zip
        REM store the ZIP file name without path before adding outdir below
        set zip_file_name=!zip_file!
        if not "!outdir!"=="" (
          if not exist "!outdir!" (
            mkdir "!outdir!"
            set outdir_created=1
          )
          set zip_file=!outdir!\!zip_file!
        )
        copy "!zip!" "!zip_file!" > NUL 2>&1
        echo Copied file to !zip_file!.
      ) else (
        goto signing_failed
      )
    )
  )
  
  REM create the ZIP list for loading or create bundle from the ZIP files
  if "!bundle!"=="" (
    if "!loadzips!"=="" (
      set loadzips=!zip_file!
      set unzips=!zip_file_name!
    ) else (
      set loadzips=!loadzips! !zip_file!
      set unzips=!unzips!,!zip_file_name!
    )
  ) else (
    REM add package to bundle
    if not "!zip_file!"=="__bdldir\!zip_name!" (
      copy "!zip_file!" "__bdldir\!zip_name!" > NUL 2>&1
    )
    echo I:1/!zip_name!>> __bdldir\pkg_inst.man
  )
)

REM create download bundle, if multiple ZIPs are downloaded
if not "!bundle!"=="" (
   del "!bundle!" > NUL 2>&1
   if "!use7zip!"=="y" (
     "!7z_exe!" a "!bundle!" .\__bdldir\* > NUL 2>&1
   ) else (
     CScript "!script_path!zip.vbs" __bdldir "!bundle!" > NUL 2>&1
   )
   if not "!errorlevel!"=="0" (
     goto create_bundle_failed
   )
   echo Created bundle !bundle! successfully.
   rd /S /Q __bdldir > NUL 2>&1
   REM for loading==y all ZIPs of bundle are downloaded to I:1
   set add_para=!add_para! setdrive.i setgroup.1
   REM bundle will be loaded instead of ZIPs
   set loadzips=!bundle!
   for %%F in (!bundle!) do set unzips=%%~nxF
)

if not "!loading!"=="y" goto end

REM start download
if "!signing!"=="y" echo.
echo Download ZIP files, please wait...
set clearflags=
if "!clearall!"=="y" set clearflags=-r*:*/ -rconfig.sys
set ddl_cmd=ddl -p!port! -c !clearflags! !add_para! *UNZIP=!unzips! !loadzips!
echo !ddl_cmd!
!ddl_cmd!
if "!errorlevel!"=="1" goto download_failed
goto end

REM subroutine to sign a ZIP file containing a sign.fst
REM sigfile <zip_to_sign.zip> <outfile.zip> <result>
REM <result> is an output parameter (0=OK, 1=error, 2=file skipped) 
:signfile
  REM init return code with error
  endlocal & set %~4=1
  rd /S /Q __exdir > NUL 2>&1
  mkdir __exdir
  if "!use7zip!"=="y" (
    "!7z_exe!" x "%~1" -o__exdir > NUL 2>&1
  ) else (
    "!script_path!unzip.vbs" "%~1" __exdir > NUL 2>&1
  )
  if not "!errorlevel!"=="0" goto signfile_cleanup
  
  cd __exdir
  
  set fst_file=%~3
  if exist "!fstfile!" (
    REM FileSignature tool will fail, if FST file contains
    REM a certificate location folder, which does not exist.
    REM Therefore, we create it to ship around this problem.
    set crt_location=
    for /f "usebackq delims=" %%a in ("!fst_file!") do (
      if "!crt_location!"=="" (
        set ln=%%a
        if "!ln:~0,1!"=="[" (
          set section=!ln!
        ) else (
          if "!section!"=="[Certificate name & location]/3" (
            REM remove " characters (if any)
            set crt_location=!ln:"=!
          )
        )
      )
    )
    
    REM create the folder, if it does not exist
    if not "!crt_location!"=="" (
      for /f %%i in ("!crt_location!") do (
         set crt_location=%%~dpi
      )
      if not exist "!crt_location!" mkdir "!crt_location!"
    )
  REM no FST file found
  ) else (
    REM skip this package, if no bundle should be created
    if "!bundle!"=="" (
      echo FST file "!fst_file!" not found, skip signing.
      endlocal & set %~4=2
      cd ..
      goto signfile_cleanup
    )
    REM skip this package, if it already contains any certificates
    dir /b /s *.crt > NUL 2>&1
    if "!errorlevel!"=="0" (
      echo FST file "!fst_file!" not found, skip signing.
      endlocal & set %~4=2
      cd ..
      goto signfile_cleanup
    )
    
    REM workaround for bundle mode:
    REM we need to sign at least one file of the ZIP, since terminal
    REM will abort installation after a ZIP not containing a certificate
    set /a any_file=0
    for /f "tokens=*" %%f in ('dir /a-d /b /s') do (
      if !any_file! equ 0 (
        for /f %%i in ("%%f") do (
          set fpath=%%~di%%~pi
          set fname=%%~ni%%~xi
        )
        REM remove current dir prefix from the path to get the relative path
        set p=!fpath!
        call set rel_path=%%p:!CD!\=%%
        REM skip files on root
        if not "!rel_path!"=="" (
          REM remove beginning '\'
          set rel_path=!rel_path:~0,-1!
          REM remove pending path (if any)
          for /f "delims=�" %%i in ("!rel_path:\=�!") do set path_prefix=%%i
          set flash_file=0
          echo !path_prefix!|findstr /r /c:"[f,F][0-9]*$" > NUL 2>&1
          if "!errorlevel!"=="0" set flash_file=1
          REM create a FST file
          echo [Files to sign]/1>>!fst_file!
          echo !flash_file! ".\!rel_path!\!fname!">>!fst_file!
          echo.>>!fst_file!
          echo [Security Level]/2>>!fst_file!
          echo ^0>>!fst_file!
          echo.>>!fst_file!
          echo [Certificate name ^& location]/3>>!fst_file!
          echo ".\!rel_path!\Certif.crt">>!fst_file!
          echo.>>!fst_file!
          set /a any_file=1
        )
      )
    )
  )
  REM do singing  
  set fst_cmd=FileSignature.exe !fst_file! -nogui
  echo Signing files, please wait...
  echo !fst_cmd!
  !fst_cmd!> fst_result.txt
  type fst_result.txt
  find "... OK!" fst_result.txt > NUL 2>&1
  if not "!errorlevel!"=="0" (
    cd ..
    goto signfile_cleanup
  )
  
  del !fst_file! fst_result.txt > NUL 2>&1
  
  REM move signature files (not already located on drive i) to drive i in a folder having the same group "i<gid>"
  dir /s /b *.p7s > NUL 2>&1
  if not "!errorlevel!"=="0" goto skip_p7s_loop
  
  setlocal disabledelayedexpansion
  for /f "tokens=*" %%f in ('dir /s /b *.p7s') do (
    for /f %%i in ("%%f") do (
      set fpath=%%~di%%~pi
      set fname=%%~ni%%~xi
    )
    setlocal enabledelayedexpansion
        
    REM remove current dir prefix from the path to get the relative path
    set p=!fpath!
    call set rel_path=%%p:!CD!\=%%
    REM p7s on root is ignored
    if not "!rel_path!"=="" (
      REM remove beginning '\'
      set rel_path=!rel_path:~0,-1!
      REM remove pending path (if any)
      for /f "delims=�" %%i in ("!rel_path:\=�!") do set path_prefix=%%i
      set path_suffix=
      if not "!rel_path!"=="!path_prefix!" (
        REM store the pending path
        call set path_suffix=%%rel_path:!path_prefix!\=%%
      )
      echo !path_prefix!|findstr /r /c:"^[0-9]*$" > NUL 2>&1
      if errorlevel 1 (
        REM str is not a number and contains drive prefix
        set drive=!path_prefix:~0,1!
        set group=!path_prefix:~1!
      ) else (
        REM str is already a group folder without drive
        set drive=""
        set group=!path_prefix!
      )
      if not "!group!"=="" (
        if "!drive!"=="I" set drive="i"
        if not "!drive!"=="i" (
          set destdir=i!group!
          if not "!path_suffix!"=="" set destdir=i!group!\!path_suffix!
          if not exist "!destdir!" mkdir "!destdir!"
          move "!rel_path!\!fname!" "!destdir!\!fname!" > NUL 2>&1
        )
      )
    )
    endlocal
  )
  endlocal
:skip_p7s_loop
  cd ..
  REM remove empty folders
  for /f "delims=" %%i in ('dir /b /s /ad __exdir') do rd "%%i" > NUL 2>&1
  if "!use7zip!"=="y" (
    "!7z_exe!" a "%~2" .\__exdir\* > NUL 2>&1
  ) else (
    CScript "!script_path!zip.vbs" __exdir "%~2" > NUL 2>&1
  )
  if not "!errorlevel!"=="0" goto signfile_cleanup
  REM result is OK
  endlocal & set %~4=0
  
:signfile_cleanup
  rd /S /Q __exdir > NUL 2>&1
  goto:eof
REM end of subroutine signfile

:no_port_number
echo Error: No port number specified^^!
goto failure

:no_clearall_set
echo Error: Flag 'clearall' not specified^^!
goto failure

:signing_failed
echo Error: Signing files failed^^!
goto failure

:download_failed
echo Error: File download failed^^!
goto failure


:create_bundledir_failed
echo Error: Cannot create bundle directory^^!
goto failure

:create_bundle_failed
echo Error: Cannot create bundle !bundle!^^!
goto failure

:invalid_input_bundle_file
echo Error: Invalid input bundle file !input_bundle_file!^^!
goto failure


:usage
echo Usage: ddl_zip_load.bat [--clearall=y/n] [--ddl_port=port] [--add_para=parameter]
echo                         [--signing=y/n] [--loading=y/n] [--silent=y/n] 
echo                         [--outdir=dir] [--bundle=dl.bundle.zip] [--fstfile=sign.fst]
echo                         [--isbundle=y/n] [--use7zip=y/n] file1.zip [file2.zip ...]
echo Sign and load Verix eVo download packages specified in parameter list. 
echo Description of parameters:
echo --clearall:  Clears all files in terminal before download (default: prompt user before download)
echo --ddl_port:  COM port used for DDL download tool (default: prompt user before download)
echo --add_para:  Additional parameter for DDL command line (default: empty string)
echo --signing:   Set to 'n' to skip signing and direct download of ZIP file (default: 'y')
echo --loading:   Set to 'n' for just signing without download the ZIP file (default: 'y')
echo              Note: At least one option --signing or --loading must be set to 'y'^^!
echo --silent:    Silent mode, do not prompt and abort if mandatory data is missing (default: 'n')
echo --outdir:    Output folder for signed ZIP files (default: folder of this batch file)
echo --bundle:    If this file name is set, a loadable bundle is created from the ZIP files list.
echo              If --loading is 'y' the bundle is loaded instead of the ZIP files (default: unset)
echo --fstfile:   Relative path to FST file in the ZIP. This file contains the certificate location
echo              the list of files to be signed. Default is "sign.fst", which is the standard
echo              FST file in ADK download packages.
echo --isbundle:  Set to 'y' to pass a bundle as ZIP input file. Please note that only one ZIP file
echo              is allowed to be passed as bundle in parameter list. Default is 'n', which ZIP files
echo              in parameter list are considered as download packages.
echo --use7zip:   Set to 'y' to use 7z.exe for ZIP packing/extraction. Default is 'n' for using
echo              VB scripts zip.vbs and unzip.vbs instead. Note: VB scripts or 7z.exe are looked up
echo              next to this file. If 7z.exe does not exist, it is used from ^%PATH^%.
echo.
echo Prerequisites for this batchfile:
echo 1. Installed File Signature Tool (FST) tool under "c:\Program Files (x86)\VeriFone\FST" or
echo    "c:\Program Files\VeriFone\FST"
echo 2. Installed DDL tool under "%%VRXSDK%%\bin"
echo 3. zip.vbs/unzip.vbs looked up next to this file (or 7z.exe, see option --use7zip)
goto failure

:failure
rd /S /Q __bdldir > NUL 2>&1
rd /S /Q __exdir > NUL 2>&1
if !outdir_created! EQU 1 rd /S /Q "!outdir!" > NUL 2>&1
exit /B 1

:end
rd /S /Q __bdldir > NUL 2>&1
rd /S /Q __exdir > NUL 2>&1
rd /S /Q __bdltmp > NUL 2>&1

exit /B 0