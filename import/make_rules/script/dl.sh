#!/bin/bash

argv0=$0

function usage {
   echo "Usage: $argv0 <httplink> [--component <name>] [--tag <tag>] [--targets <targets>]"
   echo "                         [--user <user>] [--password <password>] [--dir <dir>]"
   echo "                         [--dest <dir>]"
   exit 1
}

function clean_up {
  # housekeeping
  if [ -d $STAGING_DIR ]; then rm -rf $STAGING_DIR; fi
  trap 0  # reset to default action
  exit
}

# set defaults for some variables
TARGETS="vos vrx"
TARGETSCASE="vos VOS vos2 VOS2 vrx VRX svos SVOS x5000 X5000 x5000_p2 X5000_P2 win32 WIN32 win64 WIN64 lin32 LIN32 lin64 LIN64 doc DOC x86 X86 android_arm ANDROID_ARM trinity TRINITY android ANDROID"
USERNAME="adk_automation_ro"
PASSWORD=""
PASSWORDOPT=0
DOWNLOAD_DIR=dl
DESTDIR=import

POSITIONAL=()

while [[ $# -gt 0 ]]; do
    case "$1" in
        --component)
            COMPONENT="$2"
            shift 2
            ;;
        --tag)
            TAG="$2"
            shift 2
            ;;
        --targets)
            TARGETS="$2"
            shift 2
            ;;
        --user)
            USERNAME="$2"
            shift 2
            ;;
        --password)
            PASSWORD="$2"
            PASSWORDOPT=1
            shift 2
            ;;
        --dir)
            DOWNLOAD_DIR="$2"
            shift 2
            ;;
        --dest)
            DESTDIR="$2"
            shift 2
            ;;

        *)    # not an option
            POSITIONAL+=("$1")
            shift
            ;;
    esac
done
set -- "${POSITIONAL[@]}"

if [ ! $# -eq 1 ]; then usage; fi

LINK=$1

[ -z "$COMPONENT" ] && COMPONENT=$(echo $LINK | sed "s@.*/ADK/\(.*\)/\(.*\)/.*@\1@g" | tr [:upper:] [:lower:])
[ -z "$TAG" ] && TAG=$(echo $LINK | sed "s@.*/ADK/\(.*\)/\(.*\)/.*@\1-\2@g")

mkdir -p $DOWNLOAD_DIR
STAGING_DIR=$(mktemp -u $DOWNLOAD_DIR/staging_dir_XXXX)
EXTRACT_DIR=$STAGING_DIR/extract

trap clean_up 0 1 2 3 15
mkdir -p $STAGING_DIR

proceed=0

ZIP_FILE=`basename $LINK | tr [:upper:] [:lower:]`
echo "checking for $ZIP_FILE"

if [ ! -f $DOWNLOAD_DIR/$ZIP_FILE ]; then
  if [ "$PASSWORDOPT" = "0" ]; then
    if [ "$USERNAME" = "adk_automation_ro" ]; then
        PASSWORD=$(${argv0%/*}/get_password artifactory/adk_automation_ro 2>/dev/null)
        if [ "$?" != "0" ]; then
          echo "Not able to get password for default user 'adk_automation_ro'" >&2
          exit 1
        fi
    else
      # need to enter password
      read -s -p "Enter Artifactory password for user '$USERNAME': " PASSWORD
      echo ""
      fi
  fi
  (cd $STAGING_DIR; wget --http-user=$USERNAME --http-password=$PASSWORD $LINK)
  FILE=`ls $STAGING_DIR`
  mv $STAGING_DIR/$FILE $STAGING_DIR/$ZIP_FILE 2>/dev/null
  if [ ! -f $STAGING_DIR/*.zip ]; then
    echo "Couldn't download $LINK, exit."
    exit 1
  fi
  mv $STAGING_DIR/*.zip $DOWNLOAD_DIR/$ZIP_FILE
  touch $DOWNLOAD_DIR/$ZIP_FILE
  proceed=1
fi

if [ $proceed -eq 0 ]; then
  for target in $TARGETS;
  do
    # proceed, if no tag exists and target was not extracted before
    if [ ! -f $DOWNLOAD_DIR/$target/$COMPONENT/$TAG ]; then
      proceed=1
      break
      fi
    # proceed, if ZIP file is newer than the tag file
    if [ $DOWNLOAD_DIR/$ZIP_FILE -nt $DOWNLOAD_DIR/$target/$COMPONENT/$TAG ]; then
      proceed=1
      break
    fi
  done
fi

# we are done
if [ $proceed -eq 0 ]; then exit 0; fi

mkdir -p $EXTRACT_DIR
#extract distribution bundles into staging directory
( cd $EXTRACT_DIR
  unzip ../../$ZIP_FILE
  if test "$?" != "0"
  then
    echo "File \"../../$ZIP_FILE\" not found or not a valid ZIP format, exit."
    exit 1
  fi
)

# delete previous tags
for target in $TARGETS;
do
  if [ -d "$DOWNLOAD_DIR/$target/$COMPONENT" ]
  then
    for tagfile in `ls $DOWNLOAD_DIR/$target/$COMPONENT | xargs`;
    do
      if [ -f $DOWNLOAD_DIR/$target/$COMPONENT/$tagfile ];
      then
        for line in $(cat ${DOWNLOAD_DIR}/${target}/${COMPONENT}/${tagfile});
        do
          # remove the file (if any)
          if [ -f $DESTDIR/$target/$line ];
          then
            rm -f $DESTDIR/$target/$line
            # empty folder left -> remove it
            folder=`dirname $DESTDIR/$target/$line`
            folderfiles=`ls -A $folder`
            if [ -z "$folderfiles" ]; then rm -rf $folder; fi
          fi
          
          # remove folder, if it is empty
          if [ -d $DESTDIR/$target/$line ];
          then 
            rmdir $DESTDIR/$target/$line 2>/dev/null
          fi
        done
        rm -f $DOWNLOAD_DIR/$target/$COMPONENT/$TAG 2>/dev/null
      fi
    done
  fi
done

for i in $(echo $TARGETSCASE | gawk '{for(i=1;i<=NF;i++) z[tolower($i)]=1;} END {for(i in z) printf "%s ",i;}')
do
   mkdir -p $STAGING_DIR/$i
done

#check if the files are in the fullpackage format
fullpackage=0
for target in $TARGETS;
do
  if [ -d $EXTRACT_DIR/$target ]
  then
    fullpackage=1
    continue
  fi

  #Did not find find, try the upper case variant
  hcasedir=`echo $target | tr '[:lower:]' '[:upper:]'`
  
  if [ -d $EXTRACT_DIR/$hcasedir ]
  then
    fullpackage=1
  else
    #If we already know it is a full package make sure all specified targets are available.
    if [ $fullpackage -eq 1 ]
    then
      echo "ERROR. Targets $TARGETS specified, but target $target not available"
      echo "Use --targets to specify the available targets"
      exit 1
    fi
  fi
done

if [ $fullpackage -eq 1 ]
then
  for target in $TARGETSCASE;
  do
    lcasedir=`echo $target | tr '[:upper:]' '[:lower:]'`
    if [ -d $EXTRACT_DIR/$target ]
    then
      rmdir $STAGING_DIR/$lcasedir
      mv $EXTRACT_DIR/$target $STAGING_DIR/$lcasedir
    fi  
  done
  
  # If the contents are already extracted, then zip again
  # We check if there is a .zip in the folder, if yes we assume it is not extracted, but well-packaged
  # We need to delete everything which is not a zip, as not to confuse the final code-block
  
  for target in $TARGETS;
  do
    if [ -d $STAGING_DIR/$target ]
    then
      if [ "$target" = "doc" ]; 
        then 
        (cd $STAGING_DIR/$target; zip -r $COMPONENT-$TAG-$target.zip *)
        mv $STAGING_DIR/$target/$COMPONENT-$TAG-$target.zip $STAGING_DIR
        rm -Rf $STAGING_DIR/$target/
        mkdir $STAGING_DIR/$target/
        mv $STAGING_DIR/$COMPONENT-$TAG-$target.zip $STAGING_DIR/$target/
        continue
      fi
      if [ -z "$(ls $STAGING_DIR/$target/ | grep .zip)" ];
      then
        (cd $STAGING_DIR/$target; zip -r $COMPONENT-$TAG-$target-dev.zip *)
        mv $STAGING_DIR/$target/$COMPONENT-$TAG-$target-dev.zip $STAGING_DIR
        rm -Rf $STAGING_DIR/$target/
        mkdir $STAGING_DIR/$target/
        mv $STAGING_DIR/$COMPONENT-$TAG-$target-dev.zip $STAGING_DIR/$target/
      fi
    fi
  done
else
  #Single platform package
  
  if [ -d $EXTRACT_DIR/include ] || [ -d $EXTRACT_DIR/lib ] || [ -d $EXTRACT_DIR/load ] || [ -d $EXTRACT_DIR/doc ] || [ -d $EXTRACT_DIR/bin ]
  then
    for target in $TARGETS;
    do
      echo `echo $LINK | awk -F/ '{print $NF}'`
      if [ -n "$(echo $LINK | awk -F/ '{print $NF}' | grep $target-)" ]
      then
        if [ "$target" == "doc" ]
        then
          (cd $EXTRACT_DIR; zip -r $COMPONENT-$TAG-$target.zip *)
          mv $EXTRACT_DIR/$COMPONENT-$TAG-$target.zip $STAGING_DIR/$target
        else
          (cd $EXTRACT_DIR; zip -r $COMPONENT-$TAG-$target-dev.zip *)
          mv $EXTRACT_DIR/$COMPONENT-$TAG-$target-dev.zip $STAGING_DIR/$target
        fi
      else
        echo "ERROR. Targets $TARGETS specified, but target $target not available"
        echo "Use --targets to specify the available targets"
        exit 1
      fi
    done
  fi
fi

# extract libraries
for target in $TARGETS;
do   
  #folders are named VOS and VRX
  target_upper=$(echo $target | tr [:lower:] [:upper:])
  
  # handle captial letters of folders
  mv $STAGING_DIR/$target_upper $STAGING_DIR/$target 2>/dev/null
  
  for file in `ls $STAGING_DIR/$target/`;
  do
    if [[ "$file" == *"$target"*"barcodelibs"* ]] \
    || [[ "$file" == *"$target"*"dev.zip"* ]] \
    || [[ "$file" == *"doc.zip"* ]] \
    || [[ "$file" == *"$target"*"vats.zip"* ]] \
    || [[ "$target" == "vrx" ]];
    then
      rm -rf $DOWNLOAD_DIR/$target/$COMPONENT
      mkdir -p $DOWNLOAD_DIR/$target/$COMPONENT
      # add the file list to tag file and extract the archive to import
      unzip -l $STAGING_DIR/$target/$file | cut -c29- | tail -n +4 | head -n -2 | tr -d '\r' > $DOWNLOAD_DIR/$target/$COMPONENT/$TAG
      if test "$?" != "0"
      then
        echo "File \"$STAGING_DIR/$target/$file\" not found or not a valid ZIP format, exit."
        exit 1
      fi
      mkdir -p $DESTDIR
      unzip -o $STAGING_DIR/$target/$file -d $DESTDIR/$target
    fi
  done
done

