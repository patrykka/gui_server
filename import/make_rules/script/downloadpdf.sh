#!/bin/bash

argv0=$0
user="gss_automation_system"
passwd=""
passwdopt=0

function usage {
   echo "Download Confluence page as PDF."
   echo ""
   echo "Usage: $0 [--user <user>] [--password <password>] <url> <outfile.pdf>"
   exit 1
}

while [ $# -gt 1 -a "${1:0:2}" = "--" ] ; do
  case "$1" in
    --user)
      user="$2"
      shift 2
      ;;
    --password)
      passwd="$2"
      passwdopt=1
      shift 2
      ;;
    *)
      usage
      ;;
  esac
done

if [ $# != 2 ] ; then usage; fi

if [ "$passwdopt" = "0" ]; then
  if [ "$user" = "gss_automation_system" ] ; then
    passwd=$(${argv0%/*}/get_password confluence/gss_automation_system 2>/dev/null)
    if [ "$?" != "0" ]; then
      echo "Not able to get password for default user 'gss_automation_system'" >&2
      exit 1
    fi
  else
    # need to enter password
    read -s -p "Enter confluence password for user '$user': " passwd
    echo ""
  fi
fi

url="$1"
prot="http"
if [[ "$url" =~ "https://" ]]; then prot="https"; fi
host=$(echo $url | sed 's/'${prot}':\/\///;s|\/.*||')
dest="$2"

function urlencode() {
  # urlencode <string>
  local length="${#1}"
  for (( i = 0; i < length; i++ )); do
    local c="${1:i:1}"
    case $c in
      [a-zA-Z0-9.~_-]) printf "$c" ;;
      *) printf '%%%02X' "'$c"
    esac
  done
}

# URL encoding of user and password for wget
user=$(urlencode "$user")
passwd=$(urlencode "$passwd")

tmpdir=$(mktemp -d)
trap "rm -r $tmpdir" EXIT

# do the login
wget --save-cookies ${tmpdir}/cookies.bin --keep-session-cookies --post-data='os_username='"$user"'&os_password='"$passwd"'' ${prot}://${host}/dologin.action -O /dev/null

# donwload the HTML page containing the confluence page ID
wget --load-cookies ${tmpdir}/cookies.bin "$url" -O ${tmpdir}/page.html
# extract the page ID from HTML code
page_id=$(sed -n '/meta/s/.*name="ajs-page-id"\s\+content="\([^"]\+\).*/\1/p' ${tmpdir}/page.html)

# download PDF
msg=$(wget --load-cookies ${tmpdir}/cookies.bin ${prot}"://${host}/spaces/flyingpdf/pdfpageexport.action?pageId=${page_id}" -O "$dest" 2>&1)

# check result
if [[ $msg != *"[application/pdf]"* ]]
then
  echo "PDF generation failed!" >&2
  exit 1
fi
