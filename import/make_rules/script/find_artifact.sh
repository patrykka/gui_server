#!/bin/bash

argv0=$0

# set defaults for some variables
OUTPUT="version"
PASSWORD=""
PASSWORDOPT=0
PLATFORM=""
REPO="RMS_Release_CLW"
SPACE="ADK"
TARGET="full"
URL="https://artifactory.verifone.com/artifactory"
USERNAME="adk_automation_ro"
VERSION="latest"

# Retain spaces in a variable
IFS='%'

help="Usage: $argv0 [options..] <component>\n\
This script prints the latest version of artifact.\n\
 Options:\n\
   \t--url      <url>      Artifactory server URL [${URL}]\n\
   \t--repo     <repo>     Artifactory repository [${REPO}]\n\
   \t--space    <space>    Artifactory directory [${SPACE}]\n\
   \t--target   <target>   Target platform, e.g. vrx, vos2, [${TARGET}]\n\
   \t--platform <platform> Target sub-platform if appropriate, e.g. linux-x86_64 for x86 platform, [${PLATFORM}]\n\
   \t--user     <user>     Artifactory user [${USERNAME}]\n\
   \t--password <password> Artifactory password\n\
   \t--output   <output>   Output variant, one from [${OUTPUT}], link\n\
   \t--version  <version>  Exact component version [${VERSION}]\n\
   \tcomponent  ADK component lowercase, e.g. unittest\n\
 Note:\n\
   \t<target> can contain package type, e.g. vos-dev.\
"

usage()
{
  >&2 echo -e $help
   exit 1
}


POSITIONAL=()

while [[ $# -gt 0 ]]; do
    case "$1" in
        --output)
            OUTPUT="$2"
            shift 2
            ;;
        --password)
            PASSWORD="$2"
            PASSWORDOPT=1
            shift 2
            ;;
        --platform)
            PLATFORM="$2"
            shift 2
            ;;
        --repo)
            REPO="$2"
            shift 2
            ;;
        --space)
            SPACE="$2"
            shift 2
            ;;
        --target)
            TARGET="$2"
            shift 2
            ;;
        --url)
            URL="$2"
            shift 2
            ;;
        --user)
            USERNAME="$2"
            shift 2
            ;;
        --version)
            VERSION="$2"
            shift 2
            ;;

        *)    # not an option
            POSITIONAL+=("$1")
            shift
            ;;
    esac
done
set -- "${POSITIONAL[@]}"

# Check input parameters
if [ ! $# -eq 1 ]; then usage; fi
if [ ! "$OUTPUT" = "version" -a ! "$OUTPUT" = "link" ]; then usage; fi

if [ "$VERSION" = "latest" ]; then VERSION=""; else VERSION="$VERSION*"; fi

COMPONENT="${1}"

if [ "$PASSWORDOPT" = "0" ]; then
  if [ "$USERNAME" = "adk_automation_ro" ]; then
      PASSWORD=$(${argv0%/*}/get_password artifactory/adk_automation_ro 2>/dev/null)
      if [ "$?" != "0" ]; then
        echo "Not able to get password for default user 'adk_automation_ro'" >&2
        exit 1
      fi
  else
    # need to enter password
    read -s -p "Enter Artifactory password for user '$USERNAME': " PASSWORD
    echo ""
    fi
fi

if [ -z "$PLATFORM" ]; then
if [[ $TARGET =~ x86.* ]]; then
  # Determine extended artifact name based on current platform
  case $(gcc -dumpmachine) in
    *linux*) PLATFORM=linux-$(uname -m) ;;
    *cygwin) PLATFORM=cygwin-$(uname -m) ;;
    *mingw32) PLATFORM=mingw32-$(uname -m) ;;
  esac
fi
fi

artifact=$(wget -q --http-user=$USERNAME --http-password=$PASSWORD -O - "${URL}/api/search/pattern?pattern=${REPO}:${SPACE}/*/*/${COMPONENT}-${TARGET}*${VERSION}${PLATFORM}.zip" \
	| jq '.files[]' \
	| sed -e "s/^\"//" -e "s/\"[\r]*$//" \
	| sort -V \
	| tail -n 1)

if [ -z "$artifact" ]; then exit 1; fi

case $OUTPUT in
version)
	echo $artifact | cut -d'/' -f3
;;
link)
	echo ${URL}/${REPO}/$artifact
;;
esac
