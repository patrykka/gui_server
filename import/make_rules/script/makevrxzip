#!/bin/bash -e

argv0=$0

function usage {
   echo "Usage: $argv0 [--zipfile <name.zip>] [<files>] [--drive <drive> | --group <group>]"
   echo "   Creates a ZIP download package for Verix devices. Files and folders are imported"
   echo "   as parameter <files>, which is a file list with entries separated by blanks."
   echo "   Files and folders may be renamed using <newname>=<oldname> for an entry."
   echo "   Use '--zipfile' to specify the name of ZIP file for output. Default is 'a.zip'."
   echo "   Several destination drives may be separated by '--drive'."
   echo "   Several destination groups may be separated by '--group'."
   echo "   Drive and group is applied to <files> following option --drive and --group on"
   echo "   the command line. If both, --drive and --group are not provided, the files are"
   echo "   added to root of the ZIP file and then it is up to Verix OS to select the"
   echo "   location for installation. This is usually on drive I: in the group, which"
   echo "   the user has selected in Verix Terminal Manager when entering download mode."
   exit 1
}

# set to 1 to enable debug outputs
enable_debug=0
function debug {
  if [ "$enable_debug" -gt 0 ]; then echo "debug: $1"; fi
}

if [ $# = 0 ]; then usage; fi

tempdir=$(mktemp -d)
trap "rm -rf $tempdir" EXIT
drive=""
group=""
zipfile="a.zip"

while true; do

    while [ $# -gt 1 -a "${1:0:2}" = "--" ] ; do
        debug "parameter $1"
        case "$1" in
            --drive)
                drive="$2"
                if [ -n "$drive" ]; then
                  # check if we have a valid drive
                  drive=`echo "$drive" | tr '[:upper:]' '[:lower:]'`
                  if [ "$drive" != "i" -a "$drive" != "f" -a "$drive" != "n" ] ; then usage; fi
                fi
                debug "drive is $drive"
                shift 2
                ;;
            --group)
                group="$2"
                if [ -n "$group" ]; then
                  # check if group is a digit between 1 and 15 and not 46
                  nodigits=`echo "$group" | sed "s/[[:digit:]]//g"`
                  if [ -n "$nodigits" ] ; then usage; fi
                  if [ "$group" -ne 46 -a "$group" -gt 15 -o "$group" -lt 1 ]; then usage; fi
                fi
                debug "group is $group"
                shift 2
                ;;
            --zipfile)
                zipfile="$2"
                ext="${zipfile##*.}"
                # check for file extension "zip"
                if [ "$ext" != "zip" ]; then usage; fi
                shift 2
                ;;
            *)
                debug "1. usage"
                usage
                ;;
        esac
    done

    if [ $# = 0 ]; then
      debug "2. usage"
      usage;
    fi

    dest="${tempdir}/zip"
    if [ -n "$group" -o -n "$drive" ]; then
      dest="${dest}/${drive}${group}"
      debug "use dest=${dest}"
    fi
    echo mkdir -p "$dest"
    mkdir -p "$dest"

    x=0
    for i in "$@"; do

        debug "i=$i"

        if [ "$i" = "--drive" ]; then
            debug "--drive detected"
            shift $x
            continue 2
        elif [ "$i" = "--group" ]; then
            debug "--group detected"
            shift $x
            continue 2
        fi
        x=$(($x+1))

        right="${i#*=}"
        if [ "$i" = "$right" ] ; then
            echo cp -r "$i" "$dest"
            cp -r "$i" "$dest"
        else
            left="${i%%=*}"
            debug "left=${left}"
            # if source is a folder
            if [ -d "$right" ]; then
               leftdir="${left%%/*}"
               debug "leftdir=${leftdir}"
               debug "${leftdir} == ${left}"
               if [ "$leftdir" != "$left" ] ; then
                  echo mkdir -p "${dest}/${leftdir}"
                  mkdir -p "${dest}/${leftdir}"
               fi
               echo cp -r "$right" "${dest}/${left}"
               cp -r "$right" "${dest}/${left}"
            # else source is a file
            else
               leftdir="${left%/*}"
               debug "leftdir=${leftdir}"
               if [ "$leftdir" != "$left" ] ; then
                  echo mkdir -p "${dest}/${leftdir}"
                  mkdir -p "${dest}/${leftdir}"
               fi
               echo cp "$right" "${dest}/${left}"
               cp "$right" "${dest}/${left}"
            fi
        fi
    done

    break
done

find "${tempdir}/zip" -name .svn -print0 | xargs -0 rm -rf
(cd "${tempdir}/zip"; zip -r ../out.zip *)
rm "$zipfile" 2>/dev/null || true
mv "${tempdir}/out.zip" "$zipfile"

