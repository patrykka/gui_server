#!/usr/bin/env bash

##
# File:        mkx5000hxp
# Author:      D. Liebermann
# Created:     Wed Sep 05 13:00:00 CET 2018
#
# Discription: This script analyses the content of x5000_ADD_DLFILES_dev.
#              If there is content defined in this variables the content will
#              be analysed and the files are copied to the given destination
#              folder. 
#              E.g. ./common/usr2/*@tmp/ meens: all files from ./common/usr2
#                   will be copied to folder ./tmp/project_name/tmp
#              the folder ./tmp/project_name/tmp must be defined as imput in
#              variable x5000_APP_FILES.
##

while getopts :dr opt
do
  case $opt in
    d) VARIANT=debug;;
    r) VARIANT=release;;
  esac
done

echo -e "\n\n===                Script mkx5000hxp started...              ===\n"
target=x5000

echo "variant                               : " ${VARIANT}
echo "target                                : " ${target}

ADD_DLFILES=$(printenv ${target}_ADD_DLFILES_dev)
echo "ADD_DLFILES                           : " ${ADD_DLFILES}

if test "${ADD_DLFILES}" != ""
then
  tmpDir="./tmp"
  rm -rf $tmpDir
  mkdir $tmpDir
  echo "tmpDir                                : " ${tmpDir}

  hxpFileName=$(printenv ${target}_${VARIANT}_HXPSOURCE)
  echo "HXP name (x5000_${VARIANT}_HXPSOURCE)    : " ${hxpFileName}
  hxpContentDir="${tmpDir}/${hxpFileName%.*}"
  mkdir "$hxpContentDir"
  echo "HXP content dir                       : " ${hxpContentDir}

  curHxpContentFiles=$(printenv x5000_ADD_DLFILES_dev)
  echo "curHxpContentFiles                    : " ${curHxpContentFiles}
  altHxpContentFiles=$(printenv x5000_ADD_DLFILES_dev_$VARIANT)
  echo "altHxpContentFiles                    : " ${altHxpContentFiles}
  copies=0
  links=0
  for k in ${curHxpContentFiles} ${altHxpContentFiles}
  do
    if [[ $k == *">"* ]]
    then
      if test "$(uname -o)" = "Msys"
      then
        echo -e "\033[1;31mError: Symbolic links do not work with Msys\033[0m" >&2
        exit 1
      else
        src=${k#*>}
#        echo "=== src                     : " ${src}
        dst=${tmpDir}/${k%>*}
#        echo "=== dst                     : " ${dst}
        test ! -d "${dst%/*[^/]}" && mkdir -p "${dst%/*[^/]}"
        ln -s $src $dst
        if test "$?" = "0"
        then
          let "links += 1"
        fi
      fi
    else
      src=${k%@*}
#      echo "src                     : " ${src}
      dst=${k#*@}
#      echo "dst                     : " ${dst}
      if test "$src" = "$dst"
      then
        dst="$hxpContentDir"
      else
        dst="$hxpContentDir/$dst"
        test ! -d "${dst%/*[^/]}" && mkdir -p "${dst%/*[^/]}"
      fi
      files="$(cp -vr $src $dst | sed -e "s|.* -> ||" -e "s|[^/]*||" -e "s|.$||")"
      IFS=$'\n'
      for fname in $files
      do
        if test ! -d "$fname"
        then
          let "copies++"
        fi
      done
      unset IFS
    fi
  done
  echo -e "\n===                $copies file(s) copied."
  if test "$links" != "0"
  then
    echo " \n===                $links link(s) created."
  fi
  echo -en "\n\033[42m!!! Creation of HXP content directory ${dst} done.    S u c c e s s f u l     !!!\033[0m\n\n"
else
  echo -e "\n\033[43m!!! Warning: No x5000_ADD_DLFILES_dev or _prd defined. No content directory created.\033[0m\n"
fi
