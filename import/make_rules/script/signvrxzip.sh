#!/bin/bash -e

argv0=$0
scriptdir=${0%/*}

bundle=""
ziplist=""
outdir=""
outdir_created=false
suffix=""
fstfile="syssign.fst"
verbose=false
silent=false
keep_fst=false
enoact=false
testmode=false

function error_cleanup {
  # remove partial filled output directory
  if $outdir_created; then rm -rf $outdir 2>/dev/null || true; fi
}

function usage {
   echo "Usage: $argv0 [-f <fst file>] [-v] [-S] [-o <dir>] [-s <suffix>]"
   echo "       [-b dl.bundle.zip] [-k] [-e] [-t] <file.zip ...>"
   echo "Sign a Verix eVo ZIP installation package containing a FST file (option -f)."
   echo "For productive signing (without option -t) make sure that signing service with"
   echo "File Signing Tool (FST) runs on your PC when calling this script."
   echo "Description of options:"
   echo "-f  Relative path to FST file in the ZIP. This file contains the certificate location"
   echo "    the list of files to be signed. Option -f is useful to handle packages for different"
   echo "    signing cards, e.g. \"-f sign.fst\" usually contains files for user signing."
   echo "    Default is \"syssign.fst\", which is generally used for EOS singing."
   echo "-v  Verbose mode"
   echo "-S  Silent mode, do not prompt user and use suitable defaults."
   echo "-o  Output directory for signed packages. If not specified, ZIP files are replaced."
   echo "    Note: Skipped packages (that were not signed) are not stored to this directory."
   echo "-s  Name suffix for signed packages to avoid that origin packages are replaced,"
   echo "    e.g. \"-s -signed\" creates dl.package-signed.zip for dl.package.zip".
   echo "    Note: Skipped packages (that were not signed) do not get this suffix."
   echo "-b  If this file name is set, a loadable bundle is created from the ZIP file list."
   echo "    The specified bundle must have extension .zip, e.g. \"-b bundle.zip\""
   echo "    A path prefix in file name is allowed, but please note that output directory must"
   echo "    exists. If option -o is used, the path prefix of the file name (if any) is ignored."
   echo "-k  Keep FST file (specified by option -f) after signing. Default: FST file"
   echo "    will be deleted."
   echo "-e  Exits with error code 2, if the specified installation package got not signed."
   echo "    This option only is reliable with one installation package."
   echo "-t  Enable signing for test mode devices. This option requires signing tool"
   echo "    FileSign.exe for test mode in folder ../bin next to this script or in PATH."
   echo "    Note: Option -t cannot be used for EOS packages (see option -f)."
   error_cleanup
   exit 1
}

function debug {
  if $verbose; then echo -e "DEBUG: $1"; fi
}

while getopts :f:vSo:s:b:ket opt
do
  case $opt in
    f) fstfile="$OPTARG";;
    v) verbose=true;;
    S) silent=true;;
    o) outdir="$OPTARG";;
    s) suffix="$OPTARG";;
    b) bundle="$OPTARG";;
    k) keep_fst=true;;
    e) enoact=true;;
    t) testmode=true;;
    *) usage;;
  esac
done

shift $((OPTIND - 1))

if [ $# -lt 1 ]; then usage; fi

if test "$(uname -o)" = "Msys"
then
    echo -e "\033[1;31mError: Msys is not supported\033[0m" >&2
    exit 1
fi

# both signer tools are required for bundle mode
use_testsigner=true
use_prodsigner=true
if [ -z "$bundle" ]; then
  if $testmode; then use_prodsigner=false;
  else               use_testsigner=false;
  fi
fi
# check existence signing tools
if $use_testsigner; then
  if [ -f "${scriptdir}/../bin/FileSign.exe" ]; then
    filesign="${scriptdir}/../bin/FileSign.exe"
  elif [ -n "`which FileSign.exe`" ]; then
    filesign="`which FileSign.exe`"
  else
    echo -e "\033[1;31mFileSign Tool for test mode does not exist in script folder, exit.\033[0m" >&2
    exit 1
  fi
fi
if $use_prodsigner; then
  # check OS
  if test "$(uname -o)" = "Cygwin"
  then # Windows
    # check existence of FileSignature Tool
    fst=
    for i in "${PROGRAMFILES}" "${ProgramW6432}" "C:\Program Files (x86)"
    do
      if [ -f "${i}/VeriFone/FST/FileSignature" ]; then
        fst="${i}/VeriFone/FST/FileSignature"
        break
      fi
    done
    if [ -z "$fst" ]; then
      echo -e "\033[1;31mVerifone FileSignature Tool (FST) is not installed, exit.\033[0m" >&2
      exit 1
    fi
  else # Linux
    fst="$(printenv vrx_SIGNER)"
    if test -z "$fst" -o "$fst" = "NOTAVAIL"
    then
      echo -e "\033[1;31mProductiver signer not available, please set vrx_SIGNER in environment!\033[0m" >&2
      exit 1
    fi
  fi
fi

tmpdir=$(mktemp -d)
trap "rm -rf $tmpdir" EXIT
# extract folder for ZIP contents
mkdir ${tmpdir}/zip

# setup the output directory (if any)
if [ -n "$outdir" ]; then
  if [ ! -e "$outdir" ]; then
    mkdir -p $outdir
    if [ "$?" != "0" ]; then
      echo -e "\033[1;31mOutput directory \"$outdir\" cannot be created, exit.\033[0m" >&2
      exit 1
    fi
    outdir_created=true
    debug "created output directory \"$outdir\"."
  else
    if [ ! -d "$outdir" ]; then
      echo -e "\033[1;31mOutput directory \"$outdir\" is not a directory, exit.\033[0m" >&2
      exit 1
    fi
  fi
fi

# setup the bundle directory
if [ -n "$bundle" ]; then
  bname=$(basename "$bundle")
  bext="${bname##*.}"
  if [ "$bext" != "zip" ]; then
    echo -e "\033[1;31mInvalid bundle file extension specified by option \"-b\", exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi
  if [ -z "${bname%.*}" ]; then
    echo -e "\033[1;31mInvalid bundle file name specified by option \"-b\", exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi
  bdir=$(dirname "$bundle")
  if [ ! -d "$bdir" -a -z "$outdir" ]; then
    echo -e "\033[1;31mInvalid bundle file path specified by option \"-b\", exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi
  mkdir ${tmpdir}/bundle
  # add VFI installer files
  (cd "$scriptdir"; cp pkg_inst.sdm vfi.p7s vfi.ped vxossign.crt ${tmpdir}/bundle 2>/dev/null)
  if [ "$?" != "0" ]; then
    echo -e "\033[1;31mVFI installer files not available, exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi
fi

if ! $verbose; then zipopts="-q"; fi

# flag used to prompt user for re-inserting the signing card
reinsert_card=false

# process the ZIP file list
for i in $@; do
  filename=$(basename "$i")
  extension="${filename##*.}"
  if [ "$extension" != "zip" ]; then usage; fi
  if [ -z "${filename%.*}" ]; then usage; fi
  if [ ! -f "$i" ]; then
    echo -e "\033[1;31mFile \"$i\" does not exist, exit.\033[0m" >&2;
    error_cleanup
    exit 1;
  fi
  echo "Processing \"$i\"..."
  debug "unzip $zipopts $i -d ${tmpdir}/zip"
  unzip $zipopts $i -d ${tmpdir}/zip
  if [ "$?" != "0" ]; then
    echo -e "\033[1;31mFile \"$i\" not found or not a valid ZIP format, exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi

  # prompt user to re-insert the card (note: reinsert_card is not set in silent mode)
  if $reinsert_card; then
    reinsert_card=false
    read -p "If desired, re-insert FST signing card and press ENTER to continue."
  fi

  fst_file="$fstfile"

  # if FST file is missing
  if [ ! -f "${tmpdir}/zip/${fst_file}" ]; then
    # skip package, if no bundle should be created
    if [ -z "$bundle" ]; then
      echo -e "\033[1;33mFile \"$fst_file\" not found. Skipped signing \"$i\".\033[0m" >&2
      rm -rf $tmpdir/zip* 2>/dev/null || true
      continue
    fi

    # bundle mode !!!

    # if ZIP already contains certificates, signing is skipped
    any_file=$(find ${tmpdir}/zip -type f -iname "*.crt" 2>/dev/null)
    if [ -n "$any_file" ]; then
      debug "cp $i ${tmpdir}/bundle"
      cp "$i" "${tmpdir}/bundle"
      echo -e "\033[1;33mPackage \"$i\" already signed, added to bundle.\033[0m" >&2
      rm -rf $tmpdir/zip* 2>/dev/null || true
      continue
    fi

    # use another FST file, found in ZIP
    fst_files=($(find ${tmpdir}/zip -maxdepth 1 -type f -iname "*.fst" -print 2>/dev/null))
    if [ ${#fst_files[*]} -gt 0 ]; then
      # for silent mode use the first FST found in ZIP
      if $silent; then
        echo -e "\033[1;33mFile \"$fst_file\" not found. Use \""$(basename ${fst_files[0]})"\" (silent mode).\033[0m" >&2
        fst_file=$(basename ${fst_files[0]})
      else
        # prompt the user to select another file from package
        echo -e "\033[1;33mFile \"$fst_file\" not found.\033[0m" >&2
        echo "Select other FST file found in \"${filename}\":"
        idx=0
        for f in ${fst_files[@]}; do
          echo "("${idx}"): "$(basename ${f})
          idx=$(($idx+1))
        done
        echo "(a): abort"
        read -p ">" idx
        case ${idx:0:1} in
          a|A )
            echo -e "\033[1;31mAborted, exit.\033[0m" >&2
            error_cleanup
            exit 1
          ;;
          * )
            if [ $idx -lt 0 -o $idx -ge ${#fst_files[*]} ]; then
              echo -e "\033[1;31mWrong input, exit.\033[0m" >&2
              error_cleanup
              exit 1
            fi
            fst_file=$(basename ${fst_files[$idx]})
            echo -e "\033[1;33mUse \"$fst_file\", continue...\033[0m" >&2
          ;;
        esac
      fi
    fi

    # workaround for bundle mode, if no FST file is available:
    # NOTE: At least one file of the ZIP must be signed, otherwise terminal
    # will abort installation, since the ZIP will not contain a certificate.
    if [ ! -f "${tmpdir}/zip/${fst_file}" ]; then
      # for silent mode use the first FST found in ZIP
      if ! $silent; then
        echo -e "\033[1;33mFile \"$fst_file\" not found.\033[0m" >&2
        read -p "Proceed with signing an arbitrary file (y/n) >" yesno
        case $yesno in
          [Yy]* ) ;;
          * ) echo -e "\033[1;31mAborted, exit.\033[0m" >&2
              error_cleanup
              exit 1
              ;;
        esac
      fi
      # search for an arbitrary file to sign (ignore files on root level)
      any_file=$(find ${tmpdir}/zip -mindepth 2 -type f -print 2>/dev/null | head -n 1)
      if [ -z "$any_file" ]; then
        echo -e "\033[1;31mInvalid package format. No files found in \"$i\", exit.\033[0m" >&2
        error_cleanup
        exit 1
      fi
      rel_path=$(dirname $any_file)
      # get the relative path to the file
      rel_path=${rel_path#${tmpdir}/zip/}
       # save file path for test mode signing (see below)
      rel_path_crt_file=${rel_path}/Certif.crt
      # replace '/' by '\'
      rel_path=${rel_path////\\}
      # remove pending path to get the path prefix, e.g. "f1" or "1"
      path_prefix=${rel_path%%\\*}
      flash_drive=0
      if [ -n "$(echo "$path_prefix" | grep -E "[f,F][0-9]*$")" ]; then flash_drive=1; fi
      # reduce filpath to filename
      any_file=$(basename $any_file)
      # create a temporary fst file
      echo "[Files to sign]/1">>${tmpdir}/zip/${fst_file}
      echo "${flash_drive} \".\\${rel_path}\\${any_file}\"">>${tmpdir}/zip/${fst_file}
      echo "">>${tmpdir}/zip/${fst_file}
      echo "[Security Level]/2">>${tmpdir}/zip/${fst_file}
      echo "0">>${tmpdir}/zip/${fst_file}
      echo "" >>${tmpdir}/zip/${fst_file}
      echo "[Certificate name & location]/3">>${tmpdir}/zip/${fst_file}
      echo "\".\\${rel_path}\Certif.crt\"">>${tmpdir}/zip/${fst_file}
      echo "">>${tmpdir}/zip/${fst_file}
      debug "Created dummy \"$fst_file\" for package \"$filename\"."
    fi
  fi

  # convert existing FST file (workarounds for FileSignature tool):
  # 1. workaround: replace \ by / in file paths, since FileSignature
  # will fail on Linux with \, wheres as Windows is able to use /
  ( cd ${tmpdir}/zip &&
    awk '/^[ \t]*\[/{z=0} {if((z==1||z==2) && NF!=0) {gsub(/\\/,"/"); print $0} else {print $0} } /^[ \t]*\[Files to sign\]/{z=1} /^[ \t]*\[Certificate name & location\]/{z=2}' "${fst_file}" >tmp &&
    mv -f tmp "${fst_file}")
  # 2. workaround: FileSignature tool will fail, if FST file
  # contains a certificate location folder, which does not exist.
  # Therefore, we create it to ship around this problem.
  crt_location=$(awk '/^[ \t]*\[/{z=0} {if(z==1 && NF!=0) {print $0; z=0} } /^[ \t]*\[Certificate name & location\]/{z=1}' ${tmpdir}/zip/${fst_file})
  # remove " characters (if any)
  crt_location=${crt_location#*\"}
  crt_location=${crt_location%\"*}
  # replace \ by / and remove leading "./" if any
  crt_location=$(echo $crt_location | sed -e 's/^\.\///g')
  # save file name for test mode signing (see below), remove leading "./" if any
  rel_path_crt_file="$crt_location"
  # create the absolute temp dir path
  crt_location=$(dirname $crt_location)
  crt_location=$(echo "${tmpdir}/zip/${crt_location}" | sed s#//*#/#g)
  mkdir -p "$crt_location" 2>/dev/null
  if [ "$?" != "0" ]; then
    echo -e "\033[1;31mCould not create certificate location directory \"$crt_location\", exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi

  test_signing=$testmode
  # check to use the correct signing tool
  if $test_signing; then
     # EOS packages cannot be signed with test mode signer
    if [ "$fst_file" = "syssign.fst" ]; then
      # abort for a single download package
      if [ -z "$bundle" ]; then
        echo -e "\033[1;31mOption -t is not allowed for EOS package \"${filename}\", exit.\033[0m" >&2
        error_cleanup
        exit 1
      fi

      # bundle mode !!!

      # silent mode: echo warning and switch hard to productive FST signer with current signing card
      if $silent; then
        echo -e "\033[1;33mUse productive FST signer for EOS package \"${filename}\" (silent mode).\033[0m" >&2
      else
        # prompt the user to switch to EOS signing card
        echo -e "\033[1;33mEOS package \"${filename}\" must be signed with FST signer and EOS signing card.\033[0m" >&2
        read -p "Proceed signing with productive FST signer (y/n) >" yesno
        case $yesno in
          [Yy]* ) ;;
          * ) echo -e "\033[1;31mAborted, exit.\033[0m" >&2
            error_cleanup
            exit 1
            ;;
        esac
      fi
      test_signing=false
    fi
  else
    # another FST file was selected
    if [ "$fst_file" != "$fstfile" ]; then
      if $silent; then
        echo -e "\033[1;33mUse productive FST signer for \"${filename}\" (silent mode).\033[0m" >&2
      else
        # prompt the user to switch to another signing card
        echo -e "If desired, change FST signing card for \"${filename}\"." >&2
        read -p "Proceed signing with productive FST signer (y/n) >" yesno
        case $yesno in
          [Yy]* ) ;;
          * ) echo -e "\033[1;31mAborted, exit.\033[0m" >&2
            error_cleanup
            exit 1
            ;;
        esac
        # set flag to prompt user to re-insert the card for next package
        reinsert_card=true
      fi
    fi
  fi

  # do signing
  if $test_signing; then
    # test mode signing
    awk '/^[ \t]*;/{next} /^[ \t]*\[/{z=0} {if(z==1 && NF!=0) print $0; } /^[ \t]*\[Files to sign\]/{z=1}' ${tmpdir}/zip/${fst_file} | while read -r flash_flag file_to_sign; do
      # remove " characters (if any)
      file_to_sign=${file_to_sign#*\"}
      file_to_sign=${file_to_sign%\"*}
      # replace \ by / and remove leading "./" if any
      file_to_sign=$(echo $file_to_sign | sed -e 's/\\/\//g' -e 's/^\.\///g')
      # save name for output below
      _file_to_sign="$file_to_sign"
      # create the absolute temp dir path
      file_to_sign=$(echo "${tmpdir}/zip/${file_to_sign}" | sed s#//*#/#g)
      # file path must be converted to windows style, otherwise FileSign fails
      if [ "$(uname -o)" = "Cygwin" ]; then
        file_to_sign=$(cygpath -w "$file_to_sign")
      fi
      flashopt=""
      if [ "$flash_flag" != "0" ]; then flashopt="-L"; fi
      echo -n "Signing File $_file_to_sign... "
      $filesign -C "${scriptdir}/vs090400.crt" -K "${scriptdir}/vs090400.key" -F "$file_to_sign" -S "$file_to_sign.p7s" $flashopt 2>/dev/null
      if [ "$?" == "0" ]; then
        echo "OK!"
      else
        echo "FAILED!"
        exit 1;
      fi
    done
    if [ "$?" != "0" ]; then
      echo -e "\033[1;31mSigning failed, exit.\033[0m"
      error_cleanup
      exit 1
    fi
    # create absolute tempdir path of certificate file
    rel_path_crt_file=$(echo "${tmpdir}/zip/${rel_path_crt_file}" | sed s#//*#/#g)
    # copy certificate file
    cp "${scriptdir}/vs090400.crt" "$rel_path_crt_file"
    # copy sponsor certificate
    rel_path_crt_file=$(dirname $rel_path_crt_file)/sponsorCert.crt
    cp "${scriptdir}/VS208736.crt" "$rel_path_crt_file"
  else
    # productive signing
    debug "Signing \"$filename\"... "
    debug "\$($fst ${fst_file} -nogui 2>&1)"
    pushd "${tmpdir}/zip" > /dev/null
    msg="$("$fst" "${fst_file}" -nogui 2>&1)" || true
    popd > /dev/null
    if [[ ! $msg =~ "... OK"|"Signing file successful." ]]; then
      echo $msg >&2
      echo -e "\033[1;31mSigning failed, exit.\033[0m"
      error_cleanup
      exit 1
    fi
    echo "$msg"
  fi

  enoact=false  # do not return 2, if at least one file signed
  # delete fst file after signing by option
  if ! $keep_fst; then rm -f "${tmpdir}/zip/${fst_file}" 2>/dev/null || true; fi

  # move signature files (not already located on drive i) to drive i in a folder having the same group "i<gid>"
  for j in $(find ${tmpdir}/zip -type f -iname "*.p7s" -printf '%P\n' 2>/dev/null | xargs); do
    fpath=$(dirname "$j")
    fname=$(basename "$j")

    # get the path prefix, e.g. "1" of "1/a/b" . Rest of path "a/b" is stored in path_suffix
    path_prefix=${fpath%%/*}
    path_suffix=""
    if [ "$fpath" != "$path_prefix" ]; then path_suffix=${fpath#*/}; fi

    nodigits=$(echo "$path_prefix" | sed "s/[[:digit:]]//g")
    if [ -n "$nodigits" ] ; then
      # folder is not a number and contains drive prefix
      drive=${path_prefix:0:1}
      group=${path_prefix:1}
    else
      # already a group folder without drive prefix
      drive=""
      group="$path_prefix"
    fi
    if [ -n "$group" ]; then
      if [ "$drive" = "I" ]; then drive="i"; fi
      if [ "$drive" != "i" ]; then
        destdir="${tmpdir}/zip/i${group}"
        # if there is already a folder with capital 'I',
        # use it instead of creating a new one
        if [ -d "${tmpdir}/zip/I${group}" ]; then
          destdir="${tmpdir}/zip/I${group}"
        fi
        # append the path suffix
        if [ -n "$path_suffix" ]; then destdir=${destdir}/${path_suffix}; fi
        mkdir -p "$destdir"
        mv "${tmpdir}/zip/$j" "${destdir}/${fname}" 2>/dev/null
        debug "Moved signature file ${fname}:\nmv ${tmpdir}/zip/$j ${destdir}/${fname} 2>/dev/null"
      fi
    fi
  done

  # default: replace the existing ZIP file
  zipdest="$i"
  # prepare the output ZIP file
  zipfile=$(basename "$i")
  # ZIP file has a file suffix (set by option -s)
  if [ -n "$suffix" ]; then
    ext=${i##*.}
    name=$(basename "$i" .$ext)
    zipfile=${name}${suffix}.${ext}
  fi

  if [ -n "$bundle" ]; then  # bundle mode
    # just move to temporary bundle dir
    zipdest=${tmpdir}/bundle/${zipfile}
  else # no bundle mode
    if [ -n "$outdir" ]; then
      # store ZIP file to output directory (having the same basename)
      zipdest=$(echo "${outdir}/${zipfile}" | sed s#//*#/#g)
    else
      # if a suffix was set, new ZIP file is added on same level as origin one
      dir=$(dirname "$i")
      if [ -n "$dir" ]; then
        zipdest=$(echo "${dir}/${zipfile}" | sed s#//*#/#g)
      fi
    fi
  fi
  rm -f $zipdest 2>/dev/null || true

  debug "(cd $tmpdir/zip; zip $zipopts -r ../$zipfile *)"
  (cd $tmpdir/zip; zip $zipopts -r ../$zipfile *)
  debug "${tmpdir}/${zipfile} $zipdest 2>/dev/null"
  mv "${tmpdir}/${zipfile}" "$zipdest" 2>/dev/null
  if [ "$?" != "0" ]; then
    echo -e "\033[1;31mCouldn't create package \"$zipdest\", exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi
  if [ -n "$bundle" ]; then
    echo "Added package \""$(basename "$zipdest")"\" to bundle \""$(basename "$bundle")"\"."
  else
    if [ "$zipdest" = "$i" ]; then
      echo -e "\033[0;32mReplaced package \"$zipdest\" successfully.\033[0m"
    else
      echo -e "\033[0;32mCreated package \"$zipdest\" successfully.\033[0m"
    fi
  fi
  rm -rf $tmpdir/zip* 2>/dev/null || true
done

if $enoact; then exit 2; fi

# create the bundle
if [ -n "$bundle" ]; then
  (cd $tmpdir/bundle;
   for i in $(find . -type f -iname "*.zip" -printf '%P\n' 2>/dev/null | xargs); do
     # add filename to package list
     echo "I:1/${i}">> pkg_inst.man
     debug "Added \"${i}\" to bundle list \"pkg_inst.man\"."
   done
  debug "zip $zipopts -r ../bundle.zip *"
  zip $zipopts -r ../bundle.zip *)
  bundledest="$bundle"
  if [ -n "$outdir" ]; then
    bundledest=$(echo "${outdir}/"$(basename "$bundle") | sed s#//*#/#g)
  fi
  mv "${tmpdir}/bundle.zip" "$bundledest" 2>/dev/null
  if [ "$?" != "0" ]; then
    echo -e "\033[1;31mCouldn't create bundle \"$bundledest\", exit.\033[0m" >&2
    error_cleanup
    exit 1
  fi
  echo -e "\033[0;32mCreated bundle \"$bundledest\" successfully.\033[0m"
fi

exit 0
