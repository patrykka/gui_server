#ifndef _LIBCCP_H_
#define _LIBCCP_H_

#if defined __GNUC__ && defined VFI_CCP_SHARED_EXPORT
#define DllSpec __attribute__((visibility("default")))
#else
#define DllSpec // used for both, static libraries and program symbol import
#endif

#ifdef __cplusplus
extern "C" {
#endif

enum CCPMenuName {
	CCP_MENU_MAIN = 0,
	CCP_MENU_MAIN_NO_PWD,
	CCP_MENU_CONFIGURATION,
	CCP_MENU_CONFIGURATION_NO_PWD,
	CCP_MENU_DIAGNOSTIC,
	CCP_MENU_INFORMATION
};

enum CCPErrorCodes {
	CCP_LIB_OK = 0,
	CCP_LIB_FAIL
};

enum CCPInterfaces {
	CCP_INTERFACE_ETH0                = 0,
	CCP_INTERFACE_ETH1                = 1,
	CCP_INTERFACE_WLAN0               = 2,
	CCP_INTERFACE_PPP_DIAL            = 3,
	CCP_INTERFACE_GPRS0               = 4,
	CCP_INTERFACE_ETH_BT              = 5,
	CCP_INTERFACE_PPP_BT              = 6,
	CCP_INTERFACE_ETH_USB_GADGET      = 7,
	CCP_INTERFACE_ETH_USB1_GADGET     = 8,
	CCP_INTERFACE_ETH_USB_HOST_GADGET = 9,
	CCP_INTERFACE_PPP_USBD            = 10,
	CCP_INTERFACE_BRIDGE              = 11,
	CCP_INTERFACE_SERIAL_USBD         = 12,
	CCP_INTERFACE_ETH_USB0            = 13,
	CCP_INTERFACE_BT                  = 14
};

typedef struct
{
	const char *name;
	const char *mccMnc;
	const char *apn;
	const char *username;
	const char *password;
	const char *authentication;
} CCPgprsParameter;

/** returns a zero-terminated string with version and build information of libccp
 * Format: [major].[minor].[patch]-[build]
 * @return version string */
DllSpec const char *ccp_getVersion(void);

/**
 * Show selected dialog from Com Control Panel to configure your communication device
 * 
 * When using this call on an unattended device (ux300, ux301, ux410) the GUI property UI_PROP_TIMEOUT
 * will be checked. If the value is positive, then this value will be respected. If not, then the property will be
 * set to 60000 milliseconds at the beginning of ccp_show and reverted back to its previous value when ccp_show returns.
 * 
 * 
 * @brief ccp_show					Show selected dialog from Com Control Panel to configure your
 *									communication device
 *
 * @param which						Which dialog to show. See CCPMenuName.
 * @param infodb					path to database for storing of the settings (eg. "flash/info.db")
 * @param useInternalKeyTimeout		Use an ccp internal timeout which specifies how long it takes for a character in
 * 									a password dialog to transform from that character into a star (*).
 * 									See UI_PROP_PASSWORD_SHOW_CHAR in the vfigui documentation. The value of this
 * 									GUI parameter will be reset to its previous value once the function returns
 * 
 * @return							will return CCP_LIB_FAIL on errors, otherwise CCP_LIB_OK
 */
DllSpec enum CCPErrorCodes ccp_show(enum CCPMenuName which, const char *infodb, bool useInternalKeyTimeout = true);

/**
 * While CCP is an expert menu, ccp_wizard should make it easier to setup network profiles.
 * Main menu of wizard is similar to main menu of CCP, but number of steps to configure one network
 * is reduced to a minimal number.
 *
 * @param infodb					path to database for storing of the settings (eg. "flash/info.db")
 * @param useInternalKeyTimeout		Use an ccp internal timeout which specifies how long it takes for a character in
 * 									a password dialog to transform from that character into a star (*).
 */
DllSpec void ccp_wizard(const char *infodb, bool useInternalKeyTimeout = true);

/**
 * Set the interfaces which should be shown in the CCP or wizard.
 * 
 * Call with desired interfaces before calling ccp_show().
 * Should NOT be used while ccp_show is running. If you would like to disable the whitelist call the function
 * again with whitelist set to NULL and count to 0
 *
 * @brief 							Set the interfaces which should be shown in the CCP.
 * @param whitelist					List of interfaces to be shown
 * @param count						Number of interfaces in list
 * 
 * @return							will return CCP_LIB_FAIL if whitelist is non-NULL but count is 0.
 * 									will return CCP_LIB_FAIL if whitelist is NULL but count is >0
 * 									will return CCP_LIB_OK on success
 */
DllSpec enum CCPErrorCodes ccp_setWhiteList(CCPInterfaces *whitelist, unsigned int count);

/**
 * Sync the data of desired interfaces in CCP with the underlying OS.
 * 
 * VOS/VOS2 only. Only the interfaces CCP_INTERFACE_ETH0, CCP_INTERFACE_WLAN0 and CCP_INTERFACE_GPRS0 are supported
 * Should NOT be used while ccp_show is running.
 * Please note interface WLAN0 possibly having several profiles. The entry WLAN0 in the name of "CCP_INTERFACE_WLAN0"
 * does not refer to the profile number saved in the CCP, but refers to the _network_-interface in the system.
 * This is the same as the other interfaces as well. When specifying WLAN0 the first profile "0" will be synced.
 * If this operation fails it does not mean that no other profiles are present! For example it could be the case that someone
 * has added three WLAN profiles, deleted the first two, which would leave one WLAN profile (WLAN2) present. This function would
 * look for WLAN0, which would fail, as the profile has been deleted. For syncing in these special cases use \cccp_sync_ext
 * 
 *
 * @brief 							Sync the data of desired interfaces in CCP with the underlying OS.
 * @param interface					interface to be synced
 * @param infodb					path to database for storing of the settings (eg. "flash/info.db")
 * 
 * @return							will return CCP_LIB_FAIL if the interface is not CCP_INTERFACE_ETH0, CCP_INTERFACE_WLAN0 or CCP_INTERFACE_GPRS0
 * 									will return CCP_LIB_FAIL if the OS does not support syncing
 * 									will return CCP_LIB_FAIL if profile to be synced does not exist
 * 									will return CCP_LIB_FAIL if the syncing operation failed
 * 									will return CCP_LIB_FAIL if param infodb is NULL
 * 									will return CCP_LIB_OK on success
 */
DllSpec enum CCPErrorCodes ccp_sync(CCPInterfaces interface, const char *infodb);

/**
 * Sync the data of desired interfaces in CCP with the underlying OS.
 * 
 * See \cccp_sync for a discussion of this function. The difference to ccp_sync is the parameter @number
 * which will be appended to the database-path to discover the profile-location
 * 
 *
 * @brief 							Set the interfaces which should be shown in the CCP.
 * @param interface					interface to be synced
 * @param infodb					path to database for storing of the settings (eg. "flash/info.db")
 * @param number					Profile number to be synced
 * 
 * @return							will return CCP_LIB_FAIL if the interface is not CCP_INTERFACE_ETH0, CCP_INTERFACE_WLAN0 or CCP_INTERFACE_GPRS0
 * 									will return CCP_LIB_FAIL if the OS does not support syncing
 * 									will return CCP_LIB_FAIL if profile to be synced does not exist
 * 									will return CCP_LIB_FAIL if the syncing operation failed
 * 									will return CCP_LIB_FAIL if param infodb is NULL
 * 									will return CCP_LIB_OK on success
 */
DllSpec enum CCPErrorCodes ccp_sync_ext(CCPInterfaces interface, const char *infodb, unsigned int number);

/**
 * set a list of APNs and corresponding parameters (user name, password, ...)
 * This list is used by ccp_wizard (mobile communication) for automatic setup of GPRS profile.
 * If mobile network (MCC+MNC) matches first 5 digits of IMSI, corresponding profile is used.
 * Otherwise parameters must be set manually.
 *
 * @param apnList list of APNs and corresponding parameters
 * @param number  number of entries in list
 */
DllSpec void ccp_setApnList(const CCPgprsParameter *apnList, unsigned int number);

#ifdef __cplusplus
} // extern C
#endif

#undef DllSpec

#endif //_LIBCCP_H_
