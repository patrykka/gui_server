/**
 * @file   libcom.h
 * @author ADKteam
 * @brief  ADK-COM API
 *
 * Public ADK-COM API
 */

#ifndef LIBCOM_H_
#define LIBCOM_H_

#if defined __GNUC__ && defined VFI_COM_SHARED_EXPORT
#define DllSpec __attribute__((visibility("default")))
#else
#define DllSpec // used for both, static libraries and program symbol import
#endif

#if defined _VOS2
#define ADKCOM_MARK_DEPRECATED(why) __attribute__((deprecated(why)))
#else
#define ADKCOM_MARK_DEPRECATED(why) __attribute__((deprecated))
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>

/*! Status of a network */
enum com_NetworkStatus {
	COM_NETWORK_STATUS_ERROR = 0, /*!< An error occured while retrieving the network status, check com_errno for reason */
	COM_NETWORK_STATUS_START = 1, /*!< Network is running */
	COM_NETWORK_STATUS_STOP  = 2  /*!< Network is not running */
};

/*! Status of the connection. These are returned by the wait function */
enum com_WaitStatus {
	COM_CONNECTION_STATUS_CONNECT = 0, /*!< connection has been established */
	COM_CONNECTION_STATUS_FAIL    = 1, /*!< connection could not be established. com_errno contains details about the connection attempt. In case you have used a connection profile with multiple fallbacks com_errno will contain only the error of the last tried connection. Use the connection callback to retrieve the errors of all fallback connections if needed. */
	COM_CONNECTION_STATUS_TIMEOUT = 2, /*!< A timeout has occured */
	COM_CONNECTION_STATUS_ERROR   = 3  /*!< A critical error has occured -- check com_errno for more details */
};

/*! Status of the network. These are returned by the network-wait function */
enum com_WaitNetStatus {
	COM_NET_STATUS_OK      = 0, /*!< network command has been successfully executed */
	COM_NET_STATUS_FAIL    = 1, /*!< network command has failed */
	COM_NET_STATUS_TIMEOUT = 2, /*!< A timeout has occured */
	COM_NET_STATUS_ERROR   = 3  /*!< A critical error has occured -- check com_errno */
};

/*! Events thrown by the connection layer */
enum com_ConnectionEvent {
	COM_EVENT_CONNECTION_ESTABLISHED       = 0,                               /*!< connection successfully established */
	COM_EVENT_CONNECTION_NEXT              = 1,                               /*!< trying to establish next connection of a profile */
	COM_EVENT_CONNECTION_FAILED            = 2,                               /*!< connection cannot be established */
	COM_EVENT_PROFILE_FAILED               = 3,                               /*!< none of the connections defined in profile can be established */
	COM_EVENT_SSL_PASSWORD_REQUIRED        = 4,                               /*!< DEPRECATED - use COM_EVENT_SSL_PKCS12_PASSWORD_REQUIRED instead --- In order to establish a SSL/TLS connection a PKCS#12 password needs to be set*/
	COM_EVENT_SSL_PKCS12_PASSWORD_REQUIRED = COM_EVENT_SSL_PASSWORD_REQUIRED, /*!< In order to establish a SSL/TLS connection a PKCS#12 password needs to be set*/
	COM_EVENT_SSL_CONFIRM_POLICY_OVERRIDE  = 5,                               /*!< In order to override the SSL/TLS policy the application has to confirm the request*/
	COM_EVENT_SSL_PKEY_PASSWORD_REQUIRED   = 6,                               /*!< In order to establish a SSL/TLS connection a password for the private client key needs to be set*/
	COM_EVENT_CONNECTION_SKIPPED           = 7,                               /*!< Connection skipped. Network interface for this connection is off and should not be enabled though start_network flag (connection profile) */
	COM_EVENT_CONNECTION_LISTENING         = 8                                /*!< Server waiting for connections. */
};

enum com_ConnectionState {
	COM_CON_STATE_ACTIVE         = 0, /*!< The remote is considered to be active */
	COM_CON_STATE_PARAM_INVALID  = 1, /*!< Handle passed by application is invalid */
	COM_CON_STATE_PARAM_NO_CON   = 2, /*!< Handle passed by application is not connection-oriented */
	COM_CON_STATE_RESET          = 3, /*!< The remote has reset the connection. The connection was not properly shut down. Pass handle to com_ConnectClose */
	COM_CON_STATE_REMOTE_CLOSING = 4, /*!< The remote is waiting for the connection to close. Pass handle to com_ConnectClose to properly shut down*/
	COM_CON_STATE_TIMEDOUT       = 5  /*!< The other end didn't acknowledge retransmitted data after some time*/
};

/*! Events thrown by the network layer */
enum com_NetworkEvent {
	COM_EVENT_NETWORK_INTERFACE_UP       = 1,  /*!< A network interface was brought up */
	COM_EVENT_NETWORK_INTERFACE_DOWN     = 2,  /*!< A network interface was brought down */
	COM_EVENT_NETWORK_LINK_UP            = 3,  /*!< A network link was established */
	COM_EVENT_NETWORK_LINK_DOWN          = 4,  /*!< A network link was lost */
	COM_EVENT_NETWORK_SIGNAL             = 5,  /*!< A network state change was detected. Format is a comma seperated list with values [percentage], [dBm] and [RSSI]. In case a BT signal is returned additionally the according BT MAC address is appended */
	COM_EVENT_NETWORK_RECONNECT          = 6,  /*!< A network reconnection is pending */
	COM_EVENT_SIM_PIN_REQUEST            = 7,  /*!< SIM card is requesting for PIN entry. This is for the SIM-Slot 1 */
	COM_EVENT_SIM_PUK_REQUEST            = 8,  /*!< SIM card is requesting for PUK entry. This is for the SIM-Slot 1 */
	COM_EVENT_SIM2_PIN_REQUEST           = 18, /*!< SIM card is requesting for PIN entry. This is for the SIM-Slot 2 */
	COM_EVENT_SIM2_PUK_REQUEST           = 19, /*!< SIM card is requesting for PUK entry. This is for the SIM-Slot 2 */
	COM_EVENT_NETWORK_PACKET_SWITCH      = 9,  /*!< A packet switch URC is received from the modem */
	COM_EVENT_INTERNAL_PING              = 10, /*!< Internal ping between the comdaemon and the application. Will not be visible to the application*/
	COM_EVENT_BT_CONFIRM_PAIR_REQUEST    = 11, /*!< Deprecated */
	COM_EVENT_BT_PAIR_DONE               = 12, /*!< Deprecated */
	COM_EVENT_BT_DISCOV_TO_ELAPSED       = 13, /*!< Deprecated */
	COM_EVENT_NETWORK_FAILED             = 14, /*!< Network operation could not be performed. This event is only available to the process which has started the operation. 
	  												This should only be used by the VHQ. Other applications should ignore this event*/
	COM_EVENT_BT_SPP_SERVER_CONNECTED    = 15, /*!< Deprecated */
	COM_EVENT_BT_SPP_SERVER_DISCONNECTED = 16, /*!< Deprecated */
	COM_EVENT_GEOFENCE_VIOLATION         = 17, /*!< A geofence boundary has been violated. The name is specified as a null-terminated string in the event-data */

	COM_EVENT_BT_EXT_PIN       = 20, /*!< PIN entry on the remote has been requested. A PIN, which is displayed by the remote, needs to be entered on this device. Friendly name and address will be set in data1 in JSON. See full documentation.*/
	COM_EVENT_BT_EXT_CONFIRM   = 21, /*!< Numeric comparison has been requested. PIN, friendly name and address will be set in data1 in JSON. The PIN must be displayed on this device. See full documentation.*/
	COM_EVENT_BT_EXT_VISUALIZE = 22, /*!< Visualizing has been requested. PIN, friendly name and address will be set in data1 in JSON. The PIN must be displayed on this device and entered on the remote device. See full documentation.*/
	COM_EVENT_BT_EXT_TIMEOUT   = 23, /*!< The pairing with the remote device has timed out. Address and friendly name will be set in data1 in JSON.*/
	COM_EVENT_BT_EXT_FAILED    = 24, /*!< The pairing with the remote device has failed due a negotiation error. Address and friendly will be set in data1 in JSON.*/
	COM_EVENT_BT_EXT_SUCCESS   = 25, /*!< The pairing with the remote device has succeded. Address and friendly name will be set in data1 in JSON.*/

	COM_EVENT_BLE_EXT_PIN       = 33, /*!< (BLE Pairing) PIN entry on the remote has been requested. A PIN, which is displayed by the remote, needs to be entered on this device. Friendly name and address will be set in data1 in JSON. See full documentation.*/
	COM_EVENT_BLE_EXT_CONFIRM   = 34, /*!< (BLE Pairing) Numeric comparison has been requested. PIN, friendly name and address will be set in data1 in JSON. The PIN must be displayed on this device. See full documentation.*/
	COM_EVENT_BLE_EXT_VISUALIZE = 35, /*!< (BLE Pairing) Visualizing has been requested. PIN, friendly name and address will be set in data1 in JSON. The PIN must be displayed on this device. See full documentation.*/
	COM_EVENT_BLE_EXT_TIMEOUT   = 36, /*!< (BLE Pairing) The pairing with the remote device has timed out. Address and friendly name will be set in data1 in JSON.*/
	COM_EVENT_BLE_EXT_FAILED    = 37, /*!< (BLE Pairing) The pairing with the remote device has failed due a negotiation error. Address and friendly will be set in data1 in JSON.*/
	COM_EVENT_BLE_EXT_SUCCESS   = 38, /*!< (BLE Pairing) The pairing with the remote device has succeded. Address and friendly name will be set in data1 in JSON.*/

	COM_EVENT_NETWORK_ATTACH     = 26, /*!< Network operation (Attach) was perfomed. This event is only available to the process which has started the operation.
													This does not necessitate that the network has changed, only that the operation was accepted.
													This should only be used by the VHQ. Other applications should ignore this event.*/
	COM_EVENT_NETWORK_DETACH     = 27, /*!< Network operation (Detach) was perfomed. This event is only available to the process which has started the operation.
	  												This does not necessitate that the network has changed, only that the operation was accepted.
	  												This should only be used by the VHQ. Other applications should ignore this event.*/
	COM_EVENT_DEVICE_PWR_OFF     = 28, /*!< A hardware device / module was powered OFF*/
	COM_EVENT_DEVICE_PWR_ON      = 29, /*!< A hardware device / module was powered ON*/
	COM_EVENT_NETWORK_SIGNAL_EXT = 30, /*!< A network signal state change was detected. Format in JSON (VOS2 only) */
	COM_EVENT_REG_STATUS_CHANGE  = 31, /*!< Mobile network registration status change. Format in JSON (VOS2 only) */
	COM_EVENT_SIM_SLOT_SWITCH    = 32  /*!< The active SIM card slot was changed. New SIM slot is provided in data of the event callback. Refer to com_SimSlot for valid values.*/
};

enum com_WirelessScanEvent {
	COM_EVENT_WIRELESS_SCAN_SUCCESS = 1, /*!< The asynchronous wireless scan has succeded. Results are available JSON encoded via data */
	COM_EVENT_WIRELESS_SCAN_FAILURE = 2  /*!< The asynchronous wireless scan has failed. Error cause is available via com_errno */
};

enum com_DataFormat {
	COM_DATA_FORMAT_FLAT = 0 /*!<  Flat data representation in JSON encoding (see full documentation for details) */
};

#ifndef ADKCOM_3_0
#define com_DeviceProperityInt    com_DevicePropertyInt
#define com_DeviceProperityString com_DevicePropertyString
#define COM_PROP_SUPP_INTERFACES  COM_PROP_SUPPORTED_FEATURES_1
#define com_InterfaceMask         com_FeatureMask1
#define COM_LAN_1                 COM_FEATURE1_LAN_1
#define COM_LAN_2                 COM_FEATURE1_LAN_2
#define COM_GPRS                  COM_FEATURE1_GPRS
#define COM_UMTS                  COM_FEATURE1_UMTS
#define COM_ISDN                  COM_FEATURE1_ISDN
#define COM_MODEM                 COM_FEATURE1_MODEM
#define COM_SERIAL_1              COM_FEATURE1_SERIAL_1
#define COM_SERIAL_2              COM_FEATURE1_SERIAL_2
#define COM_SERIAL_3              COM_FEATURE1_SERIAL_3
#define COM_SERIAL_4              COM_FEATURE1_SERIAL_4
#define COM_SERIAL_5              COM_FEATURE1_SERIAL_5
#define COM_SERIAL_6              COM_FEATURE1_SERIAL_6
#define COM_USB                   COM_FEATURE1_USB
#define COM_BLUETOOTH             COM_FEATURE1_BLUETOOTH
#define COM_WIFI                  COM_FEATURE1_WIFI
#define COM_MDB                   COM_FEATURE1_MDB
#define COM_RS485                 COM_FEATURE1_RS485
#define COM_SERIAL_USBD           COM_FEATURE1_SERIAL_USBD
#define COM_LAN_USBD              COM_FEATURE1_LAN_USBD
#define COM_SERIAL_COM1A          COM_FEATURE1_SERIAL_COM1A
#define COM_SERIAL_COM1B          COM_FEATURE1_SERIAL_COM1B
#define COM_SERIAL_COM1C          COM_FEATURE1_SERIAL_COM1C
#define COM_SERIAL_COM1D          COM_FEATURE1_SERIAL_COM1D
#define COM_SERIAL_COM1E          COM_FEATURE1_SERIAL_COM1E
#define COM_LAN_USBH              COM_FEATURE1_LAN_USBH
#define COM_BC_READER             COM_FEATURE1_BC_READER
#define COM_USBSER                COM_FEATURE1_USBSER
#define COM_LAN_USBD1             COM_FEATURE1_LAN_USBD1
#define COM_BLE                   COM_FEATURE1_BLE
#define COM_4G_LTE                COM_FEATURE1_4G_LTE
#define COM_DUAL_SIM              COM_FEATURE1_DUAL_SIM
#endif

/*! Bit mask of available/supported comm interfaces. This can be queried by asking for the integer property COM_PROP_SUPPORTED_FEATURES_1*/
enum com_FeatureMask1 {
	COM_FEATURE1_LAN_1        = 1 << 0,  /*!< LAN 1 / ETH0: the main Ethernet interface */
	COM_FEATURE1_LAN_2        = 1 << 1,  /*!< LAN 2 / ETH1: optional second Ethernet interface */
	COM_FEATURE1_GPRS         = 1 << 2,  /*!< GSM / GPRS */
	COM_FEATURE1_UMTS         = 1 << 3,  /*!< 3G: UMTS */
	COM_FEATURE1_ISDN         = 1 << 4,  /*!< ISDN modem*/
	COM_FEATURE1_MODEM        = 1 << 5,  /*!< Dial-up modem */
	COM_FEATURE1_SERIAL_1     = 1 << 6,  /*!< first serial port, COM1 */
	COM_FEATURE1_SERIAL_2     = 1 << 7,  /*!< second serial port, COM2 */
	COM_FEATURE1_SERIAL_3     = 1 << 8,  /*!< third serial port, COM3 */
	COM_FEATURE1_SERIAL_4     = 1 << 9,  /*!< fourth serial port, COM4 */
	COM_FEATURE1_SERIAL_5     = 1 << 10, /*!< fifth serial port, COM5 */
	COM_FEATURE1_SERIAL_6     = 1 << 11, /*!< sixth serial port, COM6*/
	COM_FEATURE1_USB          = 1 << 12, /*!< USB Host*/
	COM_FEATURE1_BLUETOOTH    = 1 << 13, /*!< Bluetooth, which includes support for Serial (SPP), Dial-in (DUN) and Ethernet (PAN) */
	COM_FEATURE1_WIFI         = 1 << 14, /*!< Wireless LAN*/
	COM_FEATURE1_MDB          = 1 << 15, /*!< Multi-Drop Bus (MDB) */
	COM_FEATURE1_RS485        = 1 << 16, /*!< RS485 */
	COM_FEATURE1_SERIAL_USBD  = 1 << 17, /*!< Serial over USB (serial gadget) */
	COM_FEATURE1_LAN_USBD     = 1 << 18, /*!< Ethernet over USB (Ethernet gadget, USB0)*/
	COM_FEATURE1_SERIAL_COM1A = 1 << 19, /*!< Virtual COM1A*/
	COM_FEATURE1_SERIAL_COM1B = 1 << 20, /*!< Virtual COM1B*/
	COM_FEATURE1_SERIAL_COM1C = 1 << 21, /*!< Virtual COM1C*/
	COM_FEATURE1_SERIAL_COM1D = 1 << 22, /*!< Virtual COM1D*/
	COM_FEATURE1_SERIAL_COM1E = 1 << 23, /*!< Virtual COM1E*/
	COM_FEATURE1_LAN_USBH     = 1 << 24, /*!< Ethernet over USB Host (Ethernet gadget, USBH1)*/
	COM_FEATURE1_BC_READER    = 1 << 25, /*!< Built-in barcode reader (this bit is never set)*/
	COM_FEATURE1_USBSER       = 1 << 26, /*!< USB Serial (Deprecated -- Do not use)*/
	COM_FEATURE1_LAN_USBD1    = 1 << 27, /*!< 2nd Ethernet over USB channel (Ethernet gadget, USB1)*/
	COM_FEATURE1_BLE          = 1 << 28, /*!< Bluetooth low energy, which includes support IBeacon */
	COM_FEATURE1_4G_LTE       = 1 << 29, /*!< 4G LTE */
	COM_FEATURE1_DUAL_SIM     = 1 << 30  /*!< Radio module supports two SIM-Slots */
};

/*! Bit mask of available/supported comm interfaces. This can be queried by asking for the integer property COM_PROP_SUPPORTED_FEATURES_2*/
enum com_FeatureMask2 {
	COM_FEATURE2_PPP_SERIAL               = 1 << 0,  /*!< Supports PPP over Serial network profiles */
	COM_FEATURE2_USBGADGET_SERIAL         = 1 << 1,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_USBGADGET_ETH_RNDIS      = 1 << 2,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_USBGADGET_SERIAL_RNDIS   = 1 << 3,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_USBGADGET_SERIAL_ECM     = 1 << 4,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_USBGADGET_ETH_ECM        = 1 << 5,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_USBGADGET_ETH_ECMx2      = 1 << 6,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_USBGADGET_SERIAL_ECMx2   = 1 << 7,  /*!< Refer to enum com_USBGadgetMode */
	COM_FEATURE2_BRIDGING                 = 1 << 8,  /*!< Supports bridging interfaces */
	COM_FEATURE2_CSD                      = 1 << 9,  /*!< Supports CSD (Raw radio) interfaces */
	COM_FEATURE2_IBEACON                  = 1 << 10, /*!< Supports iBeacons */
	COM_FEATURE2_EDDYSTONE                = 1 << 11, /*!< Supports Eddystone beacons */
	COM_FEATURE2_GPRS_SIGNAL_EVENTS       = 1 << 12, /*!< Supports 2G/3G/4G signal quality events */
	COM_FEATURE2_BT_PAIR_EXT              = 1 << 13, /*!< Supports extended bluetooth pairing */
	COM_FEATURE2_BT_GATT                  = 1 << 14, /*!< Supports GATT functionality */
	COM_FEATURE2_WIFI_SIGNAL_EVENTS       = 1 << 15, /*!< Supports WiFi signal quality events */
	COM_FEATURE2_BT_VIRTUAL_CHANNELS      = 1 << 16, /*!< Supports Bluetooth Virtual Channels. Refer to com_BTEnableVirtualChannels */
	COM_FEATURE2_BT_GATT_SERIAL           = 1 << 17, /*!< Supports GATT Serial emulation */
	COM_FEATURE2_DEVICE_NO_AUTOPOWER      = 1 << 18, /*!< Set when ADKCOM is configured to NOT automatically power on any comm interfaces. See documentation for further details*/
	COM_FEATURE2_BASE_COMMUNICATIONS      = 1 << 19, /*!< Supports a base (Does not mean it is currently docked) */
	COM_FEATURE2_BT_GATT_BEACON_CTL       = 1 << 20, /*!< Supports separate turning off/on of GATT Beaconing*/
	COM_FEATURE2_USB_PORT_1_CONFIG        = 1 << 21, /*!< Supports configuration of USB Port 1 (Never set under VOS1)*/
	COM_FEATURE2_USB_PORT_2_CONFIG        = 1 << 22, /*!< Supports configuration of USB Port 2 (Never set under VOS1)*/
	COM_FEATURE2_USB_PORT_1_GADGET_CONFIG = 1 << 23, /*!< Supports configuration of USB Port 1 gadget settings (Never set under VOS1)*/
	COM_FEATURE2_USB_PORT_2_GADGET_CONFIG = 1 << 24, /*!< Supports configuration of USB Port 2 gagdet settings (Never set under VOS1)*/
	COM_FEATURE2_SERIAL_USBD2             = 1 << 25, /*!< Serial over USB 2 (serial gadget) */
	COM_FEATURE2_LAN_USBD2                = 1 << 26, /*!< 3rd Ethernet over USB channel (Ethernet gadget, USB2)*/
	COM_FEATURE2_LAN_USBD3                = 1 << 27, /*!< 4th Ethernet over USB channel (Ethernet gadget, USB3)*/
	COM_FEATURE2_RADIO_POWER_PROFILE      = 1 << 28, /*!< Supports Radio Power Profile*/
	COM_FEATURE2_DUAL_BAND_WIFI           = 1 << 29, /*!< Supports dual-band WiFi */
	COM_FEATURE2_IPV6                     = 1 << 30  /*!< Supports IPV6 interfaces */
};

/*! Bit mask of available/supported comm interfaces. This can be queried by asking for the integer property COM_PROP_SUPPORTED_FEATURES_3*/
enum com_FeatureMask3 {
	COM_FEATURE3_AUTO_RECONNECT = 1 << 0,  /*!< Automatically reconnects supported networks */
	COM_FEATURE3_LTE_CAT_M1     = 1 << 30, /*!< Supports LTE CAT-M1 Radio Access Technology */
};

/*! Integer Device Properties, read/write permissions in brackets */
enum com_DevicePropertyInt {
	COM_PROP_SUPPORTED_FEATURES_1   = 0,  /*!< Bit list of available features on this terminal type. Check enum com_FeatureMask1 [R/O] */
	COM_PROP_SUPPORTED_FEATURES_2   = 46, /*!< Bit list of available features on this terminal type. Check enum com_FeatureMask2 [R/O] */
	COM_PROP_SUPPORTED_FEATURES_3   = 53, /*!< Bit list of available features on this terminal type. Check enum com_FeatureMask3 [R/O] */
	COM_PROP_GSM_SIGNAL_QUALITY     = 1,  /*!< DEPRECATED. Mobile network signal quality, 0 not attached, 1 bad signal ... 255 good signal [R/O] */
	COM_PROP_GSM_SIGNAL_ERROR_RATE  = 2,  /*!< DEPRECATED. Signal error rate: bit error rate [R/O] */
	COM_PROP_GSM_SIM_PRESENT        = 3,  /*!< SIM card present in the device [R/O] */
	COM_PROP_GSM_PIN_COUNT          = 4,  /*!< Number of remaining tries to enter the PIN referred to by pin_status, 0 is returned if no SIM present [R/O] */
	COM_PROP_GSM_SIGNAL_RSSI        = 5,  /*!< Mobile network signal in RSSI value [R/O]*/
	COM_PROP_GSM_SIGNAL_PERCENTAGE  = 6,  /*!< Mobile network signal in RSSI percentage value [R/O]*/
	COM_PROP_GSM_SIGNAL_DBM         = 7,  /*!< Mobile network signal in dbm value [R/O]*/
	COM_PROP_GSM_SIGNAL_STRENGTH    = 49, /*!< VOS2 only: Mobile network signal Strength. Range 0..5. Must be used to draw signal strength indicator bars [R/O]*/
	COM_PROP_GSM_ROAM_STATUS        = 8,  /*!< Roaming indicator 0-registered to home or not registered, 1-registered to other network [R/O]*/
	COM_PROP_GSM_FREQ               = 9,  /*!< Radio Frequency. See enum com_RadioFreq. Please note this works only for 2G and 3G networks. 4G networks will return value 0 (COM_GSM_FREQ_UNKNOWN)[R/O] */
	COM_PROP_GSM_RADIO_ACCESS_TECH  = 10, /*!< Radio Access Technology. For valid values see enum com_RadioACT [R/O] (VOS2 [R/W]) */
	COM_PROP_RADIO_ACCESS_TECH      = 10, /*!< Radio Access Technology. For valid values see enum com_RadioACT [R/O] (VOS2 [R/W]) */
	COM_PROP_GPRS_PACKET_SWITCH     = 11, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_POWER_MANAGEMENT_MODE  = 12, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_VERBOSITY              = 13, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_WLAN_SIGNAL_PERCENTAGE = 14, /*!< WLAN network signal in RSSI percentage value [R/O]*/
	COM_PROP_WLAN_SIGNAL_DBM        = 15, /*!< WLAN network signal in dbm value [R/O]*/
	COM_PROP_WLAN_FREQ              = 16, /*!< Channel frequency [R/O] */
	COM_PROP_ETH_0_LINK_STATUS      = 17, /*!< Cable link status for ETH0 ethernet interface, 1 is returned for link up, 0 is returned for link down [R/O] */
	COM_PROP_ETH_1_LINK_STATUS      = 18, /*!< Cable link status for ETH1 ethernet interface, 1 is returned for link up, 0 is returned for link down [R/O] */
	COM_PROP_ETH_BT_LINK_STATUS     = 19, /*!< Cable link status for BLUETOOTH ETH ethernet interface, 1 is returned for link up, 0 is returned for link down [R/O] */
	COM_PROP_BT_DISCOVERABLE        = 20, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_BT_DISCOVERY_TIMEOUT   = 21, /*!< Deprecated -- Do not use [N/A] */
	/* Gap is intended: #22 occupied */
	COM_PROP_BT_SSP_CONFIRMATION             = 23, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_BT_CONFIRM_PAIR                 = 24, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_BT_IBEACON_STATUS               = 25, /*!< Check iBeacon status. 0 = not advertising, 1 = currently advertising, -1 = error [R/O]*/
	COM_PROP_BT_IBEACON_OFF                  = 26, /*!< Stop advertising via iBeacon. The integer value given is ignored [W/O]*/
	COM_PROP_USB_GADGET_MODE                 = 27, /*!< Configures the USB Client gagdet mode of the terminal (USB host + client). See enum com_USBGadgetMode for all available values. [R/W]\n
	                                                    The mode cannot be set on-the-fly. A Terminal reboot is required to apply the new settings!! \n
	                                                    When this property is read, it returns the currently configured gadget mode which is not necessarily the mode which is currently active. \n
	                                                    You can read the currently active gadget mode using property COM_PROP_USB_GADGET_MODE_ACTIVE */
	COM_PROP_ETH_USB_GADGET_LINK_STATUS      = 28, /*!< Cable link status for USB Gadget Ethernet interface, 1 is returned for link up, 0 is returned for link down [R/O] */
	COM_PROP_WLAN_SIGNAL_THRESHOLD           = 29, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_GSM_SIGNAL_THRESHOLD            = 30, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_BT_GET_SPP0_CONFIG              = 31, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_BT_GET_SPP2_CONFIG              = 32, /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_GSM_GET_SIM_STATUS              = 33, /*!< SIM status property for mobile devices. Possible values: see enum com_SIMStatus [R/O]*/
	COM_PROP_ETH_USB_HOST_GADGET_LINK_STATUS = 34, /*!< VOS2 only: Cable link status for USB Host Gadget Ethernet interface, 1 is returned for link up, 0 is returned for link down [R/O] */
	COM_PROP_USB1_MODE                       = 35, /*!< VOS2 only: USB mode of USB1. Can be one of com_USB_Mode. When set a terminal reboot is required to apply the new settings [R/W]*/
	COM_PROP_USB2_MODE                       = 36, /*!< VOS2 only: USB mode of USB2. Can be one of com_USB_Mode. When set a terminal reboot is required to apply the new settings [R/W]*/
	COM_PROP_USB_GADGET_MODE_ACTIVE          = 37, /*!< Read the currently active USB Client gagdet mode of the terminal. [R/O] \n On Trident V/OS this returns the same value as for COM_PROP_USB_GADGET_MODE due to limited OS capabilities. */
	COM_PROP_ETH_USB1_GADGET_LINK_STATUS     = 38, /*!< VOS2 only: Cable link status for USB Gadget Ethernet interface, channel 2 (usb1), 1 is returned for link up, 0 is returned for link down [R/O] */
	COM_PROP_SELECT_SIM_SLOT                 = 39, /*!< Select which SIM slot should be used or read currently selected SIM-Slot. Please refer to enum com_SimSlot for the appropriate values. Writing this property is only supported on VOS2 devices. [R/W] */
	COM_PROP_RAW_MODEM_CURRENT_MODULATION    = 40, /*!< Retrieve the current modulation type of a connected raw modem. Must be cast to com_Modulation. If modem is not connected, -1 is returned and com_errno is set to COM_ERR_NET_NOEXIST. [R/O] */
	COM_PROP_RAW_MODEM_CURRENT_SPEED_RX      = 41, /*!< Retrieve the current RX speed of a connected raw modem [unit: Baud]. If the raw modem is not connected, -1 is returned and com_errno is set to COM_ERR_NET_NOEXIST. [R/O] */
	COM_PROP_RAW_MODEM_CURRENT_SPEED_TX      = 42, /*!< Retrieve the current TX speed of a connected raw modem [unit: Baud]. If the raw modem is not connected, -1 is returned and com_errno is set to COM_ERR_NET_NOEXIST. [R/O] */
	COM_PROP_PPP_MODEM_CURRENT_MODULATION    = 43, /*!< Retrieve the current modulation type of a connected ppp modem. Must be cast to com_Modulation. If the ppp modem is not connected, -1 is returned and com_errno is set to COM_ERR_NET_NOEXIST. [R/O] */
	COM_PROP_PPP_MODEM_CURRENT_SPEED_RX      = 44, /*!< Retrieve the current RX speed of a connected ppp modem [unit: Baud]. If the ppp modem is not connected, -1 is returned and com_errno is set to COM_ERR_NET_NOEXIST. [R/O] */
	COM_PROP_PPP_MODEM_CURRENT_SPEED_TX      = 45, /*!< Retrieve the current TX speed of a connected ppp modem [unit: Baud]. If the ppp modem is not connected, -1 is returned and com_errno is set to COM_ERR_NET_NOEXIST. [R/O] */
	/* Gap is intended: #46 already used by COM_PROP_SUPPORTED_FEATURES_2 */
	COM_PROP_INTERNAL_LOG_CLEAR = 47, /*!< Clear the internal error log messages. The set value is irrelevant [W/O]*/
	COM_PROP_MODEM_LINK_STATUS  = 48, /*!< Current status of the dial-up modem (active/inactive). Applies for all supported modem modes [R/O] */
	/* Gap is intended: #49 already used by COM_PROP_GSM_SIGNAL_STRENGTH */
	COM_PROP_ETH_0_LINK_SPEED  = 50, /*!< Cable link speed for ETH0 ethernet interface. See enum com_EthernetSpeed for supported values. Note that read values will reflect the real speed, thus COM_ETH_SPEED_AUTO is never returned [R/W] */
	COM_PROP_ETH_1_LINK_SPEED  = 51, /*!< Cable link speed for ETH1 ethernet interface, see enum com_EthernetSpeed for supported values. Note that read values will reflect the real speed, thus COM_ETH_SPEED_AUTO is never returned [R/W] */
	COM_PROP_GSM_POWER_PROFILE = 52, /*!< Write/Read Used GSM Power Profile Index (0,1,2,3...N), 0 = No Power limitation. N depends on radio model. com_errno will be set to COM_ERR_NO_PROFILE_NODE if the slot is empty or the slot does not exist. com_errno will be set to COM_ERR_INVALID_PARAM if the value to be set is negative. com_errno will be set to COM_ERR_NET_NOEXIST if this feature is not supported on the current device [R/W] */
	/* Gap is intended: #53 already used by COM_PROP_SUPPORTED_FEATURES_3 */
	COM_PROP_GSM_CONFIGURED_RAT         = 54, /*!< Read/write the configured/desired Radio Access Technology. The returned value is not necessarily the currently active RAT. For this see COM_PROP_GSM_RADIO_ACCESS_TECH. Type: enum com_RadioACT [R/W] (VOS2 only) */
	COM_PROP_WLAN_ROAM_THRESHOLD_2_4GHZ = 55, /*!< 2.4 GHz WiFi Roaming Threshold. Value range: 0 to -100 in dBm where 0 means default value. Network must be restarted to apply the setting! [R/W] (VOS2 only) */
	COM_PROP_WLAN_ROAM_THRESHOLD_5GHZ   = 56, /*!< 5 GHz WiFi Roaming Threshold. Value range: 0 to -100 in dBm where 0 means default value. Network must be restarted in order to apply! [R/W] (VOS2 only) */
};

/*! String Device Properties, read/write permissions in brackets */
enum com_DevicePropertyString {
	COM_PROP_GSM_SIM_PIN          = 100,        /*!< Set PIN for the used SIM card [W/O] */
	COM_PROP_GSM_SIM_PUK          = 101,        /*!< Set PUK for the used SIM card to recover the SIM PIN. COM_PROP_GSM_SIM_PIN_NEW must be set before this call in order to specifiy the new PIN to be set after PUK was accepted! [W/O] */
	COM_PROP_GSM_SIM_PIN_NEW      = 102,        /*!< Set new PIN, used only if you want to change the SIM PIN. COM_PROP_GSM_SIM_PIN must be set before this call! [W/O] */
	COM_PROP_GSM_PROVIDER_NAME    = 103,        /*!< Name of the network provider [R/O] */
	COM_PROP_GSM_MCC              = 104,        /*!< GSM mobile country code (visitor network)[R/O] */
	COM_PROP_GSM_MNC              = 105,        /*!< GSM mobile network code (visitor network)[R/O] */
	COM_PROP_GSM_HOME_MCC         = 106,        /*!< GSM mobile country code (home network)[R/O] */
	COM_PROP_GSM_HOME_MNC         = 107,        /*!< GSM mobile network code (home network)[R/O] */
	COM_PROP_GSM_HOME_OPERATOR    = 108,        /*!< GSM mobile operator (home network)[R/O] */
	COM_PROP_GSM_LAC              = 109,        /*!< GSM location area code [R/O] */
	COM_PROP_GSM_CELL_ID          = 110,        /*!< GSM cell ID [R/O] */
	COM_PROP_GSM_IMEI             = 111,        /*!< International Mobile Equipment Identity [R/O] */
	COM_PROP_GSM_IMSI             = 112,        /*!< International Mobile Subscriber Identity [R/O] */
	COM_PROP_GSM_SIM_ID           = 113,        /*!< SIM card identification number [R/O] */
	COM_PROP_WLAN_SSID            = 114,        /*!< WLAN Service Set Identifier of currently connected Wi-Fi network [R/O] */
	COM_PROP_WLAN_BSSID           = 115,        /*!< WLAN Basic Service Set Identifier. This basically is the hardware address of the currently connected access point in a Wi-Fi network  [R/O] */
	COM_PROP_WLAN_SITE_SURVEY     = 116,        /*!< List of available WLAN networks. Consider using the com_WirelessScan function instead [R/O] */
	COM_PROP_BT_SCAN              = 117,        /*!< List of available Bluetooth Sites*/
	COM_PROP_BT_PAIRING_STATUS    = 118,        /*!< JSON-coded list of all currently paired devices. If not paired with any peer then following JSON string is returned: {"BT_COUNT":0}  [R/O]*/
	COM_PROP_BT_CONNECTION_STATUS = 119,        /*!< JSON-coded list of all currently connected devices. If not connected to any peer then following JSON string is returned: {"BT_COUNT":0}  [R/O]*/
	COM_PROP_BT_IBEACON_ON        = 120,        /*!< Advertise with the specified UUID. [W/O]*/
	                                            /*!< The format is UUID:MAJOR:MINOR. MAJOR/MINOR is intepreted as an integer. You may omit hyphens*/
	                                            /*!< Example: 123e4567-e89b-12d3-a456-426655440000:2000:80 will broadcast*/
	                                            /*!< the UUID 123e4567-e89b-12d3-a456-426655440000 with major 2000 and minor 80 [W/O]*/
	COM_PROP_GSM_SCAN             = 121,        /*!< List of available GSM/GPRS operators [R/O]*/
	COM_PROP_BT_LOCAL_NAME        = 122,        /*!< Deprecated -- Do not use [N/A] */
	COM_PROP_BT_SERVICE_DISCOVERY = 123,        /*!< List of available Services per scanned BT device [R/O]*/
	COM_PROP_INTERNAL_LOG         = 124,        /*!< Get a list of all recently occurred errors in ADKCOM service [R/O]*/
	COM_PROP_3GPP_ERRORS          = 125,        /*!< Get a JSON string with recently occurred 3GPP errors. Refer to manual for member descriptions VOS2 only! [R/O]*/
	COM_PROP_BT_PAIRING_LIST      = 126,        /*!< JSON-coded list of all currently paired devices. Please note that this does not return the profiles entries as with COM_PROP_BT_CONNECTION_STATUS.*/
	                                            /*!< If not paired with any peer then following JSON string is returned: {"BT_COUNT":0} [R/O]*/
	COM_PROP_BT_SERVICE_EXT_SPP          = 127, /*!< List of available bluetooth devices and their SPP channels. If none are available returns {"BT_COUNT":0} [R/O]*/
	COM_PROP_WLAN_COUNTRY_REG_DOM        = 128, /*!< Set/Get WLAN country code and apply regulatory settings. Country code (CC) in ISO-3166-1 alpha-2 format + optional revision number in the format CC or CC/REV, for ex. "JP" or "JP/5". When read, the returned string always contains the revision [R/W] VOS2 only */
	COM_PROP_BT_PAIRED_SERVICE_EXT_SPP   = 129, /*!< List of available paired bluetooth devices and their SPP channels. If none are available returns {"BT_COUNT":0} [R/O] Consider using the com_WirelessScan function instead */
	COM_PROP_BT_PAIRED_SERVICE_DISCOVERY = 130, /*!< List of available Services per scanned paired BT device. Consider using the com_WirelessScan function instead [R/O]*/
	COM_PROP_GSM_PREFERRED_OPERATOR_LIST = 131, /*!< List of preferred mobile operators [R/W] VOS2 only*/
	COM_PROP_BT_OWN_ADDR                 = 132, /*!< Own BT address as string (eg. 11:22:33:44:55:66) [R/O] */
};

/*! Type of connection to a host */
enum com_ConnectionType {
	COM_CONNECTION_TYPE_NONE       = 0, /*!< No connection (error)*/
	COM_CONNECTION_TYPE_TCP        = 1, /*!< TCP/IP connection */
	COM_CONNECTION_TYPE_UDP        = 2, /*!< UDP/IP connection */
	COM_CONNECTION_TYPE_SSL        = 3, /*!< SSL over TCP, single or mutual authentication */
	COM_CONNECTION_TYPE_RAW_MODEM  = 4, /*!< Raw dial-up modem connection */
	COM_CONNECTION_TYPE_RAW_GSM    = 5, /*!< Deprecated, renamed to COM_CONNECTION_TYPE_RAW_CSD */
	COM_CONNECTION_TYPE_RAW_CSD    = 5, /*!< Raw circuit switched connection */
	COM_CONNECTION_TYPE_RAW_ISDN   = 6, /*!< Raw ISDN connection */
	COM_CONNECTION_TYPE_RAW_SERIAL = 7, /*!< Raw SERIAL (UART/USB emulated) connection */
	COM_CONNECTION_TYPE_WS         = 8, /*!< Plain websocket */
	COM_CONNECTION_TYPE_WSS        = 9  /*!< TLS secured websocket */
};

/*! Type of network */
enum com_NetworkType {
	COM_NETWORK_TYPE_NONE       = 0, /*!< No Network (for example direct modem connection)*/
	COM_NETWORK_TYPE_LAN        = 1, /*!< Ethernet/LAN network */
	COM_NETWORK_TYPE_PPP_MODEM  = 2, /*!< PPP network over dial-up modem */
	COM_NETWORK_TYPE_PPP_GPRS   = 3, /*!< GPRS data connection */
	COM_NETWORK_TYPE_WLAN       = 4, /*!< WLAN network */
	COM_NETWORK_TYPE_PPP_SERIAL = 5, /*!< PPP network over serial */
	COM_NETWORK_TYPE_PPP_CSD    = 6, /*!< PPP network over GSM CSD connection*/
	COM_NETWORK_TYPE_LOOP_LOCAL = 7  /*!< Local loopback interface*/
};

/*! ADK-COM Error codes */
enum com_ErrorCodes {
	COM_ERR_NONE = 0, /*!< No error*/

	COM_ERR_INVALID_HANDLE         = 1,   /*!< Handle is invalid (NULL)*/
	COM_ERR_INVALID_PARAM          = 2,   /*!< Parameters to the function (aside form the handle) are invalid*/
	COM_ERR_ALREADY_INIT           = 3,   /*!< Init function has already been called*/
	COM_ERR_NO_INIT                = 4,   /*!< Init function has NOT already been called*/
	COM_ERR_ALREADY_EST            = 5,   /*!< Connection has already been established*/
	COM_ERR_DAEMON_COM             = 6,   /*!< Communication error with the comdaemon, most likely not active*/
	COM_ERR_DAEMON_INVALID         = 7,   /*!< Comdaemon has sent invalid response*/
	COM_ERR_DAEMON_INVALID_REQUEST = 8,   /*!< Comdaemon has not understood our request*/
	COM_ERR_DAEMON_INTERNAL        = 9,   /*!< Generic internal error of the Daemon */
	COM_ERR_CON_NOFILE             = 10,  /*!< Connection XML or database could not be parsed*/
	COM_ERR_CON_NOINFOSVC          = 11,  /*!< The infoservice is not supported in this version*/
	COM_ERR_CON_CORRUPT            = 12,  /*!< Connection XML/DB file corrupt or otherwise unusable*/
	COM_ERR_CON_INVALID            = 13,  /*!< Connection Section in DB/XML does not have enough or invalid parameters*/
	COM_ERR_CON_SOCKET             = 14,  /*!< The socket connection could not be set up*/
	COM_ERR_CON_USED               = 15,  /*!< The raw data connection is currently in use and is not shared*/
	COM_ERR_CON_NOTREADY           = 16,  /*!< Connection has not yet been established*/
	COM_ERR_CON_AGAIN              = 17,  /*!< Send/Recv would block, but the socket is in non-blocking mode*/
	COM_ERR_CON_SEVERED            = 18,  /*!< Connection has been terminated and must be reestablished - DEPRECATED*/
	COM_ERR_CON_CANCEL             = 19,  /*!< Application has canceled the connection*/
	COM_ERR_CON_INVALIDHOST        = 20,  /*!< DNS name resolution failed*/
	COM_ERR_CON_TIMEOUT            = 21,  /*!< Connection Timeout expired*/
	COM_ERR_CON_SSL                = 22,  /*!< SSL connection could not be set up*/
	COM_ERR_CON_SSL_VERIFY         = 23,  /*!< The verification of the certificate failed or the certificate does not match the hostname*/
	COM_ERR_CON_SSL_CERT           = 24,  /*!< Invalid private key / certificate or wrong PKCS#12 password*/
	COM_ERR_CON_SSL_KEY            = 25,  /*!< The private key does not contain valid data*/
	COM_ERR_CON_SSL_INTERUPT       = 26,  /*!< The connection was interrupted during SSL handshake*/
	COM_ERR_CON_SSL_CIPHERSET      = 27,  /*!< OpenSSL rejected the use of the cipher list*/
	COM_ERR_CON_SSL_BUSY           = 28,  /*!< OpenSSL currently can't read or write data due to OpenSSL internal processing, try again later*/
	COM_ERR_CON_SSL_PROTOCOL       = 29,  /*!< OpenSSL protocol error occured*/
	COM_ERR_CON_SSL_PASSWORD       = 30,  /*!< PKCS#12 secret was not correctly entered */
	COM_ERR_NET_NOFILE             = 31,  /*!< Network XML or database could not be parsed (probably won’t apply to db as the same file)*/
	COM_ERR_NET_CORRUPT            = 32,  /*!< Network XML/DB file corrupt or otherwise unusable*/
	COM_ERR_NET_INVALID            = 33,  /*!< Network Section in DB/XML does not have enough or invalid parameters */
	COM_ERR_NET_NOATTACH           = 34,  /*!< Process not attached to specified network*/
	COM_ERR_NET_NOEXIST            = 35,  /*!< Specified network interface does not exist*/
	COM_ERR_NET_DENY               = 36,  /*!< Specified network interface could not be set up/reset*/
	COM_ERR_NET_NOSUPPORT          = 37,  /*!< A request/feature/setting is not supported*/
	COM_ERR_NET_TIMEOUT            = 38,  /*!< The specified timeout of the network profile has expired */
	COM_ERR_NET_SIM_VERIFY         = 39,  /*!< The SIM Card verification is pending. Set the PIN using the Property functions first. */
	COM_ERR_NET_ALREADY_UP         = 40,  /*!< Network is already up */
	COM_ERR_NET_DOWN               = 41,  /*!< Sudden network disconnection */
	COM_ERR_NET_PPP_AUTH_FAIL      = 42,  /*!< PPP Authentication failed */
	COM_ERR_NET_PPP_FAIL           = 43,  /*!< General PPP failure */
	COM_ERR_PROP_NOEXIST           = 44,  /*!< Property does not exist*/
	COM_ERR_PROP_DENY              = 45,  /*!< Property exists but could not be set/retrieved*/
	COM_ERR_PROP_RDONLY            = 46,  /*!< Property exists but is read-only*/
	COM_ERR_PROP_WRONLY            = 47,  /*!< Property exists but is write-only*/
	COM_ERR_PROP_BUF               = 48,  /*!< String-Property read successfully, but application buffer is too small*/
	COM_ERR_DEV_INTERNAL           = 49,  /*!< Internal device error */
	COM_ERR_DEV_NOSIM              = 50,  /*!< No SIM card found */
	COM_ERR_DEV_GSM_PWR            = 51,  /*!< Error powering GSM/GPRS hardware */
	COM_ERR_DEV_PIN_PUK_VERIFY     = 52,  /*!< SIM PIN or PUK verification failed */
	COM_ERR_DEV_IN_USE             = 53,  /*!< Device is still busy. Case where device get/set property is called while device is initializing*/
	COM_ERR_DEV_BATTERY            = 54,  /*!< General battery failure including low battery*/
	COM_ERR_DEV_DETACHED           = 55,  /*!< Device detached ( e.g. not connected to base)*/
	COM_ERR_DEV_NO_CARRIER         = 56,  /*!< No carrier*/
	COM_ERR_DEV_NO_DIAL_TONE       = 57,  /*!< No dial tone*/
	COM_ERR_DEV_NO_ANSWER          = 58,  /*!< No answer*/
	COM_ERR_DEV_SDLC_FAIL          = 59,  /*!< SDLC Init/switch failure*/
	COM_ERR_DEV_LINE_IN_USE        = 60,  /*!< The line is in use*/
	COM_ERR_DEV_BUSY               = 61,  /*!< Remote system is busy*/
	COM_ERR_NO_RESOURCES           = 62,  /*!< Plain resources such as memory could not be allocated*/
	COM_ERR_DEV_NO_CABLE           = 63,  /*!< Connection cable is unplugged */
	COM_ERR_NO_PROFILE_NODE        = 64,  /*!< No such profile node present*/
	COM_ERR_DEV_OPEN               = 65,  /*!< Error opening device */
	COM_ERR_DEV_POWER              = 66,  /*!< The device associated with the request is not powered*/
	COM_ERR_OP_AMBIGUOUS           = 67,  /*!< The requested operation is ambiguous*/
	COM_ERR_NET_BT_PAIR_PENDING    = 68,  /*!< BT Confirm Pair pending. Set BT_CONFIRM_PAIR using the Property functions first. */
	COM_ERR_NET_BT_PAIR_FAILED     = 69,  /*!< The terminal failed to pair with BT device */
	COM_ERR_NET_BT_PAN_NOCONN      = 70,  /*!< The terminal's PAN profile is not connected to any VFI bluetooth basestation */
	COM_ERR_NET_BT_OUTOFRANGE      = 71,  /*!< The requested BT device is out of range (not available) */
	COM_ERR_DEV_PIN_REQUIRED       = 72,  /*!< PUK was sent without setting a COM_PROP_SIM_PIN_NEW */
	COM_ERR_DEV_NO_MODEM_PROFILE   = 73,  /*!< No modem country profile is set for this device. Use sysmode to choose one or get OS signed download file to apply the settings.*/
	COM_ERR_OS_OPERATION_FAILED    = 74,  /*!< An OS operation has failed */
	COM_ERR_NOT_DOCKED             = 75,  /*!< Operation cannot be performed because the terminal is not docked to the base station or is no mobile device */
	COM_ERR_TRY_AGAIN              = 76,  /*!< Functionality or property currently not available. Try again later */
	COM_ERR_NET_NOT_CONNECTED      = 77,  /*!< The network interface associated to that request is not connected/active */
	COM_ERR_NET_WRONG_CREDENTIALS  = 78,  /*!< The credentials for the network are incorrect. */
	COM_ERR_NET_BIND               = 79,  /*!< Host binding failed */
	COM_ERR_PING_MSG_TOO_LONG      = 80,  /*!< Ping message size too long */
	COM_SYSTEM_ERRNO_BASE          = 1000 /*!< Error numbers greater than COM_SYSTEM_ERRNO_BASE represent standard errno numbers returned by OS socket functions.\n
											   It is COM_SYSTEM_ERRNO_BASE + errno (see also standard errno.h).\n
											  */
};

/*! Available communication interfaces in the system */
enum com_Interfaces {
	COM_INTERFACE_NONE  = 0, /*!< NONE (Invalid)*/
	COM_INTERFACE_ETH0  = 1, /*!< The first (main) Ethernet adapter */
	COM_INTERFACE_ETH1  = 2, /*!< The second Ethernet adapter */
	COM_INTERFACE_GPRS0 = 3, /*!< The main GPRS modem */

	COM_INTERFACE_DIAL     = 4, /*!< The dial modem */
	COM_INTERFACE_PPP_DIAL = 5, /*!< The dial modem for PPP */

	COM_INTERFACE_CSD = 41, /*!< CSD Serial interface (VOS2 Only)*/

	COM_INTERFACE_SERIAL_COM1      = 6,  /*!< COM 1 */
	COM_INTERFACE_SERIAL_COM2      = 7,  /*!< COM 2 */
	COM_INTERFACE_SERIAL_COM3      = 8,  /*!< COM 3 */
	COM_INTERFACE_SERIAL_COM4      = 9,  /*!< COM 4 */
	COM_INTERFACE_SERIAL_COM5      = 10, /*!< COM 5 */
	COM_INTERFACE_SERIAL_COM6      = 11, /*!< COM 6 */
	COM_INTERFACE_SERIAL_USBD      = 12, /*!< USB Device serial emulation (USB serial gadget)*/
	COM_INTERFACE_SERIAL_USBD2     = 48, /*!< USB Device serial emulation 2 (USB serial gadget)*/
	COM_INTERFACE_SERIAL_USB_DYN   = 35, /*!< Dynamic USB Serial Device hardware (USB to serial converter hardware)*/
	COM_INTERFACE_SERIAL_BC_READER = 37, /*!< Barcode reader */
	COM_INTERFACE_SERIAL_USBSER    = 40, /*!< Deprecated -- Do not use [N/A] */
	COM_INTERFACE_SERIAL_GATT      = 43, /*!< GATT Serial interface*/

	COM_INTERFACE_WLAN0 = 13, /*!< The wlan interface */

	COM_INTERFACE_BT                   = 14, /*!< Bluetooth interface for properties */
	COM_INTERFACE_ETH_BT               = 15, /*!< Bluetooth Ethernet (PAN) */
	COM_INTERFACE_PPP_BT               = 16, /*!< Bluetooth PPP/Dial (DUN) */
	COM_INTERFACE_DIAL_BT              = 17, /*!< Bluetooth Dial-only raw (DUN) */
	COM_INTERFACE_SERIAL_BT_SPP0       = 18, /*!< Bluetooth Serial0 (SPP0) */
	COM_INTERFACE_SERIAL_BT_SPP1       = 19, /*!< Bluetooth Serial1 (SPP1) */
	COM_INTERFACE_SERIAL_BT_SPP2       = 20, /*!< Bluetooth Serial2 (SPP2) */
	COM_INTERFACE_SERIAL_BT_SPP        = 45, /*!< BT SPP (Dynamic)*/
	COM_INTERFACE_SERIAL_BT_SPP_SERVER = 49, /*!< BT SPP (Incoming)*/

	COM_INTERFACE_ETH_USB_GADGET  = 21, /*!< USB Ethernet gadget USB0 */
	COM_INTERFACE_ETH_USB1_GADGET = 38, /*!< USB Ethernet gadget USB1 - Raptor exclusive */
	COM_INTERFACE_ETH_USB2_GADGET = 46, /*!< USB Ethernet gadget USB2 - Carbon8/10 exclusive */
	COM_INTERFACE_ETH_USB3_GADGET = 47, /*!< USB Ethernet gadget USB3 - Carbon8/10 exclusive */

	COM_INTERFACE_SERIAL_COM1A = 22, /*!< COM 1A - Please check the documentation for supported devices */
	COM_INTERFACE_SERIAL_COM1B = 23, /*!< COM 1B - Please check the documentation for supported devices */
	COM_INTERFACE_SERIAL_COM1C = 24, /*!< COM 1C - Please check the documentation for supported devices */
	COM_INTERFACE_SERIAL_COM1D = 25, /*!< COM 1D - Please check the documentation for supported devices */
	COM_INTERFACE_SERIAL_COM1E = 26, /*!< COM 1E - Please check the documentation for supported devices */

	COM_INTERFACE_PPP_COM1    = 27, /*!< PPP over COM1 */
	COM_INTERFACE_PPP_COM2    = 28, /*!< PPP over COM2 */
	COM_INTERFACE_PPP_COM3    = 29, /*!< PPP over COM3 */
	COM_INTERFACE_PPP_COM4    = 30, /*!< PPP over COM4 */
	COM_INTERFACE_PPP_COM5    = 31, /*!< PPP over COM5 */
	COM_INTERFACE_PPP_COM6    = 32, /*!< PPP over COM6 */
	COM_INTERFACE_PPP_USBD    = 33, /*!< PPP over USB Device serial emulation (USB serial gadget) */
	COM_INTERFACE_PPP_USB_DYN = 36, /*!< PPP over Dynamic USB Serial Device hardware (USB to serial converter hardware) */
	COM_INTERFACE_PPP_CSD     = 42, /*!< PPP over CSD (Placeholder, currently not supported) (VOS2 only) */

	COM_INTERFACE_ETH_USB_HOST_GADGET = 34, /*!< USB Ethernet Host gadget USBH1 - currently only Raptor platform supports this. Check also COM_PROP_USB1_MODE / COM_PROP_USB2_MODE.*/

	/* 35 occupied by COM_INTERFACE_SERIAL_USB_DYN, see above */
	/* 36 occupied by COM_INTERFACE_PPP_USB_DYN, see above */
	/* 37 occupied by COM_INTERFACE_SERIAL_BC_READER, see above */
	/* 38 occupied by COM_INTERFACE_ETH_USB1_GADGET, see above */

	COM_INTERFACE_BRIDGE = 39, /*!< Bridge interface */

	/* 40 occupied by COM_INTERFACE_SERIAL_USBSER, see above */
	/* 41 occupied by COM_INTERFACE_CSD, see above */
	/* 42 occupied by COM_INTERFACE_PPP_CSD, see above */
	/* 43 occupied by COM_INTERFACE_SERIAL_GATT, see above */

	COM_INTERFACE_LOOP_LOCAL = 44,

	/* 45 occupied by COM_INTERFACE_SERIAL_BT_SPP, see above */
	/* 46 occupied by COM_INTERFACE_ETH_USB2_GADGET, see above */
	/* 47 occupied by COM_INTERFACE_ETH_USB3_GADGET, see above */
	/* 48 occupied by COM_INTERFACE_SERIAL_USBD2, see above */
	/* 49 occupied by COM_INTERFACE_SERIAL_BT_SPP_SERVER, see above */

	COM_INTERFACE_ANY = 200 /*!< ANY Interface (used by com_Ping) */
};

/*! Ethernet PHY speed settings and duplex mode */
enum com_EthernetSpeed {
	COM_ETH_SPEED_AUTO    = 0, /*!< Ethernet auto-negotiation for speed and duplex mode */
	COM_ETH_SPEED_100M_FD = 1, /*!< 100 Mbit, Full-Duplex */
	COM_ETH_SPEED_100M_HD = 2, /*!< 100 Mbit, Half-Duplex */
	COM_ETH_SPEED_10M_FD  = 3, /*!< 10 Mbit, Full-Duplex */
	COM_ETH_SPEED_10M_HD  = 4  /*!< 10 Mbit, Half-Duplex */
};

/*! WLAN network connection type*/
enum com_WLANBSSType { COM_WLAN_BSS_TYPE_INFRA = 0,
#ifndef ADKCOM_3_0
	COM_WLAN_BSS_TYPE_ADHOC = 1
#endif
};

/*! WLAN authentication type*/
enum com_WLANAuthAlg {
	COM_WLAN_AUTH_NONE = 0, /*!< No authentication present in XML/Database-- Error*/
	COM_WLAN_AUTH_OPEN = 1, /*!< Open Mode (WPA / RSN (WPA2)) or no encryption*/
#ifndef ADKCOM_3_0
	COM_WLAN_AUTH_SHARED = 2 /*!< Shared authentication (WEP)*/
#endif
};

/*! WLAN protocol type*/
enum com_WLANProto {
	COM_WLAN_PROTO_WPA_WPA2 = 0, /*!< WPA/WPA2 Auto mode */
	COM_WLAN_PROTO_WPA      = 1, /*!< WPA Only */
	COM_WLAN_PROTO_WPA2     = 2, /*!< WPA2 Only*/
#ifndef ADKCOM_3_0
	COM_WLAN_PROTO_WEP = 3 /*!< WEP */
#endif
};

/*! WLAN Key Management*/
enum com_WLANKeyMgmt {
	COM_WLAN_KEY_MGMT_PSK = 0, /*!< Pre shared key management*/
	COM_WLAN_KEY_MGMT_EAP = 1, /*!< Extensible Authentication protocol management*/
#ifndef ADKCOM_3_0
	COM_WLAN_KEY_MGMT_NONE = 2 /*!< No key Mangement (unencrypted)*/
#endif
};

/*! WLAN pairwise/group ciphers*/
enum com_WLANCipherTypes {
	COM_WLAN_CIPHER_CCMP_TKIP = 0,                         /*!< Automatic cipher negotiation (TKIP or CCMP)*/
	COM_WLAN_CIPHER_AUTO      = COM_WLAN_CIPHER_CCMP_TKIP, /*!< Automatic cipher negotiation (TKIP or CCMP) - other name for COM_WLAN_CIPHER_CCMP_TKIP */
	COM_WLAN_CIPHER_TKIP      = 1,                         /*!< TKIP cipher Only */
	COM_WLAN_CIPHER_CCMP      = 2,                         /*!< CCMP (AES) cipher Only */
#ifndef ADKCOM_3_0
	COM_WLAN_CIPHER_WEP = 3, /*!< WEP (Wired Equivalent Privacy) */
#endif
	COM_WLAN_CIPHER_NONE = 4 /*!< No cipher */
};

/*! EAP type. Mandatory if KEY_MGMT is EAP*/
enum com_WLANEAPType {
	COM_WLAN_EAP_TYPE_NONE = 0, /*!< No EAP type -- Error*/
	COM_WLAN_EAP_TYPE_TLS  = 1, /*!< EAP-TLS (uses certificates)*/
	COM_WLAN_EAP_TYPE_PEAP = 2, /*!< EAP-PEAP (MSCHAPv2, uses identity + password)*/
	COM_WLAN_EAP_TYPE_FAST = 3, /*!< EAP-FAST (uses identity + password)*/
	COM_WLAN_EAP_TYPE_TTLS = 4, /*!< EAP-TTLS (tunneled TLS)*/
	COM_WLAN_EAP_TYPE_PWD  = 5, /*!< EAP-PWD*/
	COM_WLAN_EAP_TYPE_PSK  = 6, /*!< EAP-PSK*/
	COM_WLAN_EAP_TYPE_GPSK = 7  /*!< EAP-GPSK*/
};

/*! Visibility of the WLAN to be connected to (Hidden or visible)*/
enum com_WLANVisibility {
	COM_WLAN_SSID_VISIBLE = 0, /*!< WiFi Network is visible (default)*/
	COM_WLAN_SSID_HIDDEN  = 1  /*!< WiFi Network is hidden*/
};

/*! GPRS Radio Access Technology */
enum com_RadioACT {
	COM_GPRS_ACT_2G = 0, /*!< Deprecated - use COM_RADIO_ACCESS_TECHNOLOGY_2G instead */
	COM_GPRS_ACT_3G = 1, /*!< Deprecated - use COM_RADIO_ACCESS_TECHNOLOGY_3G instead */
	COM_GPRS_ACT_4G = 2, /*!< Deprecated - use COM_RADIO_ACCESS_TECHNOLOGY_4G instead */

	COM_RADIO_ACCESS_TECHNOLOGY_2G         = COM_GPRS_ACT_2G, /*!< 2G only */
	COM_RADIO_ACCESS_TECHNOLOGY_3G         = COM_GPRS_ACT_3G, /*!< 3G only */
	COM_RADIO_ACCESS_TECHNOLOGY_4G         = COM_GPRS_ACT_4G, /*!< 4G only */
	COM_RADIO_ACCESS_TECHNOLOGY_AUTO       = 3,               /*!< Automatic selection, preferring the highest supported */
	COM_RADIO_ACCESS_TECHNOLOGY_LTE_CAT_M1 = 4                /*!< LTE CAT-M1 only */
};

/*! GPRS Radio Frequency */
enum com_RadioFreq {
	COM_GSM_FREQ_UNKNOWN = 0,    /*!< Unknown*/
	COM_GSM_FREQ_500     = 500,  /*!< 500 MHz */
	COM_GSM_FREQ_700     = 700,  /*!< 700 MHz */
	COM_GSM_FREQ_850     = 850,  /*!< 850 MHz */
	COM_GSM_FREQ_900     = 900,  /*!< 900 MHz */
	COM_GSM_FREQ_1800    = 1800, /*!< 1800 MHz */
	COM_GSM_FREQ_1900    = 1900, /*!< 1900 MHz */
	COM_GSM_FREQ_2100    = 2100  /*!< 2100 MHz */
};

/*! Modem dialing types */
enum com_DialType {
	COM_DIAL_TONE  = 0, /*!< Dual-tone multi-frequency signaling (DTMF) */
	COM_DIAL_PULSE = 1  /*!< Pulse dialing */
};

/*! Modem dialing modes */
enum com_DialMode {
	COM_DIAL_ASYNC = 0, /*!< Asynchronous mode */
	COM_DIAL_SYNC  = 1  /*!< Synchronous mode */
};

/*! Modem modulation modes (Does not contain CSD modulation modes)*/
enum com_Modulation {
	COM_MOD_V92     = 0,  /*!< V.92 */
	COM_MOD_BELL103 = 1,  /*!< BELL 103 */
	COM_MOD_BELL212 = 2,  /*!< BELL 212 */
	COM_MOD_V21     = 3,  /*!< V21 */
	COM_MOD_V22     = 4,  /*!< V22 */
	COM_MOD_V22bis  = 5,  /*!< V22bis */
	COM_MOD_V32     = 6,  /*!< V32 */
	COM_MOD_V32bis  = 7,  /*!< V32bis */
	COM_MOD_V34     = 8,  /*!< V34 */
	COM_MOD_V90     = 9,  /*!< V.90 */
	COM_MOD_AUTO    = 10, /*!< Use the default modulation setting decided by the modem */
	COM_MOD_V23     = 11  /*!< V23 */
};

enum com_ModulationCSD {
	COM_MOD_CSD_AUTO        = 0, /*!< auto  speed */
	COM_MOD_CSD_V22BIS_2400 = 1, /*!< 2400  bps V.22bis */
	COM_MOD_CSD_V32_4800    = 2, /*!< 4800  bps V.32 */
	COM_MOD_CSD_V32_9600    = 3, /*!< 9600  bps V.32 */
	COM_MOD_CSD_V34_14400   = 4, /*!< 14400 bps V.34  */
	COM_MOD_CSD_V110_2400   = 5, /*!< 2400  bps V.110 */
	COM_MOD_CSD_V110_4800   = 6, /*!< 4800  bps V.110 */
	COM_MOD_CSD_V110_9600   = 7, /*!< 9600  bps V.110 */
	COM_MOD_CSD_V110_14400  = 8  /*!< 14400 bps V.110 */
};

/*! ISDN Protocols */
enum com_ISDNProt {
	COM_ISDN_X31B   = 0, /*!< X31B */
	COM_ISDN_X75    = 1, /*!< X75 */
	COM_ISDN_TRANSP = 2, /*!< Transparent */
	COM_ISDN_HDLC   = 3  /*!< HDLC */
};

/*! Error correction modes */
enum com_ErrorCorrection {
	COM_ERRCOR_V42_MNP      = 0, /*!< Selects auto reliable mode. This operates the same as COM_ERRCOR_V42_MNP_ONLY except failure to make a reliable connection results in the modem falling back to the speed buffered normal mode */
	COM_ERRCOR_NONE         = 1, /*!< Selects normal speed buffered mode (disables error-correction mode) */
	COM_ERRCOR_V42_MNP_ONLY = 2, /*!< Selects reliable (error-correction) mode. The modem will first attempt a LAPM connection and then an MNP connection */
	COM_ERRCOR_V42_ONLY     = 3, /*!< Selects LAPM error-correction mode. Failure to make an LAPM error-correction connection results in the modem hanging up */
	COM_ERRCOR_MNP_ONLY     = 4, /*!< Selects MNP error-correction mode. Failure to make an MNP error-correction connection results in the modem hanging up */
	COM_ERRCOR_AUTO         = 5  /*!< Use the default error correction decided by the modem */
};

/*! Fast connect modes */
enum com_FastConnect {
	COM_FASTCONNECT_DISABLE = 0, /*!< Disable Fast connect */
	COM_FASTCONNECT_ENABLE  = 1  /*!< Enable Fast connect */
};

/*! Compression modes */
enum com_Compression {
	COM_COMPR_NONE    = 0, /*!< Disables data compression */
	COM_COMPR_V42_MNP = 1, /*!< Microcom Networking Protocol compression */
	COM_COMPR_AUTO    = 2  /*!< Use the default data compression decided by the modem */
};

/*! PPP Authentication modes */
enum com_PPPAuth {
	COM_PPP_PAP      = 0, /*!< Password Authentication Protocol */
	COM_PPP_NONE     = 1, /*!< No PPP authentication */
	COM_PPP_CHAP     = 2, /*!< Challenge Handshake Authentication Protocol */
	COM_PPP_CHAP_PAP = 3  /*!< CHAP or PAP, let the PP daemon decide */
};

/*! Network device start modes */
enum com_StartupMode {
	COM_STARTUP_AUTO     = 0, /*!< Start network automatically when Terminal is booted */
	COM_STARTUP_ONDEMAND = 1  /*!< Start the network on demand only */
};

/*! SSL Protocol version */
enum com_SSLVersion {
	COM_SSL_NOVERSION = 0, /*!< No version (Error) */
	COM_SSL_TLS1_2    = 1, /*!< TLSv1.2 only */
	COM_SSL_TLS1_1    = 2, /*!< TLSv1.1 only */
	COM_SSL_TLS1_0    = 3, /*!< TLSv1 only */
	COM_SSL_SSL23     = 4, /*!< SSLv3 / TLSv1 / TLSv1.1 /TLSv1.2 (SSL 2.0 disabled). Highest available version is negotiated.*/
	COM_SSL_SSL3      = 5  /*!< SSLv3 only */
};

/*! SSL Protocol minimum version if version is set to  \c COM_SSL_SSL23. Ignored for all other SSL versions than SSL23. */
enum com_SSL23MinVersion {
	COM_SSL23MIN_ALL    = 0, /*!< No restrictions in protocol version. SSLv3 and higher are negotiated with the peer. */
	COM_SSL23MIN_TLS1_2 = 1, /*!< set TLSv1.2 as minimum allowed TLS version */
	COM_SSL23MIN_TLS1_1 = 2, /*!< set TLSv1.1 as minimum allowed TLS version */
	COM_SSL23MIN_TLS1_0 = 3, /*!< set TLSv1.0 as minimum allowed TLS version */
	COM_SSL23MIN_SSL3   = 4  /*!< set SSLv3 as minimum allowed TLS version */
};

/*! Serial Baudrate */
enum com_SerialBaudrate {
	COM_SERIAL_BAUD_INVALID = 0,      /*!< INVALID BAUDRATE */
	COM_SERIAL_BAUD_300     = 300,    /*!< 300 Baud */
	COM_SERIAL_BAUD_600     = 600,    /*!< 600 Baud */
	COM_SERIAL_BAUD_1200    = 1200,   /*!< 1200 Baud */
	COM_SERIAL_BAUD_2400    = 2400,   /*!< 2400 Baud */
	COM_SERIAL_BAUD_4800    = 4800,   /*!< 4800 Baud */
	COM_SERIAL_BAUD_9600    = 9600,   /*!< 9600 Baud */
	COM_SERIAL_BAUD_19200   = 19200,  /*!< 19200 Baud */
	COM_SERIAL_BAUD_38400   = 38400,  /*!< 38400 Baud */
	COM_SERIAL_BAUD_57600   = 57600,  /*!< 57600 Baud */
	COM_SERIAL_BAUD_115200  = 115200, /*!< 115200 Baud */
	COM_SERIAL_BAUD_460800  = 460800, /*!< 460800 Baud. Serial PPP only. VOS2 only. Used for CM5 Android<->Raptor interconnect*/
	COM_SERIAL_BAUD_921600  = 921600  /*!< 921600 Baud (Serial only. Used for USB)*/
};

/*! Serial Parity */
enum com_SerialParity {
	COM_SERIAL_PAR_INVALID = 0, /*!< INVALID PARITY */
	COM_SERIAL_PAR_NONE    = 1, /*!< No parity */
	COM_SERIAL_PAR_ODD     = 2, /*!< Odd parity */
	COM_SERIAL_PAR_EVEN    = 3  /*!< Even parity */
};

/*! Serial Databits */
enum com_SerialDatabits {
	COM_SERIAL_DATAB_INVALID = 0, /*!< INVALID DATABITS */
	COM_SERIAL_DATAB_5       = 5, /*!< 5 data bits */
	COM_SERIAL_DATAB_6       = 6, /*!< 6 data bits */
	COM_SERIAL_DATAB_7       = 7, /*!< 7 data bits */
	COM_SERIAL_DATAB_8       = 8  /*!< 8 data bits */
};

/*! Serial Stopbits */
enum com_SerialStopbits {
	COM_SERIAL_STOPB_INVALID = 0, /*!< INVALID STOPBITS */
	COM_SERIAL_STOPB_ONE     = 1, /*!< One stop bit */
	COM_SERIAL_STOPB_TWO     = 2  /*!< Two stop bits */
};

/*! Serial Flow Control */
enum com_SerialFlowcontrol {
	COM_SERIAL_FLOW_INVALID  = 0, /*!< INVALID FLOWCONTROL */
	COM_SERIAL_FLOW_NONE     = 1, /*!< No flow control */
	COM_SERIAL_FLOW_HARDWARE = 2, /*!< Hardware (CTS, RTS) flow control */
	COM_SERIAL_FLOW_SOFTWARE = 3  /*!< Software (Xon, Xoff) flow control */
};

/*! IPV6 discovery mode*/
enum com_IPV6Mode {
	COM_IPV6_MODE_INVALID = 0, /*!< Invalid mode*/
	COM_IPV6_MODE_STATIC  = 1, /*!< Static IPV6 configuration*/
	COM_IPV6_MODE_AUTO    = 2  /*!< Automatic IPV6 configuration*/
};

/*! Integer properties which can be obtained from a callback data-block */
enum com_ProfilePropertyInt {
	COM_PROFILE_PROP_INT_PORT                 = 0, /*!< The host service port number*/
	COM_PROFILE_PROP_INT_CONNECTION_TIMEOUT   = 1, /*!< Time a connection establishment may take before it is cancelled.*/
	COM_PROFILE_PROP_INT_NETWORK_TIMEOUT      = 2, /*!< Timeout for the network */
	COM_PROFILE_PROP_INT_SSL_VERSION          = 3, /*!< SSL: Protocol version used for client or server mode*/
	COM_PROFILE_PROP_INT_SSL_CHECK_EXPIRATION = 4, /*!< **DEPRECATED** SSL: Check expiration date of the certificate, default is "yes"*/
	COM_PROFILE_PROP_INT_INTERFACE            = 5, /*!< Defined symbol for the network interface */
	COM_PROFILE_PROP_INT_STARTUP_MODE         = 6, /*!< Defines the networks start-up behavior\n
	                                                                                                "AUTO" (network is brought up automatically on every system boot)\n
	                                                                                                "ON-DEMAND" (network is brought up only if the application demands it) 	*/

	COM_PROFILE_PROP_INT_MODEM_DIAL_MODE        = 7,  /*!< The analogue modem connection mode*/
	COM_PROFILE_PROP_INT_MODEM_DIAL_TYPE        = 8,  /*!< The analogue modem dialling type*/
	COM_PROFILE_PROP_INT_MODEM_DIAL_MODULATION  = 9,  /*!< The modulation types for analogue modem*/
	COM_PROFILE_PROP_INT_MODEM_DIAL_COMPRESSION = 10, /*!< The compression type for analogue modem*/
	COM_PROFILE_PROP_INT_MODEM_DIAL_BLIND_DIAL  = 11, /*!< 0: blind dialing disabled; non-0: blind dialing enabled*/
	COM_PROFILE_PROP_INT_MODEM_DIAL_ERR_CORR    = 12, /*!< error correcting mode to be negotiated in a subsequent data connection*/

#ifndef ADKCOM_3_0
	COM_PROFILE_PROP_INT_RAW_PSTN_DIAL_MODE        = 7,  /*!< The analogue modem connection mode*/
	COM_PROFILE_PROP_INT_RAW_PSTN_DIAL_TYPE        = 8,  /*!< The analogue modem dialling type*/
	COM_PROFILE_PROP_INT_RAW_PSTN_DIAL_MODULATION  = 9,  /*!< The modulation types for analogue modem*/
	COM_PROFILE_PROP_INT_RAW_PSTN_DIAL_COMPRESSION = 10, /*!< The compression type for analogue modem*/
	COM_PROFILE_PROP_INT_RAW_PSTN_DIAL_BLIND_DIAL  = 11, /*!< 0: blind dialing disabled; non-0: blind dialing enabled*/
	COM_PROFILE_PROP_INT_RAW_PSTN_DIAL_ERR_CORR    = 12, /*!< error correcting mode to be negotiated in a subsequent data connection*/

	COM_PROFILE_PROP_INT_RAW_ISDN_DIAL_MODE        = 14, /*!< The analogue modem connection mode*/
	COM_PROFILE_PROP_INT_RAW_ISDN_DIAL_TYPE        = 15, /*!< The analogue modem dialling type*/
	COM_PROFILE_PROP_INT_RAW_ISDN_DIAL_MODULATION  = 16, /*!< The modulation types for analogue modem*/
	COM_PROFILE_PROP_INT_RAW_ISDN_DIAL_COMPRESSION = 17, /*!< The compression type for analogue modem*/
	COM_PROFILE_PROP_INT_RAW_ISDN_DIAL_BLIND_DIAL  = 18, /*!< 0: blind dialing disabled; non-0: blind dialing enabled*/
	COM_PROFILE_PROP_INT_RAW_ISDN_DIAL_ERR_CORR    = 19, /*!< error correcting mode to be negotiated in a subsequent data connection*/
#endif

	COM_PROFILE_PROP_INT_DHCP = 21, /*!< DHCPv4 client usage for this network */

	COM_PROFILE_PROP_INT_PPP_IDLE_TIMEOUT_SEC = 22, /*!< PPP authentication mode */
	COM_PROFILE_PROP_INT_PPP_AUTH             = 23, /*!< PPP authentication mode */
#ifndef ADKCOM_3_0
	COM_PROFILE_PROP_INT_PPPGPRS_IDLE_TIMEOUT_SEC = 22, /*!< If no new connection within x minutes and no active connections, release modem connection*/
	COM_PROFILE_PROP_INT_PPPGPRS_AUTH             = 23, /*!< PPP authentication mode */

	COM_PROFILE_PROP_INT_PPPMODEM_IDLE_TIMEOUT_SEC = 24, /*!< If no new connection within x minutes and no active connections, release modem connection*/
	COM_PROFILE_PROP_INT_PPPMODEM_AUTH             = 25, /*!< PPP authentication mode */

	COM_PROFILE_PROP_INT_PPPMODEM_DIAL_MODE        = 26, /*!< The analogue modem connection mode*/
	COM_PROFILE_PROP_INT_PPPMODEM_DIAL_TYPE        = 27, /*!< The analogue modem dialling type*/
	COM_PROFILE_PROP_INT_PPPMODEM_DIAL_MODULATION  = 28, /*!< The modulation types for analogue modem*/
	COM_PROFILE_PROP_INT_PPPMODEM_DIAL_COMPRESSION = 29, /*!< The compression type for analogue modem*/
	COM_PROFILE_PROP_INT_PPPMODEM_DIAL_BLIND_DIAL  = 30, /*!< 0: blind dialing disabled; non-0: blind dialing enabled*/
	COM_PROFILE_PROP_INT_PPPMODEM_DIAL_ERR_CORR    = 31, /*!< error correcting mode to be negotiated in a subsequent data connection*/
#endif

	COM_PROFILE_PROP_INT_NETWORK_TYPE = 32, /*!< The type of network profile associated with the connection profile*/

	COM_PROFILE_PROP_INT_WLAN_AUTH_ALG     = 33, /*!< WLAN authentication mode */
	COM_PROFILE_PROP_INT_WLAN_BSS_TYPE     = 34, /*!< WLAN network connection type*/
	COM_PROFILE_PROP_INT_WLAN_CHANNEL      = 35, /*!< WLAN channel number */
	COM_PROFILE_PROP_INT_WLAN_CIPHER_GROUP = 36, /*!< WLAN Group cipher */
#ifndef ADKCOM_3_0
	COM_PROFILE_PROP_INT_WLAN_CIPHER_GROUPWISE = COM_PROFILE_PROP_INT_WLAN_CIPHER_GROUP, /*!< WLAN Group cipher (DEPRECATED - use COM_PROFILE_PROP_INT_WLAN_CIPHER_GROUP instead)*/
#endif
	COM_PROFILE_PROP_INT_WLAN_CIPHER_PAIRWISE = 37, /*!< WLAN Pairwise cipher */
#ifndef ADKCOM_3_0
	COM_PROFILE_PROP_INT_WLAN_WEP_INDEX = 38, /*!< WLAN WEP index*/
#endif
	COM_PROFILE_PROP_INT_WLAN_EAP_TYPE   = 39, /*!< WLAN EAP type*/
	COM_PROFILE_PROP_INT_WLAN_KEY_MGMT   = 45, /*!< WLAN Key Management*/
	COM_PROFILE_PROP_INT_WLAN_PROTO      = 46, /*!< WLAN Protocol*/
	COM_PROFILE_PROP_INT_WLAN_VISIBILITY = 61, /*!< WLAN visibility option (if network to be connected to is hidden/visible). See enum com_WLANVisibility*/

	COM_PROFILE_PROP_INT_RAW_SERIAL_BAUDRATE    = 40, /*!< Serial Baudrate*/
	COM_PROFILE_PROP_INT_RAW_SERIAL_PARITY      = 41, /*!< Serial Parity*/
	COM_PROFILE_PROP_INT_RAW_SERIAL_DATABITS    = 42, /*!< Amount of data bits*/
	COM_PROFILE_PROP_INT_RAW_SERIAL_STOPBITS    = 43, /*!< Amount of stop bits*/
	COM_PROFILE_PROP_INT_RAW_SERIAL_FLOWCONTROL = 44, /*!< Flow control*/

	COM_PROFILE_PROP_INT_SERVER                   = 45,                                       /*!< Server mode*/
	COM_PROFILE_PROP_INT_SSL_MUTUAL               = 46,                                       /*!< Mutual SSL authentication (if server mode)*/
	COM_PROFILE_PROP_INT_SSL_POLICY               = 47,                                       /*!< SSL policy*/
	COM_PROFILE_PROP_INT_BAUDRATE                 = COM_PROFILE_PROP_INT_RAW_SERIAL_BAUDRATE, /*!< Baudrate*/
	COM_PROFILE_PROP_INT_PARITY                   = COM_PROFILE_PROP_INT_RAW_SERIAL_PARITY,   /*!< Parity bits*/
	COM_PROFILE_PROP_INT_DATABITS                 = COM_PROFILE_PROP_INT_RAW_SERIAL_DATABITS, /*!< Data bits*/
	COM_PROFILE_PROP_INT_STOPBITS                 = COM_PROFILE_PROP_INT_RAW_SERIAL_STOPBITS, /*!< Stop bits*/
	COM_PROFILE_PROP_INT_TCP_MAXSEG               = 48,                                       /*!< TCP_MAXSEG TCP socket option*/
	COM_PROFILE_PROP_INT_TCP_FIN_TIMEOUT          = 49,                                       /*!< Deprecated -- Do not use [N/A] */
	COM_PROFILE_PROP_INT_MODEM_FAST_CONNECT       = 50,                                       /*!< defines the property symbol for fast connect */
	COM_PROFILE_PROP_INT_IPV6_ENABLED             = 51,                                       /*!< IPv6 Enabled for this network */
	COM_PROFILE_PROP_INT_IPV6_MODE                = 52,                                       /*!< IPv6 Address if DHCPv6 disabled */
	COM_PROFILE_PROP_INT_IPV6_GLOBAL_PREFIX       = 53,                                       /*!< IPv6 network prefix if DHCPv6 disabled */
	COM_PROFILE_PROP_INT_IPV6_UNIQUE_LOCAL_PREFIX = 54,                                       /*!< IPv6 network prefix if DHCPv6 disabled */
	COM_PROFILE_PROP_INT_IPV4_ENABLED             = 55,                                       /*!< IPv4 Enabled for this network */
	COM_PROFILE_PROP_INT_SSL23_MINVERSION         = 56,                                       /*!< SSL: Minimum Protocol version used for client or server mode if version is set to SSL23*/
	COM_PROFILE_PROP_INT_BIND                     = 57,                                       /*!< Bind to host for this connection enabled or disabled*/
	COM_PROFILE_PROP_INT_SIM_SLOT                 = 58,                                       /*!< SIM Slot to be used*/
	COM_PROFILE_PROP_INT_CSD_MODULATION           = 59,                                       /*!< CSD Modulation type to be used. Refer to enum com_ModulationCSD*/
	COM_PROFILE_PROP_INT_SO_REUSEADDR             = 60,                                       /*!< socket option SO_REUSEADDR set or unset for server sockets*/
	//61 is used by COM_PROFILE_PROP_INT_WLAN_VISIBILITY above*/
	COM_PROFILE_PROP_INT_SO_LINGER         = 62, /*!< socket option SO_LINGER set or unset*/
	COM_PROFILE_PROP_INT_SO_LINGER_TIMEOUT = 63, /*!< socket option SO_LINGER timeout*/
	COM_PROFILE_PROP_INT_SHARED_CONNECTION = 64, /*!< VOS raw modem connections only: connection marked as shared by other apps*/
	COM_PROFILE_PROP_INT_RESTRICT_LOCAL    = 65, /*!< Server-connection should be restricted to local interface if network-profile has local interface*/
	COM_PROFILE_PROP_INT_LCP_RESTART       = 66, /*!< PPP LCP Restart interval */
	COM_PROFILE_PROP_INT_DNS_TIMEOUT       = 67, /*!< DNS Timeout, -1 indicates it is not set*/
	COM_PROFILE_PROP_INT_PORT_SERVER       = 68  /*!< IP port read from the server listen socket (note: can only be used from
                                                      *   callback comCallbackConnectionType with event COM_EVENT_CONNECTION_LISTENING */

};

/*! String properties which can be obtained from a callback data-block */
enum com_ProfilePropertyString {
	COM_PROFILE_PROP_STRING_NETWORKNAME      = 0, /*!< Name of the Network Profile to be used.*/
	COM_PROFILE_PROP_STRING_NETWORKNAME_FULL = 1, /*!< Full of the Network Profile to be used.*/

	COM_PROFILE_PROP_STRING_ADDRESS   = 2,  /*!< The host address. Either an IP address or a DNS name*/
	COM_PROFILE_PROP_STRING_IPADDRESS = 40, /*!< The host address. If present it is always the (resolved) ip-address*/

	COM_PROFILE_PROP_STRING_SSL_CA_CERT_FILE  = 3, /*!< SSL: Path and name of file containing trusted CA certificates (use instead of SSL_CERT_DIR)*/
	COM_PROFILE_PROP_STRING_SSL_CA_CERT_DIR   = 4, /*!< SSL: Name of directory containing trusted CA certificates (use instead of SSL_CERT_FILE)*/
	COM_PROFILE_PROP_STRING_SSL_OWN_CERT_FILE = 5, /*!< SSL: Name of file containing the own public key certificate (required for mutual authentication)*/
	COM_PROFILE_PROP_STRING_SSL_OWN_PKEY_FILE = 6, /*!< SSL: Name of file containing the private key (required for mutual authentication)*/
	COM_PROFILE_PROP_STRING_SSL_PKCS12_FILE   = 7, /*!< SSL: Name of PKCS#12 file containing the private key and client cert*/
	COM_PROFILE_PROP_STRING_SSL_CIPHER_LIST   = 8, /*!< SSL: OpenSSL Cipher Suite list*/

	COM_PROFILE_PROP_STRING_RAW_PSTN_PABX       = 9,  /*!< PABX Number (dial prefix)*/
	COM_PROFILE_PROP_STRING_RAW_PSTN_ADDR       = 10, /*!< Host dial number*/
	COM_PROFILE_PROP_STRING_RAW_ISDN_PABX       = 11, /*!< PABX Number (dial prefix)*/
	COM_PROFILE_PROP_STRING_RAW_ISDN_ADDR       = 12, /*!< Host dial number*/
	COM_PROFILE_PROP_STRING_RAW_ISDN_X25_NUMBER = 13, /*!< Additional routing information in case of X.31-B connection*/
	COM_PROFILE_PROP_STRING_RAW_ISDN_USER_DATA  = 14, /*!< Additional routing information in case of X.31-B connection*/

	COM_PROFILE_PROP_STRING_OWN_IP_ADDR = 15, /*!< IPv4 Address if DHCP disabled */
	COM_PROFILE_PROP_STRING_OWN_NETMASK = 16, /*!< IPv4 network mask if DHCP disabled */
	COM_PROFILE_PROP_STRING_OWN_GATEWAY = 17, /*!< IPv4 Gateway if DHCP disabled */
	COM_PROFILE_PROP_STRING_OWN_DNS1    = 18, /*!< first DNS server if DHCP is disabled */
	COM_PROFILE_PROP_STRING_OWN_DNS2    = 19, /*!< second DNS server if DHCP is disabled */
#ifndef ADKCOM_3_0
	COM_PROFILE_PROP_STRING_LAN_OWN_IP_ADDR = 15, /*!< IPv4 Address if DHCP disabled */
	COM_PROFILE_PROP_STRING_LAN_OWN_NETMASK = 16, /*!< IPv4 network mask if DHCP disabled */
	COM_PROFILE_PROP_STRING_LAN_OWN_GATEWAY = 17, /*!< IPv4 Gateway if DHCP disabled */
	COM_PROFILE_PROP_STRING_LAN_OWN_DNS1    = 18, /*!< first DNS server if DHCP is disabled */
	COM_PROFILE_PROP_STRING_LAN_OWN_DNS2    = 19, /*!< second DNS server if DHCP is disabled */
#endif

	COM_PROFILE_PROP_STRING_PPPGPRS_APN = 20, /*!< GPRS APN */

	COM_PROFILE_PROP_STRING_PPP_USERNAME = 21, /*!< PPP username required for authentication */
	COM_PROFILE_PROP_STRING_PPP_PASSWORD = 22, /*!< PPP password required for authentication */

	COM_PROFILE_PROP_STRING_PPPGPRS_USERNAME = 21, /*!< PPP username required for authentication */
	COM_PROFILE_PROP_STRING_PPPGPRS_PASSWORD = 22, /*!< PPP password required for authentication */

	COM_PROFILE_PROP_STRING_PPPGPRS_DIALSTRING = 23, /*!< GPRS dialing string */

	COM_PROFILE_PROP_STRING_PPPMODEM_PABX        = 24, /*!< PABX Number (dial prefix)*/
	COM_PROFILE_PROP_STRING_PPPMODEM_DIAL_NUMBER = 25, /*!< PPP host number */
	COM_PROFILE_PROP_STRING_PPPMODEM_USERNAME    = 26, /*!< PPP username required for authentication */
	COM_PROFILE_PROP_STRING_PPPMODEM_PASSWORD    = 27, /*!< PPP password required for authentication */

	COM_PROFILE_PROP_STRING_WLAN_SSID = 28, /*!< WLAN network name */
	COM_PROFILE_PROP_STRING_WLAN_PSK  = 29, /*!< WPA pre-shared key */
#ifndef ADKCOM_3_0
	COM_PROFILE_PROP_STRING_WLAN_WEP_0 = 30, /*!< Static WEP keys */
	COM_PROFILE_PROP_STRING_WLAN_WEP_1 = 31, /*!< Static WEP keys */
	COM_PROFILE_PROP_STRING_WLAN_WEP_2 = 32, /*!< Static WEP keys */
	COM_PROFILE_PROP_STRING_WLAN_WEP_3 = 33, /*!< Static WEP keys */
#endif
	COM_PROFILE_PROP_STRING_WLAN_EAP_IDENTITY    = 34, /*!< Identity */
	COM_PROFILE_PROP_STRING_WLAN_EAP_PASSWORD    = 35, /*!< Password */
	COM_PROFILE_PROP_STRING_WLAN_EAP_CACERT      = 36, /*!< File path to CA certificate file. */
	COM_PROFILE_PROP_STRING_WLAN_EAP_CLIENTCERT  = 37, /*!< File path to client certificate file. */
	COM_PROFILE_PROP_STRING_WLAN_EAP_PRIVKEY     = 38, /*!< File path to private key file. */
	COM_PROFILE_PROP_STRING_WLAN_EAP_PRIVKEY_PWD = 39, /*!< Password for private key file. */
	                                                   // 40 is COM_PROFILE_PROP_STRING_IPADDRESS
	COM_PROFILE_PROP_STRING_COUNTRY_CODE = 41,         /*!< Country code, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 */

	COM_PROFILE_PROP_STRING_OWN_IPV6_GLOBAL_ADDR       = 44, /*!< IPv6 Unique local address */
	COM_PROFILE_PROP_STRING_OWN_IPV6_UNIQUE_LOCAL_ADDR = 45, /*!< IPv6 Unique local address */
	COM_PROFILE_PROP_STRING_OWN_IPV6_GATEWAY           = 46, /*!< IPv6 Gateway if DHCPv6 disabled */
	COM_PROFILE_PROP_STRING_OWN_IPV6_DNS1              = 47, /*!< first IPV6 DNS server if DHCPv6 is disabled */
	COM_PROFILE_PROP_STRING_OWN_IPV6_DNS2              = 48, /*!< second IPv6 DNS server if DHCPv6 is disabled */
	COM_PROFILE_PROP_STRING_BIND_GW                    = 49, /*!< Bind Gateway IPv4*/
	COM_PROFILE_PROP_STRING_BIND_GW_V6                 = 50, /*!< Bind Gateway IPv6*/
	COM_PROFILE_PROP_STRING_ADDR_FAMILY                = 51, /*!< Address family*/
	COM_PROFILE_PROP_STRING_WLAN_EAP_PAC_FILE          = 52, /*!< User EAP FAST PAC file */
	COM_PROFILE_PROP_STRING_PPPCSD_DIAL_NUMBER         = 53, /*!< CSD dial number */

	COM_PROFILE_PROP_STRING_OWN_DHCP_HOSTNAME  = 54, /*!< DHCP client hostname if DHCP is enabled */
	COM_PROFILE_PROP_STRING_WEBSOCK_HOST       = 55, /*!< content of host header */
	COM_PROFILE_PROP_STRING_WEBSOCK_ORIGIN     = 56, /*!< content of origin header */
	COM_PROFILE_PROP_STRING_WEBSOCK_PATH       = 57, /*!< URI Path */
	COM_PROFILE_PROP_STRING_WEBSOCK_PROTOCOL   = 58, /*!< name of WS protocol to connect to */
	COM_PROFILE_PROP_STRING_SSL_SNI_SERVERNAME = 59, /*!< SSL SNI servername */
	COM_PROFILE_PROP_STRING_WLAN_BAND          = 60, /*!< WiFi band selection */
	COM_PROFILE_PROP_STRING_OWN_DHCP_CLIENTID  = 61, /*!< DHCP client ID if DHCP is enabled */
	COM_PROFILE_PROP_STRING_IPADDRESS_SERVER   = 62  /*!< IP address read from the server listen socket (note: can only be used from
                                                          *   callback comCallbackConnectionType with event COM_EVENT_CONNECTION_LISTENING */
};

/*! Network Event structure*/
struct com_networkEventData {
	enum com_Interfaces network; /*!< Type of Network */
	char *data1;                 /*!< Additional Data */
};

/*! Bit mask of available/supported bluetooth profiles */
enum com_BTProfileMask {
	COM_BT_DUN   = 1 << 0, /*!< Dial-Up Modem Profile */
	COM_BT_PAN   = 1 << 1, /*!< Ethernet Profile */
	COM_BT_SPP   = 1 << 2, /*!< Serial Profile 0 */
	COM_BT_SPP1  = 1 << 3, /*!< Serial Profile 1 */
	COM_BT_SPP2  = 1 << 4, /*!< Serial Profile 2 */
	COM_BT_ERROR = 1 << 30 /*!< Error occured. Check com_errno */
};

/*! BT SPP Port to configure for outgoing (client) or incoming (server) connection */
enum com_BTConfigSPPPort {
	COM_BT_CONFIG_SPP0 = 0, /*!< BT SPP0 port */
	COM_BT_CONFIG_SPP2 = 1  /*!< BT SPP2 port  */
};

#define COM_IPV4_ADDR_LENGTH    (15 + 1) /*!< Max length of an IPv4 address in string representation */
#define COM_IPV6_ADDR_LENGTH    (47 + 1) /*!< Max length of an IPv6 address in string representation */
#define COM_MAC_ADDR_LENGTH     (17 + 1) /*!< Max length of an MAC address in string representation */
#define COM_ETH_MAC_ADDR_LENGTH COM_MAC_ADDR_LENGTH /*!< Max length of an Ethernet MAC address in string representation */

/*! IP configuration information */
struct com_IPConfig {
	unsigned int dhcp;                  /*!< 0 if DHCP disabled */
	char ip_addr[COM_IPV4_ADDR_LENGTH]; /*!< IPv4 address (string) */
	char netmask[COM_IPV4_ADDR_LENGTH]; /*!< network mask (string) */
	char gateway[COM_IPV4_ADDR_LENGTH]; /*!< Gateway address (string), empty string if not set */
	char dns1[COM_IPV4_ADDR_LENGTH];    /*!< DNS1 address (string), empty string if not set */
	char dns2[COM_IPV4_ADDR_LENGTH];    /*!< DNS2 address (string), empty string if not set */
	char mac[COM_ETH_MAC_ADDR_LENGTH];  /*!< Ethernet MAC address (string) */
};

enum com_LinkType {
	COM_LINK_ETHERNET       = 0,
	COM_LINK_POINT_TO_POINT = 1,
	COM_LINK_LOCAL_LOOP     = 2
};

enum com_IPv6_AddrScope {
	COM_IPV6_SCOPE_UNSPECIFIED = 0,
	COM_IPV6_SCOPE_GLOBAL      = 1,
	COM_IPV6_SCOPE_LINK_LOCAL  = 2,
	COM_IPV6_SCOPE_SITE_LOCAL  = 3,
	COM_IPV6_SCOPE_V4MAPPED    = 4,
	COM_IPV6_SCOPE_V4COMPAT    = 5,
	COM_IPV6_SCOPE_LOOPBACK    = 6
};

struct com_IPv6_Data {
	struct com_IPv6_Data *next; // next chain element or NULL if end reached

	char IPV6_Addr[COM_IPV6_ADDR_LENGTH];
	unsigned char IPV6_Addr_Prefixlen;
	enum com_IPv6_AddrScope IPV6_Addr_Scope;
};

/*! Network interface configuration and statistics */
struct com_NetworkInterfaceInfo {
	enum com_NetworkStatus status;

	char IPV4_Addr[COM_IPV4_ADDR_LENGTH];
	char IPV4_Netmask[COM_IPV4_ADDR_LENGTH];
	char IPV4_Broadcast[COM_IPV4_ADDR_LENGTH];
	char IPV4_Destination[COM_IPV4_ADDR_LENGTH];
	char IPV4_Gateway[COM_IPV4_ADDR_LENGTH];
	char IPV4_DNS1[COM_IPV4_ADDR_LENGTH];
	char IPV4_DNS2[COM_IPV4_ADDR_LENGTH];
	int IPV4_DHCP_Enabled;

	int IPV6_Supported;
	int IPV6_Enabled;
	struct com_IPv6_Data *IPV6_Address_List;
	char IPV6_Gateway[COM_IPV6_ADDR_LENGTH];
	char IPV6_DNS1[COM_IPV6_ADDR_LENGTH];
	char IPV6_DNS2[COM_IPV6_ADDR_LENGTH];
	int IPV6_DHCP_Enabled;

	unsigned int RX_Packets;
	unsigned int RX_Errors;
	unsigned int RX_Bytes;

	unsigned int TX_Packets;
	unsigned int TX_Errors;
	unsigned int TX_Bytes;

	char HW_Addr[COM_ETH_MAC_ADDR_LENGTH];
	int MTU_Size;
	int Link_Speed;
	enum com_LinkType Link_Type;
};

enum com_NetworkRouteProtocol {
	COM_RTPROT_UNSPEC   = 0, /*!< Unspecified */
	COM_RTPROT_REDIRECT = 1, /*!< Route installed by ICMP redirects; not used by current IPv4 */
	COM_RTPROT_KERNEL   = 2, /*!< Route installed by kernel */
	COM_RTPROT_BOOT     = 3, /*!< Route installed during boot */
	COM_RTPROT_STATIC   = 4  /*!< Route installed by administrator */
};

enum com_NetworkRouteScope {
	COM_RT_SCOPE_UNIVERSE = 0,   /*!< Universe */
	COM_RT_SCOPE_SITE     = 200, /*!< Site */
	COM_RT_SCOPE_LINK     = 253, /*!< Link */
	COM_RT_SCOPE_HOST     = 254, /*!< Host */
	COM_RT_SCOPE_NOWHERE  = 255  /*!< Nowhere */
};

/*! Network routing table entry (IPv4) */
struct com_NetworkRouteInfo_IPv4 {
	struct com_NetworkRouteInfo_IPv4 *next; /*!< Pointer to next routing table element or NULL if end of list is reached */

	char ifName[16];                        /*!< Network interface name */
	char dstAddr[COM_IPV4_ADDR_LENGTH];     /*!< Destination address */
	char srcAddr[COM_IPV4_ADDR_LENGTH];     /*!< Source address */
	char gateWay[COM_IPV4_ADDR_LENGTH];     /*!< Gateway address */
	char dstNetmask[COM_IPV4_ADDR_LENGTH];  /*!< Destination Netmask */
	unsigned char dstPrefixlen;             /*!< Destination prefix length */
	unsigned int metric;                    /*!< Metric */
	enum com_NetworkRouteScope scope;       /*!< Scope */
	enum com_NetworkRouteProtocol protocol; /*!< Routing protocol */
	int isDefaultRoute;                     /*!< set to 1 if default route, 0 otherwise */
};

/*! Network routing table entry (IPv6) */
struct com_NetworkRouteInfo_IPv6 {
	struct com_NetworkRouteInfo_IPv6 *next; /*!< Pointer to next routing table element or NULL if end of list is reached */

	char ifName[16];                        /*!< Network interface name */
	char dstAddr[COM_IPV6_ADDR_LENGTH];     /*!< Destination address */
	char srcAddr[COM_IPV6_ADDR_LENGTH];     /*!< Source address */
	char gateWay[COM_IPV6_ADDR_LENGTH];     /*!< Gateway address */
	unsigned char dstPrefixlen;             /*!< Destination prefix length */
	unsigned int metric;                    /*!< Metric */
	enum com_NetworkRouteScope scope;       /*!< Scope */
	enum com_NetworkRouteProtocol protocol; /*!< Routing protocol */
	int isDefaultRoute;                     /*!< set to 1 if default route, 0 otherwise */
};

/*! Address Family to specify the IP stack used to establish an IP connection */
enum com_AddressFamily {
	COM_AF_UNSPEC = 0, /*!< Unspecified Internet Protocol version */
	COM_AF_INET   = 1, /*!< Internet Protocol version 4 */
	COM_AF_INET6  = 2  /*!< Internet Protocol version 6 */
};

/*! Bit mask of power saving configurations */
enum com_PowerMgmtMask {
	COM_GPRC_URC_OFF           = 0x0000, /*!< Disable RSSI and packet switch notification */
	COM_GPRS_URC_RSSI          = 0x0001, /*!< GPRS signal */
	COM_GPRS_URC_PACKET_SWITCH = 0x0002, /*!< Packet switch event */
	COM_ALL_NETWORK_STATUS_ON  = 0x0004, /*!< Enable sending of network events connection status */
	COM_ALL_NETWORK_STATUS_OFF = 0x0008  /*!< Disables sending of all network events including wireless signal strength. Resets to ON when COM connect API is called */
};

/*! Password type requested via event callback function */
enum com_PasswordType {
	COM_PASSWORD_SSL               = 0,                /*!< DEPRECATED - use COM_PASSWORD_SSL_PKCS12 instead --- PKCS#12 password for SSL required */
	COM_PASSWORD_SSL_PKCS12        = COM_PASSWORD_SSL, /*!< PKCS#12 password for SSL required */
	COM_PASSWORD_SSL_POLICY_ACCEPT = 1,                /*!< Accept the changed SSL/TLS policy. Password sent must be "POLICY_ACCEPTED_INSECURE_NONPCICOMPLIANT_DANGEROUS" if policy change was accepted by application */
	COM_PASSWORD_SSL_PKEY          = 2                 /*!< Password to dectrypt a passphraze secured SSL private key required */
};

/*! Bluetooth Pairing method */
enum com_BTPairMethod {
	COM_BT_PAIR_AUTO          = 0, /*!< Pairing method is automatically determined based on friendly name */
	COM_BT_PAIR_LEGACY        = 1, /*!< Bluetooth Legacy pairing */
	COM_BT_PAIR_SSP_JUSTWORKS = 2, /*!< Bluetooth Secure Simple Pairing in Just-Works mode */
	COM_BT_PAIR_SSP_PASSKEY   = 3  /*!< Bluetooth Secure Simple Paiting with Passkey confirmation */
};

/*! Type of wireless networks to be scanned */
enum com_WirelessNetworkType {
	COM_WIRELESS_TYPE_BT                         = 0, /*!< Request a BT device discovery */
	COM_WIRELESS_TYPE_WLAN                       = 1, /*!< Request a WLAN device scan */
	COM_WIRELESS_TYPE_GSM                        = 2, /*!< Request a GSM operator scan */
	COM_WIRELESS_TYPE_BT_SERVICES                = 3, /*!< Request a BT service scan of all BT devices in vicinity */
	COM_WIRELESS_TYPE_BT_SERVICES_EXT_SPP        = 4, /*!< Request a BT extended SPP scan of all BT devices in vicinity */
	COM_WIRELESS_TYPE_BT_PAIRED_SERVICES         = 5, /*!< Request a BT service scan of all BT devices in vicinity (Paired only -- Faster)*/
	COM_WIRELESS_TYPE_BT_PAIRED_SERVICES_EXT_SPP = 6  /*!< Request a BT extended SPP scan of all BT devices in vicinity (Paired only -- Faster)*/
};

enum com_SIMStatus {
	COM_GPRS_SIM_READY       = 0, /* SIM is ready and is "unlocked" */
	COM_GPRS_SIM_NEEDS_PIN   = 1, /* PIN is needed, SIM is not ready */
	COM_GPRS_SIM_NEEDS_PUK   = 2, /* PUK is needed, SIM is locked */
	COM_GPRS_SIM_BUSY        = 3, /* Currently the status cannot be obtained, try again later */
	COM_GPRS_SIM_NOT_PRESENT = 4, /* There is currently no SIM present */
	COM_GPRS_SIM_FAILURE     = 5, /* SIM Failure */
	COM_GPRS_SIM_WRONG       = 6  /* SIM wrong */
};

struct com_PingInfo {
	unsigned int ntransmitted; /*!< number of transmitted packets */
	unsigned int nreceived;    /*!< number of received packets */
	unsigned long tavg;        /*!< average round trip in milliseconds*/
	unsigned long tmin;        /*!< max round trip in milliseconds*/
	unsigned long tmax;        /*!< min round trip in milliseconds*/
};

/*!< Wait until data is ready to read */
#define COM_SELECT_READ (1 << 0)
/*!< Wait until the socket becomes available for writing */
#define COM_SELECT_WRITE (1 << 1)
/*!< Return if an interrupt is currently pending*/
#define COM_SELECT_INTERRUPT (1 << 2)

#define MAX_WLAN_SURVEY_COUNT 25 /*!< Maximum number of WLAN networks per WLAN scan */
#define COM_BT_COUNT          "BT_COUNT" /*!< Name of JSON object defining the count of BT stations stored in JSON message returned by BT properties or scan*/
#define COM_WLAN_COUNT        "WLAN_COUNT" /*!< Name of JSON object defining the count of WLAN networks stored in JSON message returned by WLAN scan/survey*/
#define COM_GSM_COUNT         "GSM_COUNT" /*!< Name of JSON object defining the count of GSM operators stored in JSON message returned by GSM scan/survey*/
#define COM_LOG_COUNT         "LOG_COUNT" /*!< Name of JSON object defining the count of log entries stored in JSON message returned by get-log-property*/

/*! USB gadget mode */
enum com_USBGadgetMode {
	COM_USBGADGET_DEFAULT      = 0, /*!< DEPRECATED!!! Default gadget mode defined by the platform software group (in fact it defaults to serial gadget) */
	COM_USBGADGET_SERIAL       = 1, /*!< Serial over USB */
	COM_USBGADGET_ETH_RNDIS    = 2, /*!< Ethernet over USB using RNDIS protocol to work with Microsoft Windows peers*/
	COM_USBGADGET_SERIAL_RNDIS = 3, /*!< Serial and Ethernet (RNDIS) over USB */
	COM_USBGADGET_SERIAL_ECM   = 4, /*!< Serial and Ethernet (ECM) over USB [Raptor only] */
	COM_USBGADGET_ETH_ECM      = 5, /*!< Ethernet over USB using ECM protocol to work with other Verifone terminals as peer and other ECM compatible devices (Windows isn't!) [Raptor only]*/
	COM_USBGADGET_ETH_ECMx2    = 6, /*!< Two Ethernet over USB channels using ECM protocol to work with other Verifone terminals as peer and other ECM compatible devices (Windows isn't!) [Raptor only]*/
	COM_USBGADGET_SERIAL_ECMx2 = 7  /*!< One Serial and two Ethernet (ECM) over USB channels to work with other Verifone terminals as peer and other ECM compatible devices (Windows isn't!) [Raptor only]*/
};

struct com_USBInfo {
	struct com_USBInfo *next;   /*!< Pointer to next USB device info element or NULL if end of list is reached */
	char manufacturer[256 + 1]; /*!< Name of the manufacturer */
	char product[256 + 1];      /*!< Name of the product */
	char serial[256 + 1];       /*!< USB device serial number */
	char idVendor[4 + 1];       /*!< Vendor ID (Assigned by USB Org) */
	char idProduct[4 + 1];      /*!< Product ID (Assigned by Manufacturer) */
	char bcddevice[4 + 1];      /*!< Device Release Number */
	char ttyDevice[16 + 1];     /*!< currently assigned tty device or zero-length string if no tty device */
};

enum com_USB_Mode {
	COM_USB_MODE_DEFAULT = 1, /*!< Default USB mode as defined by the Operating system */
	COM_USB_MODE_HOST    = 2, /*!< USB Host mode */
	COM_USB_MODE_DEVICE  = 3  /*!< USB Device mode */
};

enum com_SimSlot {
	COM_SIM_SLOT_CURRENT = 0, /*!< The currently active SIM-Slot */
	COM_SIM_SLOT_1       = 1, /*!< The first SIM-Slot */
	COM_SIM_SLOT_2       = 2  /*!< The second SIM-Slot */
};

struct com_MAC {
	char MAC[COM_MAC_ADDR_LENGTH]; /*!< Human-readable MAC address. Example: 01:23:45:67:89:ab*/
};

enum com_BeaconPowerLevel {
	COM_BEACON_POWER_LEVEL_ULTRA_LOW = 0,
	COM_BEACON_POWER_LEVEL_LOW       = 1,
	COM_BEACON_POWER_LEVEL_MEDIUM    = 2,
	COM_BEACON_POWER_LEVEL_HIGH      = 3
};

enum com_BeaconURLPrefix {
	COM_BEACON_URL_PREFIX_HTTP_WWW  = 0, /*!< Prefix will be http://www. */
	COM_BEACON_URL_PREFIX_HTTPS_WWW = 1, /*!< Prefix will be https://www. */
	COM_BEACON_URL_PREFIX_HTTP      = 2, /*!< Prefix will be http:// */
	COM_BEACON_URL_PREFIX_HTTPS     = 3  /*!< Prefix will be https:// */
};

struct com_EddystoneUID {
	enum com_BeaconPowerLevel powerLevel; /*!< Power level of the beacon. please refer to \c com_BeaconPowerLevel */
	unsigned int interval;                /*!< Interval in ms. Can range from 100 to 5000 ms (5 sec) */
	unsigned char namesp[10];             /*!< Namespace to advertise. Note that this does not need to be zero-terminated */
	unsigned char instance[6];            /*!< Instance to advertise. Note that this does not need to be zero-terminated */
};

struct com_EddystoneURL {
	enum com_BeaconPowerLevel powerLevel; /*!< Power level of the beacon. please refer to \c com_BeaconPowerLevel*/
	int interval;                         /*!< Interval in ms. Can range from 100 to 5000 ms (5 sec).*/
	enum com_BeaconURLPrefix url_prefix;  /*!< URL Prefix. Please refer to \c com_BeaconPrefix */
	char url[18];                         /*!< URL to advertise. Will be appended after the prefix. Must be null-terminated*/
};

struct com_BLEAttribute {
	short id;                /* handle ID of the characteristic which is read or to be written (See "handle" attribute in GATT-Server XML)*/
	size_t size;             /* Size of the data which is read or is to be written or was read*/
	unsigned char data[512]; /* Data. The data from 0 to 512 bytes. 512 bytes is the maximum length of one attribute. The amount of data used depends on size*/
	void *ext;               /* May be used in future for additional information. Reserved. Must be set by the application to 0*/
};

enum com_BLEAttributeEvent {
	COM_BLE_ATTR_EVENT_NOTIFY     = 0,
	COM_BLE_ATTR_EVENT_INDICATE   = 1,
	COM_BLE_ATTR_EVENT_CONNECT    = 2,
	COM_BLE_ATTR_EVENT_DISCONNECT = 3
};

enum com_BLEStatusEvent {
	COM_BLE_STATUS_EVENT_CONNECT    = 2,
	COM_BLE_STATUS_EVENT_DISCONNECT = 3
};

/*!< Hardware device / module to be powered / unpowered */
enum com_Device {
	COM_DEVICE_NONE   = 0, /*!< None */
	COM_DEVICE_WIFI   = 1, /*!< WiFi / WLAN */
	COM_DEVICE_BT     = 2, /*!< Bluetooth */
	COM_DEVICE_MOBILE = 3  /*!< Mobile data, 2G/3G/4G */
};

/*!< Hardware module Power modes */
enum com_DevicePowerMode {
	COM_DEVICE_POWER_OFF    = 0, /*!< Hardware OFF. For mobile data (2G, 3G, 4G) the module is put into airplane mode if supported */
	COM_DEVICE_POWER_ON     = 1, /*!< Hardware ON / leave airplane mode */
	COM_DEVICE_POWER_HW_OFF = 2  /*!< Hardware OFF. Same as COM_DEVICE_POWER_OFF, even airplane supporting mobile data modules are powered off. */
};

/*!< Network mode in which the base stations wired network should operate (routed vs. bridged) */
enum com_BaseNetworkMode {
	COM_BASE_NET_ROUTER, /*!< base station network mode: routed interfaces*/
	COM_BASE_NET_BRIDGE  /*!< base station network mode: bridged interfaces*/
};

/*!< Structure keeping the signal quality values of a certain interface */
struct com_WirelessSignalQuality {
	int percentage;
	int rssi;
	int dbm;
};

/*! Connection Callback. Is specific to each connection*/
typedef void (*comCallbackConnectionType)(enum com_ConnectionEvent event, enum com_ConnectionType type, const void *data, void *priv, enum com_ErrorCodes com_errno);

/*! Network Callback. There is one instance of this running if the callback is set by the application*/
typedef void (*comCallbackNetworkType)(enum com_NetworkEvent event, enum com_NetworkType type, const void *data, void *priv, enum com_ErrorCodes com_errno);

/*! Wireless Scan Callback. Specific to each asynchronous scan */
typedef void (*comCallbackWirelessScanType)(enum com_WirelessScanEvent event, enum com_WirelessNetworkType type, enum com_DataFormat format, const void *data, void *priv, enum com_ErrorCodes com_errno);

/*! GATT characteristic change callback. Will be called when the characteristic changes */
typedef void (*comCallbackGATTCharacteristicChanged)(enum com_BLEAttributeEvent eventType, struct com_BLEAttribute *attribute, void *priv, enum com_ErrorCodes com_errno);

/*! GATT Connect change callback. Will be called when the a client connects or disconnects */
typedef void (*comCallbackGATTStatus)(enum com_BLEStatusEvent eventType, struct com_MAC mac, void *priv, enum com_ErrorCodes com_errno);

struct com_ConnectHandle;
typedef struct com_ConnectHandle com_NetworkHandle;

/*! Initialize the COM ADK. Needs to be called once in order to use the COM ADK API functions.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_Init(enum com_ErrorCodes *com_errno);

/*! Close the COM ADK. Needs to be called before the main thread has exited by the application.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_Destroy(enum com_ErrorCodes *com_errno);

/*! blocking connection establishment.  The caller is informed about connection specific events via callback function.
* @param[in] connectionProfile name of the connection profile
* @param[in] callback is the function that will be called when an event is generated. If the pointer is NULL no events will be generated.
* @param[in] priv is application dependent data which will be passed to the callback-function. Applications may use this to pass connection specific information for their own use.
* @param[in] timeout_msec timeout in msec for blocking call to this function.
* @param[out] com_errno Error code
* @return returns connection handle or NULL in case of error
**/
DllSpec struct com_ConnectHandle *com_Connect(const char *connectionProfile, comCallbackConnectionType callback, void *priv, int timeout_msec, enum com_ErrorCodes *com_errno);

/** start asynchronous connection establishment. The caller is informed about connection specific events via callback function.
* @param[in] connectionProfile name of the connection profile
* @param[in] callback is the function that will be called when an event is generated. If the pointer is NULL no events will be generated.
* @param[in] priv is application dependent data which will be passed to the callback-function. Applications may use this to pass connection specific information for their own use.
* @param[out] com_errno Error code
* @return returns connection handle or NULL in case of error
**/
DllSpec struct com_ConnectHandle *com_ConnectAsync(const char *connectionProfile, comCallbackConnectionType callback, void *priv, enum com_ErrorCodes *com_errno);

/** wait for async operation to complete
* @param[in] handle handle returned by com_Connect*
* @param[in] timeout_msec timeout in msec for blocking call to this function
* @param[out] com_errno Error code
* @return see \c com_WaitStatus for further details
**/
DllSpec enum com_WaitStatus com_ConnectWait(struct com_ConnectHandle *handle, unsigned int timeout_msec, enum com_ErrorCodes *com_errno);

/** wait for async network connection establishment to complete
* @param[in] handle handle returned by com_*Network
* @param[in] timeout_msec timeout in msec for blocking call to this function
* @param[out] com_errno Error code
* @return returns OK in case of connection established,
*         returns TIMEOUT if wait timeout for this function expired
*         returns ERROR
**/
DllSpec enum com_WaitNetStatus com_NetworkWait(com_NetworkHandle *handle, unsigned int timeout_msec, enum com_ErrorCodes *com_errno);

/** get the file descriptor for the connection
* @param[in] handle handle returned by com_Connect*
* @param[out] com_errno Error code
* @return returns file descriptor (socket handle) \n
*         returns -1 if handle does not refer to a plain TCP or UDP connection
**/
DllSpec int com_ConnectGetFD(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** get the file descriptor for the SSL-connection
* @param[in] handle handle returned by com_Connect*
* @param[out] com_errno Error code
* @return returns SSL-file descriptor (socket handle). This is the plain non-encrypted tcp-socket.
*         returns -1 if handle does not refer to a SSL connection
**/
DllSpec int com_ConnectGetSslFD(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** get the SSL object for the current SSL connection
* @param[in] handle handle returned by com_Connect*
* @param[out] com_errno Error code
* @return returns SSL object or NULL if no SSL connection established.
*         The returned void pointer should be cast to SSL*
**/
DllSpec void *com_ConnectGetSslObj(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** closes the connection handle. Can be called at any time if a valid handle is provided.
* Once the handle has been closed the handle is invalid and must not be used.
* The result of further usage of this handle is undefined.
* @param[in] handle handle of the connection profile which should be closed
* @param[out] com_errno Error code
**/
DllSpec int com_ConnectClose(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** closes the network handle. Can be called at any time if a valid handle is provided.
* Once the handle has been closed the handle is invalid and should not be used.
* Further usage will result in undefined behaviour.
* @param[in] handle handle of the network profile which should be closed
* @param[out] com_errno Error code
**/
DllSpec int com_CloseNetworkHandle(com_NetworkHandle *handle, enum com_ErrorCodes *com_errno);

/** get the type of the network connection
* @param[in] handle handle returned by com_Connect*
* @param[out] com_errno Error code
* @return returns connection type of handle, COM_CONNECTION_TYPE_NONE in case of error
**/
DllSpec enum com_ConnectionType com_GetConnectionType(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** attach network (start the network if required, increase attach usage count)
* @param[in] networkProfile name of the network profile
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_AttachNetwork(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** attach network asynchroniously (start the network if required, increase attach usage count)
* @param[in] networkProfile name of the network profile
* @param[out] com_errno Error code
* @return returns a network-handle if ok, NULL in case of error
**/
DllSpec com_NetworkHandle *com_AttachNetworkAsync(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** detach network (stop the network if connection & network usage count drops to 0 and used network is ON-DEMAND)
* @param[in] networkProfile name of the network profile
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_DetachNetwork(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** detach network asynchronously (stop the network if connection & network usage count drops to 0 and used network is ON-DEMAND)
* @param[in] networkProfile name of the network profile
* @param[out] com_errno Error code
* @return returns a network-handle if ok, NULL in case of error.
**/
DllSpec com_NetworkHandle *com_DetachNetworkAsync(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** get the network status
* @param[in] networkProfile name of the network profile
* @param[out] com_errno Error code
* @return returns a com_NetworkStatus value.
* If the value is COM_NETWORK_STATUS_ERROR check com_errno for the reason
**/
DllSpec enum com_NetworkStatus com_GetNetworkStatus(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** get basic network information referenced by a network profile
* @param[in] networkProfile name of the network profile
* @param[in] ipconfig pointer to memory location where IP configuration information will be stored
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetNetworkInfo(const char *networkProfile, struct com_IPConfig *ipconfig, enum com_ErrorCodes *com_errno);

/** get advanced network information referenced by an interface
* @param[in] interface interface number you want to query
* @param[in] netinfo pointer to memory location where network information will be stored
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
* @note  Use the associated free function to release memory if no longer required
**/
DllSpec int com_GetNetworkInterfaceInfo(enum com_Interfaces interface, struct com_NetworkInterfaceInfo **netinfo, enum com_ErrorCodes *com_errno);

/** Free up memory reserved for a com_NetworkInterfaceInfo structure
* @param[in] netinfo pointer to memory location where network information is stored
* @return nothing
**/
DllSpec void com_NetworkInterfaceInfo_Free(struct com_NetworkInterfaceInfo *netinfo);

/** Free up memory reserved for a com_NetworkInterfaceInfo structure
* @param[in] netinfo pointer to memory location where network information is stored
* @return nothing
**/
DllSpec void com_NetworkInterfaceInfo_Free(struct com_NetworkInterfaceInfo *netinfo);

/** get integer-type device property
* @param[in] intProperty name of the integer-type property
* @param[out] value pointer to integer value of the property
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetDevicePropertyInt(enum com_DevicePropertyInt intProperty, int *value, enum com_ErrorCodes *com_errno);

/** get string-type device property
* @param[in] stringProperty name of the string-type property
* @param[in] buf pointer to char buffer for the property value
* @param[in] len length of the char buffer
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetDevicePropertyString(enum com_DevicePropertyString stringProperty, char *buf, int len, enum com_ErrorCodes *com_errno);

/** set integer-type device property
* @param[in] intProperty name of the integer-type property
* @param[in] value value of the property
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_SetDevicePropertyInt(enum com_DevicePropertyInt intProperty, int value, enum com_ErrorCodes *com_errno);

/** set string-type device property
* @param[in] stringProperty name of the string-type property
* @param[in] buf pointer to char buffer for the property value
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_SetDevicePropertyString(enum com_DevicePropertyString stringProperty, const char *buf, enum com_ErrorCodes *com_errno);

/** get integer-type profile property. Must be called from a connection callback function.
* @param[in] data data object obtained from the connection-callback function
* @param[in] type type of the object. Also obtained from the connection-callback function
* @param[in] intProperty name of the integer-type property
* @param[out] result pointer to integer value of the property.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetProfilePropertyInt(const void *data, enum com_ConnectionType type, enum com_ProfilePropertyInt intProperty, int *result, enum com_ErrorCodes *com_errno);

/** get string-type profile property. Must be called from a connection callback function.
* @param[in] data data object obtained from the connection-callback function
* @param[in] type type of the object. Also obtained from the connection-callback function
* @param[in] stringProperty name of the string-type property
* @param[in] result pointer to char buffer for the property value (read-only).
*            The result may be NULL or an empty string wit no com_errno set. This means there was no error
*            but the application has specified no or an empty string inside the profile.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetProfilePropertyString(const void *data, enum com_ConnectionType type, enum com_ProfilePropertyString stringProperty, const char **result, enum com_ErrorCodes *com_errno);

/** get integer-type profile property
* @param[in] data data object obtained from the connection-callback function
* @param[in] type type of the object. Also obtained from the connection-callback function
* @param[in] intProperty name of the integer-type property
* @param[in] which number of the node to be accessed. Varies depending on the network-type.
* @param[out] result pointer to integer value of the property.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetProfileNodePropertyInt(const void *data, enum com_ConnectionType type, enum com_ProfilePropertyInt intProperty, unsigned int which, int *result, enum com_ErrorCodes *com_errno);

/** get string-type profile property
* @param[in] data data object obtained from the connection-callback function
* @param[in] type type of the object. Also obtained from the connection-callback function
* @param[in] stringProperty name of the string-type property
* @param[in] which number of the node to be accesed. Varies depending on the network-type.
* @param[in] result pointer to char buffer for the property value (read-only).
*            The result may be NULL or an empty string wit no com_errno set. This means there was no error
*            but the application has specified no or an empty string inside the profile.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetProfileNodePropertyString(const void *data, enum com_ConnectionType type, enum com_ProfilePropertyString stringProperty, unsigned int which, const char **result, enum com_ErrorCodes *com_errno);

/** Restart a network to apply changed network parameters
* @param[in] networkProfile Name of the network profile which should be restarted
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_NetworkRestart(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** Asynchroniously restart a network to apply changed network parameters
* @param[in] networkProfile Name of the network profile which should be restarted
* @param[out] com_errno Error code
* @return returns a handle if ok, NULL in case of error
**/
DllSpec com_NetworkHandle *com_RestartNetworkAsync(const char *networkProfile, enum com_ErrorCodes *com_errno);

/** receive data. This operation is non-blocking.
* @param[in] handle handle returned by com_Connect*
* @param[out] buffer buffer for receiving data read from the link
* @param[in] size size of buffer into which will be read. Must be non-zero.
* @param[out] com_errno Error code
* @return On success: the number of bytes read is returned (zero indicates EOF). com_errno must be ignored.
* On error: -1 is returned and com_errno ist accordingly.
* @note If -1 is returned, the return value com_errno can have a value greater than COM_SYSTEM_ERRNO_BASE which indicates a system error.
* In this case com_errno substracted by COM_SYSTEM_ERRNO_BASE will equal the system-errno.
* If the system-errno is EAGAIN or EWOULDBLOCK then there is no data available. Try again later.
* @note It is not an error if the returned number is smaller than the number of bytes requested;
* this may happen for example because fewer bytes are actually available right now (maybe because we were close
* to end-of-file, or because we are reading from a pipe, or from a terminal), or because read() was interrupted
* by a signal.
* @note Websockets only: since websockets are message based each call to com_Receive will return a complete message. The internal maximum
* message length was set to 8192 bytes, so the caller must make sure the buffer has at least that capacity otherwise messages may be truncated.
* 
*
* com_Receive() may return -1 with com_errno set to COM_ERR_CON_SOCKET if the socket associated with the handle is a UDP socket.
* In that case you should use the according function com_ReceiveFrom() instead.
*/
DllSpec ssize_t com_Receive(struct com_ConnectHandle *handle, void *buffer, size_t size, enum com_ErrorCodes *com_errno);

/** receive data. This operation is non-blocking.
* @param[in] handle handle returned by com_Connect*
* @param[in] message Points to the buffer where the message should be stored
* @param[in] length Specifies the length in bytes of the buffer pointed to by the buffer argument. Must be non-zero.
* @param[out] addr_type Address family of the specified dest_addr. If NULL, no data is returned
* @param[out] addr IP Address of the host we received the datagram from. Length depends on the addr_type and therefore the buffer must be at least COM_IPV6_ADDR_LENGTH. If NULL, no data is returned
* @param[out] addr_length Length of the buffer that addr points at. Must be at least COM_IPV6_ADDR_LENGTH. If addr != NULL addr_length must be specified as well.
* @param[out] port UDP Port of the remote peer. If NULL, no data is returned
* @param[out] com_errno Error code
* @return On success: the number of bytes read is returned (zero indicates EOF). com_errno must be ignored.
* On error: -1 is returned and com_errno ist accordingly.
* @note If -1 is returned, the return value com_errno can have a value greater than COM_SYSTEM_ERRNO_BASE which indicates a system error.
* In this case com_errno substracted by COM_SYSTEM_ERRNO_BASE will equal the system-errno.
* If the system-errno is EAGAIN or EWOULDBLOCK then there is no data available. Try again later.
* @note Upon  successful  completion, com_ReceiveFrom() shall return the length of the message in bytes. If no messages are available to be received and the peer has performed an orderly shutdown, com_ReceiveFrom() shall return 0. Otherwise, the function shall return −1 and set com_errno to indicate the error.
*/
DllSpec ssize_t com_ReceiveFrom(struct com_ConnectHandle *handle, void *message, size_t length, enum com_AddressFamily *addr_type, char *addr, size_t addr_length, unsigned short *port, enum com_ErrorCodes *com_errno);

/** send data. This operation is non-blocking.
* @param[in] handle handle returned by com_Connect*
* @param[in] buffer buffer for sending data
* @param[in] size number of bytes to send. Must be non-zero.
* @param[out] com_errno Error code
* @return On success: the number of bytes written is returned. com_errno must be ignored.
* On error: -1 is returned and com_errno ist accordingly.
* @note If -1 is returned, the return value com_errno can have a value greater than COM_SYSTEM_ERRNO_BASE which indicates a system error.
* In this case com_errno substracted by COM_SYSTEM_ERRNO_BASE will equal the system-errno.
* If the system errno is EAGAIN or EWOULDBLOCK then the system-buffer is full. Try again later.
* If the application has written to a socket, which is closed by the remote-host, the system errno is set to EPIPE.
* However, it is not guaranteed that an error will always be reported here. For a reliable way of detecting a closed socket, please refer to the manual.
* No EPIPE signal will be raised.
* @note It is not an error if the number of written bytes is smaller than the number of bytes requested;
* this may happen for example because the system-buffer is full or because write () was interrupted by a signal.
*
* com_Send() may return -1 with com_errno set to COM_ERR_CON_SOCKET if the socket associated with the handle is a UDP socket.
* In that case you should use the according function com_SendTo() instead.
*/
DllSpec ssize_t com_Send(struct com_ConnectHandle *handle, const void *buffer, size_t size, enum com_ErrorCodes *com_errno);

/** UDP only: send datagram to specified host. This operation is non-blocking.
* @param[in] handle handle returned by com_Connect*
* @param[in] message buffer the datagram to be sent
* @param[in] length number of bytes to send. Must be non-zero.
* @param[in] dest_addr Destination address of the host
* @param[in] dest_port Destination UDP port
* @param[out] com_errno Error code
* @return On success: the number of bytes written is returned. com_errno must be ignored.
* On error: -1 is returned and com_errno ist accordingly.
* @note If -1 is returned, the return value com_errno can have a value greater than COM_SYSTEM_ERRNO_BASE which indicates a system error.
* In this case com_errno substracted by COM_SYSTEM_ERRNO_BASE will equal the system-errno.
* If the system errno is EAGAIN or EWOULDBLOCK then the system-buffer is full. Try again later.
* If the application has written to a socket, which is closed by the remote-host, the system errno is set to EPIPE.
* No EPIPE signal will be raised.
* @note It is not an error if the number of written bytes is smaller than the number of bytes requested;
* this may happen for example because the system-buffer is full or because write () was interrupted by a signal.
*/
DllSpec ssize_t com_SendTo(struct com_ConnectHandle *handle, const void *message, size_t length, const char *dest_addr, unsigned short dest_port, enum com_ErrorCodes *com_errno);

/** Connection based connections only: Check state of remote.
* @param[in] handle handle returned by com_Connect*
* @return State of the specified connection
*/
DllSpec enum com_ConnectionState com_GetConnectionState(struct com_ConnectHandle *handle);

/** register global callback function for network events
* @param[in] callback is the function that will be called when an event is generated. If the pointer is NULL no events will be generated.
* @param[in] priv is application dependent data which will be passed to the callback-function. Applications may use this to pass callback specific information for their own use.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_NetworkSetCallback(comCallbackNetworkType callback, void *priv, enum com_ErrorCodes *com_errno);

/** returns a zero-terminated string with a desciption matching the input com_errno
  * @param[in] com_errno_in the errno of which you want the desciption
  * @return description of com_errno_in*/
DllSpec const char *com_GetErrorString(enum com_ErrorCodes com_errno_in);

/** Asks the cache to release all held entries. Note that releasing the entries
 * from the cache does not necessarily mean that all memory is released.
 * If there are currently com-operations in progress it may be that some
 * entries are still used and therefore will only be released once these operations
 * are complete.
 * @return returns 0 if ok, -1 in case of error*/
DllSpec int com_purgeCache(enum com_ErrorCodes *com_errno);

/** returns a zero-terminated string with version and build information of libvficom
 * Format: [major].[minor].[patch]-[build]
 * @return version string */
DllSpec const char *com_GetVersion(void);

/** returns a zero-terminated string with version and build information of the remote com service
 * Format: [major].[minor].[patch]-[build]
 * @return version string, NULL if version cannot be determined*/
DllSpec const char *com_GetSvcVersion(void);

/** Set the requested password
* @param[in] pwtype Type of password
* @param[in] data data object obtained from the connection-callback function
* @param[in] password Password string
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_SetPassword(enum com_PasswordType pwtype, const void *data, const char *password, enum com_ErrorCodes *com_errno);

/** Pair with the specified Bluetooth Device
* If this device is a PCI4+ device this function cannot complete the pairing will fail after a short while.
* @param[in] bt_addr Bluetooth device address of the Device. Must have the format AA:BB:CC:DD:EE:FF.
* @param[in] method Pairing Method used.
* @param[in] PIN Optional PIN string. Required if pairing method is COM_BT_PAIR_SSP_PASSKEY or COM_BT_PAIR_LEGACY
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_BTPair(const char *bt_addr, enum com_BTPairMethod method, const char *PIN, enum com_ErrorCodes *com_errno);

/** Unpair with the specified Bluetooth Device and delete it from the list of known devices
* @param[in]  bt_addr Bluetooth device address of the Device. Must have the format AA:BB:CC:DD:EE:FF.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_BTUnPair(const char *bt_addr, enum com_ErrorCodes *com_errno);

/** Return the scan result for one of supported wireless network types.
 * Writes a JSON encoded string containing scan information into the buffer buf.
 * Type COM_WIRELESS_TYPE_WLAN contains following JSON string members (x is the number of found networks, starts with '0')
 * 		WLANx_SSID
 *		WLANx_BSSID
 *		WLANx_BSS_TYPE
 * 		WLANx_PAIRWISE
 *		WLANx_GROUP
 *		WLANx_KEY_MGMT
 * 		WLANx_DBM
 *		WLANx_PERCENTAGE
 *		WLANx_FREQ
 *		WLANx_PROTO
 * The count of found WiFi networks can be retrieved using the JSON member "WLAN_COUNT". See define COM_WLAN_COUNT.
 * Type COM_WIRELESS_TYPE_BT contains following JSON string members (x is the number of found networks, starts with '0')
 * 		BTx_ADDR
 *		BTx_NAME
 * The count of found Bluetooth networks can be retrieved using the JSON member "BT_COUNT". See define COM_BT_COUNT.
 * Type COM_WIRELESS_TYPE_GSM contains following JSON string members (x is the number of found networks, starts with '0')
 * 		GSMx_NAME
 *		GSMx_MCCMNC
 * The count of found mobile networks can be retrieved using the JSON member "GSM_COUNT". See define COM_GSM_COUNT.
 * Type COM_WIRELESS_TYPE_BT_SERVICES and COM_WIRELESS_TYPE_BT_PAIRED_SERVICES contain following JSON string members (x is the number of found networks, starts with '0')
 * 		BTx_NAME
 * 		BTx_ADDR
 *		BTx_SUPP_PROF (Please see enum com_BTProfileMask to interpret this value)
 * The count of found BT networks can be retrieved using the JSON member "BT_COUNT". See define COM_BT_COUNT.
 * Type COM_WIRELESS_TYPE_BT_SERVICES_EXT_SPP and COM_WIRELESS_TYPE_BT_PAIRED_SERVICES_EXT_SPP contain following JSON string members (x is the number of found networks, starts with '0')
 * 		BTx_NAME
 * 		BTx_ADDR
 *		BTx_CHANNEL (integer value)
 *		BTx_CHANNEL_NAME (String description of this channel -- Can be used to select the right service)
 * The count of found BT networks can be retrieved using the JSON member "BT_COUNT". See define COM_BT_COUNT.
 * Note COM_WIRELESS_TYPE_BT_PAIRED_SERVICES_EXT_SPP may return the same address mutiple times,
 * as one device may have several SPP ports!
 * The BTx_CHANNEL number is the one you need to write into your SPP profile ("channel" tag)
 *
* @param[out] type Type of scan
* @param[out] buf Pointer to char buffer for the scan results.
* @param[in]  len Length of the char buffer
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_WirelessScan(enum com_WirelessNetworkType type, char *buf, unsigned int len, enum com_ErrorCodes *com_errno);

/** Trigger asynchronous wireless scan for one of the supported wireless network types.
* @param[in]  type wireless network type to scan
* @param[in]  callback is the function that will be called when an event is generated. The scan will trigger event COM_EVENT_WIRELESS_SCAN_SUCCESS or COM_EVENT_WIRELESS_SCAN_FAILURE on completion
* @param[in]  format specifiy the requested output format. See enum com_DataFormat for details.
* @param[in]  priv is application dependent data which will be passed to the callback-function. Applications may use this to pass callback specific information for their own use.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 if case of error
*/
DllSpec int com_WirelessScanAsync(enum com_WirelessNetworkType type, comCallbackWirelessScanType callback, enum com_DataFormat format, void *priv, enum com_ErrorCodes *com_errno);

/** Ping the specified host. Returns when either an error has occured, the timeout has expired, or count has been reached.
* @param[in] addr Address of the host
* @param[in] interface network interface via which the ICMP request packets should be sent
* @param[in] count How many packets to send
* @param[in] timeout Overall timeout in seconds
* @param[out] result Result of the pinging process
* @param[out] com_errno Error code (will be set COM_ERR_NET_NOEXIST if the address family for the hostname is not supported by the terminal)
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_Ping(const char *addr, enum com_Interfaces interface, unsigned int count, unsigned int timeout, struct com_PingInfo *result, enum com_ErrorCodes *com_errno);

/** Add a network or host routing table entry
* @param[in] destination IP string of the destination. Can be either IPv4 or IPv6 format without network suffix
* @param[in] netmask Network mask. For IPV4 this can either be an empty string or NULL pointer (for host routes) or a netmask in IPv4 representation or prefixlen value (for network routes).
             For IPV6 this can be an empty string, NULL pointer or prefix length value
* @param[in] gateway IP string of the gateway. Can be either IPv4 or IPv6 format, or NULL if no gateway needed
* @param[in] interface network interface to be used
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_AddNetworkRoute(const char *destination, const char *netmask, const char *gateway, enum com_Interfaces interface, enum com_ErrorCodes *com_errno);

/** Delete a network or host routing table entry. Be aware that in multi-application environments it may be possible that the same route was added several times.
* Therfore it will be removed only if the last user no longer needs the route (internal usage counters are maintained)
* @param[in] destination IP string of the destination. Can be either IPv4 or IPv6 format with or without network suffix (for example "192.0.0.1" for ipv4 host route or "192.168.1.0/8" for ipv4 network route)
* @param[in] netmask Network mask. For IPV4 this can either be an empty string or NULL pointer (for host routes) or a netmask in IPv4 representation or prefixlen value (for network routes).
             For IPV6 this can be an empty string, NULL pointer or prefix length value
* @param[in] gateway IP string of the gateway. Can be either IPv4 or IPv6 format, or NULL if no gateway needed
* @param[in] interface network interface to be used
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_DeleteNetworkRoute(const char *destination, const char *netmask, const char *gateway, enum com_Interfaces interface, enum com_ErrorCodes *com_errno);

/** Read routing tables for IPv4 and IPv6 from Kernel. Returns either a valid pointer when routing table entries are available or NULL if not.
* The returned pointer is the beginning of a linked list of elements. Each element contains the data of one routing table entry. The member "next" points
* to NULL if the end of the list is reached.
* If you are not interested in one of the routing tables, simply set the pointer to NULL.
* The user is responsible to call com_NetworkRouteInfo_Free() in order to release allocated memory.
* @param[out] rtinfo4 pointer to the routing info list for IPv4 or NULL
* @param[out] rtinfo6 pointer to the routing info list for IPv6 or NULL
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetNetworkRouteInfo(struct com_NetworkRouteInfo_IPv4 **rtinfo4, struct com_NetworkRouteInfo_IPv6 **rtinfo6, enum com_ErrorCodes *com_errno);

/** Free any previously allocated routing information. NULL-pointers are ignored.
* @param[in] rtinfo4 pointer to the routing info list for IPv4 or NULL
* @param[in] rtinfo6 pointer to the routing info list for IPv6 or NULL
* @return void
**/
DllSpec void com_NetworkRouteInfo_Free(struct com_NetworkRouteInfo_IPv4 *rtinfo4, struct com_NetworkRouteInfo_IPv6 *rtinfo6);

/** Wait for an event on a connection handle. Returns when either an error has occured, the timeout has expired, or activity on the handle has occured.
* @param[in] handle handle returned by com_Connect*
* @param[in] flags_in events which should be waited for. You may OR several COM_SELECT_ values to wait for several events
* @param[in] timeout Overall timeout in milliseconds. A negative value specifies an indefinite timeout
* @param[out] flags_out events which occured. Multiple events at once are possible if flags_in has multiple events
* @param[out] com_errno Error code
* @return returns greater or equal 1 if an event specified in flags_in occured. 0 is returned in case of a timeout. -1 is returned in case of error. Check com_errno
* @note if com_errno is greater than COM_SYSTEM_ERRNO_BASE then -1 should have been returned.
* In this case com_errno subtracted by COM_SYSTEM_ERRNO_BASE will equal to the system-errno.
* @note If the connection is a SSL connection, the SSL context has buffered bytes internally,
* and you have specified SELECT_READ in flags_in, then all other flags in flags_in will be ignored
* and only SELECT_READ will be set in flags_out
**/
DllSpec int com_Select(struct com_ConnectHandle *handle, unsigned int flags_in, int timeout, unsigned int *flags_out, enum com_ErrorCodes *com_errno);

/** Interrupts any com_Select which is currently pending for this connection.
* Cause a com_Select to return if the interrupt flag has been requested.
* Any subsequent com_Select calls with this handle will return immediately until com_SelectInterruptReset is called.
*
* @param[in] handle handle returned by com_Connect*
* @param[out] com_errno Error code
* @return returns 0 if the handle is valid
**/
DllSpec int com_SelectInterrupt(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** Clears the interrupt from a connection-handle. The handle must not be in-use during this operation
* @param[in] handle handle returned by com_Connect*
* @param[out] com_errno Error code
* @return returns 0 if the handle is valid
**/
DllSpec int com_SelectInterruptReset(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno);

/** Deprecated -- Do not use
* @param[in] handle handle returned by com_Connect*.  The handle must be connected.
* @param[out] com_errno Error code
* @return always Returns non-zero if an error occured. Check com_errno
* @note this function MUST be called by the thread which originally obtained connection handle. Otherwise the results are undefined.
**/
DllSpec int com_SetMultiThreadingMode(struct com_ConnectHandle *handle, enum com_ErrorCodes *com_errno) ADKCOM_MARK_DEPRECATED("The OS, for which this was necessary, is no longer supported");

/** Deprecated -- Do not use
* @param[in] sppPort BT SPP Port to configure. Value should be either COM_BT_CONFIG_SPP0 or COM_BT_CONFIG_SPP2
* @param[in] portSetting Port setting can either be Server (1) for incoming connections or Client (0) for outgoing connections
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_SetBTSPPPort(enum com_BTConfigSPPPort sppPort, int portSetting, enum com_ErrorCodes *com_errno) ADKCOM_MARK_DEPRECATED("The OS, for which this was necessary, is no longer supported");

/** Read currently registered USB device list. Returns either a valid pointer when USB devices were found or NULL if not.
* The returned pointer is the beginning of a linked list of elements. Each element contains the data of one registered USB device. The member "next" points
* to NULL if the end of the list is reached.
* The user is responsible to call com_USBInfo_Free() in order to release allocated memory.
* @param[out] usbinfo pointer to the USB device list or NULL
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_GetUSBInfo(struct com_USBInfo **usbinfo, enum com_ErrorCodes *com_errno);

/** Free any previously allocated USB device information. NULL-pointers are ignored.
* @param[in] usbinfo pointer to the USB device list
* @return void
**/
DllSpec void com_USBInfo_Free(struct com_USBInfo *usbinfo);

/** Add a network callback. This callback will be maintained by libcom until it is removed by com_NetworkRemoveCallback. This does not modify the callback set by com_NetworkSetCallback.
* You may add the callback one time. Adding a currently present callback again will result in an error (this does not include the callback set with com_NetworkSetCallback).
* @param[in] callback is the function that will be called when an event is generated.
* @param[in] eventsArr Array of network events this callback should receive. If specified the array must have at least 1 member.
* If not specified (NULL pointer) all events will be received by the callback.
* @param[in] count number of network events in the array. If eventsArr is NULL the count must be 0.
* @param[in] priv is application dependent data which will be passed to the callback-function. Applications may use this to pass callback specific information for their own use.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_NetworkAddCallback(comCallbackNetworkType callback, enum com_NetworkEvent eventsArr[], size_t count, void *priv, enum com_ErrorCodes *com_errno);

/** Remove Network Callback. The given network callback must be previously registered using com_NetworkAddCallback. This does not modify the callback set by com_NetworkSetCallback.
* @param[in] callback function to be removed
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_NetworkRemoveCallback(comCallbackNetworkType callback, enum com_ErrorCodes *com_errno);

/** Store a new stunnel configuration on terminal
* @param[in] stunnel_cfg_file File in file system containing the stunnel configuration. If set to NULL all previously stored configurations will be erased and stunnel is stopped. Note that 'run_now' is ignored in that case.
* @param[in] run_now When set to 1 the stunnel is started after the configuration was applied.  When set to 0 the stunnel configuration is only stored in the OS
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_SetStunnelConfig(const char *stunnel_cfg_file, int run_now, enum com_ErrorCodes *com_errno);

/** Create a bridge between two network interfaces. V/OS Raptor/Trident only.
* @param[in] iface1 Interface to be bridged with iface2
* @param[in] iface2 Interface to be bridged with iface1
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_AddInterfaceBridge(enum com_Interfaces iface1, enum com_Interfaces iface2, enum com_ErrorCodes *com_errno);

/** Remove a bridge between two network interfaces. V/OS Raptor/Trident only
* @param[in] iface1 Interface which is part of the bridge to be removed
* @param[in] iface2 Interface which is part of the bridge to be removed
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_RemoveInterfaceBridge(enum com_Interfaces iface1, enum com_Interfaces iface2, enum com_ErrorCodes *com_errno);

/** Pair a docked device
 * Handset must be docked onto base while running this API. Otherwise error COM_ERR_NOT_DOCKED is returned.
* @param[out] mac MAC address of the device which we have paired with
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_PairDockedBase(struct com_MAC *mac, enum com_ErrorCodes *com_errno);

/** Change priority of interfaces. The priority of the interfaces will be decided by the order.
* Interfaces in front will be of higher order than interfaces in the back.
* Passing a null pointer with count 0 will reset the interface-priority to the default values.
* @param[in] interfaces List of interfaces
* @param[in] count Count of interfaces
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_SetInterfacePriority(enum com_Interfaces interfaces[], size_t count, enum com_ErrorCodes *com_errno);

/** Set a specified eddystone UID as advertisement. This will stop a currently running UID advertisement. Please note that the beacon will not be started.
* @param[in] eddystoneUID UID to advertise
* @param[in] startupMode Specifies if the beacon should automatically be enabled at startup of the terminal
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneUIDSetAdvertisement(struct com_EddystoneUID eddystoneUID, enum com_StartupMode startupMode, enum com_ErrorCodes *com_errno);

/** Set a specified eddystone URL as advertisement. This will stop a currently running URL advertisement.  Please note that the beacon will not be started.
* @param[in] eddystoneURL URL to advertise
* @param[in] startupMode Specifies if the beacon should automatically be enabled at startup of the terminal
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneURLSetAdvertisement(struct com_EddystoneURL eddystoneURL, enum com_StartupMode startupMode, enum com_ErrorCodes *com_errno);

/** Start eddystone-UID advertisement. If an eddystone-UID beacon is currently being advertised, then no action will be performed.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneUIDStartAdvertisement(enum com_ErrorCodes *com_errno);

/** Start eddystone-URL advertisement. If an eddystone-URL beacon is currently being advertised, then no action will be performed.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneURLStartAdvertisement(enum com_ErrorCodes *com_errno);

/** Obtain current eddystone UID-advertisement.
* @param[out] eddystoneUID Pointer to struct which should be filled with advertisement data.
* @param[out] startupMode Autostart mode currently configured
* @param[out] isRunning Is the beacon currently advertising? Set to 0 if not running, to 1 if running
* @param[out] com_errno Error code.
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneUIDGetAdvertisement(struct com_EddystoneUID *eddystoneUID, enum com_StartupMode *startupMode, int *isRunning, enum com_ErrorCodes *com_errno);

/** Obtain current eddystone URL-advertisement.
* @param[out] eddystoneURL Pointer to struct which should be filled with advertisement data.
* @param[out] startupMode Autostart mode currently configured
* @param[out] isRunning Is the beacon currently advertising? Set to 0 if not running, to 1 if running
* @param[out] com_errno Error code.
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneURLGetAdvertisement(struct com_EddystoneURL *eddystoneURL, enum com_StartupMode *startupMode, int *isRunning, enum com_ErrorCodes *com_errno);

/** Stop the currently running eddystone UID advertisement. If it is already stopped, then no action will be performed.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneUIDStopAdvertisement(enum com_ErrorCodes *com_errno);

/** Stop the currently running eddystone URL advertisement. If it is already stopped, then no action will be performed.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_EddystoneURLStopAdvertisement(enum com_ErrorCodes *com_errno);

/** Sets bluetooth discovery on. If the discovery is already running no action will be performed.
* This will use the extended method of pairing bluetooth devices.
* This function only works if COM_FEATURE2_BT_PAIR_EXT in FEATURE2 is set.
* Please note that the discovery will be turned off once the pairing process has been done once, regardless if it has been successful or not.
* See the full documentation for further information
* @param[in] discovery_timeout_seconds timeout of the discovery operation in seconds. Please note that this timeout refers to the timeout of the discovery, not the timeout of this function itself.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_BTExtStartDiscovery(unsigned int discovery_timeout_seconds, enum com_ErrorCodes *com_errno);

/** Sets bluetooth discovery off. If the discovery is not running, then no action will be performed.
* This will use the extended method of pairing bluetooth devices.
* This function only works if COM_FEATURE2_BT_PAIR_EXT in FEATURE2 is set.
* Please note that the discovery will be turned off once the pairing process has been done once, regardless if it has been successful or not.
* See the full documentation for further information
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_BTExtStopDiscovery(enum com_ErrorCodes *com_errno);

/** Initiate pairing with the the specified device. This function only works if COM_FEATURE2_BT_PAIR_EXT in FEATURE2 is set.
* See the full documentation for further information
* @param[in]  bt_addr Bluetooth device address of the Device. Must have the format AA:BB:CC:DD:EE:FF.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. A return code of 0 does not indicate that pairing is successful, only that the pairing has been initiated.
**/
DllSpec int com_BTExtPair(const char *bt_addr, enum com_ErrorCodes *com_errno);

/** Confirm pairing with remote device. This function only works if COM_FEATURE2_BT_PAIR_EXT in FEATURE2 is set.
* This function should only be called in case the network-event is COM_EVENT_BT_EXT_CONFIRM.
* See the full documentation for further information
* @param[in] yesOrNo The PIN shown on screen has been accepted by the user. Parameter should be 0 in case the user has not accepted, 1 if the user has accepted.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. A return code of 0 does not indicate that pairing is successful, only that the confrimation on this side has been accepted.
**/
DllSpec int com_BTExtConfirm(unsigned int yesOrNo, enum com_ErrorCodes *com_errno);

/** Set pin when pairing with remote device. This function only works if COM_FEATURE2_BT_PAIR_EXT in FEATURE2 is set.
* This function should only be called in case the network-event is COM_EVENT_BT_EXT_PIN.
* See the full documentation for further information
* @param[in] pin The pin which has been entered by the user
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. A return code of 0 does not indicate that pairing is successful, only that the pin has been set.
**/
DllSpec int com_BTExtSetPIN(const char *pin, enum com_ErrorCodes *com_errno);

/** (Re)Start the GATT server. This feature is only active if COM_FEATURE2_BT_GATT is set in the feature-bitset. This reads both the configuration and starts the beaconing.
* If the server was already stated the configuration will be read again.
* @param[in] xmlfile_path Path to the xml-file containing the server information. See full documentation on what this XML should contain. The path must not be relative, but full (absolute).
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. 0 will be returned even if no action is taken.
**/
DllSpec int com_BTGATTStart(const char *xmlfile_path, enum com_ErrorCodes *com_errno);

/** Setup the GATT server. This feature is only active if COM_FEATURE2_BT_GATT_BEACON_CTL is set in the feature-bitset. This does not start beaconing, but only starts the server.
* If the server was already started the configuration will be read again. If you have turned on beaconing previously the beaconing will still be active.
* @param[in] xmlfile_path Path to the xml-file containing the server information. See full documentation on what this XML should contain. The path must not be relative, but full (absolute).
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. 0 will be returned even if no action is taken.
**/
DllSpec int com_BTGATTSetup(const char *xmlfile_path, enum com_ErrorCodes *com_errno);

/** Start GATT Server beaconing. This feature is only active if COM_FEATURE2_BT_GATT_BEACON_CTL is set in the feature-bitset. The server must be running (see functions above). If the beacon is visible no action is taken.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. 0 will be returned even if no action is taken.
**/
DllSpec int com_BTGATTStartBeaconing(enum com_ErrorCodes *com_errno);

/** Stop GATT Server beaconing. This feature is only active if COM_FEATURE2_BT_GATT_BEACON_CTL is set in the feature-bitset. This does not stop the server itself. The server must be running (see functions above). If the beacons are currently off, then no action is taken.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. 0 will be returned even if no action is taken.
**/
DllSpec int com_BTGATTStopBeaconing(enum com_ErrorCodes *com_errno);

/** Stop the GATT server. If the GATT server is not running no action is taken. This feature is only active if COM_FEATURE2_BT_GATT is set in the feature-bitset. This stops beaconing as well as the running server-instance itself.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. 0 will be returned even if no action is taken.
**/
DllSpec int com_BTGATTStop(enum com_ErrorCodes *com_errno);

/** Read GATT Characteristic. Please note the "id" member of attribute will be used to determine which attribute to read.
* Field "size" will be set to the amount which was read and field data will hold "size" amount of data after the operation completes successfully.
* @param[in] attribute Attribute description to be read. See com_BLEAttribute structure for more information.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTGATTReadCharacteristic(struct com_BLEAttribute *attribute, enum com_ErrorCodes *com_errno);

/** Write GATT Characteristic.
* @param[in] attribute Attribute description to be written. See com_BLEAttribute structure for more information.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTGATTWriteCharacteristic(struct com_BLEAttribute *attribute, enum com_ErrorCodes *com_errno);

/** Add callback to be called when a characterstic is changed. When com_Destroy is called all GATT callbacks will be deregistered.
* @param[in] callback Callback to be called when a characterstic is changed
* @param[in] priv Pointer to application data
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTGATTAddCharacteristicCallback(comCallbackGATTCharacteristicChanged callback, void *priv, enum com_ErrorCodes *com_errno);

/** Remove already callback which was registered by com_BTGATTAddCharacteristicCallback
* @param[in] callback Registered callback to be removed
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTGATTRemoveCharacteristicCallback(comCallbackGATTCharacteristicChanged callback, enum com_ErrorCodes *com_errno);

/** Add callback to be called when a BLE Client connects or disconnects. When com_Destroy is called all GATT callbacks will be deregistered.
* @param[in] callback Callback to be called when a characterstic is changed
* @param[in] priv Pointer to application data
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTGATTAddStatusCallback(comCallbackGATTStatus callback, void *priv, enum com_ErrorCodes *com_errno);

/** Remove already callback which was registered by com_BTGATTAddCharacteristicCallback
* @param[in] callback Registered callback to be removed
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTGATTRemoveStatusCallback(comCallbackGATTStatus callback, enum com_ErrorCodes *com_errno);

/** Disconnect a GATT client.
* If this operation is not supported com_errno will be set to COM_ERR_NET_NOSUPPORT
* If the GATT client is already disconnected no error will be returned
* @param[in] bt_address Hardware address to disconnect. Note you will get this address when a GATT-connect occurs
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error. 0 will be returned even if no action is taken.
**/
DllSpec int com_BTGATTDisconnect(const char *bt_address, enum com_ErrorCodes *com_errno);

/** Connect with iAP2/PMRMUX to remote device. Device must be paired beforehand and in range.
* You will have to set this after every boot.
* If you want to change the device which is used, please disconnect virtual channels with com_BTDisableVirtualChannels first.
* @param[in] bt_address Address to connect to
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTEnableVirtualChannels(const char *bt_address, enum com_ErrorCodes *com_errno);

/** Disconnect iAP2/PMRMUX of remote device.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_BTDisableVirtualChannels(enum com_ErrorCodes *com_errno);

/** Obtain the base station network mode. Currently only BT PAN interface is affected.
 * Handset must be docked onto base while running this API. Otherwise error COM_ERR_NOT_DOCKED is returned.
* @param[out] mode current network mode of the base station
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_GetBaseNetworkMode(enum com_BaseNetworkMode *mode, enum com_ErrorCodes *com_errno);

/** Set the base station network mode. Currently only BT PAN interface is affected.
 * Handset must be docked onto base while running this API. Otherwise error COM_ERR_NOT_DOCKED is returned.
 * !!! The base station must be rebooted for the changes to take effect !!!
* @param[out] mode desired network mode of the base station
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_SetBaseNetworkMode(const enum com_BaseNetworkMode mode, enum com_ErrorCodes *com_errno);

/** Obtain the Ethernet link status of a base station
* @param[in] bt_addr Optional parameter that that defines the BT MAC address to be queried. If not specified (NULL) it is assumed the handset is docked to the base and it will determine the Ethernet link state of the currently docked base.
* @param[out] com_errno Error code
* @return returns 0 if Ethernet cable is unplugged, 1 if Ethernet cable is plugged or -1 in case of error.
**/
DllSpec int com_GetBaseEthernetLinkState(const char *bt_addr, enum com_ErrorCodes *com_errno);

/** Set the power mode of the specified hardware module. A module can either be powered or unpowered.
* @param[in] device Type of hardware module
* @param[in] power_mode Power mode to be set for the specified hardware module
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_PowerModeSet(enum com_Device device, enum com_DevicePowerMode power_mode, enum com_ErrorCodes *com_errno);

/** Get the current power mode of the specified hardware module.
* @param[in] device Type of hardware module
* @param[out] power_mode Power mode currently active for the specified hardware moduled
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error
**/
DllSpec int com_PowerModeStatus(enum com_Device device, enum com_DevicePowerMode *power_mode, enum com_ErrorCodes *com_errno);

/** Obtain the signal quality of a specific wireless connection
* Note: This function is only available on Raptor/VOS2 platform. Other platforms will return -1 and set the com_errno to COM_ERR_NET_NOSUPPORT
* @param[in] interface Interface to query (can be one of COM_INTERFACE_WLAN0, COM_INTERFACE_BT or COM_INTERFACE_GPRS0; others will result in error)
* @param[in] input optional input value. Only needed for COM_INTERFACE_BT: string that defines the BT MAC address to be queried. Ignored for other interfaces
* @param[out] signal Pointer to com_WirelessSignalQuality where the signal quality values will be copied to.
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_GetWirelessSignalQuality(enum com_Interfaces interface, const char *input, struct com_WirelessSignalQuality *signal, enum com_ErrorCodes *com_errno);

/** DNS Lookup function
* @param[in] hostname name of the host to be looked up
* @param[in] interface Interface to query. COM_INTERFACE_ANY (default) will use the DNS servers of the default network interface. Any other interface value will take the associated DNS server of that network interface
* @param[in] af The address family to query. COM_AF_INET will return only A DNS records, COM_AF_INET6 will return only AAAA DNS records and COM_AF_UNSPEC will retrun both - A records first
* @param[in] timeout_ms Timeout value of the DNS query in miliseconds
* @param[in] buffer char buffer to store the DNS query result in JSON format
* @param[in] size size of the buffer
* @param[out] com_errno Error code
* @return returns 0 if ok, -1 in case of error.
**/
DllSpec int com_DNSResolveHostname(const char *hostname, enum com_Interfaces interface, enum com_AddressFamily af, unsigned int timeout_ms, char *buffer, const size_t size, enum com_ErrorCodes *com_errno);

#ifdef __cplusplus
} // extern C
#endif

#undef ADKCOM_MARK_DEPRECATED
#undef DllSpec

#endif //_LIBCOM_H_
