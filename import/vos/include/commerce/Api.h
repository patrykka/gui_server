
#ifndef API_H_
#define API_H_

#include <string>
#include <vector>
#include <map>
#include "cpdllspec.h"

namespace vficpl
{
  /** TransactionData container struct for defined return values of Api
    */
  struct TransactionData
  {
    /** type of transaction executed by the payment application */
    std::string transactionType;

    /** amount of transaction */
    std::string transactionAmount;

    /** alphabetic currency code (ISO 4217) */
    std::string currency;

    /** tip value */
    std::string gratuityAmount;

    /** true permits the payment application to obtain card data via manual card
      * entry. false indicates that the manual card entry is not an option.
      */
    bool manualEntry;

    /** Only filled if manualEntry is true.
      *
      * Possible values are
      * - ZIP prompts to enter zipcode
      * - NOCVV disables prompt for CVV
      *
      * If ZIP is specified, the result of the AVS is returned in AVS_Code
      */
    std::vector<std::string> manualPromptOption;

    /** reference of payment application ehich refers to the PAN in question. */
    std::string panHandle;

    /** result of transaction */
    std::string result;

    /** description of error */
    std::string errorDescription;

    /** transaction id provided by the payment gateway, which is a client
      * specific transaction identifier.
      */
    std::string transactionId;

    /** invoice or bill number this transaction relates to */
    std::string invoice;
  };

  enum ApiError
  {
    API_OK,
    API_FAIL,
    API_SERVICE_TIMEOUT,
    API_INVALID_PARAMETERS,
    API_UNSUPPORTED_REQUEST,
    API_ENCRYPTION_UNAVAILABLE,
    API_NO_PAPER
  };

  class CPDllSpec Api
  {
    friend class CommerceData;
    // forward declaration for hidden implementation details
    class ApiPrivate;
    ApiPrivate *apiImpl_;
    // prevent calling copy constructor or assignment operator
    Api(const Api &o);
    void operator=(const Api &o);
  public:
    /** constructor */
    Api();


    /** destructor */
    virtual ~Api();

    /**
     * append receipt is used to establish communication between cp and payment
     * app.
     * @param  receiptType    cardholer or merchant type
     * @param  receiptContent content of cardholder or merchant receipt
     * @return                success indicator true or false;
     */
    virtual bool appendReceipt(std::string receiptType, std::string receiptContent);

    /**
    * API CP_APP_REQUESTS_PRINT
    * Method is called when CP App wants to print a receipt.
    * By default this function calls vfiprt::prtHTML.
    * Can be overwritten by payment application to perform other actions.
    * @param[in] str String to be printed.
    * @return true if printing was ok, otherwise false
    */
    virtual bool printReceipt(std::string str);

    /**
     * is printing allowed
     * @return success indiactor true allowed, false forbidden
     */
    virtual bool isPrintingAllowed();

    /**
    * API CPP_APP_REQUESTS_SEND_DATA_TO_POS
    * Method is called, when CP App wants to send data
    * to Point Of Sale (e.g. ECR).
    * @param[in] data Data to be send
    * @return true to inform CP App, that POS received the data; false otherwise
    */
    virtual bool sendDataToPos(const std::string &data) const;

    /**
    * API CP_APP_REQUESTS_ENCRYPTED_CARD
    * Method is called when CP App requests encrypted card data.
    * By default this function does not encrypt data and returns false.
    * Must be overwritten by payment application for encrypting card data.
    * @param[in] panHandle Handle of PAN to be used for encryption.
    * @return Map filled with encrypted card data
    *         ("Encrypted_PAN" and "Encrypted_PAN_KSN") if ok, otherwise empty map
    */
    virtual std::map<std::string,std::string> encryptCardData(std::string panHandle);

    /**
    * API CP_APP_REQUESTS_CARD_DATA
    * Method is called when CP App wants to get card data.
    * By default this function return an empty map.
    * Can be overwritten by payment application to perform other actions.
    * @param[in] cardType Source for card data. Can be one or more of following values:
    *                     "MAG_STRIPE", "CHIP", "CTLS_CARD", "CTLS_PHONE", "MANUAL"
    * @return map filled with card data if ok, otherwise empty map
    */
    virtual std::map<std::string,std::string> requestCardData(std::vector<std::string> cardType);

    /**
     * API CP_APP_REQUESTS_PAYMENT_TRANSACTION_START
     * @param  txnType     transaction type
     * @param  amount      transaction amount
     * @param  currency    transaction currency
     * @param  manualEntry manual options
     * @param  gratuity    gratuity amount
     * @return success indicator true request approved, false declined
     */
    virtual bool requestStartTransaction(
      std::string &txnType,
      std::string &amount,
      std::string &currency,
      bool manualEntry,
      std::string &gratuity);

    /**
    * API CP_APP_REQUESTS_APPEND_RECEIPT
    * Check if footer string exists, which has to appended to customer receipt.
    * Can be called multiple times, until it returns false.
    * @return true if footer exists, otherwise false
    */
    bool hasNextCustomerReceiptFooter() const;

    /**
    * API CP_APP_REQUESTS_APPEND_RECEIPT
    * Get footer string for customer receipt.
    * @return footer string to be appended to customer receipt
    */
    std::string getNextCustomerReceiptFooter();

    /**
    * API CP_APP_REQUESTS_APPEND_RECEIPT
    * Check if footer string exists, which has to appended to merchant receipt.
    * Can be called multiple times, until it returns false.
    * @return true if footer exists, otherwise false
    */
    bool hasNextMerchantReceiptFooter() const;

    /**
    * API CP_APP_REQUESTS_APPEND_RECEIPT
    * Get footer string for merchant receipt.
    * @return footer string to be appended to merchant receipt
    */
    std::string getNextMerchantReceiptFooter();

    /**
    * API CP_APP_REQUESTS_PAYMENT_TRANSACTION_START
    * Check if CP App requested start of transaction.
    * If true getRequestedTransactionData() will return data.
    * @return true if CP App has called CP_APP_REQUESTS_PAYMENT_TRANSACTION_START.
    */
    bool hasRequestedTransactionStart() const;

    /**
    * API CP_APP_REQUESTS_PAYMENT_TRANSACTION_START
    * Get transaction data previously given by CP App when calling CP_APP_REQUESTS_PAYMENT_TRANSACTION_START.
    * Transaction data can only be read once. After reading transaction data is cleared.
    * @return TransactionData if CP App hasn't requested starting transaction or empty TransactionData
    */
    TransactionData getRequestedTransactionData();

    /**
    * API CP_APP_REQUESTS_POS_INPUT
    * Get POS confirmation flag
    */
    virtual ApiError posGetConfirmation(const std::string& html, int maxseconds, bool& result);

    /**
    * API CP_APP_REQUESTS_POS_INPUT
    * Get POS text input
    */
    virtual ApiError posGetTextInput(const std::string& html, int maxseconds, std::string& result);

    /**
    * API CP_APP_REQUESTS_POS_INPUT
    * Get POS decimal input
    */
    virtual ApiError posGetDecimalInput(const std::string& html, int maxseconds, std::string& result);

    /**
    * API CP_APP_NOTIFIES_POS_DISPLAY
    * Show message on POS display
    */
    virtual void posDisplay(const std::string& html, int minseconds);
  };
} // namespace vficpl
#endif /* API_H_ */
