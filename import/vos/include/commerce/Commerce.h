#ifndef COMMERCE_H_
#define COMMERCE_H_

/* specific includes */
#include "Api.h"
#include "CpTrigger.h"
#include "EnvironmentDataProvider.h"
#include "ITransactionDataProvider.h"
#include "cpdllspec.h"

/* adk includes */
#include "ipc/jsobject.h"

/* common includes */
#include <string>
#include <list>
#include <stdint.h>

namespace vficpl
{
  /**
  * @enum ErrorCode
  * @brief Represents possible error codes of invoke call
  */
  typedef enum
  {
    CP_INVOKE_SUCCESS,                  ///< invoke was successful
    CP_INVOKE_FAILED,                   ///< result of invoke is failed
    CP_INVOKE_TIMEOUT,                  ///< invoke is timed out
    CP_INVOKE_MANDATORY_PARAMS_MISSING, ///< invoke rejected because mandatory parameters are missing
    CP_INVOKE_SERVICE_SUCCESS,          ///< invoke of app type service successful
    CP_INVOKE_NATIVE_SUCCESS,           ///< invoke of app type native successful
    CP_INVOKE_CANCELED                  ///< invoke of app canceled by user, can occur in invokeFromDesktop
  } ErrorCode;

  /**
   * @struct InvokeResult
   * @brief Trigger invocation result
   */
  struct CPDllSpec InvokeResult
  {
    /**
     * Construct from main error code and set application return code to -1
     * @param[in] mec - main error code
     */
    explicit InvokeResult(ErrorCode mec);

    /**
     * Construct from both main error code and application return code
     * @param[in] mec - main error code
     * @param[in] arc - application return code
     */
    InvokeResult(ErrorCode mec, int arc);

    /**
     * Implicit conversion to boolean
     * @return true if both main error code is CP_INVOKE_SUCCESS and
     *         application return code is 0, false otherwise
     */
    operator bool() const;

    ErrorCode mec; ///< CP library main error code
    int arc;       ///< CP application return code
  };

  /**
  * @brief Represents Barcode object on CP-Payment communication.
  */
  class CPDllSpec Barcode
  {
  public:
    /** constructor of Barcode */
    Barcode();

    /** constructor of Barcode filled with jsObj
      * @param jsObj - JSObject with construction informations
      */
    explicit Barcode(const vfiipc::JSObject &jsObj);

    /** default destructor */
    virtual ~Barcode();

    /** content - Data to be encoded as a barcode */
    std::string content;

    /** xDimension - Decimal value representing the base dimension in
      * millimeters as per GS1 specification. Refer to: http://www.gs1.org
      */
    float xDimension;

    /** errorCorrectionLevel - Error correction level applicable to QR codes.
      * Must be one of: L, M, Q, H
      */
    std::string errorCorrectionLevel;

    /** columns - Integer value representing the number of columns for a 2D
      * barcode.
      */
    uint16_t columns;

    /** rows - Integer value representing the number of rows for a 2D barcode.*/
    uint16_t rows;

    /** type - Type of barcode, must be one of:
      * - EAN_13,
      * - UPC_A,
      * - EAN_8,
      * - UPC_E,
      * - DATABAR_OMNI,
      * - DATABAR_TRUNCATED,
      * - DATABAR_STACKED,
      * - DATABAR_STACKED_OMNI,
      * - DATABAR_OMNI_LIMITED,
      * - DATABAR_EXPANDED,
      * - DATABAR_EXPANDED_STACKED,
      * - ITF_14,
      * - GS1_128
      * - GS1_DATAMATRIX,
      * - GS1_QRCODE
      */
    std::string type;
  };

  /**
  * @brief Represents LoyaltyIdentifier object on CP-Payment communication
  */
  class CPDllSpec LoyaltyIdentifier
  {
  public:
    /** constructor of LoyaltyIdentifier object */
    LoyaltyIdentifier();

    /** Specific constructor of LoyaltyIdentifier object
      * \param jsObj - JSObject with LoyaltyIdentifier parameters
      */
    explicit LoyaltyIdentifier(const vfiipc::JSObject &jsObj);

    /** destructor of LoyaltyIdentifier object */
    virtual ~LoyaltyIdentifier();

    /** programId - Unique reference to identify the Loyality Program */
    std::string programId;

    /** customerPhoneNumber - Available in NFC phone tap. an alternate to Loyalty Id */
    std::string customerPhoneNumber;

    /** customerName - Name of the customer */
    std::string customerName;

    /** customerEmail - Email of the customerId */
    std::string customerEmail;

    /** loyaltyPointsBalance - balance of loyalty points */
    std::string loyaltyPointsBalance;

    /** loyaltyPayload - Some loyalty providers may only pass through to the
     *                   POS their encrypted data set. This allows a loyalty
     *                   provider to include their own proprietary fields to
     *                   parse on the POS side
     */
    std::string loyaltyPayload;

    /** customerId - Unique to the phone for customer tracking purposes */
    std::string customerId;

    /** customerLoyaltyId - List of Customer Loyalty Ids */
    std::list<std::string> customerLoyaltyIds;
  };

  /**
  * @enum OFFER_TYPE
  * @brief Represents the Offer object on CP-Payment communication
  */
  typedef enum
  {
    OFFER_UNDEFINED,            ///< offer is undefined
    OFFER_MERCHANT_COUPON,      ///< offer consists of merchant coupon
    OFFER_MANUFACTURER_COUPON,  ///< offer consists of manufacturer coupon
    OFFER_OTHER_COUPON,         ///< offer consists of special coupon
    OFFER_PRODUCT_DISCOUNT,     ///< offer consists of product discount coupon
    OFFER_LOYALTY_CARD,         ///< offer consists of loyalty card
    OFFER_TRANSACTION_DISCOUNT, ///< offer consists of transaction discount
  } OFFER_TYPE;

  // Class to be used with the following events:
  // CP_SYSTEM_RECEIVES_LOYALTY
  // CP_SYSTEM_REQUESTS_BASKET_ADJUSTMENT
  // CP_SYSTEM_RECEIVES_BASKET_ADJUSTMENT
  // CP_SYSTEM_NOTIFIES_BASKET_FINALIZED

  /**
  * @brief Represents the Offer object on CP-Payment communication
  */
  class CPDllSpec Offer
  {
  public:
    /** constructor of Offer object */
    Offer();

    /** destructor of Offer object */
    virtual ~Offer();

    /** id - Offer_Id - Alphanumeric offer reference. May be multiple offers in
      * the same transaction. Could be a back end loaded offer that the POS will
      * redeem or a manufacturer coupon code.
      */
    std::string id;

    /** programId - Unique reference to identify the Program. */
    std::string programId;

    /** type - Type of offer, must be one of: MERCHANT_COUPON,
      * MANUFACTURER_COUPON, OTHER_COUPON, PRODUCT_DISCOUNT, LOYALTY_CARD,
      * TRANSACTION_DISCOUNT
      */
    OFFER_TYPE type;

    /** description - Eg: 20% off one pair of shoes. */
    std::string description;

    /** isRefundable - Indicates if the purchase is returnable */
    bool isRefundable;

    /** isCombinable - Indicates if the offer may combined with other combinable
      * offers.
      */
    bool isCombinable;

    /** percentDiscount - Percentage discount associated with the offer. For
      * example a 20% discount is represented as -0.20 and affects the price by
      * "Adjusted_Amount = Adjusted_Amount + Trans_Amount * Offer_PercentDiscount"
      */
    float percentDiscount;

    /** amountDiscount - Discount in currency specified in the trigger Payload.
      * This is represented as negative amount. String amount formatting
      * applies. This affects the price by Adjusted_Amount = Adjusted_Amount +
      * Offer_Discount
      */
    std::string amountDiscount;

    /** merchantOfferCode - Field for merchant specific offers. */
    std::string merchantOfferCode;

    /** productCode - Represented as
      * SKU (Stock Keeping Unit) or
      * PLU (Price Look Up) or
      * UPC (Unified Product Code)
      */
    std::string productCode;

    /** associatedProductCode - Only applies to special product offers */
    std::string associatedProductCode;

    /** specialProductCode - Eg: BOGO (Buy One Get One) using product and
      * associated product code. BOGOH ( Buy One Get One Half Off) using
      * Product_Code, Percentage_Discount and Associated_Product_Code.
      */
    std::string specialProductCode;

    /** barcode - Barcode information related to this object */
    Barcode barcode;

    /** getTypeStr - get type of OFFER_TYPE as string */
    std::string getTypeStr();

    /** getTypeEnum - get enum from given offer type String.
      * @param strType - string which shall be converted to enumeration value
      * @return OFFER_TYPE - corresponding enum entry to strType
      */
    static OFFER_TYPE getTypeEnum(const std::string &strType);
  };

  /**
  * @brief Represents the Merchandise object on CP-Payment communication
  */
  class CPDllSpec Merchandise
  {
  public:
    /** constructor of merchandise */
    Merchandise();

    /** destructor of merchandise */
    virtual ~Merchandise();

    /** lineItemId - Basket Line Item */
    std::string lineItemId;

    // for now its string with decimal dot as per API
    /** Price of Merchandise. String amount formatting applies */
    std::string unitPrice;

    /** extendedPrice - The product of Unit_Price and Quantity. String amount
      * formatting applies.
      */
    std::string extendedPrice;

    /** qunatity - Number of Merchandise items */
    unsigned long quantity;

    /** sku - SKU of the Merchandise item */
    std::string sku;

    /** upc - UPC code */
    std::string upc;

    /** description - Short one line string that describes the merchandise.
      * Usally used on receipts in front of qunatity and price, typical length
      * is 35 characters max.
      */
    std::string description;
  };


  /**
  * @brief Represents the Donation object on CP-Payment communication
  */
  class CPDllSpec Donation
  {
  public:
    /** constructor of Donation */
    Donation();

    /** destructor of Donation */
    virtual ~Donation();

    /** amount - Amount of Donation in currency specified in the trigger
      * Payload. String amount formatting applies.
      */
    std::string amount;

    /** description - Short one line string that describes the donation.
      * Usally used on receipts in front of quantity and price, typical length
      * is 35 characters max. Usally the recipient of the donation, eg.
      * American Red Cross
      */
    std::string description;
  };

  /**
  * @brief Represents Alt Payment Data object on CP-Payment communication
  */
  struct CPDllSpec AltPaymentData
  {
    /** Amount approved by the Alternate Payment Commerce
      * Application. This amount is returned as a negative number. String amount
      * formatting applies.
      */
    std::string approvedAmount;

    /** Number of units of alternate currency consumed. */
    std::string redeemedUnits;

    /** The transaction Authorization Code usally provided by the
      * Alternate Payment Service Provider.
      */
    std::string authCode;

    /** Description of Alternate Payment service. */
    std::string methodsOfPayment;
  };

  /**
  * @brief Customer identification to be used equivalent to a card pre-dip / pre-swipe.
  */
  struct CPDllSpec CustomerIdentification
  {
    /** A customers identifier(s) unique in the service provider system.
      */
    std::list<std::string> ids;

    /** Payment Account Reference (PAR) represents the Payment Account
      * at the same level that PAN represents the Payment Account.
      */
    std::string par;

    /** Customer token(s) provided by the Verifone tokenization service.
      */
    std::list<std::string> tokens;
  };

  /**
  * @brief Payment amount adjustment Fx data
  */
  struct PaymentAmountAdjustment
  {
    std::string fxAmount;
    std::string fxCurrency;
    double      fxRate;
    double      fxInvertedRate;
    double      fxPercentageCommision;
    double      fxPercentageMargin;
    bool        fxAccepted;
    std::string fxMerchantId;
    std::string fxTerminalId;
  };

  /**
  * get API instance
  * @return reference to API object
  */
  CPDllSpec Api& getApi();

  /**
  * Sets new Environment Data Provider
  * @param[in] envData - reference to EnvironmentDataProvider
  * @return true on success
  */
  CPDllSpec bool setEnvironmentDataProvider(EnvironmentDataProvider &envData);

  /**
  * set basket data used for CP_SYSTEM_NOTIFIES_BASKET_FINALIZED and
  * CP_SYSTEM_REQUESTS_BASKET_ADJUSTMENT
  * @param[in] merchandises - merchandises send to CP App
  * @param[in] offers - offers send to CP App
  * @param[in] donations - donations send to CP App
  */
  CPDllSpec void setBasket(const std::list<Merchandise>& merchandises,
               const std::list<Offer>& offers,
               const std::list<Donation>& donations);

  /**
  * get basket data used for CP_SYSTEM_NOTIFIES_BASKET_FINALIZED and
  * CP_SYSTEM_REQUESTS_BASKET_ADJUSTMENT
  * @param[in] merchandises - merchandises used by CP App
  * @param[in] offers - offers used by CP App
  * @param[in] donations - donations used by CP App
  * @return true if basket is adjusted by CP App
  */
  CPDllSpec bool getBasket(std::list<Merchandise>& merchandises,
               std::list<Offer>& offers,
               std::list<Donation>& donations);

  /**
  * set instance of API object
  * @param[in] api - the api reference to ApiCallbacks implementation
  * @return true on success
  */
  CPDllSpec bool setApi(Api &api);


  /********************** Getters for response data begin********************************/

  /**
   * get provision - Receipt Management response
   * @return the provision
   */
  CPDllSpec std::string getProvision();

  /**
   * get publisher name
   * @return publisher name. Identify which offers or loyalty platform is
   * being leveraged.
   */
  CPDllSpec std::string getPublisher();

  /**
   * get publisher id
   * @return publisher identifier. Provider may elect to pass a numeric
   * identifer for their platform
   */
  CPDllSpec std::string getPublisherId();

  /**
   * get qr code string
   * @return qr code string (RFU)
   */
  CPDllSpec std::string getQrCodeString();

  /**
  * Get loyalty offers returned from CP App while executing CP_SYSTEM_REQUESTS_LOYALTY.
  * @return array of LoyaltyOffer object
  */
  CPDllSpec std::list<Offer> getLoyaltyOffers();

  /**
  * getLoyaltyIdentifiers returned from CP App while executing CP_SYSTEM_REQUESTS_LOYALTY.
  * @return array of LoyaltyIdentifier objects
  */
  CPDllSpec std::list<LoyaltyIdentifier> getLoyaltyIdentifiers();

  /**
  * Get data returned from CP App while executing CP_SYSTEM_REQUESTS_PAYMENT_AMOUNT_ADJUSTMENT.
  */
  CPDllSpec std::string getFxAmount();

  /**
  * Get data returned from Cp App while executing CP_SYSTEM_REQUESTS_PAYMENT_AMOUNT_ADJUSTMENT.
  */
  CPDllSpec std::string getFxCurrency();

  /**
  * Get data returned from CP App while executing CP_SYSTEM_REQUESTS_ALT_PAYMENT.
  * @see AltPaymentData
  */
  CPDllSpec AltPaymentData getAltPaymentData();

  /**
  * Get data returned from CP App while executing CP_SYSTEM_REQUESTS_CUSTOMER_IDENTIFICATION.
  * @see CustomerIdentification
  */
  CPDllSpec CustomerIdentification getCustomerIdentification();

  /**
  * Get payment amount adjustment Fx fields
  * @see Paymentamountadjustment
  */
  CPDllSpec PaymentAmountAdjustment getPaymentAmountAdjustment();

  /**
  * initialization of Commerce
  *
  * @param[in] applicationId - the application id specifies application using commerce
  * @param[in] trxData - Transaction Data Provider taken from current transaction.
  * @param[in] api - api reference to ApiCallbacks implementation.
  * @param[in] envData - Environment Data Provider.
  * @return true on successful initialization
  */
  CPDllSpec bool init(const std::string &applicationId, ITransactionDataProvider &trxData, Api &api, EnvironmentDataProvider &envData);

  /**
  * initialization of Commerce
  *
  * @param[in] applicationId - the application id specifies application using commerce
  * @param[in] trxData - Transaction Data Provider taken from current transaction.
  * @param[in] api - api reference to ApiCallbacks implementation.
  * @return true on successful initialization
  */
  CPDllSpec bool init(const std::string &applicationId, ITransactionDataProvider &trxData, Api &api);

  /**
  * initialization of Commerce
  *
  * @param[in] applicationId - the application id specifies application using commerce
  * @param[in] trxData - Transaction Data Provider taken from current transaction.
  * @param[in] envData - Environment Data Provider.
  * @return true on successful initialization
  */
  CPDllSpec bool init(const std::string &applicationId, ITransactionDataProvider &trxData, EnvironmentDataProvider &envData);

  /**
  * initialization of Commerce
  *
  * @param[in] applicationId - the application id specifies application using commerce
  * @param[in] trxData - Transaction Data Provider taken from current transaction.
  * @return true on successful initialization
  */
  CPDllSpec bool init(const std::string &applicationId, ITransactionDataProvider &trxData);

  /**
  * deinitialization of Commerce
  *
  */
  CPDllSpec void deinit();

  /**
   * sends the notification to CP. The call will be blocking the thread until CP app is closed.
   * decision for request/response or request only is done internally depending on trigger.
   * @param[in] trigger - the trigger message that will be sent to CPR
   * @return returns whether the notification was delivered
   */
  CPDllSpec InvokeResult invoke(const CpTrigger trigger);

  /**
   * sends the notification to CP. The call will be blocking the thread until CP app is closed.
   * decision for request/response or request only is done internally depending on trigger.
   * @param[in] trigger - the trigger message that will be sent to CPR
   * @param[in] sysToForground - default parameter executing sysToForground at end of invoke
   * @return returns whether the notification was delivered
   */
  CPDllSpec InvokeResult invoke(const CpTrigger trigger, bool sysToForground);

  /**
  * sends the notification to CP. The call will be blocking the thread until CP app is closed.
  * decision for request/response or request only is done internally depending on trigger.
  * @param[in] trigger - the trigger message that will be sent to CPR
  * @param  programName - name of the program that will be started in mac
  * @return returns whether the notification was delivered
  */
  CPDllSpec InvokeResult invokeByName(const CpTrigger trigger, const std::string &programName);

  /**
  * sends the notification to CP. The call will be blocking the thread until CP app is closed.
  * decision for request/response or request only is done internally depending on trigger.
  * @param[in] trigger - the trigger message that will be sent to CPR
  * @param  programName - name of the program that will be started in mac
  * @param[in] sysToForground - default parameter executing sysToForground at end of invoke
  * @return returns whether the notification was delivered
  */
  CPDllSpec InvokeResult invokeByName(const CpTrigger trigger, const std::string &programName, bool sysToForground);

  /**
   * invoke from desktop shows application selection desktop with all apps from
   * a given trigger. After selection the program is directly invoked in mac.
   * This method waits until the application ends.
   * @param  trigger which is used for application selection
   * @return returns whether the application has been executed
   */
  CPDllSpec InvokeResult invokeFromDesktop(const CpTrigger trigger);

  /**
   * invoke from desktop shows application selection desktop with all apps from
   * a given trigger. After selection the program is directly invoked in mac.
   * This method waits until the application ends.
   * @param  trigger which is used for application selection
   * @param[in] sysToForground - default parameter executing sysToForground at end of invoke
   * @return returns whether the application has been executed
   */
  CPDllSpec InvokeResult invokeFromDesktop(const CpTrigger trigger, bool sysToForground);

  /** get CP application list of given trigger
  * @return vector of strings with application names to the given trigger
  */
  CPDllSpec std::vector<std::string> getCpAppList(const CpTrigger trigger);

  /** get CP application property js object  of given trigger
  * @return js object to the given trigger
  */
  CPDllSpec vfiipc::JSObject getCpPropList(const CpTrigger trigger);
} // namespace vficpl
#endif /* COMMERCE_H_ */
