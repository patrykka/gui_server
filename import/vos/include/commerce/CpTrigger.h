#ifndef CPTRIGGER_H_
#define CPTRIGGER_H_

namespace vficpl
{
  /** @brief CpTrigger - contains all triggers used by commerce platform */
  typedef enum
  {
    /** CP_SYSTEM_NOTIFIES_IDLE_ENTERED - Fired by Payment Application when
      * entering idle state. This happens once when application first starts up
      * and each time after a transaction completes.
      *
      * This allows the Commerce Applications to control the screen and devices
      * in-between transactions.
      */
    NOTIF_IDLE = 0,

    /** CP_SYSTEM_NOTIFIES_MANUAL_LAUNCH_SELECTED - Fired by Payment application
      * when one of the service label has been selected.
      */
    NOTIF_MANUAL_LAUNCH = 1,

    /** CP_SYSTEM_REQUESTS_CUSTOMER_IDENTIFICATION - Customer identification
      * to be used equivalent to a card "pre-dip / pre-swipe".
      * This is used to enable loyalty programs at the ECR.
      * Some alternative payment methods support loyalty programs,
      * which are usually linked to using the same provider for alternative payment.
      */
    REQUEST_CUSTOMER_IDENTIFICATION = 16,

    /** CP_SYSTEM_NOTIFIES_TRANSACTION_STARTED - Fired by System when exiting
      * the idle screen and ready to initiate a transaction. Exiting idle from a
      * menu like in other services will not fire this event.
      */
    NOTIF_TRX_STARTED = 2,

    /** CP_SYSTEM_REQUESTS_BASKET_ADJUSTMENT - The pre-adjusted basket may be
      * responded to by the commerce application by explicit offer format
      * against the basket line details. This would be the case when the
      * commerce application is basket aware and can quickly adjudicate the
      * basket.
      */
    REQUEST_BASKET_ADJUSTMENT = 3,

    /** CP_SYSTEM_NOTIFIES_BASKET_FINALIZED - No response is needed to final
      * basket, as attempts to adjudicate the basket are now be closed.
      *
      * _Note:_ For Payment applications allowing alterations to the basket post
      * tender, it should be noted that a pre-adjusted basket may be received
      * after final to repeat the process if new information is obtained during
      * tendering. For example, basket may be adjusted as a result of trigger
      * CP_SYSTEM_REQUESTS_LOYALTY.
      */
    NOTIF_BASKET_FINALIZED = 4,

    /** CP_SYSTEM_REQUESTS_LOYALTY - Loyalty data returned from the launched
      * commerce application. Loyalty response may or may not include offers
      * data.
      */
    REQUEST_LOYALTY = 5,

    /** CP_SYSTEM_REQUESTS_AMOUNT_ADJUSTMENT - Fired by Payment Application when
      * amount is received from POS or entered directly. This amount may have
      * been altered by a discount. Permits the commerce application to adjust
      * the final either by a amount or by an percentage.
      *
      * Example usage: Pennies application which rounds up the final amount. For
      * example an amount of $8.19 is rounded up to $9.00
      */
    REQUEST_AMOUNT_ADJUSTMENT = 6,

    /** CP_SYSTEM_NOTIFIES_AMOUNT_FINALIZED - Fired by Payment Application when
      * amount was received from POS or entered directly. This informs the
      * commerce applications on the final amount and intermediate amounts, if
      * any.
      */
    NOTIF_AMOUNT_FINALIZED = 7,

    /** CP_SYSTEM_REQUESTS_ALT_PAYMENT - Fired by Payment Application when one
      * of the alternate payment label has been selected by cardholder.
      */
    REQUEST_ALT_PAYMENT = 8,

    /** CP_SYSTEM_NOTIFIES_CARD_PRESENTED - Fired by Payment application when:
      * - Magstripe is swiped
      * - Chip card has been inserted and read for PAN
      * - Contacless card/phone has been presented
      *
      * Access to the card data is on-demand.
      */
    NOTIF_CARD_PRESENTED = 9,

    /** CP_SYSTEM_NOTIFIES_CARD_BIN_RECEIVED - Fired by Payment application when
      * - Magstripe is swiped
      * - Chip card has been inserted and read for PAN
      * - Contacless card/phone has been presented
      *
      * Access to the card data is on-demand.
      *
      * This event has card BIN information in the payload
      */
    NOTIF_CARD_BIN_RECEIVED = 10,

    /** CP_SYSTEM_REQUESTS_PAYMENT_AMOUNT_ADJUSTMENT - Fired by Payment
      * application with the same conditions as
      * CP_SYSTEM_REQUESTS_AMOUNT_ADJUSTMENT.
      *
      * This gives the opportunity for Commerce Applications to adjust the
      * amount _only_ for only certain card types or provide Dynamic Currency
      * Conversion (DCC) capability.
      */
    REQUEST_PAYMENT_AMOUNT_ADJUSTMENT = 11,

    /** CP_SYSTEM_NOTIFIES_AUTHORIZATION_COMPLETED - Fired by Payment
      * Application when payment authorization is complete.
      */
    NOTIF_AUTHORIZATION_COMPLETED = 12,

    /** CP_SYSTEM_NOTIFIES_PAYMENT_COMPLETED - Fired by Payment Application when
      * payment transaction is completed. This is important because we may be in
      * the payment section for several cycles with split tenders.
      */
    NOTIF_PAYMENT_COMPLETED = 13,

    /** CP_SYSTEM_REQUESTS_RECEIPT_MANAGEMENT - Fired by Payment Application
      * when a receipt is available to be processed. This allows a CP
      * application to provide electronic receipt management services. This may
      * be triggered multiple times during a transaction, but only once per
      * receipt type.
      * If signature is required for customer verification during a
      * transaction, then this will be triggered by the payment application when
      * verification is required, with signature indicators set. If the request
      * does not contain a signature image, but requires a signature - and the
      * response Provision is NONE or OFFER_DECLINED then the payment
      * application will have to print the receipt (internal printer or POS
      * printer) to allow verification to be performed as usual.
      * A CP Application may choose to use the CP_APP_REQUESTS_PRINT API call
      * during this trigger to have the payment application produce a physical
      * receipt.
     */
    REQUEST_RECEIPT_MANAGEMENT = 14,

    /** CP_SYSTEM_NOTIFIES_TRANSACTION_ENDED - Fired by Payment Application when
      * transaction is complete.
      */
    NOTIF_TRX_ENDED = 15
  } CpTrigger;
} // namespace vficpl

#endif // CPTRIGGER_H_
