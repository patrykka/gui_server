/**
 * @file EnvironmentDataProvider.h
 * @brief interface of class EnvironmentDataProvider
 * Implementation with default values exists in vficpl
 * User can overwrite each function to get another value.
 * All environment variables can be set by user except schema and API version.
 */
#ifndef ENVIRONMENTDATAPROVIDER_H_
#define ENVIRONMENTDATAPROVIDER_H_

#include <string>
#include <vector>
#include "cpdllspec.h"

namespace vficpl
{
  class CPDllSpec EnvironmentDataProvider
  {
  public:

    /**
     * constructor
     */
    EnvironmentDataProvider();

    /**
     * destructor
     */
    virtual ~EnvironmentDataProvider();

    /**
     * The default language of the terminal.
     * see https://tools.ietf.org/html/bcp47 for additional details
     * default: "en-US"
     * @return language
     */
    virtual std::string getLocale();

    /**
     * Time zone of the terminal location
     * see https://en.wikipedia.org/wiki/List_of_UTC_time_offsets
     * default: "UTC-08:00"
     * @return time zone
     */
    virtual std::string getTimezone();

    /**
     * Country of the terminal location
     * see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
     * default: "US"
     * @return country
     */
    virtual std::string getCountry();

    /**
     * Name of the merchant this terminal is located at
     * default: "Mr. X"
     * @return merchant name
     */
    virtual std::string getMerchantName();

    /**
     * Street address of the terminal location
     * default: "88 West Plumeria Dr San Jose, CA 95134"
     * @return address
     */
    virtual std::vector<std::string> getMerchantAddress();

    /**
     * Represented as a number, positive represents North latitude
     * default: 0
     * @return latitude
     */
    virtual float getLatitude();

    /**
     * Represented as a number, positive represents East longitude
     * defaut: 0
     * @return longitude
     */
    virtual float getLongitude();

    /**
     * Represented as a number, in meters relatively to mean sea level
     * default: 0
     * @return altitude
     */
    virtual float getAltitude();

    /**
     * Identification of the payment application environment
     * default: "X"
     * @return application name
     */
    virtual std::string getPaymentAppName();

    /**
     * default: "1.0.0"
     * @return payment application version
     */
    virtual std::string getPaymentAppVersion();

    /**
     * Indicates if Payment Application supports Dynamic Currency Conversion
     * default: false
     * @return true if DCC supported
     */
    virtual bool isDCCSupported();

    /**
     * Specify the presence and capability of the printer.
     * The value either "TEXT" or "GRAPHICS" indicating the capability of the printer.
     * If not specified (empty string), it is assumed that there is no printer support.
     * default: "GRAPHICS"
     * @return printer type
     */
    virtual std::string getPrinterType();

    /**
     * Specify the width of the printer in columns.
     * The value of printerWidth is considered only when printer is specified and is meaningful.
     * default: 42
     * @return printer witdh
     */
    virtual int getPrinterColWidth();

    /**
     * Specify the width of the font char in pixels.
     * The value of fontWidth is considered only when printer is specified and is meaningful.
     * default: 6
     * @return font width
     */
    virtual int getPrinterFontWidth();

    /**
     * Specify the width of the printer in pixels.
     * The value of pixelWidth is considered only when printer is specified and is meaningful.
     * default: 256
     * @return pixel width
     */
    virtual int getPrinterPixelWidth();

    /**
     * Indicates if the Payment Application supports barcode reader
     * default: false
     * @return true if supported
     */
    virtual bool isBarcodeReaderSupported();

    /**
     * Specifies the list of beacon types supported.
     * Currently only IBEACON and EDDYSTONE are supported.
     * default: ["IBEACON", "EDDYSTONE"]
     * @return supported beacon types
     */
    virtual std::vector<std::string> getBeaconType();

    /**
     * Indicates if Payment Application supports a POS (ECR)
     * default: false
     * @return true if supported
     */
    virtual bool isPOSSupported();
  };
} // namespace vficpl

#endif
