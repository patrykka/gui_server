#ifndef ITRANSACTIONDATAPROVIDER_H_
#define ITRANSACTIONDATAPROVIDER_H_

/* specific includes */
#include "CpTrigger.h"
#include "cpdllspec.h"
/* common includes */
#include <string>
#include <vector>

namespace vficpl
{
  /** @enum TransactionOriginator
    * Indicates which entity was responsible for starting the transaction.
    */
  typedef enum
  {
    ORIGINATOR_MIN_VALUE = -1,      /**< min value of enumeration */
    ORIGINATOR_UNDEFINED = 0,       /**< originator is undefined */
    ORIGINATOR_PAYMENT_APPLICATION, /**< transaction started by payment application. */
    ORIGINATOR_CP_APPLICATION       /**< transaction started by cp application */
  } TransactionOriginator;

  /** @enum TransactionType
    * Type of transaction executed by the Payment Application
    */
  typedef enum
  {
    TRX_TYPE_MIN_VALUE = -1,              /**< min value of enumeration */
    TRX_TYPE_UNDEFINED = 0,               /**< type is undefined */
    TRX_TYPE_PAYMENT,                     /**< type of transaction is payment */
    TRX_TYPE_DEFERRED_PAYMENT,            /**< type of transaction is deferred payment */
    TRX_TYPE_DEFERRED_PAYMENT_COMPLETION, /**< type of transaction is deferred payment completion */
    TRX_TYPE_REFUND,                      /**< type of transaction is refund */
    TRX_TYPE_VOID,                        /**< type of transaction is void */
    TRX_TYPE_PRE_AUTHORISATION,           /**< type of transaction is pre authorisation */
    TRX_TYPE_PRE_AUTHORISATION_COMPLETION,/**< type of transaction is pre authorisation completion */
    TRX_TYPE_CASH_ADVANCE,                /**< type of transaction is cash advance */
    TRX_TYPE_CASHBACK,                    /**< type of transaction is cashback */
    TRX_TYPE_CASH_PAYMENT,                /**< type of transaction is cash payment */
    TRX_TYPE_REVERSAL                     /**< type of transaction is reversal */
  } TransactionType;

  /** @enum CardType
    * Specifies card type used to obtain card data.
    */
  typedef enum
  {
    CARD_TYPE_MIN_VALUE = -1, /**< min value of enumeration */
    CARD_TYPE_UNDEFINED = 0,  /**< card type is undefined */
    CARD_TYPE_MAG_STRIPE,     /**< used card type is mag-stripe */
    CARD_TYPE_CHIP,           /**< used card type is chip */
    CARD_TYPE_CTLS_CARD,      /**< used card type is ctls card */
    CARD_TYPE_CTLS_PHONE,     /**< used card type is ctls phone */
    CARD_TYPE_MANUAL,         /**< used card type is manual */
  } CardType;

  /** @enum AuthEntity
    * Entity that has authorized the transaction.
    */
  typedef enum
  {
    AUTH_ENTITY_MIN_VALUE = -1, /**< min value of enumeration */
    AUTH_ENTITY_UNDEFINED = 0,  /**< entity is undefined */
    AUTH_ENTITY_MERCHANT,       /**< merchant has authorized the transaction */
    AUTH_ENTITY_ACQUIRER,       /**< acquirer has authorized the transaction */
    AUTH_ENTITY_CARD_SCHEME,    /**< card scheme used for authorization of transaction */
    AUTH_ENTITY_ISSUER          /**< issuer has authorized the transaction */
  } AuthEntity;

  /** @enum AuthResult
    * Result of transaction authorization.
    */
  typedef enum
  {
    AUTH_RESULT_MIN_VALUE = -1,     /**< min value of enumeration */
    AUTH_RESULT_UNDEFINED = 0,      /**< result is undefined */
    AUTH_RESULT_AUTHORIZED_ONLINE,  /**< online authorized */
    AUTH_RESULT_AUTHORIZED_OFFLINE, /**< offline authorized */
    AUTH_RESULT_REJECTED_ONLINE,    /**< online rejected */
    AUTH_RESULT_CASH_VERIFIED,      /**< transaction verified and cash payed */
    AUTH_RESULT_ABORTED             /**< authorization aborted */
  } AuthResult;

  /** @enum HolderAuth
    * Means by which the transaction is authorized.
    */
  typedef enum
  {
    HOLDER_AUTH_MIN_VALUE = -1, /**< min value of enumeration */
    HOLDER_AUTH_UNDEFINED = 0,  /**< holder auth is undefined */
    HOLDER_AUTH_PIN,            /**< holder authorized by pin */
    HOLDER_AUTH_SIGNATURE,      /**< authorized by signature */
    HOLDER_AUTH_ID,             /**< authorized by id */
    HOLDER_AUTH_OTHER,          /**< authorized by other method */
    HOLDER_AUTH_NONE            /**< holder not authorized */
  } HolderAuth;

  /** @enum TransactionResult
    * The result of the transaction
    */
  typedef enum
  {
    TRX_RESULT_MIN_VALUE = -1, /**< min value of enumeration */
    TRX_RESULT_UNDEFINED = 0,  /**< transaction result is undefined */
    TRX_RESULT_SUCCESS,        /**< transaction result is success */
    TRX_RESULT_CANCELLED,      /**< transaction result is cancelled */
  } TransactionResult;

  /**
   * @interface ITransactionDataProvider
   */
  class CPDllSpec ITransactionDataProvider
  {
    public:
      /** default destructor */
      virtual ~ITransactionDataProvider() {};

      /** @return TransactionOriginator which is responsible for transaction start */
      virtual TransactionOriginator getTransactionOriginator() = 0;

      /** @return std::string with unique reference to transaction */
      virtual std::string getInvoice() = 0;

      /** @return std::string with store number assigned by the merchant */
      virtual std::string getStoreNumber() = 0;

      /** @return std::string identifies the payment terminal location in the
        * store
        */
      virtual std::string getLane() = 0;

      /** @return std::string identifies the cashier */
      virtual std::string getCashierId() = 0;

      /** @return std::string transaction id provided by the payment gateway, which is a
        * client specific transaction identifier
        */
      virtual std::string getTransId() = 0;

      /** @return std::string transaction executed by the payment application */
      virtual TransactionType getTransactionType() = 0;

      /** @return std::string transaction amount */
      virtual std::string getTransAmount() = 0;

      /** @return std::string alphabetic currency code (ISO 4217).
        * Ex: USD,EUR */
      virtual std::string getTransCurrency() = 0;

      /** @return std::string reference of payment application which refers to
        * the PAN in question.
        */
      virtual std::string getPanHandle() = 0;

      /** @return the first 6 digits of the card holder PAN */
      virtual std::string getPanFirst6() = 0;

      /** @return std::string the last four digits of the card holder PAN */
      virtual std::string getPanLast4() = 0;

      /** @return std::string card type used to obtain card data */
      virtual CardType getCardType() = 0;

      /** @return std::string transaction authorization code provided by the
        * payment processing service or acquirer
        */
      virtual std::string getAuthCode() = 0;

      /** @return AuthEntity entity that has authorized the transaction */
      virtual AuthEntity getAuthEntity() = 0;

      /** @return AuthResult result of transaction authorization. */
      virtual AuthResult getAuthResult() = 0;

      /** @return std::string descriptive text accompanying the Auth_Code from
        * payment processor.
        */
      virtual std::string getAuthRespTxt() = 0;

      /** @return HolderAuth means by which the transaction is authorized. */
      virtual HolderAuth getHolderAuth() = 0;

      /** @return std::string acquirer merchant id */
      virtual std::string getAcquirerMerchantId() = 0;

      /** @return std::string acquirer terminal id */
      virtual std::string getAcquirerTerminalId() = 0;

      /** @return std::string acquirer id */
      virtual std::string getAcquirerId() = 0;

      /** @return std::string gateway merchant id */
      virtual std::string getGatewayMerchantId() = 0;

      /** @return std::string gateway terminal id */
      virtual std::string getGatewayTerminalId() = 0;

      /** @return std::string loyalty identifier */
      virtual std::string getLoyaltyId() = 0;

      /** @return TransactionResult result of transaction */
      virtual TransactionResult getTransactionResult() = 0;

      // Final_Amount = Initial_Amount + Adjusted_Amount + Gratuity_Amount - Already_Approved_Amount
      /** @return std::string the amount that payment is attempted. */
      virtual std::string getInitialAmount() = 0;

      /** @return std::string the sum of adjustments that have to be applied to
        * the initial amount excluding the gratuity.
        */
      virtual std::string getAdjustedAmount() = 0;

      /** @return double percentage amount to adjust, e.g 0.1 if 10% */
      //virtual double getAdjustmentPercentage() = 0;

      /** @return std::string description field for amount adjustment */
      //virtual std::string getAdjustmentDescription() = 0;

      /** @return std::string tip and its value */
      virtual std::string getGratuityAmount() = 0;

      /** @return std::string amount the transaction was approved for. */
      virtual std::string getApprovedAmount() = 0;

      /** @return std::string card currency (only EMV cards Tag 9F42) */
      virtual std::string getCardCurrency() = 0;

      /** @return std::string card country (only EMV cards Tag 5F28) */
      virtual std::string getCardCountry() = 0;

      /** @return std::vector<std::string> card preferred language (only EMV
        * cards Tag 5F2D) */
      virtual std::vector<std::string> getPrefLanguages() = 0;

      /** @return true when paid by cash, card_type will be disregarded, if
        * false or not present, Card_Type must be specified.
        */
      virtual bool getCashPayment() = 0;

      /** @return std::vector<std::string> list of payment mechanisms used */
      virtual std::vector<std::string> getPaymentMethods() = 0;

      /* setters */

      /** method sets transaction amount
        * @param amount replaces current transaction amount
        */
      virtual void setTransAmount(const std::string& amount) = 0;

      /** method sets adjusted amount
        * @param amount replaces adjusted amount
        */
      virtual void setAdjustedAmount(const std::string& amount) = 0;

      /** methods sets percentage of amount adjusted for convenience, e.g. 0.1 if 10%
        * @param pct replaces existing value
        */
      //virtual void setAdjustmentPercentage(double pct) = 0;

      /** method sets description of adjusted amount
        * @param text replaces description
        */
      //virtual void setAdjustmentDescription(const std::string& text) = 0;

      /** @return The type of receipt to be printed */
      virtual std::string getReceiptType() = 0;

      /** @return The receipt in HTML format.  Any images (including signature) are to be embedded in the HTML */
      virtual std::string getReceipt() = 0;

      /** @return Indicates if the Receipt contains an area where the customer is still required to sign */
      virtual bool requiresSignature() = 0;

      /** @return Indicates if the Receipt contains an embedded signature image */
      virtual bool hasSignatureImage() = 0;

      /**
       * method initTrigger is used by the commerce library to manipulate the
       * transaction data right before call of invoke
       * @param trigger to manipulate
       */
      virtual void initTrigger(const CpTrigger& trigger) = 0;
  };
} // namespace vficpl

#endif
