#ifndef CPAPP_H_20150505
#define CPAPP_H_20150505

/** \file cpapp.h */

#include <map>
#include <string>

#if defined _WIN32
#  if   defined VFI_CPAPP_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_CPAPP_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_CPAPP_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

#ifdef _WIN32
#include <windows.h>
typedef LONG _Atomic_word;
#endif

namespace cpapp {
#if 0
}
#endif



/** helper class for managing CPApp handles */
template<class T> class counted_ptr {
   T *p;
 public:

   counted_ptr(T *ptr=0) {
      p=ptr;
#ifdef _WIN32
      if(p) InterlockedIncrement(&p->m_refcount);
#elif defined __GNUC__
      if(p) __gnu_cxx::__atomic_add_dispatch(&p->m_refcount,1);
#else
      if(p) p->m_refcount++;
#endif
   }

   counted_ptr(const counted_ptr &o) {
      p=o.p;
#ifdef _WIN32
      if(p) InterlockedIncrement(&p->m_refcount);
#elif defined __GNUC__
      if(p) __gnu_cxx::__atomic_add_dispatch(&p->m_refcount,1);
#else
      if(p) p->m_refcount++;
#endif
   }

   counted_ptr &operator=(const counted_ptr &o) {
      if(&o==this) return *this;
#ifdef _WIN32
      if(o.p) InterlockedIncrement(&const_cast<counted_ptr &>(o).p->m_refcount);
      if(p && InterlockedDecrement(&p->m_refcount)==0) {
         delete p;
      }
#elif defined __GNUC__
      if(o.p) __gnu_cxx::__atomic_add_dispatch(&const_cast<counted_ptr &>(o).p->m_refcount,1);
      if(p && __gnu_cxx::__exchange_and_add_dispatch(&p->m_refcount,-1)==1) {
         delete p;
      }
#else
      if(o.p) o.p->m_refcount++;
      if(p && --p->m_refcount==0) delete p;
#endif
      p=o.p;
      return *this;
   }

   ~counted_ptr() {
#ifdef _WIN32
      if(p && InterlockedDecrement(&p->m_refcount)==0) {
         delete p;
      }
#elif defined __GNUC__
      if(p && __gnu_cxx::__exchange_and_add_dispatch(&p->m_refcount,-1)==1) {
         delete p;
      }
#else
      if(p && --p->m_refcount==0) delete p;
#endif
   }

   void reset(T *ptr=0) {
#ifdef _WIN32
      if(p && InterlockedDecrement(&p->m_refcount)==0) {
         delete p;
      }
#elif defined __GNUC__
      if(p && __gnu_cxx::__exchange_and_add_dispatch(&p->m_refcount,-1)==1) {
         delete p;
      }
#else
      if(p && --p->m_refcount==0) delete p;
#endif
      p=ptr;
#ifdef _WIN32
      if(p) InterlockedIncrement(&p->m_refcount);
#elif defined __GNUC__
      if(p) __gnu_cxx::__atomic_add_dispatch(&p->m_refcount,1);
#else
      if(p) p->m_refcount++;
#endif
   }

   T* operator->() { return p; }
   const T* operator->() const { return p; }

   T &operator*() { return *p; }
   const T& operator*() const { return *p; }

   T* get() { return p; }
   const T* get() const { return p; }

};

/** helper class for managing CPApp handles */
class DllSpec counted {
   template<class T> friend class counted_ptr;
   _Atomic_word m_refcount;
 public:
   counted() { m_refcount=0; }
   counted(const counted &) { m_refcount=0; }
   void operator=(const counted &) {}; // the counter value always refers to the current object and is never copied
};

/** Opaque CP application class */
class DllSpec CPApp: public counted {
 protected:
   CPApp() {};
   CPApp(const CPApp &o);
   void operator=(const CPApp &o);
 public:
   virtual ~CPApp() {};
};

/** CP application handle, required for cancelling a CP application */
typedef counted_ptr<CPApp> CPAppHandle;

/** application error codes */
enum CPAppResult {
   CPA_OK,            /**< no error */
   CPA_ERR_PARAMETER, /**< invalid parameters */
   CPA_ERR_MANIFEST,  /**< reading the manifest file failed */
   CPA_ERR_NO_APP,    /**< the section does not exist or does not contain any application */
   CPA_ERR_THREAD,    /**< failed to create thread */
   CPA_ERR_CHECK      /**< the CP app installation directory has wrong permissions / is owned by wrong user */
};

/** Run a CP application in a new thread. When the application is done the provided callback function is invoked
 * \param[out] handle This returns a handle that can be used to cancel the app using cpAppCancel(). In case of an error,
 *             the returned handle is 0.
 * \param[in] region_id Region ID in which the application will be shown (see ADKGUI for a description of GUI regions).
 * \param[in] manifest filename (path) of the manifest file that describes the application
 * \param[in] section section name in the manifest that determines which function to be run
 * \param[in] data key value map containing data that will be provided to the CP application
 * \param[in] result callback function that gets invoked when the CP application has terminated.
 * \param[in] cb_data data pointer that gets passed on to the result callback as first parameter
 * \return Error code that reports errors during starting the application. Errors that happen after
 *         starting the application thread are reported via the result callback function.
 *
 * The callback function takes the following parameters
 * \param[in] cb_data data pointer provided to cpAppRun()
 * \param[in] retval return value / error code of the application. This uses the error codes from ADKGUI (e.g. UI_ERR_OK)
 * \param[in] data key value map containing data returned from the CP application
 */
DllSpec enum CPAppResult cpAppRun(
   CPAppHandle &handle,
   int region_id,
   const char *manifest,
   const char *section,
   const std::map<std::string,std::string> &data,
   void (*result)(void *cb_data, int retval, const std::map<std::string,std::string> &data),
   void *cb_data
);

/** Run a CP application in a new thread. When the application is done the provided callback function is invoked
 * \param[out] handle This returns a handle that can be used to cancel the app using cpAppCancel(). In case of an error,
 *             the returned handle is 0.
 * \param[in] display display ID
 * \param[in] region_id Region ID in which the application will be shown (see ADKGUI for a description of GUI regions).
 * \param[in] manifest filename (path) of the manifest file that describes the application
 * \param[in] section section name in the manifest that determines which function to be run
 * \param[in] data key value map containing data that will be provided to the CP application
 * \param[in] result callback function that gets invoked when the CP application has terminated.
 * \param[in] cb_data data pointer that gets passed on to the result callback as first parameter
 * \return Error code that reports errors during starting the application. Errors that happen after
 *         starting the application thread are reported via the result callback function.
 *
 * The callback function takes the following parameters
 * \param[in] cb_data data pointer provided to cpAppRun()
 * \param[in] retval return value / error code of the application. This uses the error codes from ADKGUI (e.g. UI_ERR_OK)
 * \param[in] data key value map containing data returned from the CP application
 */
DllSpec enum CPAppResult cpAppRun(
   CPAppHandle &handle,
   int display,
   int region_id,
   const char *manifest,
   const char *section,
   const std::map<std::string,std::string> &data,
   void (*result)(void *cb_data, int retval, const std::map<std::string,std::string> &data),
   void *cb_data
);

/** Cancel a CP application
 * \param[in] d Handle returned from cpAppRun()
 */
DllSpec void cpAppCancel(const CPAppHandle &h);


/** Dummy function, no longer supported since now a hard wired path is used. It is just provided
 * for backwards compatibility.
 */
DllSpec void cpSetAppFSRoot(const std::string &root);


/** installer error codes */
enum CPInstallResult {
   CPI_OK,            /**< no error, packages have been installed or removed */
   CPI_NO_PACKAGE,    /**< no installation package found, i.e. nothing has been installed */
   CPI_ERROR,         /**< error happened during installation, nonetheless,
                       *   packages may have been installed or removed */
   CPI_WARNING        /**< warning was issued during installation */
};

/**
 * run CP installer
 * \param[in] print fprintf-like callback function
 * \param[in] data data pointer passed as first parameter to print callback
 * \return Installation result
 */
DllSpec enum CPInstallResult cpInstall(void (*print)(void *data, const char *format,...), void *data);

/** \return Platform specific directory in which cpInstall() expects to find packages for installation
 */
DllSpec const char *cpInstallSourceDir();

/** \return Platform specific apps root directory. Installed CP applications can be found in this directory:
 * - <app-id>.mft
 * - <app-id>/<app-files>
 */
DllSpec const char *cpInstallAppRoot();

/** \return Platform specific manifest root directory. Marketplace manifests of installed CP applications can be found in this directory:
 * - <app-id>/mpmanifest.mft
 * - <app-id>confOwner.json
 */
DllSpec const char *cpInstallManifestRoot();


/** extract certificates as tar archive
 * \param[out] out string containing tar archive
 */
DllSpec void cpGetCertificateArchive(std::string &out);

/** Create source directory for installation packages with appropriate permissions
 * \return error code
 */
DllSpec enum CPInstallResult cpInstallCreateSourceDir();


/** Create source directory for installation packages with appropriate permissions
 * \param[in] print fprintf-like callback function for printing error messages
 * \param[in] data data pointer passed as first parameter to print callback
 * \return Installation result
 */
DllSpec enum CPInstallResult cpInstallCreateSourceDir(void (*print)(void *data, const char *format,...), void *data);


} // namespace cpapp

#undef DllSpec

#endif
