
#ifndef __ADK_CRD_SYNC_CLIENT_INTERFACE__
#define __ADK_CRD_SYNC_CLIENT_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

// ================================================================================================


/// @defgroup CRDSYNC_Return_Codes Return Codes
//@{
typedef int CRDSYNC_RET;               ///< Response code type definition

#define CRDSYNC_Success               0  ///< Operation was successful
#define CRDSYNC_Failure               1  ///< Operation failed
#define CRDSYNC_Error_Power_Action    2  ///< Power-Up operation failed
#define CRDSYNC_Error_Not_Supported   3  ///< Synchronous card type not supported
#define CRDSYNC_Error_BadParam        4  ///< A function parameter is invalid
#define CRDSYNC_Error_Protocol        5  ///< Communication error with synchronous card

#define CRDSYNC_Error_FORMAT       -101  ///< Malformed message
#define CRDSYNC_Error_TLV          -102  ///< Error parsing TLV input buffer
#define CRDSYNC_Error_PARAMETER    -103  ///< Missing parameter
#define CRDSYNC_Error_UNKNOWN_CLA  -104  ///< Unknown class
#define CRDSYNC_Error_UNKNOWN_INS  -105  ///< Unknown instruction
#define CRDSYNC_Error_BLOCKED      -106  ///< CRDSYNC service is blocked by another client/function call

#define CRDSYNC_Error_NO_SERVER    -201  ///< No server configured
#define CRDSYNC_Error_IPC          -202  ///< IPC error (e.g. connection to server lost)
//@}


/// @defgroup CRDSYNC_Card_Types Synchronous Card Types
/// @brief Used by crdSync_PowerUp()
//@{
#define CRDSYNC_CARDTYPE_AT24C01SC             1  ///< Atmel AT24C01SC
#define CRDSYNC_CARDTYPE_AT24C02SC             2  ///< Atmel AT24C02SC
#define CRDSYNC_CARDTYPE_AT24C04SC             3  ///< Atmel AT24C04SC
#define CRDSYNC_CARDTYPE_AT24C08SC             4  ///< Atmel AT24C08SC
#define CRDSYNC_CARDTYPE_AT24C16SC             5  ///< Atmel AT24C16SC
#define CRDSYNC_CARDTYPE_ST14C02SC             6  ///< STMicro ST14C02SC
//@}


/** Server configuration */
typedef struct {
  const char *hostname;     /**< host name of server, NULL or empty string means localhost */
  unsigned short port;      /**< port to connect to, 0 means default port (5805) */
} CRDSYNC_ServerConfig;


/**
 * Type of function that is called for traces, see crdSync_SetTraceCallback()
 * \param[in] str : Trace message.
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* CRDSYNC_TraceCallback) (const char *str, void *data);


/**
 * Configure servers.
 * Set servers and connect to them.
 * \param[in] server_cnt : number of servers. Set to 0 to disconnect from all servers.
 * \param[in] server_cfg : list of server configurations
 * \return
 * - \ref CRDSYNC_Success : Successfully connected to all servers
 * - \ref CRDSYNC_Error_IPC : Could not connect to one or more servers
 * - \ref CRDSYNC_Failure : Internal error
 */
int crdSync_ConfigureServer(unsigned char server_cnt, const CRDSYNC_ServerConfig *server_cfg);


/**
 * Get Version of library
 * \param[in,out]  version  in: Buffer for version info, out: C-String with library name and version
 * \param[in]      len      Allocated size of version
 */
void crdSync_Version(char *version, unsigned char len);


/**
 * Set callback function for trace output.
 * \param[in] cbf : Callback function for trace messages, may be NULL.
 * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
 */
void crdSync_SetTraceCallback(CRDSYNC_TraceCallback cbf, void *cb_data);


//****************************************************************************
/// @brief Open the synchronous card driver @n
///
/// @param[in] server_idx   - index of server in server configuration (see crdSync_ConfigureServer()).
/// @param[in] Options      - Reserved for future use
///
/// @returns #CRDSYNC_Success - Synchronous card driver opened.
/// @returns #CRDSYNC_Failure - Error, synchronous card driver is already opened.
//****************************************************************************
CRDSYNC_RET crdSync_Open(unsigned char server_idx, unsigned Options);


//****************************************************************************
/// @brief Close the synchronous card driver @n
///
/// @param[in] server_idx   - index of server in server configuration (see crdSync_ConfigureServer()).
/// @param[in] Options      - Reserved for future use
///
/// @returns #CRDSYNC_Success - Synchronous card driver closed.
/// @returns #CRDSYNC_Failure - Error, synchronous card driver is not opened.
//****************************************************************************
CRDSYNC_RET crdSync_Close(unsigned char server_idx, unsigned Options);


//****************************************************************************
/// @brief Check if a card is inserted in the full-sized card reader.  @n
///
/// @param[in] server_idx   - index of server in server configuration (see crdSync_ConfigureServer()).
///
/// @returns #CRDSYNC_Success - A card is inserted.
/// @returns #CRDSYNC_Failure - No card inserted.
//****************************************************************************
CRDSYNC_RET crdSync_IsCardPresent(unsigned char server_idx);


//****************************************************************************
/// @brief Power up the synchronous card @n
///
/// @param[in]     server_idx - index of server in server configuration (see crdSync_ConfigureServer()).
/// @param[in]     CardType   - Synchronous card type, see  \ref CRDSYNC_Card_Types
/// @param[in,out] AtrBuf     - Pointer to buffer to hold any response from the card (optional)
/// @param[in,out] AtrLen     - Pointer to 8-bit variable to hold length of card response (optional)
///
/// @returns #CRDSYNC_Success              - No card
/// @returns #CRDSYNC_Failure              - Error, driver not opened or card not inserted
/// @returns #CRDSYNC_Error_Power_Action   - Error, unable to power up card
/// @returns #CRDSYNC_Error_Not_Supported  - Invalid CardType
//****************************************************************************
CRDSYNC_RET crdSync_PowerUp(unsigned char  server_idx,
                            int            CardType,
                            unsigned char  AtrBuf[],
                            unsigned char *AtrLen);


//****************************************************************************
/// @brief Power down the synchronous card @n
///
/// @param[in] server_idx   - index of server in server configuration (see crdSync_ConfigureServer()).
///
/// @returns #CRDSYNC_Success - Card powered down successfully.
/// @returns #CRDSYNC_Failure - Error powering down card - not powered up.
//****************************************************************************
CRDSYNC_RET crdSync_PowerDown(unsigned char server_idx);


//****************************************************************************
/// @brief Read data from the synchronous card @n
///
/// @param[in]     server_idx       - index of server in server configuration (see crdSync_ConfigureServer()).
/// @param[in]     MemoryAddress    - memory address where to start reading from
///                                   if -1, start at current location
/// @param[in,out] RxBuf            - Pointer to buffer where read data is stored
///                                   Must be large enough to hold 'RxLen' bytes
/// @param[in]     RxLen            - Number of bytes to read from card
///
/// @returns #CRDSYNC_Success         - Operation successful
/// @returns #CRDSYNC_Failure         - Error - driver not opened, card not inserted or powered up
/// @returns #CRDSYNC_Error_BadParam  - Invalid parameter, RxLen is zero, or RxBuf is NULL
/// @returns #CRDSYNC_Error_Protocol  - Error communicating with the card - wrong CardType
//****************************************************************************
CRDSYNC_RET crdSync_ReadData(unsigned char  server_idx,
                             int            MemoryAddress,
                             unsigned char  RxBuf[],
                             unsigned short RxLen);


//****************************************************************************
/// @brief Write data to the synchronous card @n
///
/// @param[in]     server_idx       - index of server in server configuration (see crdSync_ConfigureServer()).
/// @param[in]     MemoryAddress    - memory address where to start reading from
/// @param[in,out] TxBuf            - Pointer to buffer where data to write is stored
/// @param[in]     TxLen            - Number of bytes to write to the card
///
/// @returns #CRDSYNC_Success         - Operation successful
/// @returns #CRDSYNC_Failure         - Error - driver not opened, card not inserted or powered up
/// @returns #CRDSYNC_Error_BadParam  - Invalid parameter, TxLen is zero, or TxBuf is NULL
/// @returns #CRDSYNC_Error_Protocol  - Error communicating with the card - wrong CardType
//****************************************************************************
CRDSYNC_RET crdSync_WriteData(unsigned char       server_idx,
                              int                 MemoryAddress,
                              const unsigned char TxBuf[],
                              unsigned short      TxLen);


// ================================================================================================

#ifdef __cplusplus
}
#endif

#endif  // avoid double include
