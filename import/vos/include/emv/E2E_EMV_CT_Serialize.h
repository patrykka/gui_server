/****************************************************************************
*  Product:     ADK Cards Service - EMV Contact (CT)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Definitions for serial interface
****************************************************************************/

#ifndef EMV_CT_TLV_H   /* avoid double interface-includes */
  #define EMV_CT_TLV_H

#include <stdarg.h>

// *************************
// defines for serialization
// *************************

#define HEADER_SIZE          (4)
#define MAX_MESSAGE_SIZE     (2048)
#define MAX_DATA_SIZE        (MAX_MESSAGE_SIZE-HEADER_SIZE)

#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif

// ========================================================================================================
// === EMVCo and card issuer tags ===
// ========================================================================================================

/// @defgroup EMV_TAGS BER TLV tags used by EMV framework
/// @ingroup ADK_SERIALIZATION

/// @defgroup EMVCO_TAGS Tags defined by EMVCo
/// @ingroup EMV_TAGS
/// @{
#define TAG_42_ISSUER_ID            0x42u     ///< Issuer Identification Number (IIN)                (@b 42)  @n used for EMV_CT_CANDIDATE_DATA_STRUCT::IIN
#define TAG_4F_APP_ID               0x4Fu     ///< Application Identifier (AID) - Card               (@b 4F)
#define TAG_50_APP_LABEL            0x50u     ///< Application Label                                 (@b 50)   @n used for EMV_CT_APPLIDATA_STRUCT::AppName, EMV_CT_DOM_CHIP_STRUCT::ucAppName, EMV_CT_CANDIDATE_STRUCT::name
#define TAG_52_CMD_TO_PERFORM       0x52u     ///< Command to Perform                                (@b 52)
#define TAG_57_TRACK2_EQUIVALENT    0x57u     ///< Track 2 Equivalent Data                           (@b 57)   @n used as @c T_57_DataTrack2 in ::EMV_CT_TRANSRES_STRUCT, and @c puc57_track2 in @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS )
#define TAG_5A_APP_PAN              0x5Au     ///< Application Primary Account Number                (@b 5A)   @n used as @c T_5A_PAN in ::EMV_CT_TRANSRES_STRUCT, @c puc5A_pan in @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS ), @c puc5A_pan in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC), and @c puc5A_pan in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_5F20_CARDHOLDER_NAME    0x5F20u   ///< Cardholder Name                                   (@b 5F20) @n used as @c T_5F20_Cardholder in ::EMV_CT_TRANSRES_STRUCT, @c puc5F24_expdate in @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS ), and @c uc5F24_expdate in @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)
#define TAG_5F24_APP_EXP_DATE       0x5F24u   ///< Application Expiration Data                       (@b 5F24) @n used as @c T_5F24_AppExpDate in ::EMV_CT_TRANSRES_STRUCT, @c puc5F24_expdate in @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS ), and @c pucExpDate in @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)
#define TAG_5F25_APP_EFF_DATE       0x5F25u   ///< Application Effective Data                        (@b 5F25) @n used as @c T_5F25_AppEffDate in ::EMV_CT_TRANSRES_STRUCT
#define TAG_5F28_ISS_COUNTRY_CODE   0x5F28u   ///< Issuer Country Code                               (@b 5F28) @n used as @c T_5F28_IssCountryCode in ::EMV_CT_TRANSRES_STRUCT, and @c puc5F28_icc in @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS )
#define TAG_5F2A_TRANS_CURRENCY     0x5F2Au   ///< Transaction Currency Code                         (@b 5F2A) @n Configured with @c CurrencyTrans in ::EMV_CT_TERMDATA_STRUCT (for DCC possibly in ::EMV_CT_SELECT_STRUCT). Used as @c T_5F2A_CurrencyTrans in ::EMV_CT_TRANSRES_STRUCT, @c puc5F2A_tcc in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC), and @c puc5F2A_tcc in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_5F2D_LANGUAGE           0x5F2Du   ///< Language Preference                               (@b 5F2D) @n used as @c T_5F2D_Lang_Pref in ::EMV_CT_TRANSRES_STRUCT
#define TAG_5F30_SERVICE_CODE       0x5F30u   ///< Service Code                                      (@b 5F30)
#define TAG_5F34_PAN_SEQUENCE_NB    0x5F34u   ///< PAN Sequence Number                               (@b 5F34) @n used as @c T_5F34_PANSequenceNo in ::EMV_CT_TRANSRES_STRUCT
#define TAG_5F36_TRANS_CURRENCY_EXP 0x5F36u   ///< Transaction Currency Exponent                     (@b 5F36) @n Configured with @c Exp_Trans in ::EMV_CT_TERMDATA_STRUCT (for DCC possibly in ::EMV_CT_SELECT_STRUCT). Used as @c T_5F36_Trx_Currency_Exp in ::EMV_CT_TRANSRES_STRUCT, @c puc5F36_tce in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC), and @c puc5F36_tce in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_5F50_ISSUER_URL         0x5F50u   ///< Issuer URL                                        (@b 5F50)
#define TAG_5F53_IBAN               0x5F53u   ///< International Bank Account Number (IBAN)          (@b 5F53) @n used for EMV_CT_CANDIDATE_DATA_STRUCT::IBAN
#define TAG_5F54_BIC                0x5F54u   ///< Bank Identifier Code (BIC)                        (@b 5F54) @n used for EMV_CT_CANDIDATE_DATA_STRUCT::BIC
#define TAG_5F55_ISSUER_COUNTRY_2   0x5F55u   ///< Issuer Country Code (alpha 2 format)              (@b 5F55) @n used for EMV_CT_CANDIDATE_DATA_STRUCT::country2
#define TAG_5F56_ISSUER_COUNTRY_3   0x5F56u   ///< Issuer Country Code (alpha 3 format)              (@b 5F56) @n used for EMV_CT_CANDIDATE_DATA_STRUCT::country3
#define TAG_5F56_ISSUER_COUNTRY_CODE 0x5F56u   ///< Issuer Country Code                              (@b 5F56)
#define TAG_5F57_ACCOUNT_TYPE       0x5F57u   ///< Account Type                                      (@b 5F57)
#define TAG_61_APP_TEMPLATE         0x61u     ///< Application Template                              (@b 61)
#define TAG_6F_FCI_TEMPLATE         0x6Fu     ///< File Control Information (FCI) Template           (@b 6F)
#define TAG_70_AEF_DATA_TEMPLATE    0x70u     ///< AEF Data Template                                 (@b 70)
#define TAG_71_ISS_SCRIPT_TPLT_1    0x71u     ///< Issuer Script Template 1                          (@b 71)
#define TAG_72_ISS_SCRIPT_TPLT_2    0x72u     ///< Issuer Script Template 2                          (@b 72)
#define TAG_73_DIR_DISCR_TEMPLATE   0x73u     ///< Directory Discretionary Template                  (@b 73)
#define TAG_77_RS_MSG_TPLT_FRMT_2   0x77u     ///< Response Message Template Format 2                (@b 77)
#define TAG_80_RS_MSG_TPLT_FRMT_1   0x80u     ///< Response Message Template Format 1                (@b 80)
#define TAG_81_BIN_AMOUNT_AUTH      0x81u     ///< Amount Authorization Binary                       (@b 81)
#define TAG_82_AIP                  0x82u     ///< Application Interchange Profile                   (@b 82) @n used as @c T_82_AIP in ::EMV_CT_TRANSRES_STRUCT
#define TAG_83_CMD_TPLT             0x83u     ///< Command Template                                  (@b 83)
#define TAG_84_DF_NAME              0x84u     ///< Dedicated File Name                               (@b 84) @n used as @c T_84_DFName in ::EMV_CT_TRANSRES_STRUCT
#define TAG_86_ISS_SCRIPT_CMD       0x86u     ///< Issuer Script Command                             (@b 86)
#define TAG_87_APP_PRIORITY_ID      0x87u     ///< Application Priority Indicator                    (@b 87) @n used for EMV_CT_CANDIDATE_DATA_STRUCT::API
#define TAG_88_SHORT_FILE_ID        0x88u     ///< Short File Identifier (SFI)                       (@b 88)
#define TAG_89_AUTH_CODE            0x89u     ///< Authorization Code                                (@b 89) @n used as @c AuthorizationCode in ::EMV_CT_HOST_STRUCT
#define TAG_8A_AUTH_RESP_CODE       0x8Au     ///< Authorization Response Code                       (@b 8A) @n used as @c AuthResp in ::EMV_CT_HOST_STRUCT
#define TAG_8C_CDOL_1               0x8Cu     ///< CDOL 1                                            (@b 8C)
#define TAG_8D_CDOL_2               0x8Du     ///< CDOL 2                                            (@b 8D)
#define TAG_8E_CVM_LIST             0x8Eu     ///< Cardholder Verification Method List               (@b 8E)
#define TAG_8F_CERTIF_AUTH_PK_ID    0x8Fu     ///< Certification Autority Public key Index           (@b 8F)
#define TAG_90_ISS_PK_CERTIF        0x90u     ///< Issuer Public Key Certificate                     (@b 90)
#define TAG_91_ISS_AUTH_DATA        0x91u     ///< Issuer Authentication Data                        (@b 91) @n used as @c AuthData in ::EMV_CT_HOST_STRUCT
#define TAG_92_ISS_PK_REMAINDER     0x92u     ///< Issuer Public Key Remainder                       (@b 92)
#define TAG_93_SGND_STAT_APP_DATA   0x93u     ///< Signed Static Application Data                    (@b 93)
#define TAG_94_AFL                  0x94u     ///< Application File Locator                          (@b 94)
#define TAG_95_TVR                  0x95u     ///< Terminal Verification Result                      (@b 95) @n used as @c T_95_TVR in ::EMV_CT_TRANSRES_STRUCT
#define TAG_97_TDOL                 0x97u     ///< TDOL                                              (@b 97)
#define TAG_98_TC_HASH_VALUE        0x98u     ///< Transaction Certificate Hash Value                (@b 98)
#define TAG_99_TRANS_PIN_DATA       0x99u     ///< Transaction PIN Data                              (@b 99)
#define TAG_9A_TRANS_DATE           0x9Au     ///< Transaction Date                                  (@b 9A) @n used as @c T_9A_Date in ::EMV_CT_TRANSRES_STRUCT, and @c Date in ::EMV_CT_SELECT_STRUCT
#define TAG_9B_TSI                  0x9Bu     ///< Transaction Status Information                    (@b 9B) @n used as @c T_9B_TSI in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9C_TRANS_TYPE           0x9Cu     ///< Transaction Type                                  (@b 9C) @n used as @c TransType in ::EMV_CT_SELECT_STRUCT, @c T_9C_TransType in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9D_DDF_NAME             0x9Du     ///< Directory Definition File Name                    (@b 9D)
#define TAG_9F01_ACQ_ID             0x9F01u   ///< Acquirer Identifier                               (@b 9F01)
#define TAG_9F02_NUM_AMOUNT_AUTH    0x9F02u   ///< Amount Authorized (Numeric)                       (@b 9F02) @n used as @c Betrag_num in ::EMV_CT_SELECT_STRUCT, @c puc9F02_amount in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC), and @c puc9F02_amount in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_9F03_NUM_AMOUNT_OTHER   0x9F03u   ///< Amount Other (Numeric)                            (@b 9F03)
#define TAG_9F04_BIN_AMOUNT_OTHER   0x9F04u   ///< Amount Other (Binary)                             (@b 9F04) @n used as @c T_9F06_AID in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F05_APP_DISCR_DATA     0x9F05u   ///< Application Discretionary Data                    (@b 9F05)
#define TAG_9F06_AID                0x9F06u   ///< Application Identifier (AID)-terminal             (@b 9F06) @n used as EMV_CT_TRANSRES_STRUCT::T_9F06_AID and EMV_CT_CANDIDATE_DATA_STRUCT::len9F06
#define TAG_9F07_APP_USAGE_CONTROL  0x9F07u   ///< Application Usage Control                         (@b 9F07)
#define TAG_9F08_ICC_APP_VERSION_NB 0x9F08u   ///< Application Version Number - ICC                  (@b 9F08) @n used as EMV_CT_TRANSRES_STRUCT::T_9F08_ICC_Appli_Vers_No
#define TAG_9F09_TRM_APP_VERSION_NB 0x9F09u   ///< Application Version Number                        (@b 9F09) @n used as EMV_CT_APPLIDATA_STRUCT::VerNum, @c T_9F09_VerNum in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F0A_ASRPD              0x9F0Au   ///< Application Selection Registered Proprietary Data (@b 9F0A) @n EMV Specification Bulletin No. 175 (ID1, L1, V1, ID2, L2, V2, ...) ID: 2 byte, L: length, 1 byte @n used as EMV_CT_CANDIDATE_DATA_STRUCT::selData
#define TAG_9F0B_CARDHOLDER_NAME_XT 0x9F0Bu   ///< Cardholder Name Extended                          (@b 9F0B)
#define TAG_9F0D_IAC_DEFAULT        0x9F0Du   ///< Issuer Action Code - Default                      (@b 9F0D) @n used as @c T_9F0D_IACDefault in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F0E_IAC_DENIAL         0x9F0Eu   ///< Issuer Action Code - Denial                       (@b 9F0E) @n used as @c T_9F0E_IACDenial in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F0F_IAC_ONLINE         0x9F0Fu   ///< Issuer Action Code - Online                       (@b 9F0F) @n used as @c T_9F0F_IACOnline in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F10_ISS_APP_DATA       0x9F10u   ///< Issuer Application Data                           (@b 9F10) @n used as @c T_9F10_DataIssuer in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F11_ISS_CODE_TABLE_ID  0x9F11u   ///< Issuer Code Table Index                           (@b 9F11) @n used as @c EMV_CT_SELECTRES_STRUCT::T_9F11_CodeTable and EMV_CT_CANDIDATE_DATA_STRUCT::CodeTableIndex
#define TAG_9F12_APP_PREFERRED_NAME 0x9F12u   ///< Application Preferred Name                        (@b 9F12) @n used as @c T_9F12_PreferredName in ::EMV_CT_SELECTRES_STRUCT
#define TAG_9F13_LAST_ONLINE_ATC    0x9F13u   ///< Last Online ATC Register                          (@b 9F13)
#define TAG_9F14_LO_OFFLINE_LIMIT   0x9F14u   ///< Lower Consecutive Offline Limit                   (@b 9F14)
#define TAG_9F15_MERCH_CATEG_CODE   0x9F15u   ///< Merchant Category Code                            (@b 9F15) @n used as EMV_CT_APPLIDATA_STRUCT::BrKey
#define TAG_9F16_MERCHANT_ID        0x9F16u   ///< Merchant Identifier                               (@b 9F16) @n used as EMV_CT_APPLIDATA_STRUCT::MerchIdent, @c T_9F16_MerchIdent in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F17_PIN_TRIES_LEFT     0x9F17u   ///< PIN Try Counter                                   (@b 9F17)
#define TAG_9F18_ISS_SCRIPT_ID      0x9F18u   ///< Issuer Script Identifier                          (@b 9F18)
#define TAG_9F1A_TRM_COUNTRY_CODE   0x9F1Au   ///< Terminal Country Code                             (@b 9F1A) @n used as EMV_CT_TERMDATA_STRUCT::TermCountryCode, EMV_CT_APPLIDATA_STRUCT::App_CountryCodeTerm, EMV_CT_TRANSRES_STRUCT::T_9F1A_TermCountryCode, and EMV_CT_CARDDATA_STRUCT::T_9F1A_TermCountryCode
#define TAG_9F1B_TRM_FLOOR_LIMIT    0x9F1Bu   ///< Terminal Floor Limit                              (@b 9F1B) @n used as EMV_CT_APPLIDATA_STRUCT::FloorLimit, @c puc9F1B_fl in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC), and @c puc9F1B_fl in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_9F1C_TRM_ID             0x9F1Cu   ///< Terminal Identification                           (@b 9F1C) @n used as EMV_CT_APPLIDATA_STRUCT::TermIdent
#define TAG_9F1D_TRM_RISK_MNGT_DATA 0x9F1Du   ///< Terminal Risk Management Data                     (@b 9F1D)
#define TAG_9F1E_IFD_SERIAL_NB      0x9F1Eu   ///< Interface Device (IFD) Serial Number              (@b 9F1E) @n used as @c SerNum in ::EMV_CT_TERMDATA_STRUCT, @c T_9F1E_SerNum in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F1F_TRACK_1_DISCR_DATA 0x9F1Fu   ///< Track 1 Discretionary Data                        (@b 9F1F)
#define TAG_9F20_TRACK_2_DISCR_DATA 0x9F20u   ///< Track 2 Discretionary Data                        (@b 9F20)
#define TAG_9F21_TRANS_TIME         0x9F21u   ///< Transaction Time                                  (@b 9F21) @n used as @c Time in ::EMV_CT_SELECT_STRUCT, @c T_9F21_Time in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F22_CERTIF_AUTH_PK_ID  0x9F22u   ///< Certification Authority Public Key Index          (@b 9F22)
#define TAG_9F23_HI_OFFLINE_LIMIT   0x9F23u   ///< Upper Consecutive Offline Limit                   (@b 9F23)
#define TAG_9F24_PAYM_ACCOUNT_REF   0x9F24u   ///< Payment Account Reference                         (@b 9F24) @n EMV Specification Bulletin No. 178, an29
#define TAG_9F26_APP_CRYPTOGRAM     0x9F26u   ///< Application Cryptogram                            (@b 9F26) @n used as @c T_9F26_Cryptogramm in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F27_CRYPT_INFO_DATA    0x9F27u   ///< Cryptogram Information Data                       (@b 9F27) @n used as @c T_9F27_CryptInfo in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F2D_ICC_PIN_PK_CERTIF  0x9F2Du   ///< ICC PIN Encipherment Public Key Certificate       (@b 9F2D)
#define TAG_9F2E_ICC_PIN_PK_EXP     0x9F2Eu   ///< ICC PIN Encipherment Public Key Exponent          (@b 9F2E)
#define TAG_9F2F_ICC_PIN_PK_REMAIN  0x9F2Fu   ///< ICC PIN Encipherment Public Key Remainder         (@b 9F2F)
#define TAG_9F32_ISS_PK_EXP         0x9F32u   ///< Issuer Public Key Exponent                        (@b 9F32)
#define TAG_9F33_TRM_CAPABILITIES   0x9F33u   ///< Terminal Capabilities                             (@b 9F33) @n used as @c TermCap in ::EMV_CT_TERMDATA_STRUCT, EMV_CT_APPLIDATA_STRUCT::App_TermCap, @c T_9F33_TermCap in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F34_CVM_RESULTS        0x9F34u   ///< Cardholder Verification Method (CVM) Results      (@b 9F34) @n used as @c T_9F34_CVM_Res in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F35_TRM_TYPE           0x9F35u   ///< Terminal Type                                     (@b 9F35) @n used as @c TermTyp in ::EMV_CT_TERMDATA_STRUCT, @c T_9F35_TermTyp in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F36_ATC                0x9F36u   ///< Application Transaction Counter (ATC)             (@b 9F36) @n used as @c T_9F36_ATC in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F37_UNPREDICTABLE_NB   0x9F37u   ///< Unpredictable Number                              (@b 9F37) @n used as @c T_9F37_RandomNumber in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F38_PDOL               0x9F38u   ///< Processing Options Data Object List (PDOL)        (@b 9F38)
#define TAG_9F39_POS_ENTRY_MODE     0x9F39u   ///< Point-of-Service (POS) Entry Mode                 (@b 9F39) @n used as EMV_CT_APPLIDATA_STRUCT::POS_Eing, @c T_9F39_POSEntryMode in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F3A_AMNT_REF_CURRENCY  0x9F3Au   ///< Amount, Reference Currency                        (@b 9F3A) @n used as @c Betrag_num in ::EMV_CT_SELECT_STRUCT
#define TAG_9F3B_APP_REF_CURRENCY   0x9F3Bu   ///< Application Reference Currency                    (@b 9F3B)
#define TAG_9F3C_TRANS_REF_CURRENCY 0x9F3Cu   ///< Transaction Reference Currency Code               (@b 9F3C) @n In case CommonServices request this tag EMV ADK will give back same as #TAG_5F2A_TRANS_CURRENCY.
#define TAG_9F3D_TRANS_CURRENCY_EXP 0x9F3Du   ///< Transaction Reference Currency Exponent           (@b 9F3D) @n In case CommonServices request this tag EMV ADK will give back same as #TAG_5F36_TRANS_CURRENCY_EXP.
#define TAG_9F40_ADD_TRM_CAP        0x9F40u   ///< Additional Terminal Capabilities                  (@b 9F40) @n used as @c TermAddCap in ::EMV_CT_TERMDATA_STRUCT, EMV_CT_APPLIDATA_STRUCT::App_TermAddCap, @c T_9F40_AddTermCap in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F41_TRANS_SEQUENCE_NB  0x9F41u   ///< Transaction Sequence Counter                      (@b 9F41) @n used as @c TransCount in ::EMV_CT_TRANSAC_STRUCT, @c T_9F41_TransCount in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F42_APP_CURRENCY_CODE  0x9F42u   ///< Application Currency Code                         (@b 9F42) @n used as @c puc9F42_acc in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC), @c puc9F42_acc in @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_9F43_APP_REF_CURRCY_EXP 0x9F43u   ///< Application Reference Currency Exponent           (@b 9F43)
#define TAG_9F44_APP_CURRENCY_EXP   0x9F44u   ///< Application Currency Exponent                     (@b 9F44)
#define TAG_9F45_DATA_AUTHENT_CODE  0x9F45u   ///< Data Authentication Code                          (@b 9F45) @n used as @c T_9F45_DataAuthCode in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F46_ICC_PK_CERTIF      0x9F46u   ///< ICC Public Key Certificate                        (@b 9F46)
#define TAG_9F47_ICC_PK_EXP         0x9F47u   ///< ICC Public Key Exponent                           (@b 9F47)
#define TAG_9F48_ICC_PK_REMAINDER   0x9F48u   ///< ICC Public Key Remainder                          (@b 9F48)
#define TAG_9F49_DDOL               0x9F49u   ///< Dynamic Data Authentication Data Object List      (@b 9F49)
#define TAG_9F4A_SDA_TAG_LIST       0x9F4Au   ///< Static Data Authentication Tag List               (@b 9F4A)
#define TAG_9F4B_SDA_DATA           0x9F4Bu   ///< Signed Dynamic Application Data                   (@b 9F4B)
#define TAG_9F4C_ICC_DYNAMIC_NB     0x9F4Cu   ///< ICC Dynamic Number                                (@b 9F4C) @n used as @c T_9F4C_ICCDynNumber in ::EMV_CT_TRANSRES_STRUCT
#define TAG_9F4E_TAC_MERCHANTLOC    0x9F4Eu   ///< Merchant name and location (VISA VCPS 2.1.1)      (@b 9F4E)
#define TAG_A5_FCI_PROPR_TPLT       0xA5u     ///< FCI Proprietary Template                          (@b A5)
#define TAG_BF0C_FCI_ISS_DISCR      0xBF0Cu   ///< FCI Issuer Discretionary Data                     (@b BF0C)
/// @}

/// @defgroup VISA_TAGS Propriertary tags used by Visa
/// @ingroup EMV_TAGS
/// @brief see [Visa_Card], page A-63
/// @{
//#define TAG_9F51_APP_CURRENCY_CODE           0x9F51 ///< Application Currency Code
//#define TAG_9F52_DEFAULT_ACTION              0x9F52 ///< Application Default Action (ADA)
//#define TAG_9F53_CONSECUTIVE_LIM_TRANS_INT   0x9F53 ///< Consecutive Transaction Limit (International)
//#define TAG_9F54_CUMULATIVE_TRANS_LIMET      0x9F54 ///< Cumulative Total Transaction Amount Limit
//#define TAG_9F56_ISS_AUTH_INCATOR            0x9F56 ///< Issuer Authentication Indicator
//#define TAG_9F5D_VISA_AOSA                   0x9F5D ///< Contactless: Available Offline Spending Amount (AOSA) @n used as @c T_9F5D_CL_VISA_AOSA in ::EMV_CT_TRANSRES_STRUCT
//#define TAG_9F66_VISA_TTQ                    0x9F66 ///< VISA TTQ (Contactless only)
//#define TAG_9F6C_VISA_CTQ                    0x9F6C ///< VISA CTQ (Contactless only)
/// @}

/// @defgroup MC_TAGS Proprietary tags used by MasterCard
/// @ingroup EMV_TAGS
/// @brief see [MC_FA], page 7-78
/// @{
#define TAG_9F53_TRANS_CATEGORY_CODE         0x9F53 ///< Transaction Category Code, 1 byte binary
/// @}


// ========================================================================================================
// === Verifone internal tags ===
// ========================================================================================================

/// @defgroup VERI_TAGS Verifone internal tags
/// @ingroup ingroup EMV_TAGS
/// @brief There are many data objects, which do not have an EMVCo defined tags.
///        For usage of TLV interface it's necessary to define internal tags.

/// @defgroup VERI_CONSTR_TAGS Constructed tags
/// @ingroup VERI_TAGS
/// @{
#define TAG_E2_FORMAT_B             0xE2    ///< Data objects with format "b" @n Used for EMV_CT_APPLIDATA_STRUCT::Additional_Tags
#define TAG_E3_FORMAT_N             0xE3    ///< Data objects with format "n" @n Used for EMV_CT_APPLIDATA_STRUCT::Additional_Tags
#define TAG_E4_FORMAT_CN            0xE4    ///< Data objects with format "cn" @n Used for EMV_CT_APPLIDATA_STRUCT::Additional_Tags
#define TAG_E5_FORMAT_A             0xE5    ///< Data objects with format "a" @n Used for EMV_CT_APPLIDATA_STRUCT::Additional_Tags
#define TAG_E6_FORMAT_AN            0xE6    ///< Data objects with format "an" @n Used for EMV_CT_APPLIDATA_STRUCT::Additional_Tags
#define TAG_E7_FORMAT_ANS           0xE7    ///< Data objects with format "ans" @n Used for EMV_CT_APPLIDATA_STRUCT::Additional_Tags

#define TAG_F0_EMV_TEMPLATE         0xF0    ///< All BER TLV streams are packed in this tag.

#define TAG_DOM_CHIP                0xFF01  ///< constructed tag for domestic chip applications
#define TAG_FALLBACK_MSR            0xFF02  ///< constructed tag for MSR data
#define TAG_CAND_LIST               0xFF03  ///< constructed tag for candidate list, used for EMV_CT_CANDIDATE_TYPE, EMV_CT_CandListType and within EMV_CT_updateTxnTags
#define TAG_KEY                     0xFF04  ///< constructed tag for CAP keys
/// @}

/// @defgroup VERI_PRIM_TAGS Primitive tags
/// @ingroup VERI_TAGS
/// @{
#define TAG_ISO_DATA                0xDF01  ///< Tag for ISO data
#define TAG_TRACE                   0xDF02  ///< Tag for trace data
#define TAG_KEY_NUMBER              0xDF03  ///< CAP Key Number of Keys, 1 byte 0...255
#define TAG_DF04_AID                0xDF04  ///< used for EMV_CT_CANDIDATE_STRUCT::candidate, AID in ::EMV_CT_APPLI_STRUCT, TRM_GetAppliData(), TRM_SelAppli(), TRM_ReduceCandidateList()
#define TAG_DF05_BUILD_APPLILIST    0xDF05  ///< EMV_CT_SELECT_STRUCT::InitTXN_Buildlist
#define TAG_DF06_CARDREADER_NUM     0xDF06  ///< L1 reader options @n@c ucOptions in EMV_CT_SmartISO()
#define TAG_DF07_UNCRIT_SCR         0xDF07  ///< Script results of uncritical scripts in response to EMV_CT_ContinueOnline() @n@c EMV_CT_SRCRIPTRES_TYPE::ScriptUnCritResult
#define TAG_DF08_CRIT_SCR           0xDF08  ///< Script results of critical scripts in response to EMV_CT_ContinueOnline() @n@c EMV_CT_SRCRIPTRES_TYPE::ScriptCritResult
#define TAG_KEY_INDEX               0xDF09  ///< CAP Key Index @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_INDEX, @n Index in EMV_CT_CAPKEY_STRUCT::Index, @n XML Tag: #XML_TAG_CAP_KEYS_INDEX
#define TAG_KEY_RID                 0xDF0A  ///< CAP Key RID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_RID, @n RID in EMV_CT_CAPKEY_STRUCT::RID, @n XML Tag: #XML_TAG_CAP_KEYS_RID
#define TAG_KEY_KEY                 0xDF0B  ///< CAP Key Modulus @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_KEY, @n Key in EMV_CT_CAPKEY_STRUCT::Key, @n XML Tag: #XML_TAG_CAP_KEYS_KEY
#define TAG_KEY_HASH                0xDF0C  ///< CAP Key Hash @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_HASH, @n Hash in EMV_CT_CAPKEY_STRUCT::Hash, @n XML Tag: #XML_TAG_CAP_KEYS_HASH
#define TAG_KEY_EXPONENT            0xDF0D  ///< CAP Key Exponent @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_EXPONENT, @n Exponent in EMV_CT_CAPKEY_STRUCT::Exponent, @n XML Tag: #XML_TAG_CAP_KEYS_EXPONENT
#define TAG_KEY_CRL                 0xDF0E  ///< CAP Key Certification Revocation List @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_CRL, @n RevocEntries in EMV_CT_CAPKEY_STRUCT::RevocEntries, @n XML Tag: #XML_TAG_CAP_KEYS_REVOC_LIST

#define TAG_DF11_LIB_VERSION        0xDF11  ///< Library version given back from EMV_CT_GetTermData() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF11_LIB_VERSION, @n FrameworkVersion in EMV_CT_TERMDATA_STRUCT::FrameworkVersion, @n XML Tag: #XML_TAG_TERMDATA_FRAMEWORK_VERSION
#define TAG_DF12_CHECKSUM           0xDF12  ///< EMVCo checksum given back from EMV_CT_GetTermData() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF12_CHECKSUM, @n ChksumASCIIEMVCO in EMV_CT_APPLIDATA_STRUCT::ChksumASCIIEMVCO, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_EMVCO
#define TAG_DF13_TERM_PARAM         0xDF13  ///< used for EMV_CT_APPLIDATA_STRUCT::Chksum_Params
#define TAG_DF14_ADD_TAGS_RES       0xDF14  ///< @deprecated Don't use EMV_CT_PAYMENT_STRUCT::Additional_Result_Tags anymore. Use EMV_CT_fetchTxnTags() instead.
#define TAG_DF15_OFFL_ONLY_PROCESS  0xDF15  ///< used for EMV_CT_APPLIDATA_STRUCT::ucAC_before_after
#define TAG_KEY_KEYLEN              0xDF16  ///< CAP Key key length @n@c Struct, @c XML Reference: @n KeyLen in EMV_CT_CAPKEY_STRUCT::KeyLen, @n XML Tag: #XML_TAG_CAP_KEYS_KEYLEN
#define TAG_DF17_FALLBACK_MIDS      0xDF17  ///< used for EMV_CT_APPLIDATA_STRUCT::tucFallbackMIDs
#define TAG_DF18_FALLABCK           0xDF18  ///< tag for EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling
#define TAG_DF19_PARAMETER_4        0xDF19  ///< RFU
#define TAG_DF1C_SPECIAL_TRX        0xDF1C  ///< tag for EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX
#define TAG_DF1D_PRIO_APPLI         0xDF1D  ///< each element of EMV_CT_APPLIDATA_STRUCT::xAIDPrio is packed in this tag

#define TAG_DF20_ASI                0xDF20  ///< used for EMV_CT_APPLIDATA_STRUCT::ASI, EMV_CT_DOM_CHIP_STRUCT::ucASI
#define TAG_DF21_TAC_DENIAL         0xDF21  ///< see EMV_CT_APPLIDATA_STRUCT::TACDenial  and ::EMV_CT_TRANSRES_STRUCT @n also used for callback @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)
#define TAG_DF21_TAC_ABLEHNUNG      TAG_DF21_TAC_DENIAL  ///< @deprecated use @ref TAG_DF21_TAC_DENIAL instead
#define TAG_DF22_TAC_ONLINE         0xDF22  ///< see EMV_CT_APPLIDATA_STRUCT::TACOnline  and ::EMV_CT_TRANSRES_STRUCT @n also used for callback @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)
#define TAG_DF23_TAC_DEFAULT        0xDF23  ///< see EMV_CT_APPLIDATA_STRUCT::TACDefault and ::EMV_CT_TRANSRES_STRUCT @n also used for callback @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)
#define TAG_DF24_THRESHHOLD         0xDF24  ///< see EMV_CT_APPLIDATA_STRUCT::Threshhold  @n also used for callback @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_DF25_MAXPERCENT_ONL     0xDF25  ///< see EMV_CT_APPLIDATA_STRUCT::MaxTargetPercentage
#define TAG_DF26_PERCENT_ONL        0xDF26  ///< see EMV_CT_APPLIDATA_STRUCT::TargetPercentage
#define TAG_DF27_DEFAULT_TDOL       0xDF27  ///< see EMV_CT_APPLIDATA_STRUCT::Default_TDOL
#define TAG_DF28_DEFAULT_DDOL       0xDF28  ///< see EMV_CT_APPLIDATA_STRUCT::Default_DDOL
#define TAG_DF29_ADD_TAGS           0xDF29  ///< see EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM
#define TAG_DF2A_DUTY_TAGS          0xDF2A  ///< see EMV_CT_APPLIDATA_STRUCT::Taglist
#define TAG_DF2B_APP_FLOW_CAP       0xDF2B  ///< see EMV_CT_APPLIDATA_STRUCT::App_FlowCap
#define TAG_DF2C_ADD_TAGS_CRD       0xDF2C  ///< see EMV_CT_APPLIDATA_STRUCT::Additional_Tags_CRD
#define TAG_DF2D_EMV_APPLI          0xDF2D  ///< see EMV_CT_APPLIDATA_STRUCT::EMV_Application
#define TAG_DF2E_CVM_NOT_SUPP       0xDF2E  ///< see EMV_CT_APPLIDATA_STRUCT::AIP_CVM_not_supported
#define TAG_DF2F_CVM_CUSTOM         0xDF2F  ///< see EMV_CT_APPLIDATA_STRUCT::Customer_CVM

#define TAG_DF30_LANG               0xDF30  ///< see customer language index (@ref TF_LANGUAGES), format is like in EMV_CT_PAYMENT_STRUCT::PreSelected_Language
#define TAG_DF31_TEXT_NUM           0xDF31  ///< text index, format is like in EMV_CT_SELECTRES_STRUCT::T_DF63_DisplayText
#define TAG_DF33_APPLICATION_TAG_50 0xDF33  ///< EMV_CT_SELECTRES_STRUCT::T_50_ApplicationName
#define TAG_DF34_AMOUNT_CONF        0xDF34  ///< EMV_CT_TRANSAC_STRUCT::uc_AmountConfirmation
#define TAG_DF36_TRX_OPTIONS        0xDF36  ///< EMV_CT_TRANSAC_STRUCT::tucTxnOptions
#define TAG_DF37_TRX_STEPS          0xDF37  ///< EMV_CT_TRANSAC_STRUCT::TxnSteps
#define TAG_DF39_DCC_CBCK_INFO      0xDF39  ///< @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
#define TAG_DF3A_FB_MSR_OPTIONS     0xDF3A  ///< EMV_CT_APPS_SELECT_STRUCT::xFallback_MS (partially, together with #TAG_DF4C_FALLB_MSR_APPLI)
#define TAG_DF3B_PARAMETER_1        0xDF3B  ///< RFU
#define TAG_DF3C_PARAMETER_2        0xDF3C  ///< RFU
#define TAG_DF3D_PARAMETER_3        0xDF3D  ///< RFU
#define TAG_DF3E_CDA_PROC           0xDF3E  ///< EMV_CT_APPLIDATA_STRUCT::uc_CDA_Processing

#define TAG_DF40_FORCE_ONLINE       0xDF40  ///< EMV_CT_SELECT_STRUCT::Force_Online
#define TAG_DF41_PIN_BYPASS         0xDF41  ///< If #PIN_BYPASS was configured in EMV_CT_APPLIDATA_STRUCT::App_FlowCap: This tag is given by EMV ADK in PIN entry callback (@ref EMV_CT_CALLBACK_FnT with #TAG_BF08_CBK_PIN).
#define TAG_DF42_STATUS             0xDF42  ///< EMV_CT_TRANSRES_STRUCT::StatusInfo
#define TAG_DF43_FORCE_ACCEPT       0xDF43  ///< EMV_CT_SELECT_STRUCT::Force_Acceptance
#define TAG_DF45_NO_DIR_SELECT      0xDF45  ///< EMV_CT_SELECT_STRUCT::No_DirectorySelect
#define TAG_DF47_PRIOR_LANG_SEL     0xDF47  ///< EMV_CT_SELECT_STRUCT::PreSelected_Language and EMV_CT_TRANSAC_STRUCT::PreSelected_Language
#define TAG_DF48_ONLINE_SWITCH      0xDF48  ///< EMV_CT_SELECT_STRUCT::Online_Switch
#define TAG_DF49_APL_SEC_LIMIT      0xDF49  ///< EMV_CT_APPLIDATA_STRUCT::Security_Limit, limit below which other terminal capabilties apply (optional)
#define TAG_DF4A_APL_SEC_CAPS       0xDF4A  ///< EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, terminal capabilties below the security limit (optio
#define TAG_DF4B_DOM_CHIP_APPLI     0xDF4B  ///< EMV_CT_SELECT_STRUCT::countDomesticChip
#define TAG_DF4C_FALLB_MSR_APPLI    0xDF4C  ///< EMV_CT_APPS_SELECT_STRUCT::countFallbackMS and EMV_CT_APPS_SELECT_STRUCT::xFallback_MS, EMV_CT_SELECTRES_STRUCT::countFallbackMS and EMV_CT_SELECTRES_STRUCT::xFallback_MS (partially, together with #TAG_DF3A_FB_MSR_OPTIONS)
#define TAG_DF4D_CARD_CONF          0xDF4D  ///< EMV_CT_SELECT_STRUCT::ucCardholderConfirmation
#define TAG_DF4E_PDOL_INFO          0xDF4E  ///< EMV_CT_SELECT_STRUCT::PDOL_tags

#define TAG_DF50_ONL_RES            0xDF50  ///< EMV_CT_HOST_STRUCT::OnlineResult
#define TAG_DF51_ISS_REF_RES        0xDF51  ///< EMV_CT_HOST_STRUCT::Result_referral
#define TAG_DF52_AUTH_DATA          0xDF52  ///< EMV_CT_HOST_STRUCT::LenAuth and EMV_CT_HOST_STRUCT::AuthData
#define TAG_DF53_SCRIPT_CRIT        0xDF53  ///< EMV_CT_HOST_STRUCT::LenScriptCrit and EMV_CT_HOST_STRUCT::ScriptCritData
#define TAG_DF54_SCRIPT_UNCRIT      0xDF54  ///< EMV_CT_HOST_STRUCT::LenScriptUnCrit and EMV_CT_HOST_STRUCT::ScriptUnCritData
#define TAG_DF55_AC_ISS_REF         0xDF55  ///< EMV_CT_HOST_STRUCT::AuthResp_Referral
#define TAG_DF56_AC_WRONG_PIN       0xDF56  ///< EMV_CT_HOST_STRUCT::AuthResp_Wrong_PIN
#define TAG_DF57_AC_ADD_OK          0xDF57  ///< EMV_CT_HOST_STRUCT::AuthResp_Positive
#define TAG_DF59_OFFL_PIN_ERRORS    0xDF59  ///< Number of wrong PIN entries (only in case of offline PIN!!!) EMV_CT_TRANSRES_STRUCT::T_DF59_Offl_PIN_errors
#define TAG_DF5B_DCC_PROHIBIT       0xDF5B  ///< @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS)
#define TAG_DF5C_DOM_INFO           0xDF5C  ///< @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS)
#define TAG_DF5D_DOM_OPTION         0xDF5D  ///< @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS)
#define TAG_DF5F_ADD_APP_VERSION    0xDF5F  ///< see EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No

#define TAG_DF60_VELOCITY_ORIG_IDX  0xDF60  ///< original candidate index in the list of application before it was presented to the Velocity kernel for applications selection (The kernel changes the order of the list according to different "priority" / "partial select" rules, the index keeps the refrence to the original candidate list
#define TAG_DF61_INF_REC_DATA       0xDF61  ///< EMV_CT_SELECTRES_STRUCT::T_DF61_Info_Received_Data, EMV_CT_TRANSRES_STRUCT::T_DF61_Info_Received_Data and EMV_CT_CARDDATA_STRUCT::T_DF61_Info_Received_Data
#define TAG_DF62_ERROR_DATA         0xDF62  ///< Transaction error information according to "Fehlerkennung" (German ZKA specification) @n EMV_CT_SELECTRES_STRUCT::T_DF62_ErrorData, EMV_CT_TRANSRES_STRUCT::T_DF62_ErrorData and EMV_CT_CARDDATA_STRUCT::T_DF62_ErrorData
#define TAG_DF62_APPLI_SELECT_FLAG  0xDF62u ///< Application Selection Flag ('DF62', Interac, Canada), EMV_CT_CANDIDATE_DATA_STRUCT::DF62_ASF
#define TAG_DF63_DISPLAY_TEXT       0xDF63  ///< EMV_CT_SELECTRES_STRUCT::T_DF63_DisplayText and EMV_CT_TRANSRES_STRUCT::T_DF63_DisplayText
#define TAG_DF64_KERNEL_DEBUG       0xDF64  ///< EMV_CT_TRANSRES_STRUCT::T_DF64_KernelDebugData
#define TAG_KERNEL_VERSION          0xDF65  ///< Kernel version (EMV_CT_TERMDATA_STRUCT::KernelVersion)
#define TAG_HANDLE_APPLI_TYPE       0xDF66  ///< Configuration mode for applications @ref APPLI_CONF_MODE, see @c eHandleCAPKeyType in call of EMV_CT_StoreCAPKey()
#define TAG_SUPP_LANG               0xDF67  ///< Supported languages (EMV_CT_TERMDATA_STRUCT::SuppLang)
#define TAG_ATR                     0xDF68  ///< Answer to Reset from ICC (@c EMV_CT_SmartReset in call of EMV_CT_SmartReset()), n byte binary
#define TAG_EXCLUDE_AID             0xDF69  ///< AIDs to exclude from the configuration list for this transaction (EMV_CT_APPS_SELECT_STRUCT::ExcludeEmvAIDs inside EMV_CT_SELECT_STRUCT::SEL_Data)
#define TAG_EMV_CONFORM             0xDF6A  ///< EMV compliant app or not (e.g. domestic app) @n EMV_CT_DOM_CHIP_STRUCT::uc_EMVConformSelect
#define TAG_L1DRIVER_VERSION        0xDF6B  ///< L1 driver version given back from EMV_CT_GetTermData() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_L1DRIVER_VERSION, @n L1DriverVersion in EMV_CT_TERMDATA_STRUCT::L1DriverVersion, @n XML Tag: #XML_TAG_TERMDATA_L1DRIVER_VERSION
#define TAG_CONFIG_ID               0xDF6C  ///< Unique id generated when terminal configuration has changed and the option #EMV_CT_INIT_OPT_EPP_MASTER is set
#define TAG_DF6D_MODEL_NAME         0xDF6D  ///< Model included in EMV_CT_GetTermData() and EMV_CTLS_GetTermData() derived for use with emv-desired.xml and emv-allowed.xml.
#define TAG_DF6E_CONFIG_DESIRED     0xDF6E  ///< Desired L2 kernel list

#define TAG_DF8F01_MANI_CANDLIST    0xDF8F01  ///< Modified candidate list (only valid in reentrance mode using the candidate list manipulation option EMV_CT_APPS_SELECT_STRUCT::ModifiedCandidates inside EMV_CT_SELECT_STRUCT::SEL_Data)
/// @}

/// @defgroup TLV_CBCK_TLV Tags for callback functions
/// @ingroup VERI_PRIM_TAGS
/// @brief Used in function @ref EMV_CT_CALLBACK_FnT
/// @{
// callback function identifiers
#define TAG_DF6D_CBK_CARDHOLDER_RET 0xDF6D  ///< return value of cardholder information
#define TAG_DF6E_CBK_CARDHOLDERINFO 0xDF6E  ///< cardholder information during transaction, e.g. Wrong PIN, see @ref eCardholderInfo
#define TAG_DF6F_CBK_PIN_ICC_RESP   0xDF6F  ///< @deprecated ICC answer to VERIFY command (2 byte SW1/2), not needed anymore, EMV ADK stores this info internally during EMV_CT_Send_PIN_Offline()
#define TAG_DF70_CBK_MERCHINFO      0xDF70  ///< MerchantInfo(), see #EMV_ADK_MerchantInfo
#define TAG_DF71_CBK_AMOUNTCONF     0xDF71  ///< @c amount confirmation result (TRUE/FALSE)
#define TAG_DF72_CBK_BLACKLIST      0xDF72  ///< Indicator if used PAN is in blacklist, TRUE/FALSE
#define TAG_DF73_CBK_TRANSLOG       0xDF73  ///< @c stored amount of the selected PAN
#define TAG_DF74_CBK_SELECTOR       0xDF74  ///< see @c ucSelector, allowed values see @ref DEF_CBK_SEL
#define TAG_DF75_CBK_APP_NO         0xDF75  ///< If length == 1: Return value of terminal selection process == chosen candidate, otherwise reduced candidate list
#define TAG_DF6C_CBK_REPEAT         0xDF6C  ///< Decide if ADK should repeat the callback call or not @n (1 byte, 1=Repeat/0=Do not repeat). If absent: Do not repeat @n see #TAG_BF04_CBK_REDUCE_CAND
#define TAG_DF76_CBK_MANIPUL_TRX    0xDF76  ///< Return value to EMV ADK in callback @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS) @n Possible values: @li #EMV_ADK_ARQC to let transaction continue but force to go online @li #EMV_ADK_AAC to let transaction terminate with AAC in GENAC1 @li #EMV_ADK_ABORT and any other value to let transaction abort
#define TAG_DF77_CBK_LANGUAGE       0xDF77  ///< @deprecated EMV ADK does not send selected customer language and it ignores this parameter if put in
#define TAG_DF78_CBK_SEL_OPT        0xDF78  ///< @c tucOptions in ::EMV_CT_SELECT_STRUCT
#define TAG_DF79_CBK_PIN_INFO       0xDF79  ///< EMV ADK to application: PIN type requested (@ref ADK_PIN_INPUT) @n Application to EMV ADK: PIN entry result (see @ref ADK_PIN_RETURN)
#define TAG_DF7A_CBK_PIN_KEY_DATA   0xDF7A  ///< PIN (ICC) key modulus
#define TAG_DF7B_CBK_PIN_KEY_EXP    0xDF7B  ///< PIN (ICC) key exponent
#define TAG_DF7C_CBK_MERCHINFO_RET  0xDF7C  ///< transfer return value of merchant information callback (EMV_CT_CALLBACK_FnT with TAG_BF01_CBK_MERCHINFO) to EMV ADK
#define TAG_DF7D_CBK_DCC_CHECK      0xDF7D  ///< DCC mode, see @ref DCC_MODE
#define TAG_DF7E_CBK_DOM_APP_RES    0xDF7E  ///< result of domestic app processing: keep in list or remove from list (@ref DEF_CBK_DOMAPP)
#define TAG_DF7F_CBK_COMM_ERR       0xDF7F  ///< During execution of callback function a communication error occured. @n Indicator to be set by calling application. @n Will result in abort of transaction (#EMV_ADK_INTERNAL)

#define TAG_DFD001_MANI_CANDLIST    0xDFD001  ///< Modified candidate list (only valid in reentrance mode using the candidate list manipulation option EMV_CT_APPS_SELECT_STRUCT::ModifiedCandidates inside EMV_CT_SELECT_STRUCT::SEL_Data)
#define TAG_DFD002_APPNAME_CHOSEN   0xDFD002  ///< chosen application name based on the combination 9F12, 9F11, 50, application default name
#define TAG_DFD003_STORED_AMOUNT    0xDFD003  ///< configure transaction log amount via EMV_CT_updateTxnTag for the given card PAN when using re-entrance mode
#define TAG_DFD004_TXN_TYPES_SALE   0xDFD004  ///< Additional Transaction Types for sale (length 1..3). See description of EMV_CT_APPLIDATA_STRUCT::TxnTypesSale
#define TAG_DFD005_TXN_TYPES_CASH   0xDFD005  ///< Additional Transaction Types for cash (length 1..3). See description of EMV_CT_APPLIDATA_STRUCT::TxnTypesCash
#define TAG_DFD006_TXN_TYPES_CASHBACK 0xDFD006///< Additional Transaction Types for cashback (length 1..3). See description of EMV_CT_APPLIDATA_STRUCT::TxnTypesCashback
#define TAG_DFD007_TXN_TYPES_REFUND 0xDFD007  ///< Additional Transaction Types for refund (length 1..3). See description of EMV_CT_APPLIDATA_STRUCT::TxnTypesRefund
/// @}

/* 3-Byte-Tags: */
//used in CTLS: #define TAG_DF8F0A_EXIT_OPTIONS     0xDF8F0A



/* 3-Byte-Tags: */
/// @defgroup VERI_PRIM_TAGS_3BYTE 3-byte primitive tags
/// @brief For all unsigned long variables in functional interface, please use 4 byte in the serialization
/// @ingroup VERI_PRIM_TAGS
/// @{
#define TAG_DF8F0A_EXIT_OPTIONS           0xDF8F0A   ///< Options send with Exit_Framework, see @ref EXIT_FW_OPTIONS
#define TAG_DF8F0F_APPLYCONFIG_OPTIONS    0xDF8F0F   ///< Options send with EMV_CT_ApplyConfiguration(), unsigned long in functional interface, please use 4 byte in the serialization
#define TAG_DF8F30_REQUESTED_TAGS         0xDF8F30   ///< for requestedTags parameter in EMV_CTLS_fetchTxnTags()
/// @}

// range 0xDFD9xx reserved for encryption manager
// range 0xDFDAxx reserved for msr
// range 0xDFDBxx reserved for technology selection


/// @defgroup VERI_L1_DUMP_TAGS Tags for L1 dump data
/// @ingroup VERI_TAGS
/// @{
// tags for L1 Status
#define TAG_DFDD01_L1_ISSUE    				    0xDFDD01
#define TAG_DFDD02_L1_ISSUE_REASON		    0xDFDD02
#define TAG_DFDD03_L1_ISSUE_COMMAND		    0xDFDD03
#define TAG_DFDD04_L1_LAST_COMMAND		    0xDFDD04
#define TAG_DFDD05_L1_LAST_SW12   		    0xDFDD05

// tags for PSE dump
#define NO_PSE_RECS                       10
#define TAG_DFDF01_PSE_1       				    0xDFDF01
#define TAG_DFDF02_PSE_2       				    0xDFDF02
#define TAG_DFDF03_PSE_3       				    0xDFDF03
#define TAG_DFDF04_PSE_4       				    0xDFDF04
#define TAG_DFDF05_PSE_5       				    0xDFDF05
#define TAG_DFDF06_PSE_6       				    0xDFDF06
#define TAG_DFDF07_PSE_7       				    0xDFDF07
#define TAG_DFDF08_PSE_8       				    0xDFDF08
#define TAG_DFDF09_PSE_9       				    0xDFDF09
#define TAG_DFDF10_PSE_10       				  0xDFDF0A

// tags for LOA processing
#define NO_LOA_RECS                       15
#define TAG_DFDF11_LOA_1       				    0xDFDF11
#define TAG_DFDF12_LOA_2       				    0xDFDF12
#define TAG_DFDF13_LOA_3       				    0xDFDF13
#define TAG_DFDF14_LOA_4       				    0xDFDF14
#define TAG_DFDF15_LOA_5       				    0xDFDF15
#define TAG_DFDF16_LOA_6       				    0xDFDF16
#define TAG_DFDF17_LOA_7       				    0xDFDF17
#define TAG_DFDF18_LOA_8       				    0xDFDF18
#define TAG_DFDF19_LOA_9       				    0xDFDF19
#define TAG_DFDF1A_LOA_10       				  0xDFDF1A
#define TAG_DFDF1B_LOA_11       				  0xDFDF1B
#define TAG_DFDF1C_LOA_12       				  0xDFDF1C
#define TAG_DFDF1D_LOA_13       				  0xDFDF1D
#define TAG_DFDF1E_LOA_14       				  0xDFDF1E
#define TAG_DFDF1F_LOA_15       				  0xDFDF1F

// tags for Final Select
#define NO_FS_RECS                        5
#define TAG_DFDF21_FINALS_1    				    0xDFDF21
#define TAG_DFDF22_FINALS_2     				  0xDFDF22
#define TAG_DFDF23_FINALS_3     				  0xDFDF23
#define TAG_DFDF24_FINALS_4     				  0xDFDF24
#define TAG_DFDF25_FINALS_5     				  0xDFDF25

// tags for GPO
#define NO_GETPROC_RECS                   5
#define TAG_DFDF29_GETPROC_1    				  0xDFDF29
#define TAG_DFDF2A_GETPROC_2    				  0xDFDF2A
#define TAG_DFDF2B_GETPROC_3    				  0xDFDF2B
#define TAG_DFDF2C_GETPROC_4    				  0xDFDF2C
#define TAG_DFDF2D_GETPROC_5    				  0xDFDF2D

// tags for ReadRecords
#define NO_RR_RECS                        15
#define TAG_DFDF31_RR_1       				    0xDFDF31
#define TAG_DFDF32_RR_2       				    0xDFDF32
#define TAG_DFDF33_RR_3         				  0xDFDF33
#define TAG_DFDF34_RR_4       		  		  0xDFDF34
#define TAG_DFDF35_RR_5       			  	  0xDFDF35
#define TAG_DFDF36_RR_6       				    0xDFDF36
#define TAG_DFDF37_RR_7       				    0xDFDF37
#define TAG_DFDF38_RR_8         				  0xDFDF38
#define TAG_DFDF39_RR_9       		  		  0xDFDF39
#define TAG_DFDF3A_RR_10       				    0xDFDF3A
#define TAG_DFDF3B_RR_11       				    0xDFDF3B
#define TAG_DFDF3C_RR_12       				    0xDFDF3C
#define TAG_DFDF3D_RR_13       				    0xDFDF3D
#define TAG_DFDF3E_RR_14       				    0xDFDF3E
#define TAG_DFDF3F_RR_15       				    0xDFDF3F

// tags for GetData (PINTry Counter, RiskManagement, ...)
#define NO_GETD_RECS                      8
#define TAG_DFDF41_GETD_1      				    0xDFDF41
#define TAG_DFDF42_GETD_2     			  	  0xDFDF42
#define TAG_DFDF43_GETD_3     				    0xDFDF43
#define TAG_DFDF44_GETD_4     				    0xDFDF44
#define TAG_DFDF45_GETD_5     				    0xDFDF45
#define TAG_DFDF46_GETD_6     	  			  0xDFDF46
#define TAG_DFDF47_GETD_7     		  		  0xDFDF47
#define TAG_DFDF48_GETD_8     			  	  0xDFDF48

// tags for other commands (of course no verify)
#define TAG_DFDF51_INTAUTH     				    0xDFDF51
#define TAG_DFDF52_EXTAUTH    			  	  0xDFDF52
#define TAG_DFDF53_GENAC1     				    0xDFDF53
#define TAG_DFDF54_GENAC2     				    0xDFDF54
#define TAG_DFDF55_CHALL      				    0xDFDF55
#define TAG_DFDF5F_UNKNOW_TAG_LIST  	    0xDFDF5F

// tags for Critical Scripts
#define NO_SCRIPT_RECS                    10
#define TAG_DFDF61_SCRIPT_1    		 		    0xDFDF61
#define TAG_DFDF62_SCRIPT_2     				  0xDFDF62
#define TAG_DFDF63_SCRIPT_3     				  0xDFDF63
#define TAG_DFDF64_SCRIPT_4     				  0xDFDF64
#define TAG_DFDF65_SCRIPT_5     				  0xDFDF65
#define TAG_DFDF69_SCRIPT_6    	 			    0xDFDF69
#define TAG_DFDF6A_SCRIPT_7     				  0xDFDF6A
#define TAG_DFDF6B_SCRIPT_8     				  0xDFDF6B
#define TAG_DFDF6C_SCRIPT_9     				  0xDFDF6C
#define TAG_DFDF6D_SCRIPT_10    				  0xDFDF6D

// tags for unknown commands
#define NO_UNKNOW_RECS                    15
#define TAG_DFDF71_UNKNOW_1   				    0xDFDF71
#define TAG_DFDF72_UNKNOW_2   				    0xDFDF72
#define TAG_DFDF73_UNKNOW_3     				  0xDFDF73
#define TAG_DFDF74_UNKNOW_4   		  		  0xDFDF74
#define TAG_DFDF75_UNKNOW_5   			  	  0xDFDF75
#define TAG_DFDF76_UNKNOW_6   				    0xDFDF76
#define TAG_DFDF77_UNKNOW_7   				    0xDFDF77
#define TAG_DFDF78_UNKNOW_8     				  0xDFDF78
#define TAG_DFDF79_UNKNOW_9   		  	    0xDFDF79
#define TAG_DFDF7A_UNKNOW_10   				    0xDFDF7A
#define TAG_DFDF7B_UNKNOW_11   				    0xDFDF7B
#define TAG_DFDF7C_UNKNOW_12   				    0xDFDF7C
#define TAG_DFDF7D_UNKNOW_13   				    0xDFDF7D
#define TAG_DFDF7E_UNKNOW_14   				    0xDFDF7E
#define TAG_DFDF7F_UNKNOW_15    			    0xDFDF7F
/// @}


// ========================================================================================================
// === TLV tags for transport layer ===
// ========================================================================================================

/// @defgroup ADK_TRANSPORT_TAGS Tags for transport layer
/// @ingroup ADK_SERIALIZATION
/// @brief Used for enclosing TLV container at transport.
///        That's necessary in case EMV ADK and calling application are placed in separated entities.
///        Transport layer (e.g. PINPad ISO2) must have indicators to determine whether data is meant for EMv ADK or not.
/// @{
#define CLA_EMV        0x39 ///< Class for requests to EMV ADK
#define CLA_CRD        0x41 ///< Class for ICC functions
#define CLA_LED        0x43 ///< Class for LED functions
#define EMV_CT_CLA_CBCK_REQ   0x91 ///< Class for callback requests from EMV ADK to calling application (see @ref TLV_CALLBCK)
#define EMV_CT_CLA_CBCK_RESP  0x92 ///< Class for callback responses (see @ref TLV_CALLBCK)

#define P2_SET         0x00  ///< Used for config command, e.g. @ref EMV_CT_SetTermData
#define P2_GET         0x01  ///< Used for config command, e.g. @ref EMV_CT_GetTermData
#define P2_CLONE       0x02  ///< Used reading configuration data that shall be transfered to another system (internal use, serialised API only)

#define INS_CBCK_FCT   0x01 ///< Instruction byte used for callback functions, used with #EMV_CT_CLA_CBCK_REQ
#define INS_CBCK_CRD   0x02 ///< Instruction byte used for callbacks to cardreader, used with #EMV_CT_CLA_CBCK_REQ
#define INS_CBCK_TRACE 0xFF ///< Instruction byte used for trace output, used with #EMV_CT_CLA_CBCK_REQ
#define INS_INIT       0x00 ///< Instruction byte for @ref EMV_CT_Init_Framework(), respectively EMV_CT_Init_FrameworkClient(), used with #CLA_EMV
#define INS_TERM_CFG   0x01 ///< Instruction byte for @ref EMV_CT_SetTermData, @ref EMV_CT_GetTermData, used with #CLA_EMV
#define INS_APPLI_CFG  0x02 ///< Instruction byte for @ref EMV_CT_SetAppliData, @ref EMV_CT_GetAppliData, used with #CLA_EMV
#define INS_CAPKEY_CFG 0x03 ///< Instruction byte for @ref EMV_CT_StoreCAPKey, used with #CLA_EMV
#define INS_APPLY_CFG  0x04 ///< Instruction byte for @ref EMV_CT_ApplyConfiguration, used with #CLA_EMV
#define INS_GET_VER    0x05 ///< Instruction byte for @ref EMV_CT_FRAMEWORK_GetVersion, used with #CLA_EMV
#define INS_VIRT_CFG   0x06 ///< Instruction byte for @ref EMV_CT_MapVirtualTerminal, used with #CLA_EMV
#define INS_SELECT     0x10 ///< Instruction byte for @ref EMV_CT_StartTransaction, used with #CLA_EMV
#define INS_TRANSAC    0x11 ///< Instruction byte for @ref EMV_CT_ContinueOffline, used with #CLA_EMV
#define INS_ONLINE     0x12 ///< Instruction byte for @ref EMV_CT_ContinueOnline, used with #CLA_EMV
#define INS_UPDATE_TAG 0x13 ///< Instruction byte for @ref EMV_CT_updateTxnTags, used with #CLA_EMV
#define INS_FETCH_TAG  0x14 ///< Instruction byte for @ref EMV_CT_fetchTxnTags, used with #CLA_EMV
#define INS_END_TRX    0x15 ///< Instruction byte for @ref EMV_CT_EndTransaction, used with #CLA_EMV
#define INS_CND_DATA   0x16 ///< Instruction byte for @ref EMV_CT_GetCandidateData, used with #CLA_EMV
#define INS_CHECK_AID  0x17 ///< Instruction byte for @ref EMV_CT_CheckSupportedAID, used with #CLA_EMV
#define INS_FETCH_DOL  0x18 ///< Instruction byte for @ref EMV_CT_fetchTxnDOL, used with #CLA_EMV
#define INS_ICC_DETECT 0x01 ///< Instruction byte for @ref EMV_CT_SmartDetect, used with #CLA_CRD
#define INS_ICC_RESET  0x02 ///< Instruction byte for @ref EMV_CT_SmartReset, used with #CLA_CRD
#define INS_ICC_ISO    0x03 ///< Instruction byte for @ref EMV_CT_SmartISO, used with #CLA_CRD
#define INS_ICC_OFF    0x04 ///< Instruction byte for @ref EMV_CT_SmartPowerOff, used with #CLA_CRD
#define INS_ICC_PIN    0x05 ///< Instruction byte for @ref EMV_CT_Send_PIN_Offline, used with #CLA_CRD
#define INS_ICC_PIN_DIRECT 0x08 ///< Instruction byte for @ref EMV_CT_SmartPIN, used with #CLA_CRD
#define INS_ICC_LED_SWITCH 0x09 ///< Instruction byte for @ref EMV_CT_LED, used with #CLA_CRD
#define INS_LED_SWITCH     0x03 ///< Instruction byte for @ref EMV_CT_LED, used with #CLA_LED

/// @}

#endif
