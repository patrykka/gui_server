/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Interface definitions and functions
****************************************************************************/

/**
* @file
* @brief Interface of CTLS-Client
*
*/

#ifndef _EMV_CTLS_INTERFACE_H_   /* avoid double interface-includes */
#define _EMV_CTLS_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "emv/EMV_Common_Interface.h"
#include "EMV_CTLS_Link.h"
#include "EMV_CTLS_Version.h"

#define DLL_CLC // no client shared libraries anymore


// ========================================================================================================
// === COMMON PART ===
// ========================================================================================================


/// @ingroup STRDEF_COMMON
/// @brief 9F06, AID (5 bytes RID + n bytes PIX)
typedef struct EMV_CTLS_APPLI_STRUCT
{
  unsigned char    aidlen;  ///< length of AID @n length of value from #TAG_DF04_AID
  unsigned char    AID[16]; ///< AID data @n TLV tag #TAG_DF04_AID @n XML tag #XML_TAG_APPLIDATA_AID
} EMV_CTLS_APPLI_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief AID (9F06) and kernel ID
typedef struct EMV_CTLS_APPLI_KERNEL_STRUCT
{
  unsigned char    KernelID[3]; ///< Kernel ID, see @ref DEF_KERNEL_ID @n TLV tag #TAG_DFAB01_KERNEL_ID @n XML tag #XML_TAG_AD_KERNEL_ID
  unsigned char    aidlen;      ///< length of AID @n length of value from #TAG_4F_APP_ID
  unsigned char    AID[16];     ///< AID data @n TLV tag #TAG_4F_APP_ID @n XML tag #XML_TAG_AD_AID
} EMV_CTLS_APPLI_KERNEL_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief data object list
typedef struct EMV_CTLS_DOL_STRUCT
{
  unsigned char    dollen;           ///< length of DOL data
  unsigned char    DOL[EMV_ADK_MAX_LG_DDOL]; ///< DOL data
} EMV_CTLS_DOL_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief track 2 data
typedef struct EMV_CTLS_TRACK2_STRUCT
{
  unsigned char    tr2len;      ///< length of track 2 data
  unsigned char    tr2data[19]; ///< track 2 data
} EMV_CTLS_TRACK2_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief cardholder name
typedef struct EMV_CTLS_CRDNAME_STRUCT
{
  unsigned char    crdNameLen;    ///< length of cardholder name
  unsigned char    crdName[26];   ///< cardholder name
} EMV_CTLS_CRDNAME_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief icc dynamic number
typedef struct EMV_CTLS_ICCRND_STRUCT
{
  unsigned char    iccRNDLen;    ///< length ICC dynamic number
  unsigned char    iccRND[8];    ///< ICC dynamic number
} EMV_CTLS_ICCRND_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief issuer application data
typedef struct EMV_CTLS_ISSDATA_STRUCT
{
  unsigned char    issDataLen;    ///< length of issuer application data
  unsigned char    issData[32];   ///< issuer application data
} EMV_CTLS_ISSDATA_TYPE;


// ========================================================================================================
// === Transaction Information ===
// ========================================================================================================

/// @defgroup DEF_ADK_DEBUG Additional error information
/// @ingroup ADK_GENERAL
/// @brief Filled in case of error, value to describe which error occured.
///

/// @defgroup DEF_ADK_DEBUG_EXIT Additional error info exit code
/// @ingroup DEF_ADK_DEBUG
/// @brief Last step performed by EMV ADK
/// 0       ?
/// 10-27   VK-related
/// 30-4B   Amex
/// 50-6x   Discover
/// 7       Gemalto
/// 80-8A   setup
/// 9       ?
/// A0-B5   common TXN
/// C0-DB   MK-related
/// E       ?
/// F0-F4   online
///
/// @{
  #define EMV_CTLS_ADK_SETUP_START                             0x80  ///< Initialization Setup
  #define EMV_CTLS_ADK_SETUP_NO_CONTEXT                        0x81  ///< Setup: context missing
  #define EMV_CTLS_ADK_SETUP_NO_INIT                           0x82  ///< Setup: not initialized
  #define EMV_CTLS_ADK_SETUP_TERMDATA                          0x83  ///< Setup: terminal data not available
  //#define EMV_CTLS_ADK_SETUP_CL_DISABLED                       0x84  ///< Setup: contactless disabled
  #define EMV_CTLS_ADK_SETUP_ABOVE_CEILING_EMV_ONLY            0x85  ///< Setup: above ceiling limit EMV
  #define EMV_CTLS_ADK_SETUP_ABOVE_CEILING_NO_DOM              0x86  ///< Setup: above ceiling limit no domestic app
  #define EMV_CTLS_ADK_SETUP_RND_FAIL                          0x87  ///< Setup: RND failed
  #define EMV_CTLS_ADK_SETUP_DETECT                            0x88  ///< Setup: detect failed
  #define EMV_CTLS_ADK_SETUP_SET_PARAM_FAIL                    0x89  ///< Setup: txn setup failure
  #define EMV_CTLS_ADK_SETUP_OK                                0x8A  ///< Setup: OK

  #define EMV_CTLS_ADK_TXN_START                               0xA0  ///< TXN: Start
  #define EMV_CTLS_ADK_TXN_NO_ATR                              0xA1  ///< TXN: no ATR
  #define EMV_CTLS_ADK_TXN_2_CARDS                             0xA2  ///< TXN: 2 cards in field
  #define EMV_CTLS_ADK_TXN_CAND_LIST_EMPTY                     0xA3  ///< TXN: empty candidate list of terminal
  #define EMV_CTLS_ADK_TXN_DOM_ONE_APP_SELECTED                0xA4  ///< TXN: a single domestic app is selected
  #define EMV_CTLS_ADK_TXN_DOM_ONE_APP_FAIL                    0xA5  ///< TXN: single domestic app failed
  #define EMV_CTLS_ADK_TXN_L1_DIR_READING                      0xA6  ///< TXN: L1 error at directory reading
  #define EMV_CTLS_ADK_TXN_L1_CAND_LIST                        0xA7  ///< TXN: L1 error candidate list building
  #define EMV_CTLS_ADK_TXN_DOM_APP_NOT_SELECTED                0xA8  ///< TXN: domestic application selection failed
  #define EMV_CTLS_ADK_TXN_DOM_APP_SELECTED                    0xA9  ///< TXN: domestic application selected
  #define EMV_CTLS_ADK_TXN_NO_COMBO_EMPTY_LIST                 0xAA  ///< TXN: empty combo list
  #define EMV_CTLS_ADK_TXN_NO_DOM_APP_SELECT_NO_FINAL          0xAB  ///< TXN: domestic app select failed no final select issued
  #define EMV_CTLS_ADK_TXN_L1_FINAL_SELECT                     0xAC  ///< TXN: L1 error final select
  #define EMV_CTLS_ADK_TXN_AID_NOT_READ_EMPTY                  0xAD  ///< TXN: AID parameter reading failed, list empty
  #define EMV_CTLS_ADK_TXN_AID_SKIP_DOM_EMPTY                  0xAE  ///< TXN: domestic app skipped, list empty
  #define EMV_CTLS_ADK_TXN_AID_SKIP_EMV_EMPTY                  0xAF  ///< TXN: emv app skipped, list empty
  #define EMV_CTLS_ADK_TXN_DOM_FINAL_SEL_OK                    0xB0  ///< TXN: domestic application with final select ok
  #define EMV_CTLS_ADK_TXN_UNKNOW_KERNEL                       0xB1  ///< TXN: unknown kernel
  #define EMV_CTLS_ADK_TXN_EP_INIT_FAILED                      0xB2  ///< TXN: Entry Point Init failed
  #define EMV_CTLS_ADK_TXN_LIST_ITERATION_KO                   0xB3  ///< TXN: error list iteration
  #define EMV_CTLS_ADK_TXN_CANDIDATE_EVAL                      0xB4  ///< TXN: candidate list evaluation
  #define EMV_CTLS_ADK_TXN_CANDIDATE_FINAL_SELECT              0xB5  ///< TXN: final select

  #define EMV_CTLS_ADK_TXN_MK_START                            0xC0  ///< TXN: Start of PayPass
  #define EMV_CTLS_ADK_TXN_MK_NEW_SEL                          0xC1  ///< TXN: new select
  #define EMV_CTLS_ADK_TXN_MK_RETAP_START                      0xC2  ///< TXN: retap required
  #define EMV_CTLS_ADK_TXN_MK_MOBILE_START                     0xC3  ///< TXN: mobile requested
  #define EMV_CTLS_ADK_TXN_MK_L1_START                         0xC4  ///< TXN: L1 error
  #define EMV_CTLS_ADK_TXN_MK_RETAP_MSD                        0xC5  ///< TXN: retap reuired MSD
  #define EMV_CTLS_ADK_TXN_MK_MOBILE_MSD                       0xC6  ///< TXN: mobile requested MSD
  #define EMV_CTLS_ADK_TXN_MK_L1_MSD                           0xC7  ///< TXN: L1 error MSD
  #define EMV_CTLS_ADK_TXN_MK_NO_DATA_MSD                      0xC8  ///< TXN: no data MSD
  #define EMV_CTLS_ADK_TXN_MK_DECLINE_MSD                      0xC9  ///< TXN: decline MSD
  #define EMV_CTLS_ADK_TXN_MK_OK_MSD                           0xCA  ///< TXN: ok MSD
  #define EMV_CTLS_ADK_TXN_MK_ONLINE_MSD                       0xCB  ///< TXN: online MSD
  #define EMV_CTLS_ADK_TXN_MK_OTHER_MSD                        0xCC  ///< TXN: other MSD
  #define EMV_CTLS_ADK_TXN_MK_RETAP_CHP                        0xCD  ///< TXN: retap reuired CHP
  #define EMV_CTLS_ADK_TXN_MK_MOBILE_CHP                       0xCE  ///< TXN: mobile requested CHP
  #define EMV_CTLS_ADK_TXN_MK_L1_CHP                           0xCF  ///< TXN: L1 error CHP
  #define EMV_CTLS_ADK_TXN_MK_CARDREAD_CHP                     0xD0  ///< TXN: card data read CHP
  #define EMV_CTLS_ADK_TXN_MK_DECLINE_CHP                      0xD1  ///< TXN: decline CHP
  #define EMV_CTLS_ADK_TXN_MK_ONLINE_CHP                       0xD2  ///< TXN: online CHP
  #define EMV_CTLS_ADK_TXN_MK_OK_CHP                           0xD3  ///< TXN: ok CHP
  #define EMV_CTLS_ADK_TXN_MK_INTERFACE_CHP                    0xD4  ///< TXN: use other interface
  #define EMV_CTLS_ADK_TXN_MK_NEW_SEL_CHP                      0xD5  ///< TXN: new select CHP
  #define EMV_CTLS_ADK_TXN_MK_OTHER_CHP                        0xD6  ///< TXN: other error CHP
  #define EMV_CTLS_ADK_TXN_MK_DECLINE_BEFORE_GAC               0xD7  ///< TXN: decline start
  #define EMV_CTLS_ADK_TXN_MK_FALLBACK_START                   0xD8  ///< TXN: fallback start
  #define EMV_CTLS_ADK_TXN_MK_INIT_FAIL                        0xD9  ///< TXN: Init failed
  #define EMV_CTLS_ADK_TXN_MK_GPO_RR                           0xDA  ///< TXN: Step ReadRecords and GPO
  #define EMV_CTLS_ADK_TXN_MK_FALLBACK_OTHERCARD               0xDB  ///< TXN: use other interface or other card

  #define EMV_CTLS_ADK_TXN_VK_START                            0x10  ///< TXN: Start of payWave
  #define EMV_CTLS_ADK_TXN_VK_NEW_SEL                          0x11  ///< TXN: new select
  #define EMV_CTLS_ADK_TXN_VK_MOBILE_START                     0x12  ///< TXN: mobile requested
  #define EMV_CTLS_ADK_TXN_VK_L1_START                         0x13  ///< TXN: L1 error
  #define EMV_CTLS_ADK_TXN_VK_L1_MSD                           0x14  ///< TXN: L1 error MSD
  #define EMV_CTLS_ADK_TXN_VK_FALLBACK_MSD                     0x15  ///< TXN: Start of PayPass
  #define EMV_CTLS_ADK_TXN_VK_DECLINE_MSD                      0x16  ///< TXN: decline MSD
  #define EMV_CTLS_ADK_TXN_VK_OK_MSD                           0x17  ///< TXN: ok MSD
  #define EMV_CTLS_ADK_TXN_VK_ONLINE_MSD                       0x18  ///< TXN: online MSD
  #define EMV_CTLS_ADK_TXN_VK_OTHER_MSD                        0x19  ///< TXN: other error MSD
  #define EMV_CTLS_ADK_TXN_VK_L1_CHP                           0x1A  ///< TXN: L1 error CHP
  #define EMV_CTLS_ADK_TXN_VK_READCARD_CHP                     0x1B  ///< TXN: read card CHP
  #define EMV_CTLS_ADK_TXN_VK_DECLINE_CHP                      0x1C  ///< TXN: decline CHP
  #define EMV_CTLS_ADK_TXN_VK_ONLINE_CHP                       0x1D  ///< TXN: online CHP
  #define EMV_CTLS_ADK_TXN_VK_OK_CHP                           0x1E  ///< TXN: ok CHP
  #define EMV_CTLS_ADK_TXN_VK_FALLBACK_CHP                     0x1F  ///< TXN: fallback CHP
  #define EMV_CTLS_ADK_TXN_VK_OTHER_CHP                        0x20  ///< TXN: other error CHP
  #define EMV_CTLS_ADK_TXN_VK_DECLINE_START                    0x21  ///< TXN: decline start
  #define EMV_CTLS_ADK_TXN_VK_FALLBACK_START                   0x22  ///< TXN: fallback start
  #define EMV_CTLS_ADK_TXN_VK_OTHER_START                      0x23  ///< TXN: other error start
  #define EMV_CTLS_ADK_TXN_VK_INIT_FAIL                        0x24  ///< TXN: Init failed
  #define EMV_CTLS_ADK_TXN_VK_GPO_RR                           0x25  ///< TXN: Step ReadRecords and GPO
  #define EMV_CTLS_ADK_TXN_VK_INCONS_TRACK2                    0x26  ///< TXN: inconsisten track 2 and PAN
  #define EMV_CTLS_ADK_TXN_VK_READCARD_MSD                     0x27  ///< TXN: card read MSD
  #define EMV_CTLS_ADK_TXN_VK_RETAP_START                      0x28  ///< TXN: retap required
  #define EMV_CTLS_ADK_TXN_VK_FALLBACK_CARD_LOG                0x29  ///< TXN: fallback because of failed card log reading

  #define EMV_CTLS_ADK_TXN_KERNEL_START                        0x30  ///< TXN: Start of Kernel Processing
  #define EMV_CTLS_ADK_TXN_KERNEL_NEW_SEL                      0x31  ///< TXN: new select
  #define EMV_CTLS_ADK_TXN_KERNEL_RETAP_START                  0x32  ///< TXN: retap required
  #define EMV_CTLS_ADK_TXN_KERNEL_MOBILE_START                 0x33  ///< TXN: mobile requested
  #define EMV_CTLS_ADK_TXN_KERNEL_L1_START                     0x34  ///< TXN: L1 error
  #define EMV_CTLS_ADK_TXN_KERNEL_RETAP_MSD                    0x35  ///< TXN: retap reuired MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_MOBILE_MSD                   0x36  ///< TXN: mobile requested MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_L1_MSD                       0x37  ///< TXN: L1 error MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_NO_DATA_MSD                  0x38  ///< TXN: no data MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_DECLINE_MSD                  0x39  ///< TXN: decline MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_OK_MSD                       0x3A  ///< TXN: ok MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_ONLINE_MSD                   0x3B  ///< TXN: online MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_OTHER_MSD                    0x3C  ///< TXN: other MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_RETAP_CHP                    0x3D  ///< TXN: retap reuired CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_MOBILE_CHP                   0x3E  ///< TXN: mobile requested CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_L1_CHP                       0x3F  ///< TXN: L1 error CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_CARDREAD_CHP                 0x40  ///< TXN: card data read CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_DECLINE_CHP                  0x41  ///< TXN: decline CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_ONLINE_CHP                   0x42  ///< TXN: online CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_OK_CHP                       0x43  ///< TXN: ok CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_INTERFACE_CHP                0x44  ///< TXN: use other interface
  #define EMV_CTLS_ADK_TXN_KERNEL_NEW_SEL_CHP                  0x45  ///< TXN: new select CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_OTHER_CHP                    0x46  ///< TXN: other error CHP
  #define EMV_CTLS_ADK_TXN_KERNEL_DECLINE_BEFORE_GAC           0x47  ///< TXN: decline start
  #define EMV_CTLS_ADK_TXN_KERNEL_FALLBACK_START               0x48  ///< TXN: fallback start
  #define EMV_CTLS_ADK_TXN_KERNEL_INIT_FAIL                    0x49  ///< TXN: Init failed
  #define EMV_CTLS_ADK_TXN_KERNEL_GPO_RR                       0x4A  ///< TXN: Step ReadRecords and GPO
  #define EMV_CTLS_ADK_TXN_KERNEL_FALLBACK_OTHERCARD           0x4B  ///< TXN: use other card
  #define EMV_CTLS_ADK_TXN_KERNEL_TRY_ZIP                      0x4C  ///< TXN: try Discover ZIP
  #define EMV_CTLS_ADK_TXN_KERNEL_FALLBACK_MSD                 0x4D  ///< TXN: fallback MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_DECLINE_START                0x4E  ///< TXN: decline before EOT
  #define EMV_CTLS_ADK_TXN_KERNEL_READCARD_MSD                 0x4F  ///< TXN: read card MSD
  #define EMV_CTLS_ADK_TXN_KERNEL_FALLBACK_CHP                 0x50  ///< TXN: use other interface or other card
  #define EMV_CTLS_ADK_TXN_KERNEL_START_FALLBACK_OTHERCARD     0x51  ///< TXN: use other interface or other card
  #define EMV_CTLS_ADK_TXN_KERNEL_OTHER_TERM_CHP               0x52  ///< TXN: advice merchant to use other terminal
  #define EMV_CTLS_ADK_TXN_KERNEL_TOO_MANY_TAPS                0x53  ///< TXN: card holder exceeded number of taps allowed for one transaction
  #define EMV_CTLS_ADK_TXN_KERNEL_BAD_FFI                      0x54  ///< TXN: Interac bad FFI

  #define EMV_CTLS_ADK_TXN_GK_GET_DATA_OKAY                    0x70  ///< TXN: Gemalto, special transaction GET DATA finished okay
  #define EMV_CTLS_ADK_TXN_GK_GET_DATA_FAIL                    0x71  ///< TXN: Gemalto, special transaction GET DATA finished fail
  #define EMV_CTLS_ADK_TXN_GK_PUT_DATA_OKAY                    0x72  ///< TXN: Gemalto, special transaction PUT DATA finished okay
  #define EMV_CTLS_ADK_TXN_GK_PUT_DATA_FAIL                    0x73  ///< TXN: Gemalto, special transaction PUT DATA finished fail

  #define EMV_CTLS_ADK_ONL_START                               0xF0  ///< Online: Start
  #define EMV_CTLS_ADK_ONL_NO_CTLS_TXN                         0xF1  ///< Online: No Ctls transaction happened before call
  #define EMV_CTLS_ADK_ONL_NO_HOST                             0xF2  ///< Online: no answer from host
  #define EMV_CTLS_ADK_ONL_HOST_OK                             0xF3  ///< Online: host approved
  #define EMV_CTLS_ADK_ONL_HOST_DECLINE                        0xF4  ///< Online: host declined
#define EMV_CTLS_ADK_ONL_HOST_SWITCH_IF                        0xF5  ///< Online: Strong Consumer Authentication - switch interface
#define EMV_CTLS_ADK_ONL_HOST_ONLINE_PIN                       0xF6  ///< Online: Strong Consumer Authentication - Online PIN
/// @}


/// @defgroup DEF_ADK_DEBUG_STATUS Additional error info, status
/// @ingroup DEF_ADK_DEBUG
/// @brief Internal error index
/// @{
  #define EMV_CTLS_ADK_STEP_01  1  ///< sub step 1
  #define EMV_CTLS_ADK_STEP_02  2  ///< sub step 2
  #define EMV_CTLS_ADK_STEP_03  3  ///< sub step 3
  #define EMV_CTLS_ADK_STEP_04  4  ///< sub step 4
  #define EMV_CTLS_ADK_STEP_05  5  ///< sub step 5
  #define EMV_CTLS_ADK_STEP_06  6  ///< sub step 6
  #define EMV_CTLS_ADK_STEP_07  7  ///< sub step 7
  #define EMV_CTLS_ADK_STEP_08  8  ///< sub step 8
  #define EMV_CTLS_ADK_STEP_09  9  ///< sub step 9
  #define EMV_CTLS_ADK_STEP_10  10 ///< sub step 10
  #define EMV_CTLS_ADK_STEP_11  11 ///< sub step 11
  #define EMV_CTLS_ADK_STEP_12  12 ///< sub step 12
  #define EMV_CTLS_ADK_STEP_13  13 ///< sub step 13
  #define EMV_CTLS_ADK_STEP_14  14 ///< sub step 14
  #define EMV_CTLS_ADK_STEP_15  15 ///< sub step 15
  #define EMV_CTLS_ADK_STEP_16  16 ///< sub step 16
  #define EMV_CTLS_ADK_STEP_17  17 ///< sub step 17
  #define EMV_CTLS_ADK_STEP_18  18 ///< sub step 18
  #define EMV_CTLS_ADK_STEP_19  19 ///< sub step 19
  #define EMV_CTLS_ADK_STEP_20  20 ///< sub step 20
/// @}


// ========================================================================================================
// === TERMINAL CONFIGURATION ===
// ========================================================================================================

/// @ingroup DEF_CONF_TERM
/// @brief Length of Contactless kernel version information
/// @{
#define  EMV_CTLS_KERNEL_VERSION_SIZE  255  ///< Max. length of EMV_CTLS_TERMDATA_STRUCT::KernelVersion
/// @}

/// @ingroup DEF_CONF_TERM
/// @brief struct for interface to EMV_CTLS_SetTermData() and EMV_CTLS_GetTermData()
typedef struct EMV_CTLS_TERMDATA_STRUCT /* === EMV_CTLS_TERMDATA_TYPE === */
{
  unsigned long   CL_Modes_Supported;          ///< obsolete, ignored
  unsigned char   TermTyp;                     ///< @brief Terminal type, for possible values see @ref TERM_TYPES @n mandatory for first call of EMV_CTLS_SetTermData()
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_TYPE @n TLV tag #TAG_9F35_TRM_TYPE @n XML Tag: #XML_TAG_TERMDATA_TERM_TYP
  unsigned char   CountryCodeTerm[2];          ///< Terminal country code according ISO 3166 @n Availability bit: #INPUT_CTLS_TRM_COUNTRY_CODE  @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE @n XML Tag: #XML_TAG_TERMDATA_COUNTRY_CODE_TERM
  unsigned char   CurrencyTrans[2];            ///< @brief Transaction currency code according ISO 4217
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_CURRENCY @n TLV tag #TAG_5F2A_TRANS_CURRENCY @n XML Tag: #XML_TAG_TERMDATA_CURRENCY_TRANS
  unsigned char   ExpTrans;                    ///< @brief Transaction currency exponent
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_EXP_CURRENCY @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP @n XML Tag: #XML_TAG_TERMDATA_EXP_TRANS
  unsigned char   SuppLang[EMV_ADK_MAX_LANG];  ///< Languages supported by the terminal application, see @ref TF_LANGUAGES @n Availability bit: #INPUT_CTLS_TRM_LANGUAGES @n TLV tag #TAG_SUPP_LANG @n XML Tag: #XML_TAG_TERMDATA_SUPP_LANG
  unsigned char   IFDSerialNumber[8];          ///< @brief Interface Device (IFD) Serial Number
                                               ///< @n In case there are no special project requirements this parameter can be filled as follows:
                                               ///< @n <em> Verix: </em> use function @c SVC_INFO_SERLNO() and take the last 8 digits from the result
                                               ///< @n <em> V/OS: </em> function @c platforminfo_get() with <tt> InfoType==PI_SERIAL_NUM </tt>
                                               ///< @n On VFI reader the configured value does not come into effect. VFI reader internally fetches the 9F1E from the OS.
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_IFD_SERIAL @n TLV tag #TAG_9F1E_IFD_SERIAL_NB @n XML Tag: #XML_TAG_TERMDATA_IFD_SERIAL_NUMBER
  unsigned char   FlowOptions[10];             ///< Flow Options, see @ref TERM_FLOW_OPTIONS @n Availability bit: #INPUT_CTLS_TRM_FLOW_OPTIONS @n TLV tag #TAG_DF8F0B_TERM_FLOW_OPTIONS @n XML Tag: #XML_TAG_TERMDATA_FLOW_OPTIONS
  unsigned char   MaxCTLSTranslimit[4];        ///< @brief (Hexadecimal coded) Contactless transaction limit
                                               ///< @n CTLS Setup will fail if the amount is above this limit (EPAL requirement) - not recommended to configure (leave empty) if not explicitly needed like in Australia
                                               ///< @n TLV tag #TAG_DF8F0E_TERM_MAXCTLS_TRANSLIMIT @n XML Tag: #XML_TAG_TERMDATA_MAXCTLS_TRANSLIMIT @n Availability bit #INPUT_CTLS_TRM_MAXCTLS_TRANSLIMIT
  unsigned short  BeepVolume;                  ///< @brief Beeper Volume
                                               ///< Allows the application to modify the volume of the success tone at the end of a contactless transaction. @n See also #EMV_CTLS_INIT_OPT_VFI_BUZZER_ADK
                                               ///< @n TLV tag #TAG_DF62_BUZZER_VOLUME @n XML Tag: #XML_TAG_TERMDATA_BEEP_VOLUME @n Availability bit #INPUT_CTLS_TRM_BEEP_VOLUME
  unsigned short  BeepFrequencySuccess;        ///< @brief Beeper frequency for success tone (unit: Hz)
                                               ///< @n Optional, default: 1500 @n See also #EMV_CTLS_INIT_OPT_VFI_BUZZER_ADK
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_BEEP_FREQ_SUCCESS @n TLV tag #TAG_DFD006_BEEP_FREQ_SUCCESS @n XML Tag: #XML_TAG_TERMDATA_BEEP_FREQUENCY_SUCCESS
  unsigned short  BeepFrequencyAlert;          ///< @brief Beeper frequency for alert tone (unit: Hz)
                                               ///< @n Optional, default: 750 @n See also #EMV_CTLS_INIT_OPT_VFI_BUZZER_ADK
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_BEEP_FREQ_ALERT @n TLV tag #TAG_DFD007_BEEP_FREQ_ALERT, @n XML Tag: #XML_TAG_TERMDATA_BEEP_FREQUENCY_ALERT
  unsigned char   HostCommunicationTimeout;    ///< @brief Maximum time between response of EMV_CTLS_ContinueOffline() and call of EMV_CTLS_ContinueOnline().
                                               ///< @n Relevant for schemes having second tap scenarios (Discover, JCB and Rupay).
                                               ///< @n VFI reader only!
                                               ///< @n Optional, default: 15 Seconds
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_HOST_COMM_TIMEOUT @n TLV tag #TAG_DFD009_HOST_COMM_TIMEOUT @n XML Tag: #XML_TAG_TERMDATA_HOST_COMM_TIMEOUT
  unsigned char   SecondTapDelay;              ///< @brief Maximum time to wait for the second card tap after the online session.
                                               ///< @n Relevant for schemes having second tap scenarios (Discover, JCB and Rupay).
                                               ///< @n VFI reader only!
                                               ///< @n Optional, default: 10 Seconds
                                               ///< @n Availability bit: #INPUT_CTLS_TRM_SECOND_TAP_DELAY @n TLV tag #TAG_DFD008_SECOND_TAP_DELAY @n XML Tag: #XML_TAG_TERMDATA_SECOND_TAP_DELAY
  const unsigned char* Hotlist;                ///< @brief Blacklisted card numbers
                                               ///< optional pointer to PAN list, BER-TLV coded list of tag '5A' each optionally followed by PAN sequence number tag '5F34'. The list end is marked by trailing binary '0'.
                                               ///< Please note: It is recommended that the payment application itself checks for hotcard after the payment because of timing considerations (if the scheme allows this).
                                               ///< Anyway for certification purpose and short lists a support is added. The hotlist is member of the contactless terminal wide configuration structure.
                                               ///< It is a const byte array with a TLV coded list of '5A' tags of arbitrary length. The interface is write-only.
                                               ///< Deletion is achieved by sending an empty list. The list is stored in a file named "EMV_CTLS_Hotlist.xml" containing XML tags "<Hotlist>" and "<PAN>".
                                               ///< Limitations: Simple algorithm Since the hotlist is foreseen to be small for instance. So the check is just a simple list iteration.
                                               ///< Therefore the time consumption is O(n). No partial match, the PANs have to match completely that is they have to have the same length.
                                               ///< TLV tag #TAG_HOTLIST @n Availability bit #INPUT_CTLS_TRM_HOTLIST

  char  KernelVersion[EMV_CTLS_KERNEL_VERSION_SIZE];   ///< Version of EMVCo Kernels, only valid for EMV_CTLS_GetTermData() @n Availability bit: #INPUT_CTLS_TRM_KERNEL_VERSION @n TLV tag #TAG_KERNEL_VERSION @n XML Tag: #XML_TAG_TERMDATA_KERNEL_VERSION
  char  FrameworkVersion[EMV_ADK_VERSION_ASCII_SIZE];  ///< Version of the Framework, only valid for EMV_CTLS_GetTermData() @n Availability bit: #INPUT_CTLS_TRM_FRAMEWORK_VERSION @n TLV tag #TAG_DF11_LIB_VERSION @n XML Tag: #XML_TAG_TERMDATA_FRAMEWORK_VERSION
  char  L1DriverVersion[EMV_ADK_VERSION_ASCII_SIZE];   ///< Version of L1 driver in the form <I>&lt;PCD software id&gt;,&lt;PCD hardware id&gt;;&lt;OS driver version&gt;;&lt;PCD name&gt; &lt;checksum&gt;</I>, only valid for EMV_CTLS_GetTermData() @n Availability bit: #INPUT_CTLS_TRM_L1DRIVER_VERSION @n TLV tag #TAG_DF6F_L1DRIVER_VERSION @n XML Tag: #XML_TAG_TERMDATA_L1DRIVER_VERSION

  unsigned char   Info_Included_Data[8];     ///< Which data is included in the message
} EMV_CTLS_TERMDATA_TYPE; ///< typedef for EMV_CTLS_TERMDATA_STRUCT

/// @defgroup DEF_INPUT_TERM Terminal data: Which data is valid
/// @ingroup DEF_CONF_TERM
/// @brief Contents of the EMV_CTLS_TERMDATA_STRUCT::Info_Included_Data
/// @{
/* Byte 1 */
#define  INPUT_CTLS_TRM_SUPPORTED_CTLS     0x01  ///< B1b1: EMV_CTLS_TERMDATA_STRUCT::CL_Modes_Supported
#define  INPUT_CTLS_TRM_TYPE               0x02  ///< B1b2: EMV_CTLS_TERMDATA_STRUCT::TermTyp
#define  INPUT_CTLS_TRM_CURRENCY           0x04  ///< B1b3: EMV_CTLS_TERMDATA_STRUCT::CurrencyTrans
#define  INPUT_CTLS_TRM_EXP_CURRENCY       0x08  ///< B1b4: EMV_CTLS_TERMDATA_STRUCT::ExpTrans
#define  INPUT_CTLS_TRM_COUNTRY_CODE       0x10  ///< B1b5: EMV_CTLS_TERMDATA_STRUCT::CountryCodeTerm
#define  INPUT_CTLS_TRM_FLOW_OPTIONS       0x20  ///< B1b6: EMV_CTLS_TERMDATA_STRUCT::FlowOptions
#define  INPUT_CTLS_TRM_MAXCTLS_TRANSLIMIT 0x40  ///< B1b7: EMV_CTLS_TERMDATA_STRUCT::MaxCTLSTranslimit
#define  INPUT_CTLS_TRM_BEEP_VOLUME        0x80  ///< B1b8: EMV_CTLS_TERMDATA_STRUCT::BeepVolume

/* Byte 2 */
#define  INPUT_CTLS_TRM_LANGUAGES          0x01  ///< B2b1: EMV_CTLS_TERMDATA_STRUCT::SuppLang
#define  INPUT_CTLS_TRM_IFD_SERIAL         0x02  ///< B2b2: EMV_CTLS_TERMDATA_STRUCT::IFDSerialNumber
#define  INPUT_CTLS_TRM_KERNEL_VERSION     0x04  ///< B2b3: EMV_CTLS_TERMDATA_STRUCT::KernelVersion
#define  INPUT_CTLS_TRM_FRAMEWORK_VERSION  0x08  ///< B2b4: EMV_CTLS_TERMDATA_STRUCT::FrameworkVersion
#define  INPUT_CTLS_TRM_HOTLIST            0x10  ///< B2b5: EMV_CTLS_TERMDATA_STRUCT::Hotlist
#define  INPUT_CTLS_TRM_BEEP_FREQ_SUCCESS  0x20  ///< B2b6: EMV_CTLS_TERMDATA_STRUCT::BeepFrequencySuccess
#define  INPUT_CTLS_TRM_BEEP_FREQ_ALERT    0x40  ///< B2b7: EMV_CTLS_TERMDATA_STRUCT::BeepFrequencyAlert
#define  INPUT_CTLS_TRM_L1DRIVER_VERSION   0x80  ///< B2b8: EMV_CTLS_TERMDATA_STRUCT::L1DriverVersion

/* Byte 3 */
#define  INPUT_CTLS_TRM_SECOND_TAP_DELAY   0x01  ///< B3b1: EMV_CTLS_TERMDATA_STRUCT::SecondTapDelay
#define  INPUT_CTLS_TRM_HOST_COMM_TIMEOUT  0x02  ///< B3b2: EMV_CTLS_TERMDATA_STRUCT::HostCommunicationTimeout
/// @}

/// @defgroup CL_MODES Supported Contactless modes
/// @ingroup DEF_CONF_TERM
/// @brief Defines for EMV_CTLS_APPLIDATA_STRUCT::CL_Modes, EMV_CTLS_TRANSRES_STRUCT::CL_Mode
///
/// @deprecated Use @ref DEF_KERNEL_ID instead.
/// @{
#define  CL_DISABLED                      0x00 ///< Contactless TXNs are disabled, all other contactless parameters are ignored
#define  CL_MC_PAYPASS_CHIP               0x01 ///< Maestro Paypass enabled (chip only, e.g. Maestro definiton)
#define  CL_MC_PAYPASS_MSR                0x02 ///< Mastercard Paypass enabled, msr only
#define  CL_MC_PAYPASS_COMBINED           0x03 ///< Mastercard Paypass enabled (MC mandates to support chip and msr per standard)
#define  CL_VISA_CHIP                     0x04 ///< VISA qVSDC enabled (no magstripe, VISA mandates using chip only in Europe)
#define  CL_VISA_MSR                      0x08 ///< VISA magstripe enabled, msr only
#define  CL_VISA_COMBINED                 0x0C ///< VISA chip and magstripe enabled
#define  CL_AMEX_COMBINED                 0x10 ///< Amex enabled, chip and msr (this is the Amex default)
#define  CL_DISCOVER_ZIP                  0x20 ///< Discover Zip enabled (msr only system)
#define  CL_INTERAC                       0x40 ///< Interac (Canada) enabled (always chip and msr)
#define  CL_QPBOC_COMBINED                0x80 ///< China PBOC/UnionPay enabled (msr and chip)
#define  CL_JCB                          0x100 ///< JCB enabled
#define  CL_EPAL                         0x200 ///< EPAL (Australia) enabled
#define  CL_AMEX_MSR                     0x400 ///< Amex enable, msr only enabled (either use Amex combined or Amex msr)
#define  CL_DISCOVER_DPAS_CHIP           0x800 ///< D-PAS (Discover) chip enabled, chip only
#define  CL_DISCOVER_DPAS_MSR           0x1000 ///< D-PAS (Discover) magstripe enabled, msr only
#define  CL_DISCOVER_DPAS_COMBINED      0x1800 ///< D-PAS (Discover) chip and magstripe enabled
#define  CL_RUPAY                       0x2000 ///< RuPay (India) enabled
#define  CL_JCB_EMV                     0x4000 ///< JCB with EMV mode
#define  CL_JCB_LEGACY                  0x8000 ///< JCB with Legacy mode
#define  CL_GEMALTO                   0x010000 ///< Gemalto kernel
#define  CL_GIROCARD                  0x020000 ///< girocard (German debit brand) kernel based on PayPass
#define  CL_SIBS                      0x040000 ///< SIBS (chip only)
#define  CL_PAGOBANCOMAT              0x080000 ///< PagoBancomat
#define  CL_MIR                       0x100000 ///< MIR

#define  CL_SUPPORTED_VELOCITY        0xFFFFFF ///< (used internally)
#define  CL_SUPPORTED_VFI             0xFFFFFF ///< (used internally)
#define  CL_ALL                       0xFFFFFF ///< This can be used to configure Terminal to support all modes

#define  CL_UNKNOWN                 0x40000000 ///< unknown kernel
#define  CL_APPS_KERNEL             0x80000000 ///< kernel is handled by the application, entry point returns to app if this AID is selected
/// @}

/// @defgroup TERM_FLOW_OPTIONS Terminal configuration: Collection of flow options
/// @ingroup DEF_CONF_TERM
/// @brief Contents of the EMV_CTLS_TERMDATA_STRUCT::FlowOptions
/// @{
/* Byte 1 */
#define  INPUT_CTLS_TRM_FLOWOPT_VISA_WAVE            0x01  ///< B1b1: Support for Visa wave @n for VFI reader or enabling Raptor VISA AP kernel (PDOL decision) @n Info: VFI reader tag DF64, 1FF0 and others - This feature is not configurable per AID only global
#define  INPUT_CTLS_CKO_AMEXEMV_OR_NONEMV_MARKET     0x02  ///< B1b2: Support for Amex EMV or NON EMV market @n Map to Byte2 BIT1 of Contactless Kernel Options(1FF0) @n Info: VFI reader tag 1FF0 - This feature is not configurable per AID only global
#define  INPUT_CTLS_TRM_FLOWOPT_UI_SCHEME_DEFAULT    0x04  ///< B1b3: VFI reader only: Use reader's UI scheme "default", as standard "EMEA" is used. @n Visa Europe tests will fail if this is activated. But it's needed for display callback "L1 error" (#EMV_ADK_TXT_RETAP_SAME_L1)
#define  INPUT_CTLS_TRM_IFD_SERIAL_USE_TID           0x08  ///< B1b4: Use the TID as IFD Serial number. Background: If application can not provide IFD_Serial this can be used as replacement to have a unique value available (not for VFI-Reader).
#define  INPUT_CTLS_TRM_FLOWOPT_LED_NON_VISA_EUROPE  0x10  ///< B1b5: Switch off Visa Europe work around for LED2. This option is like INPUT_CTLS_TRM_FLOWOPT_UI_SCHEME_DEFAULT but for VERTEX.
#define  INPUT_CTLS_TRM_FLOWOPT_LED_EP_OPTION_2      0x20  ///< B1b6: Use LED scheme option 2 as defined in Entry Point spec [EMV BB]. Coloured LEDs similar to Visa Wave 2 (Visa Asia Pacific). For LED configuration see EMV_CTLS_LED_ConfigDesign_Extended().
#define  INPUT_CTLS_TRM_FLOWOPT_LED_INTERAC          0x40  ///< B1b7: Use LED blinking scheme for Interac (Canada)
#define  INPUT_CTLS_TRM_FLOWOPT_FORWARD_VFI_FAIL     0x80  ///< B1b8: VFI Reader final scenario "Fail" is forwarded to the application by returning EMV_ADK_VFI_FAIL instead of per default EMV_ADK_AAC. Additionnally the ADK provides the SW12 in T_DF64_KernelDebugData[0]&[1], VFI Reader Error Code in T_DF64_KernelDebugData[3], and VFI Reader RF State Code in T_DF64_KernelDebugData[30]

/* Byte 2 */
#define  INPUT_CTLS_TRM_FLOWOPT_DISABLE_DISCOVER_ZIP_CHECK   0x01  ///< B2b1: VFI Reader only: Disable the check according to D-PAS 1.0 Bulletin TAS-002 v1.0 which requires to consider the legacy Discover ZIP application as mandatory on any card/phone with the Discover/Diners D-PAS AID, and to reject the card if ZIP is not present
/// @}

// ========================================================================================================
// === APPLICATION CONFIGURATION ===
// ========================================================================================================

/// @defgroup CL_TTQ VISA terminal transaction qualifier
/// @ingroup DEF_CONF_APPLI
/// @brief Possible values for EMV_CTLS_APPLIDATA_STRUCT::VisaTTQ
/// @{
#define VISA_TTQ_MAGSTRIPE_SUPPORTED      0x80  ///< B1b8: Contactless magnetic stripe (MSD) supported
#define VISA_TTQ_VSDC_SUPPORTED           0x40  ///< B1b7: Contactless VSDC supported
#define VISA_TTQ_qVSDC_SUPPORTED          0x20  ///< B1b6: Contactless qVSDC supported
#define VISA_TTQ_CONTACT_VSDC_SUPPORTED   0x10  ///< B1b5: Contact VSDC supported
#define VISA_TTQ_OFFLINE_ONLY             0x08  ///< B1b4: Reader is Offline Only
#define VISA_TTQ_ONLINE_PIN_SUPPORTED     0x04  ///< B1b3: Online PIN supported
#define VISA_TTQ_SIGNATURE_SUPPORTED      0x02  ///< B1b2: Signature supported
#define VISA_TTQ_ODA_ONLINE_SUPPORTED     0x01  ///< B1b1: ODA for Online Authorizations supported
#define VISA_TTQ_ONLINE_FORCED            0x80  ///< B2b8: Online cryptogram required
#define VISA_TTQ_CVM_FORCED               0x40  ///< B2b7: CVM required
#define VISA_TTQ_CONTACT_PINOFFL          0x20  ///< B2b6: (Contact Chip) Offline PIN supported
/// @}


/// @defgroup DYNAMIC_READER_LIMITS Dynamic reader limits
/// @ingroup DEF_CONF_APPLI

/// @defgroup DRL_FEATURE_SWITCHS DRL feature switching
/// @ingroup DYNAMIC_READER_LIMITS
/// @brief Values for VISA and Amex DRL features which can be switched on and off
/// @{
#define VISA_DRL_DISABLE_ZERO_AMOUNT_CHECK         0x01  ///< b1: bit set: VISA: Disable the Zero Amount Check for this DRL entry (Visa default is on)
#define VISA_DRL_ENABLE_STATUS_CHECK               0x02  ///< b2: bit set: VISA: Enable the Status Check for this DRL entry (Visa default is off)

#define VISA_AMEX_DRL_DISABLE_FLOOR_LIMIT_CHECK    0x20  ///< b6: bit set: AMEX and VISA: Disable the floor limit Check for this DRL entry (Default is on)
#define VISA_AMEX_DRL_DISABLE_TRANS_LIMIT_CHECK    0x40  ///< b7: bit set: AMEX and VISA: Disable the transaction limit Check for this DRL entry (Default is on)
#define VISA_AMEX_DRL_DISABLE_CV_LIMIT_CHECK       0x80  ///< b8: bit set: AMEX and VISA: Disable the cardholder verification limit Check for this DRL entry (Default is on)
/// @}

/// @defgroup DRL_MAX_ENTRIES Number of DRL entries for Visa and Amex
/// @ingroup DYNAMIC_READER_LIMITS
/// @brief For VISA and Amex: How many DRL entries are possible
/// @{
#define   EMV_ADK_DRL_ENTRIES_VISA             4  ///< Number of entries in EMV_CTLS_APPLIDATA_STRUCT::VisaDRLParams
#define   EMV_ADK_DRL_ENTRIES_AMEX             16  ///< Number of entries in EMV_CTLS_APPLIDATA_STRUCT::AmexDRLParams
/// @}

#ifdef OLD_CONFIG_INTERFACE
/// @ingroup DYNAMIC_READER_LIMITS
/// @brief Struct for additional Visa DRL parameters of contactless transactions
///
/// Used for EMV_CTLS_APPLIDATA_STRUCT::VisaDRLParams
/// @n TLV tag #TAG_FA_VISA_DRL_RISK
/// @n Availability bit #INPUT_CTLS_APL_VISA_DRL
/// @n XML tag #XML_TAG_APPLIDATA_VISA_DRL_PARAMS
typedef struct EMV_CTLS_VISA_DRL_STRUCT
{
  unsigned char  Index;                  ///< Switch transaction limits 1 to 4, 0 means not configured @n Index in EMV_CTLS_APPLIDATA_STRUCT::VisaDRLParams @n TLV tag #TAG_C3_INDEX @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_INDEX
  unsigned char  Floorlimit[4];          ///< DRL floor limit, to disable use FFFFFFFF (This is 4 byte binary because the origin EMVCo tag for floorlimit 9F1B is defined in that way) @n TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT
  unsigned char  ucAppPrgIdLen;          ///< DRL Application programm ID length @n TLV tag #TAG_C4_APP_PRG_ID_LEN @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_PRG_ID_LEN
  unsigned char  Application_PRG_ID[16]; ///< DRL Application programm ID @n TLV tag #TAG_C5_APP_PRG_ID @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_APP_PRG_ID
  unsigned char  TXNlimit[6];            ///< DRL transaction limit, to disable use 999999999999 @n TLV tag #TAG_C6_TXN_LIMIT @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT
  unsigned char  CVMlimit[6];            ///< DRL CVM limit, to disable use 999999999999 @n TLV tag #TAG_C0_TRM_CL_CVM_LIMIT @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT
  unsigned char  OnOffSwitch;            ///< DRL switch on / off other DRL features, see @ref DYNAMIC_READER_LIMITS @n TLV tag #TAG_CA_DRL_ON_OFF @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_SWITCH
} EMV_CTLS_VISA_DRL_TYPE;


/// @ingroup DYNAMIC_READER_LIMITS
/// @brief Struct for additional Amex DRL parameters of contactless transactions
///
/// Used for EMV_CTLS_APPLIDATA_STRUCT::AmexDRLParams
/// @n TLV tag #TAG_FD_AMEX_DRL_RISK
/// @n Availability bit #INPUT_CTLS_APL_AMEX_DRL
/// @n XML tag #XML_TAG_APPLIDATA_AMEX_DRL_PARAMS
typedef struct EMV_CTLS_AMEX_DRL_STRUCT
{
  unsigned char  Index;          ///< @brief DRL Index, 0x80...0x8F
                                 ///< @n Identifies index of AMEX Dynamic Limits Set 0...15, 0 means not configured, the first no zero entry in this list will be used as the AMEX Default entry.
                                 ///< @n TLV tag #TAG_C3_INDEX @n XML tag #
  unsigned char  OnOffSwitch;    ///< @brief Bitmap of tests to be performed, switch on/off DRL checks, see @ref DYNAMIC_READER_LIMITS
                                 ///< @n TLV tag #TAG_CA_DRL_ON_OFF @n XML tag #XML_TAG_APPLIDATA_AMEX_DRL_SWITCH
  unsigned char  Floorlimit[4];  ///< @brief DRL floor limit, to disable use FFFFFFFF (This is 4 byte binary because the origin EMVCo tag for floorlimit 9F1B is defined in that way)
                                 ///< @n TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT @n XML tag #XML_TAG_APPLIDATA_AMEX_DRL_FLOORLIMIT
  unsigned char  TXNlimit[6];    ///< @brief DRL transaction limit, to disable use 999999999999
                                 ///< @n TLV tag #TAG_C6_TXN_LIMIT @n XML tag #XML_TAG_APPLIDATA_AMEX_DRL_TXN_LIMIT
  unsigned char  CVMlimit[6];    ///< @brief DRL CVM limit, to disable use 999999999999
                                 ///< @n TLV tag #TAG_C0_TRM_CL_CVM_LIMIT @n XML tag #XML_TAG_APPLIDATA_AMEX_DRL_CVM_LIMIT
} EMV_CTLS_AMEX_DRL_TYPE;


/// @defgroup PP3_PHONE_MSG_TABLE PayPass Phone Message Table
/// @ingroup DEF_CONF_APPLI
/// @brief PayPass3 Phone Message Table

/// @defgroup PP3_PMT_NO_OF_ENTRIES Number of entries in Phone message table
/// @ingroup PP3_PHONE_MSG_TABLE
/// @brief Number of entries in Phone message table
/// @{
#define   EMV_ADK_PMSG_ENTRIES           10
/// @}


/// @ingroup PP3_PHONE_MSG_TABLE
/// @brief Struct for PayPass3 Phone Message Table
///
/// Used for EMV_CTLS_APPLIDATA_STRUCT::PhoneMessageTable
/// @n TLV tag 0xDF3F
typedef struct EMV_CTLS_PHONEMSG_STRUCT
{
  unsigned char  Index;             ///< switch on off 1...10, 0 means not configured
  unsigned char  PCII_Msk[3];       ///< PCII Mask
  unsigned char  PCII_Val[3];       ///< PCII Value
  unsigned char  Message;           ///< Message Index
  unsigned char  Status;            ///< Status
} EMV_CTLS_PHONEMSG_TYPE;

#endif

/// @defgroup PP3_PMT_NO_OF_ENTRIES Number of entries in Phone message table
/// @ingroup DEF_CONF_APPLI
/// @brief Number of entries in Phone message table
/// @{
#define   EMV_ADK_PMSG_ENTRIES           10
/// @}

/// @defgroup TAGS_NON_EMVCO Handling of tags not defined by EMVCo
/// @ingroup SPECIAL_TAG_HANDLING
///
/// @{
#define EMV_CTLS_MAX_NO_OF_NON_EMVCO_TAGS      25  ///< Max. number of non-EMVCo-tags included in additional tags (EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_TRM and EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_CRD)
#define EMV_CTLS_BUFFER_FOR_NON_EMVCO_TAGS   1230  ///< Calculation rule: max. tag value size + 10 bytes/tag
/// @}

#define EMV_CTLS_MAX_TAGLIST_SIZE      20  ///< Maximum number of tags in mandatory tag list, used for EMV_CTLS_TAGLIST_STRUCT::tags
#define EMV_CTLS_MAX_APP_VERS          10  ///< Maximum number of application versions

/// @ingroup DEF_CONF_APPLI
/// @brief structure for EMV_CTLS_PAYMENT_TYPE::Additional_Result_Tags
typedef struct EMV_CTLS_TAGLIST_STRUCT
{
  unsigned char   anztag;                           ///< Number of tags included in @c tags
  unsigned short  tags[EMV_CTLS_MAX_TAGLIST_SIZE];  ///< Buffer for tags (no length, no value) @n max. number of tags: #EMV_CTLS_MAX_TAGLIST_SIZE
} EMV_CTLS_TAGLIST_TYPE;


#ifdef OLD_CONFIG_INTERFACE
/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application
///   @n @deprecated Use @ref EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT instead
typedef struct EMV_CTLS_APPLIDATA_STRUCT
{
  // EMVCo general data
  unsigned long  CL_Modes;                   ///< General: Supported Contactless mode @n Possible values: see @ref CL_MODES @n Availability bit: #INPUT_CTLS_APL_SUPPORTED_CTLS @n TLV tag #TAG_C2_TRM_CL_MODES, @n XML Tag: #XML_TAG_APPLIDATA_CL_MODES
  unsigned char  AppName[16+1];              ///< General: Default application name to be used in case application label (tag 50) and application preferred name (tag 9F12) are not read from chip @n Availability bit: #INPUT_CTLS_APL_NAME @n TLV tag #TAG_50_APP_LABEL, @n XML Tag: #XML_TAG_APPLIDATA_APP_NAME
  unsigned char  ASI;                        ///< General: Application selection indicator @n Must the card's AID match the configured AID exactly? @n @c 0 ... yes @n @c 1 ... no @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_ASI @n TLV tag #TAG_DF20_ASI, @n XML Tag: #XML_TAG_APPLIDATA_ASI
  unsigned char  App_CountryCodeTerm[2];     ///< General: Change "Terminal country code" (EMV_CTLS_TERMDATA_STRUCT::CountryCodeTerm) (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_COUNTRY_CODE @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_APPLIDATA_COUNTRY_CODE_TERM
  unsigned char  BrKey[2];                   ///< General: Merchant category code @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_MERCHANT_CATCODE @n TLV tag #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_APPLIDATA_BR_KEY
  unsigned char  TermIdent[8];               ///< General: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliData @n Restriction for VFI reader: As the reader supports only one terminal ID all the applications must be configured with the same value! @n Availability bit: #INPUT_CTLS_APL_TID @n TLV tag #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_APPLIDATA_TERM_IDENT
  unsigned char  CL_CVM_Soft_Limit[4];       ///< General: Binary coded @n Below this limit CVM processing is skipped, above performed @n Equal to this limit it depends on the scheme: with payWave it's performed, with PayPass not @n Use FF FF FF FF to disable @n Visa and MasterCard spec naming: "Reader CVM Required Limit" @n Availability bit: #INPUT_CTLS_APL_CVM_LIMIT @n TLV tag #TAG_C0_TRM_CL_CVM_LIMIT, @n XML Tag: #XML_TAG_APPLIDATA_CL_CVM_SOFT_LIMIT
  unsigned char  CL_Ceiling_Limit[4];        ///< General: Binary coded @n No contactless txns above this limit (as defined by PayPass) @n PayPass spec naming: "Reader Contactless Transaction Limit" @n Visa spec naming: "Reader Contactless Transaction Limit (RCTL)" @n Visa spec strongly recommends to disable this limit. @n Use FF FF FF FF to disable @n Availability bit: #INPUT_CTLS_APL_CEILING_LIMIT @n TLV tag #TAG_C1_TRM_CL_CEIL_LIMIT, @n XML Tag: #XML_TAG_APPLIDATA_CL_CEILING_LIMIT
  unsigned char  FloorLimit[4];              ///< General: Terminal floor limit (binary coded) @n Use FF FF FF FF to disable @n Availability bit: #INPUT_CTLS_APL_FLOOR_LIMIT @n TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT, @n XML Tag: #XML_TAG_APPLIDATA_FLOOR_LIMIT
  unsigned char  CHPVerNum[2];               ///< General: Application chip version number @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_CHPVERSION @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_CHPAPPLIDATA_VER_NUM
  unsigned char  MSRVerNum[2];               ///< General: Application msr version number @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_MSRVERSION @n TLV tag #TAG_9F6D_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_MSRAPPLIDATA_VER_NUM
  unsigned char  Additional_Versions_No[2*EMV_CTLS_MAX_APP_VERS]; ///< General: up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2) @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM @n Availability bit #INPUT_CTLS_APL_ADD_VERSIONS
  unsigned char  MerchIdent[15];             ///< General: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_MERCHANT_IDENT @n TLV tag #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_APPLIDATA_MERCH_IDENT

  // parameters for domestic / local application selection and processing handling
  EMV_CTLS_APPLI_TYPE  xAIDPrio[EMV_ADK_MAX_PRIO_APP];  ///< General: Priority applications for this application (::EMV_CTLS_APPLI_STRUCT) @n max. number see #EMV_ADK_MAX_PRIO_APP @n Availability bit: #INPUT_CTLS_APL_PRIO_APPS @n TLV tag #TAG_DF1D_PRIO_APPLI, @n XML Tag: #XML_TAG_APPLIDATA_AID_PRIO
  unsigned char        xuc_Special_TRX[8];              ///< General: List of special allowed transactions (manual reversal, refund, reservation, tip, Referral), see @ref SPECIAL_TRXS @n Availability bit: #INPUT_CTLS_APL_SPECIAL_TXN @n TLV tag #TAG_DF1C_SPECIAL_TRX, @n XML Tag: #XML_TAG_APPLIDATA_SPECIAL_TRX

  // flow capabilities
  unsigned char  App_FlowCap[5];  ///< General: Configure special application characteristics, see @ref APP_FLOW_CAPS @n Availability bit: #INPUT_CTLS_APL_FLOW_CAPS @n TLV tag #TAG_DF2B_APP_FLOW_CAP, @n XML Tag: #XML_TAG_APPLIDATA_APP_FLOW_CAP
  unsigned char  RetapFieldOff;   ///< General: On mobile retap the RF field is switched off. This parameter determines for how long. Unit: 0.1 second. Vertex only.  @n TLV tag #TAG_DF30_RETAP_FIELD_OFF @n XML Tag: #XML_TAG_APPLIDATA_RETAP_FIELD_OFF @n Availability bit #INPUT_CTLS_APL_RETAP_FIELD_OFF

  // optional data
  unsigned char      Additional_Tags_TRM[EMV_ADK_ADD_TAG_SIZE];  ///< General: Additional terminal data for special applications @n Variants: a) list of primitive tags, b) primitive tags embedded in constructed tags defining the format (see #TAG_E2_FORMAT_B, #TAG_E3_FORMAT_N, ...) @n restriction: Tag length must not exceed 4 bytes @n Availability bit: #INPUT_CTLS_APL_ADD_TAGS @n TLV tag #TAG_DF29_ADD_TAGS, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM
  EMV_CTLS_DOL_TYPE  Additional_Tags_CRD;                        ///< @brief General: Tag + maximum Length: Additional ICC tags for special applications @n e.g. domestic cards with additional tags in ReadRecords, fleetcards with additional customer specific tags and others @n see ::EMV_CTLS_DOL_STRUCT @n restriction: only 1- and 2-byte-tags are supported, no support for 3-byte-tags
                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_ADD_CRD_TAGS @ TLV tag #TAG_DF2C_ADD_TAGS_CRD, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD

  // EMVCo tags that may be used by by the schemes but with different approach or for host compliancy only
  unsigned char  TermCap[3];      ///< General: "terminal capabilities" (EMV_CTLS_APPLIDATA_STRUCT::TermCap) for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_TERM_CAPS @n TLV tag #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_CAP
  unsigned char  TermAddCap[5];   ///< General: "additional terminal capabilities" (EMV_CTLS_APPLIDATA_STRUCT::TermAddCap) for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_ADD_TERM_CAPS  @n TLV tag #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_ADD_CAP
  unsigned char  App_TermTyp;     ///< General: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_TRM_TYPE @n TLV tag #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_TYP

  // PayPass3, EPAL, Amex specific parameters
  unsigned char           TACDenial[5];                             ///< Paypass: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_TAC_DENIAL @n TLV tag #TAG_DF21_TAC_DENIAL, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DENIAL
  unsigned char           TACOnline[5];                             ///< Paypass: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_TAC_ONLINE @n TLV tag #TAG_DF22_TAC_ONLINE, @n XML Tag: #XML_TAG_APPLIDATA_TAC_ONLINE
  unsigned char           TACDefault[5];                            ///< Paypass: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliData @n Availability bit: #INPUT_CTLS_APL_TAC_DEFAULT @n TLV tag #TAG_DF23_TAC_DEFAULT, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DEFAULT
  EMV_CTLS_DOL_TYPE       Default_TDOL;                             ///< @brief Paypass: Default TDOL (1st byte: length), maximum length see #EMV_ADK_MAX_LG_TDOL
                                                                    ///< @n Availability bit: #INPUT_CTLS_APL_TDOL @n TLV tag #TAG_DF27_DEFAULT_TDOL, @n XML Tag: #XML_TAG_APPLIDATA_DEFAULT_TDOL

  // PayPass3
  EMV_CTLS_PHONEMSG_TYPE  PhoneMessageTable[EMV_ADK_PMSG_ENTRIES];  ///< PayPass: Phone message table, up to 10 entries @n Availability bit: #INPUT_CTLS_APL_PP3_PMSG_TABLE @n TLV tag #TAG_FB_PP3_PMSG_TABLE, @n XML Tag: #XML_TAG_APPLIDATA_PP3_PMSG_TABLE
  unsigned char           MagstripeCVM_aboveLimit;                  ///< Paypass: separate cardholder verification methods for CTLS magstripe transactions for PayPass3 above the CVM limit @n PayPass3 tag: @c DF811E @n For values see PayPass spec, examples: 0x10=Signature, 0x20=OnlinePIN @n Availability bit: #INPUT_CTLS_APL_MSR_CVM_ABOVE @n TLV tag #TAG_DF2F_MSR_CVM_ABOVE, @n XML Tag: #XML_TAG_APPLIDATA_MSR_CVM_ABOVE
  unsigned char           MagstripeCVM_belowLimit;                  ///< Paypass: separate cardholder verification methods for CTLS magstripe transactions for PayPass3 below the CVM limit @n PayPass3 tag: @c DF812C @n For values see PayPass spec, examples: 0x10=Signature, 0x20=OnlinePIN @n Availability bit: #INPUT_CTLS_APL_MSR_CVM_BELOW @n TLV tag #TAG_DF10_MSR_CVM_BELOW, @n XML Tag: #XML_TAG_APPLIDATA_MSR_CVM_BELOW
  unsigned char           ChipCVM_belowLimit;                       ///< Paypass: separate cardholder verification methods for CTLS chip transactions for PayPass3 below the CVM limit (former Byte 2 of 9F33) @n Availability bit: #INPUT_CTLS_APL_CHIP_CVM_BELOW @n TLV tag #TAG_DF44_CHIP_CVM_BELOW, @n XML Tag: #XML_TAG_APPLIDATA_CHIP_CVM_BELOW
  unsigned char           CL_Ceiling_LimitMobile[4];                ///< PayPass: Binary coded @n No contactless txns above this limit for PayPass mobiles @n Availability bit: #INPUT_CTLS_APL_CEIL_LIMIT_MOBILE @n TLV tag #TAG_DF49_CEIL_LIMIT_MOBILE, @n XML Tag: #XML_TAG_APPLIDATA_CEILING_LIMIT_MOBILE
  unsigned char           TRN_TXN_Lifetime[2];                      ///< Paypass: lifetime for torn transaction, 0 means no torn txns are stored @n Availability bit: #INPUT_CTLS_APL_TORN_LIFETIME @n TLV tag #TAG_DF45_CHIP_TXN_LIFETIME, @n XML Tag: #XML_TAG_APPLIDATA_TORN_TXN_LIFETIME
  unsigned char           TRN_TXN_Number;                           ///< Paypass: Number of torn transaction to stored. Please be aware that this value should be consistent over the AIDs because otherwise the size of the torn txn store will be changed accordingly each time this AID is used for a transaction @n Availability bit: #INPUT_CTLS_APL_TORN_NUMBER @n TLV tag #TAG_DF46_CHIP_TXN_NO, @n XML Tag: #XML_TAG_APPLIDATA_TORN_TXN_NO
  unsigned char           TXN_CategoryCode;                         ///< Paypass: transaction category code:  ##INPUT_CTLS_APL_TORN_NUMBER @n TLV tag #TAG_9F53_TRANS_CATEGORY_CODE, @n XML Tag: #XML_TAG_APPLIDATA_TTC

  // payWave, DPAS, qpBOC, JCB specific parameters
  unsigned char           MerchantName_Location[30];           ///< Visa based General: @n Availability bit: #INPUT_CTLS_APL_MERCHANT TLV @n TLV tag #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_APPLIDATA_MERCH_NAME_LOCATION
  unsigned char           TTQ[4];                              ///< Visa based General: Visa-defined Terminal Transaction Qualifiers @n Availability bit: #INPUT_CTLS_APL_TTQ @n TLV tag #TAG_9F66_TTQ, @n XML Tag: #XML_TAG_APPLIDATA_VISA_TTQ

  // payWave specific parameters
  EMV_CTLS_VISA_DRL_TYPE  VisaDRLParams[EMV_ADK_DRL_ENTRIES_VISA];  ///< PayWave: dynamic reader limits, see ::EMV_CTLS_VISA_DRL_STRUCT @n Availability bit: #INPUT_CTLS_APL_VISA_DRL @n TLV tag #TAG_FA_VISA_DRL_RISK, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_PARAMS

  // Interac specific
  unsigned char  MTI_Merchant_Type_Ind;	     ///< Interac: Merchant Type Indicator (MTI) @n Info: VFI reader tag 9F58 @n Availability bit: #INPUT_CTLS_APL_MTI_MERCHANT_TYPE_IND @n TLV tag #TAG_9F58_MERCH_TYPE_INDICATOR, @n XML Tag: #XML_TAG_APPLIDATA_MTI_MERCHANT_TYPE_IND
  unsigned char  TTI_Term_Trans_Info[3];     ///< Interac: Terminal Transaction Information (TTI) @n Info: VFI reader tag 9F59 @n Default value: DC 87 00 @n Availability bit: #INPUT_CTLS_APL_TTI_TERM_TRANS_INFO @n TLV tag #TAG_9F59_TERM_TRANS_INFO, @n XML Tag: #XML_TAG_APPLIDATA_TTI_TERM_TRANS_INFO
  unsigned char  TTT_Term_Trans_Type;	       ///< Interac: Terminal Transaction Type (TTT) @n Info: VFI reader tag 9F5A @n Availability bit: #INPUT_CTLS_APL_TTT_TERM_TRANS_TYPE @n TLV tag #TAG_9F5A_TERM_TRANS_TYPE, @n XML Tag: #XML_TAG_APPLIDATA_TTI_TERM_TRANS_TYPE
  unsigned char  TOS_Term_Option_Status[2];	 ///< Interac: Terminal option status (TOS) @n Info: VFI reader tag: 9F5E @n Availability bit: #INPUT_CTLS_APL_TOS_TERM_OPTION_STATUS @n TLV tag #TAG_9F5E_TERM_OPTION_STATUS, @n XML Tag: #XML_TAG_APPLIDATA_TOS_TERM_OPTION_STATUS
  unsigned char  Term_CTLS_Receipt_REQLimit[EMV_ADK_BCD_AMOUNT_LEN];       ///< Interac: Interac Terminal CTLS Receipt required limit (numeric) @n Availability bit: #INPUT_CTLS_APL_TERM_RECEIPT_REQLIMIT @n TLV tag #TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT, @n XML Tag: #XML_TAG_APPLIDATA_TERM_RECEIPT_REQUIRED_LIMIT
  unsigned char  ThresholdBCD[6];            ///< Interac: Threshold Value for Biased Random Online Selection during risk management (n12) @n TLV tag #TAG_DF24_THRESHHOLD @n Availability bit #INPUT_CTLS_APL_THRESHOLD_BCD @n XML tag: #XML_TAG_APPLIDATA_THRESHOLD_BCD
  unsigned char  TargetPercentage;           ///< Interac: Target percentage for random online selection during risk management (n2) @n TLV tag #TAG_DF26_PERCENT_ONL @n Availability bit #INPUT_CTLS_APL_TARGET_PERCENTAGE @n XML tag: #XML_TAG_APPLIDATA_TARGET_PERCENTAGE
  unsigned char  MaxTargetPercentage;        ///< Interac: Maximum target percentage for random online selection during risk management (n2) @n TLV tag #TAG_DF25_MAXPERCENT_ONL @n Availability bit #INPUT_CTLS_APL_MAX_TARGET_PERCENTAGE @n XML tag: #XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE
  unsigned char  TryAgainLimit;              ///< Interac: allowed number of retries for Interac AID after TRY AGAIN result (n1), defaults to 3, Interac retry limit (allowed number of retries after TRY AGAIN) @n TLV tag #TAG_DF6D_TRY_AGAIN_LIMIT @n Availability bit #INPUT_CTLS_APL_TRY_AGAIN_LIMIT @n XML tag: #XML_TAG_APPLIDATA_TRY_AGAIN_LIMIT


  // American Express specific
  EMV_CTLS_AMEX_DRL_TYPE  AmexDRLParams[EMV_ADK_DRL_ENTRIES_AMEX];  ///< Expresspay: dynamic reader limits, see ::EMV_CTLS_AMEX_DRL_STRUCT @n Availability bit: #INPUT_CTLS_APL_AMEX_DRL  @n TLV tag #TAG_FD_AMEX_DRL_RISK, @n XML Tag: #XML_TAG_APPLIDATA_AMEX_DRL_PARAMS
  unsigned char           AMEX_TerminalCaps;                        ///< Expresspay: special terminal capabilities of Expresspay, Amex tag 9F6D, indicating the version of Amex spec used for CTLS  @n TLV tag #TAG_9F6D_AMEX_CAPABILITIES, @n XML Tag: #XML_TAG_AMEX_TERMINAL_CAPS @n Availability bit #INPUT_CTLS_APL_AMEX_TERMCAPS
  unsigned char           AMEX_Enhanced_TerminalCaps[4];            ///< Expresspay: special enhanced reader capabilities of Expresspay, Amex tag 9F6E. CV is performed both above and below the floor limit but there are different procedures for the kernel to follow in each case. The kernel no longer uses byte 2 of tag 9F33 to determine supported CV methods because byte 2 of tag 9F6E is now used instead. However only 1 value is required for tag 9F6E because the kernel itself modifies this bitmap depending if the CV limit is exceeded or not. Only 1 value for tag 9F33 is required because the kernel still uses byte 3 of this tag to determine supported DA methods @n TLV tag #TAG_9F6E_AMEX_ENHANCED_CAPABILITIES @n XML_TAG_AMEX_TERMINAL_CAPS @n Availability bit #INPUT_CTLS_APL_AMEX_ENH_READER_CAPS


  // formal parameters
  unsigned char  Chksum_Params[5];                                ///< General: Terminal parameters defined as major @n <B> only for checksum calculation </B> @n not all of those parameters are configurable yet but may be in future @n Availability bit: #INPUT_CTLS_APL_CHECKSUM_PARAMS @n TLV tag #TAG_DF13_TERM_PARAM, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_PARAMS
           char  Chksum_EntryPoint[EMV_ADK_CHECKSUM_ASCII_SIZE];  ///< General: Entrypoint checksum dynamically calculated by the configuration @n Only valid in EMV_CTLS_GetAppliData() @n Availability bit: #INPUT_CTLS_APL_CHECKSUM_ENTRYPNT @n TLV tag #TAG_DF3B_PARAMETER_1, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_ENTRYPNT
           char  Chksum_Kernel[EMV_ADK_CHECKSUM_ASCII_SIZE];      ///< General: Kernel checksum dynamically calculated by the configuration @n Only valid in EMV_CTLS_GetAppliData() @n Availability bit: #INPUT_CTLS_APL_CHECKSUM_KERNEL @n TLV tag #TAG_DF12_CHECKSUM, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_KERNEL

  EMV_CTLS_APPLI_TYPE  MasterAID;  ///< General: master AID (internal use) with the same parameters (stored for grouping reasons), Turkey request to configure more than one AID at once @n Availability bit: #INPUT_CTLS_MASTER_AID  @n TLV tag #TAG_DF04_AID, @n XML Tag: #XML_TAG_APPLIDATA_MASTER_AID

  unsigned char  Info_Included_Data[8];      ///< General: Which data is included in the message
} EMV_CTLS_APPLIDATA_TYPE;  ///< typedef for EMV_CTLS_APPLIDATA_STRUCT


/// @defgroup DEF_INPUT_APPLI Application data: Which data is valid
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_STRUCT::Info_Included_Data
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_SUPPORTED_CTLS     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_STRUCT::CL_Modes
#define  INPUT_CTLS_APL_CHPVERSION         0x02  ///< B1b2: EMV_CTLS_APPLIDATA_STRUCT::CHPVerNum
#define  INPUT_CTLS_APL_NAME               0x04  ///< B1b3: EMV_CTLS_APPLIDATA_STRUCT::AppName
#define  INPUT_CTLS_APL_ASI                0x08  ///< B1b4: EMV_CTLS_APPLIDATA_STRUCT::ASI
#define  INPUT_CTLS_APL_MERCHANT_CATCODE   0x10  ///< B1b5: EMV_CTLS_APPLIDATA_STRUCT::BrKey
#define  INPUT_CTLS_APL_TID                0x20  ///< B1b6: EMV_CTLS_APPLIDATA_STRUCT::TermIdent
#define  INPUT_CTLS_APL_CVM_LIMIT          0x40  ///< B1b7: EMV_CTLS_APPLIDATA_STRUCT::CL_CVM_Soft_Limit
#define  INPUT_CTLS_APL_CEILING_LIMIT      0x80  ///< B1b8: EMV_CTLS_APPLIDATA_STRUCT::CL_Ceiling_Limit
  /* Byte 2 */
#define  INPUT_CTLS_APL_FLOOR_LIMIT        0x01  ///< B2b1: EMV_CTLS_APPLIDATA_STRUCT::FloorLimit
#define  INPUT_CTLS_APL_TAC_DENIAL         0x02  ///< B2b2: EMV_CTLS_APPLIDATA_STRUCT::TACDenial
#define  INPUT_CTLS_APL_TAC_ONLINE         0x04  ///< B2b3: EMV_CTLS_APPLIDATA_STRUCT::TACOnline
#define  INPUT_CTLS_APL_TAC_DEFAULT        0x08  ///< B2b4: EMV_CTLS_APPLIDATA_STRUCT::TACDefault
#define  INPUT_CTLS_APL_TDOL               0x10  ///< B2b5: EMV_CTLS_APPLIDATA_STRUCT::Default_TDOL
//#define  RFU_B2B6                          0x20
#define  INPUT_CTLS_APL_MERCHANT_IDENT     0x40  ///< B2b7: EMV_CTLS_APPLIDATA_STRUCT::MerchIdent
#define  INPUT_CTLS_APL_ADD_TAGS           0x80  ///< B2b8: EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_TRM
  /* Byte 3 */
#define  INPUT_CTLS_APL_TTQ                0x01  ///< B3b1: EMV_CTLS_APPLIDATA_STRUCT::TTQ
#define  INPUT_CTLS_APL_TERM_CAPS          0x02  ///< B3b2: EMV_CTLS_APPLIDATA_STRUCT::TermCap
#define  INPUT_CTLS_APL_FLOW_CAPS          0x04  ///< B3b3: EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap
#define  INPUT_CTLS_APL_COUNTRY_CODE       0x08  ///< B3b4: EMV_CTLS_APPLIDATA_STRUCT::App_CountryCodeTerm
#define  INPUT_CTLS_APL_ADD_TERM_CAPS      0x10  ///< B3b5: EMV_CTLS_APPLIDATA_STRUCT::TermAddCap
#define  INPUT_CTLS_APL_SPECIAL_TXN        0x20  ///< B3b6: EMV_CTLS_APPLIDATA_STRUCT::xuc_Special_TRX
#define  INPUT_CTLS_APL_CHECKSUM_PARAMS    0x40  ///< B3b7: EMV_CTLS_APPLIDATA_STRUCT::Chksum_Params
#define  INPUT_CTLS_APL_CHECKSUM_ENTRYPNT  0x80  ///< B3b8: EMV_CTLS_APPLIDATA_STRUCT::Chksum_EntryPoint
  /* Byte 4 */
#define  INPUT_CTLS_APL_TRM_TYPE           0x01  ///< B4b1: EMV_CTLS_APPLIDATA_STRUCT::App_TermTyp
#define  INPUT_CTLS_APL_RFU_B4B2           0x02  ///< B4b2: RFU
#define  INPUT_CTLS_APL_VISA_DRL           0x04  ///< B4b3: EMV_CTLS_APPLIDATA_STRUCT::VisaDRLParams
#define  INPUT_CTLS_APL_MERCHANT           0x08  ///< B4b4: EMV_CTLS_APPLIDATA_STRUCT::MerchantName_Location
#define  INPUT_CTLS_MASTER_AID             0x10  ///< B4b5: EMV_CTLS_APPLIDATA_STRUCT::MasterAID
#define  INPUT_CTLS_APL_MSRVERSION         0x20  ///< B4b6: EMV_CTLS_APPLIDATA_STRUCT::MSRVerNum
#define  INPUT_CTLS_APL_MSR_CVM_ABOVE      0x40  ///< B4b7: EMV_CTLS_APPLIDATA_STRUCT::MagstripeCVM_aboveLimit
#define  INPUT_CTLS_APL_PRIO_APPS          0x80  ///< B4b8: EMV_CTLS_APPLIDATA_STRUCT::xAIDPrio
/* Byte 5 */
#define  INPUT_CTLS_APL_ADD_CRD_TAGS       0x01  ///< B5b1: EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_CRD
#define  INPUT_CTLS_APL_PP3_PMSG_TABLE     0x02  ///< B5b2: EMV_CTLS_APPLIDATA_STRUCT::PhoneMessageTable
#define  INPUT_CTLS_APL_MSR_CVM_BELOW      0x04  ///< B5b3: EMV_CTLS_APPLIDATA_STRUCT::MagstripeCVM_belowLimit
#define  INPUT_CTLS_APL_CHIP_CVM_BELOW     0x08  ///< B5b4: EMV_CTLS_APPLIDATA_STRUCT::ChipCVM_belowLimit
#define  INPUT_CTLS_APL_CEIL_LIMIT_MOBILE  0x10  ///< B5b5: EMV_CTLS_APPLIDATA_STRUCT::CL_Ceiling_LimitMobile
#define  INPUT_CTLS_APL_TORN_LIFETIME      0x20  ///< B5b6: EMV_CTLS_APPLIDATA_STRUCT::TRN_TXN_Lifetime
#define  INPUT_CTLS_APL_TORN_NUMBER        0x40  ///< B5b7: EMV_CTLS_APPLIDATA_STRUCT::TRN_TXN_Number
#define  INPUT_CTLS_APL_CHECKSUM_KERNEL    0x80  ///< B5b8: EMV_CTLS_APPLIDATA_STRUCT::Chksum_Kernel
/* Byte 6 */
#define  INPUT_CTLS_APL_ADD_VERSIONS                0x01  ///< B6b1: EMV_CTLS_APPLIDATA_STRUCT::Additional_Versions_No, additional version number of AID (optional)
#define  INPUT_CTLS_APL_AMEX_TERMCAPS               0x02  ///< B6b2: EMV_CTLS_APPLIDATA_STRUCT::AMEX_TerminalCaps
#define  INPUT_CTLS_APL_MTI_MERCHANT_TYPE_IND       0x04  ///< B6b3: EMV_CTLS_APPLIDATA_STRUCT::MTI_Merchant_Type_Ind
#define  INPUT_CTLS_APL_TTI_TERM_TRANS_INFO       	0x08  ///< B6b4: EMV_CTLS_APPLIDATA_STRUCT::TTI_Term_Trans_Info
#define  INPUT_CTLS_APL_TTT_TERM_TRANS_TYPE         0x10  ///< B6b5: EMV_CTLS_APPLIDATA_STRUCT::TTT_Term_Trans_Type
#define  INPUT_CTLS_APL_TOS_TERM_OPTION_STATUS      0x20  ///< B6b6: EMV_CTLS_APPLIDATA_STRUCT::TOS_Term_Option_Status
#define  INPUT_CTLS_APL_TRANS_CATEGORY_CODE         0x40  ///< B6b7: EMV_CTLS_APPLIDATA_STRUCT::TXN_CategoryCode

/* Byte 7 */
#define  INPUT_CTLS_APL_TERM_RECEIPT_REQLIMIT       0x01  ///< B7b1: EMV_CTLS_APPLIDATA_STRUCT::Term_CTLS_Receipt_REQLimit
#define  INPUT_CTLS_APL_AMEX_ENH_READER_CAPS        0x02  ///< B7b2: EMV_CTLS_APPLIDATA_STRUCT::AMEX_Enhanced_TerminalCaps
#define  INPUT_CTLS_APL_AMEX_DRL                    0x04  ///< B7b3: EMV_CTLS_APPLIDATA_STRUCT::AmexDRLParams
#define  INPUT_CTLS_APL_THRESHOLD_BCD               0x08  ///< B7b4: EMV_CTLS_APPLIDATA_STRUCT::ThresholdBCD
#define  INPUT_CTLS_APL_MAX_TARGET_PERCENTAGE       0x10  ///< B7b6: EMV_CTLS_APPLIDATA_STRUCT::MaxTargetPercentage
#define  INPUT_CTLS_APL_TARGET_PERCENTAGE           0x20  ///< B7b5: EMV_CTLS_APPLIDATA_STRUCT::TargetPercentage
#define  INPUT_CTLS_APL_TRY_AGAIN_LIMIT             0x40  ///< B7b7: EMV_CTLS_APPLIDATA_STRUCT::TryAgainLimit
#define  INPUT_CTLS_APL_RETAP_FIELD_OFF             0x80  ///< B7b8: EMV_CTLS_APPLIDATA_STRUCT::RetapFieldOff

/// @}


/// @defgroup APP_FLOW_CAPS Defines for Application flow capabilities
/// @ingroup DEF_CONF_APPLI
/// @brief Application (transaction flow) capabilities (see EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap)
/// @{
// Byte 1, EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap[0]
#define  CTLS_READ_PP3_BALANCE_BEFORE_GAC  0x01  ///< B1b1: PayPass: Feature Read Balance for PayPass 3 before GenAC (configurable for PayPass3, VISA AOSA for example is mandatory and therefore not configurable)
#define  CTLS_READ_PP3_BALANCE_AFTER_GAC   0x02  ///< B1b2: PayPass: Feature Read Balance for PayPass 3 after GenAC (configurable for PayPass3, VISA AOSA for example is mandatory and therefore not configurable)
#define  CTLS_DOM_NO_FINAL_SELECT          0x04  ///< B1b3: General: Do not perform the final select if the AID belongs to a nonEMV/Domestic kernel (not available for VFI-Reader)
#define  CTLS_STATUS_CHECK_ENABLE          0x08  ///< B1b4: PayWave: Enable VISA Status check for one single unit of currency (1$ txn is going online for status check)
#define  CTLS_PP3_ENABLE_DATA_EXCHANGE     0x10  ///< B1b5: PayPass: Enable DataExchange for PayPass 3
#define  CTLS_LOA_ENABLE                   0x20  ///< B1b6: General: Enable list of applications processing for this AID (per default not recommended for CTLS txns because directory file is available)
#define  CTLS_ZERO_CHECK_ENABLE_SKIP       0x40  ///< B1b7: PayWave: Enable VISA zero check for zero amount transaction, skip AID for zero amount
#define  CTLS_ZERO_CHECK_ENABLE_ONLINE     0x80  ///< B1b8: PayWave: Enable VISA zero check for zero amount transaction, going online for 0 amount txns
// Byte 2, EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap[1]
#define  CTLS_CASH_SUPPORT                 0x01  ///< B2b1: General: (Vel: y VFI: y) Support of cash transactions
#define  CTLS_CASHBACK_SUPPORT             0x02  ///< B2b2: General: (Vel: y VFI: y) Support of cashback transactions
#define  CTLS_OMIT_NOT_ALLOWED_SELECTION   0x04  ///< B2b3: General: (Vel: y VFI: n) Omit not allowed candidates already during Entry Point selection process, e.g. above CTLS transaction limit. However: Warning: The EP defines Contactless transaction Limit >=, while MasterCard PP3 defines it as >. So do not set it for MasterCard AIDs
#define  CTLS_FORCE_ENTRY_POINT            0x08  ///< B2b4: General: (Vel: y VFI: n) Force Entry Point Processing of the former EMVCo Entry Point specification (not available for VFI-Reader)
#define  CTLS_FORCE_KERNEL_USAGE           0x10  ///< B2b5: General: (Vel: y VFI: n) Force Kernel ID to be used, even if EP tag 9F2A is available (not available for VFI-Reader)
#define  CTLS_LIST_OF_AIDS_IF_PPSE_MISSING_ONLY 0x20  ///< B2b6: General: List of AIDs method only if the PPSE directory does not exist in a card with the corresponding AID. Precondition: #CTLS_LOA_ENABLE is active.
#define  CTLS_LIST_OF_AIDS_IF_PPSE_FAILED_ONLY  0x40  ///< B2b7: General: "List of AID" method only if PPSE directory  exists  in the card but a response for PPSE request was badly formatted and PPSE stage failed. @n This feature is supported by VFI reader only. Precondition: #CTLS_LOA_ENABLE is active.
#define  CTLS_CARD_READ_TXN_ZERO_AMOUNT         0x80  ///< B2b8: General: If this bit is set then the reader will send an amount of zero to the card for card read transactions, otherwise the reader will send the normal amount
// Byte 3, EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap[2]
#define  CTLS_CHECK_INCONS_TRACK2_PAN      0x01  ///< B3b1: PayWave: Check consistency of track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT) provided by ICC. Validate format and that contents matches to #TAG_5A_APP_PAN. Visa Europe stuff VppT
#define  CTLS_SKIP_CARDSELECTION_APPS      0x02  ///< B3b2: General: If this bit is set this AID will be skipped if requiring explicit cardholder selection
#define  CTLS_SKIP_TXN_LIMIT_CHECK         0x04  ///< B3b3: General: If this bit is set for this AID the transaction limit check will be skipped and the AID remains in the candidate list
#define  CTLS_FORCE_ONLINE                 0x08  ///< B3b4: General: Feature "Force online" supported
#define  CTLS_CASHBACK_NOT_FORCED_CVM_VISA 0x10  ///< B3b5: PayWave and other VISA based CTLS schemes: per default cashback transactions require CVM and are forced online by setting floorlimit to 0, you can use standard CVM limit and floorlimit if you set this flag
#define  CTLS_WAIT_CARD_REMOVAL_END        0x20  ///< B3b6: General: (Vel: y VFI: y) Wait for card removal at the end of an EMV transaction before returning to application (on TC, AAC and ARQC --> Card Read Complete)
#define  CTLS_REQUEST_CDA_WITH_ARQC        0x40  ///< B3b7: On first GAC do request CDA despite cryptogram type is ARQC (P1 = '90'), e.g. for Interac.
#define  CTLS_START_REMOVAL_DETECTION      0x80  ///< B3b8: General: (Vel: y VFI: n) Start card removal procedure after kernel processing. This enables the service #EMV_CTLS_CardRemoval.
// Byte 4, EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap[3]
#define  CTLS_DISABLE_MOBILE_CVM           0x01  ///< B4b1: PayPass: disable "on device CVM" and change b6 of DF811B and B1b3 of 9F1D
#define  CTLS_EP_EXTENDED_SELECTION        0x02  ///< B4b2: General: (Vel: y VFI: n) enable the Entry Point extended selection (9F29 is used if present on ICC)
#define  CTLS_TRANS_TYPE_17_FOR_CASH       0x04  ///< B4b3: PayPass: enable mapping of transaction type for cash transaction from 0x01 to 0x17
#define  CTLS_ENABLE_OFFLINE_CASHBACK      0x08  /**< B4b4: General: Do not apply business rule: "Cash back is online only". Instead rely on terminal and card risk management.
                                                  * Anyhow the application might force the transaction online by transaction flow option.
                                                  * \n<b>Caution</b>: For Visa like schemes (Visa, Visa-AP, Discover and UPI) this flag has a different effect: It suppresses the mandory reduction of the floor limit to zero.*/
/// @}

#endif // OLD_CONFIG_INTERFACE
/// @defgroup APPLY_CONFIG_OPTION Options how to apply configuration (VFI reader only)
/// @ingroup DEF_CONF_APPLI
/// @brief Options for EMV_CTLS_ApplyConfiguration()
/// @{
#define  CTLS_APPLY_CFG_APPS   0x00000001  ///< Apply data from EMV_CTLS_APPLIDATA_STRUCT if something changed since last configuration to the reader
#define  CTLS_APPLY_CFG_TERM   0x00000002  ///< Apply data from EMV_CTLS_TERMDATA_STRUCT if something changed since last configuration to the reader
#define  CTLS_APPLY_CFG_CAPK   0x00000004  ///< Apply data from EMV_CTLS_CAPKEY_STRUCT if something changed since last configuration to the reader
#define  CTLS_APPLY_CFG_ALL    0x00000007  ///< Apply all configuration data if something changed since last configuration to the reader
#define  CTLS_APPLY_CFG_FORCE  0x00000008  ///< Apply all configuration data even in case nothing changed since last reader configuration
/// @}

// ========================================================================
// === APPLICATION CONFIGURATION (new scheme specific config interface) ===
// ========================================================================


/// @defgroup DEF_KERNEL_ID Contactless Kernel Ids
/// @ingroup DEF_CONF_APPLI
///
/// @brief Short or Extended Kernel Id as integer
///
/// Kernel Ids according to EMVCo and EMV-ADK's internal definitions for non-EMVCo registered kernel Ids.
/// First byte in case of Short Kernel Id or bytes 1-3 in case of Extended Kernel Id for
/// EMV_CTLS_APPLI_KERNEL_TYPE::KernelID. Used as integer for
/// #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::InternalKernelId_DFAB23
/// @n 0x01 ... 0x0F are reserved for the EMV L2 kernel
/// @n 0x10 ... 0x1F for international non EMVCo kernels (EPAL, Interac, ...)
/// @n domestic application kernels start with 0xF0 ... 0xFF
/// @{
// EMVCo standard
#define EMV_CTLS_KERN_EP   0x00  ///< Entry Point, not applicable for application configuration
#define EMV_CTLS_KERN_PK   0x01  ///< Visa Asia/Pacific
#define EMV_CTLS_KERN_MK   0x02  ///< PayPass (MasterCard, maestro, ...)
#define EMV_CTLS_KERN_VK   0x03  ///< payWave (Visa, Visa electron, V-PAY, ...)
#define EMV_CTLS_KERN_AK   0x04  ///< Expresspay (American Express)
#define EMV_CTLS_KERN_JK   0x05  ///< JCB
#define EMV_CTLS_KERN_DK   0x06  ///< Discover
#define EMV_CTLS_KERN_CK   0x07  ///< China Union Pay (CUP, UnionPay International)

// Verifone Internal
#define EMV_CTLS_KERN_IK   0x10  ///< Interac (Canada)
#define EMV_CTLS_KERN_EK   0x11  ///< EPAL (Australia)
#define EMV_CTLS_KERN_RK   0x12  ///< RuPay (India)
#define EMV_CTLS_KERN_GK   0x13  ///< gemalto PURE
#define EMV_CTLS_KERN_PB   0x14  ///< PagoBANCOMAT (Italy)
//                         0x15 VERTEX value for MIR, not used on ADK interface
#define EMV_CTLS_KERN_WK   0x16  ///< reserved for WISE
#define EMV_CTLS_KERN_BK   0x17  ///< reserved for CPACE

// EMVCo registered
#define EMV_CTLS_KERN_GC   0x2A  ///< girocard (Germany)
#define EMV_CTLS_KERN_SAMA 0x2D  ///< Saudi Arabian Monetary Authority, implicitly mapped to internal kernel Id #EMV_CTLS_KERN_GK

#define EMV_CTLS_KERN_DOM  0xF0  ///< @brief Domestic kernel
                                 ///< EMV ADK only handles application selection.
                                 ///< Payment processing has to be done by calling application.

// Extended Kernel Ids
#define EMV_CTLS_KERN_MR   0x810643 ///< MIR (Russia)
#define EMV_CTLS_KERN_SK   0xC1     ///< SIBS (Portugal) @deprecated use #EMV_CTLS_KERN_SB instead
#define EMV_CTLS_KERN_SB   0xC14D42 ///< SIBS MB (MULTIBANCO, Portugal)

/// @}

#define KERNEL_ID_TO_BYTE_ARRAY(kernelId, byteArray)\
    if ((kernelId)<0xff)\
    {\
        byteArray[0] = (unsigned char) (kernelId);\
        byteArray[1] = 0;\
        byteArray[2] = 0;\
    }\
    else\
    {\
        byteArray[0] = (unsigned char) ((kernelId) >> 16);\
        byteArray[1] = (unsigned char) ((kernelId) >> 8);\
        byteArray[2] = (unsigned char) ((kernelId));\
    }

#define KERNEL_ID_TO_INT(byteArray)\
    ((byteArray[0] >= EMV_CTLS_KERN_DOM && byteArray[1] == 0 && byteArray[2] == 0)\
     ? EMV_CTLS_KERN_DOM :\
    ((byteArray[0] & 0x80) ? ((unsigned) byteArray[0] << 16) | ((unsigned) byteArray[1] << 8) | byteArray[2] : byteArray[0]))

typedef struct EMV_CTLS_DATA_STRUCT {
  unsigned short  len;  ///< length of data
  unsigned char  *data; ///< data
} EMV_CTLS_DATA_TYPE;


/// @defgroup DEF_TEC technologies used in TecSupport_DFAB30
/// @ingroup DEF_CONF_APPLI
///
/// It's a bitlist, so values can be combined.
/// @n Used in EMV_CTLS_APPLIDATA_VK_STRUCT::TecSupport_DFAB30, EMV_CTLS_APPLIDATA_DK_STRUCT::TecSupport_DFAB30,
/// EMV_CTLS_APPLIDATA_CK_STRUCT::TecSupport_DFAB30, EMV_CTLS_APPLIDATA_AK_STRUCT::TecSupport_DFAB30
/// @n For Discover this parameter (DFAB30) has a specific meaning in terms of the legacy DISCOVER ZIP: 0x01 = DPAS Chip, 0x02 = DPAS MSR, 0x00 = LEGACY DISCOVER ZIP
/// @{
#define EMV_CTLS_TEC_CHIP  0x01  ///< Chip Support
#define EMV_CTLS_TEC_MSR   0x02  ///< MSR Support
/// @}

/// @defgroup DEF_MK_KERNCFG used in EMV_CTLS_APPLIDATA_MK_STRUCT::KernelConfiguration_DF811B
/// @ingroup DEF_CONF_APPLI
/// @{
#define EMV_CTLS_MK_CFG_NOMSR     0x80  ///< No MSR Support
#define EMV_CTLS_MK_CFG_NOCHIP    0x40  ///< No Chip Support
#define EMV_CTLS_MK_CFG_ONDEVCVM  0x20  ///< On Device Cardholder Verification Support
#define EMV_CTLS_MK_CFG_RRP       0x10  ///< Relay Resistance Protocal supported
/// @}

/// @defgroup DEF_RK_KERNCFG used in EMV_CTLS_APPLIDATA_RK_STRUCT::KernelConfiguration_DF811B
/// @ingroup DEF_CONF_APPLI
/// @{
#define EMV_CTLS_RK_CFG_ONDEVCVM  0x20  ///< On Device Cardholder Verification Support
/// @}

/// @defgroup DEF_FLOW_GLOB Application flow capabilities for all schemes
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AppFlowCap_DFAB03
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_GLOB_DOM_NO_FINAL_SELECT_0               0x01  ///< B1b1: (Vel: y VFI: n) Do not perform the final select if the AID belongs to a nonEMV/Domestic kernel
#define EMV_CTLS_FLOW_GLOB_LOA_ENABLE_0                        0x02  ///< B1b2: (Vel: y VFI: y) Enable list of applications processing for this AID (per default not recommended for CTLS txns because directory file is available)
#define EMV_CTLS_FLOW_GLOB_CASH_SUPPORT_0                      0x04  ///< B1b3: (Vel: y VFI: y) Support of cash transactions
#define EMV_CTLS_FLOW_GLOB_CASHBACK_SUPPORT_0                  0x08  ///< B1b4: (Vel: y VFI: y) Support of cashback transactions
#define EMV_CTLS_FLOW_GLOB_OMIT_NOT_ALLOWED_SELECTION_0        0x10  ///< B1b5: (Vel: y VFI: n) Omit not allowed candidates already during Entry Point selection process, e.g. above CTLS transaction limit. However: Warning: The EP defines Contactless transaction Limit >=, while MasterCard PP3 defines it as >. So do not set it for MasterCard AIDs
#define EMV_CTLS_FLOW_GLOB_FORCE_ENTRY_POINT_0                 0x20  ///< B1b6: (Vel: y VFI: n) Force Entry Point processing of the former EMVCo Entry Point specification. It has the effect like #EMV_CTLS_FLOW_GLOB_OMIT_NOT_ALLOWED_SELECTION_0 plus continue with START_B after L1 error at Combination Selection.
#define EMV_CTLS_FLOW_GLOB_FORCE_KERNEL_USAGE_0                0x40  ///< B1b7: (Vel: y VFI: n) Force Kernel ID to be used, even if EP tag 9F2A is available
#define EMV_CTLS_FLOW_GLOB_LIST_OF_AIDS_IF_PPSE_MISSING_ONLY_0 0x80  ///< B1b8: (Vel: y VFI: y) List of AIDs method only if PPSE directory does not exist in a card with the corresponding AID. Precondition: #EMV_CTLS_FLOW_GLOB_LOA_ENABLE_0 is set.
  /* Byte 2 */
#define EMV_CTLS_FLOW_GLOB_LIST_OF_AIDS_IF_PPSE_FAILED_ONLY_1  0x01  ///< B2b1: (Vel: n VFI: y) List of AIDs method only if PPSE directory exists in the card but a response for PPSE request was badly formatted and PPSE stage failed. Precondition: #EMV_CTLS_FLOW_GLOB_LOA_ENABLE_0 is set.
#define EMV_CTLS_FLOW_GLOB_CARD_READ_TXN_ZERO_AMOUNT_1         0x02  ///< B2b2: (Vel: y VFI: y) If this bit is set then the reader will send an amount of zero to the card for card read transactions, otherwise the reader will send the normal amount
#define EMV_CTLS_FLOW_GLOB_SKIP_CARDSELECTION_APPS_1           0x04  ///< B2b3: (Vel: y VFI: n) If this bit is set this AID will be skipped if requiring explicit cardholder selection
#define EMV_CTLS_FLOW_GLOB_SKIP_TXN_LIMIT_CHECK_1              0x08  ///< B2b4: (Vel: y VFI: n) If this bit is set for this AID the transaction limit check will be skipped and the AID remains in the candidate list
#define EMV_CTLS_FLOW_GLOB_FORCE_ONLINE_1                      0x10  ///< B2b5: (Vel: y VFI: n) Feature "Force online" supported (suspicious customer). This relates to Terminal Verification Result B4b4 "Merchant forced transaction online". Note: Force Online is performed if both flags are set, the support flag here and the transaction flow option #EMV_CTLS_PAYMENT_STRUCT::Force_Online.
#define EMV_CTLS_FLOW_GLOB_WAIT_CARD_REMOVAL_END_1             0x20  ///< B2b6: (Vel: y VFI: y) Wait for card removal at the end of an EMV transaction before returning to application (on TC, AAC and ARQC --> Card Read Complete)
#define EMV_CTLS_FLOW_GLOB_START_REMOVAL_DETECTION_1           0x40  ///< B2b7: (Vel: y VFI: n) Start card removal procedure after kernel processing. This enables the service #EMV_CTLS_CardRemoval. @n Only effective on deactivated #EMV_CTLS_FLOW_GLOB_WAIT_CARD_REMOVAL_END_1
#define EMV_CTLS_FLOW_GLOB_EP_EXTENDED_SELECTION_1             0x80  ///< B2b8: (Vel: y VFI: n) Enable the Entry Point extended selection (9F29 is used if present on ICC)
// byte3
#define EMV_CTLS_FLOW_GLOB_MULTIPLE_AID_KERNEL_ID_2            0x01  ///< B3b1: (Vel: y VFI: n) byte 3 of kernel id is used to store multiple configurations of same kernel Id-AID pair. If this flag is set the 3rd byte of the kernel id is set to zero in the Terminal Candidate List which is presented to the EntryPoint while the original value is used towards application interface.
#define EMV_CTLS_FLOW_GLOB_ENABLE_OFFLINE_CASHBACK_2           0x02  /**< B3b2: (Vel: y VFI: n) Do not apply business rule: "Cash back is online only". Instead rely on terminal and card risk management.
                                                                      * Anyhow the application might force the transaction online by transaction flow option EMV_CTLS_PAYMENT_STRUCT::Online_Switch.
                                                                      * \n<b>Caution</b>: For Visa like schemes (Visa, Visa-AP, Discover and UPI) this flag has a different effect: It suppresses the mandory reduction of the floor limit to zero.*/
#define EMV_CTLS_FLOW_GLOB_RESET_TXN_TYPE_2                    0x04  /**< B3b3: (Vel: y VFI: n) A custom transaction type ('9C') might be used during kernel processing in a virtual terminal for card read
                                                                      * transactions. Therefore tag '9C' has to be configured to @ref EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsTRM_DFAB20.
                                                                      * If this flag is raised the transaction type in the kernel database will be reset after completion (e.g. to '20' for refund) in order
                                                                      * to hide this modification from the outside world (vouchers, host messages) */
#define EMV_CTLS_FLOW_GLOB_CFG_APPL_NAME_2                     0x08  /**< B3b4: (Vel: y VFI: n) **Use with care**: Do not use card's application name (EMVCo rule: 9F12 prior to 50) but always use the default application label EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::DefaultApplicationName_DFAB22
                                                                      * in result data EMV_CTLS_TRANSRES_STRUCT::AppName */
#define EMV_CTLS_FLOW_GLOB_CARD_READ_SKIP_TXN_LIMIT_CHECK_2    0x10  ///< B3b5: (Vel: y VFI: n) If this bit is set for this AID the transaction limit check will be skipped for card read transactions and the AID remains in the candidate list
/// @}

/// @defgroup DEF_FLOW_MK Application flow capabilities for PayPass (MasterCard)
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_MK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_MK_READ_BALANCE_BEFORE_GAC_0             0x01  ///< B1b1: (Vel: y VFI: y) Feature Read Balance before GenAC
#define EMV_CTLS_FLOW_MK_READ_BALANCE_AFTER_GAC_0              0x02  ///< B1b2: (Vel: y VFI: y) Feature Read Balance after GenAC
#define EMV_CTLS_FLOW_MK_ENABLE_DATA_EXCHANGE_0                0x04  ///< B1b3: (Vel: y VFI: n) Enable DataExchange
#define EMV_CTLS_FLOW_MK_TRANSACTION_TYPE_17_FOR_CASH_0        0x08  ///< B1b4: (Vel: y VFI: n) Enable mapping of transaction type for cash transaction from 0x01 to 0x17
#define EMV_CTLS_FLOW_MK_CARD_READ_ACTION_ANALYSIS_0           0x10  ///< B1b5: (Vel: y VFI: n) On card read transaction do not force decline but let the kernel do TAC/IAC Action Analysis
/// @}

/// @defgroup DEF_FLOW_VK Application flow capabilities for payWave (Visa)
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_VK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_VK_STATUS_CHECK_ENABLE_0                 0x01  ///< B1b1: (Vel: y VFI: y) Enable status check for one single unit of currency (1$ txn is going online for status check)
#define EMV_CTLS_FLOW_VK_ZERO_CHECK_ENABLE_SKIP_0              0x02  ///< B1b2: (Vel: y VFI: y) Enable zero check for zero amount transaction, Option 2: skip AID for zero amount txns. Can't be combined with Option 2 in B1b3
#define EMV_CTLS_FLOW_VK_ZERO_CHECK_ENABLE_ONLINE_0            0x04  ///< B1b3: (Vel: y VFI: y) Enable zero check for zero amount transaction, Option 1: going online for zero amount txns.  Can't be combined with Option 1 in B1b2. This is the default option. This is the prioritary option if both options are set
#define EMV_CTLS_FLOW_VK_CHECK_INCONS_TRACK2_PAN_0             0x08  ///< B1b4: (Vel: y VFI: n) Check consistency of track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT) provided by ICC. Validate format and that contents matches to #TAG_5A_APP_PAN. Visa Europe stuff VppT
#define EMV_CTLS_FLOW_VK_CASHBACK_NOT_FORCED_CVM_0             0x10  ///< B1b5: (Vel: y VFI: n) Per default cashback transactions require CVM and are forced online by setting floorlimit to 0, you can use standard CVM limit and floorlimit if you set this flag
#define EMV_CTLS_FLOW_VK_SUCCESS_TONE_ON_DECLINE               0x20  ///< B1b6: @deprecated: If GPO results in AAC: default was changed to sound solely success tone
#define EMV_CTLS_FLOW_VK_ALERT_TONE_ON_DECLINE                 0x40  ///< B1b7: (Vel: y VFI: n) Per default the tone indicates if card read successful or not.
                                                                     ///< Outcome of the transaction is not indicated.
                                                                     ///< With this option an additional alert tone is sounded in case transaction is declined.
#define EMV_CTLS_FLOW_VK_GET_DATA_9F68_0                       0x80  ///< B1b8: (Vel: y VFI: n) Card Read Flow initiated with #EMV_ADK_TRAN_TYPE_READ_CARD_DATA for retrieval of tag 9F68 (Card Additional Processes)

// Byte 2, EMV_CTLS_APPLIDATA_VK_STRUCT::AppFlowCap_DFAB31[1]
#define EMV_CTLS_FLOW_VK_ENABLE_TAGS_AT_GPO                    0x01  ///< B2b1: (Vel: y VFI: n) Apply workaround that Visa kernel up to version 2.1.5 accepts certain tags at GPO step. These are 9F07 & 5F28 for instance.
#define EMV_CTLS_FLOW_VK_ZERO_CHECK_DISABLE_1                  0x02  ///< B2b2: (Vel: y VFI: n) Disable Visa zero check
// B2b3 RFU
#define EMV_CTLS_FLOW_VK_ODO_SUPPORT_1                         0x08  ///< B2b4: (Vel: y VFI n) VK2.2.3 only: Enable Offline Data Authentication for online transactions (ODO) according "Visa ODA for qVSDC Online Specification". Note: To enable this feature, additionally TTQ B1b1 (9F66, Offline Data Authentication for Online Authorizations supported) must be set.
#define EMV_CTLS_FLOW_VK_ODO_SDA_1                             0x10  ///< B2b5: (Vel: y VFI n) VK2.2.3 only: Enable SDA for ODO
#define EMV_CTLS_FLOW_VK_ODO_DDA_1                             0x20  ///< B2b6: (Vel: y VFI n) VK2.2.3 only: Enable fDDA for ODO
#define EMV_CTLS_FLOW_VK_ODO_SDA_ERR_ONLINE_1                  0x40  ///< B2b7: (Vel: y VFI n) VK2.2.3 only: Outcome for ODO SDA error: 0 = Decline, 1 = Go online
#define EMV_CTLS_FLOW_VK_ODO_DDA_ERR_ONLINE_1                  0x80  ///< B2b8: (Vel: y VFI n) VK2.2.3 only: Outcome for ODO fDDA error: 0 = Decline, 1 = Go online

// Byte 3, EMV_CTLS_APPLIDATA_VK_STRUCT::AppFlowCap_DFAB31[2]
#define EMV_CTLS_FLOW_VK_OMIT_AUC_INT_CASH_2                   0x01  ///< B3b1: (Vel: y VFI n) Omit AUC "International Cash" check
#define EMV_CTLS_FLOW_VK_OMIT_AUC_INT_CASHBACK_2               0x02  ///< B3b2: (Vel: y VFI n) Omit AUC "International Cashback" check
#define EMV_CTLS_FLOW_VK_OMIT_AUC_DOM_CASH_2                   0x04  ///< B3b3: (Vel: y VFI n) Omit AUC "Domestic Cash" check
#define EMV_CTLS_FLOW_VK_OMIT_AUC_DOM_CASHBACK_2               0x08  ///< B3b4: (Vel: y VFI n) Omit AUC "Domestic Cashback" check
#define EMV_CTLS_FLOW_VK_ZERO_CASHBACK_AMOUNT_2                0x10  ///< B3b5: (Vel: y VFI n) Set Cashback amount to zero for Cashback transactions. Workaround to allow Cashback transactions with Russian cards not supporting domestic Cashback
#define EMV_CTLS_FLOW_VK_CASHBACK_TRANS_TYPE_09_2              0x20  ///< B3b6: (Vel: y VFI n) Use Transaction Type 09 for cashback transactions. VCSP 2.2 mandates Transaction Type 00.

/// @}

/// @defgroup DEF_FLOW_AK Application flow capabilities for ExpressPay (Amex)
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_AK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_AK_CARDREAD_GENAC1_ARQC_0               0x01  ///> B1b1: (Vel: y VFI: n) Card read transaction (e.g. refund): request ARQC in GENAC1, usually AAC is requested
/// @}

/// @defgroup DEF_FLOW_JK Application flow capabilities for JCB
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_JK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_JK_MULTIPLE_RANDOM_NUMBERS_0             0x01  ///< B1b1: (Vel: y VFI: ?) Generate new random number ('9F37') with each kernel activation, that is 2nd GAC
/// @}

/// @defgroup DEF_FLOW_DK Application flow capabilities for Discover/Diners
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_DK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_DK_CASHBACK_NOT_FORCED_CVM_0             0x01  ///< B1b1: (Vel: y VFI: n) Per default cashback transactions require CVM and are forced online by setting floorlimit to 0, you can use standard CVM limit and floorlimit if you set this flag
#define EMV_CTLS_FLOW_DK_DATA_STORAGE_0           0x02  ///< B1b2: (Vel: y VFI: n) DPAS-2: Activate kernel feature Data Storage
#define EMV_CTLS_FLOW_DK_EXTENDED_LOGGGING_0      0x04  ///< B1b3: (Vel: y VFI: n) DPAS-2: Activate kernel feature Extended Logging
#define EMV_CTLS_FLOW_DK_TORN_TXN_RECOVERY_0      0x08  ///< B1b4: (Vel: y VFI: n) DPAS-2: Activate kernel feature Torn Transaction Recovery
#define EMV_CTLS_FLOW_DK_DEFERRED_AUTHORISATION_0 0x10  ///< B1b5: (Vel: y VFI: n) DPAS-2: Activate kernel feature Deferred Authorisation
/// @}

/// @defgroup DEF_FLOW_IK Application flow capabilities for Interac
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_IK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_IK_REQUEST_CDA_WITH_ARQC_0               0x01  ///< B1b1: (Vel: y VFI: n) On first GAC do request CDA despite cryptogram type is ARQC (P1 = '90').
#define EMV_CTLS_FLOW_IK_REFUND_FLOW_INTERAC_0                 0x02  ///< B1b2: (Vel: y VFI: n) Do not force AppAction to decline and do not modify and TAC or limit. Note: Unless otherwise specified by regional needs, Interac flow should be enabled for Interac AIDs.
/// @}

/// @defgroup DEF_FLOW_EK Application flow capabilities for EPAL
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_EK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
/// @}

/// @defgroup DEF_FLOW_PK Application flow capabilities for Visa Asia/Pacific
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_PK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_PK_CHECK_INCONS_TRACK2_PAN_0             0x01  ///< B1b1: (Vel: y VFI: n) Check consistency of track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT) provided by ICC. Validate format and that contents matches to #TAG_5A_APP_PAN. Visa Europe stuff VppT
#define EMV_CTLS_FLOW_PK_CASHBACK_NOT_FORCED_CVM_0             0x02  ///< B1b2: (Vel: y VFI: n) Per default cashback transactions require CVM and are forced online by setting floorlimit to 0, you can use standard CVM limit and floorlimit if you set this flag
/// @}

/// @defgroup DEF_FLOW_CK Application flow capabilities for China Union Pay
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_CK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_CK_STATUS_CHECK_ENABLE_0                 0x01  ///< B1b1: (Vel: y VFI: y) Enable status check for one single unit of currency (1$ txn is going online for status check)
#define EMV_CTLS_FLOW_CK_ZERO_CHECK_ENABLE_SKIP_0              0x02  ///< B1b2: (Vel: y VFI: y) Enable zero check for zero amount transaction, skip AID for zero amount
#define EMV_CTLS_FLOW_CK_ZERO_CHECK_ENABLE_ONLINE_0            0x04  ///< B1b3: (Vel: y VFI: y) Enable zero check for zero amount transaction, going online for zero amount txns
#define EMV_CTLS_FLOW_CK_CHECK_INCONS_TRACK2_PAN_0             0x08  ///< B1b4: (Vel: y VFI: n) Check consistency of track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT) provided by ICC. Validate format and that contents matches to #TAG_5A_APP_PAN. Visa Europe stuff VppT
#define EMV_CTLS_FLOW_CK_CASHBACK_NOT_FORCED_CVM_0             0x10  ///< B1b5: (Vel: y VFI: n) Per default cashback transactions require CVM and are forced online by setting floorlimit to 0, you can use standard CVM limit and floorlimit if you set this flag
#define EMV_CTLS_FLOW_CK_ENABLE_FDDA_VERSION_0                 0x20  ///< B1b6: (Vel: y VFI: n) Per default the legacy fDDA version 0 is switched off. If you want to support it on top of the up date fDDA version 1. you can enable it by setting this bit
#define EMV_CTLS_FLOW_CK_DISABLE_PIN_BELOW_CVM_0               0x40  ///< B1b7: (Vel: y VFI: y) Do not ask for online PIN and Signature for transactions below CVM Limit when this was requested by card. This bit should be set for Credit / Quasi Credit AIDs according to Ref. "UPI QuickPass Implementation Guide for Acquirers v1.3.9": Terminals shall not ask for online PIN when all of below situations are met -QuickPass credit/Quasi credit cards are accepted, -Contactless transaction amount is below CVM limit of the terminal.
/// @}

/// @defgroup DEF_FLOW_GK Gemalto/Pure: Application flow capabilities
/// @ingroup DEF_CONF_APPLI
/// @brief Defines for EMV_CTLS_APPLIDATA_GK_STRUCT::AppFlowCap_DFAB31
/// @{
/* Byte 1 (index 0) */
#define EMV_CTLS_FLOW_GK_SUPPORT_APPLI_AUTHENTICATE_0   0x01  ///< B1b1: (Vel: y VFI: y) Support for special transaction type "Application Authentication Transaction", see also #EMV_CTLS_TRAN_TYPE_PURE_AUTHENTICATE
#define EMV_CTLS_FLOW_GK_SUPPORT_GETDATA_0              0x02  ///< B1b2: (Vel: y VFI: y) Support for special transaction type "Retrieve application data elements using GET DATA command", see also #EMV_CTLS_TRAN_TYPE_PURE_GETDATA
#define EMV_CTLS_FLOW_GK_SUPPORT_PUTDATA_0              0x04  ///< B1b3: (Vel: y VFI: y) Support for special transaction type "Update application data elements using PUT DATA command", see also #EMV_CTLS_TRAN_TYPE_PURE_PUTDATA
#define EMV_CTLS_FLOW_GK_AMOUNT_MULTIPLY_100_0          0x08  ///< B1b4: (Vel: y VFI: n) Vietnam VCCS special feature: Multiply amount by 100 before using it in GENAC1
/// @}

/// @defgroup DEF_FLOW_RK Application flow capabilities for RuPay
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_RK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
#define EMV_CTLS_FLOW_RK_SERVICE_FEATURE_ENABLE_0              0x01  ///< B1b1: (Vel: y VFI: y) Rupay Service Feature Enable/Disable
#define EMV_CTLS_FLOW_RK_TORN_TRANSACTION_INDICATOR_0          0x02  ///< B1b2: (Vel: y VFI: y) Rupay Torn Transaction Indicator
#define EMV_CTLS_FLOW_RK_READ_RECORD_SFI1_QDDA_0               0x04  ///< B1b3: (Vel: n VFI: y) Perform READ RECORD of SFI 1 only when qDDA is disabled
#define EMV_CTLS_FLOW_RK_MULTIPLE_RANDOM_NUMBERS_0             0x08  ///< B1b4: (Vel: y VFI: n) Generate new random number ('9F37') with each kernel activation, that is 2nd GAC
#define EMV_CTLS_FLOW_RK_ONLINE_PIN_SUPPORTED_0                0x10  ///< B1b5: (Vel: y VFI: n) Online PIN supported
#define EMV_CTLS_FLOW_RK_SIGNATURE_SUPPORTED_0                 0x20  ///< B1b6: (Vel: y VFI: n) Signature supported
#define EMV_CTLS_FLOW_RK_ON_DEVICE_CVM_SUPPORTED_0             0x40  ///< B1b7: (Vel: y VFI: n) On-Device CVM (mobile CVM) supported
#define EMV_CTLS_FLOW_RK_CVM_REQUIRED_0                        0x80  ///< B1b8: (Vel: y VFI: n) CVM required, transaction will fail in case card decision is "no CVM)

  /* Byte 2 */
#define EMV_CTLS_FLOW_RK_ISSUER_UPDATE_SUPPORTED_1             0x01  ///< B2b1: (Vel: y VFI: n) Issuer script processing supported
/// @}

/// @defgroup DEF_FLOW_SK Application flow capabilities for SIBS
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_SK_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
/// @}

/// @defgroup DEF_FLOW_PB Application flow capabilities for PagoBancomat
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_PB_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
/// @}

/// @defgroup DEF_FLOW_MR Application flow capabilities for MIR
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_MR_STRUCT::AppFlowCap_DFAB31
/// @{
  /* Byte 1 */
/// @}


////////////////
// MasterCard //
////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, MK specific part. See see Book C-2, Kernel 2 Specification at <a href="http://www.emvco.com/">[EMVCo Homepage]</a>.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_MK_STRUCT {
  unsigned char                         IncludedData[8];                              ///< Availability bits, see @ref DEF_INPUT_APPLI_MK
  unsigned char                            TermIdent_9F1C[8];                         ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_9F1C_0 @n  TLV tag #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_MK_9F1C_TERM_IDENT
  unsigned char                  TerminalCountryCode_9F1A[2];                         ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_MK_9F1A_0 @n  TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_MK_9F1A_TERM_COUNTRY_CODE
  unsigned char                         TerminalType_9F35;                            ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_MK_9F35_0  @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_MK_9F35_TERM_TYPE
  unsigned char       AdditionalTerminalCapabilities_9F40[5];                         ///< TempUpdate allowed: YES @n Description: "Additional Terminal Capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_MK_9F40_0   @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_MK_9F40_ADD_TERM_CAPS
  unsigned char              MagstripeCVM_aboveLimit_DF811E;                          ///< TempUpdate allowed: YES @n Description: Separate cardholder verification methods for CTLS magstripe transactions for PayPass3 above the CVM limit (former Byte 2 of 9F33) @n PayPass3 tag: @c DF811E @n For values see PayPass spec, examples: 0x10=Signature, 0x20=OnlinePIN @n Availability bit: #INPUT_CTLS_APL_MK_DF811E_0 @n TLV tag: #TAG_DF811E_MSR_CVM_ABOVE_LIMIT, @n XML Tag: #XML_TAG_AD_MK_DF811E_MSR_CVM_ABOVE_LIMIT
  unsigned char              MagstripeCVM_belowLimit_DF812C;                          ///< TempUpdate allowed: YES @n Description: Separate cardholder verification methods for CTLS magstripe transactions for PayPass3 below the CVM limit (former Byte 2 of 9F33) @n PayPass3 tag: @c DF812C @n For values see PayPass spec, examples: 0x10=Signature, 0x20=OnlinePIN @n Availability bit: #INPUT_CTLS_APL_MK_DF812C_0 @n TLV tag: #TAG_DF812C_MSR_CVM_BELOW_LIMIT, @n XML Tag: #XML_TAG_AD_MK_DF812C_MSR_CVM_BELOW_LIMIT
  unsigned char                   ChipCVM_aboveLimit_DF8118;                          ///< TempUpdate allowed: YES @n Description: Separate cardholder verification methods for CTLS chip transactions for PayPass3 above the CVM limit (former Byte 2 of 9F33) @n Availability bit: #INPUT_CTLS_APL_MK_DF8118_0 @n TLV tag: #TAG_DF8118_CVM_CAPABILITY__CVM_REQUIRED, @n XML Tag: #XML_TAG_AD_MK_DF8118_CHP_CVM_ABOVE_LIMIT
  unsigned char                   ChipCVM_belowLimit_DF8119;                          ///< TempUpdate allowed: YES @n Description: Separate cardholder verification methods for CTLS chip transactions for PayPass3 below the CVM limit (former Byte 2 of 9F33) @n Availability bit: #INPUT_CTLS_APL_MK_DF8119_0 @n TLV tag: #TAG_DF8119_CVM_CAPABILITY__NO_CVM_REQUIRED, @n XML Tag: #XML_TAG_APPLIDATA_CHIP_CVM_BELOW
  unsigned char                   SecurityCapability_DF811F;                          ///< TempUpdate allowed: YES @n Description: Indicates the security capability of the Kernel (former Byte 3 of 9F33) @n Availability bit: #INPUT_CTLS_APL_MK_DF811F_1 @n TLV tag: #TAG_DF811F_SECURITY_CAPABILITY @n XML Tag: #XML_TAG_AD_MK_DF811F_SECURITY_CAP
  unsigned char              CardDataInputCapability_DF8117;                          ///< TempUpdate allowed: NO  @n Description: Indicates the card data input capability of the Terminal and Reader (former Byte 1 of 9F33) @n Availability bit: #INPUT_CTLS_APL_MK_DF8117_1 @n TLV tag: #TAG_DF8117_CARD_DATA_INPUT_CAPABILITY @n XML Tag: #XML_TAG_AD_MK_DF8117_CARD_DATA_INPUT_CAP
  unsigned char                           FloorLimit_DF8123[6];                       ///< TempUpdate allowed: YES @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_MK_DF8123_1  @n TLV tag: #TAG_DF8123_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_MK_DF8123_FLOOR_LIMIT
  unsigned char           TransactionLimitNoOnDevice_DF8124[6];                       ///< TempUpdate allowed: YES @n Description: Reader Contactless Transaction Limit (No On-device CVM) (n12) @n Availability bit: #INPUT_CTLS_APL_MK_DF8124_1 @n  TLV tag #TAG_DF8124_READER_CTLS_TRX_LIMIT__NO_ON_DEV_CVM, @n XML Tag: #XML_TAG_AD_MK_DF8124_TRX_LIMIT_NO_ON_DEVICE
  unsigned char             TransactionLimitOnDevice_DF8125[6];                       ///< TempUpdate allowed: YES @n Description: Reader Contactless Transaction Limit (On-device CVM) (n12) @n Availability bit: #INPUT_CTLS_APL_MK_DF8125_1 @n  TLV tag #TAG_DF8125_READER_CTLS_TRX_LIMIT__ON_DEVICE_CVM, @n XML Tag: #XML_TAG_AD_MK_DF8125_TRX_LIMIT_ON_DEVICE
  unsigned char                     CVMRequiredLimit_DF8126[6];                       ///< TempUpdate allowed: YES @n Description: Reader CVM Required Limit (n12) @n Availability bit: #INPUT_CTLS_APL_MK_DF8126_1 @n TLV tag: #INPUT_CTLS_APL_MK_DF8126_1, @n XML Tag: #XML_TAG_AD_MK_DF8126_CVM_REQUIRED_LIMIT
  unsigned char                    ChipVersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];   ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_9F09_1 @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_MK_9F09_CHP_VERSION_NUMBER
  unsigned char                     MSRVersionNumber_9F6D[2*EMV_CTLS_MAX_APP_VERS];   ///< TempUpdate allowed: NO  @n Description: Application msr version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_9F6D_1 @n TLV tag: #TAG_9F6D_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_MK_9F6D_MSR_VERSION_NUMBER
  unsigned char                  KernelConfiguration_DF811B;                          ///< TempUpdate allowed: NO  @n Description: Indicates the Kernel configuration options. @n Availability bit: #INPUT_CTLS_APL_MK_DF811B_2 @n TLV tag: #TAG_DF811B_KERNEL_CONFIGURATION @n XML Tag: #XML_TAG_AD_MK_DF811B_KERNEL_CONFIG
  unsigned char              TransactionCategoryCode_9F53;                            ///< TempUpdate allowed: YES @n Description: Transaction category code:  @n Availability bit: #INPUT_CTLS_APL_MK_9F53_2  @n  TLV tag #TAG_9F53_TRANS_CATEGORY_CODE, @n XML Tag: #XML_TAG_AD_MK_9F53_TRX_CATEGORY_CODE
  unsigned char                           TACDefault_DF8120[5];                       ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_DF8120_2 @n TLV tag: #TAG_DF8120_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_MK_DF8120_TAC_DEFAULT
  unsigned char                            TACDenial_DF8121[5];                       ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_DF8121_2  @n TLV tag: #TAG_DF8121_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_MK_DF8121_TAC_DENIAL
  unsigned char                            TACOnline_DF8122[5];                       ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_DF8122_2  @n TLV tag: #TAG_DF8122_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_MK_DF8122_TAC_ONLINE
  unsigned char                             KernelID_DF810C;                          ///< TempUpdate allowed: NO  @n Description: Contains a value that uniquely identifies each Kernel. There is one occurrence of this data object for each Kernel in the Reader. @n Availability bit: #INPUT_CTLS_APL_MK_DF810C_2 @n TLV tag: #TAG_DF810C_KERNEL_ID @n XML Tag: #XML_TAG_AD_MK_DF810C_KERNEL_ID
  unsigned char           TerminalRiskManagementData_9F1D[8];                         ///< TempUpdate allowed: YES @n Description: Application-specific value used by the cardholder device for risk management purposes. If not set explicitly, then derived internally as good as possible. @n Availability bit: #INPUT_CTLS_APL_MK_9F1D_2 @n TLV tag: #TAG_9F1D_TRM_RISK_MNGT_DATA @n XML Tag: #XML_TAG_AD_MK_9F1D_TRM_RISK_MGMT_DATA
  unsigned char                 MerchantCategoryCode_9F15[2];                         ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_9F15_2  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_MK_9F15_MERCHANT_CATEGORY_CODE
           char                   MerchantIdentifier_9F16[15+1];                      ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MK_9F16_3   @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_MK_9F16_MERCHANT_ID
           char              MerchantNameAndLocation_9F4E[40+1];                      ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_MK_9F4E_3 @n  TLV tag #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_MK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                   AcquirerIdentifier_9F01[6];                         ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_MK_9F01_3 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_MK_9F01_ACQUIRER_ID
  unsigned char                        HoldTimeValue_DF8130;                          ///< TempUpdate allowed: NO  @n Description: Indicates the time that the field is to be turned off after the transaction is completed if requested to do so by the cardholder device. @n Availability bit: #INPUT_CTLS_APL_MK_DF8130_3  @n TLV tag: #TAG_DF8130_HOLD_TIME_VALUE @n XML Tag: #XML_TAG_AD_MK_DF8130_HOLD_TIME_VALUE
  unsigned char                      MessageHoldTime_DF812D[3];                       ///< TempUpdate allowed: NO  @n Description: Indicates the default delay for the processing of the next MSG Signal. The Message Hold Time is an integer in units of 100ms. @n Availability bit: #INPUT_CTLS_APL_MK_DF812D_3 @n TLV tag: #TAG_DF812D_MESSAGE_HOLD_TIME @n XML Tag: #XML_TAG_AD_MK_DF812D_MESSAGE_HOLD_TIME
  unsigned char              TornTransactionLifetime_DF811C[2];                       ///< TempUpdate allowed: NO  @n Description: Lifetime for torn transaction, 0 means no torn txns are stored @n Availability bit: #INPUT_CTLS_APL_MK_DF811C_3  @n TLV tag: #TAG_DF811C_TRN_TXN_LIFETIME, @n XML Tag: #XML_TAG_AD_MK_DF811C_TORN_TRX_LIFETIME
  unsigned char                TornTransactionNumber_DF811D;                          ///< TempUpdate allowed: NO  @n Description: Number of torn transaction to stored. Please be aware that this value should be consistent over the AIDs because otherwise the size of the torn txn store will be changed accordingly each time this AID is used for a transaction @n Availability bit: #INPUT_CTLS_APL_MK_DF811D_3  @n  TLV tag #TAG_DF811D_TRN_TXN_NUMBER, @n XML Tag: #XML_TAG_AD_MK_DF811D_TORN_TRX_NUMBER
  EMV_CTLS_DATA_TYPE               PhoneMessageTable_DF8131;                          ///< TempUpdate allowed: NO  @n Description: Phone message table, up to 10 entries @n Availability bit: #INPUT_CTLS_APL_MK_DF8131_3  @n  TLV tag #TAG_DF8131_PHONE_MSG_TABLE, @n XML Tag: #XML_TAG_AD_MK_DF8131_PHONE_MSG_TABLE
  EMV_CTLS_DATA_TYPE                      TagsToRead_DF8112;                          ///< TempUpdate allowed: NO  @n Description: PayPass Data Exchange : Tags To Read. List of tags indicating the data the Terminal has requested to be read @n Availability bit: #INPUT_CTLS_APL_MK_DF8112_4  @n  TLV tag #TAG_DF8112_TAGS_TO_READ, @n XML Tag: #XML_TAG_AD_MK_DF8112_TAGS_TO_READ
  EMV_CTLS_DATA_TYPE          TagsToWriteBeforeGenAC_FF8102;                          ///< TempUpdate allowed: NO  @n Description: PayPass Data Exchange : Tags To Write Before Gen AC @n Availability bit: #INPUT_CTLS_APL_MK_FF8102_4  @n  TLV tag #TAG_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC, @n XML Tag: #XML_TAG_AD_MK_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC
  EMV_CTLS_DATA_TYPE           TagsToWriteAfterGenAC_FF8103;                          ///< TempUpdate allowed: NO  @n Description: PayPass Data Exchange : Tags To Write After Gen AC @n Availability bit: #INPUT_CTLS_APL_MK_FF8103_4  @n  TLV tag #TAG_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC, @n XML Tag: #XML_TAG_AD_MK_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC
  unsigned char              ProceedToFirstWriteFlag_DF8110;                          ///< TempUpdate allowed: NO  @n Description: PayPass Data Exchange : Proceed To First Write Flag @n Availability bit: #INPUT_CTLS_APL_MK_DF8110_4  @n  TLV tag #TAG_DF8110_PROCEED_TO_FIRST_WRITE_FLAG, @n XML Tag: #XML_TAG_AD_MK_DF8110_PROCEED_TO_FIRST_WRITE_FLAG
  unsigned char                DSRequestedOperatorID_9F5C[8];                         ///< TempUpdate allowed: NO  @n Description: PayPass Data Exchange : Contains the Terminal determined operator identifier for data storage @n Availability bit: #INPUT_CTLS_APL_MK_9F5C_4  @n  TLV tag #TAG_9F5C_DS_REQUESTED_OPERATOR_ID, @n XML Tag: #XML_TAG_AD_MK_9F5C_DS_REQUESTED_OPERATOR_ID
  unsigned char                       DETimeoutValue_DF8127[2];                       ///< TempUpdate allowed: NO  @n Description: PayPass Data Exchange : Defines the time in ms before the timer generates a TIMEOUT Signal @n Availability bit: #INPUT_CTLS_APL_MK_DF8127_4  @n  TLV tag #TAG_DF8127_DE_TIMEOUT_VALUE, @n XML Tag: #XML_TAG_AD_MK_DF8127_DE_TIMEOUT_VALUE
  unsigned char                           AppFlowCap_DFAB31[5];                       ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_MK @n Availability bit: #INPUT_CTLS_APL_MK_DFAB31_4  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_MK_DFAB31_APP_FLOW_CAP
  unsigned char                    RR_MinGracePeriod_DF8132[2];                       ///< TempUpdate allowed: YES @n Description: Minimum Relay Resistance Grace Period [1/10 ms] @n TLV tag #TAG_DF8132_RR_MIN_GRACE_PERIOD @n Availability bit: #INPUT_CTLS_APL_MK_DF8132_5, @n XML Tag: #XML_TAG_AD_MK_DF8132_RR_MIN_GRACE_PERIOD
  unsigned char                    RR_MaxGracePeriod_DF8133[2];                       ///< TempUpdate allowed: YES @n Description: Maximum Relay Resistance Grace Period [1/10 ms] @n TLV tag #TAG_DF8133_RR_MAX_GRACE_PERIOD @n Availability bit: #INPUT_CTLS_APL_MK_DF8133_5, @n XML Tag: #XML_TAG_AD_MK_DF8133_RR_MAX_GRACE_PREIOD
  unsigned char           RR_ExpectedTransTime_CAPDU_DF8134[2];                       ///< TempUpdate allowed: YES @n Description: Terminal Expected Transmission Time For Relay Resistance C-APDU [1/10 ms] @n TLV tag #TAG_DF8134_RR_TERM_EXPECTED_TRANS_TIME_CAPDU @n Availability bit: #INPUT_CTLS_APL_MK_DF8134_5, @n XML Tag: #XML_TAG_AD_MK_DF8134_RR_EXP_TRANS_TIME_CAPDU
  unsigned char           RR_ExpectedTransTime_RAPDU_DF8135[2];                       ///< TempUpdate allowed: YES @n Description: Terminal Expected Transmission Time For Relay Resistance R-APDU [1/10 ms] @n TLV tag #TAG_DF8135_RR_TERM_EXPECTED_TRANS_TIME_RAPDU @n Availability bit: #INPUT_CTLS_APL_MK_DF8135_5, @n XML Tag: #XML_TAG_AD_MK_DF8135_RR_EXP_TRANS_TIME_RAPDU
  unsigned char                 RR_AccuracyThreshold_DF8136[2];                       ///< TempUpdate allowed: YES @n Description: Relay Resistance Accuracy Threshold [1/10 ms] @n TLV tag #TAG_DF8136_RR_ACCURACY_THRESHOLD @n Availability bit: #INPUT_CTLS_APL_MK_DF8136_5, @n XML Tag: #XML_TAG_AD_MK_DF8136_RR_ACCURACY_THRESHOLD
  unsigned char        RR_TransTimeMismatchThreshold_DF8137;                          ///< TempUpdate allowed: YES @n Description: Relay Resistance Transmission Time Mismatch Threshold [%] @n TLV tag #TAG_DF8137_RR_TRANS_TIME_MISMATCH_THRESHOLD @n Availability bit: #INPUT_CTLS_APL_MK_DF8137_5, @n XML Tag: #XML_TAG_AD_MK_DF8137_RR_TT_MISMATCH_THRESHOLD
  unsigned char                   MerchantCustomData_9F7C[20];					      ///< TempUpdate allowed: YES @n Description: Proprietary merchant data that may be requested by the Card @n TLV tag #TAG_9F7C_MERCHANT_CUSTOM_DATA @n Availability bit: #INPUT_CTLS_APL_MK_9F7C_5, @n XML Tag: #XML_TAG_AD_MK_9F7C_MERCHANT_CUSTOM_DATA
} EMV_CTLS_APPLIDATA_MK_TYPE;

#define EMV_CTLS_APPLIDATA_GC_TYPE EMV_CTLS_APPLIDATA_MK_TYPE // Kernel 2a configuration is same as PayPass for instance

/// @defgroup DEF_INPUT_APPLI_MK Appli data scheme specific - MasterCard - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_MK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_MK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_MK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_MK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_MK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_MK_9F40_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_MK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_MK_DF811E_0   0x10  ///< B1b5: EMV_CTLS_APPLIDATA_MK_STRUCT::MagstripeCVM_aboveLimit_DF811E
#define  INPUT_CTLS_APL_MK_DF812C_0   0x20  ///< B1b6: EMV_CTLS_APPLIDATA_MK_STRUCT::MagstripeCVM_belowLimit_DF812C
#define  INPUT_CTLS_APL_MK_DF8118_0   0x40  ///< B1b7: EMV_CTLS_APPLIDATA_MK_STRUCT::ChipCVM_aboveLimit_DF8118
#define  INPUT_CTLS_APL_MK_DF8119_0   0x80  ///< B1b8: EMV_CTLS_APPLIDATA_MK_STRUCT::ChipCVM_belowLimit_DF8119
  /* Byte 2 */
#define  INPUT_CTLS_APL_MK_DF811F_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_MK_STRUCT::SecurityCapability_DF811F
#define  INPUT_CTLS_APL_MK_DF8117_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_MK_STRUCT::CardDataInputCapability_DF8117
#define  INPUT_CTLS_APL_MK_DF8123_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_MK_STRUCT::FloorLimit_DF8123
#define  INPUT_CTLS_APL_MK_DF8124_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionLimitNoOnDevice_DF8124
#define  INPUT_CTLS_APL_MK_DF8125_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionLimitOnDevice_DF8125
#define  INPUT_CTLS_APL_MK_DF8126_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_MK_STRUCT::CVMRequiredLimit_DF8126
#define  INPUT_CTLS_APL_MK_9F09_1     0x40  ///< B2b7: EMV_CTLS_APPLIDATA_MK_STRUCT::ChipVersionNumber_9F09
#define  INPUT_CTLS_APL_MK_9F6D_1     0x80  ///< B2b8: EMV_CTLS_APPLIDATA_MK_STRUCT::MSRVersionNumber_9F6D
  /* Byte 3 */
#define  INPUT_CTLS_APL_MK_DF811B_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_MK_STRUCT::KernelConfiguration_DF811B
#define  INPUT_CTLS_APL_MK_9F53_2     0x02  ///< B3b2: EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionCategoryCode_9F53
#define  INPUT_CTLS_APL_MK_DF8120_2   0x04  ///< B3b3: EMV_CTLS_APPLIDATA_MK_STRUCT::TACDefault_DF8120
#define  INPUT_CTLS_APL_MK_DF8121_2   0x08  ///< B3b4: EMV_CTLS_APPLIDATA_MK_STRUCT::TACDenial_DF8121
#define  INPUT_CTLS_APL_MK_DF8122_2   0x10  ///< B3b5: EMV_CTLS_APPLIDATA_MK_STRUCT::TACOnline_DF8122
#define  INPUT_CTLS_APL_MK_DF810C_2   0x20  ///< B3b6: EMV_CTLS_APPLIDATA_MK_STRUCT::KernelID_DF810C
#define  INPUT_CTLS_APL_MK_9F1D_2     0x40  ///< B3b7: EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalRiskManagementData_9F1D
#define  INPUT_CTLS_APL_MK_9F15_2     0x80  ///< B3b8: EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantCategoryCode_9F15
  /* Byte 4 */
#define  INPUT_CTLS_APL_MK_9F16_3     0x01  ///< B4b1: EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantIdentifier_9F16
#define  INPUT_CTLS_APL_MK_9F4E_3     0x02  ///< B4b2: EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_MK_9F01_3     0x04  ///< B4b3: EMV_CTLS_APPLIDATA_MK_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_MK_DF8130_3   0x08  ///< B4b4: EMV_CTLS_APPLIDATA_MK_STRUCT::HoldTimeValue_DF8130
#define  INPUT_CTLS_APL_MK_DF812D_3   0x10  ///< B4b5: EMV_CTLS_APPLIDATA_MK_STRUCT::MessageHoldTime_DF812D
#define  INPUT_CTLS_APL_MK_DF811C_3   0x20  ///< B4b6: EMV_CTLS_APPLIDATA_MK_STRUCT::TornTransactionLifetime_DF811C
#define  INPUT_CTLS_APL_MK_DF811D_3   0x40  ///< B4b7: EMV_CTLS_APPLIDATA_MK_STRUCT::TornTransactionNumber_DF811D
#define  INPUT_CTLS_APL_MK_DF8131_3   0x80  ///< B4b8: EMV_CTLS_APPLIDATA_MK_STRUCT::PhoneMessageTable_DF8131
  /* Byte 5 */
#define  INPUT_CTLS_APL_MK_DFAB31_4   0x01  ///< B5b1: EMV_CTLS_APPLIDATA_MK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_MK_DF8112_4   0x02  ///< B5b2: EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToRead_DF8112
#define  INPUT_CTLS_APL_MK_FF8102_4   0x04  ///< B5b3: EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToWriteBeforeGenAC_FF8102
#define  INPUT_CTLS_APL_MK_FF8103_4   0x08  ///< B5b4: EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToWriteAfterGenAC_FF8103
#define  INPUT_CTLS_APL_MK_DF8110_4   0x10  ///< B5b5: EMV_CTLS_APPLIDATA_MK_STRUCT::ProceedToFirstWriteFlag_DF8110
#define  INPUT_CTLS_APL_MK_9F5C_4     0x20  ///< B5b6: EMV_CTLS_APPLIDATA_MK_STRUCT::DSRequestedOperatorID_9F5C
#define  INPUT_CTLS_APL_MK_DF8127_4   0x40  ///< B5b7: EMV_CTLS_APPLIDATA_MK_STRUCT::DETimeoutValue_DF8127

// Byte 6
#define INPUT_CTLS_APL_MK_DF8132_5    0x01  ///< B5b1: EMV_CTLS_APPLIDATA_MK_STRUCT::RR_MinGracePeriod_DF8132
#define INPUT_CTLS_APL_MK_DF8133_5    0x02  ///< B5b2: EMV_CTLS_APPLIDATA_MK_STRUCT::RR_MaxGracePeriod_DF8133
#define INPUT_CTLS_APL_MK_DF8134_5    0x04  ///< B5b3: EMV_CTLS_APPLIDATA_MK_STRUCT::RR_ExpectedTransTime_CAPDU_DF8134
#define INPUT_CTLS_APL_MK_DF8135_5    0x08  ///< B5b4: EMV_CTLS_APPLIDATA_MK_STRUCT::RR_ExpectedTransTime_RAPDU_DF8135
#define INPUT_CTLS_APL_MK_DF8136_5    0x10  ///< B5b5: EMV_CTLS_APPLIDATA_MK_STRUCT::RR_AccuracyThreshold_DF8136
#define INPUT_CTLS_APL_MK_DF8137_5    0x20  ///< B5b6: EMV_CTLS_APPLIDATA_MK_STRUCT::RR_TransTimeMismatchThreshold_DF8137
#define INPUT_CTLS_APL_MK_9F7C_5      0x40  ///< B5b7: EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantCustomData_9F7C

/// @}

/// @defgroup DEF_INPUT_APPLI_MK_PROCEED_FLAG PayPass ProceedToFirstWriteFlag_DF8110 values
/// @ingroup DEF_CONF_APPLI
/// @brief Well-known values for PayPass flag ProceedToFirstWriteFlag('DF1810') in EMV_CTLS_APPLIDATA_MK_STRUCT::Scheme.MK.ProceedToFirstWriteFlag_DF8110
/// @{
#define INPUT_CTLS_MK_PROCEED_TO_FIRST_WRITE_NO     0x00 ///< Kernel shall wait for DET signal, specification says that no DEK shall be sent
#define INPUT_CTLS_MK_PROCEED_TO_FIRST_WRITE_YES    0x01 ///< Kernel shall proceed without sending DEK signal
#define INPUT_CTLS_MK_PROCEED_TO_FIRST_WRITE_EMPTY  0xFF ///< Kernel shall send DEK signal before first write
#define INPUT_CTLS_MK_PROCEED_TO_FIRST_WRITE_ABSENT 0xFE ///< Any other value makes the flag absent. Kernel shall act like #INPUT_CTLS_MK_PROCEED_TO_FIRST_WRITE_YES.
/// @}

//////////
// Visa //
//////////

/// @ingroup DEF_CONF_APPLI
/// @brief Visa Dynamic Reader Limits, single entry
/// Element of @ref EMV_CTLS_VK_DRL_STRUCT
typedef struct EMV_CTLS_VK_DRL_ENTRY_STRUCT {
  unsigned char  AppProgramId_9F5A[16];                    ///< DRL Application programm ID @n TLV tag #TAG_9F5A_APP_PROGRAM_ID @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_APP_PRG_ID
  unsigned char  AppProgramIdLen;                          ///< DRL Application programm ID length @n TLV tag #TAG_9F5A_APP_PROGRAM_ID @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_PRG_ID_LEN
  unsigned char  ContactlessFloorLimit_DFAB40[6];          ///< Dynamic Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable @n TLV tag #TAG_DFAB40_CTLS_FLOOR_LIMIT @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT
  unsigned char  ContactlessTransactionLimit_DFAB41[6];    ///< Dynamic Reader Contactless Transaction Limit (n12) @n Use '999999999999' to disable @n TLV tag #TAG_DFAB41_CTLS_TRX_LIMIT @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT
  unsigned char  ContactlessCVMRequiredLimit_DFAB42[6];    ///< Dynamic Reader CVM Required Limit (n12) @n Use '999999999999' to disable @n TLV tag #TAG_DFAB42_CTLS_CVM_REQ_LIMIT @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT
  unsigned char  OnOffSwitch_DFAB49;                       ///< DRL switch on / off DRL features see @ref DRL_FEATURE_SWITCHS @n TLV tag #TAG_DFAB49_DRL_SWITCHES @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_SWITCH
} EMV_CTLS_VK_DRL_ENTRY_TYPE;

/// @ingroup DEF_CONF_APPLI
/// @brief Visa Dynamic Reader Limits
/// Used in @ref EMV_CTLS_APPLIDATA_VK_STRUCT::VisaDRLParams_FFAB01
typedef struct EMV_CTLS_VK_DRL_STRUCT {
  unsigned short  cnt;              ///< number of DRL entries
  EMV_CTLS_VK_DRL_ENTRY_TYPE *data; ///< data
} EMV_CTLS_VK_DRL_TYPE;

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, VK specific part. See see Book C-3, Kernel 3 Specification at <a href="http://www.emvco.com/">[EMVCo Homepage]</a>.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_VK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_VK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_VK_9F1C_0     @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_VK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal Country Code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_VK_9F1A_0    @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_VK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_VK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_VK_9F35_TERM_TYPE
  unsigned char             TerminalTransactionQualifier_9F66[4];                        ///< TempUpdate allowed: YES @n Description: Visa-defined Terminal Transaction Qualifiers @n Availability bit: #INPUT_CTLS_APL_VK_9F66_0   @n TLV tag: #TAG_9F66_TTQ, @n XML Tag: #XML_TAG_AD_VK_9F66_TERM_TRX_QUALIFIER
  unsigned char                     TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "Terminal Capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_VK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_VK_9F33_TERM_CAPS
  unsigned char           AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "Additional Terminal Capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_VK_9F40_0   @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_VK_9F40_ADD_TERM_CAPS
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_VK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_VK_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_VK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_VK_9F15_MERCHANT_CATEGORY_CODE
           char                       MerchantIdentifier_9F16[15+1];                     ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_VK_9F16_1   @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_VK_9F16_MERCHANT_ID
           char                  MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_VK_9F4E_1   @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_VK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                               TecSupport_DFAB30;                         ///< TempUpdate allowed: NO  @n Description: Specifies supported technologies, for values see @ref DEF_TEC. @n Availability bit: #INPUT_CTLS_APL_VK_DFAB30_1 @n TLV tag: #TAG_DFAB30_TEC_SUPPORT @n XML Tag: #XML_TAG_AD_VK_DFAB30_TEC_SUPPORT
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_VK @n Availability bit: #INPUT_CTLS_APL_VK_DFAB31_1   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_VK_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable @n Availability bit: #INPUT_CTLS_APL_VK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_VK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '999999999999' to disable @n Availability bit: #INPUT_CTLS_APL_VK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_VK_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '999999999999' to disable @n Availability bit: #INPUT_CTLS_APL_VK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_VK_DFAB42_CTLS_CVM_REQ_LIMIT
  EMV_CTLS_VK_DRL_TYPE                     VisaDRLParams_FFAB01;                         ///< TempUpdate allowed: NO  @n Description: Dynamic Reader Limits table @n Availability bit: #INPUT_CTLS_APL_VK_FFAB01_1 @n TLV tag: #TAG_FFAB01_DRL_PARAMETER, @n XML Tag: #XML_TAG_AD_VK_FFAB01_VISA_DRL_PARAMS
} EMV_CTLS_APPLIDATA_VK_TYPE;

/// @defgroup DEF_INPUT_APPLI_VK Appli data scheme specific - Visa - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_VK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_VK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_VK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_VK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_VK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_VK_9F66_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalTransactionQualifier_9F66
#define  INPUT_CTLS_APL_VK_9F33_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_VK_9F40_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_VK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_VK_9F09_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_VK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_VK_9F15_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantCategoryCode_9F15
  /* Byte 2 */
#define  INPUT_CTLS_APL_VK_9F16_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantIdentifier_9F16
#define  INPUT_CTLS_APL_VK_9F4E_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_VK_DFAB30_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_VK_STRUCT::TecSupport_DFAB30
#define  INPUT_CTLS_APL_VK_DFAB31_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_VK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_VK_DFAB40_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_VK_DFAB41_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_VK_DFAB42_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_VK_FFAB01_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_VK_STRUCT::VisaDRLParams_FFAB01
/// @}


//////////////////////
// American Express //
//////////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Amex Dynamic Reader Limits, single entry
/// Element of @ref EMV_CTLS_AK_DRL_STRUCT
typedef struct EMV_CTLS_AK_DRL_ENTRY_STRUCT {
  unsigned char  ContactlessFloorLimit_DFAB40[6];          ///< Dynamic Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable. @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML tag: #XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT
  unsigned char  ContactlessTransactionLimit_DFAB41[6];    ///< Dynamic Reader Contactless Transaction Limit (n12) @n Use '999999999999' to disable. @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT @n XML tag: #XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT
  unsigned char  ContactlessCVMRequiredLimit_DFAB42[6];    ///< Dynamic Reader Contactless CVM Required Limit (n12) @n Use '999999999999' to disable. @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT @n XML tag: #XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT
  unsigned char  OnOffSwitch_DFAB49;                       ///< DRL switch on / off other DRL features (see @ref DRL_FEATURE_SWITCHS) plus DRL id ([0..15]). The first DRL entry will be the default DRL and its DRL id should be 0. If missing a default DRL is generated from the application's limits @n TLV tag: #TAG_DFAB49_DRL_SWITCHES @n XML tag: #XML_TAG_APPLIDATA_VISA_DRL_SWITCH
} EMV_CTLS_AK_DRL_ENTRY_TYPE;

/// @ingroup DEF_CONF_APPLI
/// @brief Amex Dynamic Reader Limits
/// Used in @ref EMV_CTLS_APPLIDATA_AK_STRUCT::AmexDRLParams_FFAB01
typedef struct EMV_CTLS_AK_DRL_STRUCT {
  unsigned short  cnt;              ///< number of DRL entries
  EMV_CTLS_AK_DRL_ENTRY_TYPE *data; ///< data
} EMV_CTLS_AK_DRL_TYPE;

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, AK specific part. See see Book C-4, Kernel 4 Specification at <a href="http://www.emvco.com/">[EMVCo Homepage]</a>.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_AK_STRUCT {
  unsigned char                                  IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_AK
  unsigned char                                     TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_9F1C_0  @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_AK_9F1C_TERM_IDENT
  unsigned char                           TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_AK_9F1A_0  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_AK_9F1A_TERM_COUNTRY_CODE
  unsigned char                                  TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_AK_9F35_0  @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_AK_9F35_TERM_TYPE
  unsigned char             AmexContactlessReaderCapabilities_9F6D;                           ///< TempUpdate allowed: YES @n Description: Special terminal capabilities of Expresspay, Amex tag 9F6D, indicating the version of Amex spec used for CTLS  @n Availability bit: #INPUT_CTLS_APL_AK_9F6D_0  @n TLV tag: #TAG_9F6D_AMEX_CAPABILITIES, @n XML Tag: #XML_TAG_AD_AK_9F6D_AMEX_CTLS_READER_CAPS
  unsigned char           AmexTerminalTransactionCapabilities_9F6E[4];                        ///< TempUpdate allowed: YES @n Description: Special enhanced reader capabilities of Expresspay, Amex tag 9F6E. CV is performed both above and below the floor limit but there are different procedures for the kernel to follow in each case. The kernel no longer uses byte 2 of tag 9F33 to determine supported CV methods because byte 2 of tag 9F6E is now used instead. However only 1 value is required for tag 9F6E because the kernel itself modifies this bitmap depending if the CV limit is exceeded or not. Only 1 value for tag 9F33 is required because the kernel still uses byte 3 of this tag to determine supported DA methods @n Availability bit: #INPUT_CTLS_APL_AK_9F6E_0  @n TLV tag: #TAG_9F6E_AMEX_ENHANCED_CAPABILITIES, @n XML Tag: #XML_TAG_AD_AK_9F6E_AMEX_TERM_TRX_CAPS
  unsigned char                          TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "Terminal Capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_AK_9F33_0    @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_AK_9F33_TERM_CAPS
  unsigned char                AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "Additional Terminal Capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_AK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_AK_9F40_ADD_TERM_CAPS
  unsigned char                                 VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_9F09_0    @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_AK_9F09_VERSION_NUMBER
  unsigned char                          MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_9F15_1    @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_AK_9F15_MERCHANT_CATEGORY_CODE
           char                            MerchantIdentifier_9F16[15+1];                     ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_9F16_1    @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_AK_9F16_MERCHANT_ID
           char                       MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_AK_9F4E_1   @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_AK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                            AcquirerIdentifier_9F01[6];                        ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_AK_9F01_1 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_AK_9F01_ACQUIRER_ID
  unsigned char                                    TecSupport_DFAB30;                         ///< TempUpdate allowed: NO  @n Description: Specifies supported technologies. @n See @ref DEF_TEC @n Availability bit: #INPUT_CTLS_APL_AK_DFAB30_1 @n TLV tag: #TAG_DFAB30_TEC_SUPPORT @n XML Tag: #XML_TAG_AD_AK_DFAB30_TEC_SUPPORT
  unsigned char                                    AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_AK @n Availability bit: #INPUT_CTLS_APL_AK_DFAB31_1   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_AK_DFAB31_APP_FLOW_CAP
  unsigned char                         ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_AK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_AK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char                   ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '999999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_AK_DFAB41_1    @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_AK_DFAB41_CTLS_TRX_LIMIT
  unsigned char                   ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '999999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_AK_DFAB42_2   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_AK_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                                    TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_DFAB43_2   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_AK_DFAB43_TAC_DEFAULT
  unsigned char                                     TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_DFAB44_2   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_AK_DFAB44_TAC_DENIAL
  unsigned char                                     TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_AK_DFAB45_2   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_AK_DFAB45_TAC_ONLINE
  EMV_CTLS_AK_DRL_TYPE                          AmexDRLParams_FFAB01;                         ///< TempUpdate allowed: NO  @n Description: Dynamic Reader Limits table @n Availability bit: #INPUT_CTLS_APL_AK_FFAB01_2   @n TLV tag: #TAG_FFAB01_DRL_PARAMETER, @n XML Tag: #XML_TAG_AD_AK_FFAB01_AMEX_DRL_PARAMS
  unsigned char                      UnpredictableNumberRange_DFAB52;                         ///< TempUpdate allowed: YES @n Description: Limit for MSR unpredictable nummer, 60 .. 255, default value 60. Availability bit: #INPUT_CTLS_APL_AK_DFAB52_2  @n TLV tag: #TAG_DFAB52_AMEX_UN_RANGE, @n XML Tag: #XML_TAG_AD_AK_DFAB52_UN_RANGE
} EMV_CTLS_APPLIDATA_AK_TYPE;

/// @defgroup DEF_INPUT_APPLI_AK Appli data scheme specific - Amex - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_AK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_AK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_AK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_AK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_AK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_AK_9F6D_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_AK_STRUCT::AmexContactlessReaderCapabilities_9F6D
#define  INPUT_CTLS_APL_AK_9F6E_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_AK_STRUCT::AmexTerminalTransactionCapabilities_9F6E
#define  INPUT_CTLS_APL_AK_9F33_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_AK_9F40_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_AK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_AK_9F09_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_AK_STRUCT::VersionNumber_9F09
  /* Byte 2 */
#define  INPUT_CTLS_APL_AK_9F15_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_AK_9F16_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantIdentifier_9F16
#define  INPUT_CTLS_APL_AK_9F4E_1     0x04  ///< B2b3: EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_AK_9F01_1     0x08  ///< B2b4: EMV_CTLS_APPLIDATA_AK_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_AK_DFAB30_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_AK_STRUCT::TecSupport_DFAB30
#define  INPUT_CTLS_APL_AK_DFAB31_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_AK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_AK_DFAB40_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_AK_DFAB41_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessTransactionLimit_DFAB41
  /* Byte 3 */
#define  INPUT_CTLS_APL_AK_DFAB42_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_AK_DFAB43_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_AK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_AK_DFAB44_2   0x04  ///< B3b3: EMV_CTLS_APPLIDATA_AK_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_AK_DFAB45_2   0x08  ///< B3b4: EMV_CTLS_APPLIDATA_AK_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_AK_FFAB01_2   0x10  ///< B3b5: EMV_CTLS_APPLIDATA_AK_STRUCT::AmexDRLParams_FFAB01
#define  INPUT_CTLS_APL_AK_DFAB52_2   0x20  ///< B3b6: EMV_CTLS_APPLIDATA_AK_STRUCT::UnpredictableNumberRange_DFAB52
/// @}


//////////////
//// JCB  ////
//////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, JK specific part. See see Book C-5, Kernel 5 Specification at <a href="http://www.emvco.com/">[EMVCo Homepage]</a>.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_JK_STRUCT {
  unsigned char                          IncludedData[8];          ///< Availability bits, see @ref DEF_INPUT_APPLI_JK
  unsigned char                             TermIdent_9F1C[8];     ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_JK_9F1C_0    @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_JK_9F1C_TERM_IDENT
  unsigned char                   TerminalCountryCode_9F1A[2];     ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_JK_9F1A_0    @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_JK_9F1A_TERM_COUNTRY_CODE
  unsigned char                          TerminalType_9F35;        ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_JK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_JK_9F35_TERM_TYPE
  unsigned char            TerminalInterchangeProfile_9F53[3];     ///< TempUpdate allowed: YES @n Description: Flags for Supported CVM, Contact Chip support and Issuer Update support. @n Availability bit: #INPUT_CTLS_APL_JK_9F53_0 @n TLV tag: #TAG_9F53_TRM_INTERCHANGE_PROFILE @n XML Tag: #XML_TAG_AD_JK_9F53_TERM_INTERCHANGE_PROFILE
  unsigned char                  MerchantCategoryCode_9F15[2];     ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_JK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_JK_9F15_MERCHANT_CATEGORY_CODE
           char               MerchantNameAndLocation_9F4E[40+1];  ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_JK_9F4E_0  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_JK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                    AcquirerIdentifier_9F01[6];     ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_JK_9F01_0 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_JK_9F01_ACQUIRER_ID
  unsigned char                            AppFlowCap_DFAB31[5];   ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_JK @n Availability bit: #INPUT_CTLS_APL_JK_DFAB31_0    @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_JK_DFAB31_APP_FLOW_CAP
  unsigned char                 ContactlessFloorLimit_DFAB40[6];   ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '99999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_JK_DFAB40_1    @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_JK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char           ContactlessTransactionLimit_DFAB41[6];   ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '99999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_JK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_JK_DFAB41_CTLS_TRX_LIMIT
  unsigned char           ContactlessCVMRequiredLimit_DFAB42[6];   ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '99999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_JK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_JK_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                            TACDefault_DFAB43[5];   ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_JK_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_JK_DFAB43_TAC_DEFAULT
  unsigned char                             TACDenial_DFAB44[5];   ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_JK_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_JK_DFAB44_TAC_DENIAL
  unsigned char                             TACOnline_DFAB45[5];   ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_JK_DFAB45_1   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_JK_DFAB45_TAC_ONLINE
  unsigned char               RiskManagementThreshold_DFAB46[6];   ///< TempUpdate allowed: YES @n Description: Threshold Value for Biased Random Online Selection during risk management (n12) @n Availability bit: #INPUT_CTLS_APL_JK_DFAB46_1 @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD @n XML Tag: #XML_TAG_AD_JK_DFAB46_RISK_MGMT_THRESHOLD
  unsigned char        RiskManagementTargetPercentage_DFAB47;      ///< TempUpdate allowed: YES @n Description: Target percentage for random online selection during risk management (n2) @n Availability bit: #INPUT_CTLS_APL_JK_DFAB47_1 @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT @n XML Tag: #XML_TAG_AD_JK_DFAB47_RISK_MGMT_TRGT_PERC
  unsigned char RiskManagementMaximumTargetPercentage_DFAB48;      ///< TempUpdate allowed: YES @n Description: Maximum target percentage for random online selection during risk management (n2) @n Availability bit: #INPUT_CTLS_APL_JK_DFAB48_2 @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT @n XML Tag: #XML_TAG_AD_JK_DFAB48_RISK_MGMT_MAX_TRGT_PERC
  unsigned char                    CombinationOptions_DFAB4B[2];   ///< TempUpdate allowed: YES @n Description: Flags for terminal capabilities (only first byte used): @n @c 0x01 = Legacy mode supported @n @c 0x02 = EMV mode supported @n @c 0x08 = Random transaction selection @n @c 0x10 = Exception file check @n @c 0x20 = Offline data authentication supported @n @c 0x40 = Status check supported @n Kernel uses b1+b2 to determine 9F52 "Terminal Compatibility Indicator" @n Availability bit: #INPUT_CTLS_APL_JK_DFAB4B_2 @n TLV tag: #TAG_DFAB4B_COMBINATION_OPTIONS @n XML Tag: #XML_TAG_AD_JK_DFAB4B_COMBINATION_OPTIONS
  unsigned char                        RemovalTimeout_DFAB4C[2];   ///< TempUpdate allowed: YES @n Description: In case of Online Request with "Present and Hold" outcome, this parameter corresponds to the time after which cardholder is asked to remove the card. Value is given in units of 100ms. @n Availability bit: #INPUT_CTLS_APL_JK_DFAB4C_2 @n TLV tag: #TAG_DFAB4C_REMOVAL_TIMEOUT @n XML Tag: #XML_TAG_AD_JK_DFAB4C_REMOVAL_TIMEOUT
} EMV_CTLS_APPLIDATA_JK_TYPE;

/// @defgroup DEF_INPUT_APPLI_JK Appli data scheme specific - JCB - Avaiability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_JK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_JK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_JK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_JK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_JK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_JK_9F53_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalInterchangeProfile_9F53
#define  INPUT_CTLS_APL_JK_9F15_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_JK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_JK_9F4E_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_JK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_JK_9F01_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_JK_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_JK_DFAB31_0   0x80  ///< B1b8: EMV_CTLS_APPLIDATA_JK_STRUCT::AppFlowCap_DFAB31
  /* Byte 2 */
#define  INPUT_CTLS_APL_JK_DFAB40_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_JK_DFAB41_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_JK_DFAB42_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_JK_DFAB43_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_JK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_JK_DFAB44_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_JK_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_JK_DFAB45_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_JK_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_JK_DFAB46_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementThreshold_DFAB46
#define  INPUT_CTLS_APL_JK_DFAB47_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementTargetPercentage_DFAB47
  /* Byte 3 */
#define  INPUT_CTLS_APL_JK_DFAB48_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48
#define  INPUT_CTLS_APL_JK_DFAB4B_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_JK_STRUCT::CombinationOptions_DFAB4B
#define  INPUT_CTLS_APL_JK_DFAB4C_2   0x04  ///< B3b3: EMV_CTLS_APPLIDATA_JK_STRUCT::RemovalTimeout_DFAB4C
/// @}


//////////////
// Discover //
//////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, DK specific part. See see Book C-6, Kernel 6 Specification at <a href="http://www.emvco.com/">[EMVCo Homepage]</a>.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_DK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_DK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_DK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_DK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_DK_9F1A_0    @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_DK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_DK_9F35_0    @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_DK_9F35_TERM_TYPE
  unsigned char             TerminalTransactionQualifier_9F66[4];                        ///< TempUpdate allowed: YES @n Description: Visa-defined Terminal Transaction Qualifiers @n Availability bit: #INPUT_CTLS_APL_DK_9F66_0   @n TLV tag: #TAG_9F66_TTQ, @n XML Tag: #XML_TAG_AD_DK_9F66_TERM_TRX_QUALIFIER
  unsigned char                     TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "terminal capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_DK_9F33_0    @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_DK_9F33_TERM_CAPS
  unsigned char           AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "additional terminal capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_DK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_DK_9F40_ADD_TERM_CAPS
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_DK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_DK_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_DK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_DK_9F15_MERCHANT_CATEGORY_CODE
  unsigned char                               TecSupport_DFAB30;                         ///< TempUpdate allowed: NO  @n Description: Specifies supported technologies. @n See @ref DEF_TEC @n Availability bit: #INPUT_CTLS_APL_DK_DFAB30_1 @n TLV tag: #TAG_DFAB30_TEC_SUPPORT @n XML Tag: #XML_TAG_AD_DK_DFAB30_TEC_SUPPORT @n For Discover this parameter (DFAB30) has a specific meaning in terms of the legacy DISCOVER ZIP: 0x01 = DPAS Chip, 0x02 = DPAS MSR, 0x00 = LEGACY DISCOVER ZIP
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_DK @n Availability bit: #INPUT_CTLS_APL_DK_DFAB31_1   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_DK_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_DK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_DK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_DK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_DK_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_DK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_DK_DFAB42_CTLS_CVM_REQ_LIMIT
  EMV_CTLS_DATA_TYPE                DataContainerReadIds_DFAB58;                         ///< TempUpdate allowed: NO  @n Description: Data Container Read List for DPAS-2 Data Storage feature. This is a list of container IDs each 4 byte. Can be used in combination with #DataContainerReadRanges_DFAB59. @n Availability bit: #INPUT_CTLS_APL_DK_DFAB58_1   @n TLV tag: #TAG_DFAB58_ID_LIST, @n XML Tag: #XML_TAG_AD_DK_DFAB58_CONTAINER_IDS
  EMV_CTLS_DATA_TYPE             DataContainerReadRanges_DFAB59;                         ///< TempUpdate allowed: NO  @n Description: Data Container Read List for DPAS-2 Data Storage feature. This is a list of container ID ranges consisting of each 8 byte, 4 byte start container ID and 4 byte stop container ID. Can be used in combination with #DataContainerReadIds_DFAB58. @n Availability bit: #INPUT_CTLS_APL_DK_DFAB59_1   @n TLV tag: #TAG_DFAB59_RANGE_LIST, @n XML Tag: #XML_TAG_AD_DK_DFAB59_CONTAINER_RANGES
} EMV_CTLS_APPLIDATA_DK_TYPE;
/// @defgroup DEF_INPUT_APPLI_DK Appli data scheme specific - Discover - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_DK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_DK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_DK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_DK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_DK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_DK_9F66_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalTransactionQualifier_9F66
#define  INPUT_CTLS_APL_DK_9F33_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_DK_9F40_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_DK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_DK_9F09_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_DK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_DK_9F15_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_DK_STRUCT::MerchantCategoryCode_9F15
  /* Byte 2 */
#define  INPUT_CTLS_APL_DK_DFAB30_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_DK_STRUCT::TecSupport_DFAB30
#define  INPUT_CTLS_APL_DK_DFAB31_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_DK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_DK_DFAB40_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_DK_DFAB41_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_DK_DFAB42_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_DK_DFAB58_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_DK_STRUCT::DataContainerReadIds_DFAB58
#define  INPUT_CTLS_APL_DK_DFAB59_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_DK_STRUCT::DataContainerReadRanges_DFAB59
/// @}


/////////////
// Interac //
/////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, IK specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_IK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_IK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_IK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_IK_9F1A_0    @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_IK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_IK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_IK_9F35_TERM_TYPE
  unsigned char                     TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "terminal capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_IK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_IK_9F33_TERM_CAPS
  unsigned char           AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "additional terminal capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_IK_9F40_0 @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_IK_9F40_ADD_TERM_CAPS
  unsigned char                    ContactlessFloorLimit_9F5F[6];                        ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_IK_9F5F_0   @n TLV tag: #TAG_9F5F_READER_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_IK_9F5F_CTLS_FLOOR_LIMIT
  unsigned char                    MerchantTypeIndicator_9F58;                           ///< TempUpdate allowed: NO  @n Description: Merchant Type Indicator (MTI) @n Info: VFI reader tag 9F58 @n Availability bit: #INPUT_CTLS_APL_IK_9F58_0   @n TLV tag: #TAG_9F58_MERCH_TYPE_INDICATOR, @n XML Tag: #XML_TAG_AD_IK_9F58_MERCHANT_TYPE_INDICATOR
  unsigned char           TerminalTransactionInformation_9F59[3];                        ///< TempUpdate allowed: NO  @n Description: Terminal Transaction Information (TTI) @n Info: VFI reader tag 9F59 @n Default value: DC 87 00 @n Availability bit: #INPUT_CTLS_APL_IK_9F59_0   @n TLV tag: #TAG_9F59_TERM_TRANS_INFO, @n XML Tag: #XML_TAG_AD_IK_9F59_TERM_TRX_INFORMATION
  unsigned char                  TerminalTransactionType_9F5A;                           ///< TempUpdate allowed: NO  @n Description: Terminal Transaction Type (TTT) @n Info: VFI reader tag 9F5A @n Availability bit: #INPUT_CTLS_APL_IK_9F5A_1   @n TLV tag: #TAG_9F5A_TERM_TRANS_TYPE, @n XML Tag: #XML_TAG_AD_IK_9F5A_TERM_TRX_TYPE
  unsigned char                     TerminalOptionStatus_9F5E[2];                        ///< TempUpdate allowed: YES @n Description: Terminal option status (TOS) @n Info: VFI reader tag: 9F5E @n Availability bit: #INPUT_CTLS_APL_IK_9F5E_1   @n TLV tag: #TAG_9F5E_TERM_OPTION_STATUS, @n XML Tag: #XML_TAG_AD_IK_9F5E_TERM_OPTION_STATUS
  unsigned char                     ReceiptRequiredLimit_9F5D[6];                        ///< TempUpdate allowed: YES @n Description: Interac Terminal CTLS Receipt required limit (numeric) @n Availability bit: #INPUT_CTLS_APL_IK_9F5D_1    @n TLV tag: #TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT, @n XML Tag: #XML_TAG_AD_IK_9F5D_RECEIPT_REQ_LIMIT
  unsigned char                            TryAgainLimit_DF6D;                           ///< TempUpdate allowed: YES @n Description: allowed number of retries for Interac AID after TRY AGAIN result (n1), defaults to 3, Interac retry limit (allowed number of retries after TRY AGAIN). @n Availability bit: #INPUT_CTLS_APL_IK_DF6D_1 @n TLV tag: #TAG_DF6D_TRY_AGAIN_LIMIT @n XML Tag: #XML_TAG_AD_IK_DF6D_TRY_AGAIN_LIMIT
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_9F09_1   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_IK_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_9F15_1   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_IK_9F15_MERCHANT_CATEGORY_CODE
           char                       MerchantIdentifier_9F16[15+1];                     ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_9F16_1   @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_IK_9F16_MERCHANT_ID
           char                  MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_IK_9F4E_1   @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_IK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                       AcquirerIdentifier_9F01[6];                        ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_IK_9F01_2 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_IK_9F01_ACQUIRER_ID
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_IK @n Availability bit: #INPUT_CTLS_APL_IK_DFAB31_2   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_IK_DFAB31_APP_FLOW_CAP
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_IK_DFAB41_2   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_IK_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_IK_DFAB42_2   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_IK_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                               TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_DFAB43_2   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_IK_DFAB43_TAC_DEFAULT
  unsigned char                                TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_DFAB44_2   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_IK_DFAB44_TAC_DENIAL
  unsigned char                                TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_IK_DFAB45_2   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_IK_DFAB45_TAC_ONLINE
  unsigned char                  RiskManagementThreshold_DFAB46[6];                      ///< TempUpdate allowed: YES @n Description: Threshold Value for Biased Random Online Selection during risk management (n12) @n Availability bit: #INPUT_CTLS_APL_IK_DFAB46_2 @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD @n XML Tag: #XML_TAG_AD_IK_DFAB46_RISK_MGMT_THRESHOLD
  unsigned char           RiskManagementTargetPercentage_DFAB47;                         ///< TempUpdate allowed: YES @n Description: Target percentage for random online selection during risk management (n2) @n Availability bit: #INPUT_CTLS_APL_IK_DFAB47_3 @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT @n XML Tag: #XML_TAG_AD_IK_DFAB47_RISK_MGMT_TRGT_PERC
  unsigned char    RiskManagementMaximumTargetPercentage_DFAB48;                         ///< TempUpdate allowed: YES @n Description: Maximum target percentage for random online selection during risk management (n2) @n Availability bit: #INPUT_CTLS_APL_IK_DFAB48_3 @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT @n XML Tag: #XML_TAG_AD_IK_DFAB48_RISK_MGMT_MAX_TRGT_PERC
} EMV_CTLS_APPLIDATA_IK_TYPE;

/// @defgroup DEF_INPUT_APPLI_IK Appli data scheme specific - Interac - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_IK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_IK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_IK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_IK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_IK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_IK_9F33_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_IK_9F40_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_IK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_IK_9F5F_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessFloorLimit_9F5F
#define  INPUT_CTLS_APL_IK_9F58_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantTypeIndicator_9F58
#define  INPUT_CTLS_APL_IK_9F59_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalTransactionInformation_9F59
  /* Byte 2 */
#define  INPUT_CTLS_APL_IK_9F5A_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalTransactionType_9F5A
#define  INPUT_CTLS_APL_IK_9F5E_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalOptionStatus_9F5E
#define  INPUT_CTLS_APL_IK_9F5D_1     0x04  ///< B2b3: EMV_CTLS_APPLIDATA_IK_STRUCT::ReceiptRequiredLimit_9F5D
#define  INPUT_CTLS_APL_IK_DF6D_1     0x08  ///< B2b4: EMV_CTLS_APPLIDATA_IK_STRUCT::TryAgainLimit_DF6D
#define  INPUT_CTLS_APL_IK_9F09_1     0x10  ///< B2b5: EMV_CTLS_APPLIDATA_IK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_IK_9F15_1     0x20  ///< B2b6: EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_IK_9F16_1     0x40  ///< B2b7: EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantIdentifier_9F16
#define  INPUT_CTLS_APL_IK_9F4E_1     0x80  ///< B2b8: EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantNameAndLocation_9F4E
  /* Byte 3 */
#define  INPUT_CTLS_APL_IK_9F01_2     0x01  ///< B3b1: EMV_CTLS_APPLIDATA_IK_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_IK_DFAB31_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_IK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_IK_DFAB41_2   0x04  ///< B3b3: EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_IK_DFAB42_2   0x08  ///< B3b4: EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_IK_DFAB43_2   0x10  ///< B3b5: EMV_CTLS_APPLIDATA_IK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_IK_DFAB44_2   0x20  ///< B3b6: EMV_CTLS_APPLIDATA_IK_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_IK_DFAB45_2   0x40  ///< B3b7: EMV_CTLS_APPLIDATA_IK_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_IK_DFAB46_2   0x80  ///< B3b8: EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementThreshold_DFAB46
  /* Byte 4 */
#define  INPUT_CTLS_APL_IK_DFAB47_3   0x01  ///< B4b1: EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementTargetPercentage_DFAB47
#define  INPUT_CTLS_APL_IK_DFAB48_3   0x02  ///< B4b2: EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48
/// @}


//////////
// EPAL //
//////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, EK specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_EK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_EK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_EK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_EK_9F1A_0   @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_EK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_EK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_EK_9F35_TERM_TYPE
  unsigned char                     TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "Terminal Capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_EK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_EK_9F33_TERM_CAPS
  unsigned char           AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "Additional Terminal Capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_EK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_EK_9F40_ADD_TERM_CAPS
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_EK_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_EK_9F15_MERCHANT_CATEGORY_CODE
           char                       MerchantIdentifier_9F16[15+1];                     ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_9F16_0   @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_EK_9F16_MERCHANT_ID
           char                  MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_EK_9F4E_1   @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_EK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                       AcquirerIdentifier_9F01[6];                        ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_EK_9F01_1 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_EK_9F01_ACQUIRER_ID
  unsigned char             TerminalTransactionQualifier_9F66[4];                        ///< TempUpdate allowed: YES @n Description: Visa-defined Terminal Transaction Qualifiers @n Availability bit: #INPUT_CTLS_APL_EK_9F66_2   @n TLV tag: #TAG_9F66_TTQ, @n XML Tag: #XML_TAG_AD_EK_9F66_TERM_TRX_QUALIFIER
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_EK @n Availability bit: #INPUT_CTLS_APL_EK_DFAB31_1   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_EK_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_EK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_EK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_EK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_EK_DFAB41_CTLS_TRX_LIMIT
  unsigned char                               TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_EK_DFAB43_TAC_DEFAULT
  unsigned char                                TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_EK_DFAB44_TAC_DENIAL
  unsigned char                                TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_EK_DFAB45_1   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_EK_DFAB45_TAC_ONLINE
  unsigned char                 CtlsTransactionLimitCash_DFAB4A[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit for cash and cashback transactions (n12) @n Availability bit: #INPUT_CTLS_APL_EK_DFAB4A_2   @n TLV tag: #TAG_DFAB4A_CTLS_TRX_LIMIT_CASH, @n XML Tag: #XML_TAG_AD_EK_DFAB4A_CTLS_TRX_LIMIT_CASH
} EMV_CTLS_APPLIDATA_EK_TYPE;

/// @defgroup DEF_INPUT_APPLI_EK Appli data scheme specific - Epal - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_EK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_EK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_EK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_EK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_EK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_EK_9F33_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_EK_9F40_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_EK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_EK_9F09_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_EK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_EK_9F15_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_EK_9F16_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantIdentifier_9F16
  /* Byte 2 */
#define  INPUT_CTLS_APL_EK_9F4E_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_EK_9F01_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_EK_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_EK_DFAB31_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_EK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_EK_DFAB40_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_EK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_EK_DFAB41_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_EK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_EK_DFAB43_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_EK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_EK_DFAB44_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_EK_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_EK_DFAB45_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_EK_STRUCT::TACOnline_DFAB45
  /* Byte 3 */
#define  INPUT_CTLS_APL_EK_DFAB4A_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_EK_STRUCT::CtlsTransactionLimitCash_DFAB4A
#define  INPUT_CTLS_APL_EK_9F66_2     0x02  ///< B3b2: EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalTransactionQualifier_9F66
/// @}


///////////////////////
// Visa Asia/Pacific //
///////////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, PK specific part (VisaAP resp. Visa Asia/Pacific resp. Visa Wave 2).
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_PK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_PK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_PK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_PK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_PK_9F1A_0   @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_PK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values: 00 = international, 01 = domestic only @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_PK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_PK_9F35_TERM_TYPE
  unsigned char                          CvmRequirements_DF04;                           ///< TempUpdate allowed: YES @n Description: VisaAP specific, CVM requirements, bitlist, 01 = signature, 02 = Online PIN @n Availability bit: #INPUT_CTLS_APL_PK_DF04_0   @n TLV tag: #TAG_DF04_PK_CVM_REQUIREMENTS, @n XML Tag: #XML_TAG_AD_PK_CVM_REQUIREMENTS
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_PK @n Availability bit: #INPUT_CTLS_APL_PK_DFAB31_0   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_PK_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DF02[6];                        ///< TempUpdate allowed: NO  @n Description: Terminal floor limit (BCD encoded) @n Use FF FF FF FF to disable @n Availability bit: #INPUT_CTLS_APL_PK_DF02_0   @n TLV tag: #TAG_DF02_PK_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_PK_DF02_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: BCD encoded. No contactless txns above this limit (as defined by PayPass) @n PayPass spec naming: "Reader Contactless Transaction Limit" @n Visa spec naming: "Reader Contactless Transaction Limit (RCTL)" @n Visa spec strongly recommends to disable this limit. @n Use FF FF FF FF to disable @n Availability bit: #INPUT_CTLS_APL_PK_DFAB41_0   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT @n XML Tag: #XML_TAG_AD_PK_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DF01[6];                        ///< TempUpdate allowed: NO  @n Description: BCD encoded. Below this limit CVM processing is skipped, above performed @n Equal to this limit it depends on the scheme: with payWave it's performed, with PayPass not @n Use FF FF FF FF to disable @n Visa and MasterCard spec naming: "Reader CVM Required Limit" @n Availability bit: #INPUT_CTLS_APL_PK_DF01_0   @n TLV tag: #TAG_DF01_PK_CVM_REQ_LIMIT @n XML Tag: #XML_TAG_AD_PK_DF01_CTLS_CVM_REQ_LIMIT
} EMV_CTLS_APPLIDATA_PK_TYPE;

/// @defgroup DEF_INPUT_APPLI_PK Appli data scheme specific - Visa Asia/Pacific - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_PK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_PK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_PK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_PK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_PK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_PK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_PK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_PK_DF04_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_PK_STRUCT::CvmRequirements_DF04
#define  INPUT_CTLS_APL_PK_DFAB31_0   0x10  ///< B1b5: EMV_CTLS_APPLIDATA_PK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_PK_DF02_0     0x20  ///< B2b6: EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessFloorLimit_DF02
#define  INPUT_CTLS_APL_PK_DFAB41_0   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_PK_DF01_0     0x80  ///< B2b8: EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessCVMRequiredLimit_DF01
  /* Byte 2 */
/// @}


/////////////////////////////
// China Union PAY - qPBOC //
/////////////////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, CK specific part. See see Book C-7, Kernel 7 Specification at <a href="http://www.emvco.com/">[EMVCo Homepage]</a>.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_CK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_CK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_CK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_CK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_CK_9F1A_0   @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_CK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_CK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_CK_9F35_TERM_TYPE
  unsigned char             TerminalTransactionQualifier_9F66[4];                        ///< TempUpdate allowed: YES @n Description: Visa-defined Terminal Transaction Qualifiers @n Availability bit: #INPUT_CTLS_APL_CK_9F66_0   @n TLV tag: #TAG_9F66_TTQ, @n XML Tag: #XML_TAG_AD_CK_9F66_TERM_TRX_QUALIFIER
  unsigned char                     TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "terminal capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_CK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_CK_9F33_TERM_CAPS
  unsigned char           AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "additional terminal capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_CK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_CK_9F40_ADD_TERM_CAPS
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_CK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_CK_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_CK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_CK_9F15_MERCHANT_CATEGORY_CODE
           char                       MerchantIdentifier_9F16[15+1];                     ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_CK_9F16_1   @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_CK_9F16_MERCHANT_ID
           char                  MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_CK_9F4E_1   @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC @n XML Tag: #XML_TAG_AD_CK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                               TecSupport_DFAB30;                         ///< TempUpdate allowed: NO  @n Description: Specifies supported technologies. @n See @ref DEF_TEC @n Availability bit: #INPUT_CTLS_APL_CK_DFAB30_1 @n TLV tag: #TAG_DFAB30_TEC_SUPPORT @n XML Tag: #XML_TAG_AD_CK_DFAB30_TEC_SUPPORT
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_CK @n Availability bit: #INPUT_CTLS_APL_CK_DFAB31_1   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_CK_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_CK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_CK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_CK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_CK_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_CK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_CK_DFAB42_CTLS_CVM_REQ_LIMIT
} EMV_CTLS_APPLIDATA_CK_TYPE;

/// @defgroup DEF_INPUT_APPLI_CK Appli data scheme specific - China Union Pay - Avaialability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_CK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_CK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_CK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_CK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_CK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_CK_9F66_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalTransactionQualifier_9F66
#define  INPUT_CTLS_APL_CK_9F33_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_CK_9F40_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_CK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_CK_9F09_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_CK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_CK_9F15_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantCategoryCode_9F15
  /* Byte 2 */
#define  INPUT_CTLS_APL_CK_9F16_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantIdentifier_9F16
#define  INPUT_CTLS_APL_CK_9F4E_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_CK_DFAB30_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_CK_STRUCT::TecSupport_DFAB30
#define  INPUT_CTLS_APL_CK_DFAB31_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_CK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_CK_DFAB40_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_CK_DFAB41_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_CK_DFAB42_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
/// @}


//////////////////
// Gemalto Pure //
//////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, GK specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_GK_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_GK
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_GK_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_GK_9F1A_0   @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_GK_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_GK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_GK_9F35_TERM_TYPE
  unsigned char                     TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "terminal capabilities" for this application, for PayPass 3 the tags DF81xx, DF81yy, DF81zz are filled with these 3 byte, for VISA this is used for host compliancy only, for Amex in accordance with tag 9F6E, @n Availability bit: #INPUT_CTLS_APL_GK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_GK_9F33_TERM_CAPS
  unsigned char           AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "additional terminal capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_GK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_GK_9F40_ADD_TERM_CAPS
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_GK_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_GK_9F15_MERCHANT_CATEGORY_CODE
           char                       MerchantIdentifier_9F16[15+1];                     ///< TempUpdate allowed: NO  @n Description: Merchant Identifier @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_9F16_0   @n TLV tag: #TAG_9F16_MERCHANT_ID, @n XML Tag: #XML_TAG_AD_GK_9F16_MERCHANT_ID
           char                  MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_GK_9F4E_1   @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_GK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                       AcquirerIdentifier_9F01[6];                        ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_GK_9F01_1 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_GK_9F01_ACQUIRER_ID
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_GK @n Availability bit: #INPUT_CTLS_APL_GK_DFAB31_1   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_GK_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable @n Availability bit: #INPUT_CTLS_APL_GK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_GK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_GK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_GK_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_GK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_GK_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                               TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_GK_DFAB43_TAC_DEFAULT
  unsigned char                                TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_GK_DFAB44_TAC_DENIAL
  unsigned char                                TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_GK_DFAB45_2   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_GK_DFAB45_TAC_ONLINE
  unsigned char                         CtlsAppKernelCap_DFAB4F[5];                      ///< TempUpdate allowed: YES @n Description: Contactless Application/Kernel Capabilities (Gemalto PURE tag 'C8', DTTPI) @n Availability bit: #INPUT_CTLS_APL_GK_DFAB4F_2 @n TLV tag: #TAG_DFAB4F_PURE_CTLS_APP_KERN_CAP, @n XML Tag: #XML_TAG_AD_GK_DFAB4F_CTLS_APP_KERN_CAP
  EMV_CTLS_DATA_TYPE                                MTOL_DFAB50;                         ///< TempUpdate allowed: NO  @n Description: Mandatory Tag Object List (MTOL) @n Availability bit: #INPUT_CTLS_APL_GK_DFAB50_2 @n TLV tag: #TAG_DFAB50_PURE_MTOL, @n XML Tag: #XML_TAG_AD_GK_DFAB50_MTOL
  EMV_CTLS_DATA_TYPE                         DefaultDDOL_DFAB51;                         ///< TempUpdate allowed: NO  @n Description: Default Dynamic Data Authentication Data Object List (Default DDOL) @n Availability bit: #INPUT_CTLS_APL_GK_DFAB51_2 @n TLV tag: #TAG_DFAB51_CTLS_DEFAULT_DDOL, @n XML Tag: #XML_TAG_AD_GK_DFAB51_DEFAULT_DDOL
  EMV_CTLS_DATA_TYPE             TerminalTransactionData_9F76;                           ///< TempUpdate allowed: NO  @n Description: Terminal transaction data @n Availability bit: #INPUT_CTLS_APL_GK_9F76_2 @n TLV tag: #TAG_9F76_PURE_TERM_TRX_DATA, @n XML Tag: #XML_TAG_AD_GK_9F76_TERMINAL_TRX_DATA
  unsigned char                       AppliAuthTransType_DFAB5A;                         ///< TempUpdate allowed: NO  @n Description: Transaction Type used for Application Authentication Transaction @n Availability bit: #INPUT_CTLS_APL_GK_DFAB5A_2, @n TLV tag: #TAG_DFAB5A_PURE_APPLI_AUTH_TRX_TYPE, @n XML Tag: #XML_TAG_AD_GK_DFAB5A_APPLI_AUTH_TRX_TYPE
} EMV_CTLS_APPLIDATA_GK_TYPE;

/// @defgroup DEF_INPUT_APPLI_GK Appli data scheme specific - Gemalto Pure - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_GK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_GK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_GK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_GK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_GK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_GK_9F33_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_GK_9F40_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_GK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_GK_9F09_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_GK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_GK_9F15_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_GK_9F16_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantIdentifier_9F16
  /* Byte 2 */
#define  INPUT_CTLS_APL_GK_9F4E_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_GK_9F01_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_GK_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_GK_DFAB31_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_GK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_GK_DFAB40_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_GK_DFAB41_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_GK_DFAB42_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_GK_DFAB43_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_GK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_GK_DFAB44_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_GK_STRUCT::TACDenial_DFAB44
  /* Byte 3 */
#define  INPUT_CTLS_APL_GK_DFAB45_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_GK_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_GK_DFAB4F_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_GK_STRUCT::CtlsAppKernelCap_DFAB4F
#define  INPUT_CTLS_APL_GK_DFAB50_2   0x04  ///< B3b3: EMV_CTLS_APPLIDATA_GK_STRUCT::MTOL_DFAB50
#define  INPUT_CTLS_APL_GK_DFAB51_2   0x08  ///< B3b4: EMV_CTLS_APPLIDATA_GK_STRUCT::DefaultDDOL_DFAB51
#define  INPUT_CTLS_APL_GK_9F76_2     0x10  ///< B3b5: EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalTransactionData_9F76
#define  INPUT_CTLS_APL_GK_DFAB5A_2   0x20  ///< B3b6: EMV_CTLS_APPLIDATA_GK_STRUCT::AppliAuthTransType_DFAB5A
/// @}


///////////
// RuPay //
///////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, RK specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_RK_STRUCT {
  unsigned char                          IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_RK
  unsigned char                             TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_RK_9F1C_0   @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_RK_9F1C_TERM_IDENT
  unsigned char                   TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_RK_9F1A_0   @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_RK_9F1A_TERM_COUNTRY_CODE
  unsigned char                          TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_RK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_RK_9F35_TERM_TYPE
  unsigned char                  TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "terminal capabilities" for this application @n Availability bit: #INPUT_CTLS_APL_RK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_RK_9F33_TERM_CAPS
  unsigned char        AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "additional terminal capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_RK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_RK_9F40_ADD_TERM_CAPS
  unsigned char                         VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_RK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_RK_9F09_VERSION_NUMBER
  unsigned char                  MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_RK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_RK_9F15_MERCHANT_CATEGORY_CODE
  unsigned char                            AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_RK @n Availability bit: #INPUT_CTLS_APL_RK_DFAB31_0   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_RK_DFAB31_APP_FLOW_CAP
  unsigned char                 ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable @n Availability bit: #INPUT_CTLS_APL_RK_DFAB40_1   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_RK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char           ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_RK_DFAB41_1    @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_RK_DFAB41_CTLS_TRX_LIMIT
  unsigned char           ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_RK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_RK_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                            TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_RK_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_RK_DFAB43_TAC_DEFAULT
  unsigned char                             TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_RK_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_RK_DFAB44_TAC_DENIAL
  unsigned char                             TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_RK_DFAB45_1   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_RK_DFAB45_TAC_ONLINE
  unsigned char               RiskManagementThreshold_DFAB46[6];                      ///< TempUpdate allowed: YES @n Description: Threshold Value for Biased Random Online Selection during risk management (n12) @n Availability bit: #INPUT_CTLS_APL_RK_DFAB46_1 @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD @n XML Tag: #XML_TAG_AD_RK_DFAB46_RISK_MGMT_THRESHOLD
  unsigned char        RiskManagementTargetPercentage_DFAB47;                         ///< TempUpdate allowed: YES @n Description: Target percentage for random online selection during risk management (n2) @n Availability bit: #INPUT_CTLS_APL_RK_DFAB47_1 @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT @n XML Tag: #XML_TAG_AD_RK_DFAB47_RISK_MGMT_TRGT_PERC
  unsigned char RiskManagementMaximumTargetPercentage_DFAB48;                         ///< TempUpdate allowed: YES @n Description: Maximum target percentage for random online selection during risk management (n2) @n Availability bit: #INPUT_CTLS_APL_RK_DFAB48_2 @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT @n XML Tag: #XML_TAG_AD_RK_DFAB48_RISK_MGMT_MAX_TRGT_PERC
  unsigned char                       CallbackTimeout_DFAB4D[2];                      ///< TempUpdate allowed: NO  @n Description: Callback Timeout (VFI Reader 1F76) @n Availability bit: #INPUT_CTLS_APL_RK_DFAB4D_2 @n TLV tag: #TAG_DFAB4D_RUPAY_CALLBACK_TIMEOUT @n XML Tag: #XML_TAG_AD_RK_DFAB4D_CALLBACK_TIMEOUT
  unsigned char               TornTransactionInterval_DFAB4E[2];                      ///< TempUpdate allowed: NO  @n Description: Torn Transaction Interval (VFI Reader 1F77) @n Availability bit: #INPUT_CTLS_APL_RK_DFAB4E_2 @n TLV tag: #TAG_DFAB4E_RUPAY_TORN_TRX_INTERVAL @n XML Tag: #XML_TAG_AD_RK_DFAB4E_TORN_TRX_INTERVAL
  unsigned char     AdditionalTerminalCapabilitiesExt_DF3A[5];                        ///< TempUpdate allowed: YES @n Description: Additional Terminal Capabilities Extension @n Availability bit: #INPUT_CTLS_APL_RK_DF3A_2    @n TLV tag: #TAG_DF3A_ADD_TRM_CAP_EXT, @n XML Tag: #XML_TAG_AD_RK_9F40_ADD_TERM_CAPS
} EMV_CTLS_APPLIDATA_RK_TYPE;

/// @defgroup DEF_INPUT_APPLI_RK Appli data scheme specific - RuPay - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_RK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_RK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_RK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_RK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_RK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_RK_9F33_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_RK_9F40_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_RK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_RK_9F09_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_RK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_RK_9F15_0     0x40  ///< B127: EMV_CTLS_APPLIDATA_RK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_RK_DFAB31_0   0x80  ///< B1b8: EMV_CTLS_APPLIDATA_RK_STRUCT::AppFlowCap_DFAB31
  /* Byte 2 */
#define  INPUT_CTLS_APL_RK_DFAB40_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_RK_DFAB41_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_RK_DFAB42_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_RK_DFAB43_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_RK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_RK_DFAB44_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_RK_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_RK_DFAB45_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_RK_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_RK_DFAB46_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementThreshold_DFAB46
#define  INPUT_CTLS_APL_RK_DFAB47_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementTargetPercentage_DFAB47
/* Byte 3 */
#define  INPUT_CTLS_APL_RK_DFAB48_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48
#define  INPUT_CTLS_APL_RK_DFAB4D_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_RK_STRUCT::CallbackTimeout_DFAB4D
#define  INPUT_CTLS_APL_RK_DFAB4E_2   0x04  ///< B3b3: EMV_CTLS_APPLIDATA_RK_STRUCT::TornTransactionInterval_DFAB4E
#define  INPUT_CTLS_APL_RK_DF3A_2     0x08  ///< B3b4: EMV_CTLS_APPLIDATA_RK_STRUCT::AdditionalTerminalCapabilitiesExt_DF3A
/// @}


//////////////
//// SIBS  ////
//////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, SK specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_SK_STRUCT {
  unsigned char                          IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_SK
  unsigned char                             TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_SK_9F1C_0    @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_SK_9F1C_TERM_IDENT
  unsigned char                   TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal country code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_SK_9F1A_0    @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_SK_9F1A_TERM_COUNTRY_CODE
  unsigned char                          TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_SK_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_SK_9F35_TERM_TYPE
  unsigned char                  TerminalCapabilities_9F33[3];                        ///< TempUpdate allowed: YES @n Description: "terminal capabilities" for this application @n Availability bit: #INPUT_CTLS_APL_SK_9F33_0   @n TLV tag: #TAG_9F33_TRM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_SK_9F33_TERM_CAPS
  unsigned char        AdditionalTerminalCapabilities_9F40[5];                        ///< TempUpdate allowed: YES @n Description: "additional terminal capabilities" for this application, only 1st byte is effective @n Availability bit: #INPUT_CTLS_APL_SK_9F40_0    @n TLV tag: #TAG_9F40_ADD_TRM_CAP, @n XML Tag: #XML_TAG_AD_SK_9F40_ADD_TERM_CAPS
  unsigned char                         VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_SK_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_SK_9F09_VERSION_NUMBER
  unsigned char                  MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_SK_9F15_0   @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_SK_9F15_MERCHANT_CATEGORY_CODE
  char                        MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant @n Availability bit: #INPUT_CTLS_APL_SK_9F4E_0  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_SK_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                            AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_SK @n Availability bit: #INPUT_CTLS_APL_SK_DFAB31_1    @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_SK_DFAB31_APP_FLOW_CAP
  unsigned char                 ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable @n Availability bit: #INPUT_CTLS_APL_SK_DFAB40_1    @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_SK_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char           ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Transaction Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_SK_DFAB41_1   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_SK_DFAB41_CTLS_TRX_LIMIT
  unsigned char           ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable. @n Availability bit: #INPUT_CTLS_APL_SK_DFAB42_1   @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_SK_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                            TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_SK_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_SK_DFAB43_TAC_DEFAULT
  unsigned char                             TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_SK_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_SK_DFAB44_TAC_DENIAL
  unsigned char                             TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_SK_DFAB45_1   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_SK_DFAB45_TAC_ONLINE
  unsigned char                 TermCap_aboveCVMLimit_DFAB53[3];                      ///< TempUpdate allowed: YES @n Description: Separate cardholder verification methods for transactions above the CVM limit @n Availability bit: #INPUT_CTLS_APL_SK_DFAB53_1 @n TLV tag: #TAG_DFAB53_TERMCAP_ABOVE_CVM_LIMIT, @n XML Tag: #XML_TAG_AD_SK_DFAB53_TERMCAP_ABOVE_CVM_LIMIT
  unsigned char                 TermCap_belowCVMLimit_DFAB54[3];                      ///< TempUpdate allowed: YES @n Description: Separate cardholder verification methods for transactions below the CVM limit @n Availability bit: #INPUT_CTLS_APL_SK_DFAB54_2 @n TLV tag: #TAG_DFAB54_TERMCAP_BELOW_CVM_LIMIT, @n XML Tag: #XML_TAG_AD_SK_DFAB54_TERMCAP_BELOW_CVM_LIMIT
} EMV_CTLS_APPLIDATA_SK_TYPE;

/// @defgroup DEF_INPUT_APPLI_SK Appli data scheme specific - SIBS - Avaiability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_SK_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_SK_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_SK_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_SK_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_SK_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_SK_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_SK_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_SK_9F33_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_SK_STRUCT::TerminalCapabilities_9F33
#define  INPUT_CTLS_APL_SK_9F40_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_SK_STRUCT::AdditionalTerminalCapabilities_9F40
#define  INPUT_CTLS_APL_SK_9F09_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_SK_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_SK_9F15_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_SK_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_SK_9F4E_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_SK_STRUCT::MerchantNameAndLocation_9F4E
  /* Byte 2 */
#define  INPUT_CTLS_APL_SK_DFAB31_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_SK_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_SK_DFAB40_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_SK_STRUCT::ContactlessFloorLimit_DFAB40
#define  INPUT_CTLS_APL_SK_DFAB41_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_SK_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_SK_DFAB42_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_SK_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_SK_DFAB43_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_SK_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_SK_DFAB44_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_SK_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_SK_DFAB45_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_SK_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_SK_DFAB53_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_SK_STRUCT::TermCap_aboveCVMLimit_DFAB53
  /* Byte 3 */
#define  INPUT_CTLS_APL_SK_DFAB54_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_SK_STRUCT::TermCap_belowCVMLimit_DFAB54
/// @}


//////////////////
// PagoBancomat //
//////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, PagoBancomat specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_PB_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_PB
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_9F1C_0     @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_PB_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: "Terminal Country Code" (e.g. VISA Germany 0276) @n Availability bit: #INPUT_CTLS_APL_PB_9F1A_0
                                                                                         ///< @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_PB_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CTLS_SetTermData()
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_9F35_0   @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_PB_9F35_TERM_TYPE
  unsigned char             TerminalTransactionQualifier_9F66[4];                        ///< TempUpdate allowed: YES @n Description: Visa-defined Terminal Transaction Qualifiers @n Availability bit: #INPUT_CTLS_APL_PB_9F66_0
                                                                                         ///< @n TLV tag: #TAG_9F66_TTQ, @n XML Tag: #XML_TAG_AD_PB_9F66_TERM_TRX_QUALIFIER
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_9F09_0   @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_PB_9F09_VERSION_NUMBER
  char                           MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant @n Availability bit: #INPUT_CTLS_APL_PB_9F4E_0
                                                                                         ///< @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_PB_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_PB @n Availability bit: #INPUT_CTLS_APL_PB_DFAB31_0
                                                                                         ///< @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_PB_DFAB31_APP_FLOW_CAP
  unsigned char                    ContactlessFloorLimit_DFAB40[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless Floor Limit (n12) @n Use '999999999999' to disable
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB40_0   @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_PB_DFAB40_CTLS_FLOOR_LIMIT
  unsigned char              ContactlessTransactionLimit_DFAB41[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Transaction Floor Limit (n12) @n Use '9999999999' to disable.
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB41_1
                                                                                         ///< @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT, @n XML Tag: #XML_TAG_AD_PB_DFAB41_CTLS_TRX_LIMIT
  unsigned char              ContactlessCVMRequiredLimit_DFAB42[6];                      ///< TempUpdate allowed: NO  @n Description: Reader Contactless CVM Required Limit (n12) @n Use '9999999999' to disable.
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB42_1
                                                                                         ///< @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT, @n XML Tag: #XML_TAG_AD_PB_DFAB42_CTLS_CVM_REQ_LIMIT
  unsigned char                               TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_PB_DFAB43_TAC_DEFAULT
  unsigned char                                TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_PB_DFAB44_TAC_DENIAL
  unsigned char                                TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB45_1   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_PB_DFAB45_TAC_ONLINE
  unsigned char                  RiskManagementThreshold_DFAB46[6];                      ///< TempUpdate allowed: YES @n Description: Threshold Value for Biased Random Online Selection during risk management (n12)
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB46_1 @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD @n XML Tag: #XML_TAG_AD_PB_DFAB46_RISK_MGMT_THRESHOLD
  unsigned char           RiskManagementTargetPercentage_DFAB47;                         ///< TempUpdate allowed: YES @n Description: Target percentage for random online selection during risk management (n2)
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB47_1 @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT
                                                                                         ///< @n XML Tag: #XML_TAG_AD_PB_DFAB47_RISK_MGMT_TRGT_PERC
  unsigned char    RiskManagementMaximumTargetPercentage_DFAB48;                         ///< TempUpdate allowed: YES @n Description: Maximum target percentage for random online selection during risk management (n2)
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB48_1 @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT @n XML Tag: #XML_TAG_AD_PB_DFAB48_RISK_MGMT_MAX_TRGT_PERC
  unsigned char                       TACSwitchInterface_DFAB55[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Switch Interface
                                                                                         ///< @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_PB_DFAB55_2
                                                                                         ///< @n TLV tag: #TAG_DFAB55_TAC_SWITCH_INTERFACE, @n XML Tag: #XML_TAG_AD_PB_DFAB55_TAC_SWITCH_INTERFACE
  unsigned char                       IACSwitchInterface_DFAB56[5];                      ///< TempUpdate allowed: YES @n Description: Issuer Action Code - Switch Interface DF73 / Fallback parameter when
                                                                                         ///< the Card Issuer Action Code - Switch Interface 9F79 is not available @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                         ///< @n Availability bit: #INPUT_CTLS_APL_PB_DFAB56_2   @n TLV tag: #TAG_DFAB56_IAC_SWITCH_INTERFACE, @n XML Tag: #XML_TAG_AD_PB_DFAB56_IAC_SWITCH_INTERFACE
} EMV_CTLS_APPLIDATA_PB_TYPE;

/// @defgroup DEF_INPUT_APPLI_PB Appli data scheme specific - PagoBancomat - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_PB_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_PB_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_PB_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_PB_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_PB_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_PB_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_PB_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_PB_9F66_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_PB_STRUCT::TerminalTransactionQualifier_9F66
#define  INPUT_CTLS_APL_PB_9F09_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_PB_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_PB_9F4E_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_PB_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_PB_DFAB31_0   0x40  ///< B1b7: EMV_CTLS_APPLIDATA_PB_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_PB_DFAB40_0   0x80  ///< B1b8: EMV_CTLS_APPLIDATA_PB_STRUCT::ContactlessFloorLimit_DFAB40
/* Byte 2 */
#define  INPUT_CTLS_APL_PB_DFAB41_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_PB_STRUCT::ContactlessTransactionLimit_DFAB41
#define  INPUT_CTLS_APL_PB_DFAB42_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_PB_STRUCT::ContactlessCVMRequiredLimit_DFAB42
#define  INPUT_CTLS_APL_PB_DFAB43_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_PB_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_PB_DFAB44_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_PB_STRUCT::TACDenial_DFAB44
#define  INPUT_CTLS_APL_PB_DFAB45_1   0x10  ///< B2b5: EMV_CTLS_APPLIDATA_PB_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_PB_DFAB46_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_PB_STRUCT::RiskManagementThreshold_DFAB46
#define  INPUT_CTLS_APL_PB_DFAB47_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_PB_STRUCT::RiskManagementTargetPercentage_DFAB47
#define  INPUT_CTLS_APL_PB_DFAB48_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_PB_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48
/* Byte 3 */
#define  INPUT_CTLS_APL_PB_DFAB55_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_PB_STRUCT::TACSwitchInterface_DFAB55
#define  INPUT_CTLS_APL_PB_DFAB56_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_PB_STRUCT::IACSwitchInterface_DFAB56
/// @}


/////////
// MIR //
/////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application, MIR specific part.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_MR_STRUCT {
  unsigned char                             IncludedData[8];                             ///< Availability bits, see @ref DEF_INPUT_APPLI_MR
  unsigned char                                TermIdent_9F1C[8];                        ///< TempUpdate allowed: YES @n Description: Terminal Identification. @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MR_9F1C_0 @n TLV tag: #TAG_9F1C_TRM_ID, @n XML Tag: #XML_TAG_AD_MR_9F1C_TERM_IDENT
  unsigned char                      TerminalCountryCode_9F1A[2];                        ///< TempUpdate allowed: YES @n Description: Terminal Country Code. @n Availability bit: #INPUT_CTLS_APL_MR_9F1A_0 @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE, @n XML Tag: #XML_TAG_AD_MR_9F1A_TERM_COUNTRY_CODE
  unsigned char                             TerminalType_9F35;                           ///< TempUpdate allowed: YES @n Description: Terminal type, possible values see @ref TERM_TYPES. @n mandatory for first call of EMV_CTLS_SetTermData() @n Availability bit: #INPUT_CTLS_APL_MR_9F35_0 @n TLV tag: #TAG_9F35_TRM_TYPE, @n XML Tag: #XML_TAG_AD_MR_9F35_TERM_TYPE
  unsigned char                            VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];  ///< TempUpdate allowed: NO  @n Description: Application chip version number. @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MR_9F09_0 @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB, @n XML Tag: #XML_TAG_AD_MR_9F09_VERSION_NUMBER
  unsigned char                     MerchantCategoryCode_9F15[2];                        ///< TempUpdate allowed: NO  @n Description: Merchant category code. @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MR_9F15_0 @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE, @n XML Tag: #XML_TAG_AD_MR_9F15_MERCHANT_CATEGORY_CODE
  char                           MerchantNameAndLocation_9F4E[40+1];                     ///< TempUpdate allowed: NO  @n Description: Indicates the name and location of the merchant. @n Availability bit: #INPUT_CTLS_APL_MR_9F4E_0 @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC, @n XML Tag: #XML_TAG_AD_MR_9F4E_MERCHANT_NAME_LOCATION
  unsigned char                       AcquirerIdentifier_9F01[6];                        ///< TempUpdate allowed: NO  @n Description: Uniquely identifies the acquirer within each payment system. @n Availability bit: #INPUT_CTLS_APL_MR_9F01_0 @n TLV tag: #TAG_9F01_ACQ_ID @n XML Tag: #XML_TAG_AD_MR_9F01_ACQUIRER_ID
  unsigned char                       TerminalFloorLimit_DF51[6];                        ///< TempUpdate allowed: NO  @n Description: Terminal floor limit (BCD encoded): the maximum amount allowing the offline operation processing mode. @n Availability bit: #INPUT_CTLS_APL_MR_DF51_0 @n TLV tag: #TAG_DF51_TRM_FLOOR_LIMIT, @n XML Tag: #XML_TAG_AD_MR_DF51_TRM_FLOOR_LIMIT
  unsigned char                       TerminalNoCVMLimit_DF52[6];                        ///< TempUpdate allowed: NO  @n Description: Terminal No CVM Limit (BCD encoded): the maximum amount of the transaction performed without Cardholder verification. @n Availability bit: #INPUT_CTLS_APL_MR_DF52_1 @n TLV tag: #TAG_DF52_TRM_NOCVM_LIMIT, @n XML Tag: #XML_TAG_AD_MR_DF52_TRM_NOCVM_LIMIT
  unsigned char         TerminalContactlessLimitNonCDCVM_DF53[6];                        ///< TempUpdate allowed: NO  @n Description: Terminal Contactless Limit (BCD encoded) for Non CD-CVM: the maximum transaction amount using the contactless interface for Non CD-CVM. @n Availability bit: #INPUT_CTLS_APL_MR_DF53_1 @n TLV tag #TAG_DF53_TRM_CTLS_LIMIT_NON_CDCVM, @n XML Tag: #XML_TAG_AD_MR_DF53_TRM_CTLS_LIMIT_NON_CDCVM
  unsigned char            TerminalContactlessLimitCDCVM_DF54[6];                        ///< TempUpdate allowed: NO  @n Description: Terminal Contactless Limit (BCD encoded) for CD-CVM: the maximum transaction amount using the contactless interface for CD-CVM. @n Availability bit: #INPUT_CTLS_APL_MR_DF54_1 @n TLV tag #TAG_DF54_TRM_CTLS_LIMIT_CDCVM, @n XML Tag: #XML_TAG_AD_MR_DF54_TRM_CTLS_LIMIT_CDCVM
  unsigned char                  TerminalTPMCapabilities_DF55[2];                        ///< TempUpdate allowed: NO  @n Description: MIR Terminal TPM Capabilities. @n Availability bit: #INPUT_CTLS_APL_MR_DF55_1 @n TLV tag: #TAG_DF55_TRM_TPM_CAPABILITIES, @n XML Tag: #XML_TAG_AD_MR_DF55_TRM_TPM_CAPABILITIES
  unsigned char                 TransactionRecoveryLimit_DF56;                           ///< TempUpdate allowed: NO  @n Description: MIR Transaction Recovery Limit. @n Availability bit: #INPUT_CTLS_APL_MR_DF56_1 @n TLV tag: #TAG_DF56_TRANSACTION_RECOVERY_LIMIT, @n XML Tag: #XML_TAG_AD_MR_DF56_TRANSACTION_RECOVERY_LIMIT
  unsigned char                               AppFlowCap_DFAB31[5];                      ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_MR @n Availability bit: #INPUT_CTLS_APL_MR_DFAB31_1 @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_MR_DFAB31_APP_FLOW_CAP
  unsigned char                               TACDefault_DFAB43[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Default @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific ///< @n Availability bit: #INPUT_CTLS_APL_MR_DFAB43_1   @n TLV tag: #TAG_DFAB43_TAC_DEFAULT, @n XML Tag: #XML_TAG_AD_MR_DFAB43_TAC_DEFAULT
  unsigned char                                TACDenial_DFAB44[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Denial @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MR_DFAB44_1   @n TLV tag: #TAG_DFAB44_TAC_DENIAL, @n XML Tag: #XML_TAG_AD_MR_DFAB44_TAC_DENIAL
  unsigned char                                TACOnline_DFAB45[5];                      ///< TempUpdate allowed: YES @n Description: Terminal Action Code - Online @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific @n Availability bit: #INPUT_CTLS_APL_MR_DFAB45_2   @n TLV tag: #TAG_DFAB45_TAC_ONLINE, @n XML Tag: #XML_TAG_AD_MR_DFAB45_TAC_ONLINE
  EMV_CTLS_DATA_TYPE                 DataExchangeTagList_DFAB57;                         ///< TempUpdate allowed: NO  @n Description: Data Exchange Tag List: a set of tags which the terminal expects to receive from the kernel during data exchange @n Availability bit: #INPUT_CTLS_APL_MR_DFAB57_2 @n TLV tag: #TAG_DFAB57_MIR_DATA_EXCHANGE_TAGS @n XML Tag: #XML_TAG_AD_MR_DFAB57_MIR_DATA_EXCHANGE_TAGS
} EMV_CTLS_APPLIDATA_MR_TYPE;

/// @defgroup DEF_INPUT_APPLI_MR Appli data scheme specific - MIR - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_MIR_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_MR_9F1C_0     0x01  ///< B1b1: EMV_CTLS_APPLIDATA_MR_STRUCT::TermIdent_9F1C
#define  INPUT_CTLS_APL_MR_9F1A_0     0x02  ///< B1b2: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalCountryCode_9F1A
#define  INPUT_CTLS_APL_MR_9F35_0     0x04  ///< B1b3: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalType_9F35
#define  INPUT_CTLS_APL_MR_9F09_0     0x08  ///< B1b4: EMV_CTLS_APPLIDATA_MR_STRUCT::VersionNumber_9F09
#define  INPUT_CTLS_APL_MR_9F15_0     0x10  ///< B1b5: EMV_CTLS_APPLIDATA_MR_STRUCT::MerchantCategoryCode_9F15
#define  INPUT_CTLS_APL_MR_9F4E_0     0x20  ///< B1b6: EMV_CTLS_APPLIDATA_MR_STRUCT::MerchantNameAndLocation_9F4E
#define  INPUT_CTLS_APL_MR_9F01_0     0x40  ///< B1b7: EMV_CTLS_APPLIDATA_MR_STRUCT::AcquirerIdentifier_9F01
#define  INPUT_CTLS_APL_MR_DF51_0     0x80  ///< B1b8: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalFloorLimit_DF51
/* Byte 2 */
#define  INPUT_CTLS_APL_MR_DF52_1     0x01  ///< B2b1: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalNoCVMLimit_DF52
#define  INPUT_CTLS_APL_MR_DF53_1     0x02  ///< B2b2: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalContactlessLimitNonCDCVM_DF53
#define  INPUT_CTLS_APL_MR_DF54_1     0x04  ///< B2b3: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalContactlessLimitCDCVM_DF54
#define  INPUT_CTLS_APL_MR_DF55_1     0x08  ///< B2b4: EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalTPMCapabilities_DF55
#define  INPUT_CTLS_APL_MR_DF56_1     0x10  ///< B2b5: EMV_CTLS_APPLIDATA_MR_STRUCT::TransactionRecoveryLimit_DF56
#define  INPUT_CTLS_APL_MR_DFAB31_1   0x20  ///< B2b6: EMV_CTLS_APPLIDATA_MR_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_MR_DFAB43_1   0x40  ///< B2b7: EMV_CTLS_APPLIDATA_MR_STRUCT::TACDefault_DFAB43
#define  INPUT_CTLS_APL_MR_DFAB44_1   0x80  ///< B2b8: EMV_CTLS_APPLIDATA_MR_STRUCT::TACDenial_DFAB44
/* Byte 3 */
#define  INPUT_CTLS_APL_MR_DFAB45_2   0x01  ///< B3b1: EMV_CTLS_APPLIDATA_MR_STRUCT::TACOnline_DFAB45
#define  INPUT_CTLS_APL_MR_DFAB57_2   0x02  ///< B3b2: EMV_CTLS_APPLIDATA_MR_STRUCT::DataExchangeTagList_DFAB57
/// @}


///////////////////////
//     Domestic      //
///////////////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single domestic application
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
typedef struct EMV_CTLS_APPLIDATA_DOM_STRUCT {
  unsigned char                       IncludedData[8]; ///< Availability bits, see @ref DEF_INPUT_APPLI_DOM
  unsigned char                  AppFlowCap_DFAB31[5]; ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_PK @n Availability bit: #INPUT_CTLS_APL_DOM_DFAB31_0   @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_DOM_DFAB31_APP_FLOW_CAP
  unsigned char ContactlessTransactionLimit_DFAB41[6]; ///< TempUpdate allowed: NO  @n Description: BCD encoded. No contactless txns above this limit (as defined by PayPass) @n PayPass spec naming: "Reader Contactless Transaction Limit" @n Visa spec naming: "Reader Contactless Transaction Limit (RCTL)" @n Visa spec strongly recommends to disable this limit. @n Use FF FF FF FF to disable @n Availability bit: #INPUT_CTLS_APL_DOM_DFAB41_0   @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT @n XML Tag: #XML_TAG_AD_DOM_DFAB41_CTLS_TRX_LIMIT
  } EMV_CTLS_APPLIDATA_DOM_TYPE;

/// @defgroup DEF_INPUT_APPLI_DOM Appli data scheme specific - domestic kernel - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_DOM_STRUCT::IncludedData
/// @{
/* Byte 1 */
#define  INPUT_CTLS_APL_DOM_DFAB31_0   0x01  ///< B1b5: EMV_CTLS_APPLIDATA_DOM_STRUCT::AppFlowCap_DFAB31
#define  INPUT_CTLS_APL_DOM_DFAB41_0   0x02  ///< B2b7: EMV_CTLS_APPLIDATA_DOM_STRUCT::ContactlessTransactionLimit_DFAB41
/// @}


/// @defgroup DEF_FLOW_DOM application flow capabilities for domestic applications
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CTLS_APPLIDATA_DOM_STRUCT::AppFlowCap_DFAB31
/// @{
/* Byte 1 */
#define EMV_CTLS_FLOW_DOM_EXPLICIT_SELECT 0x01  ///< B1b1: Try explicit select in case of non-EMV mode or PSE failed
/// @}

////////////
// global //
////////////

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application.
/// See EMV_CTLS_SetAppliDataSchemeSpecific()
/// @n XML tag #XML_TAG_AD_APP @n no TLV tag is used
typedef struct EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT {

  unsigned char                    IncludedData[8];                                   ///< Availability bits, see @ref DEF_INPUT_APPLI_GLOBAL

  // Global ADK related parameters
  unsigned char                        ASI_DFAB02;                               ///< TempUpdate allowed: NO  @n Description: Application selection indicator @n Must the card's AID match the configured AID exactly?
                                                                                 ///< @n @c 0 ... yes @n @c 1 ... no @n mandatory for #EMV_CTLS_SetAppliDataSchemeSpecific
                                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB02_0   @n TLV tag #TAG_DFAB02_ASI, @n XML Tag: #XML_TAG_AD_DFAB02_ASI
  unsigned char                 AppFlowCap_DFAB03[5];                            ///< TempUpdate allowed: YES @n Description: Configure special application characteristics, see @ref DEF_FLOW_GLOB
                                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB03_0   @n TLV tag #TAG_DFAB03_APP_FLOW_CAP, @n XML Tag: #XML_TAG_AD_DFAB03_APP_FLOW_CAP
  EMV_CTLS_APPLI_TYPE PriorityApplications_DFAB04[EMV_ADK_MAX_PRIO_APP];         ///< TempUpdate allowed: NO  @n Description: Priority applications for this application (::EMV_CTLS_APPLI_STRUCT) @n max. number see #EMV_ADK_MAX_PRIO_APP
                                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB04_0   @n TLV tag #TAG_DFAB04_PRIO_APPS, @n XML Tag: #XML_TAG_AD_DFAB04_PRIO_APPS
  unsigned char           SpecialTRXConfig_DFAB05[8];                            ///< TempUpdate allowed: NO  @n Description: List of special allowed transactions (manual reversal, refund, reservation, tip, Referral), see @ref SPECIAL_TRXS
                                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB05_0   @n TLV tag #TAG_DFAB05_SPECIAL_TRX_CFG, @n XML Tag: #XML_TAG_AD_DFAB05_SPECIAL_TRX_CONFIG
           char           ChksumEntryPoint_DFAB06[EMV_ADK_CHECKSUM_ASCII_SIZE];  ///< TempUpdate allowed: NO  @n Description: Entrypoint checksum dynamically calculated by the configuration @n Only valid in EMV_CTLS_GetAppliData()
                                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB06_0   @n TLV tag #TAG_DFAB06_CHKSUM_EP, @n XML Tag: #XML_TAG_AD_DFAB06_CHKSUM_ENTRY_POINT
           char               ChksumKernel_DFAB07[EMV_ADK_CHECKSUM_ASCII_SIZE];  ///< TempUpdate allowed: NO  @n Description: Kernel checksum dynamically calculated by the configuration @n Only valid in EMV_CTLS_GetAppliData()
                                                                                 ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB07_0   @n TLV tag #TAG_DFAB07_CHKSUM_KERNEL, @n XML Tag: #XML_TAG_AD_DFAB07_CHKSUM_KERNEL
  unsigned char              RetapFieldOff_DFAB08;                               ///< TempUpdate allowed: YES @n Description: On mobile retap the RF field is switched off. This parameter determines for how long.
                                                                                 ///< Unit: 0.1 second. Vertex only. @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB08_0
                                                                                 ///< @n TLV tag #TAG_DFAB08_RETAP_FIELD_OFF, @n XML Tag: #XML_TAG_AD_DFAB08_RETAP_FIELD_OFF

  // Global kernel related parameters. These are relevant for ADK AND kernel configuration and therefore are handled for each of the kernels (if applicable only)
  EMV_CTLS_DATA_TYPE          AdditionalTagsTRM_DFAB20;                               ///< TempUpdate allowed: NO  @n Description: Additional terminal data for special applications
                                                                                      ///< @n Variants: a) list of primitive tags, b) primitive tags embedded in constructed tags defining the format
                                                                                      ///< @n (see #TAG_E2_FORMAT_B, #TAG_E3_FORMAT_N, ...). Restriction: Tag length must not exceed 4 bytes @n Link to struct: ::EMV_CTLS_DATA_STRUCT.
                                                                                      ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB20_1   @n TLV tag #TAG_DFAB20_ADD_TAGS_TRM, @n XML Tag: #XML_TAG_AD_DFAB20_ADD_TAGS_TRM
  EMV_CTLS_DATA_TYPE          AdditionalTagsCRD_DFAB21;                               ///< TempUpdate allowed: NO  @n Description:
                                                                                      ///< @n Vertex: Additional ICC tags for special applications. e.g. domestic cards with additional tags in ReadRecords, fleetcards with additional customer specific tags and others.
                                                                                      ///< @n VFI Reader: Additional tags to be delivered by the VFI Reader in the response ( VFI Reader ATOL)
                                                                                      ///< @n Format: DOL (Tag + maximum Length)
                                                                                      ///< @n restriction: only 1- and 2-byte-tags are supported, no support for 3-byte-tags @n Link to struct: ::EMV_CTLS_DATA_STRUCT
                                                                                      ///< @n For Vertex, length is to be coded in one byte (length value between 1 and 0xF7).
                                                                                      ///< @n For VFI Reader, length is to be coded in BER-TLV length format and can be up to 4 bytes long.
                                                                                      ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB21_1   @n TLV tag #TAG_DFAB21_ADD_TAGS_CRD, @n XML Tag: #XML_TAG_AD_DFAB21_ADD_TAGS_CRD
  char          DefaultApplicationName_DFAB22[16+1];                                  ///< TempUpdate allowed: NO  @n Description: Default application name to be used in case application label (tag 50) and
                                                                                      ///< application preferred name (tag 9F12) are not read from chip
                                                                                      ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB22_1   @n TLV tag #TAG_DFAB22_DEF_APP_NAME, @n XML Tag: #XML_TAG_AD_DFAB22_DEFAULT_APP_NAME
  unsigned InternalKernelId_DFAB23;                                                   ///< TempUpdate allowed: NO  @n Description: Internal Kernel Id from @ref DEF_KERNEL_ID, required if kernel identifier shall match unknown kernel id
                                                                                      ///< @n Availability bit: #INPUT_CTLS_APL_GLOB_DFAB23_1 @n TLV tag #TAG_DFAB23_INTERNAL_KERNEL_ID @n XML Tag: #XML_TAG_AD_DFAB23_INTERNAL_KERNEL_ID

  union {
    EMV_CTLS_APPLIDATA_MK_TYPE MK;  ///< MasterCard PayPass, link to struct: ::EMV_CTLS_APPLIDATA_MK_STRUCT @n XML tag #XML_TAG_AD_MK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_GC_TYPE GC;  ///< Extended MasterCard PayPass (kernel 2a for German debit scheme girocard), same configuration as PayPass for instance @n XML tag #XML_TAG_AD_GIROCARD @n no TLV tag is used
    EMV_CTLS_APPLIDATA_VK_TYPE VK;  ///< Visa payWave, link to struct: ::EMV_CTLS_APPLIDATA_VK_STRUCT @n XML tag #XML_TAG_AD_VK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_AK_TYPE AK;  ///< Amex ExpressPay, link to struct: ::EMV_CTLS_APPLIDATA_AK_STRUCT @n XML tag #XML_TAG_AD_AK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_JK_TYPE JK;  ///< JCB, link to struct: ::EMV_CTLS_APPLIDATA_JK_STRUCT @n XML tag #XML_TAG_AD_JK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_DK_TYPE DK;  ///< Discover resp. Diners, link to struct: ::EMV_CTLS_APPLIDATA_DK_STRUCT @n XML tag #XML_TAG_AD_DK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_IK_TYPE IK;  ///< Interac (Canada), link to struct: ::EMV_CTLS_APPLIDATA_IK_STRUCT @n XML tag #XML_TAG_AD_IK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_EK_TYPE EK;  ///< Epal (Australia), link to struct: ::EMV_CTLS_APPLIDATA_EK_STRUCT @n XML tag #XML_TAG_AD_EK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_PK_TYPE PK;  ///< Visa Asia/Pacific (Wave 2), link to struct: ::EMV_CTLS_APPLIDATA_PK_STRUCT @n XML tag #XML_TAG_AD_PK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_CK_TYPE CK;  ///< China Union Pay (qPBOC, qUICS, UPI global), link to struct: ::EMV_CTLS_APPLIDATA_CK_STRUCT @n XML tag #XML_TAG_AD_CK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_GK_TYPE GK;  ///< Gemalto Pure, link to struct: ::EMV_CTLS_APPLIDATA_GK_STRUCT @n XML tag #XML_TAG_AD_GK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_RK_TYPE RK;  ///< Rupay (India), link to struct: ::EMV_CTLS_APPLIDATA_RK_STRUCT @n XML tag #XML_TAG_AD_RK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_SK_TYPE SK;  ///< SIBS, link to struct: ::EMV_CTLS_APPLIDATA_SK_STRUCT @n XML tag #XML_TAG_AD_SK @n no TLV tag is used
    EMV_CTLS_APPLIDATA_PB_TYPE PB;  ///< PagoBancomat (Italy), link to struct: ::EMV_CTLS_APPLIDATA_PB_STRUCT @n XML tag #XML_TAG_AD_PB @n no TLV tag is used
    EMV_CTLS_APPLIDATA_MR_TYPE MR;  ///< MIR (Russia), link to struct: ::EMV_CTLS_APPLIDATA_MR_STRUCT @n XML tag #XML_TAG_AD_MR @n no TLV tag is used
    EMV_CTLS_APPLIDATA_DOM_TYPE DOM; ///< domestic, EntryPoint only, link to struct: ::EMV_CTLS_APPLIDATA_DOM_STRUCT @n XML tag #XML_TAG_AD_DOM @n no TLV tag is used
  } Scheme;

} EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_TYPE;

/// @defgroup DEF_INPUT_APPLI_GLOBAL Appli data scheme specific - global part - Availability bits
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of the input in EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::IncludedData
/// @{
  /* Byte 1 */
#define  INPUT_CTLS_APL_GLOB_DFAB02_0   0x01  ///< B1b1: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ASI_DFAB02
#define  INPUT_CTLS_APL_GLOB_DFAB03_0   0x02  ///< B1b2: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AppFlowCap_DFAB03
#define  INPUT_CTLS_APL_GLOB_DFAB04_0   0x04  ///< B1b3: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::PriorityApplications_DFAB04
#define  INPUT_CTLS_APL_GLOB_DFAB05_0   0x08  ///< B1b4: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::SpecialTRXConfig_DFAB05
#define  INPUT_CTLS_APL_GLOB_DFAB06_0   0x10  ///< B1b5: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ChksumEntryPoint_DFAB06
#define  INPUT_CTLS_APL_GLOB_DFAB07_0   0x20  ///< B1b6: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ChksumKernel_DFAB07
#define  INPUT_CTLS_APL_GLOB_DFAB08_0   0x40  ///< B1b7: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::RetapFieldOff_DFAB08
  /* Byte 2 */
#define  INPUT_CTLS_APL_GLOB_DFAB20_1   0x01  ///< B2b1: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsTRM_DFAB20
#define  INPUT_CTLS_APL_GLOB_DFAB21_1   0x02  ///< B2b2: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsCRD_DFAB21
#define  INPUT_CTLS_APL_GLOB_DFAB22_1   0x04  ///< B2b3: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::DefaultApplicationName_DFAB22
#define  INPUT_CTLS_APL_GLOB_DFAB23_1   0x08  ///< B2b4: EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::InternalKernelId_DFAB23
/// @}

// ========================================================================================================
// === EMV TRANSACTION DATA ===
// ========================================================================================================

/// @ingroup DEF_FLOW_INPUT
/// @brief Transaction parameters of an EMV transaction
///
/// Part of ::EMV_CTLS_START_STRUCT
typedef struct EMV_CTLS_PAYMENT_STRUCT // EMV_CTLS_PAYMENT_TYPE
{
  unsigned char           Amount[6];                 ///< Transaction amount (will also be used for EMVCo tags 81, 9F3A) @n mandatory @n TLV tag #TAG_9F02_NUM_AMOUNT_AUTH, validity bit: #INPUT_CTLS_SEL_AMOUNT
  unsigned char           CurrencyTrans[2];          ///< Currency code to be used for transaction. Only needed in case of DCC @n TLV tag #TAG_5F2A_TRANS_CURRENCY, validity bit: #INPUT_CTLS_SEL_AMOUNT_CURRENCY
  unsigned char           ExpTrans;                  ///< Currency exponent to be used for transaction. Only needed in case of DCC. @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP, validity bit: #INPUT_CTLS_SEL_CUREXPONENT
  unsigned char           Date[3];                   ///< Transaction date (YYMMDD) @n mandatory @n TLV tag #TAG_9A_TRANS_DATE, validity bit: #INPUT_CTLS_SEL_DATE
  unsigned char           Time[3];                   ///< Transaction time (HHMMSS) @n mandatory @n TLV tag #TAG_9F21_TRANS_TIME, validity bit: #INPUT_CTLS_SEL_TIME
  unsigned char           TransCount[4];             ///< Transaction sequence counter managed by calling application @n mandatory @n TLV tag #TAG_9F41_TRANS_SEQUENCE_NB, validity bit: #INPUT_CTLS_SEL_TXN_COUNTER
  unsigned char           Cashback_Amount[6];        ///< cashback amount, calling application must care that this amount is already included in 9F02 presented to the EMV ADK
                                                     ///< @n dependency to #EMV_CTLS_FLOW_GLOB_CASHBACK_SUPPORT_0 in @ref DEF_FLOW_GLOB @n TLV tag #TAG_9F03_NUM_AMOUNT_OTHER, validity bit: #INPUT_CTLS_SEL_CB_AMOUNT
  unsigned char           Force_Online;              ///< @c BOOL, set Merchant Forced Online-TVR bit if enabled by #EMV_CTLS_FLOW_GLOB_FORCE_ONLINE_1 in @ref DEF_FLOW_GLOB @n Not supported by all payment schemes. @n Validity bit: #INPUT_CTLS_SEL_FORCE_ONLINE @n TLV tag #TAG_DF40_FORCE_ONLINE
  unsigned char           Online_Switch;             ///< @c BOOL, request ARQC from card, but don't set "suspicious bit" @n Not supported by all payment schemes. @n TLV tag #TAG_DF48_ONLINE_SWITCH, validity bit: #INPUT_CTLS_SEL_ONLINE_SWITCH
  unsigned char           uc_AccountType;            ///< 5F57, defines of CommonServices to be used: @c AT_00_DEFAULT, @c AT_10_SAVINGS, @c AT_20_CHEQUE_DEBIT 0x20u, @c AT_30_CREDIT
                                                     ///< @n TLV tag #TAG_5F57_ACCOUNT_TYPE, validity bit: #INPUT_CTLS_SEL_ACCOUNT_TYPE
  EMV_CTLS_TAGLIST_TYPE   Additional_Result_Tags;    ///< @deprecated Use EMV_CTLS_fetchTxnTags() instead.
} EMV_CTLS_PAYMENT_TYPE;   ///< typedef for EMV_CTLS_PAYMENT_STRUCT


/// @ingroup DEF_FLOW_INPUT
/// @brief Application selection parameters of an EMV transaction
///
/// Part of ::EMV_CTLS_START_STRUCT
typedef struct EMV_CTLS_APPS_SELECT_STRUCT // EMV_CTLS_APPS_SELECT_TYPE
{
  EMV_CTLS_APPLI_TYPE     ExcludeEmvAIDs[3];         ///< Exclude EMV AIDs for this transaction dynamically (without removing from the config) @n TLV tag #TAG_EXCLUDE_AID, validity bit #INPUT_CTLS_SEL_EXCLUDE_AID
} EMV_CTLS_APPS_SELECT_TYPE;            ///< typedef for EMV_CTLS_APPS_SELECT_STRUCT


// ========================================================================================================
// === APPLICATION SETUP ===
// ========================================================================================================

/// @defgroup CLTRX_OPTIONS Options for contactless transaction processing
/// @ingroup DEF_FLOW_INPUT
/// @brief Defines for EMV_CTLS_START_TYPE::TxnOptions
/// @{
  // byte 1
#define CLTRXOP_SUPPRESS_ERRMSGBEEP  0x01  ///< B1b1: Suppress beep on error message display
#define CLTRXOP_REPEAT_L1            0x02  ///< B1b2: TRX repeated because of L1 error (same txn is started a 2nd time)
#define CLTRXOP_NO_DOMESTIC_KERNEL   0x04  ///< B1b3: skip local kernel processing for this transaction even if configured
#define CLTRXOP_NO_GLOBAL            0x08  ///< B1b4: skip global EMV processing for this transaction even if configured
#define CLTRXOP_PP_TORNTXN_RESET     0x10  ///< B1b5: reset the torn transaction store if a PayPass card is presented
#define CLTRXOP_AK_DELAYED_AUTH      0x20  ///< B1b6: Amex delayed autorization (Amex 3.1) for environments where no real-time online transaction is possible. Note: For Amex 4 kernel this is controlled by Enhanced Contactless Reader Capabilities B4b7.

#define CLTRXOP_REMOVEALL_BUT_EXCLUDED 0x80  ///< B1b8: Remove all candidates from terminal list except those which are contained in excluded AID list. This allows transactions restricted to one, two or three AIDs without terminal reconfiguration.
// byte 2
#define CLTRXOP_CANDLIST_CALLBACK    0x01  ///< B2b1: Mutual combo candidate list of terminal and card reported for selection and reordering, see #TAG_BF12_CBK_MODIFY_CAND
#define CLTRXOP_APPSELECTED_CALLBACK 0x02  ///< B2b2: callback after the final select for choosing the kernel to be used, only available for velocity kernel, see #TAG_BF0C_CTLS_CBK_APP_SELECTED
#define CLTRXOP_NO_BUL12             0x04  ///< B2b3: SU Bulletin 12 NOT supported (Velocity Entrypoint _NOT_ EMVEP_CFG_BUL12). Obsolete since EP2.0.1.
//#define CLTRXOP_MULTIPLE_MATCH_AID   0x08  ///< B2b4: removed April 3rd. 2013 --> We always request all matches, downsizing managed by ADK (was: Allow multiple matches for each supported AID (Velocity Entrypoint EMVEP_CFG_MUAID))
#define CLTRXOP_NO_LONGEST_AID_MATCH 0x10  ///< B2b5: Do not perform the longest AID match (technical nonsense but needed for a few domestic certifications)
#define CLTRXOP_SPURIOUS_CARD_DETECT 0x20  ///< B2b6: Try to avoid spurious card detections on ADK level (workaround for L1 issues, code provided by platform team), @deprecated by use of VERTEX card reader abstraction layer
#define CLTRXOP_NO_PAN_RETRIEVE_DEF  0x40  ///< B2b7: PayPass: Do NOT Use default configuration for PAN Retrieval Transactions but configured values.
                                           ///< @n For all other schemes EMV ADK does anyhow use configured values for PAN retrieval transactions. @n Not yet supported for VFI reader.
#define CLTRXOP_RND_CALLBACK         0x80  ///< B2b8: Use own random number generator instead of OS one (not recommended but for debug purposes useful), only supported for velocity kernel, see #TAG_BF13_CBK_RND
//byte 3
#define CLTRXOP_OMIT_CHECKSUM_CHECK  0x01  ///< B3b1: VFI-Reader: Dont calculate checksums of configuration files @n Only effective on EMV_CTLS_SetupTransaction()
#define CLTRXOP_STOP_ON_CHKSUM_DIFF  0x02  ///< B3b2: VFI-Reader: abort transaction when checksums of configuration files changed @n Only effective on EMV_CTLS_SetupTransaction()
#define CLTRXOP_NO_AMOUNT_PRECHECK   0x08  ///< B3b4: Disable the amount precheck --> even tap card is requested (Setup successful) if the txn amount is higher than the highest transaction limit of any AID / kernel.
                                           ///< --> This results in terminate or fallabck later on
#define CLTRXOP_L1_ERROR_CALLBACK    0x10  ///< B3b5: Enable notification callback for display "Error, try again" (#EMV_ADK_TXT_RETAP_SAME_L1) in case of L1 error and
                                           ///< "Use 1 card only" (#EMV_ADK_TXT_2_CARDS_IN_FIELD) in case 2 cards are detected @n Precondition: UI scheme "default" is set, use #INPUT_CTLS_TRM_FLOWOPT_UI_SCHEME_DEFAULT
#define CLTRXOP_SETUP_WAIT_FOR_ERROR 0x20  ///< B3b6: VFI-Reader: EMV_CTLS_SetupTransaction() waits 10 ms for a negative answer from VFI-Reader so in case of VFI-Reader having problems,
                                           ///< this is already detected in EMV_CTLS_SetupTransaction() rather than in EMV_CTLS_ContinueOffline().
#define CLTRXOP_WEEK_PRIORITY_APPS   0x40  ///< B3b7: Ignore priority applications that are not part of the terminal candidate list
#define CLTRXOP_PRELOAD_TRANSACTION  0x80  ///< B3b8: Preload feature: perform preliminary process and prepare database for payment without starting the transaction. Required for VAS processing using NFC ADK on VFI reader.
//byte 4
#define CLTRXOP_KEEP_MULTIPLE_KERNEL 0x01  ///< B4b1: After Final Select keep all AID-kernel combinations for the selected AID
#define CLTRXOP_TIP_AMOUNT_ZERO      0x02  ///< B4b2: Card holder confirmed zero amount within tip amount input dialog. In case of TIP setting DF22 position 4 = 3 the the transaction shall not be made suitable for a tip follow-up.
#define CLTRXOP_SILENT_ON_EMPTY_LIST 0x04  ///< B4b3: In case of empty candidate list (return code @ref EMV_ADK_TXN_CTLS_EMPTY_LIST) suppress error tone and LED processing (LEDs off after 750ms).
#define CLTRXOP_CVM_REQUIRED         0x08  ///< B4b4: Force CVM for this transaction due to a special business use case. Basically implemented by internally setting the CVM required limits to zero.
//byte 5
/// @}

/// @defgroup CLTRX_SCHEME_TRX_TYPES Scheme specific transaction types
/// @ingroup DEF_FLOW_INPUT
/// @brief Defines for EMV_CTLS_START_TYPE::TransType
/// @{
#define EMV_CTLS_TRAN_TYPE_PURE_GETDATA         0x78  ///< Gemalto/Pure: Retrieval of application proprietary data elements using GET DATA command, needs to be activated by means of @ref DEF_FLOW_GK
#define EMV_CTLS_TRAN_TYPE_PURE_PUTDATA         0x79  ///< Gemalto/Pure: Update of application data proprietary elements using PUT DATA command, needs to be activated by means of @ref DEF_FLOW_GK
#define EMV_CTLS_TRAN_TYPE_PURE_AUTHENTICATE    0x90  ///< Gemalto/Pure: Application Authentication Transaction: Special type of transaction allowing the terminal to only authenticate the contactless device,
                                                      ///< needs to be activated by means of @ref DEF_FLOW_GK
/// @}

/// @defgroup CLTRX_PASSTROUGH CTLS card types for which passtrough mode is set
/// @ingroup DEF_FLOW_INPUT
/// @brief Defines for EMV_CTLS_START_TYPE::passthroughCardTypes
/// @{
#define CLTRX_PASSTHROUGH_OFF        0x00  ///< no passthrough mode (default)
#define CLTRX_PASSTHROUGH_NONISO     0x01  ///< goto passtrough mode for non ISO 14443 cards (e.g. Mifare)
#define CLTRX_PASSTHROUGH_ALL        0x02  ///< goto passtrough mode for all cards
/// @}

/// @defgroup CLTRX_CARDTYPE CTLS card types
/// @ingroup DEF_FLOW_OUTPUT
/// @brief Defines for EMV_CTLS_TRANSRES_TYPE::ctlsCardType
/// @{
#define CLTRX_CARDTYPE_ISO           0x01  ///< ISO 14443A,B
#define CLTRX_CARDTYPE_MIFARE        0x02  ///< Mifare
#define CLTRX_CARDTYPE_OTHER         0x03  ///< other contactless card
/// @}

/// @ingroup DEF_FLOW_INPUT
/// @brief struct for interface to EMV_CTLS_SetupTransaction()
typedef struct EMV_CTLS_START_STRUCT
{
  unsigned char             ServerPollTimeout;         ///< Timeout in seconds for server calling EMV_CTLS_ContinueOffline. If set to 0, server does not call EMV_CTLS_ContinueOffline on its own.
                                                       ///< Do not set != 0 if ADK-TEC is used, in this case ADK-TEC takes care of polling.
                                                       ///< @n PLEASE DO NOT SET the server POLL Timeout if you use TECSEL (ADK technology selection).
                                                       ///< In this case TECSEL will take care about polling CTLS events and you MUST NOT use the alternate feature provided here.
                                                       ///< @n TLV tag #TAG_C9_POLL_TIMEOUT, validity bit #INPUT_CTLS_SEL_POLL_TIMEOUT.
  unsigned char             TransType;                 ///< Transaction type, according to ISO 8583 - Annex A: Processing Code, Position 1 + 2 @n see @ref TRANS_TYPES and @ref CLTRX_SCHEME_TRX_TYPES
                                                       ///< @n mandatory @n TLV tag #TAG_9C_TRANS_TYPE, validity bit #INPUT_CTLS_SEL_TTYPE
  EMV_CTLS_PAYMENT_TYPE     TXN_Data;                  ///< See ::EMV_CTLS_PAYMENT_STRUCT. The transaction data can be presented at the beginning of the transaction or (if not yet available) later, e.g. after the final select.
                                                       ///< Anyhow data should be presented as soon as available. @n no TLV tag for this struct, elements are serialized one-by-one
  EMV_CTLS_APPS_SELECT_TYPE SEL_Data;                  ///< See ::EMV_CTLS_APPS_SELECT_STRUCT. Data for application selection process @n no TLV tag for this struct, elements are serialized one-by-one
  unsigned char             TxnOptions[5];             ///< Transaction options, allowed values see @ref CLTRX_OPTIONS @n TLV tag #TAG_DF36_TRX_OPTIONS, validity bit #INPUT_CTLS_SEL_TXN_OPTIONS
  unsigned char             passthroughCardTypes;      ///< CTLS card types for which passtrough mode is set (VFI reader only), see @ref CLTRX_PASSTROUGH
                                                       ///< @n EMV_CTLS_ContinueOffline() will return #EMV_ADK_CONTINUE. Then application can fetch #TAG_DF8F20_CTLS_CARD_TPYE with help of EMV_CTLS_fetchTxnTags()
                                                       ///< @n TLV tag #TAG_DF4E_PASSTROUGH, validity bit #INPUT_CTLS_SEL_PASSTHROUGH
  unsigned char             Info_Included_Data[8];     ///< Which data is included in the message
} EMV_CTLS_START_TYPE;


/// @defgroup DEF_INPUT_SELECT EMV_CTLS_SetupTransaction: Which data is included in input struct
/// @ingroup DEF_FLOW_INPUT
/// @brief Contents of the input in EMV_CTLS_START_STRUCT::Info_Included_Data, EMV_CTLS_SetupTransaction()
/// @{
/* Byte 1 */
#define  INPUT_CTLS_SEL_AMOUNT              0x01  ///< B1b1: EMV_CTLS_PAYMENT_STRUCT::Amount
#define  INPUT_CTLS_SEL_AMOUNT_CURRENCY     0x02  ///< B1b2: EMV_CTLS_PAYMENT_STRUCT::CurrencyTrans
#define  INPUT_CTLS_SEL_CUREXPONENT         0x04  ///< B1b3: EMV_CTLS_PAYMENT_STRUCT::ExpTrans
#define  INPUT_CTLS_SEL_DATE                0x08  ///< B1b4: EMV_CTLS_PAYMENT_STRUCT::Date
#define  INPUT_CTLS_SEL_TIME                0x10  ///< B1b5: EMV_CTLS_PAYMENT_STRUCT::Time
#define  INPUT_CTLS_SEL_TTYPE               0x20  ///< B1b6: EMV_CTLS_START_STRUCT::TransType
#define  INPUT_CTLS_SEL_EXCLUDE_AID         0x40  ///< B1b7: EMV_CTLS_APPS_SELECT_STRUCT::ExcludeEmvAIDs
#define  INPUT_CTLS_SEL_FORCE_ONLINE        0x80  ///< B1b8: EMV_CTLS_PAYMENT_STRUCT::Force_Online
/* Byte 2 */
#define  INPUT_CTLS_SEL_ONLINE_SWITCH       0x01  ///< B2b1: EMV_CTLS_PAYMENT_STRUCT::Online_Switch
#define  INPUT_CTLS_SEL_TXN_COUNTER         0x02  ///< B2b2: EMV_CTLS_PAYMENT_STRUCT::TransCount
#define  INPUT_CTLS_SEL_CB_AMOUNT           0x04  ///< B2b3: EMV_CTLS_PAYMENT_STRUCT::Cashback_Amount
#define  INPUT_CTLS_SEL_ACCOUNT_TYPE        0x08  ///< B2b4: EMV_CTLS_PAYMENT_STRUCT::uc_AccountType
#define  INPUT_CTLS_SEL_TXN_OPTIONS         0x10  ///< B2b5: EMV_CTLS_START_STRUCT::TxnOptions
#define  INPUT_CTLS_SEL_POLL_TIMEOUT        0x20  ///< B2b6: EMV_CTLS_START_STRUCT::ServerPollTimeout
#define  INPUT_CTLS_SEL_PASSTHROUGH         0x40  ///< B2b7: EMV_CTLS_START_STRUCT::passthroughCardTypes
#define  INPUT_CTLS_SEL_ADD_TAGS            0x80  ///< B2b8: @deprecated Don't use EMV_CTLS_PAYMENT_STRUCT::Additional_Result_Tags anymore. Use EMV_CTLS_fetchTxnTags() instead.
/// @}

/// @defgroup DEF_SELECTRES_TRXINFO Bitstring with information about the outcome of EMV_CTLS_SetupTransaction()
/// @ingroup DEF_FLOW_OUTPUT
/// @brief Contents of EMV_CTLS_STARTRES_STRUCT::TxnInformation, EMV_CTLS_SetupTransaction()
/// @{
/* Byte 1 */
#define  EMV_CTLS_SETUPTRX_RES_CHKSUM_DIFF  0x01  ///< B1b1: Checksum of configuration files changed, call of EMV_CTLS_ApplyConfiguration() may be needed
/// @}

/// @defgroup DEF_OUTPUT_SELECT EMV_CTLS_SetupTransaction: Which data is included in result struct
/// @ingroup DEF_FLOW_OUTPUT
/// @brief Contents of the output in EMV_CTLS_STARTRES_STRUCT::T_DF61_Info_Received_Data, EMV_CTLS_SetupTransaction()
/// @{
/* Byte 1 */
#define  OUTPUT_CTLS_SEL_TXN_INFO  0x01  ///< B1b1: EMV_CTLS_STARTRES_STRUCT::TxnInformation
/// @}

/// @ingroup DEF_FLOW_OUTPUT
/// @brief Result data of EMV_CTLS_SetupTransaction()
typedef struct EMV_CTLS_STARTRES_STRUCT
{
    unsigned char  TxnInformation[5];             ///< Bitstring with information about the outcome of EMV_CTLS_SetupTransaction(), see @ref DEF_SELECTRES_TRXINFO
                                                  ///< @n Available flag #OUTPUT_CTLS_SEL_TXN_INFO @n TLV tag #TAG_DF8F0D_SETUP_RES_TRXINFO
    unsigned char  T_DF61_Info_Received_Data[8];  ///< Which data is valid, see @ref DEF_OUTPUT_SELECT
} EMV_CTLS_STARTRES_TYPE;



// ========================================================================================================
// === TRANSACTION EXECUTION ===
// ========================================================================================================

/// @ingroup DEF_FLOW_INPUT
/// @brief Data structure for host response data. Input for EMV_CTLS_ContinueOnline()
typedef struct EMV_CTLS_HOST_STRUCT
{
  char             OnlineResult;           ///< @brief Shows whether or not an online dialogue was successful.  If there is no connection or the message MAC is wrong or the response contains a format error,
                                           ///< then @c FALSE must be entered.  The kernel then performs a TAC-IAC-Default check and the second GenerateAC.
                                           ///< Other data is only then relevant, when the online dialogue was successful (value @c TRUE). @n default: @c FALSE
                                           ///< @n TLV tag #TAG_DF50_ONL_RES, availability bit #INPUT_CTLS_ONL_ONLINE_RESP
  unsigned char    AuthResp[2];            ///< @brief Authorisation Response Code. The response code from the host.  Note:  The format must be converted from numeric to alphanumeric @n default: 0000
                                           ///< @n TLV tag #TAG_8A_AUTH_RESP_CODE, availability bit #INPUT_CTLS_ONL_AUTH_RESP
  unsigned char    TxnOptions[5];          ///< Bitstring for options. @n See @ref CLTRX_OPTIONS @n TLV tag #TAG_DF36_TRX_OPTIONS, availability bit #INPUT_CTLS_ONL_TXN_OPTIONS
  unsigned short   LenScriptData;          ///< Length of @c ScriptData
  const unsigned char* ScriptData;         ///< @brief Issuer Script Template 1 or 2 (scripts to be performed after online authorisation) @n EMVCo tag 71/72
                                           ///< @n TLV tag #TAG_DF54_SCRIPT_CLESS, availability bit #INPUT_CTLS_ONL_SCRIPT
  unsigned char        LenAuth;            ///< Length of @c AuthData
  const unsigned char* AuthData;           ///< Issuer Authentication Data (EMVCo tag 91) @n "91xx" must be included @n Availability bit: #INPUT_CTLS_ONL_AUTHDATA @n TLV length for #TAG_DF52_AUTH_DATA_CLESS
  unsigned char    AuthResp_Positive[2];   ///< @brief Additional host AC which is considered positive (a TC is returned or is requested on 2nd GAC according to the scheme),
                                           ///< the parameter may vary per transaction (if different schemes use different alternative positive response codes)
                                           ///< @n TLV tag #TAG_DF57_AC_ADD_OK_CLESS @n Availability bit: #INPUT_CTLS_ONL_ARC_POSITIVE
  unsigned char AuthResp_SwitchInterface[2]; ///< @brief PSD2 - Strong Consumer Authentication - Response Code for switch interface.
                                             ///< @n If #AuthResp has this value, it is checked if contact interface is supported by card and terminal (return code #EMV_ADK_FALLBACK_CHIP_ONLY).
                                             ///< Otherwise the transaction will be declined (return code #EMV_ADK_AAC). For consumer devices supporting CD CVM and if desired by the acquirer (e.g. Visa), the
                                             ///< transaction shall be restarted with forced CD CVM (return code #EMV_ADK_CTLS_RETAP_SAME). In this case please restart EMV_CTLS_SetupTransaction()
                                             ///< and transaction flow option #CLTRXOP_CVM_REQUIRED.
                                             ///< @n TLV tag #TAG_DF55_AC_SWITCH_IF @n Availability bit: #INPUT_CTLS_ONL_ARC_SWITCH_IF
  unsigned char AuthResp_OnlinePIN[2];     ///< @brief PSD2 - Strong Consumer Authentication - Response Code for Online PIN
                                           ///< @n If #AuthResp has this value, the framework performs applicable checks for Online PIN support (return code #EMV_ADK_ARQC with flag #EMV_ADK_SI_ONLINE_PIN_REQUIRED set in #EMV_CTLS_TRANSRES_STRUCT::StatusInfo).
                                           ///< Note: No checks for Mastercard and just the terminal support for Visa because the issuer will do the preliminary checks. If Online PIN is not supported the switch interface processing is performed like #AuthResp_SwitchInterface would have matched.
                                           ///< @n TLV tag #TAG_DF56_AC_ONLINE_PIN @n Availability bit: #INPUT_CTLS_ONL_ARC_ONLINE_PIN
  unsigned char    Info_Included_Data[8];  ///< Which data is included in the message
} EMV_CTLS_HOST_TYPE;


/// @defgroup DEF_INPUT_ONLINE EMV_CTLS_ContinueOnline: Which data is included
/// @ingroup DEF_FLOW_INPUT
/// @brief Contents of the input in EMV_CTLS_HOST_STRUCT::Info_Included_Data, EMV_CTLS_ContinueOnline()
/// @{
/* Byte 1 */
#define  INPUT_CTLS_ONL_ONLINE_RESP         0x01  ///< B1b1: EMV_CTLS_HOST_STRUCT::OnlineResult
#define  INPUT_CTLS_ONL_AUTH_RESP           0x02  ///< B1b2: EMV_CTLS_HOST_STRUCT::AuthResp
#define  INPUT_CTLS_ONL_TXN_OPTIONS         0x04  ///< B1b3: EMV_CTLS_HOST_STRUCT::TxnOptions
#define  INPUT_CTLS_ONL_SCRIPT              0x08  ///< B1b4: EMV_CTLS_HOST_STRUCT::ScriptData
#define  INPUT_CTLS_ONL_AUTHDATA            0x10  ///< B1b5: EMV_CTLS_HOST_STRUCT::AuthData
#define  INPUT_CTLS_ONL_ARC_POSITIVE        0x20  ///< B1b6: EMV_CTLS_HOST_STRUCT::AuthResp_Positive
#define  INPUT_CTLS_ONL_ARC_SWITCH_IF       0x40  ///< B1b7: EMV_CTLS_HOST_STRUCT::AuthResp_SwitchInterface
#define  INPUT_CTLS_ONL_ARC_ONLINE_PIN      0x80  ///< B1b8: EMV_CTLS_HOST_STRUCT::AuthResp_OnlinePIN
/// @}


#define EMV_ADK_CL_SCRIPTRES_LEN (EMV_ADK_SCRIPT_RESULT_MAX * EMV_ADK_SCRIPT_RESULT_LEN + 6) ///< Max. length of data in EMV_CTLS_TRANSRES_STRUCT::ScriptResults (+6 for TLV format, not yet supported)

/// @ingroup DEF_FLOW_INPUT
/// @brief Transaction parameters of an EMV transaction at EMV_CTLS_ContinueOfflineExt()
///
typedef struct EMV_CTLS_CONT_OFFL_STRUCT
{
    unsigned long options; ///< see @ref CL_CONT_OFFL_OPTIONS, availability bit #INPUT_CTLS_CONT_OFFL_OPTIONS, TLV tag: #TAG_DF3B_PARAMETER_1
    unsigned char Info_Included_Data[8];  ///< Which data is included in the message
} EMV_CTLS_CONT_OFFL_TYPE;

/// @defgroup DEF_FLOW_CONTINUE_OFFLINE_INPUT EMV_CTLS_ContinueOfflineExt(): Which data is included
/// @ingroup DEF_FLOW_INPUT
// @brief Contents of the input in EMV_CTLS_CONT_OFFL_STRUCT::Info_Included_Data, EMV_CTLS_ContinueOfflineExt()
#define INPUT_CTLS_CONT_OFFL_OPTIONS        0x01 ///< enables EMV_CTLS_CONT_OFFL_STRUCT::options


/// @defgroup CL_CONT_OFFL_OPTIONS Options for contactless transaction processing at EMV_CTLS_ContinueOfflineExt()
/// @ingroup DEF_FLOW_INPUT
/// @brief Defines for EMV_CTLS_CONT_OFFL_STRUCT::options
/// @{
#define INPUT_CTLS_CONT_OFFL_OPT_PIN                 0x01  ///< B1b1: Restart for Offline PIN verification (for girocard)
#define INPUT_CTLS_CONT_OFFL_OPT_PIN_CANCELLED       0x02  ///< B1b2-3 = 1: PIN input cancelled (for Epal Post TAA after Online PIN)
#define INPUT_CTLS_CONT_OFFL_OPT_PIN_BYPASSED        0x04  ///< B1b2-3 = 2: PIN bypassed (for Epal Post TAA after Online PIN)
#define INPUT_CTLS_CONT_OFFL_OPT_PIN_ERROR           0x06  ///< B1b2-3 = 3: PIN pad not present or not working (for Epal Post TAA after Online PIN)
/// @}

/// @ingroup DEF_FLOW_OUTPUT
/// @brief Data structure for output data of EMV_CTLS_ContinueOffline() and EMV_CTLS_ContinueOnline()
///
/// Valid data is indicated by @ref DEF_DF61
typedef struct EMV_CTLS_TRANSRES_STRUCT // EMV_CTLS_TRANSRES_TYPE
{
  unsigned long         StatusInfo;                          ///< Common status info (see @ref STATUS_INFO) @n TLV tag #TAG_DF42_STATUS, validity bit #TRX_CTLS_STATUSINFO
  unsigned char         T_9F27_CryptInfo;                    ///< Cryptogram information data (CID) @n TLV tag #TAG_9F27_CRYPT_INFO_DATA, validity bit #TRX_CTLS_9F27_CRYPTINFO
  unsigned char         T_9F36_ATC[2];                       ///< Application transaction counter (ATC), source: ICC @n TLV tag #TAG_9F36_ATC, validity bit #TRX_CTLS_9F36_ATC
  unsigned char         T_9F26_Cryptogramm[8];               ///< Application cryptogram @n TLV tag #TAG_9F26_APP_CRYPTOGRAM, validity bit #TRX_CTLS_9F26_CRYPTOGRAMM
  unsigned char         T_5A_PAN[10];                        ///< Application Primary Account Number (PAN) @n TLV tag #TAG_5A_APP_PAN, validity bit #TRX_CTLS_5A_PAN
  unsigned char         T_9F39_POSEntryMode;                 ///< POS entry mode, see ISO 8583 @n TLV tag #TAG_9F39_POS_ENTRY_MODE, validity bit #TRX_CTLS_9F39_POS_ENTRY_MODE
  unsigned char         T_5F24_AppExpDate[3];                ///< Application Expiration Date @n TLV tag #TAG_5F24_APP_EXP_DATE, validity bit #TRX_CTLS_5F24_APPEXPDATE
  unsigned char         T_9F41_TransCount[4];                ///< Transaction Sequence Counter @n Transaction Flow just gives back what was configured by EMV_CTLS_PAYMENT_STRUCT::TransCount
                                                             ///< @n TLV tag #TAG_9F41_TRANS_SEQUENCE_NB, validity bit #TRX_CTLS_9F41_TRANSCOUNT
  unsigned char         T_5F34_PANSequenceNo[1];             ///< PAN sequence number @n TLV tag #TAG_5F34_PAN_SEQUENCE_NB, validity bit #TRX_CTLS_5F34_PAN_SEQ_NUMBER
  EMV_CTLS_TRACK2_TYPE  T_57_DataTrack2;                     ///< Track 2 Equivalent Data (this is chip equivalent data not CTLS magstripe data!) @n TLV tag #TAG_57_TRACK2_EQUIVALENT, validity bit #TRX_CTLS_57_DATA_TRACK2
  EMV_CTLS_ISSDATA_TYPE T_9F10_DataIssuer;                   ///< Issuer Application Data @n TLV tag #TAG_9F10_ISS_APP_DATA, validity bit #TRX_CTLS_9F10_DATAISSUER
  unsigned char         T_9F37_RandomNumber[4];              ///< Unpredictable Number @n TLV tag #TAG_9F37_UNPREDICTABLE_NB, validity bit #TRX_CTLS_9F37_RANDOM_NUMBER
  unsigned char         T_95_TVR[5];                         ///< Terminal Verification Results @n TLV tag #TAG_95_TVR, validity bit #TRX_CTLS_95_TVR
  unsigned char         T_9A_Date[3];                        ///< Transaction Date @n TLV tag #TAG_9A_TRANS_DATE, validity bit #TRX_CTLS_9A_DATE
  unsigned char         T_9F21_Time[3];                      ///< Transaction Time @n TLV tag #TAG_9F21_TRANS_TIME, validity bit #TRX_CTLS_9F21_TIME
  unsigned char         T_9C_TransType;                      ///< @brief Transaction Type
                                                             ///< @n see @ref TRANS_TYPES
                                                             ///< @n EMVCo value range, used on ICC interface, for cryptogram calculation, and as it has to be sent to the host
                                                             ///< @n May be different to input from EMV_CTLS_START_STRUCT::TransType as values greater #EMV_ADK_TRAN_TYPE_INTERNAL_LIMIT are mapped to #EMV_ADK_TRAN_TYPE_GOODS_SERVICE
                                                             ///< @n TLV tag #TAG_9C_TRANS_TYPE @n Availability bit #TRX_CTLS_9C_TRANSTYPE
  unsigned char         T_5F2A_CurrencyTrans[2];             ///< Transaction Currency Code (ISO 4217) @n TLV tag #TAG_5F2A_TRANS_CURRENCY, validity bit #TRX_CTLS_5F2A_TRANS_CURRENCY
  unsigned char         T_82_AIP[2];                         ///< Application Interchange Profile @n TLV tag #TAG_82_AIP, validity bit #TRX_CTLS_82_AIP
  unsigned char         T_9F1A_TermCountryCode[2];           ///< Terminal Country Code @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, validity bit #TRX_CTLS_9F1A_TERM_COUNTRY_CODE
  unsigned char         T_9F34_CVM_Res[3];                   ///< CVM Results @n TLV tag #TAG_9F34_CVM_RESULTS, validity bit #TRX_CTLS_9F34_CVM_RES
  unsigned char         T_9F33_TermCap[3];                   ///< Terminal Capabilities (as configured by EMV_CTLS_APPLIDATA_STRUCT::TermCap @n TLV tag #TAG_9F33_TRM_CAPABILITIES, validity bit #TRX_CTLS_9F33_TERMCAP
  unsigned char         T_9F35_TermTyp;                      ///< Terminal Type, see @ref TERM_TYPES @n TLV tag #TAG_9F35_TRM_TYPE, validity bit #TRX_CTLS_9F35_TERMTYP
  unsigned char         T_9F1E_IFDSerialNumber[8];           ///< Interface Device (IFD) serial number @n TLV tag #TAG_9F1E_IFD_SERIAL_NB, validity bit #TRX_CTLS_9F1E_IFDSERIALNUMBER
  EMV_CTLS_APPLI_TYPE   T_84_DFName;                         ///< Dedicated File (DF) Name @n TLV tag #TAG_84_DF_NAME, validity bit #TRX_CTLS_84_DFNAME
  unsigned char         T_9F09_VerNum[2];                    ///< Application Version Number @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB, validity bit #TRX_CTLS_9F09_VERNUM
  unsigned char         T_9B_TSI[2];                         ///< Transaction Status Information @n TLV tag #TAG_9B_TSI, validity bit #TRX_CTLS_9B_TSI
  EMV_CTLS_APPLI_TYPE   T_9F06_AID;                          ///< Application Identifier (AID) - terminal @n TLV tag #TAG_9F06_AID, validity bit #TRX_CTLS_9F06_AID
  unsigned char         Add_TXN_Tags[EMV_ADK_ADD_TAG_SIZE];  ///< @deprecated Use EMV_CTLS_fetchTxnTags() instead.
  unsigned char         T_9F02_TXNAmount[6];                 ///< Transaction Amount @n TLV tag #TAG_9F02_NUM_AMOUNT_AUTH, validity bit #TRX_CTLS_9F02_AMOUNT
  unsigned char         T_9F03_TXNAdditionalAmount[6];       ///< Transaction Additional (Cashback) Amount @n TLV tag #TAG_9F03_NUM_AMOUNT_OTHER, validity bit #TRX_CTLS_9F03_CB_AMOUNT
  unsigned char         T_9F53_MC_CatCode[1];                ///< Mastercard Transaction Category Code @n TLV tag #TAG_9F53_TRANS_CATEGORY_CODE, validity bit #TRX_CTLS_9F53_MC_CATCODE
  unsigned char         AppName[16+1];                       ///< application name, zero terminated (tag 9F12, tag 50, or configured default EMV_CTLS_APPLIDATA_STRUCT::AppName)
                                                             ///< @n TLV tag #TAG_9F12_APP_PREFERRED_NAME, validity bit #TRX_CTLS_APPNAME
  unsigned char         T_5F25_AppEffDate[3];                ///< Application Effective Date @n TLV tag #TAG_5F25_APP_EFF_DATE, validity bit #TRX_CTLS_5F25_APPEFFDATE
  unsigned char         T_5F28_IssCountryCode[2];            ///< Issuer Country Code @n TLV tag #TAG_5F28_ISS_COUNTRY_CODE, validity bit #TRX_CTLS_5F28_ISSCOUNTRYCODE
  unsigned char         T_9F45_DataAuthCode[2];              ///< Data Authentication Code, optional: present if SDA card @n TLV tag #TAG_9F45_DATA_AUTHENT_CODE, validity bit #TRX_CTLS_9F45_DATAAUTHCODE
  EMV_CTLS_ICCRND_TYPE  T_9F4C_ICCDynNumber;                 ///< optional: present if DDA/CDA card @n TLV tag #TAG_9F4C_ICC_DYNAMIC_NB, validity bit #TRX_CTLS_9F4C_ICCDYNNUMBER
  unsigned char         TACDenial[5];                        ///< Terminal Action Code - Denial @n TLV tag #TAG_DF21_TAC_DENIAL, validity bit #TRX_CTLS_TAC_DENIAL
  unsigned char         TACOnline[5];                        ///< Terminal Action Code - Online @n TLV tag #TAG_DF22_TAC_ONLINE, validity bit #TRX_CTLS_TAC_ONLINE
  unsigned char         TACDefault[5];                       ///< Terminal Action Code - Default @n TLV tag #TAG_DF23_TAC_DEFAULT, validity bit #TRX_CTLS_TAC_DEFAULT
  unsigned char         T_9F0E_IACDenial[5];                 ///< Issuer Action Code - Denial @n TLV tag #TAG_9F0E_IAC_DENIAL, validity bit #TRX_CTLS_9F0E_IAC_DENIAL
  unsigned char         T_9F0F_IACOnline[5];                 ///< Issuer Action Code - Online @n TLV tag #TAG_9F0F_IAC_ONLINE, validity bit #TRX_CTLS_9F0F_IAC_ONLINE
  unsigned char         T_9F0D_IACDefault[5];                ///< Issuer Action Code - Default @n TLV tag #TAG_9F0D_IAC_DEFAULT, validity bit #TRX_CTLS_9F0D_IAC_DEFAULT
  unsigned char         T_9F40_AddTermCap[5];                ///< Additional Terminal Capabilities (EMV_CTLS_APPLIDATA_STRUCT::TermAddCap) @n TLV tag #TAG_9F40_ADD_TRM_CAP, validity bit #TRX_CTLS_9F40_TERMCAP
  unsigned char         T_9F16_MerchIdent[15];               ///< Merchant Identifier @n TLV tag #TAG_9F16_MERCHANT_ID, validity bit #TRX_CTLS_9F16_MERCHANT_ID
  EMV_CTLS_CRDNAME_TYPE T_5F20_Cardholder;                   ///< Cardholder Name @n TLV tag #TAG_5F20_CARDHOLDER_NAME, validity bit #TRX_CTLS_5F20_CARDHOLDER
  unsigned char         T_5F2D_Lang_Pref[8+1];               ///< Language Preference (zero terminated) @n TLV tag #TAG_5F2D_LANGUAGE, validity bit #TRX_CTLS_5F2D_LANG_PREFERENCE
  unsigned char         T_9F08_ICC_Appli_Vers_No[2];         ///< ICC Application Version Number @n TLV tag #TAG_9F08_ICC_APP_VERSION_NB, validity bit #TRX_CTLS_9F08_ICC_APPLI_VERS_NO
  unsigned char         T_5F36_Trx_Currency_Exp;             ///< Transaction Currency Exponent @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP, validity bit #TRX_CTLS_5F36_TRX_CURRENCY_EXPO
  unsigned char         T_5F30_ServiceCode[2];               ///< Service code as defined in ISO/IEC 7813 for track 1 and track 2, format: n3 @n TLV tag #TAG_5F30_SERVICE_CODE, validity bit #TRX_CTLS_5F30_SERVICE_CODE
  unsigned long         CL_Mode;                             ///< @brief used Contactless mode (kernel) for this transaction @n Possible values: see @ref CL_MODES
                                                             ///< @deprecated Maintained only for backwards compatibility. Kernel is given by EMV_CTLS_TRANSRES_TYPE::T_DF64_KernelDebugData[18] and
                                                             ///< MSR transactions are indicated by EMV_CTLS_TRANSRES_TYPE::StatusInfo bit #EMV_ADK_SI_CONTACTLESS_MSR
                                                             ///< @n TLV tag #TAG_C2_TRM_CL_MODES, availability bit #TRX_CTLS_CTLS_MODES
  unsigned char         T_9F66_CL_TTQ[4];                    ///< Visa TTQ (Contactless only) @n TLV tag #TAG_9F66_TTQ, validity bit #TRX_CTLS_9F66_TTQ
  EMV_CTLS_TRACK2_TYPE  T_DF5D_CL_MAGSTRIPE_T2;              ///< Contactless magstripe data (different tags are used for the schemes (payWave,PayPass,Amex, ...) therefore an own tag is used to identify magstripe data)
                                                             ///< @n TLV tag #TAG_DF5E_CL_MAGSTRIPE_T2, validity bit #TRX_CTLS_DF5D_CL_MAGSTRIPE
  unsigned char         T_9F6C_CL_VISA_CTQ[2];               ///< Visa CTQ (Contactless only) @n TLV tag #TAG_9F6C_VISA_CTQ, validity bit #TRX_CTLS_9F6C_VISA_CTQ
  unsigned char         T_9F5D_CL_VISA_AOSA[EMV_ADK_BCD_AMOUNT_LEN];     ///< Visa contactless: Available Offline Spending Amount (AOSA) @n TLV tag #TAG_9F5D_VISA_AOSA, validity bit #TRX_CTLS_9F5D_VISA_AOSA
  unsigned char         T_DF8104_CL_MC_BALANCE[EMV_ADK_BCD_AMOUNT_LEN];  ///< PayPass: Balance before GenAC @n TLV tag #TAG_DF32_MC_BALANCE_8104, validity bit #TRX_CTLS_DF8104_MC_BALANCE
  unsigned char         T_DF8105_CL_MC_BALANCE[EMV_ADK_BCD_AMOUNT_LEN];  ///< PayPass: Balance after GenAC @n TLV tag #TAG_DF38_MC_BALANCE_8105, validity bit #TRX_CTLS_DF8105_MC_BALANCE
  unsigned char         T_DF64_KernelDebugData[32];               ///< Status data of the used CTLS Vertex kernel. See @ref subsubsection_Debugging. @n TLV tag #TAG_DF64_KERNEL_DEBUG, validity bit #TRX_CTLS_DF64_KERNEL_DEBUG
  unsigned char         ctlsCardType;                             ///< L1 type of contactless card, see @ref CLTRX_CARDTYPE
                                                                  ///< @n VFI reader framework does only fill this parameter in case EMV_CTLS_ContinueOffline() was called with EMV_CTLS_START_STRUCT::passthroughCardTypes
                                                                  ///< @n More detailed card type info can be obtained by usage of EMV_CTLS_fetchTxnTags() with #TAG_DF8F20_CTLS_CARD_TPYE
                                                                  ///< @n TLV tag #TAG_DF4F_CARDTYPE, validity bit #TRX_CTLS_CARDTYPE
  unsigned char         ScriptResults[EMV_ADK_CL_SCRIPTRES_LEN];  ///< Issuer script results @n TLV tag #TAG_DF07_UNCRIT_SCR, validity bit #TRX_CTLS_SCRIPTRESULTS
  unsigned short        ScriptResultsLen;                         ///< Issuer script results @n TLV tag #TAG_DF07_UNCRIT_SCR, validity bit #TRX_CTLS_SCRIPTRESULTS

  unsigned char    T_DF61_Info_Received_Data[8];             ///< Information which of these data was provided by the ICC, see @ref DEF_DF61
} EMV_CTLS_TRANSRES_TYPE;


/// @defgroup DEF_DF61 Information which data was provided by the ICC
/// @ingroup DEF_FLOW_OUTPUT
/// @brief Contents of EMV_CTLS_TRANSRES_STRUCT::T_DF61_Info_Received_Data
/// @{
/* Byte 1 */
#define  TRX_CTLS_STATUSINFO               0x01 ///< B1b1: EMV_CTLS_TRANSRES_STRUCT::StatusInfo
#define  TRX_CTLS_9F27_CRYPTINFO           0x02 ///< B1b2: EMV_CTLS_TRANSRES_STRUCT::T_9F27_CryptInfo
#define  TRX_CTLS_9F36_ATC                 0x04 ///< B1b3: EMV_CTLS_TRANSRES_STRUCT::T_9F36_ATC
#define  TRX_CTLS_9F26_CRYPTOGRAMM         0x08 ///< B1b4: EMV_CTLS_TRANSRES_STRUCT::T_9F26_Cryptogramm
#define  TRX_CTLS_5A_PAN                   0x10 ///< B1b5: EMV_CTLS_TRANSRES_STRUCT::T_5A_PAN
#define  TRX_CTLS_9F39_POS_ENTRY_MODE      0x20 ///< B1b6: EMV_CTLS_TRANSRES_STRUCT::T_9F39_POSEntryMode
#define  TRX_CTLS_5F24_APPEXPDATE          0x40 ///< B1b7: EMV_CTLS_TRANSRES_STRUCT::T_5F24_AppExpDate
#define  TRX_CTLS_9F41_TRANSCOUNT          0x80 ///< B1b8: EMV_CTLS_TRANSRES_STRUCT::T_9F41_TransCount

/* Byte 2 */
#define  TRX_CTLS_5F34_PAN_SEQ_NUMBER      0x01 ///< B2b1: EMV_CTLS_TRANSRES_STRUCT::T_5F34_PANSequenceNo
#define  TRX_CTLS_57_DATA_TRACK2           0x02 ///< B2b2: EMV_CTLS_TRANSRES_STRUCT::T_57_DataTrack2
#define  TRX_CTLS_9F10_DATAISSUER          0x04 ///< B2b3: EMV_CTLS_TRANSRES_STRUCT::T_9F10_DataIssuer
#define  TRX_CTLS_9F37_RANDOM_NUMBER       0x08 ///< B2b4: EMV_CTLS_TRANSRES_STRUCT::T_9F37_RandomNumber
#define  TRX_CTLS_95_TVR                   0x10 ///< B2b5: EMV_CTLS_TRANSRES_STRUCT::T_95_TVR
#define  TRX_CTLS_9A_DATE                  0x20 ///< B2b6: EMV_CTLS_TRANSRES_STRUCT::T_9A_Date
#define  TRX_CTLS_9F21_TIME                0x40 ///< B2b7: EMV_CTLS_TRANSRES_STRUCT::T_9F21_Time
#define  TRX_CTLS_9C_TRANSTYPE             0x80 ///< B2b8: EMV_CTLS_TRANSRES_STRUCT::T_9C_TransType

/* Byte 3 */
#define  TRX_CTLS_5F2A_TRANS_CURRENCY      0x01 ///< B3b1: EMV_CTLS_TRANSRES_STRUCT::T_5F2A_CurrencyTrans
#define  TRX_CTLS_82_AIP                   0x02 ///< B3b2: EMV_CTLS_TRANSRES_STRUCT::T_82_AIP
#define  TRX_CTLS_9F1A_TERM_COUNTRY_CODE   0x04 ///< B3b3: EMV_CTLS_TRANSRES_STRUCT::T_9F1A_TermCountryCode
#define  TRX_CTLS_9F34_CVM_RES             0x08 ///< B3b4: EMV_CTLS_TRANSRES_STRUCT::T_9F34_CVM_Res
#define  TRX_CTLS_9F33_TERMCAP             0x10 ///< B3b5: EMV_CTLS_TRANSRES_STRUCT::T_9F33_TermCap
#define  TRX_CTLS_9F35_TERMTYP             0x20 ///< B3b6: EMV_CTLS_TRANSRES_STRUCT::T_9F35_TermTyp
#define  TRX_CTLS_9F1E_IFDSERIALNUMBER     0x40 ///< B3b7: EMV_CTLS_TRANSRES_STRUCT::T_9F1E_IFDSerialNumber
#define  TRX_CTLS_84_DFNAME                0x80 ///< B3b8: EMV_CTLS_TRANSRES_STRUCT::T_84_DFName

/* Byte 4 */
#define  TRX_CTLS_9F09_VERNUM              0x01 ///< B4b1: EMV_CTLS_TRANSRES_STRUCT::T_9F09_VerNum
#define  TRX_CTLS_9B_TSI                   0x02 ///< B4b2: EMV_CTLS_TRANSRES_STRUCT::T_9B_TSI
#define  TRX_CTLS_9F06_AID                 0x04 ///< B4b3: EMV_CTLS_TRANSRES_STRUCT::T_9F06_AID
#define  TRX_CTLS_ADDITIONAL_TAGS          0x08 ///< B4b4: @deprecated Use EMV_CTLS_fetchTxnTags() instead.
#define  TRX_CTLS_APPNAME                  0x10 ///< B4b5: EMV_CTLS_TRANSRES_STRUCT::AppName
#define  TRX_CTLS_5F25_APPEFFDATE          0x20 ///< B4b6: EMV_CTLS_TRANSRES_STRUCT::T_5F25_AppEffDate
#define  TRX_CTLS_5F28_ISSCOUNTRYCODE      0x40 ///< B4b7: EMV_CTLS_TRANSRES_STRUCT::T_5F28_IssCountryCode
#define  TRX_CTLS_9F45_DATAAUTHCODE        0x80 ///< B4b8: EMV_CTLS_TRANSRES_STRUCT::T_9F45_DataAuthCode

/* Byte 5 */
#define  TRX_CTLS_9F4C_ICCDYNNUMBER        0x01 ///< B5b1: EMV_CTLS_TRANSRES_STRUCT::T_9F4C_ICCDynNumber
#define  TRX_CTLS_TAC_DENIAL               0x02 ///< B5b2: EMV_CTLS_TRANSRES_STRUCT::TACDenial
#define  TRX_CTLS_TAC_ONLINE               0x04 ///< B5b3: EMV_CTLS_TRANSRES_STRUCT::TACOnline
#define  TRX_CTLS_TAC_DEFAULT              0x08 ///< B5b4: EMV_CTLS_TRANSRES_STRUCT::TACDefault
#define  TRX_CTLS_9F0E_IAC_DENIAL          0x10 ///< B5b5: EMV_CTLS_TRANSRES_STRUCT::T_9F0E_IACDenial
#define  TRX_CTLS_9F0F_IAC_ONLINE          0x20 ///< B5b6: EMV_CTLS_TRANSRES_STRUCT::T_9F0F_IACOnline
#define  TRX_CTLS_9F0D_IAC_DEFAULT         0x40 ///< B5b7: EMV_CTLS_TRANSRES_STRUCT::T_9F0D_IACDefault
#define  TRX_CTLS_9F40_TERMCAP             0x80 ///< B5b8: EMV_CTLS_TRANSRES_STRUCT::T_9F40_AddTermCap

/* Byte 6 */
#define  TRX_CTLS_RFU_B6B1                 0x01 ///< B6b1: RFU
#define  TRX_CTLS_9F16_MERCHANT_ID         0x02 ///< B6b2: EMV_CTLS_TRANSRES_STRUCT::T_9F16_MerchIdent
#define  TRX_CTLS_RFU_B6B3                 0x04 ///< B6b3: RFU
#define  TRX_CTLS_5F20_CARDHOLDER          0x08 ///< B6b4: EMV_CTLS_TRANSRES_STRUCT::T_5F20_Cardholder
#define  TRX_CTLS_5F2D_LANG_PREFERENCE     0x10 ///< B6b5: EMV_CTLS_TRANSRES_STRUCT::T_5F2D_Lang_Pref
#define  TRX_CTLS_9F08_ICC_APPLI_VERS_NO   0x20 ///< B6b6: EMV_CTLS_TRANSRES_STRUCT::T_9F08_ICC_Appli_Vers_No
#define  TRX_CTLS_5F36_TRX_CURRENCY_EXPO   0x40 ///< B6b7: EMV_CTLS_TRANSRES_STRUCT::T_5F36_Trx_Currency_Exp
#define  TRX_CTLS_5F30_SERVICE_CODE        0x80 ///< B6b8: EMV_CTLS_TRANSRES_STRUCT::T_5F30_ServiceCode

/* Byte 7 */
#define  TRX_CTLS_CTLS_MODES               0x01 ///< B7b1: EMV_CTLS_TRANSRES_STRUCT::CL_Mode
#define  TRX_CTLS_9F66_TTQ                 0x02 ///< B7b2: EMV_CTLS_TRANSRES_STRUCT::T_9F66_CL_TTQ
#define  TRX_CTLS_DF5D_CL_MAGSTRIPE        0x04 ///< B7b3: EMV_CTLS_TRANSRES_STRUCT::T_DF5D_CL_MAGSTRIPE_T2
#define  TRX_CTLS_9F6C_VISA_CTQ            0x08 ///< B7b4: EMV_CTLS_TRANSRES_STRUCT::T_9F6C_CL_VISA_CTQ
#define  TRX_CTLS_9F5D_VISA_AOSA           0x10 ///< B7b5: EMV_CTLS_TRANSRES_STRUCT::T_9F5D_CL_VISA_AOSA
#define  TRX_CTLS_DF8104_MC_BALANCE        0x20 ///< B7b6: EMV_CTLS_TRANSRES_STRUCT::T_DF8104_CL_MC_BALANCE
#define  TRX_CTLS_DF8105_MC_BALANCE        0x40 ///< B7b7: EMV_CTLS_TRANSRES_STRUCT::T_DF8105_CL_MC_BALANCE
#define  TRX_CTLS_DF64_KERNEL_DEBUG        0x80 ///< B7b8: EMV_CTLS_TRANSRES_STRUCT::T_DF64_KernelDebugData

/* Byte 8 */
#define  TRX_CTLS_9F02_AMOUNT              0x01 ///< B8b1: EMV_CTLS_TRANSRES_STRUCT::T_9F02_TXNAmount
#define  TRX_CTLS_9F03_CB_AMOUNT           0x02 ///< B8b2: EMV_CTLS_TRANSRES_STRUCT::T_9F03_TXNAdditionalAmount
#define  TRX_CTLS_9F53_MC_CATCODE          0x04 ///< B8b3: EMV_CTLS_TRANSRES_STRUCT::T_9F53_MC_CatCode
#define  TRX_CTLS_CARDTYPE                 0x08 ///< B8b4: EMV_CTLS_TRANSRES_STRUCT::ctlsCardType
#define  TRX_CTLS_SCRIPTRESULTS            0x10 ///< B8B5: EMV_CTLS_TRANSRES_STRUCT::ScriptResults
/// @}


/// @defgroup EXIT_FW_OPTIONS Options for Exit Framework
/// @ingroup FUNC_INIT
/// @brief Meaning of #TAG_DF8F0A_EXIT_OPTIONS, see also EMV_CTLS_Exit_Framework_extended()
/// @{
#define  EXIT_CTLS_BASE           0x00  ///< set Framework to base-initialised state.
                                        ///< Free memory
                                        ///< Transactions not possible
#define  EXIT_CTLS_STOP_LEDS      0x01  ///< set Framework to base-initialised state.
                                        ///< additionally stop LED-Blinking-thread when exit
#define  EXIT_CTLS_COMPLETE       0x02  ///< set Framework to uninitialised state.
                                        ///< Led-Blinking
                                        ///< No callbacks will be send after this.
                                        ///< Send last Callback with TAG_BF0F_EXIT_CALLBACK_THREAD
#define  EXIT_CTLS_SEND_CALLBACK  EXIT_CTLS_COMPLETE  ///<@deprecated use #EXIT_CTLS_COMPLETE
/// @}


/// @defgroup END_TXN_OPTIONS Options for End Transaction
/// @ingroup FUNC_INIT
/// @brief Meaning of #TAG_DF8F0A_EXIT_OPTIONS, see also EMV_CTLS_EndTransaction()
/// @{
#define END_TXN_CTLS_NO_LEDS_OFF 0x01  ///< No LEDs off
/// @}


// ========================================================================================================
// === MISCELLANEOUS ===
// ========================================================================================================

#define MAX_CTLS_CONF_KEYS  99 ///< max. number of allowed CAP keys

/// @ingroup DEF_CARD_CONF
/// @brief Data structure CAP keys
///
/// Max. no.: #MAX_CTLS_CONF_KEYS
///
typedef struct EMV_CTLS_CAPKEY_STRUCT
{
  unsigned char *RID;          ///< Registered Identifier @n TLV tag #TAG_KEY_RID @n XML Tag: #XML_TAG_CAP_KEYS_RID
  unsigned char Index;         ///< Index @n TLV tag #TAG_KEY_INDEX @n XML Tag: #XML_TAG_CAP_KEYS_INDEX
  unsigned char *Key;          ///< Actual Key data @n TLV tag #TAG_KEY_KEY @n XML Tag: #XML_TAG_CAP_KEYS_KEY
  unsigned char KeyLen;        ///< length of key in bytes @n XML Tag: #XML_TAG_CAP_KEYS_KEYLEN
  unsigned char Exponent;      ///< Exponent - 01 used for 65537 @n TLV tag #TAG_KEY_EXPONENT @n XML Tag: #XML_TAG_CAP_KEYS_EXPONENT
  unsigned char *Hash;         ///< Hash value of rid, index, key, exponent @n Optional, if not provided it's simply not checked. @n TLV tag #TAG_KEY_HASH @n XML Tag: #XML_TAG_CAP_KEYS_HASH
  unsigned char noOfRevocEntries; ///< No of entries in revocation list
  unsigned char* RevocEntries;    ///< Revocation list, each entry consists of 3 byte serial no. @n TLV tag #TAG_KEY_CRL @n XML Tag: #XML_TAG_CAP_KEYS_REVOC_LIST
} EMV_CTLS_CAPKEY_TYPE;

/// @ingroup DEF_CARD_CONF
/// @brief Data structure CAP keys
typedef struct EMV_CTLS_CAPREAD_STRUCT
{
  unsigned char RID[5];        ///< Registered Identifier @n TLV tag #TAG_KEY_RID
  unsigned char Index;         ///< Index @n TLV tag #TAG_KEY_INDEX
  unsigned char KeyLen;        ///< length of key in bytes @n TLV tag #TAG_KEY_KEYLEN
} EMV_CTLS_CAPREAD_TYPE;

/// @defgroup CBCK_DE_REQUEST Options for DataExchange request
/// @ingroup TLV_CALLBCK
/// @brief Used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE, #TAG_DF8F58_DE_REQUEST
/// @{
#define  EMV_CTLS_DE_REQ_DEK          0x00  ///< DEK request
#define  EMV_CTLS_DE_REQ_DET          0x01  ///< DET request
/// @}

/// @defgroup CBCK_DE_STATE Options for DataExchange state
/// @ingroup TLV_CALLBCK
/// @brief Used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE, #TAG_DF8F59_DE_STATE
/// @{
#define  EMV_CTLS_DE_STATE_0          0x00  ///< DataExchange Inactive
#define  EMV_CTLS_DE_STATE_1          0x01  ///< MSD/Chip - Waiting for PDOL Data
#define  EMV_CTLS_DE_STATE_2          0x02  ///< MSD - Data update prior to RR
#define  EMV_CTLS_DE_STATE_3          0x03  ///< MSD - Waiting for proceed to first write flag
#define  EMV_CTLS_DE_STATE_4          0x04  ///< MSD - Final data to send
#define  EMV_CTLS_DE_STATE_5          0x05  ///< Chip - Update befre/during RR/GD
#define  EMV_CTLS_DE_STATE_6          0x06  ///< Chip - Data update after RR
#define  EMV_CTLS_DE_STATE_7          0x07  ///< Chip - Waiting for proceed to first write flag
#define  EMV_CTLS_DE_STATE_8          0x08  ///< Chip - Final data to send
/// @}

/// @defgroup CBCK_DE_MODE Options for DataExchange mode
/// @ingroup TLV_CALLBCK
/// @brief Used in @ref EMV_CTLS_CALLBACK_FnT, #TAG_BF0E_CBK_DATA_EXCHANGE, #TAG_DF8F5A_DE_MODE
/// @{
#define  EMV_CTLS_DE_MODE_STOP        0x00  ///< User wishes transaction to terminate
#define  EMV_CTLS_DE_MODE_CONT        0x01  ///< User wishes transaction processing to continue from point of interruption
#define  EMV_CTLS_DE_MODE_MORE        0x02  ///< User wishes to supply more data
/// @}

// ========================================================================================================
// === CALLBACK FUNCTION for additional features and apps interaction during the transaction            ===
// === (PIN, Application Selection, DCC, ...)                                                           ===
// ========================================================================================================

/// @defgroup CBCK_FCT_TAGS Constructed tags for callback functions
/// @ingroup TLV_CALLBCK
/// @brief Used in @ref EMV_CTLS_CALLBACK_FnT
///
/// The tag determines which callback function is to be used.
/// Data is enclosed inside this contructed tag.
/// Detailed Description uses following convention:
/// @n <b> INPUT: </b> This is what Application gets from the Framework
/// @n <b> OUTPUT: </b> This is what Application is supposed to send back
/// @{
#define TAG_BF0B_INIT_CALLBACK_THREAD    0xBF0B  ///< @brief Signals the creation of the "notification callback thread"
                                                 ///< @n Data provided by Framework via EMV_CTLS_HandleCallback() is relayed.
                                                 ///< @n When thread is stopped #TAG_BF0F_EXIT_CALLBACK_THREAD is sent.
                                                 ///< @n@n <b> INPUT: </b> ---
                                                 ///< @n <b> OUTPUT: </b> --- (notify, not generated in FW, but in client)
#define TAG_BF0C_CTLS_CBK_APP_SELECTED   0xBF0C  ///< @brief Signals that application is selected
                                                 ///< @n Used Kernel can be changed, only available for velocity, invoked depending on #CLTRXOP_APPSELECTED_CALLBACK.
                                                 ///< @n@n <b> INPUT: </b> AID (tag #TAG_DF04_AID)
                                                 ///< @n@n <b> INPUT: </b> application labels (tag #TAG_50_APP_LABEL)
                                                 ///< @n@n <b> INPUT: </b> application origin index of candidate list (tag #TAG_DF60_VELOCITY_ORIG_IDX)
                                                 ///< @n@n <b> INPUT: </b> application language preferences of candidate list (tag #TAG_5F2D_LANGUAGE)
                                                 ///< @n@n <b> INPUT: </b> application kernel ID (tag #TAG_DFD003_Kernel_ID)
                                                 ///< @n@n <b> INPUT: </b> Processing Result Bitmap of Entry Point (tag #TAG_DF63_VELOCITY_EP_PRB), contains flag for 9F2A presence (see emv_cl.h from Velocity Entry Point)
                                                 ///< @n@n <b> INPUT: </b> FCI Issuer Discretionary Data from Final Select response (tag #TAG_BF0C_FCI_ISS_DISCR)
                                                 ///< @n <b> OUTPUT: </b> 3-byte kernel ID to be used for the transaction (tag #TAG_DF7E_KERNEL_TO_USE), e.g. 0x020000 for PayPass, use 0xFF0000 to abort the transaction, use 0x000000 to force fallback to contact interface
#define TAG_BF0E_CBK_DATA_EXCHANGE       0xBF0E  ///< @brief Callback for PP3 DataExchange
                                                 ///< @n DataExchange can be enabled by #EMV_CTLS_FLOW_MK_ENABLE_DATA_EXCHANGE_0
                                                 ///< @n@n <b> INPUT: </b> #TAG_DF8F58_DE_REQUEST, #TAG_4F_APP_ID, #TAG_DFAB01_KERNEL_ID, #TAG_FF8104_DE_DATA_TO_SEND, #TAG_DF8106_DE_DATA_NEEDED, #TAG_DF8F59_DE_STATE
                                                 ///< @n <b> OUTPUT: </b> #TAG_DF8F5A_DE_MODE, #TAG_FF10_DE_DET_DATA
#define TAG_BF0F_EXIT_CALLBACK_THREAD    0xBF0F  ///< @brief Signals the stop of the "notification callback thread"
                                                 ///< @n This thread was created at #TAG_BF0B_INIT_CALLBACK_THREAD.
                                                 ///< @n@n <b> INPUT: </b> ---
                                                 ///< @n <b> OUTPUT: </b> --- (notify via EMV_CTLS_HandleCallback())
#define TAG_BF10_CTLS_CBK_LEDS           0xBF10  ///< @brief Callback for switching LEDs
                                                 ///< @n Invoked depending on #EMV_CTLS_INIT_OPT_LED_CBK_EXT
                                                 ///< @n Sent by the "notification callback thread".
                                                 ///< @n Not sent over client/server interface.
                                                 ///< @n@n <b> INPUT: </b> State of CTLS LEDs (1-byte-bitstring, e.g. #CONTACTLESS_LED_0), in tag #TAG_C8_LED_STATE
                                                 ///< @n <b> OUTPUT: </b> --- (notify via EMV_CTLS_HandleCallback())
#define TAG_BF12_CBK_MODIFY_CAND         0xBF12  ///< @brief Modify candidate list
                                                 ///< @n Invoked depending on #CLTRXOP_CANDLIST_CALLBACK.
                                                 ///< @n EMV_CTLS_GetCandidateData() may be called inside this callback to fetch additional candidate data.
                                                 ///< @n@n <b> INPUT: </b> All AIDs of candidate list (each in a single tag #TAG_DF04_AID)
                                                 ///< @n@n <b> INPUT: </b> All application labels of candidate list (each in a single tag #TAG_50_APP_LABEL)
                                                 ///< @n@n <b> INPUT: </b> All application origin indices of candidate list (each in a single tag #TAG_DF60_VELOCITY_ORIG_IDX)
                                                 ///< @n@n <b> INPUT: </b> All application language preferences of candidate list (each in a single tag #TAG_5F2D_LANGUAGE)
                                                 ///< @n@n <b> INPUT: </b> All application kernel IDs of candidate list (each in a single tag #TAG_DFD003_Kernel_ID)
                                                 ///< @n@n <b> INPUT: </b> All combination Processing Result Bitmap of Entry Point (tag #TAG_DF63_VELOCITY_EP_PRB), contains flag for 9F2A presence (see emv_cl.h from Velocity Entry Point)
                                                 ///< @n <b> OUTPUT: </b>
                                                 ///< - #TAG_DF7F_CBK_COMM_ERR
                                                 ///< - Reduced list as a list of App Index (#TAG_DF75_CBK_APP_NO), resorting only supported for velocity kernel
                                                 ///< - PostProcessingOption, skip Post Processing if TRUE (#TAG_DF76_CBK_APP_POSTPROC)
#define TAG_BF13_CBK_RND                 0xBF13  ///< @brief Random number provided by the calling application
                                                 ///< @n Usually the EMV ADK resp. L2 kernel uses the OS functionality for getting a random number.
                                                 ///< @n Only for velocity kernel, invoked depending on #CLTRXOP_RND_CALLBACK.
                                                 ///< @n@n <b> INPUT: </b> none
                                                 ///< @n <b> OUTPUT: </b> Random number from app (#TAG_9F37_UNPREDICTABLE_NB)
#define TAG_BF14_CBK_TEXT_DISPLAY        0xBF14  ///< @brief Text display
                                                 ///< @n Example: "See phone for instructions", #EMV_ADK_TXT_SEE_PHONE
                                                 ///< @n Sent by the "notification callback thread".
                                                 ///< @n@n <b> INPUT: </b> text ID to be displayed by calling application (#TAG_BF14_CBK_TEXT_DISPLAY)
                                                 ///< @n <b> OUTPUT: </b> --- (notify via EMV_CTLS_HandleCallback())
#define TAG_BF15_CBK_CARD_TAPPED         0xBF15  ///< @brief Notification that a card was detected
                                                 ///< @n Calling application shall invoke EMV_CTLS_ContinueOffline() to fetch transaction results.
                                                 ///< @n Is only called in polling mode (EMV_CTLS_START_STRUCT::ServerPollTimeout != 0).
                                                 ///< @n@n <b> INPUT: </b> result of internally called EMV_CTLS_ContinueOffline() (see @ref EMV_ADK_INFO, used tag: #TAG_DF42_STATUS)
                                                 ///< @n <b> OUTPUT: </b> --- (notify via EMV_CTLS_HandleCallback())
#define TAG_BF16_CBK_SERVICE_SHORTLIST   0xBF16  ///< @brief RuPay callback before GPO
                                                 ///< @n This allows the application to forward RuPay parameters during the transaction to the card.
                                                 ///< @n "Service Directory" and perform "Service Shortlisting". Service supported both by Card & terminal is selected.
                                                 ///< @n@n <b> INPUT: </b> Card Data - Service Info from Card
                                                 ///< @n <b> OUTPUT: </b> Application Data - Selected Service ID
#define TAG_BF17_CBK_SERVICE_FOR_GENAC   0xBF17  ///< @brief RuPay callback before Generate AC
                                                 ///< @n This allows the application to forward RuPay parameters during the transaction to the card.
                                                 ///< @n
                                                 ///< @n@n <b> INPUT: </b> Card Data - Signature Info from Card
                                                 ///< @n <b> OUTPUT: </b> Application Data - Additional CDOL1 Tags
#define TAG_BF18_CBK_PURE_GET_PUT_DATA   0xBF18  ///< @brief Gemalto/Pure callback to provide GET DATA and/or PUT DATA input
                                                 ///< @n Is invoked after selection of Gemalto application.
                                                 ///< @n Depends on #EMV_CTLS_FLOW_GK_SUPPORT_GETDATA_0 and #EMV_CTLS_FLOW_GK_SUPPORT_PUTDATA_0
                                                 ///< @n@n <b> INPUT: </b> ---
                                                 ///< @n <b> OUTPUT: </b> GET DATA input (MSRT, #TAG_BF71_PURE_GET_DATA_MSRT) and/or PUT DATA input (MSUT, #TAG_BF70_PURE_PUT_DATA_MSUT)
#define TAG_BF19_CTLS_CBK_BEEP           0xBF19  ///< @brief Application shall sound the buzzer
                                                 ///< @n Depends on #EMV_CTLS_INIT_OPT_BEEP_CBK_EXT
                                                 ///< @n@n <b> INPUT: </b> #TAG_CB_BEEP_SCENARIO (e.g. #EMV_CTLS_BEEP_SUCCESS)
                                                 ///< @n <b> OUTPUT: </b> --- (notify via EMV_CTLS_HandleCallback())
#define TAG_BF7F_CTLS_CBK_TRACE          0xBF7F  ///< @brief Callback for Traces
                                                 ///< @n Invoked depending on #EMV_CTLS_INIT_OPT_TRACE
                                                 ///< @n Sent by the "notification callback thread".
                                                 ///< @n@n <b> INPUT: </b> ASCII Trace (#TAG_TRACE)
                                                 ///< @n <b> OUTPUT: </b> --- (notify via EMV_CTLS_HandleCallback())
/// @}


// ========================================================================================================
// === SERIALIZED FUNCTIONAL CONTACTLESS INTERFACE
// ========================================================================================================


#define EMV_CTLS_SER_Init_Framework(dataIn, dataInLen, dataOut, dataOutLen) EMV_CTLS_SER_Init_Framework_Client(EMV_CTLS_FRAMEWORK_VERSION, (dataIn), (dataInLen), (dataOut), (dataOutLen))

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_Init_Framework_Client(const char *version, const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC void EMV_CTLS_SER_Exit_Framework(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_MapVirtualTerminal(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_StoreCAPKey(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_ReadCAPKeys(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_SetTermData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_GetTermData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_SetAppliDataSchemeSpecific(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_GetAppliDataSchemeSpecific(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_SetupTransaction(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_ContinueOffline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_ContinueOnline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_fetchTxnTags(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_EndTransaction(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_TransparentCommand(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_SmartISO(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_SmartReset(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_SmartPowerOff(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_CardRemoval(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_LED_SetMode(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_LED(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_LED_ConfigDesign(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC unsigned char EMV_CTLS_SER_Break(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CLC EMV_ADK_INFO EMV_CTLS_SER_GetCandidateData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

// ========================================================================================================
// === FUNCTIONAL CONTACTLESS INTERFACE                                                                 ===
// ========================================================================================================


/// @defgroup CLINIT_OPTIONS Options at framework initialisation
/// @ingroup FUNC_INIT
/// @brief Flags used in @ref EMV_CTLS_Init_Framework
/// @{
#define EMV_CTLS_INIT_OPT_BASE_INIT       0x00000001   ///< base init: set options and register callback, but do not allocate memory
#define EMV_CTLS_INIT_OPT_L1_DUMP         0x00000002   ///< allocate additional memory for L1 txn dump, each L1 response is available by tags, using the EMV_CTLS_fetchTxnTags()
#define EMV_CTLS_INIT_OPT_AUTO_RETAP      0x00000004   ///< do NOT handle retaps as two separate transactions but handle retaps internally and fire message events only to the app (@ref TAG_BF14_CBK_TEXT_DISPLAY), the retap returncodes (like @li #EMV_ADK_TXN_CTLS_MOBILE) will not appear in this case but processing is handled on ADK level. -- Detailed explanation: -- VFI-Reader: The VFI-reader in general manges the retap for a mobile internally, ALWAYS. There is no chance for the ADK hand over the retap for a moblie transaction to the application. Therefore the EMV_ADK_TXN_CTLS_MOBILE does NEVER appear with VRFI-Reader. -- Velocity: On velovcity the kernel is NEVER handling the retap itnernally. So the ADK can hand it over to the application. Then you would receive EMV_ADK_TXN_CTLS_MOBILE. For compatibility reasons with the VFI-Reader we have introduced that the ADK is handling the mobile retap optionally. This is configurable with this parameter. If you configure this, then also the EMV_ADK_TXN_CTLS_MOBILE is NEVER returned to the application. If you do not confgure it this returncode will appear. -- Only if internal handling of the Velocity or the VFI-Reader takes place, the BF14 callback is issued to allow the application to display something. If the configuration is in that way that the app wants to handle the mobile retap according to its needs, then the BF14 is NEVER invoked. This is mutually exclusive.
#define EMV_CTLS_INIT_OPT_CONFIG_MODE     0x00000008   ///< configuration is only written to files upon ExitFramework and ApplyConfiguration instead of written upon each configuration function -> performance improvement
#define EMV_CTLS_INIT_OPT_USE_DEF_VT_CAPK 0x00000010   ///< Use "EMV_CTLS_Keys.xml" for the CAPKeys of ALL virtual terminals. This option is to be set by the initialization of the virtual terminal 0 only. Once set this feature applies to all virtual terminals.
#define EMV_CTLS_INIT_OPT_DELETE_ALL      0x00000020   ///< delete all existing configuration files using the same virtual terminal
#define EMV_CTLS_INIT_OPT_USE_VFI         0x00000040   ///< option that ensures VFI Reader will be used @deprecated Ignored because combined CTLS framework not continued anymore
#define EMV_CTLS_INIT_OPT_USE_VERTEX      0x00000080   ///< option that ensures VERTEX Kernels will be used @deprecated Ignored because combined CTLS framework not continued anymore
#define EMV_CTLS_INIT_OPT_TRACE           0x00000100   ///< switch on EMV trace
#define EMV_CTLS_INIT_OPT_TRACE_CLT       0x00000200   ///< switch on client trace
#define EMV_CTLS_INIT_OPT_LED_CBK_EXT     0x00000400   ///< request external LED display. Per default the LEDs are displayed in the configured region by using the GUI ADK. It is highly recommended to use this default for speed reasons. It is possible to offer a callback so that LEDs can be managed on apps side by using this callback (@li #TAG_BF10_CTLS_CBK_LEDS).
#define EMV_CTLS_INIT_OPT_TRACE_SYSLOG    0x00000800   ///< use syslog instead of callback for trace. Not possible for Verix.
#define EMV_CTLS_INIT_OPT_LED_VFI_INT     0x00001000   ///< VFI Reader only: request internal LED display. If set, LED handling is done internally by the VFI Reader. If not set, LED display depends on #EMV_CTLS_INIT_OPT_LED_CBK_EXT
#define EMV_CTLS_INIT_OPT_VFI_BUZZER_ADK  0x00004000   ///< VFI Reader only: Buzzer is not done by VFI reader, ADK takes over the beeps. @n see also @ref EMV_CTLS_TERMDATA_STRUCT::BeepVolume
#define EMV_CTLS_INIT_OPT_CALC_CHKSUM     0x00010000   ///< Calculate checksums of configuration files, may lead to setting of result bit #EMV_CTLS_INIT_RES_CHKSUM_DIFF
#define EMV_CTLS_INIT_OPT_NEW_CFG_INTF    0x00020000   ///< Use new enhanced scheme specific config interface for application data
#define EMV_CTLS_INIT_OPT_BEEP_CBK_EXT    0x00040000   ///< Activate beeper callback (#TAG_BF19_CTLS_CBK_BEEP). Per default beeper is controlled by EMV ADK.
#define EMV_CTLS_INIT_OPT_TRACE_ADK_LOG   0x00080000   ///< use ADK-LOG instead of callback for trace
#define EMV_CTLS_INIT_OPT_TIMINGS         0x00100000   ///< measure card and terminal performance. Note: For this feature other trace output is switched off
#define EMV_CTLS_INIT_OPT_SHARED_CONFIG   0x00200000   ///< On VOS/VOS2 use /mnt/flash/etc/config/adkemv for persistence instead of $(HOME)/flash. NOTE: Only tje processes very first EMV_CTLS_Init_Framework decides the shared config mode.
#define EMV_CTLS_INIT_OPT_EPP_MASTER      0x00400000   ///< Generate a Configuration Id for every configuration change at time of persisting.
                                                       ///< Generate a checksum for every persistence file to be aware of changes via file system.
                                                       ///< The configuration id is specific to a Virtual Terminal and returned on Init Framework
                                                       ///< and when the configuration is stored to the file system. It is seen on serial interface only.

#define EMV_CTLS_INIT_VIRT_1              0x01000000   ///< virtual terminal 1: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_2              0x02000000   ///< virtual terminal 2: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_3              0x03000000   ///< virtual terminal 3: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_4              0x04000000   ///< virtual terminal 4: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_5              0x05000000   ///< virtual terminal 5: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_6              0x06000000   ///< virtual terminal 6: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_7              0x07000000   ///< virtual terminal 7: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_8              0x08000000   ///< virtual terminal 8: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_9              0x09000000   ///< virtual terminal 9: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_10             0x0A000000   ///< virtual terminal 10: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_11             0x0B000000   ///< virtual terminal 11: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_12             0x0C000000   ///< virtual terminal 12: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_13             0x0D000000   ///< virtual terminal 13: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_14             0x0E000000   ///< virtual terminal 14: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_15             0x0F000000   ///< virtual terminal 15: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_16             0x10000000   ///< virtual terminal 16: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_17             0x11000000   ///< virtual terminal 17: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_18             0x12000000   ///< virtual terminal 18: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_19             0x13000000   ///< virtual terminal 19: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CTLS_INIT_VIRT_20             0x14000000   ///< virtual terminal 20: configuration and transaction not done with the standard terminal but virtual terminal No X
/// @}

/// @defgroup CLINIT_RESULT_FLAGS Result flags for outcome of framework initialization
/// @ingroup FUNC_INIT
/// @brief Flags used in @ref EMV_CTLS_Init_Framework
/// @{
#define EMV_CTLS_INIT_RES_CHKSUM_DIFF       0x00000001  ///< Checksums of configuration files changed, call of EMV_CTLS_ApplyConfiguration() may be needed @n EMV framework does this check only if #EMV_CTLS_INIT_OPT_CALC_CHKSUM is set. Is only possible on VFI reader.
#define EMV_CTLS_INIT_RES_VFI_READER_MISS   0x00000002  ///< VFI reader usage desired (via #EMV_CTLS_INIT_OPT_USE_VFI), but VFI reader is not installed
#define EMV_CTLS_INIT_RES_ENTRY_POINT_MISS  0x00000004  ///< Vertex usage desired (via #EMV_CTLS_INIT_OPT_USE_VERTEX), but Entry Point is not installed
/// @}

#define EMV_CTLS_Init_Framework(numberOfAIDs, EMV_Callback, externalData, options,ulResult) EMV_CTLS_Init_Framework_Client(EMV_CTLS_FRAMEWORK_VERSION, (numberOfAIDs), (EMV_Callback), (externalData), (options),(ulResult))
///< @ingroup FUNC_INIT
///< @brief Initialize EMV ADK
///<
///< Transport tags used in serialization: #CLA_EMV #INS_INIT #P2_SET
///<
///< @ref anchor_emv_ctls_Initialize_CTLS_EMV_Framework "Explanation in programmers guide"
///<
///< @author GSS R&D Germany
///<
///< @param[in]   numberOfAIDs ... number of AIDs @n TLV tag #TAG_DF3B_PARAMETER_1
///< @param[in]   EMV_Callback ... function pointer for callback (see @ref EMV_CTLS_CALLBACK_FnT) @n no TLV tag, because it's not serialized
///< @param[in]   externalData ... Application data pointer, is transmitted unchanged to callback function @ref EMV_CTLS_CALLBACK_FnT @n no TLV tag, because it's not serialized
///< @param[in]   options      ... flags for framework initialisation, allowed values see @ref CLINIT_OPTIONS @n TLV tag #TAG_DF3C_PARAMETER_2
///< @param[out]  ulResult     ... bitstring with information flags about the outcome, see @ref CLINIT_RESULT_FLAGS @n TLV tag #TAG_DF8F0C_INIT_RESULT_FLAGS. Can be NULL.
///< @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_INTERNAL, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_CTLS_NOT_AVAILABLE, #EMV_ADK_ABORT,
///<         #EMV_ADK_NO_EXEC, #EMV_ADK_SAVE_ERROR, #EMV_ADK_READ_ERROR
///<

/*****************************************************************************
*  EMV_CTLS_Init_Framework_Client
*****************************************************************************/
/// @brief Internal function for initialization
/// @n Don't call direct!
/// @n Instead use #EMV_CTLS_Init_Framework
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_Init_Framework_Client(const char *version, unsigned char numberOfAIDs, EMV_CTLS_CALLBACK_FnT EMV_Callback, void* externalData, unsigned long options,unsigned long *ulResult);

/*****************************************************************************
*  EMV_CTLS_Exit_Framework
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Kernel deactivation
///
/// Transport tags used in serialization: #CLA_EMV #INS_INIT #P2_GET
///
/// @ref anchor_emv_ctls_Initialize_CTLS_EMV_Framework "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @note this function calls #EMV_CTLS_Exit_Framework_extended (0)
///
DLL_CLC void EMV_CTLS_Exit_Framework(void);

/*****************************************************************************
*  EMV_CTLS_Exit_Framework_extended
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Kernel deactivation with options
///
/// Transport tags used in serialization: #CLA_EMV #INS_INIT #P2_GET
///
/// @author GSS R&D Germany
///
/// @param[in]  options  Bitset of @ref EXIT_FW_OPTIONS
///
DLL_CLC void EMV_CTLS_Exit_Framework_extended(unsigned char options);


/*****************************************************************************
*  EMV_CTLS_CLIENT_GetVersion
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Get contactless client version
///
/// @author GSS R&D Germany
///
/// @return Client version as C-string
///
DLL_CLC const char* EMV_CTLS_CLIENT_GetVersion(void);


/*****************************************************************************
*  EMV_CTLS_FRAMEWORK_GetVersion
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Get contactless framework version
///
/// @author GSS R&D Germany
///
/// @b Serialization:
/// @image html TLV_GetFrameworkVersion.jpg
/// Class: #CLA_EMV, Instruction: #INS_GET_VER, P2: #P2_GET
///
/// @return Framework version as C-string
///
DLL_CLC const char* EMV_CTLS_FRAMEWORK_GetVersion(void);


/*****************************************************************************
*  EMV_CTLS_MapVirtualTerminal
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Changes the Virtual Terminal Map
///
/// Explanation in programmers guide: @ref subsec_emv_ctls_virt_term_map
///
/// @author GSS R&D Germany
///
/// @param[in] VirtualTermMapType  mode, see @ref VIRTUALTERMMAP_MODE @n TLV tag: #TAG_DF3B_PARAMETER_1
/// @param[in] TLVSwitchValue      TLV buffer containing switch criteria. Must contain tag #TAG_9C_TRANS_TYPE and/or tag #TAG_5F2A_TRANS_CURRENCY, all other tags are currently ignored. @n Restriction for VFI reader: Only EMVCo transaction types are supported (#EMV_ADK_TRAN_TYPE_GOODS_SERVICE, #EMV_ADK_TRAN_TYPE_CASH, #EMV_ADK_TRAN_TYPE_CASHBACK, #EMV_ADK_TRAN_TYPE_MANUAL_CASH, #EMV_ADK_TRAN_TYPE_REFUND) @n TLV tag: #TAG_DF3C_PARAMETER_2
/// @param[in] TLVBufLen           Length of TLV buffer @n TLV tag: #TAG_DF3C_PARAMETER_2
/// @param[in] VirtualTerminal     Virtual Terminal that is selected if switch criteria match. @n TLV tag: #TAG_DF3D_PARAMETER_3
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_READ_ERROR, #EMV_ADK_SAVE_ERROR
///
/// @b Serialization:
/// Class: #CLA_EMV, Instruction: #INS_VIRT_CFG
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_MapVirtualTerminal(EMV_ADK_VIRTUALTERMMAP_TYPE VirtualTermMapType, unsigned char *TLVSwitchValue, unsigned int TLVBufLen, unsigned char VirtualTerminal);


/*****************************************************************************
*  EMV_CTLS_StoreCAPKey
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Stores the specified CAP key
///
/// Explanation in programmers guide: @ref anchor_emv_ctls_Configure_CTLS_CAP_Keys "Configure CTLS CAP Keys"
///
/// @author GSS R&D Germany
///
/// @param[in] eHandleCAPKeyType  Mode of CAP key setting, see @ref APPLI_CONF_MODE @n TLV tag #TAG_HANDLE_APPLI_TYPE (Note: Multiple records are not supported)
/// @param[in] pxKeyData          All the data of the key that needs storing, see @ref EMV_CTLS_CAPKEY_STRUCT
///
/// @b Serialization:
/// @image html TLV_StoreCAPKey.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_CAPKEY_CFG, P2: #P2_SET
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_READ_ERROR, #EMV_ADK_SAVE_ERROR
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_StoreCAPKey(EMV_ADK_HANDLE_RECORD_TYPE eHandleCAPKeyType, const EMV_CTLS_CAPKEY_TYPE *pxKeyData);


/*****************************************************************************
*  EMV_CTLS_ReadCAPKeys
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Reads CAP keys
///
/// Explanation in programmers guide: @ref anchor_emv_ctls_Configure_CTLS_CAP_Keys "Configure CTLS CAP Keys"
///
/// @author GSS R&D Germany
///
/// @param[out] pxKeyData        CAP key data, max(*pucMaxnum, number of stored CAP keys) are written, see @ref EMV_CTLS_CAPREAD_STRUCT
/// @param[in,out] pucMaxnum     input: maximum number of CAP keys to write to pxKeyData; output: number of CAP keys stored in file @n TLV tag #TAG_KEY_NUMBER
///
/// @note Currently maximum number of CAP keys to be stored is 100.
/// @b Serialization:
/// @image html TLV_ReadCAPKey.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_CAPKEY_CFG, P2: #P2_GET
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_READ_ERROR
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_ReadCAPKeys(EMV_CTLS_CAPREAD_TYPE *pxKeyData, unsigned char *pucMaxnum);


/*****************************************************************************
*  EMV_CTLS_SetTermData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief set terminal specific data
///        and perform verification and saving of data
///
/// Explanation in programmers guide: @ref anchor_emv_ctls_Configure_CTLS_Terminal_Data "Configure CTLS Terminal data"
///
/// @author GSS R&D Germany
///
/// @param[in] pxTermData ... terminal data, see EMV_CTLS_TERMDATA_STRUCT
/// @return    #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_READ_ERROR, #EMV_ADK_SAVE_ERROR
///
/// @b Serialization:
/// @image html TLV_SetTerminalData.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_TERM_CFG, P2: #P2_SET
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_SetTermData(EMV_CTLS_TERMDATA_TYPE* pxTermData);

/*****************************************************************************
*  EMV_CTLS_GetTermData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief get configured terminal data
///
/// Explanation in programmers guide: @ref anchor_emv_ctls_Configure_CTLS_Terminal_Data "Configure CTLS Terminal data"
///
/// @author GSS R&D Germany
///
/// @param[out] pxTermData ... terminal data, see EMV_CTLS_TERMDATA_STRUCT
/// @return     #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_READ_ERROR
///
/// @b Serialization:
/// @image html TLV_GetTerminalData.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_TERM_CFG, P2: #P2_GET
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_GetTermData(EMV_CTLS_TERMDATA_TYPE* pxTermData);

#ifdef OLD_CONFIG_INTERFACE
/*****************************************************************************
*  EMV_CTLS_SetAppliData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Set application specific data
///
/// @deprecated Use EMV_CTLS_SetAppliDataSchemeSpecific() instead
///
/// @author GSS R&D Germany
///
/// @param[in] eHandleAppliType ... see @ref APPLI_CONF_MODE @n TLV tag #TAG_HANDLE_APPLI_TYPE
/// @param[in] pxAID ... AID (up to n-times for AIDs with same configuration), see EMV_CTLS_APPLI_STRUCT
/// @param[in] pxAppliData ... application data, see EMV_CTLS_APPLIDATA_STRUCT
/// @return #EMV_ADK_NOT_ALLOWED_WRONG_CFG_INTF, #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED,
///         #EMV_ADK_READ_ERROR, #EMV_ADK_SAVE_ERROR
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_SetAppliData(EMV_ADK_HANDLE_RECORD_TYPE eHandleAppliType, EMV_CTLS_APPLI_TYPE* pxAID, EMV_CTLS_APPLIDATA_TYPE* pxAppliData);

/*****************************************************************************
*  EMV_CTLS_GetAppliData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Get configured application data.
///
/// @deprecated Use EMV_CTLS_GetAppliDataSchemeSpecific() instead
///
/// @author GSS R&D Germany
///
/// @param[in] eReadAppliType ... see @ref READ_APPLI_TYPE @n TLV tag #TAG_HANDLE_APPLI_TYPE
/// @param[in,out] pxAID ... AID, see EMV_CTLS_APPLI_STRUCT
/// @param[out] pxAppliData ... application data, see EMV_CTLS_APPLIDATA_STRUCT
/// @return #EMV_ADK_NOT_ALLOWED_WRONG_CFG_INTF, #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED,
///         #EMV_ADK_READ_ERROR
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_GetAppliData(EMV_ADK_READAPPLI_TYPE eReadAppliType, EMV_CTLS_APPLI_TYPE* pxAID, EMV_CTLS_APPLIDATA_TYPE* pxAppliData);
#endif

/*****************************************************************************
*  EMV_CTLS_SetAppliDataSchemeSpecific
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Set application specific data
///        and perform verification and saving of data.
///
/// This function can only be used if init option #EMV_CTLS_INIT_OPT_NEW_CFG_INTF was set.
/// @n Has to be called once per application/kernel combination (Visa, MasterCard, ...).
/// @n Restriction for VFI reader:
/// @n Max. number allowed is 10 system AIDs + 10 user AIDs
/// @n System AIDs are: MasterCard, Maestro, Visa, Discover, Amex, Visa electron, VPAY, PBOC, Visa Interlink, Interac
///
/// @author GSS R&D Germany
///
/// @param[in] eHandleAppliType ... see @ref APPLI_CONF_MODE @n TLV tag #TAG_HANDLE_APPLI_TYPE
/// @param[in] pxAID ... AID/kernel combination, key for this configuration set, see EMV_CTLS_APPLI_KERNEL_STRUCT
/// @param[in] pxAppliData ... application data, see EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT
/// @return #EMV_ADK_OK in case of success@n
///         #EMV_ADK_PARAM for illegal handle type or pointer@n
///         #EMV_ADK_TLV_BUILD_ERR TLV processing problem@n
///         #EMV_ADK_INTERNAL internal communication problem, illegal file content, memory allocation@n
///         #EMV_ADK_NOT_ALLOWED concurrent call or not allowed within callback@n
///         #EMV_ADK_READ_ERROR file corruption @n
///         #EMV_ADK_SAVE_ERROR persistence problem @n
///         #EMV_ADK_NO_EXEC if number of configurable AIDs is a limit@n
///         @deprecated #EMV_ADK_NOT_ALLOWED_WRONG_CFG_INTF in case of wrong configuration function
///
/// @b Serialization:
/// @image html TLV_SetApplicationData.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_APPLI_CFG, P2: #P2_SET
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_SetAppliDataSchemeSpecific(EMV_ADK_HANDLE_RECORD_TYPE eHandleAppliType, EMV_CTLS_APPLI_KERNEL_TYPE* pxAID, EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_TYPE* pxAppliData);

/*****************************************************************************
*  EMV_CTLS_GetAppliDataSchemeSpecific
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Get configured application data.
///
/// This function can only be used if init option #EMV_CTLS_INIT_OPT_NEW_CFG_INTF was set.
/// @n
/// @warning struct members of type #EMV_CTLS_DATA_TYPE are allocated within libEMV_CTLS_Client.
///          The next call to this function as well as a call to Exit Framework will invalidate
///          the data pointers of the first result. For the same reason, this function is not
///          thread-safe
///
/// Transport tags used in serialization: #CLA_EMV #INS_APPLI_CFG #P2_GET
///
/// @author GSS R&D Germany
///
/// @param[in] eReadAppliType ... see @ref READ_APPLI_TYPE
/// @param[in,out] pxAID ... AID, see EMV_CTLS_APPLI_STRUCT
/// @param[out] pxAppliData ... application data, see EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT
/// @return #EMV_ADK_NOT_ALLOWED_WRONG_CFG_INTF, #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED,
///         #EMV_ADK_READ_ERROR
///
/// @b Serialization:
/// @image html TLV_GetApplicationData.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_APPLI_CFG, P2: #P2_GET
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_GetAppliDataSchemeSpecific(EMV_ADK_READAPPLI_TYPE eReadAppliType, EMV_CTLS_APPLI_KERNEL_TYPE* pxAID, EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_TYPE* pxAppliData);

/*****************************************************************************
*  EMV_CTLS_ApplyConfiguration
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Transfer the stored configuration to reader
///
/// Only functional in context of VFI reader.
/// @n On velocity solution this function writes the configuration to file system if @ref EMV_CTLS_INIT_OPT_CONFIG_MODE was set.
///
/// Explanation in programmers guide: @ref anchor_emv_ctls_Configure_CTLS_Application_Data "Configure CTLS Application data"
///
/// @author GSS R&D Germany
///
/// @param[in]  options  Configuration options, see @ref APPLY_CONFIG_OPTION @n TLV tag #TAG_DF8F0F_APPLYCONFIG_OPTIONS
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_ABORT, #EMV_ADK_NO_EXEC, #EMV_ADK_INTERNAL, #EMV_ADK_READ_ERROR,
/// #EMV_ADK_SAVE_ERROR, #EMV_ADK_VIRTTERMMAP_WRONG_INIT, #EMV_ADK_TLV_BUILD_ERR
///
/// @b Serialization:
/// @image html TLV_ApplyConfiguration.jpg
/// Class: #CLA_EMV or #CLA_EMV_ALT, Instruction: #INS_APPLY_CFG
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_ApplyConfiguration(unsigned long options);

/*****************************************************************************
* EMV_CTLS_SetupTransaction
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Set up a CTLS EMV transaction
///
/// Transport tags used in serialization: #CLA_EMV #INS_SELECT
///
/// @ref anchor_emv_ctls_Setup_a_Transaction "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in] pxStartInput ... transaction specific data (see @ref EMV_CTLS_START_STRUCT)
/// @param[out] pxStartRes  ... additional (error) info (see @ref EMV_CTLS_STARTRES_STRUCT)
///
/// @return #EMV_ADK_OK, #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, #EMV_ADK_CEILING_LIMIT, #EMV_ADK_TLV_BUILD_ERR,
/// #EMV_ADK_CTLS_NOT_AVAILABLE, #EMV_ADK_VIRTTERMMAP_WRONG_INIT, #EMV_ADK_READ_ERROR, #EMV_ADK_NOAPP
/// @n VFI reader only: #EMV_ADK_ABORT, #EMV_ADK_READER_CMD_NOT_ALLOWED, #EMV_ADK_NO_EXEC
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_SetupTransaction( EMV_CTLS_START_TYPE *pxStartInput, EMV_CTLS_STARTRES_TYPE* pxStartRes);

/*****************************************************************************
*  EMV_CTLS_ContinueOffline
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief CTLS EMV transaction (offline part ... including 1st cryptogram, depends on CTLS scheme)
///
/// Transport tags used in serialization: #CLA_EMV #INS_TRANSAC
///
/// @ref anchor_emv_ctls_Continue_a_Transaction__Offline_Part "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[out] pxTransRes ... transaction result data (see @ref EMV_CTLS_TRANSRES_STRUCT)
///
/// @return  #EMV_ADK_ARQC = transaction must go online
/// @n      #EMV_ADK_TC = transaction is offline approved
/// @n      #EMV_ADK_OK = card data read successfully (only possible for transaction types #EMV_ADK_TRAN_TYPE_REFUND, #EMV_ADK_TRAN_TYPE_MANUAL_REVERSAL, #EMV_ADK_TRAN_TYPE_BUCHUNG_B, #EMV_ADK_TRAN_TYPE_BUCHUNG_A)
/// @n      other values mean error: #EMV_ADK_CANCELLED, #EMV_ADK_CARD_LOG_LOG_OK, #EMV_ADK_CTLS_LOW_BATTERY,
/// #EMV_ADK_TOO_MANY_TAPS, #EMV_ADK_CAN_NOT_PROCESS, #EMV_ADK_USE_ANOTHER_CTLS_CARD, #EMV_ADK_USE_OTHER_TERMINAL,
/// #EMV_ADK_FALLBACK_CHIP_ONLY, #EMV_ADK_READER_CMD_NOT_ALLOWED, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_CTLS_NOT_AVAILABLE,
/// #EMV_ADK_CEILING_LIMIT, #EMV_ADK_CTLS_RETAP_SAME, #EMV_ADK_CTLS_DOMESTIC_APP, #EMV_ADK_NO_CARD, #EMV_ADK_NOAPP,
/// #EMV_ADK_NO_EXEC, #EMV_ADK_AAC, #EMV_ADK_PARAM, #EMV_ADK_ABORT, #EMV_ADK_CARD_BLOCKED,
/// #EMV_ADK_INTERNAL, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_FALLBACK, #EMV_ADK_2_CTLS_CARDS, #EMV_ADK_TXN_CTLS_L1_ERR,
/// #EMV_ADK_TXN_CTLS_MOBILE, #EMV_ADK_TXN_CTLS_EMPTY_LIST, #EMV_ADK_TXN_CTLS_EMV_USE_OTHER_CARD,
/// #EMV_ADK_CTLS_DOMESTIC_ONLY_NOT_READABLE, #EMV_ADK_CONTINUE, #EMV_ADK_NOT_ACCEPTED, #EMV_ADK_CTLS_OFFLINE_PIN
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_ContinueOffline(EMV_CTLS_TRANSRES_TYPE *pxTransRes);

/*****************************************************************************
*  EMV_CTLS_ContinueOfflineExt
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Like EMV_CTLS_ContinueOffline but with input parameter
///
/// @author GSS R&D Germany
///
/// @param[in]  pxContOfflInput ... Input parameter for special use cases (see @ref EMV_CTLS_CONT_OFFL_STRUCT)
/// @param[out] pxTransRes ... transaction result data (see @ref EMV_CTLS_TRANSRES_STRUCT)
///
/// @return  #EMV_ADK_ARQC = transaction must go online
/// @n      #EMV_ADK_TC = transaction is offline approved
/// @n      #EMV_ADK_OK = card data read successfully (only possible for transaction types #EMV_ADK_TRAN_TYPE_REFUND, #EMV_ADK_TRAN_TYPE_MANUAL_REVERSAL, #EMV_ADK_TRAN_TYPE_BUCHUNG_B, #EMV_ADK_TRAN_TYPE_BUCHUNG_A)
/// @n      other values mean error: #EMV_ADK_CANCELLED, #EMV_ADK_CARD_LOG_LOG_OK, #EMV_ADK_CTLS_LOW_BATTERY,
/// #EMV_ADK_TOO_MANY_TAPS, #EMV_ADK_CAN_NOT_PROCESS, #EMV_ADK_USE_ANOTHER_CTLS_CARD, #EMV_ADK_USE_OTHER_TERMINAL,
/// #EMV_ADK_FALLBACK_CHIP_ONLY, #EMV_ADK_READER_CMD_NOT_ALLOWED, #EMV_ADK_NOT_ALLOWED, #EMV_ADK_CTLS_NOT_AVAILABLE,
/// #EMV_ADK_CEILING_LIMIT, #EMV_ADK_CTLS_RETAP_SAME, #EMV_ADK_CTLS_DOMESTIC_APP, #EMV_ADK_NO_CARD, #EMV_ADK_NOAPP,
/// #EMV_ADK_NO_EXEC, #EMV_ADK_AAC, #EMV_ADK_PARAM, #EMV_ADK_ABORT, #EMV_ADK_CARD_BLOCKED,
/// #EMV_ADK_INTERNAL, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_FALLBACK, #EMV_ADK_2_CTLS_CARDS, #EMV_ADK_TXN_CTLS_L1_ERR,
/// #EMV_ADK_TXN_CTLS_MOBILE, #EMV_ADK_TXN_CTLS_EMPTY_LIST, #EMV_ADK_TXN_CTLS_EMV_USE_OTHER_CARD,
/// #EMV_ADK_CTLS_DOMESTIC_ONLY_NOT_READABLE, #EMV_ADK_CONTINUE, #EMV_ADK_NOT_ACCEPTED, #EMV_ADK_CTLS_OFFLINE_PIN
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_ContinueOfflineExt(EMV_CTLS_CONT_OFFL_TYPE *pxContOfflInput, EMV_CTLS_TRANSRES_TYPE *pxTransRes);

/*****************************************************************************
*  EMV_CTLS_ContinueOnline
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief EMV transaction (handling of host response including 2nd cryptogram)
///
/// Transport tags used in serialization: #CLA_EMV #INS_ONLINE
///
/// @ref anchor_emv_ctls_Continue_a_Transaction__Online_Part "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in] pxOnlineInput ... data of host response (see @ref EMV_CTLS_HOST_STRUCT)
///
/// @param[out] pxTransRes ... result data (see @ref EMV_CTLS_TRANSRES_STRUCT)
///
/// @return #EMV_ADK_TC, #EMV_ADK_AAC, #EMV_ADK_NO_CARD, #EMV_ADK_2_CTLS_CARDS,
//          #EMV_ADK_TXN_CTLS_L1_ERR (despite several internal silent retries),
///         #EMV_ADK_CTLS_RETAP_SAME (in case of L1 error after long tap),
///         #EMV_ADK_ABORT, #EMV_ADK_PARAM, #EMV_ADK_INTERNAL
/// @n      VFI reader only: #EMV_ADK_NO_EXEC, Vertex only: #EMV_ADK_2_CTLS_CARDS
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_ContinueOnline(EMV_CTLS_HOST_TYPE *pxOnlineInput, EMV_CTLS_TRANSRES_TYPE *pxTransRes);

/*****************************************************************************
*  EMV_CTLS_fetchTxnTags
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Fetch one or several tags at the end of the transaction
///
/// While ContinueOffline and ContinueOnline provide the most common / generic tags,
/// which are necessary for application handling in a readable way (structure),
/// this may not fulfill all local requirements.
/// This function offers to read any proprietary, extended or application specific tag,
/// which is additionally (next to the generic ones) required by the application.
/// Calling this function is optional, however it may provide benefit for single tags as well as for tag combinations,
/// which will be provided in a TLV stream (e.g. needed for BMP 55) instead of structure data,
/// which is preferable for receipt printing or offline storage.
///
/// Transport tags used in serialization: #CLA_EMV #INS_FETCH_TAG
///
/// @ref anchor_emv_ctls_Fetching_Additional_Tags "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]     options            ... Options, see @ref FETCH_TAGS_OPTIONS
/// @param[in]     requestedTags      ... requested tags
/// @param[in]     noOfRequestedTags  ... number of requested tags
/// @param[in,out] tlvBuffer          ... buffer for TLV output, allocated by calling application
/// @param[in]     bufferLength       ... number of bytes allocated for tlvBuffer
/// @param[out]    tlvDataLength      ... number of bytes written to tlvBuffer
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_fetchTxnTags(unsigned long options, unsigned long* requestedTags, unsigned short noOfRequestedTags, unsigned char* tlvBuffer, unsigned short bufferLength, unsigned short* tlvDataLength );

/*****************************************************************************
*  EMV_CTLS_fetchTxnDOL
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Retrieve formatted transaction data at the end of the transaction
///
/// Like @ref EMV_CTLS_fetchTxnTags but take a DOL and return pure data.
/// The return buffer is filled with requested data, each with exact requested
/// data length. The tag's data type is analysed and type specific truncation and
/// padding is applied. If a tag's type is unknown, binary is assumed. If the tag is
/// unknown or no data available, '00' bytes are padded.
/// If the requested data length is greater than the available data length, then numeric
/// data is padded with leading '00' and compressed numeric is padded with
/// trailing 'FF'. Other types are padded right with trailing '00'.
/// If the requested data length is smaller than the available data length, then
/// numeric values are cut at front and others at the end.
///
/// Transport tags used in serialisation: #CLA_EMV #INS_FETCH_DOL
///
/// @author GSS R&D Germany
///
/// @param[in]     options            ... Options: None, reserved for future use
/// @param[in]     DOL                ... Data Object List = sequence of BER-TLV Tag+Length pairs
/// @param[in]     dolLen             ... length of DOL [bytes]
/// @param[in,out] buffer             ... result data buffer
/// @param[in]     bufferSize         ... size of result data buffer [bytes]
/// @param[out]    dataLength         ... result data length = sum of all lengths in the DOL (in case of success)
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_fetchTxnDOL(unsigned long options, const unsigned char* DOL, unsigned dolLen, unsigned char* buffer, unsigned bufferSize, unsigned* dataLength);

/*****************************************************************************
*  EMV_CTLS_EndTransaction
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief End transaction
///
/// This function has to be called at the end of each transactions. It takes care that
/// everything is cleaned up properly, wiped and subcomponents are deinitiialized as needed.
///
/// @author GSS R&D Germany
///
/// Transport tags used in serialization: #CLA_EMV #INS_END_TRX
///
/// @param[in]     options            ... Options, bit set of @ref END_TXN_OPTIONS
///
/// @return #EMV_ADK_OK, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_EndTransaction(unsigned long options);


// ========================================================================================================
// === CONTACTLESS FUNCTIONS to be implemented in entity in which framework is running ===
// ========================================================================================================

/// @defgroup ADK_LED_DEFINES Defines for LED handling
/// @ingroup ADK_LED
/// @brief LED numbers and modes to be used in EMV_CTLS_LED() and EMV_CTLS_LED_SetMode()
/// @{
#  define CONTACTLESS_LED_FIRST   0x01 ///< first contacless LED
#  define CONTACTLESS_LED_SECOND  0x02 ///< second contacless LED
#  define CONTACTLESS_LED_THIRD   0x04 ///< third contacless LED
#  define CONTACTLESS_LED_FOURTH  0x08 ///< fourth contacless LED
// and a second list for those who like colours according Book A, Architecture and General Requirements, 9.3.3 Option 2 for Card Read Successfully/Processing Error
#  define CONTACTLESS_LED_BLUE    CONTACTLESS_LED_FIRST  ///< first LED for signalling field on
#  define CONTACTLESS_LED_YELLOW  CONTACTLESS_LED_SECOND ///< second LED indicating chip card communication
#  define CONTACTLESS_LED_GREEN   CONTACTLESS_LED_THIRD  ///< third LED "Card read successfully"
#  define CONTACTLESS_LED_RED     CONTACTLESS_LED_FOURTH ///< fourth LED indicating error
// the original and deprecated list.
#  define CONTACTLESS_LED_0       CONTACTLESS_LED_FIRST  ///< Contactless LED 1, @deprecated because confusing, use #CONTACTLESS_LED_FIRST instead
#  define CONTACTLESS_LED_1       CONTACTLESS_LED_SECOND ///< Contactless LED 2, @deprecated because confusing, use #CONTACTLESS_LED_SECOND instead
#  define CONTACTLESS_LED_2       CONTACTLESS_LED_THIRD  ///< Contactless LED 3, @deprecated because confusing, use #CONTACTLESS_LED_THIRD instead
#  define CONTACTLESS_LED_3       CONTACTLESS_LED_FOURTH ///< Contactless LED 4, @deprecated because confusing, use #CONTACTLESS_LED_FOURTH instead

#  define CONTACTLESS_LED_ALL     0x0F ///< All Contactless LEDs
#  define CONTACTLESS_LED_LOGO    0x80 ///< CTLS Logo LED (only UX family), not combinable with other LEDs

#define CONTACTLESS_LED_OFF                     0x00 ///< Switch LED Off
#define CONTACTLESS_LED_ON                      0x01 ///< Switch LED On
#define CONTACTLESS_LED_IDLE_BLINK              0x02 ///< enable blinking: ALWAYS LED 1 is used for blinking, fixed timing according to specs
#define CONTACTLESS_LED_ONLINE_BLINK            0x03 ///< enable blinking: ALWAYS LED 3 is used for blinking, fixed timing according to specs - This function is only available im LED EMV Mode, not in API Mode
#define CONTACTLESS_LED_HANDLE_EOT_SILENT       0x04 ///< End of Transaction, disable LED in the background after required 750msecs - This function is only available im LED EMV Mode, not in API Mode
#define CONTACTLESS_LED_HANDLE_EOT_SUCCESS      0x05 ///< End of Transaction, disable LED in the background after required 750msecs + Beep Success - This function is only available im LED EMV Mode, not in API Mode
#define CONTACTLESS_LED_HANDLE_EOT_FAIL         0x06 ///< End of Transaction, disable LED in the background after required 750msecs + Beep Fail - This function is only available im LED EMV Mode, not in API Mode
#define CONTACTLESS_LED_HANDLE_EOT_FAIL_BLINK   0x07 ///< End of Transaction, LED 4 is blinking 4 times + Beep Fail - This function is only available im LED EMV Mode, not in API Mode
#define CONTACTLESS_LED_HANDLE_EOT_FAIL_BLINK_3 0x08 ///< End of Transaction, LED 1-3 are blinking 4 times + Beep Fail - This function is only available im LED EMV Mode, not in API Mode

#define CONTACTLESS_LED_MODE_OFF                0x00 ///< switching LEDs must be done by the application
#define CONTACTLESS_LED_MODE_API                0x01 ///< switching LEDs must be done by the API function EMV_CTLS_LED()
#define CONTACTLESS_LED_MODE_EMV                0x02 ///< switching LEDs is done by EMV framework on its own (this is the initial mode)
/// @}

/// @defgroup ADK_BUZZER Buzzer handling
/// @ingroup TLV_CALLBCK
/// @brief Defines given in callback #TAG_BF19_CTLS_CBK_BEEP
/// @{
#define  EMV_CTLS_BEEP_SUCCESS  1  ///< Success tone is required.
#define  EMV_CTLS_BEEP_ALERT    2  ///< Alert tone needed.
/// @}

/// @defgroup ADK_ICC_OPTION Options for ICC access
/// @ingroup ADK_ICC_IF
/// @brief Options for ICC access, e.g. for EMV_CTLS_SmartISO(), EMV_CTLS_SmartReset(), EMV_CTLS_SmartPowerOff()
/// @{
#define  EMV_CTLS_RFU_1                  0x01  ///< reserved for probable reader number
#define  EMV_CTLS_RFU_2                  0x02  ///< reserved for probable reader number
#define  EMV_CTLS_SPURIOUS_DETECT        0x04  ///< switch off the workaround for spurious card detections on ADK level (workaround for L1 issues, code provided by platform team), switching off may provide better detection of 2 cards in field, which is resolved with the spurious card detection workaround. @deprecated by use of VERTEX card reader abstraction layer.
#define  EMV_CTLS_EMV_COMPLIANT          0x08  ///< (Vel: y VFI: n) EMV_CTLS_SmartReset() @c ucOptions: Only card type is returned in card info. This feature is mutually exclusive with EMV_CTLS_RESET_NO_EMV.
#define  EMV_CTLS_RESET_NO_EMV           0x10  ///< (Vel: y VFI: n) EMV_CTLS_SmartReset() @c ucOptions: Provide card type plus UID if available as card info. This feature is mutually exclusive with EMV_CTLS_EMV_COMPLIANT.
#define  EMV_CTLS_RESET_PICC_OPEN_ONLY   0x20  ///< (Vel: y VFI: n) EMV_CTLS_SmartReset() @c ucOptions: Only call PICC_Open(), do not detect cards. Obsolete by use of VERTEX card reader abstraction layer
#define  EMV_CTLS_SKIP_FIELD_ON          0x40  ///< (Vel: n VFI: y) EMV_CTLS_SmartReset() @c ucOptions: Tell VFI Reader to consider RF Field is already turned on by NFC ADK.

#define  EMV_CTLS_DETECT_REMOVAL         0x80  ///< (Vel: y VFI: n) option for EMV_CTLS_SmartPowerOff, start removal detection background task
/// @}

/*****************************************************************************
* EMV_CTLS_TransparentCommand
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Transparently send a command to the reader and receive it's response
/// @n     only supported for VFI reader
///
/// This function has limited functionality.
/// @n In case the restrictions are not acceptable for your need:
/// @n Use EMV_CTLS_TransparentSend() and EMV_CTLS_TransparentReceive() instead.
/// @n Restrictions:
/// @li Internal timeout to wait for a response is 3 seconds.
/// @li Protocol 1 commands not supported.
///     As these commands have (sometimes) double responses.
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_TRANS
///
/// @author GSS R&D Germany
///
/// @param[in]   usInDataLen, pucDataIn ... reader command
/// @param[out]  pusOutDataLen, pucDataOut ... reader response
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NO_EXEC
///
DLL_CLC unsigned char EMV_CTLS_TransparentCommand (unsigned short usInDataLen, unsigned char *pucDataIn, unsigned short *pusOutDataLen, unsigned char *pucDataOut);

/*****************************************************************************
* EMV_CTLS_TransparentSend
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Transparently send a command to the reader
/// @n      only supported for VFI reader
/// @n      to fetch the response use EMV_CTLS_TransparentReceive()
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_SEND
///
/// @author GSS R&D Germany
///
/// @param[in]   usInDataLen, pucDataIn ... reader command
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NO_EXEC
///
DLL_CLC unsigned char EMV_CTLS_TransparentSend (unsigned short usInDataLen, unsigned char *pucDataIn);

/*****************************************************************************
* EMV_CTLS_TransparentReceive
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Transparent VFI reader access: Receive a response
/// @n     only supported for VFI reader
/// @n     A command can be sent with help of EMV_CTLS_TransparentSend()
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_RECEIVE
///
/// @author GSS R&D Germany
///
/// @param[out]  pusOutDataLen, pucDataOut ... reader response
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NO_EXEC
///
DLL_CLC unsigned char EMV_CTLS_TransparentReceive (unsigned short *pusOutDataLen, unsigned char *pucDataOut);

/*****************************************************************************
* EMV_CTLS_SmartISO
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Send chip card command and receive response
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_ISO
///
/// @ref anchor_emv_ctls_Send_an_ISO_Command_to_a_Contactless_Smart_Card "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]   ucOptions ... @ref ADK_ICC_OPTION
/// @param[in]   usInDataLen, pucDataIn ... chip card command
/// @param[in]   usOutBufferLength ... max length of output buffer
/// @param[out]  pusOutDataLen, pucDataOut ... chip card response
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CLC unsigned char EMV_CTLS_SmartISO (unsigned char ucOptions, unsigned short usInDataLen, unsigned char *pucDataIn, unsigned short *pusOutDataLen, unsigned char *pucDataOut, unsigned short usOutBufferLength);

/*****************************************************************************
* EMV_CTLS_SmartReset
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Detect if card is present in the RF field (read ATR)
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_RESET
///
/// @ref anchor_emv_ctls_Activating_a_Contactless_Smart_Card "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]      ucOptions      @ref ADK_ICC_OPTION
/// @param[out]     pucCardInfo    Card type, 0: No card, 1: ISO14443A, 2: ISO14443B, for non-EMV values see OS header picc.h @n appended if available and option #EMV_CTLS_RESET_NO_EMV: UID (1 byte data length, n byte data)
/// @param[in,out]  pnInfoLength   in: allocated size of pucCardInfo, out: Number of bytes written to pucCardInfo
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CLC unsigned char EMV_CTLS_SmartReset(unsigned char ucOptions, unsigned char* pucCardInfo, unsigned long* pnInfoLength);

/*****************************************************************************
* EMV_CTLS_SmartPowerOff
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Deactivate chip card.
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_OFF
///
/// @ref anchor_emv_ctls_Deactivate_a_Smart_Card "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]   ucOptions ... @ref ADK_ICC_OPTION
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CLC unsigned char EMV_CTLS_SmartPowerOff(unsigned char ucOptions);


/*****************************************************************************
 * EMV_CTLS_CardRemoval
 *****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Wait until contactless card removed or timeout.
///
/// Transport tags used for serialization: #CLA_CRD #INS_ICC_REMOVAL
///
/// Note: The detection is only possible if there was a preceeding call of
/// EMV_CTLS_SmartPowerOff() with #EMV_CTLS_DETECT_REMOVAL or
/// #EMV_CTLS_FLOW_GLOB_START_REMOVAL_DETECTION_1 is enabled by the application
/// flow capabilities.
///
/// @author GSS R&D Germany
///
/// @param[in] timeoutMillis maximal blocking time in milli seconds (maximum is 60 seconds)
/// @return @c 1 ... card still present @n @c 0 ... card removed or removal detection not activated @n additionally possible: #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CLC unsigned char EMV_CTLS_CardRemoval(long timeoutMillis);

/*****************************************************************************
* EMV_CTLS_LED_SetMode
*****************************************************************************/
/// @ingroup ADK_LED
/// @brief Set mode of LED control
///
/// Transport tags used for serialization: #CLA_CRD #INS_LED_MODE
///
/// @ref anchor_emv_ctls_LEDs_Set_Mode "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]   ucLedMode ... LED mode, see @ref ADK_LED_DEFINES
///
/// @return #EMV_ADK_OK, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NOT_ALLOWED
///
DLL_CLC unsigned char EMV_CTLS_LED_SetMode(unsigned char ucLedMode);

/*****************************************************************************
* EMV_CTLS_LED
*****************************************************************************/
/// @ingroup ADK_LED
/// @brief Switch on LED
///
/// Transport tags used for serialization: #CLA_CRD #INS_LED_SWITCH
///
/// @ref anchor_emv_ctls_LEDs_Switch_LEDs "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]   ucLedId ... LED number, see @ref ADK_LED_DEFINES
/// @n           This is a combination of LEDs. (use binary bitwise inclusive OR of LED-Bit-masks defined in @ref ADK_LED_DEFINES)
/// @n           Bits which are not defined in @ref ADK_LED_DEFINES should be 0. They are reserved for future use and will be ignored.
/// @param[in]   ucLedState ... LED state, see @ref ADK_LED_DEFINES
///
/// @return #EMV_ADK_OK, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, #EMV_ADK_NOT_ALLOWED
///
DLL_CLC unsigned char EMV_CTLS_LED(unsigned char ucLedId, unsigned char ucLedState);

/*****************************************************************************
* EMV_CTLS_LED_ConfigDesign
*****************************************************************************/
/// @ingroup ADK_LED
/// @brief Configure the desing for the framebuffer LEDs used for virtual CTLS LEDs
///   @n <B> Only required when using virtual LEDs. </B>
/// @n @deprecated The direct framebuffer approach is deprecated - use LED callbacks and LED-API from ADK-GUIPRT instead
///
/// Transport tags used for serialization: #CLA_CRD #INS_LED_CONF_DESIGN
///
/// @ref anchor_emv_ctls_LEDs_Soft_LEDs_config_design "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]   width ... width of the rectangular LED
/// @param[in]   height ... height of the rectangular LED
/// @param[in]   color_off ... off color (24bit) (usually black == 0)
/// @param[in]   color_on ... color of the LED (24bit)
/// @param[in]   xRegion ... x coordinate of the LED Region (upper left corner)
/// @param[in]   yRegion ... y coordinate of the LED Region (upper left corner)
/// @param[in]   wRegion ... width of the LED Region
/// @param[in]   hRegion ... height of the LED Region
///
/// @return #EMV_ADK_OK, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, #EMV_ADK_NOT_ALLOWED
///
DLL_CLC unsigned char EMV_CTLS_LED_ConfigDesign(unsigned short width, unsigned short height, unsigned char* color_off, unsigned char* color_on,
                                                unsigned short xRegion, unsigned short yRegion, unsigned short wRegion, unsigned short hRegion)
__attribute ((deprecated));


/// @ingroup ADK_LED
/// @brief Data structure for LED colors. Used in EMV_CTLS_LED_CONFIG_STRUCT::shape
#define EMV_CTLS_LED_SHAPE_RECTANGLE  0    ///< rectangular LEDs
#define EMV_CTLS_LED_SHAPE_CIRCLE     1    ///< circular LEDs

/// @ingroup ADK_LED
/// @brief Data structure for LED colors. Input for EMV_CTLS_LED_ConfigDesign_Extended()
typedef struct EMV_CTLS_LED_COLORS_STRUCT
{
    unsigned char num_colors; ///< number of different colors; set to 1 if all LEDs should have the same color, set to 4 if all LEDs have different colors.
    unsigned char *colors;    ///< Array of colors, each color is 3 bytes (24 bit RGB), so length of array must be num_colors * 3.
} EMV_CTLS_LED_COLORS;

/// @ingroup ADK_LED
/// @brief Data structure for LED configuration. Input for EMV_CTLS_LED_ConfigDesign_Extended()
typedef struct EMV_CTLS_LED_CONFIG_STRUCT
{
    unsigned short region_x;          ///< x coordinate of the LED Region (upper left corner)
    unsigned short region_y;          ///< y coordinate of the LED Region (upper left corner)
    unsigned short region_width;      ///< width of the LED Region
    unsigned short region_height;     ///< height of the LED Region
    EMV_CTLS_LED_COLORS colors_off;   ///< colors of LEDs in off state
    EMV_CTLS_LED_COLORS colors_on;    ///< colors of LEDs in on state
    unsigned char *bg_color;          ///< background color, only used if shape == EMV_CTLS_LED_SHAPE_CIRCLE
    unsigned char shape;              ///< shape of LEDs, one of EMV_CTLS_LED_SHAPE_RECTANGLE, EMV_CTLS_LED_SHAPE_CIRCLE
    unsigned short *shape_params;     ///< if shape == EMV_CTLS_LED_SHAPE_RECTANGLE: first value: width of LED, second value: height of LED; if shape == EMV_CTLS_LED_SHAPE_CIRCLE: first value: radius of LED (width=height=2*radius+1)
    unsigned char *options;           ///< RFU, set to NULL
} EMV_CTLS_LED_CONFIG;

/*****************************************************************************
* EMV_CTLS_LED_ConfigDesign_Extended
*****************************************************************************/
/// @ingroup ADK_LED
/// @brief Configure the design for the framebuffer LEDs used for virtual CTLS LEDs
///   @n <B> Only required when using virtual LEDs. </B>
/// @n @deprecated The direct framebuffer approach is deprecated - use LED callbacks and LED-API from ADK-GUIPRT instead
///
/// Coloured LEDs should be used together with #INPUT_CTLS_TRM_FLOWOPT_LED_EP_OPTION_2.
///
/// Transport tags used for serialization: #CLA_CRD #INS_LED_CONF_DESIGN
///
/// @ref anchor_emv_ctls_LEDs_Soft_LEDs_config_design "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @param[in]   led_config ...  LED configuration, see @ref EMV_CTLS_LED_CONFIG_STRUCT
///
/// @return @c 0 ... okay @n @c !=0 ... error
///
DLL_CLC unsigned char EMV_CTLS_LED_ConfigDesign_Extended(const EMV_CTLS_LED_CONFIG *led_config) __attribute ((deprecated));

/*****************************************************************************
* EMV_CTLS_Break
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Universal break command
///   @n <B> Velocity: Only required when using EMV_CTLS_START_STRUCT::ServerPollTimeout != 0. </B>
///   @n <B> VFI-Reader: Always required </B>
///
/// Transport tags used for serialization: #CLA_EMV #INS_BREAK
///
/// @ref anchor_emv_ctls_Server_Polling_Mode_Break "Explanation in programmers guide"
///
/// @author GSS R&D Germany
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_INTERNAL
///
DLL_CLC unsigned char EMV_CTLS_Break(void);

#define EMV_CTLS_SUPPORTED_SIZE_9F0A 50 ///< limit for Application Section Registered Proprietary Data size (has been 20 before)
#define EMV_CTLS_SUPPORTED_SIZE_DISC 50 ///< limit for discretionary data size (template '73')

/// @ingroup FUNC_FLOW
/// @brief Candidate list additional data
///
/// To be used in conjunction with AID list in combo candidates callback #TAG_BF12_CBK_MODIFY_CAND
/// @n See also EMV_CTLS_GetCandidateData()
typedef struct EMV_CTLS_CANDIDATE_DATA_STRUCT
{
  unsigned char API;      ///< tag '87' application priority indicator
  unsigned char IIN[4];   ///< tag '42' issuer identification number (n6 or n8, padded from right with 0xff like PAN), 0xff if absent
  unsigned char IBAN[35]; ///< tag '5F53' International bank account (an ..34, terminated by '0')
  unsigned char BIC[12];  ///< tag '5F54' Bank identification code (an 8 or 11, terminated by '0')
  unsigned char country2[2];  ///< tag '5F55' Issuer Country Code (a2)
  unsigned char country3[3];  ///< tag '5F56' Issuer Country Code (a3)
  unsigned char len9F06;  ///< for determining the terminal candidate ('9F06') in case of "AID chain" and partial select
  unsigned char selData[1+EMV_CTLS_SUPPORTED_SIZE_9F0A]; ///< Application Selection Registered Proprietary Data (9F0A), Bulletin No. 175, first byte length
  unsigned char disc[1+50]; ///< discretionary data, content of tag 73, first byte is length
} EMV_CTLS_CANDIDATE_DATA_TYPE;

#define  EMV_CTLS_ADDTAG_COMBOS  6  ///< Max. number of candidates in EMV_CTLS_GetCandidateData()

/*****************************************************************************
* EMV_CTLS_GetCandidateData
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Get additional candidate information for reducing and resorting
///        the mutual candidate list.
///
/// This function can be called within the reduces candidates callback (#TAG_BF12_CBK_MODIFY_CAND).
///
/// @author GSS R&D Germany
///
/// @param[out]  candidateData  Extended candidate data, see @ref EMV_CTLS_CANDIDATE_DATA_STRUCT
/// @n                          Number of elements must be #EMV_CTLS_ADDTAG_COMBOS
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NO_EXEC
///
/// @b Serialization:
/// Class: #CLA_EMV, Instruction: #INS_CND_DATA
///
DLL_CLC EMV_ADK_INFO EMV_CTLS_GetCandidateData(EMV_CTLS_CANDIDATE_DATA_TYPE *candidateData);

#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _EMV_CTLS_INTERFACE_H_
