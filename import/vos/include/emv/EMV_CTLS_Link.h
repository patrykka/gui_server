#ifndef _EMV_CTLS_LINK_H_
#define _EMV_CTLS_LINK_H_

#include "emv/btlv.h"
#include "emv/EMV_Common_Interface.h"

#ifdef _VRXEVO
#   ifdef EMV_CLL_EXPORT
#       define DLL_CLL __declspec(dllexport)
#   else
#       ifdef EMV_TEC_LIB
#           define DLL_CLL
#       else
#           define DLL_CLL __declspec(dllimport)
#       endif
#   endif
#else
#   define DLL_CLL // NOTE: Do not set visibility("default") because export.map is used
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*  EMV_CTLS_CallbackFunction
*****************************************************************************/
/// @ingroup TLV_CALLBCK
/// @brief Common callback function:
///        send/receive TLV stream to/from calling application.
///
/// Explanation in programmers guide: @ref subsec_Transaction_Callbacks
///
/// @author GSS R&D Germany
///
/// @n Tags for transport layer:
///    @li Callback function request: <tt> 91 01 </tt> (#EMV_CTLS_CLA_CBCK_REQ, #INS_CBCK_FCT)
///    @li Trace output: <tt> 91 FF </tt> (#EMV_CTLS_CLA_CBCK_REQ, #INS_CBCK_TRACE)
///    @li Response: <tt> 92 </tt> (#EMV_CTLS_CLA_CBCK_RESP)
///
/// @n Tags found in @c pucSend determine which callback function is meant (@ref CBCK_FCT_TAGS)
///
/// @param[in]      pucSend        TLV stream to send to calling application
/// @param[in]      sSendSize      Length of TLV stream to send to calling application
/// @param[out]     pucReceive     TLV stream received from calling application
/// @param[in,out]  psReceiveSize  Input: size of buffer pucReceive points to; output: size of TLV stream received from calling application
/// @param[out]     externalData   Application data pointer, which was put in EMV_CTLS_Init_Framework()
///
/// @note for verix avaiable stack-size is limited to #EMV_CTLS_CALLBACK_STACK_SIZE
typedef void (EMV_CTLS_CALLBACK_Func) (unsigned char *pucSend, unsigned short sSendSize, unsigned char *pucReceive, unsigned short *psReceiveSize, void *externalData);

/**
    @ingroup TLV_CALLBCK
    @brief pointer to #EMV_CTLS_CALLBACK_Func
*/
typedef EMV_CTLS_CALLBACK_Func* EMV_CTLS_CALLBACK_FnT;

/**
    @ingroup TLV_CALLBCK
    @brief avaiable stack-size in #EMV_CTLS_CALLBACK_Func (verix-only)
*/
#define EMV_CTLS_CALLBACK_STACK_SIZE 0xC000

#define FS_CALLBACK_CTLS_DATA_RECORD 0xFF  // currently max. 201 bytes (5 candidates in cbk_reduceCandidateList())


/*****************************************************************************
*  EMV_CTLS_Interface
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] dataIn input TLV buffer
/// @param[in] dataInLen length of dataIn
/// @param[out] dataOut output TLV buffer
/// @param[in,out] dataOutLen length of dataOut
/// @returns #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, or return code of EMV function given in @c dataIn
///
DLL_CLL EMV_ADK_INFO EMV_CTLS_Interface(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

/*****************************************************************************
*  EMV_CTLS_IF_Header
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] header for serial interface, 4 byte: CLA, INS, P1, P2
/// @param[in] dataIn input TLV buffer (may be NULL)
/// @param[in] dataInLen length of dataIn
/// @param[out] response output TLV buffer (may be NULL)
/// @param[in,out] responseLen length of dataOut
/// @returns #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, or return code of EMV function given in @c header
///
DLL_CLL EMV_ADK_INFO EMV_CTLS_IF_Header(const unsigned char* header, const unsigned char *dataIn, unsigned short dataInLen, unsigned char *response, unsigned short *responseLen);

/*****************************************************************************
*  EMV_CTLS_IF_BERTLV
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] header for serial interface, 4 byte: CLA, INS, P1, P2
/// @param[in] dataIn input TLV Node (may be NULL)
/// @param[out] dataOut output TLV (may be NULL and may be same as input)
/// @returns #EMV_ADK_INTERNAL, #EMV_ADK_TLV_BUILD_ERR, or return code of EMV function given in @c header
///
DLL_CLL EMV_ADK_INFO EMV_CTLS_IF_BERTLV(const unsigned char* header, const struct BTLVNode* dataIn,  struct BTLVNode* dataOut);

/*****************************************************************************
* EMV_CTLS_Disconnect
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Disconnect from EMV server
///
/// Since the EMV server handles only one client this function needs to be called
/// if the EMV control shall be handed over to different client process. Note:
/// This function has no effect, if not yet connected or not using the client/server
/// library.
/// @param[in]  options RFU
///
/// @author GSS R&D Germany
///
DLL_CLL void EMV_CTLS_Disconnect(unsigned char options);

/*****************************************************************************
* EMV_CTLS_SetClientMode
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Switch to client mode
///
/// There is only one static client library that uses the EMV framework library
/// by default. Alternatively it can connect via IPC to an EMV server. A third
/// mode - the gateway mode - can be used if there shall be only one channel
/// for both, contact and contactless.
///
/// @param[in]  mode  see @ref EMV_CLIENT_MODE
///
/// @author GSS R&D Germany
///
DLL_CLL void EMV_CTLS_SetClientMode(enum EMV_CLIENT_MODE mode);

/*****************************************************************************
* EMV_CTLS_SetCallback
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Set Callback Function and activate Callback Thread
///
/// @param[in] EMV_Callback callback function pointer, may be NULL
/// @param[in] context context data pointer which will be passed back with the callback
///
/// @author GSS R&D Germany
///
DLL_CLL void EMV_CTLS_SetCallback(EMV_CTLS_CALLBACK_FnT EMV_Callback, void* context);

/*****************************************************************************
* EMV_CTLS_SetCallback
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Getter to Init Options from EMV_CTLS_Init_Framework.
///
/// @author GSS R&D Germany
///
/// @return  Options which were set by EMV_CTLS_Init_Framework()
///
DLL_CLL unsigned long EMV_CTLS_GetInitOptions(void);

#ifdef __cplusplus
}     // extern "C"
#endif

#endif // _EMV_CTLS_LINK_H_
