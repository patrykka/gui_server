/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Interface definitions and functions
****************************************************************************/


/**
* @file
* @brief Interface of CTLS-Framework
*
*/


#ifndef _EMV_CTLS_SER_INTERFACE_H_   /* avoid double interface-includes */
#define _EMV_CTLS_SER_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif


#ifdef _VRXEVO
  #ifdef EMV_CLF_EXPORT
    #define DLL_CLF __declspec(dllexport)
  #else
    #define DLL_CLF __declspec(dllimport)
  #endif
#else
  #define DLL_CLF // NOTE: Do not set visibility("default") because export.map is used
#endif


#include "emv/EMV_Common_Interface.h"



DLL_CLF EMV_ADK_INFO EMV_CTLS_HandleCallback(unsigned char *data, unsigned short *dataLen);


/*****************************************************************************
*  EMV_CTLS_SerInterface
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] dataIn input TLV buffer
/// @param[in] dataInLen length of dataIn
/// @param[out] dataOut output TLV buffer
/// @param[in,out] dataOutLen length of dataOut
/// \returns EMV_ADK_OK, EMV_ADK_INTERNAL, EMV_ADK_PARAM
///
DLL_CLF EMV_ADK_INFO EMV_CTLS_SerInterface(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);


#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _EMV_CTLS_SER_INTERFACE_H_
