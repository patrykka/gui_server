/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Definitions and functions for serial interface
****************************************************************************/

#ifndef EMV_CTLS_XML_H   /* avoid double interface-includes */
  #define EMV_CTLS_XML_H


// ========================================================================================================
// === XML tags ===
// ========================================================================================================
/// @defgroup ADK_XML_TAGS Tags for configuration storage
/// @ingroup ADK_CONFIGURATION
/// @brief Used for storing the configuration in XML files.
/// @{

#define XML_TAG_TERMDATA                              "TerminalData"                       ///< constructed xml tag for terminal data @ref EMV_CTLS_TERMDATA_STRUCT, used in "EMV_CTLS_Terminal.xml"
#define XML_TAG_TERMDATA_TERM_TYP                     "TermTyp"                            ///< TLV tag #TAG_9F35_TRM_TYPE @n Struct element EMV_CTLS_TERMDATA_STRUCT::TermTyp
#define XML_TAG_TERMDATA_COUNTRY_CODE_TERM            "CountryCodeTerm"                    ///< TLV tag #TAG_9F1A_TRM_COUNTRY_CODE @n Struct element EMV_CTLS_TERMDATA_STRUCT::CountryCodeTerm
#define XML_TAG_TERMDATA_CURRENCY_TRANS               "CurrencyTrans"                      ///< TLV tag #TAG_5F2A_TRANS_CURRENCY @n Struct element EMV_CTLS_TERMDATA_STRUCT::CurrencyTrans
#define XML_TAG_TERMDATA_EXP_TRANS                    "ExpTrans"                           ///< TLV tag #TAG_5F36_TRANS_CURRENCY_EXP @n Struct element EMV_CTLS_TERMDATA_STRUCT::ExpTrans
#define XML_TAG_TERMDATA_SUPP_LANG                    "SuppLang"                           ///< TLV tag #TAG_SUPP_LANG @n Struct element EMV_CTLS_TERMDATA_STRUCT::SuppLang
#define XML_TAG_TERMDATA_IFD_SERIAL_NUMBER            "IFD_SerialNumber"                   ///< TLV tag #TAG_9F1E_IFD_SERIAL_NB @n Struct element EMV_CTLS_TERMDATA_STRUCT::IFDSerialNumber
#define XML_TAG_TERMDATA_FLOW_OPTIONS                 "FlowOptions"                        ///< TLV tag #TAG_DF8F0B_TERM_FLOW_OPTIONS @n Struct element EMV_CTLS_TERMDATA_STRUCT::FlowOptions,
#define XML_TAG_TERMDATA_MAXCTLS_TRANSLIMIT           "MaxCTLSTranslimit"                  ///< TLV tag #TAG_DF8F0E_TERM_MAXCTLS_TRANSLIMIT @n Struct element EMV_CTLS_TERMDATA_STRUCT::MaxCTLSTranslimit
#define XML_TAG_TERMDATA_BEEP_VOLUME                  "BeepVolume"                         ///< TLV tag #TAG_DF62_BUZZER_VOLUME @n Struct element EMV_CTLS_TERMDATA_STRUCT::BeepVolume
#define XML_TAG_TERMDATA_BEEP_FREQUENCY_SUCCESS       "BeepFrequencySuccess"               ///< TLV tag #TAG_DFD006_BEEP_FREQ_SUCCESS @n Struct element EMV_CTLS_TERMDATA_STRUCT::BeepFrequencySuccess
#define XML_TAG_TERMDATA_BEEP_FREQUENCY_ALERT         "BeepFrequencyAlert"                 ///< TLV tag #TAG_DFD007_BEEP_FREQ_ALERT @n Struct element EMV_CTLS_TERMDATA_STRUCT::BeepFrequencyAlert
#define XML_TAG_TERMDATA_SECOND_TAP_DELAY             "SecondTapDelay"                     ///< TLV tag #TAG_DFD008_SECOND_TAP_DELAY @n Struct element EMV_CTLS_TERMDATA_STRUCT::SecondTapDelay
#define XML_TAG_TERMDATA_HOST_COMM_TIMEOUT            "HostCommunicationTimeout"           ///< TLV tag #TAG_DFD009_HOST_COMM_TIMEOUT @n Struct element EMV_CTLS_TERMDATA_STRUCT::HostCommunicationTimeout
#define XML_TAG_TERMDATA_KERNEL_VERSION               "KernelVersion"                      ///< TLV tag #TAG_KERNEL_VERSION @n Struct element EMV_CTLS_TERMDATA_STRUCT::KernelVersion
#define XML_TAG_TERMDATA_FRAMEWORK_VERSION            "FrameworkVersion"                   ///< TLV tag #TAG_DF11_LIB_VERSION @n Struct element EMV_CTLS_TERMDATA_STRUCT::FrameworkVersion
#define XML_TAG_TERMDATA_L1DRIVER_VERSION             "L1DriverVersion"         		   ///< TLV tag #TAG_DF6F_L1DRIVER_VERSION @n Struct element EMV_CTLS_TERMDATA_STRUCT::L1DriverVersion


#define XML_TAG_TERMDATA_HOTLIST                      "Hotlist"              ///< Enclosing tag in "EMV_CTLS_HOTLIST.xml"
#define XML_TAG_TERMDATA_HOTLIST_RECORD               "Hot_Item"             ///< One record in "EMV_CTLS_HOTLIST.xml"
#define XML_TAG_TERMDATA_HOTLIST_PAN                  "PAN"                  ///< PAN data in "EMV_CTLS_HOTLIST.xml"
#define XML_TAG_TERMDATA_HOTLIST_SEQ_NUMBER           "PAN_Sequence_Number"  ///< PAN sequence number in "EMV_CTLS_HOTLIST.xml"


#define XML_TAG_CAP_KEYS                              "CapKeys"                            ///< constructed xml tag for CAP key data, used in "EMV_CTLS_Keys.xml"
#define XML_TAG_CAP_KEYS_CAPKEY                       "CapKey"                             ///< constructed xml tag for A SINGLE CAP key @ref EMV_CTLS_CAPKEY_STRUCT
#define XML_TAG_CAP_KEYS_INDEX                        "Index"                              ///< TLV tag #TAG_KEY_INDEX @n Struct element EMV_CTLS_CAPKEY_STRUCT::Index
#define XML_TAG_CAP_KEYS_RID                          "RID"                                ///< TLV tag #TAG_KEY_RID @n Struct element EMV_CTLS_CAPKEY_STRUCT::RID
#define XML_TAG_CAP_KEYS_KEY                          "Key"                                ///< TLV tag #TAG_KEY_KEY @n Struct element EMV_CTLS_CAPKEY_STRUCT::Key
#define XML_TAG_CAP_KEYS_KEYLEN                       "KeyLen"                             ///< Length of TLV tag #TAG_KEY_KEY @n Struct element EMV_CTLS_CAPKEY_STRUCT::KeyLen
#define XML_TAG_CAP_KEYS_EXPONENT                     "Exponent"                           ///< TLV tag #TAG_KEY_EXPONENT @n Struct element EMV_CTLS_CAPKEY_STRUCT::Exponent
#define XML_TAG_CAP_KEYS_HASH                         "Hash"                               ///< TLV tag #TAG_KEY_HASH @n Struct element EMV_CTLS_CAPKEY_STRUCT::Hash
#define XML_TAG_CAP_KEYS_REVOC_LIST                   "RevocationList"                     ///< TLV tag #TAG_KEY_CRL @n Struct element EMV_CTLS_CAPKEY_STRUCT::RevocEntries

#define XML_TAG_APPLIDATA                             "ApplicationData"                    ///< constructed xml tag for application data containing sequence of #XML_TAG_APPLIDATA_APP, used in "EMV_CTLS_Applications.xml"
#define XML_TAG_APPLIDATA_APP                         "Application"                        ///< Constructed tag for one AID, contains data from @ref EMV_CTLS_APPLI_STRUCT and @ref EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT
#define XML_TAG_APPLIDATA_AID                         "AID"                                ///< TLV tag #TAG_DF04_AID @n Struct element EMV_CTLS_APPLI_STRUCT::AID
#define XML_TAG_APPLIDATA_CL_MODES                    "CL_Modes"                           ///< TLV tag #TAG_C2_TRM_CL_MODES @n Struct element EMV_CTLS_APPLIDATA_STRUCT::CL_Modes
#define XML_TAG_APPLIDATA_APP_NAME                    "AppName"                            ///< TLV tag #TAG_50_APP_LABEL @n Struct element EMV_CTLS_APPLIDATA_STRUCT::AppName
#define XML_TAG_APPLIDATA_ASI                         "ASI"                                ///< TLV tag #TAG_DF20_ASI @n Struct element EMV_CTLS_APPLIDATA_STRUCT::ASI
#define XML_TAG_APPLIDATA_COUNTRY_CODE_TERM           "CountryCodeTerm"                    ///< TLV tag #TAG_5F28_ISS_COUNTRY_CODE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::App_CountryCodeTerm
#define XML_TAG_APPLIDATA_BR_KEY                      "BrKey"                              ///< TLV tag #TAG_9F15_MERCH_CATEG_CODE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::BrKey
#define XML_TAG_APPLIDATA_TERM_IDENT                  "TermIdent"                          ///< TLV tag #TAG_9F1C_TRM_ID @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TermIdent
#define XML_TAG_APPLIDATA_CL_CVM_SOFT_LIMIT           "CL_CVM_Soft_Limit"                  ///< TLV tag #TAG_C0_TRM_CL_CVM_LIMIT @n Struct element EMV_CTLS_APPLIDATA_STRUCT::CL_CVM_Soft_Limit
#define XML_TAG_APPLIDATA_CL_CEILING_LIMIT            "CL_Ceiling_Limit"                   ///< TLV tag #TAG_C1_TRM_CL_CEIL_LIMIT @n Struct element EMV_CTLS_APPLIDATA_STRUCT::CL_Ceiling_Limit
#define XML_TAG_APPLIDATA_FLOOR_LIMIT                 "FloorLimit"                         ///< TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT @n Struct element EMV_CTLS_APPLIDATA_STRUCT::FloorLimit
#define XML_TAG_CHPAPPLIDATA_VER_NUM                  "Chip_VerNumber"                     ///< TLV tag #TAG_9F09_TRM_APP_VERSION_NB @n Struct element EMV_CTLS_APPLIDATA_STRUCT::CHPVerNum
#define XML_TAG_MSRAPPLIDATA_VER_NUM                  "MSR_VerNumber"                      ///< TLV tag #TAG_9F6D_TRM_APP_VERSION_NB @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MSRVerNum
#define XML_TAG_APPLIDATA_ADD_VER_NUM                 "AdditionalVersioNumbers"            ///< TLV tag #TAG_DF5F_ADD_APP_VERSION @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Additional_Versions_No
#define XML_TAG_APPLIDATA_MERCH_IDENT                 "MerchIdent"                         ///< TLV tag #TAG_9F16_MERCHANT_ID @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MerchIdent
#define XML_TAG_APPLIDATA_AID_PRIO                    "AID_Prio"                           ///< TLV tag #TAG_DF1D_PRIO_APPLI @n Struct element EMV_CTLS_APPLIDATA_STRUCT::xAIDPrio
#define XML_TAG_APPLIDATA_SPECIAL_TRX                 "SpecialTRX"                         ///< TLV tag #TAG_DF1C_SPECIAL_TRX @n Struct element EMV_CTLS_APPLIDATA_STRUCT::xuc_Special_TRX
#define XML_TAG_APPLIDATA_APP_FLOW_CAP                "AppFlowCap"                         ///< TLV tag #TAG_DF2B_APP_FLOW_CAP @n Struct element EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap
#define XML_TAG_APPLIDATA_RETAP_FIELD_OFF             "RetapFieldOff"                      ///< TLV tag #TAG_DF30_RETAP_FIELD_OFF @n Struct element EMV_CTLS_APPLIDATA_STRUCT::RetapFieldOff
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM         "AdditionalTagsTRM"                  ///< TLV tag #TAG_DF29_ADD_TAGS @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_TRM
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD         "AdditionalTagsCRD"                  ///< TLV tag #TAG_DF2C_ADD_TAGS_CRD @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_CRD
#define XML_TAG_APPLIDATA_APP_TERM_CAP                "AppTermCap"                         ///< TLV tag #TAG_9F33_TRM_CAPABILITIES @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TermCap
#define XML_TAG_APPLIDATA_APP_TERM_ADD_CAP            "AppTermAddCap"                      ///< TLV tag #TAG_9F40_ADD_TRM_CAP @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TermAddCap
#define XML_TAG_APPLIDATA_APP_TERM_TYP                "AppTerminalType"                    ///< TLV tag #TAG_9F35_TRM_TYPE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::App_TermTyp
#define XML_TAG_APPLIDATA_TAC_DENIAL                  "TAC_Denial"                         ///< TLV tag #TAG_DF21_TAC_DENIAL @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TACDenial
#define XML_TAG_APPLIDATA_TAC_ONLINE                  "TAC_Online"                         ///< TLV tag #TAG_DF22_TAC_ONLINE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TACOnline
#define XML_TAG_APPLIDATA_TAC_DEFAULT                 "TAC_Default"                        ///< TLV tag #TAG_DF23_TAC_DEFAULT @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TACDefault
#define XML_TAG_APPLIDATA_DEFAULT_TDOL                "DefaultTDOL"                        ///< TLV tag #TAG_DF27_DEFAULT_TDOL @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Default_TDOL
#define XML_TAG_APPLIDATA_PP3_PMSG_TABLE              "PhoneMessageTable"                  ///< TLV tag #TAG_FB_PP3_PMSG_TABLE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::PhoneMessageTable
#define XML_TAG_APPLIDATA_MSR_CVM_ABOVE               "MagstripeCVM_aboveLimit"            ///< TLV tag #TAG_DF2F_MSR_CVM_ABOVE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MagstripeCVM_aboveLimit
#define XML_TAG_APPLIDATA_MSR_CVM_BELOW               "MagstripeCVM_belowLimit"            ///< TLV tag #TAG_DF10_MSR_CVM_BELOW @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MagstripeCVM_belowLimit
#define XML_TAG_APPLIDATA_CHIP_CVM_BELOW              "ChipCVM_belowLimit"                 ///< TLV tag #TAG_DF44_CHIP_CVM_BELOW @n Struct element EMV_CTLS_APPLIDATA_STRUCT::ChipCVM_belowLimit
#define XML_TAG_APPLIDATA_CEILING_LIMIT_MOBILE        "CL_Ceiling_LimitMobile"             ///< TLV tag #TAG_DF49_CEIL_LIMIT_MOBILE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::CL_Ceiling_LimitMobile
#define XML_TAG_APPLIDATA_TORN_TXN_LIFETIME           "Torn_TXN_Liftime"                   ///< TLV tag #TAG_DF45_CHIP_TXN_LIFETIME @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TRN_TXN_Lifetime
#define XML_TAG_APPLIDATA_TORN_TXN_NO                 "Torn_TXN_Number"                    ///< TLV tag #TAG_DF46_CHIP_TXN_NO @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TRN_TXN_Number
#define XML_TAG_APPLIDATA_TTC                         "Txn_Category_Code"                  ///< TLV tag #TAG_9F53_TRANS_CATEGORY_CODE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TXN_CategoryCode
#define XML_TAG_APPLIDATA_MERCH_NAME_LOCATION         "MerchantName_Location"              ///< TLV tag #TAG_9F4E_TAC_MERCHANTLOC @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MerchantName_Location
#define XML_TAG_APPLIDATA_VISA_TTQ                    "VisaTTQ"                            ///< TLV tag #TAG_9F66_TTQ @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TTQ
#define XML_TAG_APPLIDATA_VISA_DRL_PARAMS             "VisaDRLParams"                      ///< TLV tag #TAG_FA_VISA_DRL_RISK @n Struct element EMV_CTLS_APPLIDATA_STRUCT::VisaDRLParams
#define XML_TAG_APPLIDATA_VISA_DRL_INDEX              "Index"                              ///< EMV_CTLS_VISA_DRL_STRUCT::Index (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT         "Floorlimit"                         ///< EMV_CTLS_VISA_DRL_STRUCT::Floorlimit (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_VISA_DRL_PRG_ID_LEN         "AppPrgIdLen"                        ///< EMV_CTLS_VISA_DRL_STRUCT::ucAppPrgIdLen (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_VISA_DRL_APP_PRG_ID         "Application_PRG_ID"                 ///< EMV_CTLS_VISA_DRL_STRUCT::Application_PRG_ID (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT          "TXNlimit"                           ///< EMV_CTLS_VISA_DRL_STRUCT::TXNlimit (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT          "CVMlimit"                           ///< EMV_CTLS_VISA_DRL_STRUCT::CVMlimit (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_VISA_DRL_SWITCH             "FeatureSwitch"                      ///< EMV_CTLS_VISA_DRL_STRUCT::OnOffSwitch (included in TLV tag #TAG_FA_VISA_DRL_RISK)
#define XML_TAG_APPLIDATA_AMEX_DRL_PARAMS             "AmexDRLParams"                      ///< TLV tag #TAG_FD_AMEX_DRL_RISK @n Struct element EMV_CTLS_APPLIDATA_STRUCT::AmexDRLParams
#define XML_TAG_APPLIDATA_AMEX_DRL_INDEX              "Index"                              ///< EMV_CTLS_AMEX_DRL_STRUCT::Index (included in TLV tag #TAG_FD_AMEX_DRL_RISK)
#define XML_TAG_APPLIDATA_AMEX_DRL_FLOORLIMIT         "Floorlimit"                         ///< EMV_CTLS_AMEX_DRL_STRUCT::Floorlimit (included in TLV tag #TAG_FD_AMEX_DRL_RISK)
#define XML_TAG_APPLIDATA_AMEX_DRL_TXN_LIMIT          "TXNlimit"                           ///< EMV_CTLS_AMEX_DRL_STRUCT::TXNlimit (included in TLV tag #TAG_FD_AMEX_DRL_RISK)
#define XML_TAG_APPLIDATA_AMEX_DRL_CVM_LIMIT          "CVMlimit"                           ///< EMV_CTLS_AMEX_DRL_STRUCT::CVMlimit (included in TLV tag #TAG_FD_AMEX_DRL_RISK)
#define XML_TAG_APPLIDATA_AMEX_DRL_SWITCH             "FeatureSwitch"                      ///< EMV_CTLS_AMEX_DRL_STRUCT::OnOffSwitch (included in TLV tag #TAG_FD_AMEX_DRL_RISK)
#define XML_TAG_APPLIDATA_MTI_MERCHANT_TYPE_IND       "MTI_Merchant_Type_Ind"              ///< TLV tag #TAG_9F58_MERCH_TYPE_INDICATOR @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MTI_Merchant_Type_Ind
#define XML_TAG_APPLIDATA_TTI_TERM_TRANS_INFO         "TTI_Term_Trans_Info"                ///< TLV tag #TAG_9F59_TERM_TRANS_INFO @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TTI_Term_Trans_Info
#define XML_TAG_APPLIDATA_TTI_TERM_TRANS_TYPE         "TTT_Term_Trans_Type"                ///< TLV tag #TAG_9F5A_TERM_TRANS_TYPE @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TTT_Term_Trans_Type
#define XML_TAG_APPLIDATA_TOS_TERM_OPTION_STATUS      "TOS_Term_Option_Status"             ///< TLV tag #TAG_9F5E_TERM_OPTION_STATUS @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TOS_Term_Option_Status
#define XML_TAG_APPLIDATA_TERM_RECEIPT_REQUIRED_LIMIT "TERM_RCPT_REQLimit"                 ///< TLV tag #TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Term_CTLS_Receipt_REQLimit
#define XML_TAG_APPLIDATA_THRESHOLD_BCD               "Threshold_BCD"                      ///< TLV tag #TAG_DF24_THRESHHOLD @n Struct element EMV_CTLS_APPLIDATA_STRUCT::ThresholdBCD
#define XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE       "MaxTargetPercentage"                ///< TLV tag #TAG_DF25_MAXPERCENT_ONL @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MaxTargetPercentage
#define XML_TAG_APPLIDATA_TARGET_PERCENTAGE           "TargetPercentage"                   ///< TLV tag #TAG_DF26_PERCENT_ONL @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TargetPercentage
#define XML_TAG_APPLIDATA_TRY_AGAIN_LIMIT             "TryAgainLimit"                      ///< TLV tag #TAG_DF6D_TRY_AGAIN_LIMIT @n Struct element EMV_CTLS_APPLIDATA_STRUCT::TryAgainLimit
#define XML_TAG_AMEX_TERMINAL_CAPS                    "AMEX_Term_Caps"                     ///< TLV tag #TAG_9F6D_AMEX_CAPABILITIES @n Struct element EMV_CTLS_APPLIDATA_STRUCT::AMEX_TerminalCaps
#define XML_TAG_AMEX_ENH_TERMINAL_CAPS                "AMEX_Enhanced_Reader_Capabilties"   ///< TLV tag #TAG_9F6E_AMEX_ENHANCED_CAPABILITIES @n Struct element EMV_CTLS_APPLIDATA_STRUCT::AMEX_TerminalCaps
#define XML_TAG_APPLIDATA_CHKSUM_PARAMS               "ChksumParams"                       ///< TLV tag #TAG_DF13_TERM_PARAM @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Chksum_Params
#define XML_TAG_APPLIDATA_CHKSUM_ASCII_ENTRYPNT       "Chksum_EntryPoint"                  ///< TLV tag #TAG_DF3B_PARAMETER_1 @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Chksum_EntryPoint
#define XML_TAG_APPLIDATA_CHKSUM_ASCII_KERNEL         "Chksum_Kernel"                      ///< TLV tag #TAG_DF12_CHECKSUM @n Struct element EMV_CTLS_APPLIDATA_STRUCT::Chksum_Kernel
#define XML_TAG_APPLIDATA_MASTER_AID                  "MasterAID"                          ///< TLV tag #TAG_DF04_AID @n Struct element EMV_CTLS_APPLIDATA_STRUCT::MasterAID

// new config interface

#define XML_TAG_AD                                    "ApplicationData"  ///< constructed xml tag for application data containing sequence of #XML_TAG_AD_APP, used in "EMV_CTLS_Apps_SchemeSpecific.xml"
#define XML_TAG_AD_APP                                "Application"      ///< Constructed tag for one AID, contains data from @ref EMV_CTLS_APPLI_KERNEL_STRUCT and @ref EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT
#define XML_TAG_AD_AID                                "AID"              ///< Application ID, TLV tag #TAG_4F_APP_ID @n Struct element EMV_CTLS_APPLI_KERNEL_STRUCT::AID
#define XML_TAG_AD_KERNEL_ID                          "KernelID"         ///< Kernel ID, TLV tag #TAG_DFAB01_KERNEL_ID @n Struct element EMV_CTLS_APPLI_KERNEL_STRUCT::KernelID

// global

#define XML_TAG_AD_DFAB02_ASI                         "ASI_DFAB02"                            ///<  @n TLV tag: #TAG_DFAB02_ASI                @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ASI_DFAB02;
#define XML_TAG_AD_DFAB03_APP_FLOW_CAP                "AppFlowCap_DFAB03"                     ///<  @n TLV tag: #TAG_DFAB03_APP_FLOW_CAP       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AppFlowCap_DFAB03[5];
#define XML_TAG_AD_DFAB04_PRIO_APPS                   "PriorityApplications_DFAB04"           ///<  @n TLV tag: #TAG_DFAB04_PRIO_APPS          @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::PriorityApplications_DFAB04[EMV_ADK_MAX_PRIO_APP];
#define XML_TAG_AD_DFAB05_SPECIAL_TRX_CONFIG          "SpecialTRXConfig_DFAB05"               ///<  @n TLV tag: #TAG_DFAB05_SPECIAL_TRX_CFG    @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::SpecialTRXConfig_DFAB05[8];
#define XML_TAG_AD_DFAB06_CHKSUM_ENTRY_POINT          "ChksumEntryPoint_DFAB06"               ///<  @n TLV tag: #TAG_DFAB06_CHKSUM_EP          @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ChksumEntryPoint_DFAB06[EMV_ADK_CHECKSUM_ASCII_SIZE];
#define XML_TAG_AD_DFAB07_CHKSUM_KERNEL               "ChksumKernel_DFAB07"                   ///<  @n TLV tag: #TAG_DFAB07_CHKSUM_KERNEL      @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ChksumKernel_DFAB07[EMV_ADK_CHECKSUM_ASCII_SIZE];
#define XML_TAG_AD_DFAB08_RETAP_FIELD_OFF             "RetapFieldOff_DFAB08"                  ///<  @n TLV tag: #TAG_DFAB08_RETAP_FIELD_OFF    @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::RetapFieldOff_DFAB08;
#define XML_TAG_AD_DFAB20_ADD_TAGS_TRM                "AdditionalTagsTRM_DFAB20"              ///<  @n TLV tag: #TAG_DFAB20_ADD_TAGS_TRM       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsTRM_DFAB20;
#define XML_TAG_AD_DFAB21_ADD_TAGS_CRD                "AdditionalTagsCRD_DFAB21"              ///<  @n TLV tag: #TAG_DFAB21_ADD_TAGS_CRD       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsCRD_DFAB21;
#define XML_TAG_AD_DFAB22_DEFAULT_APP_NAME            "DefaultApplicationName_DFAB22"         ///<  @n TLV tag: #TAG_DFAB22_DEF_APP_NAME       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::DefaultApplicationName_DFAB22[16+1];
#define XML_TAG_AD_DFAB23_INTERNAL_KERNEL_ID          "InternalKernelId_DFAB23"               ///<  @n TLV tag: #TAG_DFAB23_INTERNAL_KERNEL_ID @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::InternalKernelId_DFAB23;

// MasterCard

#define XML_TAG_AD_MK                                 "MasterCard"                            ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_MK_STRUCT
#define XML_TAG_AD_GIROCARD                           "girocard"                              ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_MK_STRUCT (for German domestic scheme "Girocard")
#define XML_TAG_AD_MK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                        ///< TLV tag: #TAG_9F1C_TRM_ID                                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_MK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"              ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE                          @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_MK_9F35_TERM_TYPE                  "TerminalType_9F35"                     ///< TLV tag: #TAG_9F35_TRM_TYPE                                  @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_MK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"   ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                               @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_MK_DF811E_MSR_CVM_ABOVE_LIMIT      "MagstripeCVM_aboveLimit_DF811E"        ///< TLV tag: #TAG_DF811E_MSR_CVM_ABOVE_LIMIT                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MagstripeCVM_aboveLimit_DF811E;
#define XML_TAG_AD_MK_DF812C_MSR_CVM_BELOW_LIMIT      "MagstripeCVM_belowLimit_DF812C"        ///< TLV tag: #TAG_DF812C_MSR_CVM_BELOW_LIMIT                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MagstripeCVM_belowLimit_DF812C;
#define XML_TAG_AD_MK_DF8118_CHP_CVM_ABOVE_LIMIT      "ChipCVM_aboveLimit_DF8118"             ///< TLV tag: #TAG_DF8118_CVM_CAPABILITY__CVM_REQUIRED            @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ChipCVM_aboveLimit_DF8118;
#define XML_TAG_AD_MK_DF8119_CHP_CVM_BELOW_LIMIT      "ChipCVM_belowLimit_DF8119"             ///< TLV tag: #TAG_DF8119_CVM_CAPABILITY__NO_CVM_REQUIRED         @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ChipCVM_belowLimit_DF8119;
#define XML_TAG_AD_MK_DF811F_SECURITY_CAP             "SecurityCapability_DF811F"             ///< TLV tag: #TAG_DF811F_SECURITY_CAPABILITY                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::SecurityCapability_DF811F;
#define XML_TAG_AD_MK_DF8117_CARD_DATA_INPUT_CAP      "CardDataInputCapability_DF8117"        ///< TLV tag: #TAG_DF8117_CARD_DATA_INPUT_CAPABILITY              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::CardDataInputCapability_DF8117;
#define XML_TAG_AD_MK_DF8123_FLOOR_LIMIT              "FloorLimit_DF8123"                     ///< TLV tag: #TAG_DF8123_FLOOR_LIMIT                             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::FloorLimit_DF8123[6];
#define XML_TAG_AD_MK_DF8124_TRX_LIMIT_NO_ON_DEVICE   "TransactionLimitNoOnDevice_DF8124"     ///< TLV tag: #TAG_DF8124_READER_CTLS_TRX_LIMIT__NO_ON_DEV_CVM    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionLimitNoOnDevice_DF8124[6];
#define XML_TAG_AD_MK_DF8125_TRX_LIMIT_ON_DEVICE      "TransactionLimitOnDevice_DF8125"       ///< TLV tag: #TAG_DF8125_READER_CTLS_TRX_LIMIT__ON_DEVICE_CVM    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionLimitOnDevice_DF8125[6];
#define XML_TAG_AD_MK_DF8126_CVM_REQUIRED_LIMIT       "CVMRequiredLimit_DF8126"               ///< TLV tag: #TAG_DF8126_CVM_REQUIRED_LIMIT                      @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::CVMRequiredLimit_DF8126[6];
#define XML_TAG_AD_MK_9F09_CHP_VERSION_NUMBER         "ChipVersionNumber_9F09"                ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ChipVersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_MK_9F6D_MSR_VERSION_NUMBER         "MSRVersionNumber_9F6D"                 ///< TLV tag: #TAG_9F6D_TRM_APP_MSR_VERSION_NB                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MSRVersionNumber_9F6D[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_MK_DF811B_KERNEL_CONFIG            "KernelConfiguration_DF811B"            ///< TLV tag: #TAG_DF811B_KERNEL_CONFIGURATION                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::KernelConfiguration_DF811B;
#define XML_TAG_AD_MK_9F53_TRX_CATEGORY_CODE          "TransactionCategoryCode_9F53"          ///< TLV tag: #TAG_9F53_TRANS_CATEGORY_CODE                       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionCategoryCode_9F53;
#define XML_TAG_AD_MK_DF8120_TAC_DEFAULT              "TACDefault_DF8120"                     ///< TLV tag: #TAG_DF8120_TAC_DEFAULT                             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TACDefault_DF8120[5];
#define XML_TAG_AD_MK_DF8121_TAC_DENIAL               "TACDenial_DF8121"                      ///< TLV tag: #TAG_DF8121_TAC_DENIAL                              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TACDenial_DF8121[5];
#define XML_TAG_AD_MK_DF8122_TAC_ONLINE               "TACOnline_DF8122"                      ///< TLV tag: #TAG_DF8122_TAC_ONLINE                              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TACOnline_DF8122[5];
#define XML_TAG_AD_MK_DF810C_KERNEL_ID                "KernelID_DF810C"                       ///< TLV tag: #TAG_DF810C_KERNEL_ID                               @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::KernelID_DF810C;
#define XML_TAG_AD_MK_9F1D_TRM_RISK_MGMT_DATA         "TerminalRiskManagementData_9F1D"       ///< TLV tag: #TAG_9F1D_TRM_RISK_MNGT_DATA                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalRiskManagementData_9F1D[8];
#define XML_TAG_AD_MK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"             ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE                          @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_MK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"               ///< TLV tag: #TAG_9F16_MERCHANT_ID                               @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_MK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"          ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC                           @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_MK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"               ///< TLV tag: #TAG_9F01_ACQ_ID                                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_MK_DF8130_HOLD_TIME_VALUE          "HoldTimeValue_DF8130"                  ///< TLV tag: #TAG_DF8130_HOLD_TIME_VALUE                         @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::HoldTimeValue_DF8130;
#define XML_TAG_AD_MK_DF812D_MESSAGE_HOLD_TIME        "MessageHoldTime_DF812D"                ///< TLV tag: #TAG_DF812D_MESSAGE_HOLD_TIME                       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MessageHoldTime_DF812D[3];
#define XML_TAG_AD_MK_DF811C_TORN_TRX_LIFETIME        "TornTransactionLifetime_DF811C"        ///< TLV tag: #TAG_DF811C_TRN_TXN_LIFETIME                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TornTransactionLifetime_DF811C[2];
#define XML_TAG_AD_MK_DF811D_TORN_TRX_NUMBER          "TornTransactionNumber_DF811D"          ///< TLV tag: #TAG_DF811D_TRN_TXN_NUMBER                          @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TornTransactionNumber_DF811D;
#define XML_TAG_AD_MK_DF8131_PHONE_MSG_TABLE          "PhoneMessageTable_DF8131"              ///< TLV tag: #TAG_DF8131_PHONE_MSG_TABLE                         @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::PhoneMessageTable_DF8131;
#define XML_TAG_AD_MK_DF8112_TAGS_TO_READ                  "TagsToRead_DF8112"                ///< TLV tag: #TAG_DF8112_TAGS_TO_READ                            @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToRead_DF8112;
#define XML_TAG_AD_MK_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC   "TagsToWriteBeforeGenAC_FF8102"    ///< TLV tag: #TAG_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToWriteBeforeGenAC_FF8102;
#define XML_TAG_AD_MK_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC    "TagsToWriteAfterGenAC_FF8103"     ///< TLV tag: #TAG_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToWriteAfterGenAC_FF8103;
#define XML_TAG_AD_MK_DF8110_PROCEED_TO_FIRST_WRITE_FLAG   "ProceedToFirstWriteFlag_DF8110"   ///< TLV tag: #TAG_DF8110_PROCEED_TO_FIRST_WRITE_FLAG             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ProceedToFirstWriteFlag_DF8110;
#define XML_TAG_AD_MK_9F5C_DS_REQUESTED_OPERATOR_ID        "DSRequestedOperatorID_9F5C"       ///< TLV tag: #TAG_9F5C_DS_REQUESTED_OPERATOR_ID                  @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::DSRequestedOperatorID_9F5C;
#define XML_TAG_AD_MK_DF8127_DE_TIMEOUT_VALUE              "DETimeoutValue_DF8127"            ///< TLV tag: #TAG_DF8127_DE_TIMEOUT_VALUE                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::DETimeoutValue_DF8127;
#define XML_TAG_AD_MK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                     ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP                            @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_MK_DF8132_RR_MIN_GRACE_PERIOD      "RR_MinGracePeriod_DF8132"              ///< TLV tag: #TAG_DF8132_RR_MIN_GRACE_PERIOD                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_MinGracePeriod_DF8132
#define XML_TAG_AD_MK_DF8133_RR_MAX_GRACE_PREIOD      "RR_MaxGracePeriod_DF8133"              ///< TLV tag: #TAG_DF8133_RR_MAX_GRACE_PERIOD                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_MaxGracePeriod_DF8133
#define XML_TAG_AD_MK_DF8134_RR_EXP_TRANS_TIME_CAPDU  "RR_ExpTransTimeCAPDU_DF8134"           ///< TLV tag: #TAG_DF8134_RR_TERM_EXPECTED_TRANS_TIME_CAPDU       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_ExpectedTransTime_CAPDU_DF8134
#define XML_TAG_AD_MK_DF8135_RR_EXP_TRANS_TIME_RAPDU  "RR_ExpTransTimeRAPDU_DF8135"           ///< TLV tag: #TAG_DF8135_RR_TERM_EXPECTED_TRANS_TIME_RAPDU       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_ExpectedTransTime_RAPDU_DF8135
#define XML_TAG_AD_MK_DF8136_RR_ACCURACY_THRESHOLD    "RR_AccuracyThreshold_DF8136"           ///< TLV tag: #TAG_DF8136_RR_ACCURACY_THRESHOLD                   @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_AccuracyThreshold_DF8136
#define XML_TAG_AD_MK_DF8137_RR_TT_MISMATCH_THRESHOLD "RR_TransTimeMismatchThreshold_DF8137"  ///< TLV tag: #TAG_DF8137_RR_TRANS_TIME_MISMATCH_THRESHOLD        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_TransTimeMismatchThreshold_DF8137
#define XML_TAG_AD_MK_9F7C_MERCHANT_CUSTOM_DATA       "MerchantCustomData_9F7C"               ///< TLV tag: #TAG_9F7C_MERCHANT_CUSTOM_DATA                      @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantCustomData_9F7C

// Visa
#define XML_TAG_AD_VK                                 "Visa"                                  ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_VK_STRUCT
#define XML_TAG_AD_VK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                        ///< TLV tag: #TAG_9F1C_TRM_ID                           @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_VK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"              ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_VK_9F35_TERM_TYPE                  "TerminalType_9F35"                     ///< TLV tag: #TAG_9F35_TRM_TYPE                         @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_VK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"     ///< TLV tag: #TAG_9F66_TTQ                              @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_VK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"             ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_VK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"   ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                      @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_VK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                    ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB               @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_VK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"             ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_VK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"               ///< TLV tag: #TAG_9F16_MERCHANT_ID                      @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_VK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"          ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC                  @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_VK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                     ///< TLV tag: #TAG_DFAB30_TEC_SUPPORT                    @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_VK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                     ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP                   @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_VK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"          ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT               @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_VK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"    ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_VK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"    ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_VK_FFAB01_VISA_DRL_PARAMS          "VisaDRLParams_FFAB01"                  ///< TLV tag: #TAG_FFAB01_DRL_PARAMETER                  @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::VisaDRLParams_FFAB01;
#define XML_TAG_AD_VK_9F5A_APP_PROGRAM_ID             "AppProgramId_9F5A"                     ///< TLV tag: #TAG_9F5A_APP_PROGRAM_ID                   @n Struct: #EMV_CTLS_VK_DRL_ENTRY_STRUCT::AppProgramId_9F5A[16]
#define XML_TAG_AD_VK_DFAB49_ON_OFF_SWITCH            "OnOffSwitch_DFAB49"                    ///< TLV tag: #TAG_DFAB49_DRL_SWITCHES                   @n Struct: #EMV_CTLS_VK_DRL_ENTRY_STRUCT::OnOffSwitch_DFAB49

// American Express

#define XML_TAG_AD_AK                                 "AmericanExpress"                           ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_AK_STRUCT
#define XML_TAG_AD_AK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                            ///< TLV tag: #TAG_9F1C_TRM_ID                       @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_AK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                  ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_AK_9F35_TERM_TYPE                  "TerminalType_9F35"                         ///< TLV tag: #TAG_9F35_TRM_TYPE                     @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_AK_9F6D_AMEX_CTLS_READER_CAPS      "AmexContactlessReaderCapabilities_9F6D"    ///< TLV tag: #TAG_9F6D_AMEX_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AmexContactlessReaderCapabilities_9F6D;
#define XML_TAG_AD_AK_9F6E_AMEX_TERM_TRX_CAPS         "AmexTerminalTransactionCapabilities_9F6E"  ///< TLV tag: #TAG_9F6E_AMEX_ENHANCED_CAPABILITIES   @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AmexTerminalTransactionCapabilities_9F6E[4];
#define XML_TAG_AD_AK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                 ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_AK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"       ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                  @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_AK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                        ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB           @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_AK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                 ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_AK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                   ///< TLV tag: #TAG_9F16_MERCHANT_ID                  @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_AK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"              ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC              @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_AK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                   ///< TLV tag: #TAG_9F01_ACQ_ID                       @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_AK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                         ///< TLV tag: #TAG_DFAB30_TEC_SUPPORT                @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_AK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                         ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP               @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_AK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"              ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT           @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_AK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"        ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_AK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"        ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT         @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_AK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                         ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT                @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_AK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                          ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                 @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_AK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                          ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                 @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_AK_FFAB01_AMEX_DRL_PARAMS          "AmexDRLParams_FFAB01"                      ///< TLV tag: #TAG_FFAB01_DRL_PARAMETER              @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AmexDRLParams_FFAB01;
#define XML_TAG_AD_AK_DFAB49_ON_OFF_SWITCH            "OnOffSwitch_DFAB49"                        ///< TLV tag: #TAG_DFAB49_DRL_SWITCHES               @n Struct: #EMV_CTLS_AK_DRL_ENTRY_STRUCT::OnOffSwitch_DFAB49
#define XML_TAG_AD_AK_DFAB52_UN_RANGE                 "UnpredictableNumberRange_DFAB52"           ///< TLV tag: #TAG_DFAB52_AMEX_UN_RANGE              @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::UnpredictableNumberRange_DFAB52

// JCB

#define XML_TAG_AD_JK                                 "JCB"                                            ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_JK_STRUCT
#define XML_TAG_AD_JK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                 ///< TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_JK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                       ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_JK_9F35_TERM_TYPE                  "TerminalType_9F35"                              ///< TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_JK_9F53_TERM_INTERCHANGE_PROFILE   "TerminalInterchangeProfile_9F53"                ///< TLV tag: #TAG_9F53_TRM_INTERCHANGE_PROFILE     @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalInterchangeProfile_9F53[3];
#define XML_TAG_AD_JK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                      ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_JK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                   ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC             @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_JK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                        ///< TLV tag: #TAG_9F01_ACQ_ID                      @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_JK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                              ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_JK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                   ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_JK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"             ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_JK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"             ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_JK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                              ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_JK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                               ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_JK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                               ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_JK_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                 ///< TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD       @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_JK_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"          ///< TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT       @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_JK_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"   ///< TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT   @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;
#define XML_TAG_AD_JK_DFAB4B_COMBINATION_OPTIONS      "CombinationOptions_DFAB4B"                      ///< TLV tag: #TAG_DFAB4B_COMBINATION_OPTIONS       @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::CombinationOptions_DFAB4B[2];
#define XML_TAG_AD_JK_DFAB4C_REMOVAL_TIMEOUT          "RemovalTimeout_DFAB4C"                          ///< TLV tag: #TAG_DFAB4C_REMOVAL_TIMEOUT           @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RemovalTimeout_DFAB4C[2];
// Discover

#define XML_TAG_AD_DK                                 "Discover"                                       ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_DK_STRUCT
#define XML_TAG_AD_DK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                 ///< TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_DK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                       ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_DK_9F35_TERM_TYPE                  "TerminalType_9F35"                              ///< TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_DK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"              ///< TLV tag: #TAG_9F66_TTQ                         @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_DK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                      ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_DK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"            ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                 @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_DK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                             ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_DK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                      ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_DK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                              ///< TLV tag: #TAG_DFAB30_TEC_SUPPORT               @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_DK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                              ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_DK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                   ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_DK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"             ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_DK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"             ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_DK_DFAB58_CONTAINER_IDS            "DataContainerReadIds_DFAB58"                    ///< TLV tag: #TAG_DFAB58_ID_LIST                   @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::DataContainerReadIds_DFAB58
#define XML_TAG_AD_DK_DFAB59_CONTAINER_RANGES         "DataContainerReadRanges_DFAB59"                 ///< TLV tag: #TAG_DFAB59_RANGE_LIST                @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::DataContainerReadRanges_DFAB59

// Interac

#define XML_TAG_AD_IK                                 "Interac"                                        ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_IK_STRUCT
#define XML_TAG_AD_IK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                 ///< TLV tag: #TAG_9F1C_TRM_ID                              @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_IK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                       ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_IK_9F35_TERM_TYPE                  "TerminalType_9F35"                              ///< TLV tag: #TAG_9F35_TRM_TYPE                            @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_IK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                      ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_IK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"            ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                         @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_IK_9F5F_CTLS_FLOOR_LIMIT           "ContactlessFloorLimit_9F5F"                     ///< TLV tag: #TAG_9F5F_READER_CTLS_FLOOR_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessFloorLimit_9F5F[6];
#define XML_TAG_AD_IK_9F58_MERCHANT_TYPE_INDICATOR    "MerchantTypeIndicator_9F58"                     ///< TLV tag: #TAG_9F58_MERCH_TYPE_INDICATOR                @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantTypeIndicator_9F58;
#define XML_TAG_AD_IK_9F59_TERM_TRX_INFORMATION       "TerminalTransactionInformation_9F59"            ///< TLV tag: #TAG_9F59_TERM_TRANS_INFO                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalTransactionInformation_9F59[3];
#define XML_TAG_AD_IK_9F5A_TERM_TRX_TYPE              "TerminalTransactionType_9F5A"                   ///< TLV tag: #TAG_9F5A_TERM_TRANS_TYPE                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalTransactionType_9F5A;
#define XML_TAG_AD_IK_9F5E_TERM_OPTION_STATUS         "TerminalOptionStatus_9F5E"                      ///< TLV tag: #TAG_9F5E_TERM_OPTION_STATUS                  @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalOptionStatus_9F5E[2];
#define XML_TAG_AD_IK_9F5D_RECEIPT_REQ_LIMIT          "ReceiptRequiredLimit_9F5D"                      ///< TLV tag: #TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ReceiptRequiredLimit_9F5D[6];
#define XML_TAG_AD_IK_DF6D_TRY_AGAIN_LIMIT            "TryAgainLimit_DF6D"                             ///< TLV tag: #TAG_DF6D_TRY_AGAIN_LIMIT                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TryAgainLimit_DF6D;
#define XML_TAG_AD_IK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                             ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB                  @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_IK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                      ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_IK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                        ///< TLV tag: #TAG_9F16_MERCHANT_ID                         @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_IK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                   ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_IK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                        ///< TLV tag: #TAG_9F01_ACQ_ID                              @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_IK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                              ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP                      @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_IK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"             ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_IK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"             ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT                @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_IK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                              ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT                       @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_IK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                               ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                        @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_IK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                               ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                        @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_IK_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                 ///< TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD               @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_IK_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"          ///< TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT               @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_IK_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"   ///< TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT           @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;

// EPAL

#define XML_TAG_AD_EK                                 "EPAL"                                         ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_EK_STRUCT
#define XML_TAG_AD_EK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                               ///< TLV tag: #TAG_9F1C_TRM_ID                  @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_EK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                     ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_EK_9F35_TERM_TYPE                  "TerminalType_9F35"                            ///< TLV tag: #TAG_9F35_TRM_TYPE                @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_EK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                    ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_EK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"          ///< TLV tag: #TAG_9F40_ADD_TRM_CAP             @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_EK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                           ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB      @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_EK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                    ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_EK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                      ///< TLV tag: #TAG_9F16_MERCHANT_ID             @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_EK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                 ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC         @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_EK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                      ///< TLV tag: #TAG_9F01_ACQ_ID                  @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_EK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"            ///< TLV tag: #TAG_9F66_TTQ                     @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalTransactionQualifier_9F66
#define XML_TAG_AD_EK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                            ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP          @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_EK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                 ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT      @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_EK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"           ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_EK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                            ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT           @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_EK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                             ///< TLV tag: #TAG_DFAB44_TAC_DENIAL            @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_EK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                             ///< TLV tag: #TAG_DFAB45_TAC_ONLINE            @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_EK_DFAB4A_CTLS_TRX_LIMIT_CASH      "CtlsTransactionLimitCash_DFAB4A"              ///< TLV tag: #TAG_DFAB4A_CTLS_TRX_LIMIT_CASH   @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::CtlsTransactionLimitCash_DFAB4A[6];

// Visa Asia/Pacific

#define XML_TAG_AD_PK                                 "VisaAsiaPacific"                               ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_PK_STRUCT
#define XML_TAG_AD_PK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                 @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_PK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE       @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_PK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE               @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_PK_CVM_REQUIREMENTS                "CvmRequirements_DF04"                          ///< TLV tag: #TAG_DF04_PK_CVM_REQUIREMENTS    @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::CvmRequirements_DF04;
#define XML_TAG_AD_PK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP         @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_PK_DF02_CTLS_FLOOR_LIMIT           "ContactlessFloorLimit_DF02"                    ///< TLV tag: #TAG_DF02_PK_FLOOR_LIMIT         @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessFloorLimit_DF02[6];
#define XML_TAG_AD_PK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_PK_DF01_CTLS_CVM_REQ_LIMIT         "ContactlessCVMRequiredLimit_DF01"              ///< TLV tag: #TAG_DF01_PK_CVM_REQ_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessCVMRequiredLimit_DF01[6];

// CUP

#define XML_TAG_AD_CK                                 "ChinaUnionPay"                                 ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_CK_STRUCT
#define XML_TAG_AD_CK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                 @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_CK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_CK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE               @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_CK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"             ///< TLV tag: #TAG_9F66_TTQ                    @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_CK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_CK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///< TLV tag: #TAG_9F40_ADD_TRM_CAP            @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_CK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB     @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_CK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_CK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                       ///< TLV tag: #TAG_9F16_MERCHANT_ID            @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_CK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC        @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_CK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                             ///< TLV tag: #TAG_DFAB30_TEC_SUPPORT          @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_CK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP         @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_CK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT     @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_CK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_CK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT   @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];

// Gemalto

#define XML_TAG_AD_GK                                 "Gemalto"                                       ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_GK_STRUCT
#define XML_TAG_AD_GK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                     @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_GK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE           @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_GK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE                   @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_GK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES           @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_GK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_GK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB         @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_GK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE           @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_GK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                       ///< TLV tag: #TAG_9F16_MERCHANT_ID                @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_GK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC            @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_GK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                       ///< TLV tag: #TAG_9F01_ACQ_ID                     @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_GK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP             @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_GK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT         @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_GK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT           @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_GK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_GK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT              @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_GK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///< TLV tag: #TAG_DFAB44_TAC_DENIAL               @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_GK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///< TLV tag: #TAG_DFAB45_TAC_ONLINE               @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_GK_DFAB4F_CTLS_APP_KERN_CAP        "CtlsAppKernelCap_DFAB4F"                       ///< TLV tag: #TAG_DFAB4F_PURE_CTLS_APP_KERN_CAP   @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::CtlsAppKernelCap_DFAB4F
#define XML_TAG_AD_GK_DFAB50_MTOL                     "MTOL_DFAB50"                                   ///< TLV tag: #TAG_DFAB50_PURE_MTOL                @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MTOL_DFAB50
#define XML_TAG_AD_GK_DFAB51_DEFAULT_DDOL             "DefaultDDOL_DFAB51"                            ///< TLV tag: #TAG_DFAB51_CTLS_DEFAULT_DDOL        @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::DefaultDDOL_DFAB51
#define XML_TAG_AD_GK_9F76_TERMINAL_TRX_DATA          "TerminalTransactionData_9F76"                  ///< TLV tag: #TAG_9F76_PURE_TERM_TRX_DATA         @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalTransactionData_9F76
#define XML_TAG_AD_GK_DFAB5A_APPLI_AUTH_TRX_TYPE      "AppliAuthTransType_DFAB5A"                     ///< TLV tag: #TAG_DFAB5A_PURE_APPLI_AUTH_TRX_TYPE @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AppliAuthTransType_DFAB5A

// RuPay

#define XML_TAG_AD_RK                                 "RuPay"                                         ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_RK_STRUCT
#define XML_TAG_AD_RK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_RK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_RK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_RK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_RK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                 @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_RK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_RK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_RK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_RK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_RK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_RK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_RK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_RK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_RK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_RK_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                ///< TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD       @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_RK_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"         ///< TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT       @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_RK_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"  ///< TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT   @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;
#define XML_TAG_AD_RK_DFAB4D_CALLBACK_TIMEOUT         "CallbackTimeout_DFAB4D"                        ///< TLV tag: #TAG_DFAB4D_RUPAY_CALLBACK_TIMEOUT    @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::CallbackTimeout_DFAB4D[2];
#define XML_TAG_AD_RK_DFAB4E_TORN_TRX_INTERVAL        "TornTransactionInterval_DFAB4E"                ///< TLV tag: #TAG_DFAB4E_RUPAY_TORN_TRX_INTERVAL   @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TornTransactionInterval_DFAB4E[2];
#define XML_TAG_AD_RK_DF3A_ADD_TERM_CAPS_EXT          "AdditionalTerminalCapabilitiesExt_DF3A"        ///< TLV tag: #TAG_DF3A_ADD_TRM_CAP_EXT             @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::AdditionalTerminalCapabilitiesExt_DF3A[5];

// SIBS

#define XML_TAG_AD_SK                                 "SIBS"                                          ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_SK_STRUCT
#define XML_TAG_AD_SK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_SK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_SK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_SK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///< TLV tag: #TAG_9F33_TRM_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_SK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///< TLV tag: #TAG_9F40_ADD_TRM_CAP                 @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_SK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_SK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_SK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC             @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_SK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_SK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_SK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_SK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_SK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_SK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_SK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_SK_DFAB53_TERMCAP_ABOVE_CVM_LIMIT  "TermCap_aboveCVMLimit_DFAB53"                  ///< TLV tag: #TAG_DFAB53_TERMCAP_ABOVE_CVM_LIMIT   @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TermCap_aboveCVMLimit_DFAB53[3];
#define XML_TAG_AD_SK_DFAB54_TERMCAP_BELOW_CVM_LIMIT  "TermCap_belowCVMLimit_DFAB54"                  ///< TLV tag: #TAG_DFAB54_TERMCAP_BELOW_CVM_LIMIT   @n Struct: #EMV_CTLS_APPLIDATA_SK_STRUCT::TermCap_belowCVMLimit_DFAB54[3];

// PagoBancomat

#define XML_TAG_AD_PB                                 "PagoBancomat"                                  ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_PB_STRUCT
#define XML_TAG_AD_PB_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_PB_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_PB_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_PB_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"             ///< TLV tag: #TAG_9F66_TTQ                         @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_PB_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_PB_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC             @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_PB_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_PB_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///< TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_PB_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_PB_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///< TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_PB_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_PB_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_PB_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_PB_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                ///< TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD       @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_PB_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"         ///< TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT       @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_PB_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"  ///< TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT   @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;
#define XML_TAG_AD_PB_DFAB55_TAC_SWITCH_INTERFACE     "TACSwitchInterface_DFAB55"                     ///< TLV tag: #TAG_DFAB55_TAC_SWITCH_INTERFACE      @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::TACSwitchInterface_DFAB55[5];
#define XML_TAG_AD_PB_DFAB56_IAC_SWITCH_INTERFACE     "IACSwitchInterface_DFAB56"                     ///< TLV tag: #TAG_DFAB56_IAC_SWITCH_INTERFACE      @n Struct: #EMV_CTLS_APPLIDATA_PB_STRUCT::IACSwitchInterface_DFAB56[5];

// MIR

#define XML_TAG_AD_MR                                 "MIR"                                           ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_MR_STRUCT
#define XML_TAG_AD_MR_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///< TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_MR_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///< TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_MR_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///< TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_MR_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///< TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_MR_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///< TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_MR_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///< TLV tag: #TAG_9F4E_TAC_MERCHANTLOC             @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_MR_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                       ///< TLV tag: #TAG_9F01_ACQ_ID                      @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_MR_DF51_TRM_FLOOR_LIMIT            "TerminalFloorLimit_DF51"                       ///< TLV tag: #TAG_DF51_TRM_FLOOR_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalFloorLimit_DF51[6];
#define XML_TAG_AD_MR_DF52_TRM_NOCVM_LIMIT            "TerminalNoCVMLimit_DF52"                       ///< TLV tag: #TAG_DF52_TRM_NOCVM_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalNoCVMLimit_DF52[6];
#define XML_TAG_AD_MR_DF53_TRM_CTLS_LIMIT_NON_CDCVM   "TerminalContactlessLimitNonCDCVM_DF53"         ///< TLV tag: #TAG_DF53_TRM_CTLS_LIMIT_NON_CDCVM    @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalContactlessLimitNonCDCVM_DF53[6];
#define XML_TAG_AD_MR_DF54_TRM_CTLS_LIMIT_CDCVM       "TerminalContactlessLimitCDCVM_DF54"            ///< TLV tag: #TAG_DF54_TRM_CTLS_LIMIT_CDCVM        @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalContactlessLimitCDCVM_DF54[6];
#define XML_TAG_AD_MR_DF55_TRM_TPM_CAPABILITIES       "TerminalTPMCapabilities_DF55"                  ///< TLV tag: #TAG_DF55_TRM_TPM_CAPABILITIES        @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TerminalTPMCapabilities_DF55[2];
#define XML_TAG_AD_MR_DF56_TRANSACTION_RECOVERY_LIMIT "TransactionRecoveryLimit_DF56"                 ///< TLV tag: #TAG_DF56_TRANSACTION_RECOVERY_LIMIT  @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TransactionRecoveryLimit_DF56;
#define XML_TAG_AD_MR_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_MR_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///< TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_MR_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///< TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_MR_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///< TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_MR_DFAB57_MIR_DATA_EXCHANGE_TAGS   "DataExchangeTagList_DFAB57"                    ///< TLV tag: #TAG_DFAB57_MIR_DATA_EXCHANGE_TAGS    @n Struct: #EMV_CTLS_APPLIDATA_MR_STRUCT::DataExchangeTagList_DFAB57;

// Domestic

#define XML_TAG_AD_DOM                                "domestic"                                      ///< Tag to include data of @ref EMV_CTLS_APPLIDATA_DOM_STRUCT
#define XML_TAG_AD_DOM_DFAB31_APP_FLOW_CAP            "AppFlowCap_DFAB31"                             ///< TLV tag: #TAG_DFAB31_APP_FLOW_CAP         @n Struct: #EMV_CTLS_APPLIDATA_DOM_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_DOM_DFAB41_CTLS_TRX_LIMIT          "ContactlessTransactionLimit_DFAB41"            ///< TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_DOM_STRUCT::ContactlessTransactionLimit_DFAB41[6];

#define XML_TAG_VTM                                   "VirtualTerminalMap"  ///< Enclosing tag used in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_ENTRY                             "VTMEntry"            ///< One entry in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_TERMINAL                          "Terminal"            ///< Virtual terminal index used in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_TRANSTYPE                         "TransType_9C"        ///< Transaction type used in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_CURRENCYCODE                      "CurrencyCode_5F2A"   ///< Currency code used in "EMV_VirtualTermConfig.xml"

/// @}

#endif
