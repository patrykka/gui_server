/****************************************************************************
*  Product:     ADK Cards Service - EMV Contact (CT)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Client (structure) interface - functions and definitions
****************************************************************************/

/**
* @file
* @brief Interface of CT-Client
*
*/

#ifndef _EMV_CT_INTERFACE_H_   /* avoid double interface-includes */
#define _EMV_CT_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "emv/EMV_Common_Interface.h"
#include "EMV_CT_Link.h"
#include "EMV_CT_Version.h"

#define DLL_CTC // no client shared libraries anymore


// ========================================================================================================
// === COMMON PART ===
// ========================================================================================================


/// @ingroup STRDEF_COMMON
/// @brief 9F06, AID (5 bytes RID + n bytes PIX)
typedef struct EMV_CT_APPLI_STRUCT
{
  unsigned char    aidlen;  ///< length of AID @n length of value from #TAG_DF04_AID
  unsigned char    AID[16]; ///< AID data @n TLV tag #TAG_DF04_AID
} EMV_CT_APPLI_TYPE;

/// @ingroup ADK_TRX_EXEC
/// @brief Candidate list structure
///
/// used as EMV_CT_SELECTRES_STRUCT::T_BF04_Candidates
typedef struct EMV_CT_CANDIDATE_STRUCT
{
  EMV_CT_APPLI_TYPE    candidate;  ///< Candidate AID (::EMV_CT_APPLI_STRUCT)
  unsigned char  name[17];         ///< Candidate name, could be 9F12 or 50 or 84(converted to ascii)
} EMV_CT_CANDIDATE_TYPE;

#define EMV_CT_SUPPORTED_SIZE_9F0A 50 ///< limit for Application Section Registered Proprietary Data size (has been 20 before)

/// @ingroup ADK_TRX_EXEC
/// @brief Candidate list additional data
/// Output of EMV_CT_GetCandidateData()
/// @n To be used in conjunction with EMV_CT_SELECTRES_STRUCT::T_BF04_Candidates
typedef struct EMV_CT_CANDIDATE_DATA_STRUCT
{
  unsigned char API;             ///< Tag '87' application priority indicator @n TLV tag #TAG_87_APP_PRIORITY_ID
  unsigned char CodeTableIndex;  ///< Tag '9F11' for the case that application name originates from application preferred name '9F12' @n TLV tag #TAG_9F11_ISS_CODE_TABLE_ID
  unsigned char IIN[4];          ///< Tag '42' issuer identification number (n6 or n8, right padded with 0xff), starts with 0xff if absent @n TLV tag #TAG_42_ISSUER_ID
  unsigned char IBAN[35];        ///< Tag '5F53' International bank account (an ..34, terminated by '0') @n TLV tag #TAG_5F53_IBAN
  unsigned char BIC[12];         ///< Tag '5F54' Bank identification code (an 8 or 11, terminated by '0') @n TLV tag #TAG_5F54_BIC
  unsigned char country2[2];     ///< Tag '5F55' Issuer Country Code (a2) @n TLV tag #TAG_5F55_ISSUER_COUNTRY_2
  unsigned char country3[3];     ///< Tag '5F56' Issuer Country Code (a3) @n TLV tag #TAG_5F56_ISSUER_COUNTRY_3
  unsigned char len9F06;         ///< For determining the terminal candidate ('9F06') in case of "AID chain" and partial select @n TLV tag #TAG_9F06_AID
  unsigned char DF62_ASF[33];    ///< Application Selection Flag ('DF62', Interac, Canada), first byte indicating length @n TLV tag #TAG_DF62_APPLI_SELECT_FLAG
  unsigned char selData[1+EMV_CT_SUPPORTED_SIZE_9F0A];   ///< Application Selection Registered Proprietary Data (9F0A), Bulletin No. 175, first byte length @n TLV tag #TAG_9F0A_ASRPD
} EMV_CT_CANDIDATE_DATA_TYPE;

/// @ingroup STRDEF_COMMON
#define  EMV_CT_COMMON_CANDS    10  ///< Maximum number of mutual candidates in application selection process, see EMV_CT_APPS_SELECT_STRUCT::ModifiedCandidates and EMV_CT_SELECTRES_STRUCT::T_BF04_Candidates

/// @ingroup STRDEF_COMMON
/// @brief data object list
typedef struct EMV_CT_DOL_STRUCT
{
  unsigned char    dollen;           ///< length of DOL data
  unsigned char    DOL[EMV_ADK_MAX_LG_DDOL]; ///< DOL data
} EMV_CT_DOL_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief track 2 data
typedef struct EMV_CT_TRACK2_STRUCT
{
  unsigned char    tr2len;      ///< length of track 2 data
  unsigned char    tr2data[19]; ///< track 2 data
} EMV_CT_TRACK2_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief cardholder name
typedef struct EMV_CT_CRDNAME_STRUCT
{
  unsigned char    crdNameLen;    ///< length of cardholder name
  unsigned char    crdName[26];   ///< cardholder name
} EMV_CT_CRDNAME_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief icc dynamic number
typedef struct EMV_CT_ICCRND_STRUCT
{
  unsigned char    iccRNDLen;    ///< length ICC dynamic number
  unsigned char    iccRND[8];    ///< ICC dynamic number
} EMV_CT_ICCRND_TYPE;

/// @ingroup STRDEF_COMMON
/// @brief issuer application data
typedef struct EMV_CT_ISSDATA_STRUCT
{
  unsigned char    issDataLen;    ///< length of issuer application data
  unsigned char    issData[32];   ///< issuer application data
} EMV_CT_ISSDATA_TYPE;


/// @defgroup ADK_PIN_PARAM Definitions for PIN Input callback function
/// @ingroup TLV_CALLBCK
/// @brief See @ref EMV_CT_CALLBACK_FnT (#TAG_BF08_CBK_PIN)

/// @defgroup ADK_PIN_INPUT Input params for PIN entry
/// @ingroup ADK_PIN_PARAM
/// @{
#define  EMV_CT_PIN_INPUT_ONLINE          0     ///< Online enciphered PIN
#define  EMV_CT_PIN_INPUT_PLAIN           1     ///< Offline plaintext PIN
#define  EMV_CT_PIN_INPUT_ENCIPHERED      2     ///< Offline enciphered PIN
#define  EMV_CT_PIN_INPUT_PLAIN_EXT       3     ///< Offline plaintext PIN at external device (old Verix devices only)
#define  EMV_CT_PIN_INPUT_ENCIPHERED_EXT  4     ///< Offline enciphered PIN at external device (old Verix devices only)
#define  EMV_CT_CVM_CUSTOM                5     ///< Custom CVM, the highest bit indicates the custom CVM, byte 0...3 indicates the custom CVM method
/// @}


/// @defgroup ADK_PIN_RETURN Return values
/// @ingroup ADK_PIN_PARAM
/// @brief Return values used by calling application for PIN input callback function
/// @{
#define  EMV_CT_PIN_INPUT_OKAY       0  ///< PIN input was successful
#define  EMV_CT_PIN_INPUT_COMM_ERR   1  ///< Communication error
#define  EMV_CT_PIN_INPUT_ABORT      2  ///< Customer canceled
#define  EMV_CT_PIN_INPUT_BYPASS     3  ///< Customer chose "bypass PIN entry"
#define  EMV_CT_PIN_INPUT_TIMEOUT    4  ///< Timeout
#define  EMV_CT_PIN_INPUT_OTHER_ERR  5  ///< Other error
#define  EMV_CT_PIN_UNCRIT_TIMEOUT   6  ///< uncritical timeout (no immediate abort on timeout)
/// @}


/// @defgroup EMV_MODES Supported
/// @ingroup DEF_CONF_APPLI
/// @brief Defines for supported EMV Modes
/// @{
#define  EMV_CT_EMV_0                        0x00  ///< EMV Application
#define  EMV_CT_EMV_1                        0x01  ///< EMV Application
#define  EMV_CT_NON_EMV                      0xFF  ///< NON EMV Application
/// @}


// ========================================================================================================
// === Transaction Information ===
// ========================================================================================================

/// @defgroup DEF_ADK_DEBUG Additional error information
/// @ingroup ADK_GENERAL
/// @brief Filled in case of error, value to describe which error occured.
///

/// @defgroup DEF_ADK_DEBUG_EXIT Additional error info exit code
/// @ingroup DEF_ADK_DEBUG
/// @brief Last step performed by EMV ADK
///
/// @{
  #define EMV_ADK_INF_INIT                    0x80  ///< Initialization
  #define EMV_ADK_INF_SEL_TERMDATA            0x81  ///< Application selection:          terminal data
  #define EMV_ADK_INF_SEL_TEXTE               0x82  ///< Application selection:          texts
  #define EMV_ADK_INF_SEL_PAYINIT_OK          0x83  ///< Application selection:          Init Payment OK
  #define EMV_ADK_INF_SEL_CHECK_OK            0x85  ///< Application selection:          Check OK
  #define EMV_ADK_INF_SEL_BUILD_APPLI_LIST    0x86  ///< Application selection:          Build Application List
  #define EMV_ADK_INF_SEL_ANOTHER             0x87  ///< Application selection:          Other
  #define EMV_ADK_INF_SEL_FINAL               0x88  ///< Application selection:          Final
  #define EMV_ADK_INF_SEL_NO                  0x89  ///< Application selection:          NO
  #define EMV_ADK_INF_SEL_FINISH_OK           0x8A  ///< Application selection:          Finished OK
  #define EMV_ADK_INF_SEL_BLOCK_APPLI         0x8B  ///< Application selection:          Blocked Application
  #define EMV_ADK_INF_SEL_BLOCK_CARD          0x8C  ///< Application selection:          Blocked Card
  #define EMV_ADK_INF_SEL_LOA_PROC            0x8D  ///< Application selection:          LOA Processing
  #define EMV_ADK_INF_SEL_UI_LOA_PROC         0x8E  ///< Application selection:          LOA Processing (uninterrupted)
  #define EMV_ADK_INF_TRANS_CHECK_OK          0x91  ///< Offline transaction processing: Check OK
  #define EMV_ADK_INF_TRANS_READ_TERM         0x92  ///< offline transaction processing: Read Terminal Data
  #define EMV_ADK_INF_TRANS_READ_APPLI        0x93  ///< offline transaction processing: Read Application
  #define EMV_ADK_INF_TRANS_SEL_LANG          0x95  ///< offline transaction processing: Select Language
  #define EMV_ADK_INF_TRANS_CONF_AMOUNT       0x96  ///< offline transaction processing: Confirm Amount
  #define EMV_ADK_INF_TRANS_INITIATE          0x97  ///< offline transaction processing: Initiation
  #define EMV_ADK_INF_TRANS_READ_CARD_DATA    0x98  ///< offline transaction processing: Read Card Data
  #define EMV_ADK_INF_TRANS_CHECK_APPLI_DATA  0x99  ///< offline transaction processing: Check Application Data
  #define EMV_ADK_INF_TRANS_PROC_RESTRICTION  0x9A  ///< offline transaction processing: Process Restriction
  #define EMV_ADK_INF_TRANS_CARD_AUTH         0x9B  ///< offline transaction processing: Card Authentication
  #define EMV_ADK_INF_TRANS_VERIFY            0x9C  ///< offline transaction processing: Verification
  #define EMV_ADK_INF_TRANS_RISK              0x9D  ///< offline transaction processing: Risk
  #define EMV_ADK_INF_TRANS_AAT               0x9E  ///< offline transaction processing: AAT (action analysis terminal)
  #define EMV_ADK_INF_TRANS_1GENAC            0x9F  ///< offline transaction processing: 1st Genac
  #define EMV_ADK_INF_TRANS_VERIFY_CARD       0xA0  ///< offline transaction processing: Verify Cardholder
  #define EMV_ADK_INF_TRANS_VERIFY_ABANDON    0xA1  ///< offline transaction processing: Verify Abandon
  #define EMV_ADK_INF_TRANS_VERIFY_OTHER      0xA2  ///< offline transaction processing: Verify Other
  #define EMV_ADK_INF_TRANS_VERIFY_ONLINE     0xA3  ///< offline transaction processing: Verify Online
  #define EMV_ADK_INF_TRANS_VERIFY_NOK        0xA4  ///< offline transaction processing: Verify NOT OK
  #define EMV_ADK_INF_TRANS_VERIFY_OK         0xA5  ///< offline transaction processing: Verify OK
  #define EMV_ADK_INF_TRANS_VERIFY_SIGNATURE  0xA6  ///< offline transaction processing: Verify Signature
  #define EMV_ADK_INF_TRANS_1CRYPT_TC         0xA7  ///< offline transaction processing: 1st Cryptography TC
  #define EMV_ADK_INF_TRANS_1CRYPT_ARQC_CDA   0xA8  ///< offline transaction processing: 1st Cryptography ARQC CDA
  #define EMV_ADK_INF_TRANS_1CRYPT_ARQC_OTHER 0xA9  ///< offline transaction processing: 1st Cryptography ARQC Other
  #define EMV_ADK_INF_TRANS_1CRYPT_AAC        0xAA  ///< offline transaction processing: 1st Cryptography AAC
  #define EMV_ADK_INF_TRANS_1CRYPT_AAR        0xAB  ///< offline transaction processing: 1st Cryptography AAC
  #define EMV_ADK_INF_TRANS_1CRYPT_UNKNOWN    0xAC  ///< offline transaction processing: 1st Cryptography Unknown
  #define EMV_ADK_INF_ONL_CHECK_OK            0xB1  ///< Processing the host response:   Check OK
  #define EMV_ADK_INF_ONL_FALSE               0xB3  ///< Processing the host response:   False
  #define EMV_ADK_INF_ONL_TRUE                0xB4  ///< Processing the host response:   True
  #define EMV_ADK_INF_ONL_PIN                 0xB5  ///< Processing the host response:   PIN
  #define EMV_ADK_INF_ONL_HOST_AUTH           0xB6  ///< Processing the host response:   Host Authorization
  #define EMV_ADK_INF_ONL_CRIT_SC             0xB7  ///< Processing the host response:   Cirtical Script
  #define EMV_ADK_INF_ONL_AC                  0xB8  ///< Processing the host response:   AC
  #define EMV_ADK_INF_ONL_2GENAC              0xB9  ///< Processing the host response:   2nd Generate AC
  #define EMV_ADK_INF_ONL_UNCRIT_SC           0xBA  ///< Processing the host response:   Uncritical Script
  #define EMV_ADK_INF_TRANS_VERIFY_PARAM      0xBB  ///< Processing the host response:   Verify Parameter
  #define EMV_ADK_INF_FETCH_TXN_TAG_DATA      0xBC  ///< Function @c EMV_CT_fetchTxnTags
/// @}


/// @defgroup DEF_ADK_DEBUG_STATUS Additional error info, status
/// @ingroup DEF_ADK_DEBUG
/// @brief Internal error index
/// @{
  #define EMV_ADK_STEP_01                      1  ///< sub step 1
  #define EMV_ADK_STEP_02                      2  ///< sub step 2
  #define EMV_ADK_STEP_03                      3  ///< sub step 3
  #define EMV_ADK_STEP_04                      4  ///< sub step 4
  #define EMV_ADK_STEP_05                      5  ///< sub step 5
  #define EMV_ADK_STEP_06                      6  ///< sub step 6
  #define EMV_ADK_STEP_07                      7  ///< sub step 7
  #define EMV_ADK_STEP_08                      8  ///< sub step 8
  #define EMV_ADK_STEP_09                      9  ///< sub step 9
  #define EMV_ADK_STEP_10                     10  ///< sub step 10
  #define EMV_ADK_STEP_11                     11  ///< sub step 11
  #define EMV_ADK_STEP_12                     12  ///< sub step 12
  #define EMV_ADK_STEP_13                     13  ///< sub step 13
  #define EMV_ADK_STEP_14                     14  ///< sub step 14
  #define EMV_ADK_STEP_15                     15  ///< sub step 15
  #define EMV_ADK_STEP_16                     16  ///< sub step 16
  #define EMV_ADK_STEP_17                     17  ///< sub step 17
  #define EMV_ADK_STEP_18                     18  ///< sub step 18
  #define EMV_ADK_STEP_19                     19  ///< sub step 19
  #define EMV_ADK_STEP_20                     20  ///< sub step 20
  #define EMV_ADK_STEP_21                     21  ///< sub step 21
  #define EMV_ADK_STEP_22                     22  ///< sub step 22
  #define EMV_ADK_STEP_23                     23  ///< sub step 23
  #define EMV_ADK_STEP_24                     24  ///< sub step 24
  #define EMV_ADK_STEP_25                     25  ///< sub step 25
  #define EMV_ADK_STEP_26                     26  ///< sub step 26
  #define EMV_ADK_STEP_27                     27  ///< sub step 27
  #define EMV_ADK_STEP_28                     28  ///< sub step 28
  #define EMV_ADK_STEP_29                     29  ///< sub step 29
  #define EMV_ADK_STEP_30                     30  ///< sub step 30
/// @}


/// @defgroup DEF_DF62_POS1 Position 1: chipcard command where error occurs (2 byte)
/// @ingroup DEF_DF62
/// @{
  #define EMV_ADK_DF62_ERR_POS1_CHIP_RESET              0x1000u    ///< reset (ATR) during technology selection
  #define EMV_ADK_DF62_ERR_POS1_SEL_LIST_AID            0x2000u    ///< SELECT during build of candidate list using explicit mode
  #define EMV_ADK_DF62_ERR_POS1_SEL_LIST_PSE            0x2100u    ///< SELECT during build of candidate list using PSE
  #define EMV_ADK_DF62_ERR_POS1_READ_LIST_PSE           0x2200u    ///< READ RECORD during build of candidate list using PSE
  #define EMV_ADK_DF62_ERR_POS1_SEL_FINAL               0x3000u    ///< SELECT during final selection
  #define EMV_ADK_DF62_ERR_POS1_GPO                     0x4000u    ///< GET PROCESSING OPTIONS
  #define EMV_ADK_DF62_ERR_POS1_READ                    0x5000u    ///< READ RECORD
  #define EMV_ADK_DF62_ERR_POS1_INT_AUTH                0x6000u    ///< INTERNAL AUTHENTICATE
  #define EMV_ADK_DF62_ERR_POS1_GETDATA_PTC             0x7001u    ///< GET DATA for PIN try counter
  #define EMV_ADK_DF62_ERR_POS1_GETCHALLENGE            0x7100u    ///< GET CHALLENGE
  #define EMV_ADK_DF62_ERR_POS1_VERIFY_ENC_PIN          0x7210u    ///< VERIFY for enciphered PIN
  #define EMV_ADK_DF62_ERR_POS1_VERIFY_PLAIN_PIN        0x7220u    ///< VERIFY for plaintext PIN
  #define EMV_ADK_DF62_ERR_POS1_GETDATA_ATC             0x8001u    ///< GET DATA for ATC
  #define EMV_ADK_DF62_ERR_POS1_GETDATA_LOATC           0x8002u    ///< GET DATA for LOATC
  #define EMV_ADK_DF62_ERR_POS1_GENAC_1                 0x9001u    ///< 1st GENERATE AC without CDA
  #define EMV_ADK_DF62_ERR_POS1_GENAC_1_CDA             0x9011u    ///< 1st GENERATE AC with CDA
  #define EMV_ADK_DF62_ERR_POS1_EXTAUTH                 0xA000u    ///< EXTERNAL AUTHENTICATE
  #define EMV_ADK_DF62_ERR_POS1_CRIT_SCRIPT             0xB100u    ///< critical script command
  #define EMV_ADK_DF62_ERR_POS1_GENAC_2                 0x9002u    ///< 2nd GENERATE AC without CDA
  #define EMV_ADK_DF62_ERR_POS1_GENAC_2_CDA             0x9012u    ///< 2nd GENERATE AC with CDA
  #define EMV_ADK_DF62_ERR_POS1_NON_CRIT_SCRIPT         0xB200u    ///< non critical script command
/// @}


/// @defgroup DEF_DF62_POS2 Position 2: status of chipcard communication (2 byte)
/// @ingroup DEF_DF62
/// @{
  #define EMV_ADK_DF62_ERR_POS2_TRANSPORT_LAYER         0x0000u    ///< error in transport layer (T=0, T=1)
  #define EMV_ADK_DF62_ERR_POS2_BASE_ERROR              0x0001u    ///< base error (wrong length of R-APDU)
  #define EMV_ADK_DF62_ERR_POS2_RAPDU                   0xFFFFu    ///< R-APDU data with wrong content
//      EMV_ADK_DF62_ERR_POS2_XXXX                      XXXX     // SW1 SW2 of R-APDU
/// @}


/// @defgroup DEF_DF62_POS3 Position 3: activator (1 byte)
/// @ingroup DEF_DF62
/// @{
  #define EMV_ADK_DF62_ERR_POS3_CHIPCARD                0x01      ///< chipcard :-)
  #define EMV_ADK_DF62_ERR_POS3_TERMINAL                0x02      ///< terminal
  #define EMV_ADK_DF62_ERR_POS3_ACQUIRER                0x03      ///< Authorization system
  #define EMV_ADK_DF62_ERR_POS3_CARDHOLDER              0x04      ///< cardholder
/// @}

///       @defgroup DEF_DF62 Additional error data
///       @ingroup DEF_FLOW_OUTPUT
///       @brief DF62, 15 bytes, error reference
///
///       Following positions do not need special defines here:
///       @n position 4: ARC (Tag 8A, 2 byte)
///       @n position 5: TVR (5 byte)
///       @n position 6: TSI (2 byte)

/// @defgroup DEF_DF62_POS7 Position 7: error class (1 byte)
/// @ingroup DEF_DF62
/// @{
  #define EMV_ADK_DF62_ERR_POS7_ABORT                   0x01      ///< abort of transaction or end of transaction without TC
  #define EMV_ADK_DF62_ERR_POS7_MAG_FALLBACK            0x02      ///< magstripe fallback transaction
/// @}



// ========================================================================================================
// === TERMINAL CONFIGURATION ===
// ========================================================================================================

/// @ingroup DEF_CONF_TERM
/// @brief struct for interface to EMV_CT_SetTermData() and EMV_CT_GetTermData()
typedef struct EMV_CT_TERMDATA_STRUCT /* === EMV_CT_TERMDATA_TYPE === */
{
  unsigned char   TermTyp;                     ///< Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CT_SetTermData() ,validity bit #EMV_CT_INPUT_TRM_TYPE @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F35_TRM_TYPE, @n TermTyp in EMV_CT_TERMDATA_STRUCT::TermTyp, @n XML Tag: #XML_TAG_TERMDATA_TERM_TYP
  unsigned char   TermCountryCode[2];          ///< Terminal country code according ISO 3166, validity bit #EMV_CT_INPUT_TRM_COUNTRY_CODE @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n TermCountryCode in EMV_CT_TERMDATA_STRUCT::TermCountryCode, @n XML Tag: #XML_TAG_TERMDATA_COUNTRY_CODE_TERM
  unsigned char   TermCap[3];                  ///< Terminal capabilities @n single features may be deactivated per application by EMV_CT_APPLIDATA_STRUCT::App_TermCap @n mandatory for first call of EMV_CT_SetTermData(), validity bit #EMV_CT_INPUT_TRM_CAPABILITIES @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F33_TRM_CAPABILITIES, @n TermCap in EMV_CT_TERMDATA_STRUCT::TermCap, @n XML Tag: #XML_TAG_TERMDATA_TERM_CAP
  unsigned char   TermAddCap[5];               ///< Additional terminal capabilities @n mandatory for first call of EMV_CT_SetTermData(), validity bit #EMV_CT_INPUT_TRM_ADD_CAPS @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F40_ADD_TRM_CAP, @n TermAddCap in EMV_CT_TERMDATA_STRUCT::TermAddCap, @n XML Tag: #XML_TAG_TERMDATA_TERM_ADD_CAP
  unsigned char   TermIdent[8];                ///< Terminal Identification @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_TRM_ID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_TERMDATA_STRUCT::TermIdent, @c XML Tag: #XML_TAG_TERMDATA_TERM_IDENT
  unsigned char   CurrencyTrans[2];            ///< Transaction currency code according ISO 4217 @n For a single transaction this value may be overwritten by @c CurrencyTrans of ::EMV_CT_SELECT_TYPE, validity bit #EMV_CT_INPUT_TRM_CURRENCY @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_5F2A_TRANS_CURRENCY, @n CurrencyTrans in EMV_CT_TERMDATA_STRUCT::CurrencyTrans, @n XML Tag: #XML_TAG_TERMDATA_CURRENCY_TRANS
  unsigned char   ExpTrans;                    ///< Transaction currency exponent @n For a single transaction this value may be overwritten by @c Exp_Trans of ::EMV_CT_SELECT_TYPE, validity bit #EMV_CT_INPUT_TRM_EXP_CURRENCY @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP, @n ExpTrans in EMV_CT_TERMDATA_STRUCT::ExpTrans, @n XML Tag: #XML_TAG_TERMDATA_EXP_TRANS
  unsigned char   SuppLang[EMV_ADK_MAX_LANG];  ///< Supported languages by the terminal application, validity bit #EMV_CT_INPUT_TRM_LANGUAGES @n@c Tag, @c Struct, @c XML Reference: @n TLV tag: #TAG_SUPP_LANG, @n SuppLang in EMV_CT_TERMDATA_STRUCT::SuppLang, @n XML Tag: #XML_TAG_TERMDATA_SUPP_LANG
  unsigned char   IFDSerialNumber[8];          ///< Interface Device (IFD) Serial Number @n In case there are no special project requirements this parameter can be filled as follows: @n <em> Verix: </em> use function @c SVC_INFO_SERLNO() and take the last 8 digits from the result @n <em> V/OS: </em> function @c platforminfo_get() with <tt> InfoType==PI_SERIAL_NUM </tt> @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_TRM_IFD_SERIAL @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1E_IFD_SERIAL_NB, @n IFDSerialNumber in EMV_CT_TERMDATA_STRUCT::IFDSerialNumber, @n XML Tag: #XML_TAG_TERMDATA_IFD_SERIAL_NUMBER

  char   KernelVersion[EMV_ADK_VERSION_ASCII_SIZE];       ///< version of EMVCo Kernel, validity bit #EMV_CT_INPUT_TRM_KERNEL_VERSION @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KERNEL_VERSION, @n KernelVersion in EMV_CT_TERMDATA_STRUCT::KernelVersion, @n XML Tag: #XML_TAG_TERMDATA_KERNEL_VERSION
  char   FrameworkVersion[EMV_ADK_VERSION_ASCII_SIZE];    ///< version of the Framework, see #EMV_CT_FRAMEWORK_VERSION, validity bit #EMV_CT_INPUT_TRM_FRAMEWORK_VERSION @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF11_LIB_VERSION, @n FrameworkVersion in EMV_CT_TERMDATA_STRUCT::FrameworkVersion, @n XML Tag: #XML_TAG_TERMDATA_FRAMEWORK_VERSION
  char   L1DriverVersion[EMV_ADK_VERSION_ASCII_SIZE];     ///< version of L1 Driver, validity bit #EMV_CT_INPUT_TRM_L1DRIVER_VERSION @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_L1DRIVER_VERSION, @n L1DriverVersion in EMV_CT_TERMDATA_STRUCT::L1DriverVersion, @n XML Tag: #XML_TAG_TERMDATA_L1DRIVER_VERSION

  unsigned char   Info_Included_Data[8];     ///< Which data is included in the message, see @ref DEF_INPUT_TERM
} EMV_CT_TERMDATA_TYPE; ///< typedef for EMV_CT_TERMDATA_STRUCT


/// @defgroup DEF_INPUT_TERM Terminal data: Which data is included
/// @ingroup DEF_CONF_TERM
/// @brief Contents of the EMV_CT_TERMDATA_STRUCT::Info_Included_Data
/// @{
/* Byte 1 */
#define  EMV_CT_INPUT_TRM_TYPE               0x01  ///< B1b1: EMV_CT_TERMDATA_STRUCT::TermTyp
#define  EMV_CT_INPUT_TRM_CURRENCY           0x02  ///< B1b2: EMV_CT_TERMDATA_STRUCT::CurrencyTrans
#define  EMV_CT_INPUT_TRM_EXP_CURRENCY       0x04  ///< B1b3: EMV_CT_TERMDATA_STRUCT::ExpTrans
#define  EMV_CT_INPUT_TRM_COUNTRY_CODE       0x08  ///< B1b4: EMV_CT_TERMDATA_STRUCT::TermCountryCode
#define  EMV_CT_INPUT_TRM_CAPABILITIES       0x10  ///< B1b5: EMV_CT_TERMDATA_STRUCT::TermCap
#define  EMV_CT_INPUT_TRM_ADD_CAPS           0x20  ///< B1b6: EMV_CT_TERMDATA_STRUCT::TermAddCap
#define  EMV_CT_INPUT_TRM_ID                 0x40  ///< B1b7: EMV_CT_TERMDATA_STRUCT::TermIdent
/* Byte 2 */
#define  EMV_CT_INPUT_TRM_LANGUAGES          0x01  ///< B2b1: EMV_CT_TERMDATA_STRUCT::SuppLang
#define  EMV_CT_INPUT_TRM_IFD_SERIAL         0x02  ///< B2b2: EMV_CT_TERMDATA_STRUCT::IFDSerialNumber
#define  EMV_CT_INPUT_TRM_KERNEL_VERSION     0x04  ///< B2b3: EMV_CT_TERMDATA_STRUCT::KernelVersion
#define  EMV_CT_INPUT_TRM_FRAMEWORK_VERSION  0x08  ///< B2b4: EMV_CT_TERMDATA_STRUCT::FrameworkVersion
#define  EMV_CT_INPUT_TRM_L1DRIVER_VERSION   0x10  ///< B2b5: EMV_CT_TERMDATA_STRUCT::L1DriverVersion
/// @}


// ========================================================================================================
// === APPLICATION CONFIGURATION ===
// ========================================================================================================


/// @defgroup TAC_IAC_DENIAL TAC/IAC DENIAL processing in case of Offline Only terminals
/// @ingroup DEF_CONF_APPLI
/// @brief DF15, ICS feature, for @c ucAC_before_after in ::EMV_CT_TERMDATA_STRUCT
/// @{
#define  EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE   0x00 ///< process default action codes prior to 1st GENERATE AC
#define  EMV_CT_PROCESS_TACIAC_DEFAULT_AFTER    0x01 ///< process default action codes after 1st GENERATE AC
/// @}


/// @defgroup CHKSUM_DEFS Defines for EMVCo checksum calculation
/// @ingroup DEF_CONF_APPLI
/// @brief other major terminal parameters
/// actually there are only some of them defined @n
/// most of them are hard coded in the lib as actually needed @n
/// all of them are Yes / No decisions @n
/// --> whenever there is a need to make this configurable for the checksum there will be another parameter added @n
/// In brackets there is the default value or the currently hard coded value @n
/// <B> only for checksum --> no control on the flow,</B> @n
/// e.g. Blacklist is controlled per application not per terminal parameters @n
/// <B>!!! take respect on the default !!! </B>
/// @{
#define  EMV_CT_CHECKSUM_DEFAULT                      "\x00\x00\x00\x00\x00" ///< 0x00 means: default @n --> when initialized with 0x00 0x00 0x00 then all params set as default mentioned in comment. @n For changing the default the corresponding Bit must be set: @n e.g. for Transaction Log disabling, the corresponding Bit must be activated = 1

/* Byte 1 */
#define  EMV_CT_CHECKSUM_INCLUDE_VERSION              0x01 ///< B1b1, default: NO  --> For checksum only
#define  EMV_CT_CHECKSUM_TRANSACTION_LOG              0x02 ///< B1b2, default: YES --> De/activated per application
#define  EMV_CT_CHECKSUM_EXCEPTION_FILE               0x04 ///< B1b3, default: YES --> De/activated per application
#define  EMV_CT_CHECKSUM_FORCE_ONLINE                 0x08 ///< B1b4, default: YES --> De/activated per application and by merchant
#define  EMV_CT_CHECKSUM_FORCE_ACCEPTANCE             0x10 ///< B1b5, default: YES --> De/activated per application and by merchant
#define  EMV_CT_CHECKSUM_SUPPORT_ONL_DATA_CAPTURE     0x20 ///< B1b6, default: NO  --> For checksum only --> application related decision
#define  EMV_CT_CHECKSUM_SUPPORT_PSE                  0x40 ///< B1b7, default: YES --> De/activated per transaction (EMV_Select)
#define  EMV_CT_CHECKSUM_ACCOUNT_TYPE                 0x80 ///< B1b8, default: YES --> De/activated per application

/* Byte 2 */
#define  EMV_CT_CHECKSUM_SUPPORT_ADVICES              0x01 ///< B2b1, default: NO  --> Support of advices on host prot
#define  EMV_CT_CHECKSUM_SUPPORT_CARDHOLDER_CONF      0x02 ///< B2b2, default: YES --> Cardholder Confirmation
#define  EMV_CT_CHECKSUM_PIN_BYPASS                   0x04 ///< B2b3, default: NO  --> PIN bypass
#define  EMV_CT_CHECKSUM_SUPPORT_DEFAULT_TDOL         0x08 ///< B2b4, default: YES --> Default TDOL
#define  EMV_CT_CHECKSUM_SUPPORT_BATCH_DATA_CAPTURE   0x10 ///< B2b5, default: YES --> Batch data capture
#define  EMV_CT_CHECKSUM_SUPPORT_VOICE_REF_ISS        0x20 ///< B2b6, default: YES --> Issuer initiated referrals
#define  EMV_CT_CHECKSUM_SUPPORT_VOICE_REF_CARD       0x40 ///< B2b7, default: NO  --> Card initiated referrals
#define  EMV_CT_CHECKSUM_MULTILANG_SUPPORT            0x80 ///< B2b7, default: YES since L2 7.0.2

// Byte 3
#define  EMV_CT_CHECKSUM_PIN_BYPASS_ONCE              0x01 ///< B3b1, default: NO  --> No subsequent PIN bypass when PIN bypass is active
/// @}

/// @defgroup TAGS_NON_EMVCO Handling of tags not defined by EMVCo
/// @ingroup SPECIAL_TAG_HANDLING
///
/// @{
#define EMV_CT_MAX_NO_OF_NON_EMVCO_TAGS      20  ///< Max. number of non-EMVCo-tags included in additional tags (EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM and EMV_CT_APPLIDATA_STRUCT::Additional_Tags_CRD)
/// @}

#define EMV_CT_MAX_TAGLIST_SIZE              20  ///< Maximum number of tags used in ::EMV_CT_TAGLIST_STRUCT
#define EMV_CT_MAX_APP_VERS                  10

/// @brief structure for EMV_CT_APPLIDATA_STRUCT::Mandatory_Tags_CRD and EMV_CT_PAYMENT_STRUCT::Additional_Result_Tags
typedef struct EMV_CT_TAGLIST_STRUCT
{
  unsigned char   anztag;                        ///< Number of tags included in @c tags
  unsigned short  tags[EMV_CT_MAX_TAGLIST_SIZE]; ///< Buffer for tags (no length, no value) @n max. number of tags: #EMV_CT_MAX_TAGLIST_SIZE
} EMV_CT_TAGLIST_TYPE;

/// @ingroup DEF_CONF_APPLI
/// @brief Structure for configuration of one single application
///   @n see EMV_CT_SetAppliData()
typedef struct EMV_CT_APPLIDATA_STRUCT // EMV_CT_APPLIDATA_TYPE
{
  // *** EMVCo mandatory data ***
  unsigned char     VerNum[2];                  ///< Application version number @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_VERSION  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB, @n VerNum in EMV_CT_APPLIDATA_STRUCT::VerNum, @n XML Tag: #XML_TAG_APPLIDATA_VER_NUM
  unsigned char     AppName[16+1];              ///< Default application name to be used in case application label (tag 50) and application preferred name (tag 9F12) are not read from chip, validity bit #EMV_CT_INPUT_APL_NAME  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_50_APP_LABEL, @n AppName in EMV_CT_APPLIDATA_STRUCT::AppName, @n XML Tag: #XML_TAG_APPLIDATA_APP_NAME
  unsigned char     ASI;                        ///< Application selection indicator @n Must the card's AID match the configured AID exactly? @n @c 0 ... yes @n @c 1 ... no @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_ASI  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF20_ASI, @n ASI in EMV_CT_APPLIDATA_STRUCT::ASI, @n XML Tag: #XML_TAG_APPLIDATA_ASI
  unsigned char     BrKey[2];                   ///< Merchant category code @n recommended mandatory for EMV_CT_SetAppliData() @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_APL_MERCHANT_CATCODE  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F15_MERCH_CATEG_CODE, @n BrKey in EMV_CT_APPLIDATA_STRUCT::BrKey, @n XML Tag: #XML_TAG_APPLIDATA_BR_KEY
  unsigned char     TermIdent[8];               ///< Overwrite global Terminal Identification, validity bit #EMV_CT_INPUT_APL_TID  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_APPLIDATA_STRUCT::TermIdent, @n XML Tag: #XML_TAG_APPLIDATA_TERM_IDENT
  unsigned char     FloorLimit[4];              ///< Terminal floor limit (binary coded) @n recommended mandatory for EMV_CT_SetAppliData() @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_APL_FLOOR_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT, @n FloorLimit in EMV_CT_APPLIDATA_STRUCT::FloorLimit, @n XML Tag: #XML_TAG_APPLIDATA_FLOOR_LIMIT
  unsigned char     Threshhold[4];              ///< Threshold Value for Biased Random Online Selection during risk management (binary coded) @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_THRESH  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF24_THRESHHOLD, @n Threshhold in EMV_CT_APPLIDATA_STRUCT::Threshhold, @n XML Tag: #XML_TAG_APPLIDATA_THRESHOLD
  unsigned char     TargetPercentage;           ///< Target percentage (BCD coded) for random online selection during risk management @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_TARGET  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF26_PERCENT_ONL, @n TargetPercentage in EMV_CT_APPLIDATA_STRUCT::TargetPercentage, @n XML Tag: #XML_TAG_APPLIDATA_TARGET_PERCENTAGE
  unsigned char     MaxTargetPercentage;        ///< Maximum target percentage (BCD coded) for random online selection during risk management @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_MAXTARGET  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF25_MAXPERCENT_ONL, @n MaxTargetPercentage in EMV_CT_APPLIDATA_STRUCT::MaxTargetPercentage, @n XML Tag: #XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE
  unsigned char     TACDenial[5];               ///< Terminal Action Code - Denial @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_TAC_DENIAL  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF21_TAC_DENIAL, @n TACDenial in EMV_CT_APPLIDATA_STRUCT::TACDenial, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DENIAL
  unsigned char     TACOnline[5];               ///< Terminal Action Code - Online @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_TAC_ONLINE  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF22_TAC_ONLINE, @n TACOnline in EMV_CT_APPLIDATA_STRUCT::TACOnline, @n XML Tag: #XML_TAG_APPLIDATA_TAC_ONLINE
  unsigned char     TACDefault[5];              ///< Terminal Action Code - Default @n recommended mandatory for #EMV_CT_SetAppliData, validity bit #EMV_CT_INPUT_APL_TAC_DEFAULT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF23_TAC_DEFAULT, @n TACDefault in EMV_CT_APPLIDATA_STRUCT::TACDefault, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DEFAULT
  unsigned char     EMV_Application;            ///< @c xx = EMV-Application (or similar), @c 0xFF = non-EMV-Application, validity bit #EMV_CT_INPUT_APL_EMV_APPKIND  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2D_EMV_APPLI, @n EMV_Application in EMV_CT_APPLIDATA_STRUCT::EMV_Application, @n XML Tag: #XML_TAG_APPLIDATA_EMV_APPLICATION
  EMV_CT_DOL_TYPE   Default_TDOL;               ///< Default TDOL (1st byte: length), maximum length see #EMV_ADK_MAX_LG_TDOL @n see ::EMV_CT_DOL_STRUCT, validity bit #EMV_CT_INPUT_APL_TDOL  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF27_DEFAULT_TDOL, @n Default_TDOL in EMV_CT_APPLIDATA_STRUCT::Default_TDOL, @n XML Tag: #XML_TAG_APPLIDATA_DEFAULT_TDOL
  EMV_CT_DOL_TYPE   Default_DDOL;               ///< Default DDOL (1st byte: length), maximum length see #EMV_ADK_MAX_LG_DDOL @n see ::EMV_CT_DOL_STRUCT, validity bit #EMV_CT_INPUT_APL_DDOL  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF28_DEFAULT_DDOL, @n Default_DDOL in EMV_CT_APPLIDATA_STRUCT::Default_DDOL, @n XML Tag: #XML_TAG_APPLIDATA_DEFAULT_DDOL
  unsigned char     MerchIdent[15];             ///< Merchant Identifier @n recommended mandatory for EMV_CT_SetAppliData() @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_APL_MERCHANT_IDENT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F16_MERCHANT_ID, @n MerchIdent in EMV_CT_APPLIDATA_STRUCT::MerchIdent, @n XML Tag: #XML_TAG_APPLIDATA_MERCH_IDENT
  unsigned char     CDAProcessing;              ///< CDA processing, maximum security or maximum speed, for values see @ref CDA_EMV_MODE_1, @ref CDA_EMV_MODE_2, @ref CDA_EMV_MODE_3, @ref CDA_EMV_MODE_4, validity bit #EMV_CT_INPUT_APL_CDA  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF3E_CDA_PROC, @n CDAProcessing in EMV_CT_APPLIDATA_STRUCT::CDAProcessing, @n XML Tag: #XML_TAG_APPLIDATA_CDA_PROCESSING
  unsigned char     ACBeforeAfter;              ///< TACIAC_Default handled before or after 1st GAC, only for offline only terminals, otherwise not needed @n Possible values see @ref TAC_IAC_DENIAL, validity bit #EMV_CT_INPUT_APL_AC_BEFOREAFTER  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF15_OFFL_ONLY_PROCESS, @n ACBeforeAfter in EMV_CT_APPLIDATA_STRUCT::ACBeforeAfter, @n XML Tag: #XML_TAG_APPLIDATA_AC_BEFORE_AFTER
  unsigned char     AIP_CVM_not_supported;      ///< Behaviour for AIP 'CVM not supported', see @ref CVM_NOT_SUPP, validity bit #EMV_CT_INPUT_APL_AIP_CVM_NOTSUPP  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2E_CVM_NOT_SUPP, @n AIP_CVM_not_supported in EMV_CT_APPLIDATA_STRUCT::AIP_CVM_not_supported, @n XML Tag: #XML_TAG_APPLIDATA_AIP_CVM_NOT_SUPPORTED
  unsigned char     POS_EntryMode;              ///< POS entry mode according ISO 8583:1987 @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_APL_POS_ENTRY  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F39_POS_ENTRY_MODE, @n POS_EntryMode in EMV_CT_APPLIDATA_STRUCT::POS_EntryMode, @n XML Tag: #XML_TAG_APPLIDATA_POS_ENTRY_MODE
  unsigned char     Additional_Versions_No[2*EMV_CT_MAX_APP_VERS]; ///< up to 10 additional version numbers, optional if needed for compliancy (Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
  unsigned char     Security_Limit[4];          ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT

  unsigned char     App_FlowCap[5];             ///< Configure special application characteristics, see @ref APP_FLOW_CAPS, validity bit #EMV_CT_INPUT_APL_FLOW_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2B_APP_FLOW_CAP, @n App_FlowCap in EMV_CT_APPLIDATA_STRUCT::App_FlowCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_FLOW_CAP

  // Options for further development:
  // * define limits for AIDs, e.g. use this AID from 0$ to 100$ and another AID from 50$ to 1000$, ... to enable optimizing interchange fees for the customers

  // *** optional data (range 9F50 - 9F7F) ***
  unsigned char        Additional_Tags_TRM[EMV_ADK_ADD_TAG_SIZE]; ///< Tag + Length + Value: Additional terminal data for special applications @n Example: MasterCard 9F53, validity bit #EMV_CT_INPUT_APL_ADD_TAGS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF29_ADD_TAGS, @n Additional_Tags_TRM in EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM
  EMV_CT_DOL_TYPE      Additional_Tags_CRD;                       ///< Tag + maximum Length: Additional ICC tags for special applications @n e.g. domestic cards with additional tags in ReadRecords, fleetcards with additional customer specific tags and others @n see ::EMV_CT_DOL_STRUCT, validity bit #EMV_CT_INPUT_APL_ADD_CRD_TAGS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2C_ADD_TAGS_CRD, @n Additional_Tags_CRD in EMV_CT_APPLIDATA_STRUCT::Additional_Tags_CRD, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD
  EMV_CT_TAGLIST_TYPE  Mandatory_Tags_CRD;                        ///< Tags only: These tags must be provided by the card, otherwise the transaction is terminated. The default implemetation should be an empty list (standard EMVCo settings will automatically apply). This is for fulfilling requirements of domestic specifications. @n see ::EMV_CT_TAGLIST_STRUCT, validity bit #EMV_CT_INPUT_APL_MAND_TAGS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2A_DUTY_TAGS, @n Mandatory_Tags_CRD in EMV_CT_APPLIDATA_STRUCT::Mandatory_Tags_CRD, @n XML Tag: #XML_TAG_APPLIDATA_TAGLIST

  // Terminal data per AID, enables virtual terminals per AID, duplicated from terminal wide parameters, the terminal wide is used if not available
  unsigned char     App_TermCap[3];             ///< Overwrite global "terminal capabilities" (@c TermCap in ::EMV_CT_TERMDATA_STRUCT) for this application @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_APL_TERM_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F33_TRM_CAPABILITIES, @n App_TermCap in EMV_CT_APPLIDATA_STRUCT::App_TermCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_CAP
  unsigned char     Capabilities_belowLimit[3]; ///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
  unsigned char     App_CountryCodeTerm[2];     ///< Change "Terminal country code" (EMV_CT_TERMDATA_STRUCT::TermCountryCode) (e.g. VISA Germany 0276), validity bit #EMV_CT_INPUT_APL_COUNTRY_CODE  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n App_CountryCodeTerm in EMV_CT_APPLIDATA_STRUCT::App_CountryCodeTerm, @n XML Tag: #XML_TAG_APPLIDATA_COUNTRY_CODE_TERM
  unsigned char     App_TermAddCap[5];          ///< Overwrite global "additional terminal capabilities" (@c TermAddCap in ::EMV_CT_TERMDATA_STRUCT) for this application, only 1st byte is effective, validity bit #EMV_CT_INPUT_APL_ADD_TERM_CAPS @n Can also be changed by EMV_CT_updateTxnTags() @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F40_ADD_TRM_CAP, @n App_TermAddCap in EMV_CT_APPLIDATA_STRUCT::App_TermAddCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_ADD_CAP
  unsigned char     App_TermTyp;                ///< Terminal type, possible values see @ref TERM_TYPES) @n mandatory for first call of EMV_CT_SetTermData() @n Can also be changed by EMV_CT_updateTxnTags() @n validity bit #EMV_CT_INPUT_APL_TRM_TYPE  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F35_TRM_TYPE, @n App_TermTyp in EMV_CT_APPLIDATA_STRUCT::App_TermTyp, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_TYP

  // parameters for domestic / local application selection and processing handling
  EMV_CT_APPLI_TYPE xAIDPrio[EMV_ADK_MAX_PRIO_APP];          ///< Priority applications for this application (::EMV_CT_APPLI_STRUCT) @n max. number see #EMV_ADK_MAX_PRIO_APP, validity bit #EMV_CT_INPUT_APL_PRIO_APPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF1D_PRIO_APPLI, @n xAIDPrio in EMV_CT_APPLIDATA_STRUCT::xAIDPrio, @n XML Tag: #XML_TAG_APPLIDATA_AID_PRIO
  unsigned char     tucFallbackMIDs[EMV_ADK_MAX_CHP_TO_MSR]; ///< MIDs related to this AID @n max. number see #EMV_ADK_MAX_CHP_TO_MSR @n TLV tag #TAG_DF1D_PRIO_APPLI, validity bit #EMV_CT_INPUT_APL_MID  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF1D_PRIO_APPLI, @n tucFallbackMIDs in EMV_CT_APPLIDATA_STRUCT::tucFallbackMIDs, @n XML Tag: #XML_TAG_APPLIDATA_FALLBACK_MIDS
  unsigned char     xuc_Special_TRX[8];         ///< List of special allowed transactions (manual reversal, refund, reservation, tip, Referral), see @ref SPECIAL_TRXS, validity bit #EMV_CT_INPUT_APL_SPECIAL_TXN  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF1C_SPECIAL_TRX, @n xuc_Special_TRX in EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX, @n XML Tag: #XML_TAG_APPLIDATA_SPECIAL_TRX
  unsigned char     uc_FallBack_Handling;       ///< How to handle Fallback after Final select for the selected application, see @ref FB_FINAL_SEL, validity bit #EMV_CT_INPUT_APL_FALLBACK  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF18_FALLABCK, @n uc_FallBack_Handling in EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling, @n XML Tag: #XML_TAG_APPLIDATA_FALLBACK_HANDLING
  unsigned char     Customer_CVM;               ///< non-EMVCo CVM code (byte 1, b6-b1 of CV rule) supported by this AID (ranges 100000-101111 and 110000-111110 are allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM

  unsigned char     Chksum_Params[5];           ///< Terminal parameters defined as major, see @ref CHKSUM_DEFS @n <B> only for checksum calculation </B> @n not all of those parameters are configurable yet but may be in future, validity bit #EMV_CT_INPUT_APL_CHECKSUM_PARAMS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF13_TERM_PARAM, @n Chksum_Params in EMV_CT_APPLIDATA_STRUCT::Chksum_Params, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_PARAMS
  char              ChksumASCIIEMVCO[EMV_ADK_CHECKSUM_ASCII_SIZE];   ///< EMVCo checksum dynamically calculated by the configuration @n only valid for #EMV_CT_GetAppliData, validity bit #EMV_CT_INPUT_APL_CHECKSUM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF12_CHECKSUM, @n ChksumASCIIEMVCO in EMV_CT_APPLIDATA_STRUCT::ChksumASCIIEMVCO, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_EMVCO

  EMV_CT_APPLI_TYPE MasterAID;                  ///< Master AID with the same parameters (stored for grouping reasons) @n Struct definition: ::EMV_CT_APPLI_STRUCT @n Request to configure more than one AID at once, validity bit #EMV_CT_INPUT_APL_MASTER_AID  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF04_AID, @n MasterAID in EMV_CT_APPLIDATA_STRUCT::MasterAID, @n XML Tag: #XML_TAG_APPLIDATA_MASTER_AID
  unsigned char     TxnTypesSale[3];            ///< Since kernel 7.0.3 the risk management can treat other transaction types as sale. Note: This is optional. If just #EMV_ADK_TRAN_TYPE_GOODS_SERVICE is required,
                                                ///< it should be omitted. If an additional transaction types shall be set, fill the all other elements with #EMV_ADK_TRAN_TYPE_GOODS_SERVICE. If you set all three
                                                ///< values different from #EMV_ADK_TRAN_TYPE_GOODS_SERVICE, #EMV_ADK_TRAN_TYPE_GOODS_SERVICE will not be treated as sale anymore.
                                                ///< @n@n Validity bit: #EMV_CT_INPUT_APL_TXN_TYPES_SALE @n TLV tag: #TAG_DFD004_TXN_TYPES_SALE @n XML Tag: #XML_TAG_APPLIDATA_TXN_TYPES_SALE
  unsigned char     TxnTypesCash[3];        ///< Same description as #TxnTypesSale but with #EMV_ADK_TRAN_TYPE_CASH as default and fill element.
                                                ///< @n@n Validity bit: #EMV_CT_INPUT_APL_TXN_TYPES_CASH @n TLV tag: #TAG_DFD005_TXN_TYPES_CASH @n XML Tag: #XML_TAG_APPLIDATA_TXN_TYPES_CASH
  unsigned char     TxnTypesCashback[3];    ///< Same description as #TxnTypesSale but with #EMV_ADK_TRAN_TYPE_CASHBACK as default and fill element.
                                                ///< @n@n Validity bit: #EMV_CT_INPUT_APL_TXN_TYPES_CASHBACK @n TLV tag: #TAG_DFD006_TXN_TYPES_CASHBACK @n XML Tag: #XML_TAG_APPLIDATA_TXN_TYPES_CASHBACK
  unsigned char     TxnTypesRefund[3];      ///< Same description as #TxnTypesSale but with #EMV_ADK_TRAN_TYPE_REFUND as default and fill element.
                                                ///< @n@n Validity bit: #EMV_CT_INPUT_APL_TXN_TYPES_REFUND @n TLV tag: #TAG_DFD007_TXN_TYPES_REFUND @n XML Tag: #XML_TAG_APPLIDATA_TXN_TYPES_REFUND

  unsigned char     Info_Included_Data[8];      ///< Which data is included in the message, see @ref DEF_INPUT_APPLI
} EMV_CT_APPLIDATA_TYPE;  ///< typedef for EMV_CT_APPLIDATA_STRUCT


/// @defgroup DEF_INPUT_APPLI Application data: Which data is included
/// @ingroup DEF_CONF_APPLI
/// @brief Contents of EMV_CT_APPLIDATA_STRUCT::Info_Included_Data
/// @{
/* Byte 1 */
#define  EMV_CT_INPUT_APL_VERSION            0x01  ///< B1b1: EMV_CT_APPLIDATA_STRUCT::VerNum, application version number
#define  EMV_CT_INPUT_APL_NAME               0x02  ///< B1b2: EMV_CT_APPLIDATA_STRUCT::AppName, application name
#define  EMV_CT_INPUT_APL_ASI                0x04  ///< B1b3: EMV_CT_APPLIDATA_STRUCT::ASI, ASI
#define  EMV_CT_INPUT_APL_MERCHANT_CATCODE   0x08  ///< B1b4: EMV_CT_APPLIDATA_STRUCT::BrKey, merchant category code
#define  EMV_CT_INPUT_APL_TID                0x10  ///< B1b5: EMV_CT_APPLIDATA_STRUCT::TermIdent, terminal ID
#define  EMV_CT_INPUT_APL_FLOOR_LIMIT        0x20  ///< B1b6: EMV_CT_APPLIDATA_STRUCT::FloorLimit, floor limt
#define  EMV_CT_INPUT_APL_THRESH             0x40  ///< B1b7: EMV_CT_APPLIDATA_STRUCT::Threshhold, threshhold value
#define  EMV_CT_INPUT_APL_TARGET             0x80  ///< B1b8: EMV_CT_APPLIDATA_STRUCT::TargetPercentage, target percentage
/* Byte 2 */
#define  EMV_CT_INPUT_APL_MAXTARGET          0x01  ///< B2b1: EMV_CT_APPLIDATA_STRUCT::MaxTargetPercentage, maximum target percentage
#define  EMV_CT_INPUT_APL_TAC_DENIAL         0x02  ///< B2b2: EMV_CT_APPLIDATA_STRUCT::TACDenial, TAC Denial
#define  EMV_CT_INPUT_APL_TAC_ONLINE         0x04  ///< B2b3: EMV_CT_APPLIDATA_STRUCT::TACOnline, TAC Online
#define  EMV_CT_INPUT_APL_TAC_DEFAULT        0x08  ///< B2b4: EMV_CT_APPLIDATA_STRUCT::TACDefault, TAC Default
#define  EMV_CT_INPUT_APL_TDOL               0x10  ///< B2b5: EMV_CT_APPLIDATA_STRUCT::Default_TDOL, Default TDOL
#define  EMV_CT_INPUT_APL_DDOL               0x20  ///< B2b6: EMV_CT_APPLIDATA_STRUCT::Default_DDOL, Default DDOL
#define  EMV_CT_INPUT_APL_MERCHANT_IDENT     0x40  ///< B2b7: EMV_CT_APPLIDATA_STRUCT::MerchIdent, merchant identification
#define  EMV_CT_INPUT_APL_ADD_TAGS           0x80  ///< B2b8: EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM, additional terminal tags
/* Byte 3 */
#define  EMV_CT_INPUT_APL_MAND_TAGS          0x01  ///< B3b1: EMV_CT_APPLIDATA_STRUCT::Mandatory_Tags_CRD, mandatory tags
#define  EMV_CT_INPUT_APL_TERM_CAPS          0x02  ///< B3b2: EMV_CT_APPLIDATA_STRUCT::App_TermCap, terminal capabilities for app
#define  EMV_CT_INPUT_APL_FLOW_CAPS          0x04  ///< B3b3: EMV_CT_APPLIDATA_STRUCT::App_FlowCap, flow capabilities
#define  EMV_CT_INPUT_APL_AIP_CVM_NOTSUPP    0x08  ///< B3b4: EMV_CT_APPLIDATA_STRUCT::AIP_CVM_not_supported, CVM in AIP disabled
#define  EMV_CT_INPUT_APL_COUNTRY_CODE       0x10  ///< B3b5: EMV_CT_APPLIDATA_STRUCT::App_CountryCodeTerm, country code
#define  EMV_CT_INPUT_APL_ADD_TERM_CAPS      0x20  ///< B3b6: EMV_CT_APPLIDATA_STRUCT::App_TermAddCap, addtional terminal capabilties for tapp
#define  EMV_CT_INPUT_APL_PRIO_APPS          0x40  ///< B3b7: EMV_CT_APPLIDATA_STRUCT::xAIDPrio, priority application
#define  EMV_CT_INPUT_APL_MID                0x80  ///< B3b8: EMV_CT_APPLIDATA_STRUCT::tucFallbackMIDs, MID relation
/* Byte 4 */
#define  EMV_CT_INPUT_APL_EMV_APPKIND        0x01  ///< B4b1: EMV_CT_APPLIDATA_STRUCT::EMV_Application, EMV Application Type
#define  EMV_CT_INPUT_APL_SPECIAL_TXN        0x02  ///< B4b2: EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX, configuration of special transactions
#define  EMV_CT_INPUT_APL_FALLBACK           0x04  ///< B4b3: EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling, fallback handling for this app
#define  EMV_CT_INPUT_APL_CDA                0x08  ///< B4b4: EMV_CT_APPLIDATA_STRUCT::CDAProcessing, CDA mode
#define  EMV_CT_INPUT_APL_AC_BEFOREAFTER     0x10  ///< B4b5: EMV_CT_APPLIDATA_STRUCT::ACBeforeAfter, TAC processing for offline only
#define  EMV_CT_INPUT_APL_POS_ENTRY          0x20  ///< B4b6: EMV_CT_APPLIDATA_STRUCT::POS_EntryMode, POS entry mode
#define  EMV_CT_INPUT_APL_TRM_TYPE           0x40  ///< B4b7: EMV_CT_APPLIDATA_STRUCT::App_TermTyp, terminal type
#define  EMV_CT_INPUT_APL_CUSTOMER_CVM       0x80  ///< B4b8: EMV_CT_APPLIDATA_STRUCT::Customer_CVM, customer CVM
/* Byte 5 */
#define  EMV_CT_INPUT_APL_CHECKSUM_PARAMS    0x01  ///< B5b1: EMV_CT_APPLIDATA_STRUCT::Chksum_Params, checksum parmeters
#define  EMV_CT_INPUT_APL_CHECKSUM           0x02  ///< B5b2: EMV_CT_APPLIDATA_STRUCT::ChksumASCIIEMVCO, EMVCo checksum for this app
#define  EMV_CT_INPUT_APL_MASTER_AID         0x04  ///< B5b3: EMV_CT_APPLIDATA_STRUCT::MasterAID, Master AID for groups
#define  EMV_CT_INPUT_APL_ADD_CRD_TAGS       0x08  ///< B5b4: EMV_CT_APPLIDATA_STRUCT::Additional_Tags_CRD, additional tags available on the ICC
#define  EMV_CT_INPUT_APL_ADD_VERSIONS       0x10  ///< B5b5: EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, additional version number of AID (optional)
#define  EMV_CT_INPUT_APL_SEC_LIMIT          0x20  ///< B5b6: EMV_CT_APPLIDATA_STRUCT::Security_Limit, limit below which other terminal capabilties apply (optional)
#define  EMV_CT_INPUT_APL_SEC_CAPS           0x40  ///< B5b7: EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, terminal capabilties below the security limit (optional)
// free bit 0x80

// byte 6, EMV_CT_APPLIDATA_STRUCT::Info_Included_Data[5]
#define  EMV_CT_INPUT_APL_TXN_TYPES_SALE     0x01  ///< B6b1: EMV_CT_APPLIDATA_STRUCT::TxnTypesSale additional transaction types handled as sale (optional)
#define  EMV_CT_INPUT_APL_TXN_TYPES_CASH     0x02  ///< B6b2: EMV_CT_APPLIDATA_STRUCT::TxnTypesCash additional transaction types handled as cash (optional)
#define  EMV_CT_INPUT_APL_TXN_TYPES_CASHBACK 0x04  ///< B6b3: EMV_CT_APPLIDATA_STRUCT::TxnTypesCashback additional transaction types handled as cashack (optional)
#define  EMV_CT_INPUT_APL_TXN_TYPES_REFUND   0x08  ///< B6b4: EMV_CT_APPLIDATA_STRUCT::TxnTypesRefund additional transaction types handled as refund (optional)

/// @}


/// @defgroup CVM_NOT_SUPP Byte defines for CVM not supported / CVM not required
/// @ingroup DEF_CONF_APPLI
/// @brief see @c AIP_CVM_not_supported and EMV_CT_APPLIDATA_STRUCT::CVM_not_required
/// @{
#define  CVM_CONTINUE                 0x00 ///< continue transaction
#define  CVM_DEFAULT_SIGN             0x01 ///< execute CVM signature
#define  CVM_DEFAULT_ONLINE_PIN       0x02 ///< execute CVM online PIN
#define  CVM_ABORT                    0x03 ///< abort transaction (only for @c AIP_CVM_not_supported)
/// @}


/// @defgroup APP_FLOW_CAPS Defines for Application flow capabilities
/// @ingroup DEF_CONF_APPLI
/// @brief Application (transaction flow) capabilities (see EMV_CT_APPLIDATA_STRUCT::App_FlowCap)
/// @{
/* Byte 1 */
#define  FORCE_RISK_MANAGEMENT               0x01  ///< B1b1: Perform Terminal Risk Management regardless of Application Interchange Profile bit "Terminal risk management is to be performed". **Important Note** Enable this flag because it is required according EMVCo Book 3, chapter 10.6 Terminal Risk Management.
#define  BLACKLIST                           0x02  ///< B1b2: Enables blacklist/transaction log callback (aka exception list, hot list), @ref EMV_CT_CALLBACK_FnT (#TAG_BF03_CBK_LOG_HOTLIST) will be invoked @n Depends on #EMV_CT_TRXOP_HOTLST_LOG_CALLBACK and #MS_RETURN_CALLBACKS
#define  TRANSACTION_LOG                     0x04  ///< B1b3: Enables blacklist/transaction log callback for avoiding spit payments according EMVCo Book 3, chapter 10.6.1 Floor Limits. @ref EMV_CT_CALLBACK_FnT (#TAG_BF03_CBK_LOG_HOTLIST) will be invoked @n Depends on #EMV_CT_TRXOP_HOTLST_LOG_CALLBACK and #MS_RETURN_CALLBACKS
#define  PIN_BYPASS                          0x08  ///< B1b4: PIN bypass supported
#define  FORCE_ONLINE                        0x10  ///< B1b5: Feature "Force online" supported (suspicious customer). This relates to Terminal Verification Result B4b4 "Merchant forced transaction online". Note: Force Online is performed if both flags are set, the support flag here and the transaction flow option #EMV_CT_PAYMENT_STRUCT::Force_Online.
#define  FORCE_ACCEPTANCE                    0x20  ///< B1b6: Feature "Force acceptance" supported (accept on merchant's risk). **Use with care**: This overrules the card's or terminal or authorisation host decision. Note: Force Acceptance is performed if both flags are set, the support flag here and the transaction flow option #EMV_CT_PAYMENT_STRUCT::Force_Acceptance.
/* Byte 2 */
#define  CASH_SUPPORT                        0x01  ///< B2b1: Support of cash transactions
#define  CASHBACK_SUPPORT                    0x02  ///< B2b2: Support of cashback transactions
#define  EMV_CT_CHECK_INCONS_TRACK2_PAN      0x04  ///< B2b3: Check consistency of track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT) provided by ICC. @n Mandated for MasterCard and Amex. @n Validate format and that contents matches to #TAG_5A_APP_PAN and #TAG_5F24_APP_EXP_DATE. @n See also #EMV_CT_FALLBACK_ON_INCONS_TRACK2.
#define  EMV_CT_CONF_AMOUNT_PIN              0x08  ///< B2b4: Confirm amount and enter PIN in one display. @n Invoking callback for amount confirmation depends on #EMV_CT_TRXOP_AMOUNT_CONF. @n May be deactivated by #MS_RETURN_CALLBACKS.
#define  EMV_CT_DOMESTIC_CHECK               0x10  ///< B2b5: Callback function @ref EMV_CT_CALLBACK_FnT (#TAG_BF06_CBK_LOCAL_CHECKS) will be called. @n Needs to be activated per transaction by #EMV_CT_TRXOP_LOCAL_CHCK_CALLBACK. @n May be deactivated by #MS_RETURN_CALLBACKS.
#define  TRANSACTION_TYPE_17_FOR_CASH        0x20  ///< B2b6: Enable mapping of transaction type for cash transaction from 0x01 to 0x17. See also the kernel's transaction type enhancement, #EMV_CT_APPLIDATA_STRUCT::TxnTypesCash
#define  EMV_CT_CHECK_INCONS_TRACK2_NO_EXP   0x40  ///< B2b7: Check consistency of track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT) provided by ICC. @n Mandated for MasterCard and Amex. @n Validate format and that contents matches to #TAG_5A_APP_PAN @n Only effective if #EMV_CT_CHECK_INCONS_TRACK2_PAN is deactivated. See also #EMV_CT_FALLBACK_ON_INCONS_TRACK2.
/* Byte 3 */
#define  REFERRAL_AFTER_TRX                  0x01  ///< B3b1: In case voice referral is required: Chip transaction is terminated with AAC. Referral transaction is performed subsequently as "CardNotPresent" (German handling).
#define  DCC_CHECK                           0x02  ///< B3b2: Enable DCC (Dynamic currency conversion). @n Invocation depends on #MS_RETURN_CALLBACKS, #EMV_CT_TRXOP_DCC_CALLBACK, and #EMV_CT_TRXOP_DCC_CALLBACK_ALWAYS.
#define  REFUND_CONFIRM_AMOUNT               0x04  ///< B3b3: Confirm amount during refund transaction @n Invoking callback for amount confirmation depends on #EMV_CT_TRXOP_AMOUNT_CONF and #MS_RETURN_CALLBACKS.
#define  EMV_CT_USE_CFG_APPL_NAME            0x08  ///< B3b4: **Use with care**: Do not use card's application name (EMVCo rule: 9F12 prior to 50) but always the default label. Effective for cardholder's application selection.
#define  EMV_CT_FALLBACK_ON_INCONS_TRACK2    0x10  ///< B3b5: Valid only in case of activated #EMV_CT_CHECK_INCONS_TRACK2_PAN. @n If activated: Check will be done before Cardholder Verification. In case of fail EMV ADK will process fallback handling (may result in #EMV_ADK_FALLBACK). E.g. required for German dcpos 2.4, may be used for "M/Chip Functional Architecture", AMEX ED-031 in test plan ver. 4.2. @n If not activated: Check will be done just before processing 1st cryptogram. If fail: GENAC1 will be requested with AAC. E.g. required for MasterCard REQ22, AMEX ED-031 in Swedish AMEX test plan 1.0.
#define  EMV_CT_AMOUNT_CONFIRM_ON_SIGNATURE  0x20  ///< B3b6: Customer amount confirmation in case of CVM signature @n Invoking callback for amount confirmation depends on #EMV_CT_TRXOP_AMOUNT_CONF and #MS_RETURN_CALLBACKS.
#define  EMV_CT_SDA_SELECTED_TVR_ON          0x40  ///< B3b7: Activate the support of the TVR byte SDA selected, introduced in Nov 2013 for kernels not yet supporting this feature, configurable per AID (for kernels supporting this it'll be always activated, config is don't care)
#define  EMV_CASHBACK_OFFLINE_SUPPORT        0x80  ///< B3b8: If set cash back transactions are not automatically forced online. **Use with care**: Usual business rules require online processing.
/* Byte 4 */
#define  REFUND_PROCESSING_RESTRICTIONS      0x01  ///< B4b1: Perform Processing restrictions for Refund transactions (French Refund Requirements).
#define  REFUND_NO_GENERATE_AC               0x02  ///< B4b2: Do not perform Generate AC when finishing Refund transactions (French Refund Requirements).
#define  REFUND_FLOW_INTERAC                 0x04  ///< B4b3: Perform full EMV transaction although transaction type is refund. **Use with care**: Is is recommended by EMVCo to abort the transaction once the account data has been read. Activation of the "full" flow requires that all cards and background systems support it.
#define  REFUND_NO_ZERO_AMOUNT               0x08  ///< B4b4: Do NOT set the amount to zero in CDOL1 for Refund transactions with Generate AC. **Use with care**: Card read transactions are just for getting the account data and it is best practice to "close" the card session by requesting an ARQC with zero amount.
/* Byte 5 */
/// @}


/// @defgroup CDA_MODES CDA modes
/// @ingroup DEF_CONF_APPLI
/// @brief DF3E, CDA mode to be executed for the named AID
/// @{
#define  CDA_EMV_MODE_1               0 ///< EMVCo CDA mode 1: do CDA for ARQC: yes, for TC after successful online communication: yes
#define  CDA_EMV_MODE_2               1 ///< EMVCo CDA mode 2: do CDA for ARQC: yes, for TC after successful online communication: no
#define  CDA_EMV_MODE_3               2 ///< EMVCo CDA mode 3: do CDA for ARQC: no,  for TC after successful online communication: no
#define  CDA_EMV_MODE_4               3 ///< EMVCo CDA mode 4: do CDA for ARQC: no,  for TC after successful online communication: yes
/// @}

/// @defgroup FB_FINAL_SEL Fallback handling after Final Select
/// @ingroup DEF_CONF_APPLI
/// @brief DF18, see EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling
/// @{
#define  FB_DEFAULT_EMVCO             0x00 ///< Fallback handling like described by EMVCo
#define  FB_GERMAN_POS_SPEC           0x01 ///< Fallback handling according to German spec. Additional specification is done by #EMV_ADK_FALLBACK_AFTER_CVM_NO and #EMV_ADK_FALLBACK_AFTER_CVM_YES.
#define  FB_NO_UNATTENDED_FALLB       0x80 ///< Unattended terminal: Fallback forbidden
/// @}




// ========================================================================================================
// === EMV TRANSACTION DATA ===
// ========================================================================================================

/// @ingroup ADK_TRX_EXEC
/// @brief Domestic non-EMV chip applications supporting AID selection
/// @n see EMV_CT_APPS_SELECT_STRUCT::xDomestic_Chip
/// @n TLV tag #TAG_EMV_CONFORM
typedef struct EMV_CT_DOM_CHIP_STRUCT
{
  EMV_CT_APPLI_TYPE xAIDChip;             ///< AID of domestic application (::EMV_CT_APPLI_STRUCT) @n TLV tag #TAG_DF04_AID
  unsigned char     ucAppName[16+1];      ///< Application label @n TLV tag #TAG_50_APP_LABEL
  unsigned char     ucASI;                ///< Application selection indicator @n Must the card's AID match the configured AID exactly? @n @c 0 ... yes @n @c 1 ... no @n TLV tag #TAG_DF20_ASI
  unsigned char     xuc_Special_TRX[8];   ///< @brief List of special allowed transactions (manual reversal, refund, reservation, tip, Referral)
                                          ///< @n see @ref SPECIAL_TRXS @n will be checked against EMV_CT_SELECT_STRUCT::TransType @n TLV tag #TAG_DF1C_SPECIAL_TRX
  unsigned char     uc_EMVConformSelect;  ///< @brief Selection procedure is EMV conform (0) or not (1)
                                          ///< @n In case of non EMV conformance selection process there is no select issued to the card but the application is added to the candidates list
                                          ///< @n TLV tag #TAG_EMV_CONFORM
} EMV_CT_DOM_CHIP_TYPE;  ///< typedef for EMV_CT_DOM_CHIP_STRUCT


/// @ingroup ADK_TRX_EXEC
/// @brief list of MID applications supported by terminal
///   @n DF4C, see @c xFallback_MS in ::EMV_CT_SELECT_STRUCT and ::EMV_CT_SELECTRES_STRUCT
typedef struct EMV_CT_FALLBCK_MSR_STRUCT
{
  unsigned char  xMID;                ///< MID of domestic MSR application (index managed by calling application) @n TLV tag #TAG_DF17_FALLBACK_MIDS
  unsigned char  xuc_Special_TRX[8];  ///< @brief List of special allowed transactions (manual reversal, refund, reservation, tip, Referral)
                                      ///< @n see @ref SPECIAL_TRXS @n will be checked against @c TransType of ::EMV_CT_SELECT_TYPE
                                      ///< @n TLV tag #TAG_DF1C_SPECIAL_TRX
  unsigned char  ucFallback;          ///< Conditions under which a fallback to this MSR application is allowed @n see @ref FB_OPTIONS @n TLV tag #TAG_DF18_FALLABCK
  unsigned char  ucOptions;           ///< Further conditions for fallback decision, not contained in DC POS configuration tag DF18 @n TLV tag #TAG_DF3A_FB_MSR_OPTIONS
} EMV_CT_FALLBCK_MSR_TYPE;  ///< typedef for EMV_CT_FALLBCK_MSR_STRUCT


/// @ingroup ADK_TRX_EXEC
/// @brief Transaction parameters of an EMV transaction
///
/// Used in ::EMV_CT_SELECT_STRUCT and ::EMV_CT_TRANSAC_STRUCT .
/// @n
typedef struct EMV_CT_PAYMENT_STRUCT
{
  unsigned char           Amount[6];                 ///< @brief Transaction amount (will also be used for EMVCo tags 81, 9F3A)
                                                     ///< @n mandatory @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_9F02_NUM_AMOUNT_AUTH, validity bit #INPUT_SEL_AMOUNT resp. #INPUT_OFL_AMOUNT
  unsigned char           CurrencyTrans[2];          ///< @brief Currency code to be used for transaction. Only needed in case of DCC
                                                     ///< @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_5F2A_TRANS_CURRENCY, validity bit: #INPUT_SEL_AMOUNT_CURRENCY resp. #INPUT_OFL_AMOUNT_CURRENCY
  unsigned char           ExpTrans;                  ///< @brief Currency exponent to be used for transaction.
                                                     ///< @n Only needed in case of DCC. @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP, validity bit: #INPUT_SEL_CUREXPONENT resp. #INPUT_OFL_CUREXPONENT
  unsigned char           Date[3];                   ///< @brief Transaction date (YYMMDD) @n mandatory @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_9A_TRANS_DATE, validity bit: #INPUT_SEL_DATE resp. #INPUT_OFL_DATE
  unsigned char           Time[3];                   ///< @brief Transaction time (HHMMSS) @n mandatory @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_9F21_TRANS_TIME, validity bit: #INPUT_SEL_TIME resp. #INPUT_OFL_TIME
  unsigned char           TransCount[4];             ///< @brief Transaction sequence counter managed by calling application @n mandatory @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_9F41_TRANS_SEQUENCE_NB, validity bit: #INPUT_SEL_TXN_COUNTER resp. #INPUT_OFL_TXN_COUNTER
  unsigned char           Cashback_Amount[6];        ///< @brief Cashback Amount
                                                     ///< @n Calling application must care that this amount is already included in 9F02 presented to the EMV ADK
                                                     ///< @n dependency to #CASHBACK_SUPPORT in @ref APP_FLOW_CAPS @n Can also be changed by EMV_CT_updateTxnTags()
                                                     ///< @n TLV tag #TAG_9F03_NUM_AMOUNT_OTHER, validity bit: #INPUT_SEL_CB_AMOUNT resp. #INPUT_OFL_CB_AMOUNT
  unsigned char           Force_Online;              ///< @brief Force transaction to go online @c BOOL, dependency to #FORCE_ONLINE in @ref APP_FLOW_CAPS
                                                     ///< @n TLV tag #TAG_DF40_FORCE_ONLINE, validity bit: #INPUT_SEL_FORCE_ONLINE resp. #INPUT_OFL_FORCE_ONLINE
  unsigned char           Force_Acceptance;          ///< @brief Force acceptance of transaction @c BOOL, dependency to #FORCE_ACCEPTANCE in @ref APP_FLOW_CAPS
                                                     ///< @n TLV tag #TAG_DF43_FORCE_ACCEPT, validity bit: #INPUT_SEL_FORCE_ACCEPT resp. #INPUT_OFL_FORCE_ACCEPT
  unsigned char           Online_Switch;             ///< @brief Force transaction to go online @c BOOL, request ARQC from card, but don't set "suspicious bit" (TVR B4b4)
                                                     ///< @n TLV tag #TAG_DF48_ONLINE_SWITCH, validity bit: #INPUT_SEL_ONLINE_SWITCH resp. #INPUT_OFL_ONLINE_SWITCH
  unsigned char           uc_AccountType;            ///< @brief Account Type, EMVCo definitions to be used: 0=Default, 0x10=Savings, 0x20=Cheque-Debit, 0x30=Credit
                                                     ///< @n TLV tag #TAG_5F57_ACCOUNT_TYPE, validity bit: #INPUT_SEL_ACCOUNT_TYPE resp. #INPUT_OFL_ACCOUNT_TYPE
  char                    PreSelected_Language;      ///< @brief Preselected customer language, see @ref TF_LANGUAGES
                                                     ///< @n TLV tag #TAG_DF47_PRIOR_LANG_SEL, validity bit: #INPUT_SEL_LANGUAGE resp. #INPUT_OFL_LANGUAGE
  unsigned char           uc_AmountConfirmation;     ///< @brief Configure Amount Confirmation Dialog @n Possible values see @ref CONFIRM_AMOUNT_WHEN
                                                     ///< @n Anyhow the setting of this parameter does only take effect in case of not set @ref EMV_CT_CONF_AMOUNT_PIN (EMV_CT_APPLIDATA_STRUCT::App_FlowCap)
                                                     ///< @n TLV tag #TAG_DF34_AMOUNT_CONF, validity bit: #INPUT_SEL_AMOUNT_CONF resp. #INPUT_OFL_AMOUNT_CONF
  EMV_CT_TAGLIST_TYPE     Additional_Result_Tags;    ///< @deprecated Use EMV_CT_fetchTxnTags() instead.
} EMV_CT_PAYMENT_TYPE;   ///< typedef for EMV_CT_PAYMENT_STRUCT


/// @ingroup ADK_TRX_EXEC
/// @brief Application selection parameters of an EMV transaction
///        used for EMV_CT_SELECT_STRUCT::SEL_Data
typedef struct EMV_CT_APPS_SELECT_STRUCT
{
  unsigned char            No_DirectorySelect;                       ///< @brief Application Selection: Skip PSE (Payment System Environment) handling @n BOOL
                                                                     ///< @n TLV tag #TAG_DF45_NO_DIR_SELECT @n Validity bit #INPUT_SEL_NO_PSE
  char                     ucCardholderConfirmation;                 ///< @brief Support of cardholder confirmation, see @ref DEF_CARD_CONF
                                                                     ///< @n TLV tag #TAG_DF4D_CARD_CONF @n Validity bit #INPUT_SEL_CARDCONF
  EMV_CT_APPLI_TYPE        ExcludeEmvAIDs[3];                        ///< @brief Exclude EMV AIDs for this transaction dynamically (without removing from the config)
                                                                     ///< @n Struct elements: ::EMV_CT_APPLI_STRUCT @n TLV tag #TAG_EXCLUDE_AID @n Validity bit #INPUT_SEL_EXCLUDE_AID
  unsigned char            countDomesticChip;                        ///< @brief Number of domestic non-EMV chip applications supporting AID selection, max. see #EMV_ADK_MAX_DOM_CHP
                                                                     ///< @n Validity bit #INPUT_SEL_DOMCHIP @n TLV tag #TAG_DOM_CHIP
  EMV_CT_DOM_CHIP_TYPE     xDomestic_Chip[EMV_ADK_MAX_DOM_CHP];      ///< @brief Data of domestic chip applications (::EMV_CT_DOM_CHIP_STRUCT) @n Max. number see #EMV_ADK_MAX_DOM_CHP
                                                                     ///< @n Validity bit #INPUT_SEL_DOMCHIP @n TLV tag #TAG_DOM_CHIP
  unsigned char            countFallbackMS;                          ///< @brief Number of MID applications @n Max. see #EMV_ADK_MAX_FB_MSR @n Necessary for fallback handling according to German ZKA specification
                                                                     ///< @n Validity bit #INPUT_SEL_FALLBACK_MSR @n TLV tag #TAG_FALLBACK_MSR
  EMV_CT_FALLBCK_MSR_TYPE  xFallback_MS[EMV_ADK_MAX_FB_MSR];         ///< @brief List of MID applications (::EMV_CT_FALLBCK_MSR_STRUCT)
                                                                     ///< @n Not presented to customer but automatically reduced to allow MID apps @n Max. number see #EMV_ADK_MAX_FB_MSR
                                                                     ///< @n Validity bit #INPUT_SEL_FALLBACK_MSR @n TLV tag #TAG_FALLBACK_MSR
  unsigned char            countCommonCands;                         ///< @brief Number remaining candidates in the modified candidate list
                                                                     ///< @n Only valid in reentrance mode using the candidate list manipulation option @ref RETURN_CAND_LIST_PREPROC.
                                                                     ///< @n Validity bit #INPUT_SEL_MOD_CANDLIST @n TLV tag #TAG_DF8F01_MANI_CANDLIST
  unsigned char            ModifiedCandidates[EMV_CT_COMMON_CANDS];  ///< @brief Modified candidate list if the txn steps indicate
                                                                     ///< @n Only valid in reentrance mode using the candidate list manipulation option @ref RETURN_CAND_LIST_PREPROC.
                                                                     ///< @n Validity bit #INPUT_SEL_MOD_CANDLIST @n TLV tag #TAG_DF8F01_MANI_CANDLIST
} EMV_CT_APPS_SELECT_TYPE;   ///< typedef for EMV_CT_APPS_SELECT_STRUCT



// ========================================================================================================
// === APPLICATION SELECTION ===
// ========================================================================================================

/// @defgroup FB_OPTIONS Fallback options for magstripe applications
/// @ingroup DEF_FLOW_INPUT
/// @brief for use in @c ucFallback of ::EMV_CT_FALLBCK_MSR_TYPE
/// @{
#define FB_NEVER        0x00 ///< fallback to magstripe forbidden
#define FB_CHIP_APP     0x01 ///< fallback allowed, but not in case of -# blocked chip -# a chip application assigned to this magstripe application is blocked
#define FB_APP          0x02 ///< fallback allowed, but not in case a chip application assigned to this magstripe application is blocked
#define FB_CHIP         0x03 ///< fallback allowed but not in case of blocked chip
#define FB_ALWAYS       0x04 ///< fallback allowed in every case
/// @}

/// @defgroup FB_MSR_OPTIONS Bit field for additional fallback options for magstripe applications (not originating from DC POS tag DF18)
/// @ingroup DEF_FLOW_INPUT
/// @brief for use in @c fallbackMsrOptions of ::EMV_CT_SELECT_TYPE
/// @{
#define FB_OPT_CASHBACK 0x01 ///< fallback magstripe application supports payment with cash back
/// @}


/// @ingroup ADK_TRX_EXEC
/// @brief struct for interface to EMV_CT_StartTransaction()
typedef struct EMV_CT_SELECT_STRUCT // EMV_CT_SELECT_TYPE
{
  unsigned char            InitTXN_Buildlist;      ///< @brief Decides if a new candidate list will be built
                                                   ///< @n Optional. For values see @ref BUILD_LIST_OPTIONS
                                                   ///< @n TLV tag #TAG_DF05_BUILD_APPLILIST
                                                   ///< @n Validity bit: #INPUT_SEL_BUILDLIST
  unsigned char            TransType;              ///< @brief Transaction type, according to ISO 8583 - Annex A: Processing Code, Position 1 + 2
                                                   ///< @n see @ref TRANS_TYPES @n mandatory
                                                   ///< @n Is checked against EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX, ::EMV_CT_DOM_CHIP_TYPE, and ::EMV_CT_FALLBCK_MSR_TYPE
                                                   ///< @n Can also be changed by EMV_CT_updateTxnTags() @n TLV tag #TAG_9C_TRANS_TYPE
                                                   ///< @n Validity bit: #INPUT_SEL_TTYPE
  EMV_CT_PAYMENT_TYPE      TXN_Data;               ///< @brief Transaction data (::EMV_CT_PAYMENT_STRUCT)
                                                   ///< @n Can be presented at the beginning of the transaction or (if not yet available) later, e.g. after the final select.
                                                   ///< @n Anyhow data should be presented as soon as available.
  EMV_CT_APPS_SELECT_TYPE  SEL_Data;               ///< Data for application selection process (see ::EMV_CT_APPS_SELECT_STRUCT)
  unsigned char            TxnOptions[5];          ///< Options, allowed values see @ref TXN_OPTIONS, validity bit #INPUT_SEL_TXN_OPTIONS
                                                   ///< @n TLV tag #TAG_DF36_TRX_OPTIONS
  unsigned char            TxnSteps[3];            ///< This parameter allows to interrupt the transaction at given steps for intermediate application processing, allowed values see @ref TXN_STEPS
                                                   ///< @n TLV tag #TAG_DF37_TRX_STEPS @n Validity bit #INPUT_SEL_TXN_STEPS
  unsigned char            Info_Included_Data[8];  ///< Which data is included in the message, see @ref DEF_INPUT_SELECT
} EMV_CT_SELECT_TYPE;  ///< typedef for EMV_CT_SELECT_STRUCT


/// @defgroup DEF_INPUT_SELECT Select input: Which data is included
/// @ingroup DEF_FLOW_INPUT
/// @brief Contents of EMV_CT_SELECT_STRUCT::Info_Included_Data
/// @{
/* Byte 1 */
#define  INPUT_SEL_AMOUNT              0x01  ///< B1b1: @c txn amount, EMV_CT_PAYMENT_STRUCT::Amount
#define  INPUT_SEL_AMOUNT_CURRENCY     0x02  ///< B1b2: @c currency of txn, EMV_CT_PAYMENT_STRUCT::CurrencyTrans
#define  INPUT_SEL_CUREXPONENT         0x04  ///< B1b3: @c currency exponent of txn, EMV_CT_PAYMENT_STRUCT::ExpTrans
#define  INPUT_SEL_DATE                0x08  ///< B1b4: @c date of txn, EMV_CT_PAYMENT_STRUCT::Date
#define  INPUT_SEL_TIME                0x10  ///< B1b5: @c time of txn, EMV_CT_PAYMENT_STRUCT::Time
#define  INPUT_SEL_TTYPE               0x20  ///< B1b6: @c transaction type of txn, EMV_CT_SELECT_STRUCT::TransType
#define  INPUT_SEL_EXCLUDE_AID         0x40  ///< B1b7: @c AIDs to exclude for this transaction, EMV_CT_APPS_SELECT_STRUCT::ExcludeEmvAIDs
#define  INPUT_SEL_BUILDLIST           0x80  ///< B1b8: @c build the candidate list or not, EMV_CT_SELECT_STRUCT::InitTXN_Buildlist
/* Byte 2 */
#define  INPUT_SEL_FORCE_ONLINE        0x01  ///< B2b1: @c force it online (suspicious, EMVCo), EMV_CT_PAYMENT_STRUCT::Force_Online
#define  INPUT_SEL_FORCE_ACCEPT        0x02  ///< B2b2: @c force acceptance, EMV_CT_PAYMENT_STRUCT::Force_Acceptance
#define  INPUT_SEL_NO_PSE              0x04  ///< B2b3: @c no PSE, EMV_CT_APPS_SELECT_STRUCT::No_DirectorySelect
#define  INPUT_SEL_CARDCONF            0x08  ///< B2b4: @c cardholder confirmation, EMV_CT_APPS_SELECT_STRUCT::ucCardholderConfirmation
#define  INPUT_SEL_ONLINE_SWITCH       0x10  ///< B2b5: @c force it online (domestic need), EMV_CT_PAYMENT_STRUCT::Online_Switch
#define  INPUT_SEL_TXN_OPTIONS         0x20  ///< B2b6: @c transaction options, EMV_CT_SELECT_STRUCT::TxnOptions
#define  INPUT_SEL_DOMCHIP             0x40  ///< B2b7: @c domestic chip apps included, EMV_CT_APPS_SELECT_STRUCT::countDomesticChip and EMV_CT_APPS_SELECT_STRUCT:: xDomestic_Chip
#define  INPUT_SEL_FALLBACK_MSR        0x80  ///< B2b8: @c fallback msr apps included, EMV_CT_APPS_SELECT_STRUCT:: countFallbackMS and EMV_CT_APPS_SELECT_STRUCT::xFallback_MS
/* Byte 3 */
#define  INPUT_SEL_TXN_COUNTER         0x01  ///< B3b1: @c transaction counter, EMV_CT_PAYMENT_STRUCT::TransCount
#define  INPUT_SEL_TXN_STEPS           0x02  ///< B3b2: @c transaction steps if interrupt needed instead of 1 step processing, EMV_CT_SELECT_STRUCT::TxnSteps
#define  INPUT_SEL_CB_AMOUNT           0x04  ///< B3b3: @c cashback amount, EMV_CT_PAYMENT_STRUCT::Cashback_Amount
#define  INPUT_SEL_ACCOUNT_TYPE        0x08  ///< B3b4: @c account type, EMV_CT_PAYMENT_STRUCT::uc_AccountType
#define  INPUT_SEL_LANGUAGE            0x10  ///< B3b5: @c language preselected, EMV_CT_PAYMENT_STRUCT::PreSelected_Language
#define  INPUT_SEL_AMOUNT_CONF         0x20  ///< B3b6: @c amount confirmation, EMV_CT_PAYMENT_STRUCT::uc_AmountConfirmation
#define  INPUT_SEL_ADD_TAGS            0x40  ///< B3b7: @deprecated Don't use EMV_CT_PAYMENT_STRUCT::Additional_Result_Tags anymore. Use EMV_CT_fetchTxnTags() instead.
#define  INPUT_SEL_MOD_CANDLIST        0x80  ///< B3b8: @c modified candidate list (only valid in reentrance mode using the candidate list manipulation option @ref RETURN_CAND_LIST_PREPROC)

/// @}


/// @defgroup DEF_CARD_CONF Support of cardholder confirmation
/// @ingroup DEF_FLOW_INPUT
/// @brief DF4D, see @c ucCardholderConfirmation in ::EMV_CT_SELECT_TYPE
/// @{
#define  CARD_CONF_YES          0x00  ///< @b default @n support of cardholder confirmation
#define  CARD_CONF_NO           0x01  ///< no cardholder confirmation, e.g. no cardholder display
/// @}


/// @defgroup BUILD_LIST_OPTIONS Buildlist options for EMV_CT_StartTransaction()
/// @ingroup DEF_FLOW_INPUT
/// @brief see EMV_CT_SELECT_TYPE::InitTXN_Buildlist
/// @{
#define  REUSE_LIST_REMOVE_AID      0x00  ///< B1b0: reuse the existing list and remove the currently activated AID, e.g. 6985 at GPO
#define  BUILD_NEW                  0x01  ///< B1b1: build a new candidate list (needs to be set on start of transaction)

#define  REUSE_EXISTING_LIST_SEL_0  0xF0  ///< B1b8: reuse the existing list do not remove anything and select candidate 1 of the candidate list
#define  REUSE_EXISTING_LIST_SEL_1  0xF1  ///< B1b8: reuse the existing list do not remove anything and select candidate 2 of the candidate list
#define  REUSE_EXISTING_LIST_SEL_2  0xF2  ///< B1b8: reuse the existing list do not remove anything and select candidate 3 of the candidate list
#define  REUSE_EXISTING_LIST_SEL_3  0xF3  ///< B1b8: reuse the existing list do not remove anything and select candidate 4 of the candidate list
#define  REUSE_EXISTING_LIST_SEL_4  0xF4  ///< B1b8: reuse the existing list do not remove anything and select candidate 5 of the candidate list
  // continues up to 0xFF for up to 15 candidates
/// @}


/// @ingroup ADK_TRX_EXEC
/// @brief result data of EMV_CT_StartTransaction()
typedef struct EMV_CT_SELECTRES_STRUCT
{
  EMV_CT_APPLI_TYPE        T_84_DFName;                       ///< Dedicated File (DF) Name (::EMV_CT_APPLI_STRUCT) @n If provided bit #DF61_SEL_84_DF_NAME has to be set @n TLV tag #TAG_84_DF_NAME
  unsigned char            AppName[16+1];                     ///< Application name (tag 9F12, tag 50, or configured default EMV_CT_APPLIDATA_STRUCT::AppName) @n If provided bit #DF61_SEL_AGREED_APPNAME has to be set  @n EMVCo rules may be overwritten by #EMV_CT_USE_CFG_APPL_NAME @n TLV tag #TAG_50_APP_LABEL
  unsigned char            T_50_ApplicationName[16+1];        ///< Application label (tag 50) @n If provided bit #DF61_SEL_50_APPLICATION_NAME has to be set  @n TLV tag #TAG_DF33_APPLICATION_TAG_50
  unsigned char            T_9F11_CodeTable;                  ///< Application issuer code table index (tag 9F11) @n If provided bit #DF61_SEL_9F11_CODE_TABLE has to be set  @n TLV tag #TAG_9F11_ISS_CODE_TABLE_ID @n Indicates the code table according to ISO/IEC 8859 for displaying the Application Preferred Name @n Examples: 1 ... Latin-1 (West Europe), 2 ... Latin-2 (East Europe), 6 ... Arabic, 9 ... Latin-5
  unsigned char            T_9F12_PreferredName[16+1];        ///< Application preferred name @n If provided bit #DF61_SEL_9F12_PREF_NAME has to be set  @n TLV tag #TAG_9F12_APP_PREFERRED_NAME
  unsigned char            T_5F2D_Lang_Pref[8+1];             ///< Language Preference (zero terminated) @n If provided bit #DF61_SEL_5F2D_LANG_PREF has to be set @n TLV tag #TAG_5F2D_LANGUAGE
  unsigned char            T_DF62_ErrorData[15];              ///< Optional: filled if transaction is not successful @n see @ref DEF_DF62 @n If provided bit #DF61_SEL_DF62_ERRORDATA has to be set  @n TLV tag #TAG_DF62_ERROR_DATA
  unsigned char            T_DF63_DisplayText;                ///< Optional: filled with index of display text id if anything was displayed @n If provided bit #DF61_SEL_9F63_DISPTXT has to be set  @n TLV tag #TAG_DF63_DISPLAY_TEXT
  unsigned char            countFallbackMS;                   ///< Reduced number of MID applications @n max. #EMV_ADK_MAX_FB_MSR @n If provided bit #DF61_SEL_MID_DATA has to be set  @n TLV tag #TAG_DF4C_FALLB_MSR_APPLI
  EMV_CT_FALLBCK_MSR_TYPE  xFallback_MS[EMV_ADK_MAX_FB_MSR];  ///< Reduced list of MID applications @n max. number #EMV_ADK_MAX_FB_MSR @n If provided bit #DF61_SEL_MID_DATA has to be set  @n TLV tag #TAG_DF4C_FALLB_MSR_APPLI
  unsigned char            PDOL_tags;                         ///< PDOL tag information: (b8 set) == final amount (9F02/81) is required at step application initialization (GPO) @n If provided bit #DF61_SEL_PDOL has to be set @n TLV tag #TAG_DF4E_PDOL_INFO

  EMV_CT_CANDIDATE_TYPE    T_BF04_Candidates[EMV_CT_COMMON_CANDS];    ///< The candidate list which is used in the selection process, see ::EMV_CT_CANDIDATE_STRUCT @n up to 10 entries, 1st entry with zero AID length marks end of the list @n If provided bit #DF61_SEL_CANDIDATES has to be set @n Can also be changed by EMV_CT_updateTxnTags() @n TLV tag #TAG_CAND_LIST
  EMV_CT_APPLI_TYPE        T_DF04_Aidselected;                        ///< Selected AID (::EMV_CT_APPLI_STRUCT) @n If provided bit #DF61_SEL_AID has to be set @n TLV tag #TAG_DF04_AID

  unsigned char            T_DF61_Info_Received_Data[8];  ///< Which data was received from the chip @n see @ref DEF_DF61_SELECT
} EMV_CT_SELECTRES_TYPE;  ///< typedef for EMV_CT_SELECTRES_STRUCT

#define EMV_SELECTRES_PDOL_TAG_TRANSTYPE   0x20 ///< B1b6: Transaction Type is requested in PDOL
#define EMV_SELECTRES_PDOL_TAG_ACCOUNTTYPE 0x40 ///< B1b7: Account Type is requested in PDOL
#define EMV_SELECTRES_PDOL_TAG_AMOUNT      0x80 ///< B1b8: Amount is requested in PDOL

/// @defgroup DEF_DF61_SELECT StartTransaction: Which data was received from the chip
/// @ingroup DEF_FLOW_OUTPUT
/// @brief Contents of EMV_CT_SELECTRES_STRUCT::T_DF61_Info_Received_Data
/// @{
// byte 1 (T_DF61_Info_Received_Data[0])
#define  DF61_SEL_DF62_ERRORDATA       0x01  ///< B1b1: EMV_CT_SELECTRES_STRUCT::T_DF62_ErrorData
#define  DF61_SEL_AGREED_APPNAME       0x02  ///< B1b2: EMV_CT_SELECTRES_STRUCT::AppName
#define  DF61_SEL_50_APPLICATION_NAME  0x04  ///< B1b3: EMV_CT_SELECTRES_STRUCT::T_50_ApplicationName
#define  DF61_SEL_9F11_CODE_TABLE      0x08  ///< B1b4: EMV_CT_SELECTRES_STRUCT::T_9F11_CodeTable
#define  DF61_SEL_5F2D_LANG_PREF       0x10  ///< B1b5: EMV_CT_SELECTRES_STRUCT::T_5F2D_Lang_Pref
#define  DF61_SEL_84_DF_NAME           0x20  ///< B1b6: EMV_CT_SELECTRES_STRUCT::T_84_DFName
#define  DF61_SEL_9F12_PREF_NAME       0x40  ///< B1b7: EMV_CT_SELECTRES_STRUCT::T_9F12_PreferredName
#define  DF61_SEL_9F63_DISPTXT         0x80  ///< B1b8: EMV_CT_SELECTRES_STRUCT::T_DF63_DisplayText
// byte 2 (T_DF61_Info_Received_Data[1])
#define  DF61_SEL_MID_DATA             0x01  ///< B2b1: MID data, EMV_CT_SELECTRES_STRUCT::countFallbackMS, EMV_CT_SELECTRES_STRUCT::xFallback_MS
#define  DF61_SEL_PDOL                 0x02  ///< B2b2: PDOL info, EMV_CT_SELECTRES_STRUCT::PDOL_tags
#define  DF61_SEL_AID                  0x04  ///< B2b3: selected AID, EMV_CT_SELECTRES_STRUCT::T_DF04_Aidselected
#define  DF61_SEL_CANDIDATES           0x08  ///< B2b4: candidate list, EMV_CT_SELECTRES_STRUCT::T_BF04_Candidates
/// @}



// ========================================================================================================
// === TRANSACTION EXECUTION ===
// ========================================================================================================

/// @defgroup TXN_OPTIONS Options for transaction processing
/// @ingroup DEF_FLOW_INPUT
/// @brief Defines for EMV_CT_SELECT_STRUCT::TxnOptions, EMV_CT_TRANSAC_STRUCT::TxnOptions, EMV_CT_HOST_STRUCT::TxnOptions
/// @{
  // byte 1 (TxnOptions[0])
  #define  EMV_CT_SELOP_CBCK_APPLI_SEL           0x01  ///< B1b1: Callback function for customer application selection @ref EMV_CT_CALLBACK_FnT (#TAG_BF04_CBK_REDUCE_CAND) will be generated, if there are several application possible. @n Depends on #MS_RETURN_CALLBACKS, EMV_CT_APPS_SELECT_STRUCT::ucCardholderConfirmation.
  #define  EMV_CT_SELOP_RESERV_ALLOW_B_ON_A      0x02  ///< B1b2: allow reservation type B even in case only type A is activated via configuration (see @ref SPECIAL_TRXS)
  #define  EMV_CT_SELOP_CBCK_DOMESTIC_APPS       0x04  ///< B1b3: Activate callback function for cross check if a domestic app remains in the candidate list or not (@ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)) @n Depends on #MS_RETURN_CALLBACKS
  #define  EMV_CT_SELOP_ADDALL                   0x08  ///< B1b4: Add all PSE applications to the candidate list (Cartes Bancaires - France)
  #define  EMV_CT_SELOP_ADDBLO                   0x10  ///< B1b5: Add blocked LOA applications to the candidate list (Interac - Canada)
  #define  EMV_CT_NO_LONGEST_AID_MATCH           0x20  ///< B1b6: Do not perform the longest AID match (technical nonsense but needed for a few domestic certifications)
  #define  EMV_CT_ALLOW_BLOCKED                  0x40  ///< B1b7: Allow blocked application to be processed (in Bank controlled terminals to enable script processing to unblock blocked cards)
  #define  EMV_CT_SELOP_REMOVEALL_BUT_EXCLUDED   0x80  ///< B1b8: Remove all candidates from terminal list except those which are contained in excluded AID list. This allows transactions restricted to one, two or three AIDs without terminal reconfiguration.
  // byte 2 (TxnOptions[1])
  #define EMV_CT_TRXOP_PIN_BYPASS_NO_SUBSEQUENT  0x01  ///< B2b1: If PIN bypass is supported it usually is for subsequent PIN entries, too. However some ICS (even if minor change) may announce no subsequent PIN Bypass, so you can configure here
  #define EMV_CT_TRXOP_MULTIPLE_RANDOM_NUMBERS   0x02  ///< B2b2: Create a new value for 9F37, each time a random number is requested by the ICC. Per default one random number per transaction is generated. It is recommended to use multiple random numbers for security reasons.
  #define EMV_CT_TRXOP_DCC_CALLBACK              0x04  ///< B2b3: DCC callback function @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC) will be invoked.
                                                       ///< @n Callback is only invoked if DCC is allowed (transaction amount was not yet used in PDOL or amount confirmation) resp. limit conversion is needed (transaction and configuration currency difference).
                                                       ///< @n Depends on #MS_RETURN_CALLBACKS and #DCC_CHECK.
  #define EMV_CT_TRXOP_DCC_CALLBACK_ALWAYS       0x08  ///< B2b4: DCC callback function @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC) will be called always. @n Depends on #MS_RETURN_CALLBACKS and #DCC_CHECK.
  #define EMV_CT_TRXOP_NO_FALLBACK_AFTER_CVM     0x10  ///< B2b5: Despite fallback after CVM allowed by application configuration stop transaction with card error. This is required in case of separate readers if mag. stripe has not been read prior to chip.
  #define EMV_CT_TRXOP_AMOUNT_CONF               0x20  ///< B2b6: The callback for the amount confirmation @ref EMV_CT_CALLBACK_FnT (#TAG_BF02_CBK_AMOUNTCONF) will be generated, e.g. to be deactivated if terminal is unattended. @n Invoking this callback depends on #EMV_CT_CONF_AMOUNT_PIN, #REFUND_CONFIRM_AMOUNT, #EMV_CT_AMOUNT_CONFIRM_ON_SIGNATURE, #MS_RETURN_CALLBACKS.
  #define EMV_CT_TRXOP_ENFORCE_PIN               0x40  ///< B2b7: Austrian (EPA) domestic feature: force PIN usage @n In case no PIN entry was done during Cardholder Verification: EMV ADK will force online PIN entry, that is flag bit #EMV_ADK_SI_ONLINE_PIN_REQUIRED in #EMV_CT_TRANSRES_STRUCT::StatusInfo.
  #define EMV_CT_TRXOP_FALLBACK_GOODSERVICE      0x80  ///< B2b8: German domestic feature: Execute cash transaction with transaction type "Goods & Services" (in case application's background system does not support cash)
  // byte 3 (TxnOptions[2])
  #define EMV_CT_TRXOP_MERCHINFO_CALLBACK        0x01  ///< B3b1: The callback for the intermediate merchant information will be generated (@ref EMV_CT_CALLBACK_FnT #TAG_BF01_CBK_MERCHINFO) @n Depends on #MS_RETURN_CALLBACKS
  #define EMV_CT_TRXOP_RND_CALLBACK              0x02  ///< B3b2: Use own random number generator instead of OS one (not recommended but for debug purposes useful), only for velocity kernel, see #TAG_BF13_CBK_RND
  #define EMV_CT_TRXOP_HOTLST_LOG_CALLBACK       0x04  ///< B3b3: The callback for the transaction log / hotlist will be generated @ref EMV_CT_CALLBACK_FnT (#TAG_BF03_CBK_LOG_HOTLIST) @n Depends on the AID parameters #TRANSACTION_LOG and #BLACKLIST, and on #MS_RETURN_CALLBACKS
  #define EMV_CT_TRXOP_LOCAL_CHCK_CALLBACK       0x08  ///< B3b4: The callback for the domestic and local checks will be generated @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS) @n Depends on the AID parameter #EMV_CT_DOMESTIC_CHECK and on #MS_RETURN_CALLBACKS
  #define EMV_CT_TRXOP_CARDHINFO_CALLBACK        0x10  ///< B3b5: The callback for the intermediate cardholder information will be generated @ref EMV_CT_CALLBACK_FnT (#TAG_BF09_CBK_CARDHOLDERINFO) @n Depends on #MS_RETURN_CALLBACKS
  #define EMV_CT_TRXOP_EARLY_PIN_ENTRY           0x20  ///< B3b6: Early PIN entry. @n PIN entry is done (by application) in parallel to steps "Read application data" and "Offline data authentication". @n Depends on #EMV_CT_TRXOP_DELAYED_EARLY_PIN. @n If set: Application starts PIN entry after GPO, in callback "merchant info - start read record" (@ref EMV_CT_CALLBACK_FnT #TAG_BF01_CBK_MERCHINFO with #eEMVMerReadAppData)
  #define EMV_CT_TRXOP_DELAYED_EARLY_PIN         0x40  ///< B3b7: Delayed early PIN entry. @n PIN entry is done (by application) after callback "local checks" (@ref EMV_CT_CALLBACK_FnT with #TAG_BF06_CBK_LOCAL_CHECKS). @n Precondition: #EMV_CT_TRXOP_EARLY_PIN_ENTRY is active.
  #define EMV_CT_TRXOP_KERNEL_PARSE_UNKNOWN_TLV  0x80  ///< B3b8: Let the kernel parse and store unknown TLV templates so that unknown tags can be retrieved by EMV_CT_fetchTxnTags. Precondition is that those tags are configured in the additional tag store (Additional_Tags_CRD)
  // byte 4 (TxnOptions[3])
  #define EMV_CT_TRXOP_TIP_AMOUNT_ZERO           0x02  ///< B4b2: Card holder confirmed zero amount within tip amount input dialog. In case of TIP setting DF22 position 4 = 3 the transaction shall not be made suitable for a tip follow-up.
  // byte 5 (TxnOptions[4])
  #define ONL_OPTS_LAST_PIN_TRY                  0x01  ///< B5b1 : Host detected "last PIN try" for online PIN.
  #define EMV_CT_TRXOP_FORCE_TACIAC_DEFAULT      0x10  ///< B5b5 : After Host Connection: Force TAC IAC Default processing even the host was reached, e.g. service provider host is available and returns valid AC but the issuer host was not reached for cryptogram verification
  #define EMV_CT_SELOP_WEEK_PRIORITY_APPS        0x20  ///< B5b6 : Ignore priority applications that are not part of the terminal candidate list.
  #define EMV_CT_CUST_APPLI_SELECTION_PERFORMED  0x40  ///< B5b7 : This is a possibility that allows the app to indicate that a customer application selection has been performed. If this is set, the callback / Reentrance for candidate selection will be even called if only one candidate is left. This is to inform the customer that the prior selected application was withdrawn from the candidate list after customer confirmation. In this special case a single candidate must not be selected automatically.
  #define EMV_CT_SELOP_DETECT_EMPTY_LIST         0x80  ///< B5b8 : Without this bit in case of card error and in case of no common candidate @ref EMV_ADK_FALLBACK is returned for backwards compatibility. If enabled, the second case results in return code @ref EMV_ADK_TXN_EMPTY_LIST.
/// @}


/// @defgroup TXN_STEPS Re-entrance options
/// @ingroup DEF_FLOW_INPUT
/// @brief Defines for EMV_CT_SELECT_TYPE::TxnSteps resp. EMV_CT_TRANSAC_TYPE::TxnSteps
///
/// At each of the given substeps the EMV transaction is interrupted and can be re-entered accordingly by calling the same function again.
/// @n This repeats until the end of the transaction, at each re-entry transaction parameters are updated accordingly.
/// @n Before interrupting the flow the framework fetches the latest up to date transaction parameters.
/// @n Byte 3 determines the handling of callbacks.
/// @n Possible returncodes for an interrupted function:
/// - @ref EMV_ADK_APP_REQ_CANDIDATE
/// - @ref EMV_ADK_APP_REQ_READREC
/// - @ref EMV_ADK_APP_REQ_DATAAUTH
/// - @ref EMV_ADK_APP_REQ_ONL_PIN
/// - @ref EMV_ADK_APP_REQ_OFL_PIN
/// - @ref EMV_ADK_APP_REQ_PLAIN_PIN
/// - @ref EMV_ADK_APP_REQ_CVM_END
/// - @ref EMV_ADK_APP_REQ_RISK_MAN
/// - @ref EMV_ADK_APP_REQ_APPS_PREPROC
/// @{
// byte 1
#define  RETURN_CANDIDATE_LIST       0x01  ///< B1b1: return for selecting the candidate on application level
#define  RETURN_AFTER_GPO            0x02  ///< B1b2: return after finishing GPO
#define  RETURN_AFTER_READ_RECORD    0x04  ///< B1b3: return after finishing ReadRecords
#define  RETURN_AFTER_DATA_AUTH      0x08  ///< B1b4: return after finishing Data Authentication
#define  RETURN_FOR_CVM_PROCESS      0x10  ///< B1b5: for each selected CV PIN method the control is given to the application to handle it
#define  RETURN_FOR_CVM_FINISH       0x20  ///< B1b6: CV finished
  // 0x40 leave gap for a return after CVM processing but before risk management
#define  RETURN_AFTER_RISK_MANGEMENT 0x80  ///< B1b8: return after finishing the terminal risk management (before 1st GAC)
// byte 2
#define  RETURN_CAND_LIST_PREPROC    0x01   ///< B2b1: return once the candidate list is known to manipulate the list
#define  RETURN_CAND_LIST_SKIP       0x02   ///< B2b2: Candidate list building by calling application.
// byte 3
#define  MS_RETURN_CALLBACKS            0x80  ///< B3b8: If Set: All callbacks are disabled completely, otherwise they are used as configured in the parameter set
#define  MS_ADD_TO_HOTLIST              0x01  ///< B3b1: If Set: The card of this txn will be added to the hotlist (processed after ReadRecords)
#define  MS_DECLINE_AAC                 0x02  ///< B3b2: If Set: The transaction will be declined with requesting an AAC
#define  MS_ABORT_TXN                   0x04  ///< B3b3: If Set: The transaction will be terminated immediately
#define  MS_PIN_BYPASS                  0x08  ///< B3b4: If Set: Customer or Merchant bypassed PIN entry
#define  MS_CUST_CVM_OK                 0x10  ///< B3b5: If Set: Customer CVM method success
#define  MS_PINPAD_NOTPRESENT           0x20  ///< B3b6: If Set: external PINPad was not detected for PIN processing
#define  MS_PINICC_STOP                 0x40  ///< B3b7: If Set: ICC stopped working during PIN processing
/// @}


/// @defgroup CONFIRM_AMOUNT_WHEN Amount confirmation before or after CVM processing
/// @ingroup DEF_FLOW_INPUT
/// @brief Byte defines for EMV_CT_PAYMENT_STRUCT::uc_AmountConfirmation
///   @n only valid in case no combined amount confirmation configured (see #EMV_CT_CONF_AMOUNT_PIN in @ref APP_FLOW_CAPS)
/// @{
#define  CONFIRM_AMOUNT_BEFORE_CVM   0x00 ///< @b default @n immediately after language selection before CVM
#define  CONFIRM_AMOUNT_AFTER_CVM    0x01 ///< after cardholder verification
/// @}


/// @defgroup DCC_MODE DCC mode
/// @ingroup TLV_CALLBCK
/// @brief Used to carry decision given back by @ref EMV_CT_CALLBACK_FnT (#TAG_BF07_CBK_DCC)
/// @n     TLV tag #TAG_DF7D_CBK_DCC_CHECK
/// @{
#define MODE_DCC_NO_TRX_CONTINUE   0  ///< No DCC: Continue transaction with original amount, floor limit, ...
#define MODE_DCC_YES_TRX_CONTINUE  1  ///< DCC requested: Continue transaction with returned amount, floor limit, ...
#define MODE_DCC_NO_TRX_ABORT      2  ///< Cashier requested abort: Transaction abort, display #EMV_ADK_TXT_PROCESSING_ERROR and #EMV_ADK_TXT_REMOVE_CARD
#define MODE_DCC_YES_TRX_ABORT     3  ///< DCC requested, but transaction must be restarted: Transaction abort, no text display
/// @}


/// @ingroup ADK_TRX_EXEC
/// @brief Structure for call of EMV_CT_ContinueOffline()
typedef struct EMV_CT_TRANSAC_STRUCT // EMV_CT_TRANSAC_TYPE
{
  EMV_CT_PAYMENT_TYPE TXN_Data;                  ///< The transaction data (::EMV_CT_PAYMENT_STRUCT) can be presented at the beginning of the transaction or (if not yet available) later, e.g. after the final select. Anyhow data should be presented as soon as available.

  unsigned char       TxnOptions[5];             ///< options, allowed values see @ref TXN_OPTIONS @n TLV tag #TAG_DF36_TRX_OPTIONS, validity bit #INPUT_OFL_TXN_OPTIONS
  unsigned char       TxnSteps[3];               ///< This parameter allows to interrupt the transaction at given steps for intermediate application processing, allowed values see @ref TXN_STEPS @n TLV tag #TAG_DF37_TRX_STEPS, validity bit #INPUT_OFL_TXN_STEPS

  unsigned char       Info_Included_Data[8];     ///< Which data is included in the message, see @ref DEF_INPUT_TRX
} EMV_CT_TRANSAC_TYPE;   ///< typedef for @ref EMV_CT_TRANSAC_STRUCT


  /// @defgroup DEF_INPUT_TRX Transaction input: Which data is included
  /// @ingroup DEF_FLOW_INPUT
  /// @brief Contents of EMV_CT_TRANSAC_TYPE::Info_Included_Data
/// @{
  /* Byte 1 */
#define  INPUT_OFL_TXN_COUNTER         0x01  ///< B1b1: transaction counter, EMV_CT_PAYMENT_STRUCT::TransCount
#define  INPUT_OFL_ADD_TAGS            0x02  ///< B1b2: @deprecated Don't use EMV_CT_PAYMENT_STRUCT::Additional_Result_Tags anymore. Use EMV_CT_fetchTxnTags() instead.
#define  INPUT_OFL_CB_AMOUNT           0x04  ///< B1b3: cashback amount, EMV_CT_PAYMENT_STRUCT::Cashback_Amount
#define  INPUT_OFL_ACCOUNT_TYPE        0x08  ///< B1b4: account type, EMV_CT_PAYMENT_STRUCT::uc_AccountType
#define  INPUT_OFL_LANGUAGE            0x10  ///< B1b5: language preselected, EMV_CT_PAYMENT_STRUCT::PreSelected_Language
#define  INPUT_OFL_AMOUNT_CONF         0x20  ///< B1b6: amount confirmation, EMV_CT_PAYMENT_STRUCT::uc_AmountConfirmation
#define  INPUT_OFL_TXN_OPTIONS         0x40  ///< B1b7: transaction options, EMV_CT_TRANSAC_STRUCT::TxnOptions
#define  INPUT_OFL_TXN_STEPS           0x80  ///< B1b8: transaction steps if interrupt needed instead of 1 step processing, EMV_CT_TRANSAC_STRUCT::TxnSteps
  /* Byte 2 */
#define  INPUT_OFL_AMOUNT              0x01  ///< B2b1: txn amount, EMV_CT_PAYMENT_STRUCT::Amount
#define  INPUT_OFL_AMOUNT_CURRENCY     0x02  ///< B2b2: currency of txn, EMV_CT_PAYMENT_STRUCT::CurrencyTrans
#define  INPUT_OFL_CUREXPONENT         0x04  ///< B2b3: currency exponent of txn, EMV_CT_PAYMENT_STRUCT::ExpTrans
#define  INPUT_OFL_DATE                0x08  ///< B2b4: date of txn, EMV_CT_PAYMENT_STRUCT::Date
#define  INPUT_OFL_TIME                0x10  ///< B2b5: time of txn, EMV_CT_PAYMENT_STRUCT::Time
#define  INPUT_OFL_FORCE_ONLINE        0x20  ///< B2b6: force it online (suspicious, EMVCo), EMV_CT_PAYMENT_STRUCT::Force_Online
#define  INPUT_OFL_FORCE_ACCEPT        0x40  ///< B2b7: force acceptance, EMV_CT_PAYMENT_STRUCT::Force_Acceptance
#define  INPUT_OFL_ONLINE_SWITCH       0x80  ///< B2b8: force it online (domestic need), EMV_CT_PAYMENT_STRUCT::Online_Switch
/// @}


/// @ingroup ADK_TRX_EXEC
/// @brief Data structure for issuer script results.
/// Used in EMV_CT_TRANSRES_STRUCT::scriptresults.
typedef struct EMV_CT_SRCRIPTRES_STRUCT
{
  unsigned char   countScriptCrit;                                             ///< Number of critical script results @n TLV tag #TAG_DF08_CRIT_SCR
  unsigned char   ScriptCritResult[EMV_ADK_SCRIPT_RESULT_MAX][EMV_ADK_SCRIPT_RESULT_LEN];    ///< Results of critical scripts @n TLV tag #TAG_DF08_CRIT_SCR
  unsigned char   countScriptUnCrit;                                           ///< Number of uncritical script results @n TLV tag #TAG_DF07_UNCRIT_SCR
  unsigned char   ScriptUnCritResult[EMV_ADK_SCRIPT_RESULT_MAX][EMV_ADK_SCRIPT_RESULT_LEN];  ///< Results of uncritical scripts @n TLV tag #TAG_DF07_UNCRIT_SCR
} EMV_CT_SRCRIPTRES_TYPE;


/// @ingroup ADK_TRX_EXEC
/// @brief Data structure for host response data. Input for EMV_CT_ContinueOnline()
typedef struct EMV_CT_HOST_STRUCT
{
  char             OnlineResult;           ///< Shows whether or not an online dialogue was successful.  If there is no connection or the message MAC is wrong or the response contains a format error, then @c FALSE must be entered.  The kernel then performs a TAC-IAC-Default check and the second GenerateAC.  Other data is only then relevant, when the online dialogue was successful (value @c TRUE). @n mandatory @n Validity bit: #INPUT_ONL_ONLINE_RESP @n TLV tag #TAG_DF50_ONL_RES
  unsigned char    AuthResp[2];            ///< Authorisation Response Code. The response code from the host. Note: The format must be converted from numeric to alphanumeric @n mandatory @n Can also be changed by EMV_CT_updateTxnTags() @n Validity bit: #INPUT_ONL_ONLINE_AC @n TLV tag #TAG_8A_AUTH_RESP_CODE
  unsigned char    LenAuth;                ///< Length of @c AuthData @n Validity bit: #INPUT_ONL_AUTHDATA @n TLV length for #TAG_DF52_AUTH_DATA
  unsigned char*   AuthData;               ///< Issuer Authentication Data (EMVCo tag 91) @n "91xx" must be included @n Can also be changed by EMV_CT_updateTxnTags() @n Validity bit: #INPUT_ONL_AUTHDATA @n TLV length for #TAG_DF52_AUTH_DATA
  unsigned short   LenScriptCrit;          ///< Length of @c ScriptCritData @n Validity bit: #INPUT_ONL_SCRIPTCRIT @n TLV length for #TAG_DF53_SCRIPT_CRIT
  unsigned char*   ScriptCritData;         ///< Issuer Script Template 1 (critical scripts to be performed prior to 2nd Generate AC) @n Data must consist of one or several scripts encapsulated in EMVCo tag @c 71 @n Validity bit: #INPUT_ONL_SCRIPTCRIT @n TLV length for #TAG_DF53_SCRIPT_CRIT
  unsigned short   LenScriptUnCrit;        ///< Length of @c ScriptUnCritData @n Validity bit: #INPUT_ONL_SCRIPTUNCRIT @n TLV length for #TAG_DF54_SCRIPT_UNCRIT
  unsigned char*   ScriptUnCritData;       ///< Issuer Script Template 2 (non critical scripts to be performed after 2nd Generate AC) @n Data must consist of one or several scripts encapsulated in EMVCo tag @c 72 @n Validity bit: #INPUT_ONL_SCRIPTUNCRIT @n TLV length for #TAG_DF54_SCRIPT_UNCRIT
  unsigned char    AuthorizationCode[6];   ///< Value given by the card issuer for an accepted transaction. @n mandatory @n Validity bit: #INPUT_ONL_AUTHCODE @n TLV tag #TAG_89_AUTH_CODE
  char             Result_referral;        ///< Merchant response in case of voice referral. @n @c True ... bank approved @n @c False ... bank declined @n Validity bit: #INPUT_ONL_RESULT_REFERRAL @n TLV tag #TAG_DF51_ISS_REF_RES
  char             AuthResp_Referral[2];   ///< host AC for an issuer referral. @n Validity bit: #INPUT_ONL_ARC_REFERRAL @n TLV tag #TAG_DF55_AC_ISS_REF
  char             AuthResp_Wrong_PIN[2];  ///< host AC for a wrong online PIN. @n Validity bit: #INPUT_ONL_ARC_WRONGPIN @n TLV tag #TAG_DF56_AC_WRONG_PIN
  char             AuthResp_Positive[2];   ///< additional host AC which is considered positive (a TC is requested on 2nd GAC), the parameter may vary per transaction (if different schemes use different alternative positive response codes) @n Validity bit: #INPUT_ONL_ARC_POSITIVE @n TLV tag #TAG_DF57_AC_ADD_OK
  unsigned char    PreAuth_Amount[6];      ///< Special feature: Different transaction amount to be used for @c GENAC2 @n 0 = don't change amount, use the amount already given by EMV_CT_StartTransaction() and/or EMV_CT_ContinueOffline() @n Validity bit: #INPUT_ONL_AMOUNT_GAC2 @n TLV tag #TAG_9F02_NUM_AMOUNT_AUTH

  unsigned char    TxnOptions[5];          ///< Bitstring for options. @n See @ref TXN_OPTIONS @n Validity bit: #INPUT_ONL_TXN_OPTIONS @n TLV tag #TAG_DF36_TRX_OPTIONS
  unsigned char    TxnSteps[3];            ///< This parameter allows to interrupt the transaction at given steps for intermediate application processing, allowed values see @ref TXN_STEPS @n Validity bit: #INPUT_ONL_TXN_STEPS @n TLV tag #TAG_DF37_TRX_STEPS

  unsigned char    Info_Included_Data[8];  ///< Which data is included in the message, see @ref DEF_INPUT_ONLINE
} EMV_CT_HOST_TYPE;


  /// @defgroup DEF_INPUT_ONLINE Host data: Which data is included
  /// @ingroup DEF_FLOW_INPUT
  /// @brief Contents of EMV_CT_HOST_TYPE::Info_Included_Data
/// @{
  /* Byte 1, Index 0 */
#define  INPUT_ONL_ONLINE_RESP         0x01  ///< B1b1: EMV_CT_HOST_STRUCT::OnlineResult
#define  INPUT_ONL_ONLINE_AC           0x02  ///< B1b2: EMV_CT_HOST_STRUCT::AuthResp
#define  INPUT_ONL_AUTHDATA            0x04  ///< B1b3: EMV_CT_HOST_STRUCT::AuthData
#define  INPUT_ONL_SCRIPTCRIT          0x08  ///< B1b4: EMV_CT_HOST_STRUCT::ScriptCritData
#define  INPUT_ONL_SCRIPTUNCRIT        0x10  ///< B1b5: EMV_CT_HOST_STRUCT::ScriptUnCritData
#define  INPUT_ONL_AUTHCODE            0x20  ///< B1b6: EMV_CT_HOST_STRUCT::AuthorizationCode
#define  INPUT_ONL_RESULT_REFERRAL     0x40  ///< B1b7: EMV_CT_HOST_STRUCT::Result_referral
#define  INPUT_ONL_ARC_REFERRAL        0x80  ///< B1b8: EMV_CT_HOST_STRUCT::AuthResp_Referral
  /* Byte 2, Index 1 */
#define  INPUT_ONL_ARC_WRONGPIN        0x01  ///< B2b1: EMV_CT_HOST_STRUCT::AuthResp_Wrong_PIN
#define  INPUT_ONL_TXN_OPTIONS         0x02  ///< B2b2: EMV_CT_HOST_STRUCT::TxnOptions
#define  INPUT_ONL_AMOUNT_GAC2         0x04  ///< B2b3: EMV_CT_HOST_STRUCT::PreAuth_Amount
#define  INPUT_ONL_TXN_STEPS           0x08  ///< B2b4: EMV_CT_HOST_STRUCT::TxnSteps
#define  INPUT_ONL_ARC_POSITIVE        0x10  ///< B2b5: EMV_CT_HOST_STRUCT::AuthResp_Positive
/// @}


/// @ingroup ADK_TRX_EXEC
/// @brief Data structure for output data of EMV_CT_ContinueOffline() and EMV_CT_ContinueOnline()
///
typedef struct EMV_CT_TRANSRES_STRUCT // EMV_CT_TRANSRES_TYPE
{
  unsigned long           StatusInfo;                       ///< Common status info (see @ref STATUS_INFO) @n TLV tag #TAG_DF42_STATUS @n Availability bit #TRX_STATUSINFO
  unsigned char           T_9F27_CryptInfo;                 ///< Cryptogram information data (CID) @n For details also see @ref CRYP_EMV_ADK_INF_ADD @n TLV tag #TAG_9F27_CRYPT_INFO_DATA @n Availability bit #TRX_9F27_CRYPTINFO
  unsigned char           T_9F36_ATC[2];                    ///< Application transaction counter (ATC), source: ICC @n TLV tag #TAG_9F36_ATC @n Availability bit #TRX_9F36_ATC
  unsigned char           T_9F26_Cryptogramm[8];            ///< Application cryptogram @n TLV tag #TAG_9F26_APP_CRYPTOGRAM @n Availability bit #TRX_9F26_CRYPTOGRAMM
  unsigned char           T_5A_PAN[10];                     ///< Application Primary Account Number (PAN) @n TLV tag #TAG_5A_APP_PAN @n Availability bit #TRX_5A_PAN
  unsigned char           T_9F39_POSEntryMode;              ///< POS entry mode, see ISO 8583 @n TLV tag #TAG_9F39_POS_ENTRY_MODE @n Availability bit #TRX_9F39_POS_ENTRY_MODE
  unsigned char           T_5F24_AppExpDate[3];             ///< Application Expiration Date @n TLV tag #TAG_5F24_APP_EXP_DATE @n Availability bit #TRX_5F24_APPEXPDATE
  unsigned char           T_9F41_TransCount[4];             ///< Transaction Sequence Counter @n EMV framework just gives back what was configured by @c TransCount in ::EMV_CT_TRANSAC_TYPE
                                                            ///< @n TLV tag #TAG_9F41_TRANS_SEQUENCE_NB @n Availability bit #TRX_9F41_TRANSCOUNT
  unsigned char           T_5F34_PANSequenceNo[1];          ///< PAN sequence number @n TLV tag #TAG_5F34_PAN_SEQUENCE_NB @n Availability bit #TRX_5F34_PAN_SEQ_NUMBER
  EMV_CT_TRACK2_TYPE      T_57_DataTrack2;                  ///< Track 2 Equivalent Data @n TLV tag #TAG_57_TRACK2_EQUIVALENT @n Availability bit #TRX_57_DATA_TRACK2
  EMV_CT_ISSDATA_TYPE     T_9F10_DataIssuer;                ///< Issuer Application Data @n TLV tag #TAG_9F10_ISS_APP_DATA @n Availability bit #TRX_9F10_DATAISSUER
  unsigned char           T_9F37_RandomNumber[4];           ///< Unpredictable Number @n TLV tag #TAG_9F37_UNPREDICTABLE_NB @n Availability bit #TRX_9F37_RANDOM_NUMBER
  unsigned char           T_95_TVR[5];                      ///< Terminal Verification Results @n TLV tag #TAG_95_TVR @n Availability bit #TRX_95_TVR
  unsigned char           T_9A_Date[3];                     ///< Transaction Date @n TLV tag #TAG_9A_TRANS_DATE @n Availability bit #TRX_9A_DATE
  unsigned char           T_9F21_Time[3];                   ///< Transaction Time @n TLV tag #TAG_9F21_TRANS_TIME @n Availability bit #TRX_9F21_TIME
  unsigned char           T_9C_TransType;                   ///< @brief Transaction Type
                                                            ///< @n see @ref TRANS_TYPES
                                                            ///< @n EMVCo value range, used on ICC interface, for cryptogram calculation, and as it has to be sent to the host
                                                            ///< @n May be different to input from EMV_CT_SELECT_STRUCT::TransType as values greater #EMV_ADK_TRAN_TYPE_INTERNAL_LIMIT are mapped to #EMV_ADK_TRAN_TYPE_GOODS_SERVICE
                                                            ///< @n TLV tag #TAG_9C_TRANS_TYPE @n Availability bit #TRX_9C_TRANSTYPE
  unsigned char           T_5F2A_CurrencyTrans[2];          ///< Transaction Currency Code (ISO 4217) @n TLV tag #TAG_5F2A_TRANS_CURRENCY @n Availability bit #TRX_5F2A_TRX_CURRENCY
  unsigned char           T_82_AIP[2];                      ///< Application Interchange Profile @n TLV tag #TAG_82_AIP @n Availability bit #TRX_82_AIP
  unsigned char           T_9F1A_TermCountryCode[2];        ///< Terminal Country Code @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE @n Availability bit #TRX_9F1A_TERM_COUNTRY_CODE
  unsigned char           T_9F34_CVM_Res[3];                ///< CVM Results @n TLV tag #TAG_9F34_CVM_RESULTS @n Availability bit #TRX_9F34_CVM_RES
  unsigned char           T_9F33_TermCap[3];                ///< Terminal Capabilities (as configured by @c TermCap in ::EMV_CT_TERMDATA_STRUCT, but some bits may be deactivated due to configuration of EMV_CT_APPLIDATA_STRUCT::App_TermCap)
                                                            ///< @n TLV tag #TAG_9F33_TRM_CAPABILITIES @n Availability bit #TRX_9F33_TERMCAP
  unsigned char           T_9F35_TermTyp;                   ///< Terminal Type, see @ref TERM_TYPES @n TLV tag #TAG_9F35_TRM_TYPE @n Availability bit #TRX_9F35_TERMTYP
  unsigned char           T_9F1E_IFDSerialNumber[8];        ///< Interface Device (IFD) serial number @n TLV tag #T_9F1E_IFDSerialNumber @n Availability bit #TRX_9F1E_IFDSERIALNUMBER
  EMV_CT_APPLI_TYPE       T_84_DFName;                      ///< Dedicated File (DF) Name (::EMV_CT_APPLI_STRUCT) @n TLV tag #TAG_84_DF_NAME @n Availability bit #TRX_84_DFNAME
  unsigned char           T_9F09_VerNum[2];                 ///< Application Version Number @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB @n Availability bit #TRX_9F09_VERNUM
  EMV_CT_SRCRIPTRES_TYPE  scriptresults;                    ///< Issuer Script Results (@ref EMV_CT_SRCRIPTRES_STRUCT) @n Availability bit #TRX_SCRIPTRESULTS
  unsigned char           T_9B_TSI[2];                      ///< Transaction Status Information @n TLV tag #TAG_9B_TSI @n Availability bit #TRX_9B_TSI
  EMV_CT_APPLI_TYPE       T_9F06_AID;                       ///< Application Identifier (AID) - terminal (::EMV_CT_APPLI_STRUCT) @n TLV tag #TAG_9F06_AID @n Availability bit #TRX_9F06_AID
  unsigned char           Add_TXN_Tags[EMV_ADK_ADD_TAG_SIZE]; ///< @deprecated Use EMV_CT_fetchTxnTags() instead.
  unsigned char           T_9F02_TXNAmount[6];              ///< Transaction Amount @n TLV tag #TAG_9F02_NUM_AMOUNT_AUTH @n Availability bit #TRX_9F02_AMOUNT
  unsigned char           T_9F03_TXNAdditionalAmount[6];    ///< Transaction Additional (Cashback) Amount @n TLV tag #TAG_9F03_NUM_AMOUNT_OTHER @n Availability bit #TRX_9F03_CB_AMOUNT
  unsigned char           T_9F53_MC_CatCode[1];             ///< Mastercard Transaction Category Code @n TLV tag #TAG_9F53_TRANS_CATEGORY_CODE @n Availability bit #TRX_9F53_MC_CATCODE
  unsigned char           AppName[16+1];                    ///< application name, zero terminated (tag 9F12, tag 50, or configured default EMV_CT_APPLIDATA_STRUCT::AppName)
                                                            ///< @n TLV tag #TAG_9F12_APP_PREFERRED_NAME @n Availability bit #TRX_APPNAME
  unsigned char           T_5F25_AppEffDate[3];             ///< Application Effective Date @n TLV tag #TAG_5F25_APP_EFF_DATE @n Availability bit #TRX_5F25_APPEFFDATE
  unsigned char           T_5F28_IssCountryCode[2];         ///< Issuer Country Code @n TLV tag #TAG_5F28_ISS_COUNTRY_CODE @n Availability bit #TRX_5F28_ISSCOUNTRYCODE
  unsigned char           T_9F45_DataAuthCode[2];           ///< Data Authentication Code, optional: present if SDA card @n TLV tag #TAG_9F45_DATA_AUTHENT_CODE @n Availability bit #TRX_9F45_DATAAUTHCODE
  EMV_CT_ICCRND_TYPE      T_9F4C_ICCDynNumber;              ///< optional: present if DDA/CDA card @n TLV tag #TAG_9F4C_ICC_DYNAMIC_NB @n Availability bit #TRX_9F4C_ICCDYNNUMBER
  unsigned char           TACDenial[5];                     ///< Terminal Action Code - Denial @n TLV tag #TAG_DF21_TAC_DENIAL @n Availability bit #TRX_TAC_DENIAL
  unsigned char           TACOnline[5];                     ///< Terminal Action Code - Online @n TLV tag #TAG_DF22_TAC_ONLINE @n Availability bit #TRX_TAC_ONLINE
  unsigned char           TACDefault[5];                    ///< Terminal Action Code - Default @n TLV tag #TAG_DF23_TAC_DEFAULT @n Availability bit #TRX_TAC_DEFAULT
  unsigned char           T_9F0E_IACDenial[5];              ///< Issuer Action Code - Denial @n TLV tag #TAG_9F0E_IAC_DENIAL @n Availability bit #TRX_9F0E_IAC_DENIAL
  unsigned char           T_9F0F_IACOnline[5];              ///< Issuer Action Code - Online @n TLV tag #TAG_9F0F_IAC_ONLINE @n Availability bit #TRX_9F0F_IAC_ONLINE
  unsigned char           T_9F0D_IACDefault[5];             ///< Issuer Action Code - Default @n TLV tag #TAG_9F0D_IAC_DEFAULT @n Availability bit #TRX_9F0D_IAC_DEFAULT
  unsigned char           T_9F40_AddTermCap[5];             ///< Additional Terminal Capabilities (as configured by @c TermAddCap in ::EMV_CT_TERMDATA_STRUCT, but some bits may be deactivated due to
                                                            ///< configuration of EMV_CT_APPLIDATA_STRUCT::App_TermAddCap) @n TLV tag #TAG_9F40_ADD_TRM_CAP @n Availability bit #TRX_9F40_TERMCAP
  unsigned char           T_DF62_ErrorData[15];             ///< Additional error data (German requirement),  optional: filled if transaction is not sucessful, see @ref DEF_DF62 @n TLV tag #TAG_DF62_ERROR_DATA @n Availability bit #TRX_DF62_ERRORDATA_TRANS
  unsigned char           T_9F16_MerchIdent[15];            ///< Merchant Identifier @n TLV tag #TAG_9F16_MERCHANT_ID @n Availability bit #TRX_9F16_MERCHANT_ID
  unsigned char           T_DF63_DisplayText;               ///< filled with index of display text id if anything should be displayed @n TLV tag #TAG_DF63_DISPLAY_TEXT @n Availability bit #TRX_DF63_DISPLAY_ID
  EMV_CT_CRDNAME_TYPE     T_5F20_Cardholder;                ///< Cardholder Name @n TLV tag #TAG_5F20_CARDHOLDER_NAME @n Availability bit #TRX_5F20_CARDHOLDER
  unsigned char           T_5F2D_Lang_Pref[8+1];            ///< Language Preference (zero terminated) @n TLV tag #TAG_5F2D_LANGUAGE @n Availability bit #TRX_5F2D_LANG_PREFERENCE
  unsigned char           T_9F08_ICC_Appli_Vers_No[2];      ///< ICC Application Version Number @n TLV tag #TAG_9F08_ICC_APP_VERSION_NB @n Availability bit #TRX_9F08_ICC_APPLI_VERS_NO
  unsigned char           T_5F36_Trx_Currency_Exp;          ///< Transaction Currency Exponent @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP @n Availability bit #TRX_5F36_TRX_CURRENCY_EXPO
  unsigned char           T_DF59_Offl_PIN_errors;           ///< Number of wrong PIN entries (only in case of offline PIN!!!) @n TLV tag #TAG_DF59_OFFL_PIN_ERRORS @n Availability bit #TRX_DF59_OFFLINE_PIN_ERRORS
  unsigned char           T_5F30_ServiceCode[2];            ///< Service code as defined in ISO/IEC 7813 for track 1 and track 2, format: n3 @n TLV tag #TAG_5F30_SERVICE_CODE @n Availability bit #TRX_5F30_SERVICE_CODE
  unsigned char           T_DF17_FallbackMID;               ///< If return code #EMV_ADK_FALLBACK: Fallback MID to be used. Will be filled in case application label was shown to cardholder. First assigned MID of the used AID is filled in.
                                                            ///< @n TLV tag #TAG_DF17_FALLBACK_MIDS @n Availability bit #EMV_CT_TRX_DF17_FALLBACK_MID
  unsigned char           T_8E_CVM_List[EMV_ADK_MAX_CVM_LIST_LEN];  ///< CVM list retrieved from ICC @n Length in bytes: #EMV_ADK_MAX_CVM_LIST_LEN. Max. included CVMs: #EMV_ADK_MAX_CVM.
                                                                    ///< @n TLV tag #TAG_8E_CVM_LIST. @n Availability bit #EMV_CT_TRX_8E_CVM_List
  unsigned char           T_DF64_KernelDebugData[EMV_ADK_DEBUG_DATA_SIZE];  ///< Debug Data of the Contact Optimum L2 kernel and if available of the CTLS Optimum kernels @n TLV tag #TAG_DF64_KERNEL_DEBUG @n Availability bit #TRX_DF64_KERNEL_DEBUG
  unsigned char           T_DF61_Info_Received_Data[8];     ///< Information which of these data was provided by the ICC, see @ref DEF_DF61_TRANSRES
} EMV_CT_TRANSRES_TYPE;


/// @defgroup DEF_DF61_TRANSRES ContinueOffline/ContinueOnline: Information which data was provided by the ICC
/// @ingroup DEF_FLOW_OUTPUT
/// @brief Contents of EMV_CT_TRANSRES_TYPE::T_DF61_Info_Received_Data
/// @{
/* Byte 1 */
#define  TRX_STATUSINFO               0x01 ///< B1b1: EMV_CT_TRANSRES_STRUCT::StatusInfo
#define  TRX_9F27_CRYPTINFO           0x02 ///< B1b2: EMV_CT_TRANSRES_STRUCT::T_9F27_CryptInfo
#define  TRX_9F36_ATC                 0x04 ///< B1b3: EMV_CT_TRANSRES_STRUCT::T_9F36_ATC
#define  TRX_9F26_CRYPTOGRAMM         0x08 ///< B1b4: EMV_CT_TRANSRES_STRUCT::T_9F26_Cryptogramm
#define  TRX_5A_PAN                   0x10 ///< B1b5: EMV_CT_TRANSRES_STRUCT::T_5A_PAN
#define  TRX_9F39_POS_ENTRY_MODE      0x20 ///< B1b6: EMV_CT_TRANSRES_STRUCT::T_9F39_POSEntryMode
#define  TRX_5F24_APPEXPDATE          0x40 ///< B1b7: EMV_CT_TRANSRES_STRUCT::T_5F24_AppExpDate
#define  TRX_9F41_TRANSCOUNT          0x80 ///< B1b8: EMV_CT_TRANSRES_STRUCT::T_9F41_TransCount

/* Byte 2 */
#define  TRX_5F34_PAN_SEQ_NUMBER      0x01 ///< B2b1: EMV_CT_TRANSRES_STRUCT::T_5F34_PANSequenceNo
#define  TRX_57_DATA_TRACK2           0x02 ///< B2b2: EMV_CT_TRANSRES_STRUCT::T_57_DataTrack2
#define  TRX_9F10_DATAISSUER          0x04 ///< B2b3: EMV_CT_TRANSRES_STRUCT::T_9F10_DataIssuer
#define  TRX_9F37_RANDOM_NUMBER       0x08 ///< B2b4: EMV_CT_TRANSRES_STRUCT::T_9F37_RandomNumber
#define  TRX_95_TVR                   0x10 ///< B2b5: EMV_CT_TRANSRES_STRUCT::T_95_TVR
#define  TRX_9A_DATE                  0x20 ///< B2b6: EMV_CT_TRANSRES_STRUCT::T_9A_Date
#define  TRX_9F21_TIME                0x40 ///< B2b7: EMV_CT_TRANSRES_STRUCT::T_9F21_Time
#define  TRX_9C_TRANSTYPE             0x80 ///< B2b8: EMV_CT_TRANSRES_STRUCT::T_9C_TransType

/* Byte 3 */
#define  TRX_5F2A_TRX_CURRENCY        0x01 ///< B3b1: EMV_CT_TRANSRES_STRUCT::T_5F2A_CurrencyTrans
#define  TRX_82_AIP                   0x02 ///< B3b2: EMV_CT_TRANSRES_STRUCT::T_82_AIP
#define  TRX_9F1A_TERM_COUNTRY_CODE   0x04 ///< B3b3: EMV_CT_TRANSRES_STRUCT::T_9F1A_TermCountryCode
#define  TRX_9F34_CVM_RES             0x08 ///< B3b4: EMV_CT_TRANSRES_STRUCT::T_9F34_CVM_Res
#define  TRX_9F33_TERMCAP             0x10 ///< B3b5: EMV_CT_TRANSRES_STRUCT::T_9F33_TermCap
#define  TRX_9F35_TERMTYP             0x20 ///< B3b6: EMV_CT_TRANSRES_STRUCT::T_9F35_TermTyp
#define  TRX_9F1E_IFDSERIALNUMBER     0x40 ///< B3b7: EMV_CT_TRANSRES_STRUCT::T_9F1E_IFDSerialNumber
#define  TRX_84_DFNAME                0x80 ///< B3b8: EMV_CT_TRANSRES_STRUCT::T_84_DFName

/* Byte 4 */
#define  TRX_9F09_VERNUM              0x01 ///< B4b1: EMV_CT_TRANSRES_STRUCT::T_9F09_VerNum
#define  TRX_SCRIPTRESULTS            0x02 ///< B4b2: EMV_CT_TRANSRES_STRUCT::scriptresults
#define  TRX_9B_TSI                   0x04 ///< B4b3: EMV_CT_TRANSRES_STRUCT::T_9B_TSI
#define  TRX_9F06_AID                 0x08 ///< B4b4: EMV_CT_TRANSRES_STRUCT::T_9F06_AID
#define  TRX_ADDITIONAL_TAGS          0x10 ///< B4b5: EMV_CT_TRANSRES_STRUCT::Additional_Tags
#define  TRX_APPNAME                  0x20 ///< B4b6: EMV_CT_TRANSRES_STRUCT::AppName
#define  TRX_5F25_APPEFFDATE          0x40 ///< B4b7: EMV_CT_TRANSRES_STRUCT::T_5F25_AppEffDate
#define  TRX_5F28_ISSCOUNTRYCODE      0x80 ///< B4b8: EMV_CT_TRANSRES_STRUCT::T_5F28_IssCountryCode

/* Byte 5 */
#define  TRX_9F45_DATAAUTHCODE        0x01 ///< B5b1: EMV_CT_TRANSRES_STRUCT::T_9F45_DataAuthCode
#define  TRX_9F4C_ICCDYNNUMBER        0x02 ///< B5b2: EMV_CT_TRANSRES_STRUCT::T_9F4C_ICCDynNumber
#define  TRX_TAC_DENIAL               0x04 ///< B5b3: EMV_CT_TRANSRES_STRUCT::TACDenial
#define  TRX_TAC_ONLINE               0x08 ///< B5b4: EMV_CT_TRANSRES_STRUCT::TACOnline
#define  TRX_TAC_DEFAULT              0x10 ///< B5b5: EMV_CT_TRANSRES_STRUCT::TACDefault
#define  TRX_9F0E_IAC_DENIAL          0x20 ///< B5b6: EMV_CT_TRANSRES_STRUCT::T_9F0E_IACDenial
#define  TRX_9F0F_IAC_ONLINE          0x40 ///< B5b7: EMV_CT_TRANSRES_STRUCT::T_9F0F_IACOnline
#define  TRX_9F0D_IAC_DEFAULT         0x80 ///< B5b8: EMV_CT_TRANSRES_STRUCT::T_9F0D_IACDefault

/* Byte 6 */
//#define  TRX_9F42_APPCURRENCYCODE     0x01 ///< B6b1: 2013-11-20 removed former T_9F42_AppCurrencyCode (only ::EMV_CT_CARDDATA_TYPE)
#define  TRX_9F40_TERMCAP             0x02 ///< B6b2: EMV_CT_TRANSRES_STRUCT::T_9F40_AddTermCap
#define  TRX_DF62_ERRORDATA_TRANS     0x04 ///< B6b3: EMV_CT_TRANSRES_STRUCT::T_DF62_ErrorData
#define  TRX_9F16_MERCHANT_ID         0x08 ///< B6b4: EMV_CT_TRANSRES_STRUCT::T_9F16_MerchIdent
#define  TRX_5F20_CARDHOLDER          0x10 ///< B6b5: EMV_CT_TRANSRES_STRUCT::T_5F20_Cardholder
#define  TRX_5F2D_LANG_PREFERENCE     0x20 ///< B6b6: EMV_CT_TRANSRES_STRUCT::T_5F2D_Lang_Pref
#define  TRX_9F08_ICC_APPLI_VERS_NO   0x40 ///< B6b7: EMV_CT_TRANSRES_STRUCT::T_9F08_ICC_Appli_Vers_No
#define  TRX_5F36_TRX_CURRENCY_EXPO   0x80 ///< B6b8: EMV_CT_TRANSRES_STRUCT::T_5F36_Trx_Currency_Exp

/* Byte 7 */
#define  TRX_5F30_SERVICE_CODE        0x01 ///< B7b1: EMV_CT_TRANSRES_STRUCT::T_5F30_ServiceCode
#define  EMV_CT_TRX_DF17_FALLBACK_MID 0x02 ///< B7b2: EMV_CT_TRANSRES_STRUCT::T_DF17_FallbackMID
#define  EMV_CT_TRX_8E_CVM_List       0x04 ///< B7b3: EMV_CT_TRANSRES_STRUCT::T_8E_CVM_List
#define  TRX_DF63_DISPLAY_ID          0x08 ///< B7b4: EMV_CT_TRANSRES_STRUCT::T_DF63_DisplayText
#define  TRX_DF64_KERNEL_DEBUG        0x10 ///< B7b5: EMV_CT_TRANSRES_STRUCT::T_DF64_KernelDebugData
#define  TRX_9F02_AMOUNT              0x20 ///< B7b6: EMV_CT_TRANSRES_STRUCT::T_9F02_TXNAmount
#define  TRX_9F03_CB_AMOUNT           0x40 ///< B7b7: EMV_CT_TRANSRES_STRUCT::T_9F03_TXNAdditionalAmount
#define  TRX_9F53_MC_CATCODE          0x80 ///< B7b8: EMV_CT_TRANSRES_STRUCT::T_9F53_MC_CatCode

/* Byte 8 */
#define  TRX_DF59_OFFLINE_PIN_ERRORS  0x01 ///< B8b1: EMV_CT_TRANSRES_STRUCT::T_DF59_Offl_PIN_errors

/// @}



// ========================================================================================================
// === MISCELLANEOUS ===
// ========================================================================================================


// ========================================================================================================
// === CALLBACK defines ===
// ========================================================================================================

/// @defgroup MERCH_INFO Defines for callback function "put merchant information"
/// @ingroup TLV_CALLBCK
/// @brief In order to show the merchant additional information on a system with two displays, 'information flashes' can be sent to the terminal during payment processing.
/// This allows the merchant to alert the customer to confirming the amount or entering the PIN as required.
///   @n see @ref EMV_CT_CALLBACK_FnT (#TAG_BF01_CBK_MERCHINFO)
/// @{
typedef unsigned char EMV_ADK_MerchantInfo;
#define  eEMVMerCustSelApp     0                   ///< Customer has to select Application, not used anymore
#define  eEMVMerConfAmount     1                   ///< Customer has to confirm amount
#define  eEMVMerCrdhldVerif    2                   ///< EMV step "Cardholder Verification" will be started
#define  eEMVMerReadAppData    3                   ///< EMV step "Read Application Data" will be started
#define  eEMVMerOffAuth        4                   ///< EMV step "Offline Data Authentication" will be started
#define  eEMVMer1stGAC         5                   ///< 1st GENERATE AC will be done
#define  eEMVMer2ndGAC         6                   ///< 2nd GENERATE AC will be done
#define  eEMVMerSelect         7                   ///< "Application Selection" is finished
#define  eEMVMerPINProgress    8                   ///< Input of PIN in progress, not used anymore
#define  eEMVMerCustConf       9                   ///< Wait for customer confirmation, not used anymore
#define  eEMVMerPINInput      10                   ///< Customer is asked for PIN entry
#define  eEMVMerLangSelect    11                   ///< Customer has to select language

#define  eEMVMerCustLang       0x80 ///< customer language selected = 0x80 + language ID (e.g. EMV_LANG_ITALIAN)
/// @}

/// @defgroup CARDHOLDER_INFO Defines for callback function "cardholder information"
/// @ingroup TLV_CALLBCK
/// @brief In order to show the cardholder additional information, e.g. wrong PIN, last PIN try, another app is selected, ...
///   @n see @ref EMV_CT_CALLBACK_FnT (#TAG_BF09_CBK_CARDHOLDERINFO)
/// @{
typedef unsigned char eCardholderInfo;
#define  eEMVCrdWrongPIN       0                   ///< Customer info: wrong PIN
#define  eEMVCrdCorrectPIN     1                   ///< Customer info: correct PIN
#define  eEMVCrdAppChange      2                   ///< Customer info: explicit selected application is changed
#define  eEMVCrdLastTryPIN     3                   ///< Customer info: last PIN try
/// @}


/// @defgroup DEF_CBK_SEL Types of external application selection
/// @ingroup TLV_CALLBCK
/// @brief Used as input for callback function "Application Selection" --> ReduceCandidateList() (@c ucSelector).
/// @{
#define CBK_SEL_MERCHANT 0x00 ///< Merchant has to select
#define CBK_SEL_CUSTOMER 0x01 ///< Customer has to select
/// @}


/// @defgroup DEF_CBK_DOMAPP Modes of domestic application handling
/// @ingroup TLV_CALLBCK
/// @brief Used as return for callback function Check_Dom_Appli()
/// @{
#define CBK_DOMAPP_REMAIN   0  ///< Don't remove from candidate list.
#define CBK_DOMAPP_REMOVE   1  ///< Remove from candidate list.
#define CBK_DOMAPP_REMAIN2  2  ///< Don't remove from candidate list. Additionally suppress final SELECT. Needed e.g. for German ec card. Transaction processing is done outside EMV kernel.
#define CBK_DOMAPP_REMAIN3  3  ///< Don't remove from candidate list. Additionally suppress all following SELECTs. Used for enhancing performance in case it's clear that transaction will be done outside EMV kernel.
/// @}


/// @defgroup CBCK_DOM_OPTION Domestic callback options
/// @ingroup TLV_CALLBCK
/// @brief Calling application may set some options for further transaction processing.
///
/// Defines for @c pucDomOption in @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS)
/// @{
#define DOM_OPTION_LEN             3     ///< Length of @c pucDomOption in bytes
#define DOM_OPTION_AUSTRIAN_NGV    0x01  ///< B1b1: Continue transaction as Austrian Non Guaranteed Payment.
#define DOM_OPTION_TRX_INTERCEPT   0x02  ///< B1b2: Intercept transaction. Context will remain. Repetition trx must be started with #EMV_ADK_TRAN_TYPE_REPEAT_TRX_INTERCEPT.
#define DOM_OPTION_ABORT_NO_TEXT   0x04  ///< B1b3: if @ref EMV_CT_CALLBACK_FnT (#TAG_BF05_CBK_DOM_APPS) returns @c FALSE and @c uc_manipulation != #EMV_ADK_AAC: Don't display texts #EMV_ADK_TXT_PROCESSING_ERROR and #EMV_ADK_TXT_REMOVE_CARD @n Otherwise not effective
#define DOM_OPTION_TIP_OFF         0x08  ///< B1b4: convert a tip capable transaction back to sale transaction (used for countries where tip with signature is done outside the scope of EMV and returns the txn to a normal sale transaction)
/// @}


/// @defgroup CBCK_DOM_INFO Domestic callback information
/// @ingroup TLV_CALLBCK
/// @brief Information flags for calling application (@c pucDomInfo in DomesticLocalChecks()).
/// @{
#define DOM_INFO_LEN               3    ///< Length of @c pucDomInfo in bytes
#define DOM_INFO_ICC_REQ_AMOUNT    0x01 ///< ICC already requested transaction amount (e.g. in PDOL). Amount change for running transaction is forbidden.
#define DOM_INFO_ICC_REQ_TRANSTYPE 0x02 ///< ICC already requested transaction type (e.g. in PDOL). Transaction type change for running transaction is forbidden.
/// @}

/// @defgroup CBCK_DCC_INFO DCC callback information
/// @ingroup TLV_CALLBCK
/// @brief Information flags for calling application
///
/// Defines for @c pucDccInfo in Dynamic Currency Change handling
/// @{
#define DCC_CBCK_INFO_LEN             3  ///< Length of @c pucDomOption in bytes
#define DCC_CBCK_INFO_PDOL_FORBIT  0x01  ///< B1b1: Currency change not allowed for this transaction. Reason is PDOL containing amount or currency.
/// @}


#define MAX_CONF_KEYS  60 //6*10 RIDs (up to 10 issuers ... even more cause no one uses 6 keys in parallel)

/// @ingroup DEF_CARD_CONF
/// @brief Data structure CAP keys
typedef struct EMV_CT_CAPKEY_STRUCT
{
  unsigned char *RID;          ///< Registered Identifier @n TLV tag #TAG_KEY_RID, @n RID in EMV_CT_CAPKEY_STRUCT::RID, @n XML Tag: #XML_TAG_CAP_KEYS_RID
  unsigned char Index;         ///< Index @n TLV tag #TAG_KEY_INDEX, @n Index in EMV_CT_CAPKEY_STRUCT::Index, @n XML Tag: #XML_TAG_CAP_KEYS_INDEX
  unsigned char *Key;          ///< Actual Key data @n TLV tag #TAG_KEY_KEY, @n Key in EMV_CT_CAPKEY_STRUCT::Key, @n XML Tag: #XML_TAG_CAP_KEYS_KEY
  unsigned char KeyLen;        ///< length of key in bytes @n KeyLen in EMV_CT_CAPKEY_STRUCT::KeyLen, @n XML Tag: #XML_TAG_CAP_KEYS_KEYLEN
  unsigned char Exponent;      ///< Exponent - 01 used for 65537 @n TLV tag #TAG_KEY_EXPONENT, @n Exponent in EMV_CT_CAPKEY_STRUCT::Exponent, @n XML Tag: #XML_TAG_CAP_KEYS_EXPONENT
  unsigned char *Hash;         ///< Hash value of rid, index, key, exponent @n Optional, if not provided it's simply not checked. @n TLV tag #TAG_KEY_HASH, @n Hash in EMV_CT_CAPKEY_STRUCT::Hash, @n XML Tag: #XML_TAG_CAP_KEYS_HASH
  unsigned char noOfRevocEntries; ///< No of entries in revocation list
  unsigned char* RevocEntries;    ///< Revocation list, each entry consists of 3 byte serial no. @n TLV tag #TAG_KEY_CRL, @n RevocEntries in EMV_CT_CAPKEY_STRUCT::RevocEntries, @n XML Tag: #XML_TAG_CAP_KEYS_REVOC_LIST
} EMV_CT_CAPKEY_TYPE;


/// @ingroup DEF_CARD_CONF
/// @brief Data structure CAP keys
typedef struct EMV_CT_CAPREAD_STRUCT
{
  unsigned char RID[5];        ///< Registered Identifier, TLV tag #TAG_KEY_RID
  unsigned char Index;         ///< Index, TLV tag #TAG_KEY_INDEX
  unsigned char KeyLen;        ///< Length of key in bytes, TLV tag #TAG_KEY_KEYLEN
} EMV_CT_CAPREAD_TYPE;


// ========================================================================================================
// === CALLBACK FUNCTION for additional features and apps interaction during the transaction            ===
// === (PIN, Application Selection, DCC, ...)                                                           ===
// ========================================================================================================

/// @defgroup CBCK_FCT_TAGS Constructed tags for callback functions
/// @ingroup TLV_CALLBCK
/// @brief Used in @ref EMV_CT_CALLBACK_FnT
///
/// The tag determines which callback function is to be used.
/// Data is enclosed inside this contructed tag.
/// Detailed Description uses following convention:
/// @n <b> INPUT: </b> This is what Application gets from the Framework
/// @n <b> OUTPUT: </b> This is what Application is supposed to send back
/// @{
#define TAG_BF01_CBK_MERCHINFO      0xBF01  ///< @brief Information for the merchant
                                            ///< @n For valid values see @ref MERCH_INFO
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_TRXOP_MERCHINFO_CALLBACK.
                                            ///< @n <b> INPUT: </b>  @n Transaction Info for merchant (#TAG_DF70_CBK_MERCHINFO)
                                            ///< @n <b> OUTPUT: </b> --
#define TAG_BF02_CBK_AMOUNTCONF     0xBF02  ///< @brief Amount confirmation
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_TRXOP_AMOUNT_CONF
                                            ///< @n <b> INPUT: </b>  @n Language (#TAG_DF30_LANG)
                                            ///< @n Text ID (#TAG_DF31_TEXT_NUM)
                                            ///< @n Amount (#TAG_9F02_NUM_AMOUNT_AUTH)
                                            ///< @n Cashback Amount (#TAG_9F03_NUM_AMOUNT_OTHER)
                                            ///< @n Transaction Type (#TAG_9C_TRANS_TYPE)
                                            ///< @n <b> OUTPUT: </b> @n confirmed yes/no (#TAG_DF71_CBK_AMOUNTCONF)
#define TAG_BF03_CBK_LOG_HOTLIST    0xBF03  ///< @brief Check transaction log and hotlist
                                            ///< Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_TRXOP_HOTLST_LOG_CALLBACK
                                            ///< @n <b> INPUT: </b> @n PAN (#TAG_5A_APP_PAN)
                                            ///< @n PAN Sequence (#TAG_5F34_PAN_SEQUENCE_NB)
                                            ///< @n Expiry Date (#TAG_5F24_APP_EXP_DATE)
                                            ///< @n <b> OUTPUT: </b> @n In hotlist yes/no (#TAG_DF72_CBK_BLACKLIST)
                                            ///< @n Stored amount (#TAG_DF73_CBK_TRANSLOG)
#define TAG_BF04_CBK_REDUCE_CAND    0xBF04  ///< @brief Application selection
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_SELOP_CBCK_APPLI_SEL
                                            ///< @n <b> INPUT: </b> @n Application label (#TAG_50_APP_LABEL)
                                            ///< @n AID (#TAG_DF04_AID)
                                            ///< @n Selector (merchant or customer) (#TAG_DF74_CBK_SELECTOR)
                                            ///< @n <b> OUTPUT: </b> @n Select App Index (#TAG_DF75_CBK_APP_NO)
#define TAG_BF05_CBK_DOM_APPS       0xBF05  ///< @brief Information about domestic apps found on the card
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_SELOP_CBCK_DOMESTIC_APPS
                                            ///< @n <b> INPUT: </b> @n AID (#TAG_DF04_AID)
                                            ///< @n OUTPUT: @n Domestic app in/out (#TAG_DF7E_CBK_DOM_APP_RES)
                                            ///< @n Please note that term "Domestic" here has nothing to do with Issuer's and Terminal's country code equality. Term "Domestic" here stands for "Non EMV application". Thus, this callback functionality is tightly related to type @ref EMV_CT_DOM_CHIP_TYPE, used for Non-EMV domestic application registering.
#define TAG_BF06_CBK_LOCAL_CHECKS   0xBF06  ///< @brief Callback for local checks after reading the PAN
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_TRXOP_LOCAL_CHCK_CALLBACK
                                            ///< @n <b> INPUT: </b>
                                            ///< @n Currency (#TAG_9F42_APP_CURRENCY_CODE)
                                            ///< @n PAN (#TAG_5A_APP_PAN)
                                            ///< @n Amount (#TAG_9F02_NUM_AMOUNT_AUTH)
                                            ///< @n Floorlimit (#TAG_9F1B_TRM_FLOOR_LIMIT)
                                            ///< @n CountryCode (#TAG_5F28_ISS_COUNTRY_CODE)
                                            ///< @n Expiry Date (#TAG_5F24_APP_EXP_DATE)
                                            ///< @n Info if DCC is no more possible for this transaction, that's the case if currency or amount was already sent to ICC in PDOL (#TAG_DF5B_DCC_PROHIBIT)
                                            ///< @n Special information (#TAG_DF5C_DOM_INFO)
                                            ///< @n Terminal action codes (#TAG_DF21_TAC_DENIAL, #TAG_DF22_TAC_ONLINE, #TAG_DF23_TAC_DEFAULT)
                                            ///< @n Track2 equivalent data (#TAG_57_TRACK2_EQUIVALENT)
                                            ///< @n Application label (#TAG_50_APP_LABEL)
                                            ///< @n CVM list (#TAG_8E_CVM_LIST)
                                            ///< @n <b> OUTPUT: </b>
                                            ///< @n Amount (#TAG_9F02_NUM_AMOUNT_AUTH)
                                            ///< @n Floorlimit (#TAG_9F1B_TRM_FLOOR_LIMIT)
                                            ///< @n Modify action analysis (#TAG_DF76_CBK_MANIPUL_TRX)
                                            ///< @n Option for transaction proceeding (#TAG_DF5D_DOM_OPTION)
                                            ///< @n Terminal action codes (#TAG_DF21_TAC_DENIAL, #TAG_DF22_TAC_ONLINE, #TAG_DF23_TAC_DEFAULT)
                                            ///< @n Application label (#TAG_50_APP_LABEL)
#define TAG_BF07_CBK_DCC            0xBF07  ///< @brief Dynamic currency conversion (DCC)
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS, #DCC_CHECK and #EMV_CT_TRXOP_DCC_CALLBACK
                                            ///< @n <b> INPUT: </b> @n PAN (#TAG_5A_APP_PAN)
                                            ///< @n Amount (#TAG_9F02_NUM_AMOUNT_AUTH)
                                            ///< @n Application Currency (#TAG_9F42_APP_CURRENCY_CODE)
                                            ///< @n Floorlimit (#TAG_9F1B_TRM_FLOOR_LIMIT)
                                            ///< @n CountryCode (#TAG_5F28_ISS_COUNTRY_CODE)
                                            ///< @n DCC Mode (#TAG_DF7D_CBK_DCC_CHECK)
                                            ///< @n TXNCurrency (#TAG_5F2A_TRANS_CURRENCY)
                                            ///< @n TXNCurrencyExp (#TAG_5F36_TRANS_CURRENCY_EXP)
                                            ///< @n Threshhold (#TAG_DF24_THRESHHOLD)
                                            ///< @n DCCInfo (#TAG_DF39_DCC_CBCK_INFO)
                                            ///< @n <b> OUTPUT: </b> @n Amount (#TAG_9F02_NUM_AMOUNT_AUTH)
                                            ///< @n Floorlimit (#TAG_9F1B_TRM_FLOOR_LIMIT)
                                            ///< @n CountryCode (#TAG_5F28_ISS_COUNTRY_CODE)
                                            ///< @n DCC Mode (#TAG_DF7D_CBK_DCC_CHECK)
                                            ///< @n TXNCurrency (#TAG_5F2A_TRANS_CURRENCY)
                                            ///< @n TXNCurrencyExp (#TAG_5F36_TRANS_CURRENCY_EXP)
                                            ///< @n Threshhold (#TAG_DF24_THRESHHOLD)
#define TAG_BF08_CBK_PIN            0xBF08  ///< @brief PIN Input
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS
                                            ///< @n <b> INPUT: </b> @n PIN Type (#TAG_DF79_CBK_PIN_INFO)
                                            ///< @n Bypass Info (#TAG_DF41_PIN_BYPASS)
                                            ///< @n Random number (#TAG_9F37_UNPREDICTABLE_NB)
                                            ///< @n Public Key Modulus (#TAG_DF7A_CBK_PIN_KEY_DATA)
                                            ///< @n Public key Exponent (#TAG_DF7B_CBK_PIN_KEY_EXP)
                                            ///< @n If #EMV_CT_CONF_AMOUNT_PIN: #TAG_9F02_NUM_AMOUNT_AUTH and #TAG_9F03_NUM_AMOUNT_OTHER
                                            ///< @n <b> OUTPUT: </b> @n PIN input result (#TAG_DF79_CBK_PIN_INFO)
#define TAG_BF09_CBK_CARDHOLDERINFO 0xBF09  ///< @brief A text information has to be displayed to the cardholder.
                                            ///< @n Called depending on #MS_RETURN_CALLBACKS and #EMV_CT_TRXOP_CARDHINFO_CALLBACK
                                            ///< @n Possible values: see @ref CARDHOLDER_INFO
                                            ///< @n <b> INPUT: </b> @n Transaction Info for cardholder (#TAG_DF6E_CBK_CARDHOLDERINFO)
                                            ///< @n <b> OUTPUT: </b> --
#define TAG_BF7F_CBK_TRACE          0xBF7F  ///< @brief Callback for Traces
                                            ///< @n Called depending on #EMV_CT_INIT_OPT_TRACE
                                            ///< @n <b> INPUT: </b> @n ASCII Trace (#TAG_TRACE)
                                            ///< @n <b> OUTPUT: </b> --
#define TAG_BF13_CBK_RND            0xBF13  ///< @brief Random number provided by the calling application
                                            ///< @n Usually the EMV ADK resp. L2 kernel uses the OS functionality for getting a random number.
                                            ///< @n Only for velocity kernel, invoked depending on #EMV_CT_TRXOP_RND_CALLBACK
                                            ///< @n <b> INPUT: </b> @n none
                                            ///< @n <b> OUTPUT: </b> @n random number from app (#TAG_9F37_UNPREDICTABLE_NB)
/// @}



#define FS_CALLBACK_DATA_RECORD 0xFF  // currently max. 201 bytes (5 candidates in cbk_reduceCandidateList())

// ========================================================================================================
// === SERIALIZED FUNCTIONAL CONTACT INTERFACE                                                          ===
// ========================================================================================================


#define EMV_CT_SER_Init_Framework(dataIn, dataInLen, dataOut, dataOutLen) EMV_CT_SER_Init_Framework_Client(EMV_CT_FRAMEWORK_VERSION, (dataIn), (dataInLen), (dataOut), (dataOutLen))

DLL_CTC EMV_ADK_INFO EMV_CT_SER_Init_Framework_Client(const char *version, const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC void EMV_CT_SER_Exit_Framework(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_MapVirtualTerminal(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_StoreCAPKey(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_ReadCAPKeys(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_SetTermData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_GetTermData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_SetAppliData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_GetAppliData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_StartTransaction(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_GetCandidateData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_ContinueOffline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_ContinueOnline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_fetchTxnTags(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_fetchTxnDOL(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_updateTxnTags(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_CheckSupportedAID(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC EMV_ADK_INFO EMV_CT_SER_EndTransaction(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC unsigned char EMV_CT_SER_SmartISO(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC unsigned char EMV_CT_SER_SmartDetect(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC unsigned char EMV_CT_SER_SmartReset(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC unsigned char EMV_CT_SER_SmartPowerOff(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC unsigned char EMV_CT_SER_SmartPIN(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

DLL_CTC unsigned char EMV_CT_SER_Send_PIN_Offline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);


// ========================================================================================================
// === FUNCTIONAL CONTACT INTERFACE                                                                     ===
// ========================================================================================================


/// @defgroup CTINIT_OPTIONS Options at framework initialisation
/// @ingroup FUNC_INIT
/// @brief Flags used in parameter @c options in @ref EMV_CT_Init_Framework
/// @{
#define EMV_CT_INIT_OPT_BASE_INIT       0x00000001   ///< base init: set options and register callback, but do not allocate memory. Useful on devices without contact EMV like UX410.
#define EMV_CT_INIT_OPT_L1_DUMP         0x00000002   ///< allocate additional memory for L1 txn dump, each L1 response is available by tags, using the @ref EMV_CT_fetchTxnTags function
#define EMV_CT_INIT_OPT_CONFIG_MODE     0x00000008   ///< configuration is only written to files upon ExitFramework and ApplyConfiguration instead of written upon each configuration function -> performance improvement
#define EMV_CT_INIT_OPT_DELETE_ALL      0x00000020   ///< delete all existing configuration files using the same virtual terminal
#define EMV_CT_INIT_OPT_TRACE           0x00000100   ///< switch on EMV trace
#define EMV_CT_INIT_OPT_TRACE_CLT       0x00000200   ///< switch on client trace
#define EMV_CT_INIT_OPT_TRACE_SYSLOG    0x00000800   ///< use syslog instead of callback for trace. Not possible for Verix
#define EMV_CT_INIT_OPT_TRACE_ADK_LOG   0x00080000   ///< use ADK-LOG instead of callback for trace
#define EMV_CT_INIT_OPT_TIMINGS         0x00100000   ///< measure card and terminal performance. Note: For this feature other trace output is switched off
#define EMV_CT_INIT_OPT_SHARED_CONFIG   0x00200000   ///< On VOS/VOS2 use /mnt/flash/etc/config/adkemv for persistence instead of $(HOME)/flash. NOTE: Only the processes very first EMV_CT_Init_Framework decides the shared config mode.
#define EMV_CT_INIT_OPT_EPP_MASTER      0x00400000   ///< Generate a Configuration Id for every configuration change at time of persisting.
                                                     ///< Generate a checksum for every persistence file to be aware of changes via file system.
                                                     ///< The configuration id is specific to a Virtual Terminal and returned on Init Framework
                                                     ///< and when the configuration is stored to the file system. It is seen on serial interface only.

#define EMV_CT_INIT_VIRT_1              0x01000000   ///< virtual terminal 1: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_2              0x02000000   ///< virtual terminal 2: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_3              0x03000000   ///< virtual terminal 3: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_4              0x04000000   ///< virtual terminal 4: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_5              0x05000000   ///< virtual terminal 5: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_6              0x06000000   ///< virtual terminal 6: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_7              0x07000000   ///< virtual terminal 7: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_8              0x08000000   ///< virtual terminal 8: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_9              0x09000000   ///< virtual terminal 9: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_10             0x0A000000   ///< virtual terminal 10: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_11             0x0B000000   ///< virtual terminal 11: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_12             0x0C000000   ///< virtual terminal 12: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_13             0x0D000000   ///< virtual terminal 13: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_14             0x0E000000   ///< virtual terminal 14: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_15             0x0F000000   ///< virtual terminal 15: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_16             0x10000000   ///< virtual terminal 16: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_17             0x11000000   ///< virtual terminal 17: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_18             0x12000000   ///< virtual terminal 18: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_19             0x13000000   ///< virtual terminal 19: configuration and transaction not done with the standard terminal but virtual terminal No X
#define EMV_CT_INIT_VIRT_20             0x14000000   ///< virtual terminal 20: configuration and transaction not done with the standard terminal but virtual terminal No X
/// @}


#define EMV_CT_Init_Framework(numberOfAIDs, EMV_Callback, externalData, options) EMV_CT_Init_Framework_Client(EMV_CT_FRAMEWORK_VERSION, (numberOfAIDs), (EMV_Callback), (externalData), (options))
///< @ingroup FUNC_INIT
///< @brief Initialize EMV ADK
///<
///< Explanation in programmers guide: \ref anchor_emv_ct_Initialize_EMV_Framework "Initialize EMV Framework"
///<
///< Transport tags used for serialization: #CLA_EMV #INS_INIT #P2_SET
///<
///< @author GSS R&D Germany
///<
///< @param[in] numberOfAIDs  Number of AIDs
///<                          @n Default: #EMV_ADK_DEFAULT_AIDSUPP, max.: #EMV_ADK_LIMIT_AIDSUPP
///<                          @n TLV tag: #TAG_DF3B_PARAMETER_1
///< @param[in] EMV_Callback  Function pointer for callback (see @ref EMV_CT_CALLBACK_FnT)
///<                          @n Not serialized to TLV interface.
///< @param[in] externalData  Application data pointer, is transmitted unchanged to callback function @ref EMV_CT_CALLBACK_FnT
///<                          @n Not serialized to TLV interface.
///< @param[in] options       Flags for framework initialisation, allowed values see @ref CTINIT_OPTIONS
///<                          @n TLV tag: #TAG_DF3C_PARAMETER_2
///< @return @c #EMV_ADK_OK = okay @n @c #EMV_ADK_ABORT = error
///<

/*****************************************************************************
*  EMV_CT_Init_Framework_Client
*****************************************************************************/
/// @brief Internal function for initialization
/// @n Don't call direct!
/// @n Instead use #EMV_CT_Init_Framework
///
DLL_CTC EMV_ADK_INFO EMV_CT_Init_Framework_Client(const char *version, unsigned char numberOfAIDs, EMV_CT_CALLBACK_FnT EMV_Callback, void* externalData, unsigned long options);


/*****************************************************************************
*  EMV_CT_Exit_Framework
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Kernel deactivation
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Initialize_EMV_Framework "Initialize EMV Framework"
///
/// @author GSS R&D Germany
///
/// @b Serialization:
/// @image html TLV_ExitFramework.jpg
/// Class: #CLA_EMV, Instruction: #INS_INIT, P2: #P2_GET
///
DLL_CTC void EMV_CT_Exit_Framework(void);

/*****************************************************************************
*  EMV_CT_Exit_Framework_extended
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Kernel deactivation
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Initialize_EMV_Framework "Initialize EMV Framework"
///
/// @author GSS R&D Germany
///
/// @param[in]  options  Bitset of @ref EXIT_FW_OPTIONS
///                      @n TLV tag #TAG_DF8F0A_EXIT_OPTIONS
///
/// @b Serialization:
/// @image html TLV_ExitFramework.jpg
/// Class: #CLA_EMV, Instruction: #INS_INIT, P2: #P2_GET
///
DLL_CTC void EMV_CT_Exit_Framework_extended(unsigned char options);

/// @defgroup EXIT_FW_OPTIONS Options for Exit Framework
/// @ingroup FUNC_INIT
/// @brief Meaning of #TAG_DF8F0A_EXIT_OPTIONS, see also EMV_CT_Exit_Framework_extended()
/// @{
#define  EXIT_CT_BASE             0x00  ///< set Framework to base-initialised state.
                                        ///< Free memory
                                        ///< Transactions not possible
#define  EXIT_CT_COMPLETE         0x01  ///< set Framework to uninitialised state.
                                        ///< dlclose all modules
/// @}


/*****************************************************************************
*  EMV_CT_CLIENT_GetVersion
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Get contact client version
///
/// @author GSS R&D Germany
///
/// @return  Client version as zero-terminated C-String
///
DLL_CTC const char* EMV_CT_CLIENT_GetVersion(void);


/*****************************************************************************
*  EMV_CT_FRAMEWORK_GetVersion
*****************************************************************************/
/// @ingroup FUNC_INIT
/// @brief Get contact framework version
///
/// @author GSS R&D Germany
///
/// @return  Framework version as zero-terminated C-String
///          @n TLV tag: #TAG_DF11_LIB_VERSION
///
/// @b Serialization:
/// @image html TLV_GetFrameworkVersion.jpg
/// Class: #CLA_EMV, Instruction: #INS_GET_VER, P2: #P2_GET
///
DLL_CTC const char* EMV_CT_FRAMEWORK_GetVersion(void);


/*****************************************************************************
*  EMV_CT_MapVirtualTerminal
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Changes the Virtual Terminal Map
///
/// Explanation in programmers guide: @ref subsec_emv_ct_virt_term_map
///
/// @author GSS R&D Germany
///
/// @param[in] VirtualTermMapType  Mode, see @ref VIRTUALTERMMAP_MODE
///                                @n TLV tag: #TAG_DF3B_PARAMETER_1
/// @param[in] TLVSwitchValue      TLV buffer containing switch criteria. Must contain tag #TAG_9C_TRANS_TYPE and/or tag #TAG_5F2A_TRANS_CURRENCY, all other tags are currently ignored.
///                                @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in] TLVBufLen           Length of TLV buffer
///                                @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in] VirtualTerminal     Virtual Terminal that is selected if switch criteria match.
///                                @n TLV tag #TAG_DF3D_PARAMETER_3
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM
///
/// @b Serialization:
/// Class: #CLA_EMV, Instruction: #INS_VIRT_CFG
///
DLL_CTC EMV_ADK_INFO EMV_CT_MapVirtualTerminal(EMV_ADK_VIRTUALTERMMAP_TYPE VirtualTermMapType, unsigned char *TLVSwitchValue, unsigned int TLVBufLen, unsigned char VirtualTerminal);


/*****************************************************************************
*  EMV_CT_StoreCAPKey
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Stores the specified CAP key
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Configure_CAP_Keys "Configure CAP Keys"
///
/// @author GSS R&D Germany
///
/// @param[in] eHandleCAPKeyType  Mode of CAP key setting, see @ref APPLI_CONF_MODE (Note: Multiple records are not supported)
///                               @n TLV tag #TAG_HANDLE_APPLI_TYPE
/// @param[in] pxKeyData          All the data of the key that needs storing, see @ref EMV_CT_CAPKEY_STRUCT
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM
///
/// @b Serialization:
/// @image html TLV_StoreCAPKey.jpg
/// Class: #CLA_EMV, Instruction: #INS_CAPKEY_CFG, P2: #P2_SET
///
DLL_CTC EMV_ADK_INFO EMV_CT_StoreCAPKey(EMV_ADK_HANDLE_RECORD_TYPE eHandleCAPKeyType, const EMV_CT_CAPKEY_TYPE *pxKeyData);


/*****************************************************************************
*  EMV_CT_ReadCAPKeys
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Reads CAP keys
///
/// @author GSS R&D Germany
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Configure_CAP_Keys "Configure CAP Keys"
///
/// @param[out] pxKeyData        CAP key data, max(*pucMaxnum, number of stored CAP keys) are written
///                              @n see #EMV_CT_CAPREAD_STRUCT
/// @param[in,out] pucMaxnum     Input: maximum number of CAP keys to write to pxKeyData; output: number of CAP keys stored in file
///                              @n TLV tag #TAG_KEY_NUMBER
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM
///
/// @note Currently maximum number of CAP keys to be stored is 100.
/// @b Serialization:
/// @image html TLV_ReadCAPKey.jpg
/// Class: #CLA_EMV, Instruction: #INS_CAPKEY_CFG, P2: #P2_GET
///
DLL_CTC EMV_ADK_INFO EMV_CT_ReadCAPKeys(EMV_CT_CAPREAD_TYPE *pxKeyData, unsigned char *pucMaxnum);


/*****************************************************************************
*  EMV_CT_SetTermData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Set terminal specific data
///        and perform verification and saving of data
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Configure_Terminal_Data "Configure Terminal Data"
///
/// @author GSS R&D Germany
///
/// @param[in] pxTermData Terminal data (@ref EMV_CT_TERMDATA_STRUCT)
/// @return    #EMV_ADK_OK, #EMV_ADK_SAVE_ERROR, #EMV_ADK_INTERNAL, #EMV_ADK_PARAM
///
/// @b Serialization:
/// @image html TLV_SetTerminalData.jpg
/// Class: #CLA_EMV, Instruction: #INS_TERM_CFG, P2: #P2_SET
///
DLL_CTC EMV_ADK_INFO EMV_CT_SetTermData(EMV_CT_TERMDATA_TYPE* pxTermData);

/*****************************************************************************
*  EMV_CT_GetTermData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Get configured terminal data
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Configure_Terminal_Data "Configure Terminal Data"
///
/// @author GSS R&D Germany
///
/// @param[out] pxTermData Terminal data (@ref EMV_CT_TERMDATA_STRUCT)
/// @return     #EMV_ADK_OK, #EMV_ADK_INTERNAL, #EMV_ADK_READ_ERROR, #EMV_ADK_PARAM
///
/// @b Serialization:
/// @image html TLV_GetTerminalData.jpg
/// Class: #CLA_EMV, Instruction: #INS_TERM_CFG, P2: #P2_GET
///
DLL_CTC EMV_ADK_INFO EMV_CT_GetTermData(EMV_CT_TERMDATA_TYPE* pxTermData);

/*****************************************************************************
*  EMV_CT_SetAppliData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Set application specific data
///        and perform verification and saving of data.
///    @n  Has to be called once per application (Visa, MasterCard, ...).
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Configure_Application_Data "Configure Application Data"
///
/// @author GSS R&D Germany
///
/// @param[in] eHandleAppliType  See @ref APPLI_CONF_MODE
///                              @n TLV tag #TAG_HANDLE_APPLI_TYPE
/// @param[in] pxAID             AID (up to n-times for AIDs with same configuration), see @ref EMV_CT_APPLI_STRUCT
///                              @n TLV tag #TAG_4F_APP_ID
/// @param[in] pxAppliData       Application data, see @ref EMV_CT_APPLIDATA_STRUCT
/// @return #EMV_ADK_OK in case of success@n
///         #EMV_ADK_PARAM for illegal handle type or pointer@n
///         #EMV_ADK_TLV_BUILD_ERR TLV processing problem@n
///         #EMV_ADK_INTERNAL internal communication problem, illegal file content, memory allocation@n
///         #EMV_ADK_NOT_ALLOWED concurrent call or not allowed within callback@n
///         #EMV_ADK_READ_ERROR file corruption @n
///         #EMV_ADK_SAVE_ERROR persistence problem @n
///         #EMV_ADK_NO_EXEC if number of configurable AIDs is a limit
///
/// @b Serialization:
/// @image html TLV_SetAppliData.jpg
/// Class: #CLA_EMV, Instruction: #INS_APPLI_CFG, P2: #P2_SET
///
DLL_CTC EMV_ADK_INFO EMV_CT_SetAppliData(EMV_ADK_HANDLE_RECORD_TYPE eHandleAppliType, EMV_CT_APPLI_TYPE* pxAID, EMV_CT_APPLIDATA_TYPE* pxAppliData);

/*****************************************************************************
*  EMV_CT_GetAppliData
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Get configured application data
///    @n  Has to be called once per application (Visa, MasterCard, ...).
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Configure_Application_Data "Configure Application Data"
///
/// @author GSS R&D Germany
///
/// @param[in]      eReadAppliType  See @ref READ_APPLI_TYPE
///                                 @n TLV tag #TAG_HANDLE_APPLI_TYPE
/// @param[in,out]  pxAID           AID, see @ref EMV_CT_APPLI_STRUCT
///                                 @n TLV tag #TAG_4F_APP_ID
/// @param[out]     pxAppliData     Application data, see @ref EMV_CT_APPLIDATA_STRUCT
/// @return #EMV_ADK_OK, #EMV_ADK_INTERNAL, #EMV_ADK_READ_ERROR, #EMV_ADK_PARAM
///
/// @b Serialization:
/// @image html TLV_GetAppliData.jpg
/// Class: #CLA_EMV, Instruction: #INS_APPLI_CFG, P2: #P2_GET
///
DLL_CTC EMV_ADK_INFO EMV_CT_GetAppliData(EMV_ADK_READAPPLI_TYPE eReadAppliType, EMV_CT_APPLI_TYPE* pxAID, EMV_CT_APPLIDATA_TYPE* pxAppliData);


/*****************************************************************************
*  EMV_CT_ApplyConfiguration
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Write configuration to files.
///
/// This function writes the configuration to file system if @ref EMV_CT_INIT_OPT_CONFIG_MODE was set.
/// If @ref EMV_CT_INIT_OPT_CONFIG_MODE was not set, this function returns EMV_ADK_NO_EXEC and does nothing.
///
/// Explanation in programmers guide: @ref subsec_emv_ct_virt_term_map
///
/// @author GSS R&D Germany
///
/// @param[in]  options  RFU
///             @n TLV tag #TAG_DF8F0F_APPLYCONFIG_OPTIONS
/// @return #EMV_ADK_OK, #EMV_ADK_SAVE_ERROR, #EMV_ADK_NO_EXEC
///
/// @b Serialization:
/// @image html TLV_ApplyConfiguration.jpg
/// Class: #CLA_EMV, Instruction: #INS_APPLY_CFG
///
DLL_CTC EMV_ADK_INFO EMV_CT_ApplyConfiguration(unsigned long options);


/*****************************************************************************
* EMV_CT_StartTransaction
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief EMV Application Selection
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Starting_a_Transaction "Starting a Transaction"
///
/// @author GSS R&D Germany
///
/// @param[in]   pxSelectInput  Transaction specific data, see @ref EMV_CT_SELECT_STRUCT
/// @param[out]  pxSelectRes    Additional (error) info, see @ref EMV_CT_SELECTRES_STRUCT
///
/// @return #EMV_ADK_OK, #EMV_ADK_INTERNAL, #EMV_ADK_NOAPP, #EMV_ADK_PARAM, #EMV_ADK_FALLBACK, #EMV_ADK_BADAPP, #EMV_ADK_ABORT, #EMV_ADK_APP_BLOCKED
///
/// @b Serialization:
/// @image html TLV_StartTransaction.jpg
/// Class: #CLA_EMV, Instruction: #INS_SELECT
/// @n Callbacks: #EMV_CT_CLA_CBCK_REQ, #EMV_CT_CLA_CBCK_RESP
///
DLL_CTC EMV_ADK_INFO EMV_CT_StartTransaction(EMV_CT_SELECT_TYPE *pxSelectInput, EMV_CT_SELECTRES_TYPE* pxSelectRes);

/*****************************************************************************
* EMV_CT_GetCandidateData
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Get additional candidate information for reducing, resorting or
///        displaying the mutual candidate list.
///
/// This function is optionally called after EMV_CT_StartTransaction (reentrance mode)
/// or within the reduce candidates callback. It retrieves additional information
/// for all mutual candidates. The output list array length must be at least the
/// number of mutual candidates which is less or equal #EMV_CT_COMMON_CANDS.
///
/// @author GSS R&D Germany
///
/// @param[out]  candidateData  Extended candidate data, see @ref EMV_CT_CANDIDATE_DATA_STRUCT
///
/// @return #EMV_ADK_OK in case of success
///
/// @b Serialization:
/// Class: #CLA_EMV, Instruction: #INS_CND_DATA
///
DLL_CTC EMV_ADK_INFO EMV_CT_GetCandidateData(EMV_CT_CANDIDATE_DATA_TYPE *candidateData);

/*****************************************************************************
*  EMV_CT_ContinueOffline
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief EMV transaction (offline part including 1st cryptogram)
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Continue_a_Transaction__Offline_Part "Continue a Transaction Offline Part"
///
/// @author GSS R&D Germany
///
/// @param[in]   pxTransactionInput  Transaction data, see @ref EMV_CT_TRANSAC_STRUCT
/// @param[out]  pxTransRes          Transaction result data, see @ref EMV_CT_TRANSRES_STRUCT
///
/// @return #EMV_ADK_ARQC = transaction must go online
/// @n      #EMV_ADK_TC = transaction is offline approved
/// @n      #EMV_ADK_FALLBACK = depending on domestic specification a fallback to magstripe may be allowed
/// @n      #EMV_ADK_OK = card data read successfully (only possible for transaction types #EMV_ADK_TRAN_TYPE_REFUND, #EMV_ADK_TRAN_TYPE_MANUAL_REVERSAL, #EMV_ADK_TRAN_TYPE_BUCHUNG_B, #EMV_ADK_TRAN_TYPE_BUCHUNG_A)
/// @n      other values mean error: #EMV_ADK_AAC, #EMV_ADK_AAR, #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, #EMV_ADK_CARDERR, #EMV_ADK_BADAPP, #EMV_ADK_ABORT, #EMV_ADK_CVM.
///
/// @b Serialization:
/// @image html TLV_ContinueOffline.jpg
/// Class: #CLA_EMV, Instruction: #INS_TRANSAC
/// @n Callbacks: #EMV_CT_CLA_CBCK_REQ, #EMV_CT_CLA_CBCK_RESP
///
DLL_CTC EMV_ADK_INFO EMV_CT_ContinueOffline(EMV_CT_TRANSAC_TYPE *pxTransactionInput, EMV_CT_TRANSRES_TYPE *pxTransRes);

/*****************************************************************************
*  EMV_CT_ContinueOnline
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief EMV transaction (handling of host response including 2nd cryptogram)
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Continue_a_Transaction__Online_Part "Continue a Transaction Online Part"
///
/// @author GSS R&D Germany
///
/// @param[in]   pxOnlineInput  Data of host response, see @ref EMV_CT_HOST_STRUCT
/// @param[out]  pxTransRes     Result data, see @ref EMV_CT_TRANSRES_STRUCT
///
/// @return #EMV_ADK_TC, #EMV_ADK_AAC, #EMV_ADK_ABORT, #EMV_ADK_ONLINE_PIN_RETRY, #EMV_ADK_CVM, #EMV_ADK_INTERNAL, #EMV_ADK_PARAM, #EMV_ADK_CARDERR, #EMV_ADK_BADAPP
///
/// @b Serialization:
/// @image html TLV_ContinueOnline.jpg
/// Class: #CLA_EMV, Instruction: #INS_ONLINE
///
DLL_CTC EMV_ADK_INFO EMV_CT_ContinueOnline(EMV_CT_HOST_TYPE *pxOnlineInput, EMV_CT_TRANSRES_TYPE *pxTransRes);

/*****************************************************************************
*  EMV_CT_fetchTxnTags
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Fetch one or several tags at the end of the transaction
///
/// While ContinueOffline and ContinueOnline provide the most common / generic tags,
/// which are necessary for application handling in a readable way (structure),
/// this may not fulfill all local requirements.
/// This function offers to read any proprietary, extended or application specific tag,
/// which is additionally (next to the generic ones) required by the application.
/// Calling this function is optional, however it may provide benefit for single tags as well as for tag combinations,
/// which will be provided in a TLV stream (e.g. needed for BMP 55) instead of structure data,
/// which is preferable for receipt printing or offline storage.
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Fetching_Additional_Tags "Fetching Additional Tags"
///
/// @author GSS R&D Germany
///
/// @param[in]      options            Options, see @ref FETCH_TAGS_OPTIONS
///                                    @n TLV tag #TAG_DF3B_PARAMETER_1
/// @param[in]      requestedTags      Requested tags @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in]      noOfRequestedTags  Number of requested tags @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in,out]  tlvBuffer          Buffer for TLV output, allocated by calling application @n not serialized on TLV interface
/// @param[in]      bufferLength       Number of bytes allocated for tlvBuffer @n not serialized on TLV interface
/// @param[out]     tlvDataLength      Number of bytes written to tlvBuffer @n not serialized on TLV interface
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR
///
DLL_CTC EMV_ADK_INFO EMV_CT_fetchTxnTags(unsigned long options, unsigned long* requestedTags, unsigned short noOfRequestedTags,
                                         unsigned char* tlvBuffer, unsigned short bufferLength, unsigned short* tlvDataLength );


/*****************************************************************************
*  EMV_CT_fetchTxnDOL
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Retrieve formatted transaction data at the end of the transaction
///
/// Like @ref EMV_CT_fetchTxnTags but take a DOL and return pure data.
/// The return buffer is filled with requested data, each with exact requested
/// data length. The tag's data type is analysed and type specific truncation and
/// padding is applied. If a tag's type is unknown, binary is assumed. If the tag is
/// unknown or no data available, '00' bytes are padded.
/// If the requested data length is greater than the available data length, then numeric
/// data is padded with leading '00' and compressed numeric is padded with
/// trailing 'FF'. Other types are padded right with trailing '00'.
/// If the requested data length is smaller than the available data length, then
/// numeric values are cut at front and others at the end.
///
/// Transport tags used in serialisation: #CLA_EMV #INS_FETCH_DOL
///
/// @author GSS R&D Germany
///
/// @param[in]      options     Options: None, reserved for future use @n TLV tag #TAG_DF3B_PARAMETER_1
/// @param[in]      DOL         Data Object List = sequence of BER-TLV Tag+Length pairs @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in]      dolLen      Length of DOL [bytes] @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in,out]  buffer      Result data buffer @n TLV tag #TAG_ISO_DATA
/// @param[in]      bufferSize  Size of result data buffer [bytes] @n TLV tag #TAG_DF3D_PARAMETER_3
/// @param[out]     dataLength  Result data length = sum of all lengths in the DOL (in case of success) @n TLV tag #TAG_ISO_DATA
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CTC EMV_ADK_INFO EMV_CT_fetchTxnDOL(unsigned long options, const unsigned char* DOL, unsigned dolLen, unsigned char* buffer, unsigned bufferSize, unsigned* dataLength);

/*****************************************************************************
*  EMV_CT_updateTxnTags
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Update one or several kernel tags during transaction
///
/// This update is restricted to certain (not protected) EMV L2 kernel tags,
/// if there is a need to change these tags during the transaction temporarily and
/// for this transaction only but not for the general configuration. Use carefully.
///
/// These tags are allowed to change:
/// @li #TAG_42_ISSUER_ID
/// @li #TAG_5F50_ISSUER_URL
/// @li #TAG_5F53_IBAN
/// @li TAG_5F54_BIC
/// @li #TAG_5F55_ISSUER_COUNTRY_2
/// @li #TAG_5F56_ISSUER_COUNTRY_3
/// @li 0x9F4D
/// @li #TAG_91_ISS_AUTH_DATA (EMV_CT_HOST_STRUCT::AuthData)
/// @li #TAG_9F01_ACQ_ID
/// @li #TAG_9F1D_TRM_RISK_MNGT_DATA
/// @li #TAG_9F4E_TAC_MERCHANTLOC
/// @li #TAG_9F1E_IFD_SERIAL_NB (EMV_CT_TERMDATA_STRUCT::IFDSerialNumber)
/// @li #TAG_9F15_MERCH_CATEG_CODE (EMV_CT_APPLIDATA_STRUCT::BrKey)
/// @li #TAG_9F16_MERCHANT_ID (EMV_CT_APPLIDATA_STRUCT::MerchIdent)
/// @li #TAG_9F1B_TRM_FLOOR_LIMIT (EMV_CT_APPLIDATA_STRUCT::FloorLimit)
/// @li #TAG_9F39_POS_ENTRY_MODE (EMV_CT_APPLIDATA_STRUCT::POS_EntryMode)
/// @li #TAG_9F1C_TRM_ID (EMV_CT_TERMDATA_STRUCT::TermIdent)
/// @li #TAG_9F33_TRM_CAPABILITIES (EMV_CT_APPLIDATA_STRUCT::App_TermCap)
/// @li #TAG_9F35_TRM_TYPE (EMV_CT_APPLIDATA_STRUCT::App_TermTyp)
/// @li #TAG_9F40_ADD_TRM_CAP (EMV_CT_APPLIDATA_STRUCT::App_TermAddCap)
/// @li #TAG_9A_TRANS_DATE (EMV_CT_PAYMENT_STRUCT::Date)
/// @li #TAG_9C_TRANS_TYPE (EMV_CT_SELECT_STRUCT::TransType)
/// @li #TAG_9F21_TRANS_TIME (EMV_CT_PAYMENT_STRUCT::Time)
/// @li #TAG_9F41_TRANS_SEQUENCE_NB (EMV_CT_PAYMENT_STRUCT::TransCount)
/// @li #TAG_5F2A_TRANS_CURRENCY (EMV_CT_PAYMENT_STRUCT::CurrencyTrans)
/// @li #TAG_5F36_TRANS_CURRENCY_EXP (EMV_CT_PAYMENT_STRUCT::ExpTrans)
/// @li #TAG_9F02_NUM_AMOUNT_AUTH (EMV_CT_PAYMENT_STRUCT::Amount)
/// @li #TAG_9F03_NUM_AMOUNT_OTHER (EMV_CT_PAYMENT_STRUCT::Cashback_Amount)
/// @li #TAG_8A_AUTH_RESP_CODE (EMV_CT_HOST_STRUCT::AuthResp)
/// @li #TAG_95_TVR (just setting of bits allowed)
/// @li #TAG_DFD003_STORED_AMOUNT
/// @li #TAG_CAND_LIST (EMV_CT_SELECTRES_STRUCT::T_BF04_Candidates)
/// @li All proprietary tags configured by EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM
///
/// @author GSS R&D Germany
///
/// @param[in]  options       Options, see @ref UPDATE_TAGS_OPTIONS @n TLV tag #TAG_DF3B_PARAMETER_1
/// @param[in]  tlvBuffer     Pointer to TLV data @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in]  bufferLength  Size of @c tlvBuffer @n TLV tag #TAG_DF3C_PARAMETER_2
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR
///
DLL_CTC EMV_ADK_INFO EMV_CT_updateTxnTags(unsigned long options, unsigned char *tlvBuffer, unsigned short bufferLength);

/// @ingroup ADK_TRX_EXEC
/// @brief Data structure for output of EMV_CT_CheckSupportedAID
typedef struct
{
   EMV_CT_APPLI_TYPE aid;       ///< AID returned by the card (DF name, tag '84')
                                ///< @n TLV tag #TAG_4F_APP_ID
   unsigned char  API;          ///< Application priority indicator ('82') '0' - 'F' (top bit = confirmation required)
                                ///< @n TLV tag #TAG_87_APP_PRIORITY_ID
   unsigned char  text[17];     ///< Null terminated text supplied by app, could be 9F12 or 50 or 84(converted to ascii)
                                ///< @n TLV tag #TAG_50_APP_LABEL
   unsigned char  aux_text[17]; ///< Null terminated secondary text field
                                ///< @n TLV tag #TAG_DF3B_PARAMETER_1
   unsigned char  code_table;   ///< character code table index (Value '1' - 'A') or 0 = not specified (ISO 8859)
                                ///< + indicator which label went to text:
                                ///< EMVL2_DFLTXT=0x80 for default label
                                ///< EMVL2_APPLBL=0x40 for application preferred name ('9F12')
                                ///< EMVL2_APPLBL=0x20 for application label ('50')
                                ///< @n TLV tag #TAG_9F11_ISS_CODE_TABLE_ID
   unsigned char  lang_pref[9]; ///< language preference ASCII with string termination, converted to small letters
                                ///< @n TLV tag #TAG_5F2D_LANGUAGE
} EMV_CT_CandListType;

/*****************************************************************************
 *  EMV_CT_CheckSupportedAID
 *****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Do List Of Application processing for a single terminal AID
///
/// Only recommended for experienced users.
///
/// @author GSS R&D Germany
///
/// @param[in]   aid              Terminal AID('9F 06') @n TLV tag #TAG_4F_APP_ID
/// @param[in]   ASI              Application selection indicator 0=exact, 1=partial @n TLV tag #TAG_DF20_ASI
/// @param[in]   defaultLabel     Optional: default application label @n TLV tag #TAG_50_APP_LABEL
/// @param[out]  pCandList        Buffer for candidates found @n TLV tag #TAG_CAND_LIST
/// @param[in]   MaxCand          Number items that can be stored to pCandList buffer,
///                               [1 .. EMV_CT_COMMON_CANDS] @n not serialized on TLV interface
/// @param[out]  pCandidateCount  Number of candidates found
///                               @n TLV interface: number of iterations of #TAG_CAND_LIST
/// @param[out]  sw12             Optional: status word return by card,
///                               '0000' for communication problem
///                               @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in]   adtCardTagList   Additional card tags list for Additional Tag Store configuration:
///                               @n 1 byte overall list length + n * (tag (1-4 bytes) + type (1 byte) + minimal length (1 byte) + maximal length (1 byte)).
///                               @n If null pointer the framework's standard ADT card tag list will be used, if list length is 0 no additional card tags
///                               are put to the Addtional Tag Store configuration.
///                               @n Types are according L2 kernel interface: 1 binary, 2 numeric, 3 np, 4 date, 5 an, 6 ans.
///                               @n TLV tag #TAG_DF2C_ADD_TAGS_CRD
///
/// @return
/// #EMV_ADK_OK (EMVL2_AUTO_SELECT, EMVL2_NOT_SELECTED)
/// @n #EMV_ADK_BADAPP (EMVL2_NON_EMV)
/// @n #EMV_ADK_NOAPP (EMVL2_MAG_STRIPE)
/// @n #EMV_ADK_CARD_BLOCKED (EMVL2_ICC_BLOCKED)
/// @n #EMV_ADK_APP_BLOCKED (EMVL2_DF_BLOCKED, '62 83')
/// @n #EMV_ADK_CARDERR_FORMAT (EMVL2_MNDTY_DATA)
/// @n #EMV_ADK_PARAM (EMVL2_BAD_PARAM)
/// @n #EMV_ADK_NO_CARD (EMVL2_FAIL + missing card)
/// @n #EMV_ADK_FALLBACK (EMVL2_FAIL + card in)
///
DLL_CTC EMV_ADK_INFO EMV_CT_CheckSupportedAID(
    const EMV_CT_APPLI_TYPE* aid, unsigned char ASI,
    const unsigned char* defaultLabel,
    EMV_CT_CandListType* pCandList, unsigned char MaxCand,
    unsigned char* pCandidateCount, unsigned short* sw12,
    const unsigned char* adtCardTagList);

/*****************************************************************************
*  EMV_CT_EndTransaction
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief End transaction
///
/// @author GSS R&D Germany
///
/// @param[in]  options  Options, reserved for future use @n TLV tag #TAG_DF3B_PARAMETER_1
///
/// @return #EMV_ADK_OK
///
/// @b Serialization:
/// Class: #CLA_EMV, Instruction: #INS_END_TRX
///
DLL_CTC EMV_ADK_INFO EMV_CT_EndTransaction(unsigned long options);

// ========================================================================================================
// === CONTACT FUNCTIONS for transparent ICC access of the customer card ===
// ========================================================================================================

/// @defgroup ADK_ICC_OPTION Options for ICC transparent commands
/// @ingroup ADK_ICC_IF
/// @brief Parameter @c ucOptions in EMV_CT_SmartISO(), EMV_CT_SmartDetect(), EMV_CT_SmartReset()
/// @{
#define  EMV_CT_CUSTOMER               0x00  ///< bit 1,2 used for reader selection: Customer Card Slot
#define  EMV_CT_SAM_1                  0x01  ///< bit 1,2 used for reader selection: SAM1
#define  EMV_CT_SAM_2                  0x02  ///< bit 1,2 used for reader selection: SAM2
#define  EMV_CT_SAM_3                  (EMV_CT_SAM_1|EMV_CT_SAM_2) ///< bit 1,2 used for reader selection: SAM3
#define  EMV_CT_TRY_PPS                0x04  ///< Try to increase the baudrate by using PPS
#define  EMV_CT_SKIP_ATR               0x08  ///< Skip ATR reading, not included in response
#define  EMV_CT_TRY_PPS_EPA            0x10  ///< PPS handling for Austrian Maestro Cards
#define  EMV_CT_WARMRESET              0x20  ///< Warm Reset to ICC (Cold reset must have been applied before)
#define  EMV_CT_SAM_EMV_MODE           0x40  ///< SAMs are reset in mode ISO 7816 per default, use this bit to switch to EMV 311 (for SAMs only)
#define  EMV_CT_DETECT_WRONG_ATR       0x40  ///< Distinguish between wrong ATR and card wrong side/no chip and return applicable return value (for customer slot only)
#define  EMV_CT_CARD_CLASS_B_3V        0x80  ///< For class B (3V) cards
/// @}


/*****************************************************************************
* EMV_CT_SmartISO
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief send chip card command and receive response
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Send_an_ISO_Command_to_a_Smart_Card "ISO Command to a Smart Card"
///
/// @author GSS R&D Germany
///
/// @param[in]   ucOptions                  @ref ADK_ICC_OPTION @n TLV tag #TAG_DF06_CARDREADER_NUM
/// @param[in]   usInDataLen, pucDataIn     Chip card command @n TLV tag #TAG_ISO_DATA
/// @param[in]   usOutBufferLength          Maximum length of output buffer @n not serialized on TLV interface
/// @param[out]  pusOutDataLen, pucDataOut  Chip card response (with leading status word SW1/2) @n TLV tag #TAG_ISO_DATA
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CTC unsigned char EMV_CT_SmartISO (unsigned char ucOptions, unsigned short usInDataLen, unsigned char *pucDataIn, unsigned short *pusOutDataLen, unsigned char *pucDataOut, unsigned short usOutBufferLength);

/*****************************************************************************
* EMV_CT_SmartDetect
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Detect if chip card is inserted in card reader (in readable position)
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Detecting_a_Smart_Card "Detecting a Smart Card"
///
/// @author GSS R&D Germany
///
/// @param[in]  ucOptions  @ref ADK_ICC_OPTION @n TLV tag #TAG_DF06_CARDREADER_NUM
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CTC unsigned char EMV_CT_SmartDetect(unsigned char ucOptions);

/*****************************************************************************
* EMV_CT_SmartReset
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Detect if chip card is inserted in card reader (read ATR)
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Activating_a_Smart_Card "Activating a Smart Card"
///
/// @author GSS R&D Germany
///
/// @param[in]   ucOptions    @ref ADK_ICC_OPTION @n TLV tag #TAG_DF06_CARDREADER_NUM
/// @param[out]  pucATR       ATR (min. 40 bytes must be allocated) @n TLV tag #TAG_ATR
/// @param[out]  pnATRLength  ATR length @n TLV tag #TAG_ATR
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CTC unsigned char EMV_CT_SmartReset(unsigned char ucOptions, unsigned char* pucATR, unsigned long* pnATRLength);

/*****************************************************************************
* EMV_CT_SmartPowerOff
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Deactivate chip card
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Deactivate_a_Smart_Card "Deactivate a Smart Card"
///
/// @author GSS R&D Germany
///
/// @param[in]  ucOptions  @ref ADK_ICC_OPTION @n TLV tag #TAG_DF06_CARDREADER_NUM
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CTC unsigned char EMV_CT_SmartPowerOff(unsigned char ucOptions);

/*****************************************************************************
* EMV_CT_SmartPIN
*****************************************************************************/
/// @ingroup FUNC_ICC
/// @brief Encipher PIN and send VERIFY command
///
/// This function extends the transparent card access function group. It is only
/// required if the control flow is outside the framework. Otherwise you should
/// call EMV_CT_Send_PIN_Offline, no matter if PIN is enciphered or plain because
/// this function will not be allowed.
///
/// @author GSS R&D Germany
///
/// @param[in]   INICCRandom       ICC random number extracted from Get Challenge response (8 bytes)
///                                @n TLV tag #TAG_DF3B_PARAMETER_1
/// @param[in]   PubKeyData        PIN verification public key modulus
///                                @n TLV tag #TAG_KEY_KEY
/// @param[in]   PubKeyLen         length of PubKeyData in bytes
///                                @n TLV tag #TAG_KEY_KEY
/// @param[in]   Exponent          PIN verification public key exponent (0 for 2, 1 for 3, 16 for 65537)
///                                @n TLV tag #TAG_KEY_EXPONENT
/// @param[out]  pucPINResultData  PIN result data (usually 2 bytes length + status word SW12, buffer size 4 bytes)
///                                @n TLV tag #TAG_ISO_DATA
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL, #EMV_ADK_NO_EXEC
///
DLL_CTC unsigned char EMV_CT_SmartPIN(const unsigned char * INICCRandom,
                                      const unsigned char * PubKeyData,
                                      int PubKeyLen,
                                      int Exponent,
                                      unsigned char* pucPINResultData);


/*****************************************************************************
* EMV_CT_Send_PIN_Offline
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Send entered (offline) PIN to chip card for verification
///
/// Calling application displays "Please enter PIN" dialog.
/// It also manages the PIN entry with help of the vault.
/// @n But sending the PIN to the card for verification must be done by EMV ADK, because
/// - EMV ADK will store the PIN result internally for further processing.
/// - It must be assured that the same process sends the PIN to the ICC, which might be a problem otherwise, e.g.
/// when running in a client-server environment or different tasks or event handlers ...
///
/// Explanation in programmers guide: \ref anchor_emv_ct_Send_EMV_PIN_to_Smart_Card "Send EMV PIN to Smart Card"
///
/// @author GSS R&D Germany
///
/// @param[out]  pucPINResultData  PIN result data (2 bytes with status word SW12)
///                                @n 9000 = PIN correct; 63Cn = PIN wrong (n = number of tries left)
///                                @n TLV tag #TAG_ISO_DATA
///
/// @return see @ref SMART_STATUS, additionally possible: #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
DLL_CTC unsigned char EMV_CT_Send_PIN_Offline(unsigned char *pucPINResultData);


/// @defgroup ADK_LED_DEFINES Defines for LED handling
/// @ingroup ADK_LED
/// @brief LED numbers and modes to be used in EMV_CT_LED()
/// @{
  #define CONTACT_LED_ID_0                  0x00 ///< There is currently maximum one CT Slot

  #define CONTACT_LED_OFF                   0x00 ///< Switch LED Off
  #define CONTACT_LED_ON                    0x01 ///< Switch LED On
  #define CONTACT_LED_BLINK                 0x02 ///< Enable blinking, fixed timing @deprecated: No more supported, for instance, because it relies on SVRMGR functionality that is going to be removed

  #define CONTACT_LED_COLOR_WHITE           0x01  ///< white led
  #define CONTACT_LED_COLOR_RED             0x02  ///< red led
  #define CONTACT_LED_COLOR_GREEN           0x03  ///< green led
  #define CONTACT_LED_COLOR_BLUE            0x04  ///< blue led
  #define CONTACT_LED_COLOR_YELLOW          0x05  ///< yellow led
/// @}

/*****************************************************************************
* EMV_CT_LED
*****************************************************************************/
/// @ingroup FUNC_CONF
/// @brief Control the Smart Card Reader LEDs
///
/// @author GSS R&D Germany
///
/// @param[in]  ucLedId      Id of the LED @n TLV tag #TAG_DF3B_PARAMETER_1
/// @param[in]  ucLedState   State of the LED (e.g. #CONTACT_LED_ON) @n TLV tag #TAG_DF3C_PARAMETER_2
/// @param[in]  ucLedColor   Color (e.g. #CONTACT_LED_COLOR_RED) @n TLV tag #TAG_DF3D_PARAMETER_3
/// @param[in]  ulTimeoutMs  Timeout in milliseconds @n TLV tag #TAG_DF19_PARAMETER_4
///
/// @return @c 0 ... okay @n @c !=0 ... error
///
DLL_CTC unsigned char EMV_CT_LED(unsigned char ucLedId, unsigned char ucLedState, unsigned char ucLedColor, unsigned long ulTimeoutMs);


#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _EMV_CT_INTERFACE_H_
