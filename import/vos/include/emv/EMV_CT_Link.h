/****************************************************************************
*  Product:     ADK Cards Service - EMV Contact (CT)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Client interface - transport layer
****************************************************************************/

#ifndef _EMV_CT_LINK_H_
#define _EMV_CT_LINK_H_

#include "emv/btlv.h"
#include "emv/EMV_Common_Interface.h"

#ifdef _VRXEVO
#   ifdef EMV_CTL_EXPORT
#       define DLL_CTL __declspec(dllexport)
#   else
#       ifdef EMV_TEC_LIB
#           define DLL_CTL
#       else
#           define DLL_CTL __declspec(dllimport)
#       endif
#   endif
#else
#   define DLL_CTL // NOTE: Do not set visibility("default") because export.map is used
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*  EMV_CT_CALLBACK_FnT
*****************************************************************************/
/// @ingroup TLV_CALLBCK
/// @brief Common callback function:
///        send/receive TLV stream to/from calling application.
///
/// Explanation in programmers guide: @ref anchor_emv_ct_Transaction_Callbacks "Transaction Callbacks"
///
/// @author GSS R&D Germany
///
/// @n Tags for transport layer:
///    @li Callback function request: <tt> 91 01 </tt> (#EMV_CT_CLA_CBCK_REQ, #INS_CBCK_FCT)
///    @li Data for card reader: <tt> 91 02 </tt> (#EMV_CT_CLA_CBCK_REQ, #INS_CBCK_CRD)
///    @li Trace output: <tt> 91 FF </tt> (#EMV_CT_CLA_CBCK_REQ, #INS_CBCK_TRACE)
///    @li Response: <tt> 92 </tt> (#EMV_CT_CLA_CBCK_RESP)
///
/// @n Tags found in @c pucSend determine which callback function is meant, see @ref CBCK_FCT_TAGS
///
/// @param[in]  pucSend  ... TLV stream to send to calling application
/// @param[in]  sSendSize ... length of TLV stream to send to calling application
/// @param[out]  pucReceive ... TLV stream received from calling application
/// @param[in,out]  psReceiveSize ... input: size of buffer pucReceive points to; output: size of TLV stream received from calling application
/// @param[out]     externalData  application data pointer, which was put in EMV_CT_Init_Framework()
///
typedef void (* EMV_CT_CALLBACK_FnT) (unsigned char *pucSend, unsigned short sSendSize, unsigned char *pucReceive, unsigned short *psReceiveSize, void* externalData);

/*****************************************************************************
*  EMV_CT_Interface
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] dataIn input TLV buffer
/// @param[in] dataInLen length of dataIn
/// @param[out] dataOut output TLV buffer
/// @param[in,out] dataOutLen length of dataOut
/// @returns EMV_ADK_OK, EMV_ADK_INTERNAL, EMV_ADK_PARAM
///
DLL_CTL EMV_ADK_INFO EMV_CT_Interface(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);

/*****************************************************************************
*  EMV_CT_IF_Header
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] header for serial interface, 4 byte: CLA, INS, P1, P2
/// @param[in] dataIn input TLV buffer (may be NULL)
/// @param[in] dataInLen length of dataIn
/// @param[out] response output TLV buffer (may be NULL)
/// @param[in,out] responseLen length of dataOut
/// @returns EMV_ADK_OK, EMV_ADK_INTERNAL, EMV_ADK_PARAM
///
DLL_CTL EMV_ADK_INFO EMV_CT_IF_Header(const unsigned char* header, const unsigned char *dataIn, unsigned short dataInLen, unsigned char *response, unsigned short *responseLen);

/*****************************************************************************
*  EMV_CT_IF_BERTLV
*****************************************************************************/
/// @brief Serial interface to EMV functions
///
/// @author GSS R&D Germany
///
/// @param[in] header for serial interface, 4 byte: CLA, INS, P1, P2
/// @param[in] dataIn input TLV Node (may be NULL)
/// @param[out] dataOut output TLV (may be NULL and may be same as input)
/// @returns EMV_ADK_OK, EMV_ADK_INTERNAL, EMV_ADK_PARAM
///
DLL_CTL EMV_ADK_INFO EMV_CT_IF_BERTLV(const unsigned char* header, const struct BTLVNode* dataIn,  struct BTLVNode* dataOut);

/*****************************************************************************
* EMV_CT_Disconnect
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Disconnect from EMV server
///
/// Since the EMV server handles only one client this function needs to be called
/// if the EMV control shall be handed over to different client process. Note:
/// This function has no effect, if not yet connected or not using the client/server
/// libaray.
///
/// @author GSS R&D Germany
///
DLL_CTL void EMV_CT_Disconnect(void);

/*****************************************************************************
* EMV_CT_SetClientMode
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Switch to Client-Server mode
///
/// There is only one static client library that uses the EMV framework library
/// by default. Alternatively it can connect via IPC to an EMV server. A thrird
/// mode - the gateway mode - can be used if there shall be only one channel
/// for both, contact and contactless.
///
/// @author GSS R&D Germany
///
DLL_CTL void EMV_CT_SetClientMode(enum EMV_CLIENT_MODE mode);

/*****************************************************************************
* EMV_CT_SetCallback
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Set Callback Function and activate Callback Thread
///
/// @param[in] EMV_Callback call back function pointer, may by NULL
/// @param[in] context context data pointer which will be passed back with the call back
///
/// @author GSS R&D Germany
///
DLL_CTL void EMV_CT_SetCallback(EMV_CT_CALLBACK_FnT EMV_Callback, void* context);

/*****************************************************************************
* EMV_CT_GetInitOptions
*****************************************************************************/
/// @ingroup FUNC_FLOW
/// @brief Getter to Init Options from EMV_CT_Init_Framework.
///
/// @return[in] options, in case of IPC with Two Piece Solution this is device specific
///
/// @author GSS R&D Germany
///
DLL_CTL unsigned long EMV_CT_GetInitOptions(void);

#ifdef __cplusplus
}     // extern "C"
#endif

#endif // _EMV_CT_LINK_H_
