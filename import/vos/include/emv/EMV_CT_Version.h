/****************************************************************************
*  Product:     ADK Cards Service - EMV Contact (CT)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Version definition
****************************************************************************/

#define EMV_CT_FRAMEWORK_VERSION  "4.7.15-62"  ///< Version of EMV framework @n see EMV_CT_TERMDATA_STRUCT::FrameworkVersion
