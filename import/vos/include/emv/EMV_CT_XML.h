/****************************************************************************
*  Product:     ADK Cards Service - EMV Contact (CT)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Definitions for XML config data files
****************************************************************************/

#ifndef EMV_CT_XML_H   /* avoid double interface-includes */
  #define EMV_CT_XML_H


// ========================================================================================================
// === XML tags ===
// ========================================================================================================
/// @defgroup ADK_XML_TAGS Tags for configuration storage
/// @ingroup ADK_CONFIGURATION
/// @brief Used for storing the configuration in XML files.
/// @{

#define XML_TAG_TERMDATA                        "TerminalData"            ///< constructed xml tag for terminal data, used in file "EMV_Terminal.xml"
#define XML_TAG_TERMDATA_TERM_TYP               "TermTyp"                 ///< TLV tag #TAG_9F35_TRM_TYPE @n Struct element EMV_CT_TERMDATA_STRUCT::TermTyp
#define XML_TAG_TERMDATA_COUNTRY_CODE_TERM      "CountryCodeTerm"         ///< TLV tag #TAG_9F1A_TRM_COUNTRY_CODE @n Struct element EMV_CT_TERMDATA_STRUCT::TermCountryCode
#define XML_TAG_TERMDATA_TERM_CAP               "TermCap"                 ///< TLV tag #TAG_9F33_TRM_CAPABILITIES @n Struct element EMV_CT_TERMDATA_STRUCT::TermCap
#define XML_TAG_TERMDATA_TERM_ADD_CAP           "TermAddCap"              ///< TLV tag #TAG_9F40_ADD_TRM_CAP @n Struct element EMV_CT_TERMDATA_STRUCT::TermAddCap
#define XML_TAG_TERMDATA_TERM_IDENT             "TermIdent"               ///< TLV tag #TAG_9F1C_TRM_ID @n Struct element EMV_CT_TERMDATA_STRUCT::TermIdent
#define XML_TAG_TERMDATA_CURRENCY_TRANS         "CurrencyTrans"           ///< TLV tag #TAG_5F2A_TRANS_CURRENCY @n Struct element EMV_CT_TERMDATA_STRUCT::CurrencyTrans
#define XML_TAG_TERMDATA_EXP_TRANS              "ExpTrans"                ///< TLV tag #TAG_5F36_TRANS_CURRENCY_EXP @n Struct element EMV_CT_TERMDATA_STRUCT::ExpTrans
#define XML_TAG_TERMDATA_SUPP_LANG              "SuppLang"                ///< TLV tag #TAG_SUPP_LANG @n Struct element EMV_CT_TERMDATA_STRUCT::SuppLang
#define XML_TAG_TERMDATA_IFD_SERIAL_NUMBER      "IFD_SerialNumber"        ///< TLV tag #TAG_9F1E_IFD_SERIAL_NB @n Struct element EMV_CT_TERMDATA_STRUCT::IFDSerialNumber
#define XML_TAG_TERMDATA_KERNEL_VERSION         "KernelVersion"           ///< TLV tag #TAG_KERNEL_VERSION @n Struct element EMV_CT_TERMDATA_STRUCT::KernelVersion
#define XML_TAG_TERMDATA_FRAMEWORK_VERSION      "FrameworkVersion"        ///< TLV tag #TAG_DF11_LIB_VERSION @n Struct element EMV_CT_TERMDATA_STRUCT::FrameworkVersion
#define XML_TAG_TERMDATA_L1DRIVER_VERSION       "L1DriverVersion"         ///< TLV tag #TAG_L1DRIVER_VERSION @n Struct element EMV_CT_TERMDATA_STRUCT::L1DriverVersion

#define XML_TAG_CAP_KEYS                        "CapKeys"                 ///< constructed xml tag for CAP key data, used in file "EMV_Keys.xml"
#define XML_TAG_CAP_KEYS_CAPKEY                 "CapKey"                  ///< constructed xml tag for A SINGLE CAP key @ref EMV_CT_CAPKEY_STRUCT, embedded in #XML_TAG_CAP_KEYS
#define XML_TAG_CAP_KEYS_INDEX                  "Index"                   ///< TLV tag #TAG_KEY_INDEX @n Struct element EMV_CT_CAPKEY_STRUCT::Index
#define XML_TAG_CAP_KEYS_RID                    "RID"                     ///< TLV tag #TAG_KEY_RID @n Struct element EMV_CT_CAPKEY_STRUCT::RID
#define XML_TAG_CAP_KEYS_KEY                    "Key"                     ///< TLV tag #TAG_KEY_KEY @n Struct element EMV_CT_CAPKEY_STRUCT::Key
#define XML_TAG_CAP_KEYS_KEYLEN                 "KeyLen"                  ///< Length of data in TLV tag #TAG_KEY_KEY @n Struct element EMV_CT_CAPKEY_STRUCT::KeyLen
#define XML_TAG_CAP_KEYS_EXPONENT               "Exponent"                ///< TLV tag #TAG_KEY_EXPONENT @n Struct element EMV_CT_CAPKEY_STRUCT::Exponent
#define XML_TAG_CAP_KEYS_HASH                   "Hash"                    ///< TLV tag #TAG_KEY_HASH @n Struct element EMV_CT_CAPKEY_STRUCT::Hash
#define XML_TAG_CAP_KEYS_REVOC_LIST             "RevocationList"          ///< TLV tag #TAG_KEY_CRL @n Struct element EMV_CT_CAPKEY_STRUCT::RevocEntries

#define XML_TAG_APPLIDATA                       "ApplicationData"                        ///< constructed xml tag for application data, used in file "EMV_Applications.xml"
#define XML_TAG_APPLIDATA_APP                   "Application"                            ///< Constructed tag for one AID, embedded in #XML_TAG_APPLIDATA
#define XML_TAG_APPLIDATA_AID                   "AID"                                    ///< TLV tag #TAG_DF04_AID @n Struct element EMV_CT_APPLI_STRUCT::AID
#define XML_TAG_APPLIDATA_VER_NUM               "VerNum"                                 ///< TLV tag #TAG_9F09_TRM_APP_VERSION_NB @n Struct element EMV_CT_APPLIDATA_STRUCT::VerNum
#define XML_TAG_APPLIDATA_APP_NAME              "AppName"                                ///< TLV tag #TAG_50_APP_LABEL @n Struct element EMV_CT_APPLIDATA_STRUCT::AppName
#define XML_TAG_APPLIDATA_ASI                   "ASI"                                    ///< TLV tag #TAG_DF20_ASI @n Struct element EMV_CT_APPLIDATA_STRUCT::ASI
#define XML_TAG_APPLIDATA_BR_KEY                "BrKey"                                  ///< TLV tag #TAG_9F15_MERCH_CATEG_CODE @n Struct element EMV_CT_APPLIDATA_STRUCT::BrKey
#define XML_TAG_APPLIDATA_TERM_IDENT            "TermIdent"                              ///< TLV tag #TAG_9F1C_TRM_ID @n Struct element EMV_CT_APPLIDATA_STRUCT::TermIdent
#define XML_TAG_APPLIDATA_FLOOR_LIMIT           "FloorLimit"                             ///< TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT @n Struct element EMV_CT_APPLIDATA_STRUCT::FloorLimit
#define XML_TAG_APPLIDATA_SECURE_LIMIT          "SecurityLimit"                          ///< TLV tag #TAG_DF49_APL_SEC_LIMIT @n Struct element EMV_CT_APPLIDATA_STRUCT::Security_Limit
#define XML_TAG_APPLIDATA_NON_SECURE_CAPS       "BelowLimitTerminalCapabilities"         ///< TLV tag #TAG_DF4A_APL_SEC_CAPS @n Struct element EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit
#define XML_TAG_APPLIDATA_THRESHOLD             "Threshold"                              ///< TLV tag #TAG_DF24_THRESHHOLD @n Struct element EMV_CT_APPLIDATA_STRUCT::Threshhold
#define XML_TAG_APPLIDATA_TARGET_PERCENTAGE     "TargetPercentage"                       ///< TLV tag #TAG_DF26_PERCENT_ONL @n Struct element EMV_CT_APPLIDATA_STRUCT::TargetPercentage
#define XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE "MaxTargetPercentage"                    ///< TLV tag #TAG_DF25_MAXPERCENT_ONL @n Struct element EMV_CT_APPLIDATA_STRUCT::MaxTargetPercentage
#define XML_TAG_APPLIDATA_TAC_DENIAL            "TAC_Denial"                             ///< TLV tag #TAG_DF21_TAC_DENIAL @n Struct element EMV_CT_APPLIDATA_STRUCT::TACDenial
#define XML_TAG_APPLIDATA_TAC_ONLINE            "TAC_Online"                             ///< TLV tag #TAG_DF22_TAC_ONLINE @n Struct element EMV_CT_APPLIDATA_STRUCT::TACOnline
#define XML_TAG_APPLIDATA_TAC_DEFAULT           "TAC_Default"                            ///< TLV tag #TAG_DF23_TAC_DEFAULT @n Struct element EMV_CT_APPLIDATA_STRUCT::TACDefault
#define XML_TAG_APPLIDATA_EMV_APPLICATION       "EMV_Application"                        ///< TLV tag #TAG_DF2D_EMV_APPLI @n Struct element EMV_CT_APPLIDATA_STRUCT::EMV_Application
#define XML_TAG_APPLIDATA_DEFAULT_TDOL          "DefaultTDOL"                            ///< TLV tag #TAG_DF27_DEFAULT_TDOL @n Struct element EMV_CT_APPLIDATA_STRUCT::Default_TDOL
#define XML_TAG_APPLIDATA_DEFAULT_DDOL          "DefaultDDOL"                            ///< TLV tag #TAG_DF28_DEFAULT_DDOL @n Struct element EMV_CT_APPLIDATA_STRUCT::Default_DDOL
#define XML_TAG_APPLIDATA_MERCH_IDENT           "MerchIdent"                             ///< TLV tag #TAG_9F16_MERCHANT_ID @n Struct element EMV_CT_APPLIDATA_STRUCT::MerchIdent
#define XML_TAG_APPLIDATA_CDA_PROCESSING        "CDA_Processing"                         ///< TLV tag #TAG_DF3E_CDA_PROC @n Struct element EMV_CT_APPLIDATA_STRUCT::CDAProcessing
#define XML_TAG_APPLIDATA_AC_BEFORE_AFTER       "AC_BeforeAfter"                         ///< TLV tag #TAG_DF15_OFFL_ONLY_PROCESS @n Struct element EMV_CT_APPLIDATA_STRUCT::ACBeforeAfter
#define XML_TAG_APPLIDATA_AIP_CVM_NOT_SUPPORTED "AIP_CVM_NotSupported"                   ///< TLV tag #TAG_DF2E_CVM_NOT_SUPP @n Struct element EMV_CT_APPLIDATA_STRUCT::AIP_CVM_not_supported
#define XML_TAG_APPLIDATA_POS_ENTRY_MODE        "POS_EntryMode"                          ///< TLV tag #TAG_9F39_POS_ENTRY_MODE @n Struct element EMV_CT_APPLIDATA_STRUCT::POS_EntryMode
#define XML_TAG_APPLIDATA_ADD_VER_NUM           "AdditionalVersionNumbers"               ///< TLV tag #TAG_DF5F_ADD_APP_VERSION @n Struct element EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No
#define XML_TAG_APPLIDATA_APP_FLOW_CAP          "AppFlowCap"                             ///< TLV tag #TAG_DF2B_APP_FLOW_CAP @n Struct element EMV_CT_APPLIDATA_STRUCT::App_FlowCap
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM   "AdditionalTagsTRM"                      ///< TLV tag #TAG_DF29_ADD_TAGS @n Struct element EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD   "AdditionalTagsCRD"                      ///< TLV tag #TAG_DF2C_ADD_TAGS_CRD @n Struct element EMV_CT_APPLIDATA_STRUCT::Additional_Tags_CRD
#define XML_TAG_APPLIDATA_TAGLIST               "MandatoryTaglistCRD"                    ///< TLV tag #TAG_DF2A_DUTY_TAGS @n Struct element EMV_CT_APPLIDATA_STRUCT::Mandatory_Tags_CRD
#define XML_TAG_APPLIDATA_APP_TERM_CAP          "AppTermCap"                             ///< TLV tag #TAG_9F33_TRM_CAPABILITIES @n Struct element EMV_CT_APPLIDATA_STRUCT::App_TermCap
#define XML_TAG_APPLIDATA_COUNTRY_CODE_TERM     "CountryCodeTerm"                        ///< TLV tag #TAG_9F1A_TRM_COUNTRY_CODE @n Struct element EMV_CT_APPLIDATA_STRUCT::App_CountryCodeTerm
#define XML_TAG_APPLIDATA_APP_TERM_ADD_CAP      "AppTermAddCap"                          ///< TLV tag #TAG_9F40_ADD_TRM_CAP @n Struct element EMV_CT_APPLIDATA_STRUCT::App_TermAddCap
#define XML_TAG_APPLIDATA_APP_TERM_TYP          "AppTerminalType"                        ///< TLV tag #TAG_9F35_TRM_TYPE @n Struct element EMV_CT_APPLIDATA_STRUCT::App_TermTyp
#define XML_TAG_APPLIDATA_AID_PRIO              "AID_Prio"                               ///< TLV tag #TAG_DF1D_PRIO_APPLI @n Struct element EMV_CT_APPLIDATA_STRUCT::xAIDPrio
#define XML_TAG_APPLIDATA_FALLBACK_MIDS         "FallbackMIDs"                           ///< TLV tag #TAG_DF1D_PRIO_APPLI @n Struct element EMV_CT_APPLIDATA_STRUCT::tucFallbackMIDs
#define XML_TAG_APPLIDATA_SPECIAL_TRX           "SpecialTRX"                             ///< TLV tag #TAG_DF1C_SPECIAL_TRX @n Struct element EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX
#define XML_TAG_APPLIDATA_FALLBACK_HANDLING     "FallbackHandling"                       ///< TLV tag #TAG_DF18_FALLABCK @n Struct element EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling
#define XML_TAG_APPLIDATA_CUSTOMER_CVM          "CustomerCVM"                            ///< TLV tag #TAG_DF2F_CVM_CUSTOM @n Struct element EMV_CT_APPLIDATA_STRUCT::Customer_CVM
#define XML_TAG_APPLIDATA_CHKSUM_PARAMS         "ChksumParams"                           ///< TLV tag #TAG_DF13_TERM_PARAM @n Struct element EMV_CT_APPLIDATA_STRUCT::Chksum_Params
#define XML_TAG_APPLIDATA_CHKSUM_ASCII_EMVCO    "ChksumASCII_EMVCO"                      ///< TLV tag #TAG_DF12_CHECKSUM @n Struct element EMV_CT_APPLIDATA_STRUCT::ChksumASCIIEMVCO
#define XML_TAG_APPLIDATA_MASTER_AID            "MasterAID"                              ///< TLV tag #TAG_DF04_AID @n Struct element EMV_CT_APPLIDATA_STRUCT::MasterAID
#define XML_TAG_APPLIDATA_TXN_TYPES_SALE        "TxnTypesSale"                           ///< TLV tag: #TAG_DFD004_TXN_TYPES_SALE @n Struct element: EMV_CT_APPLIDATA_STRUCT::TxnTypesSale
#define XML_TAG_APPLIDATA_TXN_TYPES_CASH        "TxnTypesCash"                           ///< TLV tag: #TAG_DFD005_TXN_TYPES_CASH @n Struct element: EMV_CT_APPLIDATA_STRUCT::TxnTypesCash
#define XML_TAG_APPLIDATA_TXN_TYPES_CASHBACK    "TxnTypesCashback"                       ///< TLV tag: #TAG_DFD006_TXN_TYPES_CASHBACK @n Struct element: EMV_CT_APPLIDATA_STRUCT::TxnTypesCashback
#define XML_TAG_APPLIDATA_TXN_TYPES_REFUND      "TxnTypesRefund"                         ///< TLV tag: #TAG_DFD007_TXN_TYPES_REFUND @n Struct element: EMV_CT_APPLIDATA_STRUCT::TxnTypesRefund

#define XML_TAG_VTM                             "VirtualTerminalMap"                     ///< Enclosing tag used in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_ENTRY                       "VTMEntry"                               ///< One entry in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_TERMINAL                    "Terminal"                               ///< Virtual terminal index used in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_TRANSTYPE                   "TransType_9C"                           ///< Transaction type used in "EMV_VirtualTermConfig.xml"
#define XML_TAG_VTM_CURRENCYCODE                "CurrencyCode_5F2A"                      ///< Currency code used in "EMV_VirtualTermConfig.xml"

/// @}

#endif
