/****************************************************************************
*  Product:     ADK Cards Service - EMV
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Functions for usage in SDI Client
****************************************************************************/


#ifndef _EMV_SDI_INTERFACE_H_   /* avoid double interface-includes */
#define _EMV_SDI_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "emv/EMV_CT_Interface.h"
#include "emv/EMV_CTLS_Interface.h"

/// @brief Data structure for output data of EMV_SDI_CT_ContinueOffline() and EMV_SDI_CT_ContinueOnline()
///
typedef struct EMV_SDI_CT_TRANSRES_STRUCT
{
    unsigned char              IncludedData_DF61[8];   ///< Availability bits, see @ref DEF_INPUT_SDI_CT_TRANSRES
    char                   obfuscatedPANstr_DF44[20];  ///< Application Primary Account Number (PAN), obfuscated, presented in ASCII format @n Availability bit #INPUT_SDI_CT_TRXRES_DF44_0
    unsigned char           obfuscatedPANcn_DF1A[10];  ///< Application Primary Account Number (PAN), obfuscated, presented in cn (compressed numeric) format @n Availability bit #INPUT_SDI_CT_TRXRES_DF1A_0
    char                obfuscatedTrack2str_DF49[39];  ///< Track 2 Equivalent Data, obfuscated, presented in ASCII format @n Availability bit #INPUT_SDI_CT_TRXRES_DF49_0
    EMV_CT_TRACK2_TYPE   obfuscatedTrack2cn_DF1B;      ///< Track 2 Equivalent Data, obfuscated, presented in cn (compressed numeric) format @n Availability bit #INPUT_SDI_CT_TRXRES_DF1B_0
    unsigned char                  cardId_DFA014[20];  ///< Card Id token @n Availability bit #INPUT_SDI_CT_TRXRES_DFA014_0
} EMV_SDI_CT_TRANSRES_TYPE;

/// @defgroup DEF_INPUT_SDI_CT_TRANSRES SDI SDI transaction results: Availability bits
/// @{
  /* Byte 1 (Index 0) */
#define  INPUT_SDI_CT_TRXRES_DF44_0    0x01  ///< B1b1: EMV_SDI_CT_TRANSRES_STRUCT::obfuscatedPANhex_DF44
#define  INPUT_SDI_CT_TRXRES_DF1A_0    0x02  ///< B1b2: EMV_SDI_CT_TRANSRES_STRUCT::obfuscatedPANcn_DF1A
#define  INPUT_SDI_CT_TRXRES_DF49_0    0x04  ///< B1b3: EMV_SDI_CT_TRANSRES_STRUCT::obfuscatedTrack2hex_DF49
#define  INPUT_SDI_CT_TRXRES_DF1B_0    0x08  ///< B1b4: EMV_SDI_CT_TRANSRES_STRUCT::obfuscatedTrack2cn_DF1B
#define  INPUT_SDI_CT_TRXRES_DFA014_0  0x10  ///< B1b5: EMV_SDI_CT_TRANSRES_STRUCT::cardId_DFA014
/// @}


/// @brief EMV transaction (offline part including 1st cryptogram)
///
/// @author GSS R&D Germany
///
/// @param[in]   pxTransactionInput  Transaction data, see @ref EMV_CT_TRANSAC_STRUCT
/// @param[out]  pxTransRes          Transaction result data, see @ref EMV_CT_TRANSRES_STRUCT
/// @param[out]  pxSdiTransRes       Transaction result data, SDI specific, see @ref EMV_SDI_CT_TRANSRES_STRUCT
///
/// @return see @ref EMV_CT_ContinueOffline()
///
EMV_ADK_INFO EMV_SDI_CT_ContinueOffline(EMV_CT_TRANSAC_TYPE *pxTransactionInput, EMV_CT_TRANSRES_TYPE *pxTransRes, EMV_SDI_CT_TRANSRES_TYPE *pxSdiTransRes);


/// @brief EMV transaction (handling of host response including 2nd cryptogram)
///
/// @author GSS R&D Germany
///
/// @param[in]   pxOnlineInput  Data of host response, see @ref EMV_CT_HOST_STRUCT
/// @param[out]  pxTransRes     Result data, see @ref EMV_CT_TRANSRES_STRUCT
/// @param[out]  pxSdiTransRes  Transaction result data, SDI specific, see @ref EMV_SDI_CT_TRANSRES_STRUCT
///
/// @return see @ref EMV_CT_ContinueOnline()
///
EMV_ADK_INFO EMV_SDI_CT_ContinueOnline(EMV_CT_HOST_TYPE *pxOnlineInput, EMV_CT_TRANSRES_TYPE *pxTransRes, EMV_SDI_CT_TRANSRES_TYPE *pxSdiTransRes);



/// @brief Data structure for output data of EMV_SDI_CTLS_ContinueOffline() and EMV_SDI_CTLS_ContinueOnline()
///
typedef struct EMV_SDI_CTLS_TRANSRES_STRUCT
{
    unsigned char              IncludedData_DF61[8];   ///< Availability bits, see @ref DEF_INPUT_SDI_CTLS_TRANSRES
    char                   obfuscatedPANstr_DF44[20];  ///< Application Primary Account Number (PAN), obfuscated, presented in ASCII format @n Availability bit #INPUT_SDI_CTLS_TRXRES_DF44_0
    unsigned char           obfuscatedPANcn_DF1A[10];  ///< Application Primary Account Number (PAN), obfuscated, presented in cn (compressed numeric) format @n Availability bit #INPUT_SDI_CTLS_TRXRES_DF1A_0
    char                obfuscatedTrack2str_DF49[39];  ///< Track 2 Equivalent Data, obfuscated, presented in ASCII format @n Availability bit #INPUT_SDI_CTLS_TRXRES_DF49_0
    EMV_CTLS_TRACK2_TYPE obfuscatedTrack2cn_DF1B;      ///< Track 2 Equivalent Data, obfuscated, presented in cn (compressed numeric) format @n Availability bit #INPUT_SDI_CTLS_TRXRES_DF1B_0
    unsigned char                  cardId_DFA014[20];  ///< Card Id token @n Availability bit #INPUT_SDI_CTLS_TRXRES_DFA014_0
} EMV_SDI_CTLS_TRANSRES_TYPE;

/// @defgroup DEF_INPUT_SDI_CTLS_TRANSRES SDI SDI transaction results: Availability bits
/// @{
  /* Byte 1 (Index 0) */
#define  INPUT_SDI_CTLS_TRXRES_DF44_0    0x01  ///< B1b1: EMV_SDI_CTLS_TRANSRES_STRUCT::obfuscatedPANhex_DF44
#define  INPUT_SDI_CTLS_TRXRES_DF1A_0    0x02  ///< B1b2: EMV_SDI_CTLS_TRANSRES_STRUCT::obfuscatedPANcn_DF1A
#define  INPUT_SDI_CTLS_TRXRES_DF49_0    0x04  ///< B1b3: EMV_SDI_CTLS_TRANSRES_STRUCT::obfuscatedTrack2hex_DF49
#define  INPUT_SDI_CTLS_TRXRES_DF1B_0    0x08  ///< B1b4: EMV_SDI_CTLS_TRANSRES_STRUCT::obfuscatedTrack2cn_DF1B
#define  INPUT_SDI_CTLS_TRXRES_DFA014_0  0x10  ///< B1b5: EMV_SDI_CTLS_TRANSRES_STRUCT::cardId_DFA014
/// @}


/// @brief EMV transaction (offline part including 1st cryptogram)
///
/// @author GSS R&D Germany
///
/// @param[in]   pxTransactionInput  Transaction data, see @ref EMV_CT_TRANSAC_STRUCT
/// @param[out]  pxTransRes          Transaction result data, see @ref EMV_CT_TRANSRES_STRUCT
/// @param[out]  pxSdiTransRes       Transaction result data, SDI specific, see @ref EMV_SDI_CT_TRANSRES_STRUCT
///
/// @return see @ref EMV_CT_ContinueOffline()
///
EMV_ADK_INFO EMV_SDI_CTLS_ContinueOffline(EMV_CTLS_TRANSRES_TYPE *pxTransRes, EMV_SDI_CTLS_TRANSRES_TYPE *pxSdiTransRes);
EMV_ADK_INFO EMV_SDI_CTLS_ContinueOfflineExt(EMV_CTLS_CONT_OFFL_TYPE *pxContOfflInput, EMV_CTLS_TRANSRES_TYPE *pxTransRes, EMV_SDI_CTLS_TRANSRES_TYPE *pxSdiTransRes);


/// @brief EMV transaction (handling of host response including 2nd cryptogram)
///
/// @author GSS R&D Germany
///
/// @param[in]   pxOnlineInput  Data of host response, see @ref EMV_CT_HOST_STRUCT
/// @param[out]  pxTransRes     Result data, see @ref EMV_CT_TRANSRES_STRUCT
/// @param[out]  pxSdiTransRes  Transaction result data, SDI specific, see @ref EMV_SDI_CT_TRANSRES_STRUCT
///
/// @return see @ref EMV_CTLS_ContinueOnline()
///
EMV_ADK_INFO EMV_SDI_CTLS_ContinueOnline(EMV_CTLS_HOST_TYPE *pxOnlineInput, EMV_CTLS_TRANSRES_TYPE *pxTransRes, EMV_SDI_CTLS_TRANSRES_TYPE *pxSdiTransRes);


#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _EMV_SDI_INTERFACE_H_
