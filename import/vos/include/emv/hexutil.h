#ifndef HEXUTIL_H
#define HEXUTIL_H

/*****************************************************************************
* Thales e-Transactions GmbH
******************************************************************************
* Internal ECR-Interface
******************************************************************************
*
* $Workfile:   hexutil.h  $                      $Date:   19 Mar 2007 10:28:54  $
* $Revision:   1.0  $                      $Modtime:   15 Mar 2007 16:00:46  $
* $Archive:   U:/ArcTRM/Terminals/archives/ARCHIVES/Headerdateien/hexutil.h-arc  $
*
*****************************************************************************/
/** @file hexutil.h
 *
 * Conversion functions Hexdump <-> Binary
 *
 * @author:      M. Meixner
 * @date Created:  09.09.2005
 */
/*****************************************************************************
*  (c) Thales e-Transactions GmbH, Bad Hersfeld
*****************************************************************************/
/*HOC************************************************************************
* REVISIONS HISTORY OF CHANGE:
*----------------------------------------------------------------------------
 $Log:   U:/ArcTRM/Terminals/archives/ARCHIVES/Headerdateien/hexutil.h-arc  $
 * 
 *    Rev 1.0   19 Mar 2007 10:28:54   Groennert
 * Initial revision.

****************************************************************************/

/** convert a byte buffer to unsigned long
 * \param[in] bytes buffer of 4 bytes length in network byte order (const unsigned char[4])
 * \return converted integer of type "unsigned" for assigning to "unsigned long" on 32 bit and 64 bit architecture
 */
#define TLVUTIL_4BYTE_TO_UINT32(bytes)(((unsigned) (bytes)[0] << 24) | ((unsigned) (bytes)[1] << 16) | ((unsigned) (bytes)[2] << 8) | (unsigned) (bytes)[3])

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _VRXEVO
#   ifdef TLV_UTIL_SHARED_EXPORT
#       define TLV_UTIL_INTERFACE __declspec(dllexport) // used for VSL symbol export
#   elif defined TLV_UTIL_STATIC_EXPORT
#       define TLV_UTIL_INTERFACE                       // used for static library
#   else
#       define TLV_UTIL_INTERFACE __declspec(dllimport) // used for VSA symbol import (also used for static linking)
#   endif
#else
#   define TLV_UTIL_INTERFACE // NOTE: Do not set visibility("default") because export.map is used
#endif

/** convert an ASCII-HEX buffer to binary
 * \param[out] dst destination buffer of size srclen/2
 * \param[in] src source buffer
 * \param[in] srclen length of source data, must be even! 
 * \return 1 if successful, 0 if the input contained invalid characters
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 * @deprecated Use prefixed version TLVUTIL_hextobin or TLVUTIL_hexstringtobin instead
 */
TLV_UTIL_INTERFACE int hextobin(unsigned char *dst, const unsigned char *src, int srclen) __attribute ((deprecated));

/** convert an ASCII-HEX buffer to binary
 * \param[out] dst destination buffer of size srclen/2
 * \param[in] src source buffer
 * \param[in] srclen length of source data, must be even!
 * \return 1 if successful, 0 if the input contained invalid characters
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 */
TLV_UTIL_INTERFACE int TLVUTIL_hextobin(unsigned char *dst, const unsigned char *src, int srclen);

/** convert an ASCII-HEX buffer to binary
 * \param[out] dst destination buffer
 * \param[in] dstLength desitination buffer length
 * \param[in] src source buffer with terminated string containing HEX ASCII (blanks between bytes ignored)
 * \return length of binary data up to dstLength
 * \note src and dest may be identical
 */
TLV_UTIL_INTERFACE int TLVUTIL_hexstringtobin(unsigned char *dst, int dstLength, const unsigned char *src);

/** convert a binary buffer to ASCII-HEX representation (0-9,A-F)
 * \param[out] dst destination buffer of size 2*srclen+1
 * \param[in] src source buffer
 * \param[in] srclen length of source data 
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 * @deprecated Use prefixed version TLVUTIL_bintohexstring instead
 */
TLV_UTIL_INTERFACE void bintohex(unsigned char *dst, const unsigned char *src, int srclen) __attribute ((deprecated));

/** convert a binary buffer to ASCII-HEX representation (0-9,A-F)
 * \param[out] dst destination buffer of size 2*srclen+1
 * \param[in] src source buffer
 * \param[in] srclen length of source data
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 */
TLV_UTIL_INTERFACE void TLVUTIL_bintohexstring(unsigned char *dst, const unsigned char *src, int srclen);

/** convert a binary buffer to ASCII-HEX representation (0-9,A-F)
 * Please note: A string termination is not appended.
 * \param[out] dst destination buffer of size 2*srclen
 * \param[in] src source buffer
 * \param[in] srclen length of source data
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 */
TLV_UTIL_INTERFACE void TLVUTIL_bintohex(unsigned char *dst, const unsigned char *src, int srclen);


#ifdef __cplusplus
}
#endif

#ifndef TLV_UTIL_COMPILE
#   undef TLV_UTIL_INTERFACE
#endif

#endif
