#ifndef GUI_ERROR_H_2013_03_22
#define GUI_ERROR_H_2013_03_22

/** \file gui_error.h
 * \addtogroup vfigui Graphical User Interface
 * \{
 */

/** Verifone GUI namespace */
#ifdef __cplusplus
#ifndef DOXYGEN
namespace vfigui {
#endif
#endif

/** error codes returned by the functions */
enum UIError{
  // general errors
  UI_ERR_OK                 =  0,  /**< no error */
  UI_ERR_ABORT              = -1,  /**< abort by user */
  UI_ERR_BACK               = -2,  /**< user selected back button */
  UI_ERR_TIMEOUT            = -3,  /**< dialog timeout */
  UI_ERR_PROPERTY           = -4,  /**< the property does not exist */
  UI_ERR_WRONG_PIN          = -5,  /**< wrong PIN was entered */
  UI_ERR_PARAMETER          = -6,  /**< parameter error */
  UI_ERR_PERMISSION         = -7,  /**< insufficient permissions */
  UI_ERR_CANCELLED          = -8,  /**< the dialog has been cancelled by callback or by displaying another dialog */
  UI_ERR_REGION             = -9,  /**< the region is not known */
  UI_ERR_FAIL               = -10, /**< generic error */
  UI_ERR_UNSUPPORTED        = -11, /**< the function requested is not supported on the current platform/device/... */
  UI_ERR_INVALID            = -12, /**< an invalid transaction ID was provided */
  UI_ERR_WAIT_TIMEOUT       = -13, /**< timeout while waiting for the result of an async function */
  UI_ERR_CONNECTION_LOST    = -14, /**< cannot connect or lost connection to server */
  UI_ERR_PROTOCOL           = -15, /**< data received violated the protocol */
  UI_ERR_SCRIPT             = -16, /**< error occurred during script processing, check uiScriptError() for more information about the error */
  UI_ERR_FILE_READ          = -17, /**< the file was not found or could not be read */
  UI_ERR_RESTRICTED         = -18, /**< the dialog was not shown due to security restrictions, e.g. using more than 3 buttons */
  UI_ERR_MODIFIED           = -19, /**< the old layout does not match */
  UI_ERR_ACTION             = -20, /**< invalid action provided */
  UI_ERR_DISPLAY            = -21, /**< invalid display */
  UI_ERR_PIN_ENTRY_DENIED   = -22  /**< requesting too many PIN sessions in a short period of time. */
};


#ifdef __cplusplus
#ifndef DOXYGEN
}
#endif
#endif

/** \} */

#endif
