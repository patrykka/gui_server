// -*- Mode: C++; -*-
#ifndef GUI_TEMPLATE_H_2014_11_17
#define GUI_TEMPLATE_H_2014_11_17

/** \file gui_template.h
 * \addtogroup vfigui Graphical User Interface
 * \{
 */

#include "gui.h"

#if defined _WIN32 && defined VFI_GUIPRT_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#elif defined __GNUC__ && defined VFI_GUIPRT_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

/** Verifone GUI namespace */
#ifndef DOXYGEN
namespace vfigui {
#endif
#if 0
} // just to satisfy automatic indentation of the editor
#endif


/** menu options */
enum UIMenuOptions {
   UI_MENU_DISABLED=1 /**< show the menu entry as disabled or leave it out (depending on device) */
};

/** Display an HTML document on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted
 *                         into the template
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \return error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 */
DllSpec int uiDisplay(int display, int region_id,const std::string &templateName, const std::string &text);
DllSpec int uiDisplay(int region_id,const std::string &templateName, const std::string &text);
inline int uiDisplay(const std::string &templateName, const std::string &text)
{ return uiDisplay(0,UI_REGION_DEFAULT,templateName,text); }


/** Asynchronously display an HTML document on screen
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return transaction ID or error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note use uiDisplayWait() to wait for the dialog to finish
 *       or use uiInvokeCancel() to cancel the dialog
 */
DllSpec int uiDisplayAsync(int display,int region_id, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
DllSpec int uiDisplayAsync(int region_id, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
inline int uiDisplayAsync(const std::string &text, uiAsyncCallback cb=0, void *data=0 )
{ return uiDisplayAsync(0,UI_REGION_DEFAULT,text,cb,data); }

/** Asynchronously display an HTML document on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted
 *                         into the template
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return transaction ID or error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note use uiDisplayWait() to wait for the dialog to finish
 *       or use uiInvokeCancl() to cancel the dialog
 */
DllSpec int uiDisplayAsync(int display, int region_id, const std::string &templateName, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
DllSpec int uiDisplayAsync(int region_id, const std::string &templateName, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
inline int uiDisplayAsync(const std::string &templateName, const std::string &text, uiAsyncCallback cb=0, void *data=0 )
{ return uiDisplayAsync(0,UI_REGION_DEFAULT,templateName,text,cb,data); }


/** Wait for the result of the display
 * \param[in] txn_id transaction id as returned by uiDisplayAsync()
 * \param[in] timeout_msec timeout in milliseconds. In case of a negative timeout wait forever.
 * \return error code (see ::UIError) or return value specified in the HTML fragment. In case of
 * a timeout UI_ERR_WAIT_TIMEOUT is returned and uiDisplayWait() may be called again.
 */
DllSpec int uiDisplayWait(int txn_id, int timeout_msec=-1);

/** Display an HTML confirmation dialog on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted
 *                         into the template
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or return value specified in the template
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 */
DllSpec int uiConfirm(int display, int region_id, const std::string &templateName, const std::string &text, uiCallback cb=0, void *cbdata=0);
DllSpec int uiConfirm(int region_id, const std::string &templateName, const std::string &text, uiCallback cb=0, void *cbdata=0);
inline int uiConfirm(const std::string &templateName, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ return uiConfirm(0,UI_REGION_DEFAULT,templateName,text,cb,cbdata); }

/** Asynchronously display an HTML confirmation dialog on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted
 *                         into the template
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return transaction ID or error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note use uiConfirmWait() to wait for the dialog to finish
 *       or use uiInvokeCancel() to cancel the dialog
 */
DllSpec int uiConfirmAsync(int display,int region_id, const std::string &templateName, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
DllSpec int uiConfirmAsync(int region_id, const std::string &templateName, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
inline int uiConfirmAsync(const std::string &templateName, const std::string &text, uiAsyncCallback cb=0, void *data=0 )
{ return uiConfirmAsync(0,UI_REGION_DEFAULT, templateName, text,cb,data); }

/** Wait for the confirm dialog to finish
 * \param[in] txn_id transaction id as returned by uiConfirmAsync()
 * \param[in] timeout_msec timeout in milliseconds. In case of a negative timeout wait forever.
 * \return error code (see ::UIError) or return value specified in the HTML fragment. In case of
 * a timeout UI_ERR_WAIT_TIMEOUT is returned and uiConfirmWait() may be called again.
 */
DllSpec int uiConfirmWait(int txn_id, int timeout_msec=-1);

/** Display an HTML input dialog on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted
 *                         into the template
 * \param[in,out] value initial value and returned value of the input fields. The first value
 *                      refers to the first input field, the second to the second, etc.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 *                 The input fields have to be specified as HTML input fields
 *                 (e.g. &lt;input type='text' ...>). The following input types are supported:
 *                   - \b text: text input
 *                   - \b password: password input
 *                   - \b checkbox: checkbox
 *                   - \b number: numeric input (HTML extension)
 *                   - \b mask: masked input (HTML extension)
 *                   - \b pin: PIN input
 *                   .
 *                 The following attributes are supported:
 *                   - \b allowed_chars: list of allowed characters
 *                   - \b mask: input mask (HTML extension): '*' is used as place holder for
 *                              filling in characters
 *                   - \b maxlengh: maximum input length
 *                   - \b name: name of the input field
 *                   - \b precision: number of decimal places
 *                   - \b size: size of the input field in characters for display on screen
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or return value specified in the template
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note PIN input cannot be combined with other input types since the keyboard is exclusively
 *       locked by the PIN entry function. In case of PIN entry the value returned in \a value
 *       is the number of digits entered or "error" in case of an error, "cancel" if the user
 *       pressed CANCEL, "clear" if the user pressed CLEAR, "bypass" in case of PIN bypass
 *       and "timeout" in case of timeout.
 * \note Radio buttons will not behave as expected since these use more than one input element
 *       to represent a single input value.
 */
DllSpec int uiInput(int display, int region_id, const std::string &templateName, std::vector<std::string> &value, const std::string &text, uiCallback cb=0, void *cbdata=0);
DllSpec int uiInput(int region_id, const std::string &templateName, std::vector<std::string> &value, const std::string &text, uiCallback cb=0, void *cbdata=0);
inline int uiInput(const std::string &templateName, std::vector<std::string> &value, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ return uiInput(0,UI_REGION_DEFAULT,templateName,value,text,cb,cbdata); }

/** Asynchronously display an HTML input dialog on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted into the template
 * \param[in] value initial value of the input fields. The first value refers to the first
 *                      input field, the second to the second, etc.
 * \param[in] text string containing an HTML fragment
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return transaction ID or error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note see also uiInput()
 * \note use uiInputWait() to wait for the dialog to finish
 *       or use uiInvokeCancel() to cancel the dialog
 */
DllSpec int uiInputAsync(int display, int region_id, const std::string &templateName, const std::vector<std::string> &value, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
DllSpec int uiInputAsync(int region_id, const std::string &templateName, const std::vector<std::string> &value, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
inline int uiInputAsync(const std::string &templateName, const std::vector<std::string> &value, const std::string &text, uiAsyncCallback cb=0, void *data=0 )
{ return uiInputAsync(0,UI_REGION_DEFAULT, templateName, value, text,cb,data); }

/** Wait for the input dialog to finish
 * \param[in] txn_id transaction id as returned by uiInputAsync()
 * \param[in] value returned value of the input fields. The first value refers to the first
 *                      input field, the second to the second, etc.
 * \param[in] timeout_msec timeout in milliseconds. In case of a negative timeout wait forever.
 * \return error code (see ::UIError) or return value specified in the HTML fragment. In case of
 * a timeout UI_ERR_WAIT_TIMEOUT is returned and uiInputWait() may be called again.
 */
DllSpec int uiInputWait(int txn_id, std::vector<std::string> &value, int timeout_msec=-1);


/** Display a PIN check dialog: The entered PIN is checkec against a known PIN
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted into the template
 * \param[in] referencePIN reference PIN
 * \param[in] text string containing an HTML fragment
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) (UI_ERR_WRON_PIN in case of a wrong PIN, UI_ERR_OK in case the PIN was OK)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 */
DllSpec int uiInputPINCheck(int display, int region_id, const std::string &templateName, const std::string &referencePIN, const std::string &text, uiCallback cb=0, void *cbdata=0);
DllSpec int uiInputPINCheck(int region_id, const std::string &templateName, const std::string &referencePIN, const std::string &text, uiCallback cb=0, void *cbdata=0);
inline int uiInputPINCheck(const std::string &templateName, const std::string &referencePIN, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ return uiInputPINCheck(0,UI_REGION_DEFAULT,templateName,referencePIN,text,cb,cbdata); }


/** Asynchronously display a PIN check dialog: The entered PIN is checkec against a known PIN
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted into the template
 * \param[in] text string containing an HTML fragment
 * \param[in] referencePIN reference PIN
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return transaction ID or error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note use uiInputPINCheckWait() to wait for the dialog to finish
 *       or use uiInvokeCancel() to cancel the dialog
 */
DllSpec int uiInputPINCheckAsync(int display, int region_id, const std::string &templateName, const std::string &referencePIN, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
DllSpec int uiInputPINCheckAsync(int region_id, const std::string &templateName, const std::string &referencePIN, const std::string &text, uiAsyncCallback cb=0, void *data=0 );
inline int uiInputPINCheckAsync(const std::string &templateName, const std::string &referencePIN, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ return uiInputPINCheckAsync(0,UI_REGION_DEFAULT, templateName, referencePIN, text,cb,data); }

/** Wait for the PIN check dialog to finish
 * \param[in] txn_id transaction id as returned by uiInputPINCheckAsync()
 * \param[in] timeout_msec timeout in milliseconds. In case of a negative timeout wait forever.
 * \return error code (see ::UIError) or return value specified in the HTML fragment. In case of
 * a timeout UI_ERR_WAIT_TIMEOUT is returned and uiInputPINCheckWait() may be called again.
 * \note update events are currently not supported for uiInputPINCheckWait().
 */
DllSpec int uiInputPINCheckWait(int txn_id, int timeout_msec=-1);


/** structure for menu entries */
struct UIMenuEntry
{
   std::string text; /**< text to be displayed */
   int value;        /**< return value */
   unsigned options; /**< options, see vfigui::UIMenuOptions */
};


/** Display an HTML menu dialog on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted into the template
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and %lt;/body>).
 * \param[in] menu array containing menu entries
 * \param[in] menusize number of entries in the array
 * \param[in] preselect value of the menu entry to be preselected.
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or value of selected menu entry.
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 */
DllSpec int uiMenu(int display, int region_id, const std::string &templateName, const std::string &text, const struct UIMenuEntry menu[], int menusize, int preselect, uiCallback cb=0, void *cbdata=0);
DllSpec int uiMenu(int region_id, const std::string &templateName, const std::string &text, const struct UIMenuEntry menu[], int menusize, int preselect, uiCallback cb=0, void *cbdata=0);
inline int uiMenu(const std::string &templateName, const std::string &text, const struct UIMenuEntry menu[], int menusize, int preselect, uiCallback cb=0, void *cbdata=0)
{ return uiMenu(0,UI_REGION_DEFAULT,templateName,text,menu,menusize,preselect,cb,cbdata); }


/** Asynchronously display an HTML menu dialog on screen using a template file.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] templateName name of a template file. The HTML code found in \a text is inserted into the template
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] menu array containing menu entries
 * \param[in] menusize number of entries in the array
 * \param[in] preselect value of the menu entry to be preselected.
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return transaction ID or error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note use uiMenuWait() to wait for the dialog to finish
 *       or use uiInvokeCancel() to cancel the dialog
 */
DllSpec int uiMenuAsync(int display, int region_id, const std::string &templateName, const std::string &text, const struct UIMenuEntry menu[], int menusize, int preselect, uiAsyncCallback cb=0, void *data=0);
DllSpec int uiMenuAsync(int region_id, const std::string &templateName, const std::string &text, const struct UIMenuEntry menu[], int menusize, int preselect, uiAsyncCallback cb=0, void *data=0);
inline int uiMenuAsync(const std::string &template_name, const std::string &text, const struct UIMenuEntry menu[], int menusize, int preselect, uiAsyncCallback cb=0, void *data=0)
{ return uiMenuAsync(0,UI_REGION_DEFAULT, template_name, text, menu, menusize, preselect,cb,data); }

/** Wait for the menu dialog to finish
 * \param[in] txn_id transaction id as returned by uiMenuAsync()
 * \param[in] timeout_msec timeout in milliseconds. In case of a negative timeout wait forever.
 * \return error code (see ::UIError) or value of selected menu entry. In case of
 * a timeout UI_ERR_WAIT_TIMEOUT is returned and uiMenuWait() may be called again.
 */
DllSpec int uiMenuWait(int txn_id, int timeout_msec=-1);


#ifndef DOXYGEN
} // namespace vfigui
#endif


/** \}*/

#undef DllSpec

#endif
