/**
 * @ingroup inf_util_backend_api
 * @file
 * @brief Common types used by inf_util
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <string>
#include <map>
#include <list>


/**
 * @brief Top-level operation codes
 * @ingroup inf_util_backend_api
 */
typedef enum
{
    INFO_OP_IMPORT,            ///< Import operation
    INFO_OP_EXPORT             ///< Export operation
} Operation;


#endif /* TYPES_H_ */
