/**
 * @defgroup propdb Property Database API
 * @brief Allows for easy to use hierarchical key/value storage.
 * @details
 * In order to use Property Database API one must
 * include @c <inf/infodb.h> and link against @c libinf.so or @c libinf-static.a depending on the chosen linkage model.
 * See also the following items:
 * - @ref sect-path.
 *
 */

/**
 * @file
 * @ingroup propdb
 * @brief Hierarchical Property Database API
 */

#ifndef INFODB_H_
#define INFODB_H_

#ifndef INF_API

#ifdef _VRXEVO
#   ifdef _INF_API_EXPORT
#       define INF_API __declspec(dllexport)
#   else
#       define INF_API __declspec(dllimport)
#   endif
#else
#   define INF_API
#endif
#endif

/**
 * @ingroup propdb
 * @brief property table name
 */
#define PROP_NAME   "registry"


#ifdef __cplusplus
extern "C"
{
#endif




/**
 * @ingroup propdb
 * @brief structure for returning the names of the direct child nodes
 */
struct InfoChildren
{
    int count;    ///< number of child nodes
    char **name;    ///< array containing the names of the direct child nodes
};


/**
 * @ingroup propdb
 * @brief Result codes
 */
enum InfoResult
{
    INFO_OK = 0,    ///< No error
    INFO_FAIL = -1,    ///< Generic operation failure
    INFO_NO_DB = -2,    ///< Database cannot be accessed
    INFO_NOT_FOUND = -3,    ///< Key was not found
    INFO_INVALID_PARAM = -4,    ///< Invalid values for function parameters
    INFO_BUSY = -5,    ///< The database is busy and cannot process the request within a reasonable timeframe
    INFO_MEM = -6,    ///< Out of memory
    INFO_BREAK = -7,    ///< The callback indicated to stop processing
    INFO_NODE_NAME_INVALID = -8,    ///< Node name is invalid (i.e. does not match [a-zA-Z0-9-_.@]

    INFO_NOT_IMPL = -900,    ///< Function is not implemented (but might be in the future)
    INFO_NOT_SUPP = -901    ///< Operation is not (and never will be) supported
};

/**
 * @ingroup propdb
 * @brief Data types
 */
enum InfoType
{
    INFO_VOID,    ///< void, no value
    INFO_INTEGER,    ///< int
    INFO_NUMBER,    ///< decimal number
    INFO_STRING,    ///< string
    INFO_BINARY    ///< memory chunk
};

/**
 * @ingroup propdb
 * @brief Database open modes
 */
enum InfoMode
{
    INFO_READ_ONLY,    ///< read-only access to database
    INFO_READ_WRITE    ///< read/write access to database
};

/**
 * @ingroup propdb
 * @brief Additional options
 */
enum InfoOption
{
    INFO_OPT_DISABLE_AUTOCOMMIT = (0x1 << 0), ///< 0x1 bit disables autocommit for every info_write* operation
    INFO_OPT_MULTIPLE_ENTRIES   = (0x1 << 1)  ///< 0x2 bit allows inserting multiple entries for single key
};

/**
 * @ingroup propdb
 * @brief 64-bit ID referencing one node/key in the database, IDs are always positive numbers, #INFO_ROOT is the ID of the root node
 */
typedef long long InfoNode;

struct InfoDBHandle;
struct InfoReference;

/**
 * @ingroup propdb
 * @brief Database handle
 */
typedef struct InfoDBHandle InfoDB;

/**
 * @ingroup propdb
 */
enum
{
    INFO_ROOT = 0    ///< Dedicated root node of the hierarchical storage
};

/**
 * @name Functions
 */

/**
 * @ingroup propdb
 * @{
 */

/** \brief get version of a library
  * \return string with version
*/
INF_API const char *info_getVersion(void);

/**
 * @brief Open the database
 * @param[in] filename filename of the database
 * @param[in] mode read-only or read-write
 * @return database handle or @c NULL in case of error
 * @note If done the database has to be closed using info_closedb()
 */
INF_API InfoDB *info_opendb(const char *filename, enum InfoMode mode);

/**
 * @brief Close database and release allocated resources
 * @param[in] handle database handle obtained from info_opendb()
 * @note It is safe to call info_closedb(NULL);
 */
INF_API void info_closedb(InfoDB *handle);

/**
 * @brief Set additional options changing behavior of info_* functions.
 * @param[in] handle database handle obtained from info_opendb()
 * @param[in] value(s) specified in enum InfoOption. 0 - default behavior.
 * @return error code
 */
INF_API enum InfoResult info_setOptions(InfoDB *handle, int options);

/**
 * @brief Get current additional options changing behavior of info_* functions.
 * @param[in] handle database handle obtained from info_opendb()
 * @return current options. Negative value indicates error.
 */
INF_API int info_getOptions(InfoDB *handle);

/**
 * @brief Enable/disable automatic commit for every info_write* operation
 * @param[in] handle database handle obtained from info_opendb()
 * @param[in] enable 0 - disable autocommit, any other value enables autocommit
 * @note Default operation mode is enabled autocommit.
 * @note This function call is equivalent to info_setOptions function call with accordingly defined INFO_OPT_DISABLE_AUTOCOMMIT bit.
 * @return error code
 */
INF_API enum InfoResult info_autocommit(InfoDB *handle, int enable);

/**
 * @brief Save database state
 * @param[in] handle database handle obtained from info_opendb()
 */
INF_API enum InfoResult info_transactionBegin(InfoDB *idb);

/**
 * @brief Commit changes
 * @param[in] handle database handle obtained from info_opendb()
 */
INF_API enum InfoResult info_transactionCommit(InfoDB *idb);

/**
 * @brief Rollback changes
 * @param[in] handle database handle obtained from info_opendb()
 */
INF_API enum InfoResult info_transactionRollback(InfoDB *idb);

/**
 * @brief Find a node within the hierarchy and return its node ID.
 * @details
 * This id can be used as starting node for subsequent addressing within the database
 * (similar to the current working directory in a file system).
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @return Node ID (>=0) or error code (<0).
 */
INF_API InfoNode info_find(InfoDB *handle, InfoNode parent, const char *path);

/**
 * @brief Determine the type of a value as it has been stored in the database
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[out] type pointer for storing the type of the value in the database. In 
 *                   case no value was found it is set to #INFO_VOID.
 * @return error code
 */
INF_API enum InfoResult info_getType(InfoDB *handle, InfoNode parent, const char *path, enum InfoType *type);

/**
 * @brief Read value from the database and convert it to int, if required
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[out] value pointer for storing the number that has been read from the 
 *                   database. In case no value was found it remains unchanged.
 * @return error code
 */
INF_API enum InfoResult info_readInt(InfoDB *handle, InfoNode parent, const char *path, int *value);

/**
 * @brief Read value from the database and convert it to unsigned int, if required
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[out] value pointer for storing the number that has been read from the
 *                   database. In case no value was found it remains unchanged.
 * @return error code
 */
INF_API enum InfoResult info_readUnsignedInt(InfoDB *handle, InfoNode parent, const char *path, unsigned int *value);

/**
 * @brief Read value from the database and convert it to double, if required
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[out] value pointer for storing the number that has been read from the 
 *                   database. In case no value was found it remains unchanged.
 * @return error code
 */
INF_API enum InfoResult info_readDouble(InfoDB *handle, InfoNode parent, const char *path, double *value);

/**
 * @brief Read value from the database and convert it to a string, if required
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[out] value pointer for storing the string. The string is returned as C-string in memory that
 *                   has been allocated using malloc(), the returned string has to be released
 *                   using free() when done. In case no value was found @c NULL is returned.
 * @return error code
 */
INF_API enum InfoResult info_readString(InfoDB *handle, InfoNode parent, const char *path, char **value);

/**
 * @brief Read value from the database as chunk of memory
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[out] value pointer for storing the chunk of memory. The memory 
 *                   has been allocated using malloc(), it has to be released
 *                   using free() when done. In case no value was found or in case @a size is 0 then @c NULL is returned.
 * @param[out] size size of the returned memory. In case no value was found 0 is returned.
 * @return error code
 */
INF_API enum InfoResult info_readBinary(InfoDB *handle, InfoNode parent, const char *path, void **value, int *size);

////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Write int value to the database
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path. If the key does not exist yet, it is created. 
 *                   Parent nodes are created as required.
 * @param[in] value int value to be written.
 * @return error code
 */
INF_API enum InfoResult info_writeInt(InfoDB *handle, InfoNode parent, const char *path, int value);


/**
 * @brief Write unsigned int value to the database
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path. If the key does not exist yet, it is created.
 *                   Parent nodes are created as required.
 * @param[in] value unsigned int value to be written.
 * @return error code
 */
INF_API enum InfoResult info_writeUnsignedInt(InfoDB *handle, InfoNode parent, const char *path, unsigned int value);


/**
 * @brief Write double value to the database
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path. If the key does not exist yet, it is created. 
 *                   Parent nodes are created as required.
 * @param[in] value double value to be written.
 * @return error code
 */
INF_API enum InfoResult info_writeDouble(InfoDB *handle, InfoNode parent, const char *path, double value);

/**
 * @brief Write string value to the database
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path. If the key does not exist yet, it is created. 
 *                   Parent nodes are created as required.
 * @param[in] value C-string to be written, may be @c NULL, in this case an empty value of type #INFO_VOID is written
 * @return error code
 */
INF_API enum InfoResult info_writeString(InfoDB *handle, InfoNode parent, const char *path, const char *value);

/**
 * @brief Write value from the database as chunk of memory
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path. If the key does not exist yet, it is created. 
 *                   Parent nodes are created as required.
 * @param[in] value pointer to memory region to be written, may be @c NULL, in this case an empty value of type #INFO_VOID is written
 * @param[in] size number of bytes to be written
 * @return error code
 */
INF_API enum InfoResult info_writeBinary(InfoDB *handle, InfoNode parent, const char *path, const void *value,
                                             int size);

////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Delete a node and all of its child nodes from the database
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path. If the key does not exist yet, it is created. 
 *                   Parent nodes are created as required.
 * @return error code
 */
INF_API enum InfoResult info_delete(InfoDB *handle, InfoNode parent, const char *path);

////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief Obtain a list of the names of all direct child nodes of a node
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[in] list pointer to list of child names that has been allocated using malloc().
 *                 All information is stored in a single block of memory. Use free() to 
 *                 release when done. 
 * @return error code
 * @note Names in the result are similar to strcmp but with special handling of contained numbers: 
 * These are treated as single character and compared by value, i.e. sorting "z","n2","n10","n1","a" would
 * be listed as "a","n1","n2","n10","z" ("n2" is listed before "n10" since 2<10)
 */
INF_API enum InfoResult info_list(InfoDB *handle, InfoNode parent, const char *path, struct InfoChildren **list);

/**
 * @brief Atomically increment an integer by some delta
 * @param[in] handle database handle
 * @param[in] parent parent node used as starting point for the path. Use #INFO_ROOT
 *                   to address relative to the root of the tree.
 * @param[in] path path see @ref sect-path
 * @param[in] delta amount by which to increment the value
 * @param[out] value pointer for resulting value after increment, if @c NULL no value will be returned.
 * @return error code
 * @note If the value does not exist it is created with a default value of 0, i.e. 
 * it will have the value @a delta after incrementing.
 */
INF_API enum InfoResult info_incrementInt(InfoDB *handle, InfoNode parent, const char *path, int delta, int *value);

/**
 * @brief Determine the absolute path of a node
 * @param[in] handle database handle
 * @param[in] node parent node for which to determine the path
 * @param[out] path absolute path of the node as C-string that has been allocated using malloc().
 *                  Use free() to release it when not needed any longer. In case of error @c NULL
 *                  is returned.
 * @return error code
 */
INF_API enum InfoResult info_getPath(InfoDB *handle, InfoNode node, char **path);

/**
 * @brief Recursively walk over the nodes of the (sub-) tree and invoke the callback for each node.
 * @param[in] handle database handle;
 * @param[in] parent parent node of the (sub-) tree
 * @param[in] path path see @ref sect-path
 * @param[in] cb callback that will be invoked for node @a parent/path and all of its descendant nodes. The callback
 *               is invoked with the path and a handle to read the type and value stored in that node.
 * @param[in] data data pointer that is passed on to the callback
 * @return error code
 */
INF_API enum InfoResult info_walkTree(InfoDB *handle, InfoNode parent, const char *path,
                                          int(*cb)(void *data, const char *path, struct InfoReference *info),
                                          void *data);

/**
 * @brief Determine type of a node
 * @param[in] info handle provided to callback function
 * @param[out] type pointer for storing the type of the value in the database. In 
 *                   case no value was found it is set to #INFO_VOID.
 * @return error code
 */
INF_API enum InfoResult info_nodeGetType(struct InfoReference *info, enum InfoType *type);

/**
 * @brief Read value from the database and convert it to int, if required
 * @param[in] info handle provided to callback function
 * @param[out] value pointer for storing the number that has been read from the 
 *                   database. In case no value was found it remains unchanged.
 * @return error code
 */
INF_API enum InfoResult info_nodeReadInt(struct InfoReference *info, int *value);

/**
 * @brief Read value from the database and convert it to double, if required
 * @param[in] info handle provided to callback function
 * @param[out] value pointer for storing the number that has been read from the 
 *                   database. In case no value was found it remains unchanged.
 * @return error code
 */
INF_API enum InfoResult info_nodeReadDouble(struct InfoReference *info, double *value);

/**
 * @brief Read value from the database and convert it to a string, if required
 * @param[in] info handle provided to callback function
 * @param[out] value pointer for storing the string. The string is returned as C-string in memory that
 *                   has been allocated using malloc(), the returned string has to be released
 *                   using free() when done. In case no value was found @c NULL is returned.
 * @return error code
 */
INF_API enum InfoResult info_nodeReadString(struct InfoReference *info, char **value);

/**
 * @brief Read value from the database as chunk of memory
 * @param[in] info handle provided to callback function
 * @param[out] value pointer for storing the chunk of memory. The memory 
 *                   has been allocated using malloc(), it has to be released
 *                   using free() when done. In case no value was found or in case @a size is 0 then @c NULL is returned.
 * @param[out] size size of the returned memory. In case no value was found 0 is returned.
 * @return error code
 */
INF_API enum InfoResult info_nodeReadBinary(struct InfoReference *info, void **value, int *size);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif // INFODB_H_
