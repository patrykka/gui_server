/******************************************************************************/
/*                                                                            */
/* dl_iso8583_defs_2003.h - ISO8583 2003 Definitions                          */
/*                                                                            */
/******************************************************************************/

#ifndef __INC_DL_ISO8583_DEFS_2003
#define __INC_DL_ISO8583_DEFS_2003

#include "../dl_base.h"
#include "../dl_iso8583_common.h"

#ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef ISO_SHARED_EXPORT
      #define ISO_EXP_DECL __declspec(dllexport) // used for VSL symbol export
    #elif ISO_STATIC_EXPORT
      #define ISO_EXP_DECL // used for static libraries
    #else
      #define ISO_EXP_DECL __declspec(dllimport) // used for VSA symbol import (also used for static linking)
    #endif
  #elif defined __GNUC__ && defined ISO_SHARED_EXPORT
    #define ISO_EXP_DECL __attribute__((visibility("default")))
  #else
    #define ISO_EXP_DECL // used for both, static libraries and program symbol import
  #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************/

// sets the specified handler
ISO_EXP_DECL void DL_ISO8583_DEFS_2003_GetHandler ( DL_ISO8583_HANDLER *oHandler );

/******************************************************************************/

#ifdef __cplusplus
}  /* end of the 'extern "C"' block */
#endif

#endif /* __INC_DL_ISO8583_DEFS_2003 */
