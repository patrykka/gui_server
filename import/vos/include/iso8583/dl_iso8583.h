/******************************************************************************/
/*                                                                            */
/* Copyright (C) 2005-2007 Oscar Sanderson                                    */
/*                                                                            */
/* This software is provided 'as-is', without any express or implied          */
/* warranty.  In no event will the author(s) be held liable for any damages   */
/* arising from the use of this software.                                     */
/*                                                                            */
/* Permission is granted to anyone to use this software for any purpose,      */
/* including commercial applications, and to alter it and redistribute it     */
/* freely, subject to the following restrictions:                             */
/*                                                                            */
/* 1. The origin of this software must not be misrepresented; you must not    */
/*    claim that you wrote the original software. If you use this software    */
/*    in a product, an acknowledgment in the product documentation would be   */
/*    appreciated but is not required.                                        */
/*                                                                            */
/* 2. Altered source versions must be plainly marked as such, and must not be */
/*    misrepresented as being the original software.                          */
/*                                                                            */
/* 3. This notice may not be removed or altered from any source distribution. */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* dl_iso8583.c - An implementation of the ISO-8583 message protocol          */
/*                                                                            */
/******************************************************************************/

#ifndef __INC_DL_ISO8583
#define __INC_DL_ISO8583

#include "dl_base.h"
#include "dl_err.h"
#include "dl_str.h"

#include "ConstData.h"
#include "SafeBuffer.hpp"

#include "dl_iso8583_common.h"
#include "dl_iso8583_fields.h"



#ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef ISO_SHARED_EXPORT
      #define ISO_EXP_DECL __declspec(dllexport) // used for VSL symbol export
    #elif ISO_STATIC_EXPORT
      #define ISO_EXP_DECL // used for static libraries
    #else
      #define ISO_EXP_DECL __declspec(dllimport) // used for VSA symbol import (also used for static linking)
    #endif
  #elif defined __GNUC__ && defined ISO_SHARED_EXPORT
    #define ISO_EXP_DECL __attribute__((visibility("default")))
  #else
    #define ISO_EXP_DECL // used for both, static libraries and program symbol import
  #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * This class allows building and parsing ISO8583 messages
 */
class ISO_EXP_DECL ISO8583_MSG
{
public:
    /******************************************************************************/

/**
 * constructor
 * \param[in] iMsgHandler: pointer to message handler instance
 */
    ISO8583_MSG ( DL_ISO8583_MSG_HANDLER *iMsgHandler,
                  DL_UINT8       *_iStaticBuf = NULL,
                  DL_UINT16       _iStaticBufSize = 0);

/**
 * destructor
 */
    virtual ~ISO8583_MSG ( );

    /******************************************************************************/

/**
 * allocates and sets the specified field
 * \param[in] iField: field index, range is 0..kDL_ISO8583_MAX_FIELD_IDX
 * \param[in] iDataStr: string containing data to be set
 * \returns error code
 */
    DL_ERR SetField_Str ( DL_UINT16       iField,
                          const DL_SINT8 *iDataStr );

/**
 * allocates and sets the specified field
 * \param[in] iField: field index, range is 0..kDL_ISO8583_MAX_FIELD_IDX
 * \param[in] _iData: buffer with data to be set, may be NULL
 * \param[in] _iDataLen: length of _iData
 * \returns error code
 */
    DL_ERR SetField_Bin ( DL_UINT16       iField,
                          const DL_UINT8 *_iData,
                          DL_UINT16       _iDataLen );

/**
 * allocates and sets the specified fields
 * \param[in] tlvFields: buffer with TLV data
 * \returns error code
 */
    DL_ERR SetFields_TLV ( com_verifone_TLVLite::ConstData_t tlvFields );
                          
    /******************************************************************************/

/**
 * check if field is set
 * \param[in] iField: field index, range is 0..kDL_ISO8583_MAX_FIELD_IDX
 * \returns 1 if the field is set, otherwise 0
 */
    int HaveField ( DL_UINT16             iField);

    /******************************************************************************/

/**
 * get data from message
 * \param[in] iField: field index, range is 0..kDL_ISO8583_MAX_FIELD_IDX
 * \param[out] oPtr: static pointer to field data
 * \returns error code
 */
    DL_ERR GetField_Str ( DL_UINT16              iField,
                          DL_UINT8             **oPtr );

/**
 * get data from message
 * \param[in] iField: field index, range is 0..kDL_ISO8583_MAX_FIELD_IDX
 * \param[out] oPtr: static pointer to field data
 * \param[out] oByteLen: length of field data
 * \returns error code
 */
    DL_ERR GetField_Bin ( DL_UINT16              iField,
                          DL_UINT8             **oPtr,
                          DL_UINT16             *oByteLen );

/**
 * get data in TLV format
 * \param[out] tlvFields: buffer to store TLV data
 * \returns error code
 */
    DL_ERR GetFields_TLV ( com_verifone_TLVLite::SafeBuffer & tlvFields );
                          
    /******************************************************************************/

/**
 * pack ISO message
 * \param[out] ioByteArr: buffer to store message data
 * \param[in] ioByteArrSize: size of buffer ioByteArr
 * \param[out] oNumBytes: length of stored message
 * \param[out] errFieldIdx: in case of error: contains the field index that caused the error
 * \returns error code
 */
    DL_ERR Pack ( DL_UINT8                 *ioByteArr,
                  DL_UINT16                ioByteArrSize,
                  DL_UINT16                *oNumBytes,
                  DL_UINT16                *errFieldIdx=NULL );

/**
 * unpack ISO message
 * \param[in] iByteArr: buffer with message data
 * \param[in] iByteArrSize: length of message data
 * \returns error code
 */
    DL_ERR Unpack ( const DL_UINT8           *iByteArr,
                    DL_UINT16                 iByteArrSize);

    /******************************************************************************/
#if defined(_VRXEVO) || defined(_VOS)

/**
 * outputs the contents of the ISO8583 message to the log
 */
    void Dump ( );

#else
/**
 * outputs the contents of the ISO8583 message to the log
 * \param[in] iOutFile: output stream
 * \param[in] _iEolStr: EOL terminator string, defaults to '\n' if NULL
 */
    void Dump ( FILE                     *iOutFile,
                const char               *_iEolStr);
#endif

    /******************************************************************************/

private:
    DL_ISO8583_MSG iMsg;
    
    DL_ISO8583_MSG_HANDLER *iMsgHandler;

};

#ifdef __cplusplus
}  /* end of the 'extern "C"' block */
#endif

#endif /* __INC_DL_ISO8583 */
