#ifndef EMEAV_TLVLITE_FWD_SAFEBUFFER_HPP
#define EMEAV_TLVLITE_FWD_SAFEBUFFER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
 #ifndef _DOXYGEN_
  #ifdef _VRXEVO
    #ifdef ISO_SHARED_EXPORT
      #define ISO_EXP_DECL __declspec(dllexport) // used for VSL symbol export
    #elif ISO_STATIC_EXPORT
      #define ISO_EXP_DECL // used for static libraries
    #else
      #define ISO_EXP_DECL __declspec(dllimport) // used for VSA symbol import (also used for static linking)
    #endif
  #elif defined __GNUC__ && defined ISO_SHARED_EXPORT
    #define ISO_EXP_DECL __attribute__((visibility("default")))
  #else
    #define ISO_EXP_DECL // used for both, static libraries and program symbol import
  #endif
#endif

/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   fwd/SafeBuffer.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Forward declaration for SafeBuffer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 

/** 
 * Exported class declarations 
 **/ 
	class ISO_EXP_DECL SafeBuffer;
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_FWD_SAFEBUFFER_HPP */
