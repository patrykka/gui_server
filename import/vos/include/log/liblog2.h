#ifndef _LIBLOG2_H_
#define _LIBLOG2_H_

#if defined(_LIBLOG_H_)
    #error Please include only one version of LOG API (liblog.h or liblog2.h), not both at the same time
#endif

// API ver. 2.4
#define LIBLOG_API_VERSION 24

#include "priv/logapi_base.h"
#include "priv/logapi_priv_v2.h"

#include "priv/logapi_printf_def_v2.h"
#include "priv/logapi_hexdump_def_v2.h"

#endif //_LIBLOG2_H_
