/**
  * @file logapi_base.h
 *
 */
#ifndef _LOGAPI_BASE_H_
#define _LOGAPI_BASE_H_

#define _LIBLOG_PRIV_

#include <stdint.h>

#ifdef _VRXEVO
#   ifndef LOG_API
#       ifdef _LOG_API_EXPORT
#           define LOG_API __declspec(dllexport)
#       else
#           define LOG_API __declspec(dllimport)
#       endif
#   endif
#else
#   define LOG_API
#endif

#ifdef _VRXEVO
#   ifndef LOG_STREAM_API
#       ifdef _LOG_STREAM_API_EXPORT
#           define LOG_STREAM_API __declspec(dllexport)
#       else
#           define LOG_STREAM_API __declspec(dllimport)
#       endif
#   endif
#else
#   define LOG_STREAM_API
#endif

#if defined( __GNUC__ ) || defined( __GNUG__ )
#   define LOGAPI_GCC

#   define LOGAPI_GCC_ATTR( x ) __attribute__(x)
#else
#   define LOGAPI_GCC_ATTR( x )
#endif

#ifndef _VRXEVO
#   define LOGAPI_PUBLIC LOGAPI_GCC_ATTR(( visibility("default") ))
#else
#   define LOGAPI_PUBLIC LOG_API
#endif

#ifndef _VRXEVO
#   define LOGSTREAM_API_PUBLIC LOGAPI_GCC_ATTR(( visibility("default") ))
#else
#   define LOGSTREAM_API_PUBLIC LOG_STREAM_API
#endif

#if defined( __cplusplus )
extern "C" {
#endif

#define MSG_LIMIT (100 * 1024)

// Max size for one liblog produced message
#define MAX_LOG_MSG 190

// Statically reserved buffer for arrived messages
#define ARRIVING_MSG (MAX_LOG_MSG * 5)

/**
 * @brief Sane limits
 */
enum
{
#ifdef _VRXEVO
    //syslog daemon will cut messages in a proper way
    LOGAPI_MSG_MAX_LEN = MAX_LOG_MSG,
    LOGAPI_ENV_NAME_MAX_LEN = 40,
#else
    //syslog does not cut messages, so high-level API should take care of that
    //currently busybox restricts messages to 250 chars
    LOGAPI_MSG_MAX_LEN = MAX_LOG_MSG,
#endif
    LOGAPI_CONFIG_NAME_MAX_LEN = 256,
    LOGAPI_FILTER_MAX_LEN = 255,
    LOGAPI_CHANNEL_MAX_LEN = 32,
    LOGAPI_SOURCE_MAX_LEN = 16,
};

typedef uintptr_t LibLogHandle;

#define LOGAPI_FILE __FILE__
#define LOGAPI_LINE __LINE__

#   define LOG_CONVERT_TAG( x ) (";" x ";")

#ifndef _LIBLOG_GET_VER
#   define _LIBLOG_GET_VER
/**
  * @brief returns release version of a library 
  */
    LOGAPI_PUBLIC const char * log_getVersion(void);
#endif

/**
 * @brief Get Syslog daemon version
 */
LOGAPI_PUBLIC const char *Log_GetSvcVersion(void);

/*
* Message log API
*/

/**
 * @brief Configuration parameters
 */
typedef enum
{
    LOGAPI_ENABLED_PARAM = 0,
    LOGAPI_MASK_PARAM = 1,
    LOGAPI_VERBOSITY_PARAM = 2,
    LOGAPI_OUTPUT_PARAM = 3
} LogAPI_CfgParam;

/**
 * @brief Configuration parameter errors
 */
typedef enum
{
    LOGAPI_CFG_OK = 0,
    LOGAPI_PARAM_ERROR = 1,
    LOGAPI_VALUE_ERROR = 2,
    LOGAPI_READ_ERROR = 3,
    LOGAPI_WRITE_ERROR = 4,
}  LogAPI_CfgError;

/**
 * @brief Enable/Disable states
 */
typedef enum
{
    LOGAPI_DISABLED = 0,
    LOGAPI_ENABLED = 1
} LogAPI_Enabled;

/**
 * @brief Basic Logging API levels with strictly defined IDs
 * @note In most cases, use DBG*_ERROR() ...DBG*_TRACE() macros
 * @see severity levels in syslog.h
 */
typedef enum
{
    LOGAPI_OFF = 0,
    LOGAPI_EMERG = 1,
    LOGAPI_ALERT = 2,
    LOGAPI_CRIT = 3,
    LOGAPI_ERROR = 4,
    LOGAPI_WARN = 5,
    LOGAPI_NOTICE = 6,
    LOGAPI_INFO = 7,
    LOGAPI_TRACE = 8
} LogAPI_Levels;

/**
 * @brief Logging API Mask
 * @note By default mask is disabled (LOGAPI_NO_MASK)
 * If defined mask has priority over Levels (LogAPI_Levels)
 */
typedef enum
{
    LOGAPI_NO_MASK = -1,
    LOGAPI_EMPTY_MASK = 0
} LogAPI_Masks;

/**
 * @brief Low level message data
 * @note It's designed for easy extensions and backward compatibility in shared libraries
 *
 */
typedef struct
{
    const char *msg; /**< Pre-formatted message of LOGAPI_MSG_MAX_LEN characters max + 1 for zero termination */
    unsigned int msg_len; /**< don't depend on zero termination here */
    
    const char *channel; /**< LOGAPI_CHANNEL_MAX_LEN characters max + 1 for zero termination */
    
    const char *source_file; /**< Use LOGAPI_FILE macro here */
    unsigned int source_line; /**< Use LOGAPI_LINE macro here */
    /** @note "source_file:source_line" is truncated to last LOGAPI_SOURCE_MAX_LEN characters */
    
    LogAPI_Levels log_level; /**< Log level */
    
    unsigned int struct_end_indicator; /**< Should always be 0, unless the structure is extended */
} LogAPI_MessageData;


/**
  * @brief Message destination
  */
enum LOGAPI_DESTINATION
{
    /** 
      * @brief outputs to console
      * @details on V/OS messages would go to <b>stderr</b>.
      * On Verix they would go to internal Verix buffer
      * @warning in order for this to work on Verix, <b>*DEBUG</b> environment variable must be set to a valid serial port
     */
    LOGAPI_CONSOLE = 0x1,

    /** @brief outputs to syslog daemon
      * @note this is default value
     */
    LOGAPI_SYSLOG = 0x2,
    /** @brief outputs both to syslog daemon and console */
    LOGAPI_ALL = LOGAPI_CONSOLE | LOGAPI_SYSLOG,

    /** @brief Only callback output, other outputs are ignored
      * @note Debug/Test purpose
     */
    LOGAPI_CALLBACK = 0x4,
};

typedef enum
{
    /** Turn off verbose mode. The same effect as if LOGAPI_SET_VERBOSITY() would not get called at all*/
    LOGAPI_VERB_OFF = 0x0,
    /** Prepend FILE:LINE to a log message */
    LOGAPI_VERB_FILE_LINE = 0x1,
    /** Prepend [PROCESS ID] to a log message */
    LOGAPI_VERB_PID = 0x2,
    /** Prepend [THREAD ID] to a log message, has effect to the Verix platform ONLY! */
    LOGAPI_VERB_TID = 0x4,
} LogAPI_Verbosity;

struct syslog_info_t
{
    int dest;
    int priority;
    const char *channel;
    const char *file;
    int verbosity;
    const char *msg;
};
typedef struct syslog_info_t* syslog_info_ptr;

typedef void (*LogCallback)(syslog_info_ptr info);

/**
 * @brief Get Syslog daemon version
 */
LOGAPI_PUBLIC const char *Log_GetSvcVersion(void);

/**
 * @brief Expand to statement, if LogAPI debug is enabled
 */
#if defined( LOGAPI_ENABLE_DEBUG )
#   define LOGAPI_WHEN_DEBUG( stmt )    stmt
#else
/** @warning don't use LOGAPI_EMPTYSTMT as this macro can be used in very hackish
 * form like "if ( LOGAPI_WHEN_DEBUG( test1 && ) test2 )"*/
#   define LOGAPI_WHEN_DEBUG( stmt )
#endif


#if defined( __cplusplus )
} // extern "C" 
#endif

#endif //_LOGAPI_BASE_H_
