/**
  * @file logapi_hexdump_def_v2.h
 * @note Define <b>LOGAPI_ENABLE_DEBUG</b> to enable DBG*() macros
 *
 *   Dump title + address + size, start separator HEX and ASCII representation, end separator
 *    - LOGAPI_HEXDUMP_EMERG(title, data, size)
 *    - LOGAPI_HEXDUMP_ALERT(title, data, size)
 *    - LOGAPI_HEXDUMP_CRIT(title, data, size)
 *    - LOGAPI_HEXDUMP_ERROR(title, data, size)
 *    - LOGAPI_HEXDUMP_WARN(title, data, size)
 *    - LOGAPI_HEXDUMP_NOTICE(title, data, size)
 *    - LOGAPI_HEXDUMP_INFO(title, data, size)
 *    - LOGAPI_HEXDUMP_TRACE(title, data, size)
 *    - DBG_HEXDUMP_TRACE(title, data, size)
 *
 *   Dump HEX and ASCII representation only
 *    - LOGAPI_HEXDUMP_RAW_EMERG(data, size)
 *    - LOGAPI_HEXDUMP_RAW_ALERT(data, size)
 *    - LOGAPI_HEXDUMP_RAW_CRIT(data, size)
 *    - LOGAPI_HEXDUMP_RAW_ERROR(data, size)
 *    - LOGAPI_HEXDUMP_RAW_WARN(data, size)
 *    - LOGAPI_HEXDUMP_RAW_NOTICE(data, size)
 *    - LOGAPI_HEXDUMP_RAW_INFO(data, size)
 *    - LOGAPI_HEXDUMP_RAW_TRACE(data, size)
 *    - DBG_HEXDUMP_RAW_TRACE(data, size)
 *
 */

#ifndef _LOGAPI_HEXDUMP_DEF_V2_H_
#define _LOGAPI_HEXDUMP_DEF_V2_H_

#if !defined(_LIBLOG_PRIV_)
    #error Please avoid directly include this file, include liblog2.h instead
#endif

#if defined( __cplusplus )
extern "C" {
#endif

/**
 * @brief helper macro
 * @warning Avoid direct use
 */
#define LOGAPI_HEXDUMP2( handle, level, title, data, size ) \
    LogAPI_hexdump2( handle, level, title, data, size, LOGAPI_FILE, LOGAPI_LINE )

#define LOGAPI_HEXDUMP_EMERG(handle, title, data, size)    LOGAPI_HEXDUMP2(handle, LOGAPI_EMERG, title, data, size)
#define LOGAPI_HEXDUMP_ALERT(handle, title, data, size)    LOGAPI_HEXDUMP2(handle, LOGAPI_ALERT, title, data, size)
#define LOGAPI_HEXDUMP_CRIT(handle, title, data, size)     LOGAPI_HEXDUMP2(handle, LOGAPI_CRIT, title, data, size)
#define LOGAPI_HEXDUMP_ERROR(handle, title, data, size)    LOGAPI_HEXDUMP2(handle, LOGAPI_ERROR, title, data, size)
#define LOGAPI_HEXDUMP_WARN(handle, title, data, size)     LOGAPI_HEXDUMP2(handle, LOGAPI_WARN, title, data, size)
#define LOGAPI_HEXDUMP_NOTICE(handle, title, data, size)   LOGAPI_HEXDUMP2(handle, LOGAPI_NOTICE, title, data, size)
#define LOGAPI_HEXDUMP_INFO(handle, title, data, size)     LOGAPI_HEXDUMP2(handle, LOGAPI_INFO, title, data, size)
#define LOGAPI_HEXDUMP_TRACE(handle, title, data, size)    LOGAPI_HEXDUMP2(handle, LOGAPI_TRACE, title, data, size)

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP_TRACE   LOGAPI_HEXDUMP_TRACE
#else
    #define DBG_HEXDUMP_TRACE(...)  LOGAPI_EMPTYSTMT
#endif

/**
 * @brief helper macro
 * @warning Avoid direct use
 */
#define LOGAPI_HEXDUMP_RAW2( handle, level, data, size ) \
    LogAPI_hexdump_raw2( handle, level, data, size, LOGAPI_FILE, LOGAPI_LINE )

#define LOGAPI_HEXDUMP_RAW_EMERG(handle, data, size)   LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_EMERG, data, size)
#define LOGAPI_HEXDUMP_RAW_ALERT(handle, data, size)   LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_ALERT, data, size)
#define LOGAPI_HEXDUMP_RAW_CRIT(handle, data, size)    LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_CRIT, data, size)
#define LOGAPI_HEXDUMP_RAW_ERROR(handle, data, size)   LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_ERROR, data, size)
#define LOGAPI_HEXDUMP_RAW_WARN(handle, data, size)    LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_WARN, data, size)
#define LOGAPI_HEXDUMP_RAW_NOTICE(handle, data, size)  LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_NOTICE, data, size)
#define LOGAPI_HEXDUMP_RAW_INFO(handle, data, size)    LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_INFO, data, size)
#define LOGAPI_HEXDUMP_RAW_TRACE(handle, data, size)   LOGAPI_HEXDUMP_RAW2(handle, LOGAPI_TRACE, data, size)

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBG_HEXDUMP_RAW_TRACE   LOGAPI_HEXDUMP_RAW_TRACE
#else
    #define DBG_HEXDUMP_RAW_TRACE(...)  LOGAPI_EMPTYSTMT
#endif

#if defined( __cplusplus )
}
#endif
#endif  //_LOGAPI_HEXDUMP_DEF_V2_H_
