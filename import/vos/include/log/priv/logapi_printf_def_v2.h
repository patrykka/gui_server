/**
 * @file logapi_printf_def_v2.h
 * @note Define <b>LOGAPI_ENABLE_DEBUG</b> to enable DBG*2() macros
 * @note Enable <b>-Wall</b> for GCC compiler to check formatting
 *
 * Public API:
 *   printf()-like message log
 *    - LOGF_EMERG (handle, format, ...)
 *    - LOGF_ALERT (handle, format, ...)
 *    - LOGF_CRIT  (handle, format, ...)
 *    - LOGF_ERROR (handle, format, ...)
 *    - LOGF_WARN  (handle, format, ...)
 *    - LOGF_NOTICE(handle, format, ...)
 *    - LOGF_INFO  (handle, format, ...)
 *    - LOGF_TRACE (handle, format, ...)
 * 
 *    - DBGF_TRACE (handle, format, ...)
 */
#ifndef _LOGAPI_PRINTF2_H_
#define _LOGAPI_PRINTF2_H_

#if !defined(_LIBLOG_PRIV_)
    #error Please avoid directly include this file, include liblog2.h instead
#endif

#if defined( __cplusplus )
extern "C"
{
#endif

/**
 * @brief Macro to be used for initialization
*/
#define LOGAPI_INIT( channel ) \
    LogAPI_Init2( channel )

/**
 * @brief Macro to be used for deinitialization
*/
#define LOGAPI_DEINIT( handle ) \
    LogAPI_Deinit2( handle )

/**
 * @brief Macro to be used for re-configuration notification
*/
#define LOGAPI_RECONFIG_NOTIFY() \
    LogAPI_ReconfigNotification()

/**
 * @brief Dump system information at info level
 */
#define LOGAPI_DUMP_SYS_INFO( handle ) \
    LogAPI_os_dump_sysinfo2( handle, LOGAPI_INFO )

/**
 * @brief Dump callstack
 */
#define LOGAPI_PRINT_CALLSTACK( handle, level ) \
    LogAPI_os_print_callstack2( handle, level )

#define LOGAPI_GETCONFIG(channel, param, value) \
    LogAPI_GetConfigParam(channel, param, value);

#define LOGAPI_SETCONFIG(channel, param, value) \
    LogAPI_SetConfigParam(channel, param, (long)value);

#   define LOGAPI_PRINTF2( handle, level, ... ) \
        LogAPI_printf2( handle, level, LOGAPI_FILE, LOGAPI_LINE, __VA_ARGS__ )

#   define LOGF_EMERG( handle, ... )   LOGAPI_PRINTF2( handle, LOGAPI_EMERG    , __VA_ARGS__ )
#   define LOGF_ALERT( handle, ... )   LOGAPI_PRINTF2( handle, LOGAPI_ALERT    , __VA_ARGS__ )
#   define LOGF_CRIT( handle, ... )    LOGAPI_PRINTF2( handle, LOGAPI_CRIT     , __VA_ARGS__ )
#   define LOGF_ERROR( handle, ... )   LOGAPI_PRINTF2( handle, LOGAPI_ERROR    , __VA_ARGS__ )
#   define LOGF_WARN( handle, ... )    LOGAPI_PRINTF2( handle, LOGAPI_WARN     , __VA_ARGS__ )
#   define LOGF_NOTICE( handle, ... )  LOGAPI_PRINTF2( handle, LOGAPI_NOTICE   , __VA_ARGS__ )
#   define LOGF_INFO( handle, ... )    LOGAPI_PRINTF2( handle, LOGAPI_INFO     , __VA_ARGS__ )
#   define LOGF_TRACE( handle, ... )   LOGAPI_PRINTF2( handle, LOGAPI_TRACE    , __VA_ARGS__ )

/* Required due to concern raised in certain unsafe coding style situations */
#define LOGAPI_EMPTYSTMT do {} while(0)

#if defined( LOGAPI_ENABLE_DEBUG )
    #define DBGF_TRACE LOGF_TRACE
#else
    #define DBGF_TRACE(...) LOGAPI_EMPTYSTMT
#endif

#if defined( __cplusplus )
} // extern "C"
#endif

#endif //_LOGAPI_PRINTF2_H_
