/**
  * @file logapi_priv_v1.h
 *
 */
#ifndef _LOGAPI_PRIV_V1_H_
#define _LOGAPI_PRIV_V1_H_

#if !defined(_LIBLOG_PRIV_)
    #error Please avoid directly include this file, include liblog.h or liblog2.h instead
#endif

#include "stdarg.h"
#include "logapi_base.h"

#if defined( __cplusplus )
extern "C" {
#endif


/**
 * @brief Platform-specific callstack dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_VerboseAbort( LogAPI_Levels log_level );

/**
 * @brief Check internal filter for tag presence
 */
LOGAPI_PUBLIC int LogAPI_IsInFilterList( const char * tag );

/**
 * @brief Platform-specific message logging implementation
 * @warning Use DBGF_*() or DBGS_*() macros
 */
LOGAPI_PUBLIC void LogAPI_os_message( const LogAPI_MessageData* msg_data );

/**
 * @brief Initialize logging library with a specified channel
 * @param channel Application name or anything meaningful of up to LOGAPI_CHANNEL_MAX_LEN
 * @note will prepend FILE:NUMBER to a message
 * @warning Use LOGAPI_INIT() macro
 */
LOGAPI_PUBLIC void LogAPI_Init( const char *channel);

/**
 * @brief Get current filter's log level
 */
LOGAPI_PUBLIC LogAPI_Levels LogAPI_GetLevel( void );

/**
 * @brief Platform-specific system information dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_dump_sysinfo( LogAPI_Levels log_level );

/**
 * @brief Get current log channel
 */
LOGAPI_PUBLIC const char *LogAPI_GetChannel( void );

/**
 * @brief Set log level filter
 * @warning Use LOGAPI_SETLEVEL() macro
 */
LOGAPI_PUBLIC void LogAPI_SetLevel( LogAPI_Levels log_level );

/**
 *@brief Sets verbosity of log messages
 *@param verbosity can be either of
 - LOGAPI_VERB_OFF 
 - LOGAPI_VERB_FILE_LINE 
 - LOGAPI_VERB_PID 
 *@see LogAPI_Verbosity
 *@attention This function must be called strictly before LOGAPI_INIT() to work correctly
*/
LOGAPI_PUBLIC void LogAPI_Set_Verbosity(int verbosity);

/**
 * @brief Deinitialize logging library
 */
LOGAPI_PUBLIC void LogAPI_Deinit(void);

/**
 * @brief Set semicolon delimited filtering tags
 */
LOGAPI_PUBLIC void LogAPI_Set_FilterList( const char * filter_list );

/**
  * @brief Sets destination of LogAPI
  * @note @see LOGAPI_DESTINATION
  * @note use LOGAPI_SETOUTPUT macro
  */
LOGAPI_PUBLIC void LogAPI_setoutput(enum LOGAPI_DESTINATION);

/**
 * @brief Platform-specific callstack dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_print_callstack( LogAPI_Levels log_level );

/**
 * @brief Log variable argument helper
 * @note Should be use in legacy logging API wrappers only
 */
LOGAPI_PUBLIC void LogAPI_vprintf(
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, va_list args
);

/**
 * @brief Log message in printf-like style
 * @warning Avoid direct use
 */
LOGAPI_PUBLIC void LogAPI_printf(
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, ...
) LOGAPI_GCC_ATTR(( format( printf, 4, 5 ) ));

/**
 * @brief Log message in printf-like style
 * @warning Avoid direct use
 */
LOGAPI_PUBLIC void LogAPI_tag_printf(
    LogAPI_Levels log_level, const char * tag, const char *file, unsigned int line,
    const char* format, ...
) LOGAPI_GCC_ATTR(( format( printf, 5, 6 ) ));

/**
* Hex dump API
***/

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX() macro
 *
 * Dump title + address + size, start separator HEX and ASCII representation, end separator
 */
LOGAPI_PUBLIC void LogAPI_hexdump(
    LogAPI_Levels log_level, const char *title, const void *data, unsigned int size,
    const char *file, unsigned int line
);

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_TAG() macro
 *
 * Dump title + address + size, start separator HEX and ASCII representation, end separator
 */
LOGAPI_PUBLIC void LogAPI_hexdump_tag(
    LogAPI_Levels log_level, const char * tag, const char *title, const void *data, unsigned int size,
    const char *file, unsigned int line
);

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_RAW_TAG() macro
 *
 * Dump HEX and ASCII representation only
 */
LOGAPI_PUBLIC void LogAPI_hexdump_raw_tag(
        LogAPI_Levels log_level, const char * tag, const void *data, unsigned int size,
        const char *file, unsigned int line
);

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_RAW() macro
 *
 * Dump HEX and ASCII representation only
 */
LOGAPI_PUBLIC void LogAPI_hexdump_raw(
        LogAPI_Levels log_level, const void *data, unsigned int size,
        const char *file, unsigned int line
);

#if defined( __cplusplus )
} // extern "C" 
#endif

#endif //_LOGAPI_PRIV_V1_H_
