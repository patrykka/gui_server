/**
  * @file logapi_priv_v2.h
 *
 */
#ifndef _LOGAPI_PRIV_V2_H_
#define _LOGAPI_PRIV_V2_H_

#if !defined(_LIBLOG_PRIV_)
    #error Please avoid directly include this file, include liblog2.h instead
#endif

#include "stdarg.h"
#include "logapi_base.h"

#if defined( __cplusplus )
extern "C" {
#endif

/**
 * @brief Initialize logging library with a specified channel
 * @param channel Application name or anything meaningful of up to LOGAPI_CHANNEL_MAX_LEN
 * @return initialization handle
 */
LOGAPI_PUBLIC LibLogHandle LogAPI_Init2( const char *channel );

/**
 *@brief Deinitialize logging library
 *@param handle initialization handle
 */
LOGAPI_PUBLIC void LogAPI_Deinit2( LibLogHandle handle );

/**
 * @brief Notify all applications to re-read their configuration files
 * @warning Use LOGAPI_RECONFIG_NOTIFY() macro
 */
LOGAPI_PUBLIC void LogAPI_ReconfigNotification( void );

/**
 * @brief Checks for notification from LOGAPI_RECONFIG_NOTIFY() and do a reconfiguration
 * @warning Use LOGAPI_RECONFIG_NOTIFY() macro
 */
LOGAPI_PUBLIC void LogAPI_reconfig(LibLogHandle handle);

/**
 * @brief Platform-specific callstack dump implementation v2
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_print_callstack2( LibLogHandle handle, LogAPI_Levels log_level );

/**
 * @brief Get log channel
 */
LOGAPI_PUBLIC const char *LogAPI_GetChannel2( LibLogHandle handle );

/**
 * @brief Check if message with given level will be put to log
 * @param handle initialization handle
 * @param log_level logging level
 * @return Returns 1 if level is enough to put message to log, otherwise returns 0
 * @warning This function always returns 0 if logs are disabled
 */
LOGAPI_PUBLIC int LogAPI_LevelEnough2( LibLogHandle handle, LogAPI_Levels log_level );

/**
 * @brief Platform-specific system information dump implementation
 * @warning Use LOGAPI_DUMP_SYS_INFO() macro
 */
LOGAPI_PUBLIC void LogAPI_os_dump_sysinfo2( LibLogHandle handle, LogAPI_Levels log_level );

/**
 * @brief Log variable argument helper
 * @note Should be use in legacy logging API wrappers only
 */
LOGAPI_PUBLIC void LogAPI_vprintf2(
    LibLogHandle handle,
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, va_list args
);

/**
 * @brief Log message in printf-like style
 * @warning Avoid direct use
 */
LOGAPI_PUBLIC void LogAPI_printf2(
    LibLogHandle handle,
    LogAPI_Levels log_level, const char *file, unsigned int line,
    const char* format, ...
) LOGAPI_GCC_ATTR(( format( printf, 5, 6 ) ));

/**
* Hex dump API
***/

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX() macro
 *
 * Dump title + address + size, start separator HEX and ASCII representation, end separator
 */
LOGAPI_PUBLIC void LogAPI_hexdump2( LibLogHandle handle,
    LogAPI_Levels log_level, const char *title, const void *data, unsigned int size,
    const char *file, unsigned int line
);

/**
 * @brief Universal hexdump
 * @warning Use LOGAPI_HEX_RAW() macro
 *
 * Dump HEX and ASCII representation only
 */
LOGAPI_PUBLIC void LogAPI_hexdump_raw2( LibLogHandle handle,
        LogAPI_Levels log_level, const void *data, unsigned int size,
        const char *file, unsigned int line
);

LOGAPI_PUBLIC int LogAPI_GetConfigParam(const char* channel, LogAPI_CfgParam param, long *value);
LOGAPI_PUBLIC int LogAPI_SetConfigParam(const char* channel, LogAPI_CfgParam param, long value);

/**
 * @brief Expand to statement, if LogAPI debug is enabled
 */
#if defined( LOGAPI_ENABLE_DEBUG )
#   define LOGAPI_WHEN_DEBUG( stmt )    stmt
#else
/** @warning don't use LOGAPI_EMPTYSTMT as this macro can be used in very hackish
 * form like "if ( LOGAPI_WHEN_DEBUG( test1 && ) test2 )"*/
#   define LOGAPI_WHEN_DEBUG( stmt )
#endif


#if defined( __cplusplus )
} // extern "C" 
#endif

#endif //_LOGAPI_PRIV_V2_H_
