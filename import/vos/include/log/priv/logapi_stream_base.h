/**
  * @file logapi_stream_base.h
 *
 */
#ifndef _LOGAPI_STREAM_BASE_H_
#define _LOGAPI_STREAM_BASE_H_

#if !defined(_LIBLOG_PRIV_)
    #error Please avoid directly include this file, include logapi_stream2.hpp instead
#endif

#include <streambuf>
#include <ostream>
#include <vector>

#include "logapi_base.h"

namespace LogAPI
{
    /** @brief Helper class
      * @warning Do not use directly, unless you know what are you doing
    */
    class MsgStreamBuf : public std::streambuf
    {
        public:
                LOGAPI_PUBLIC explicit MsgStreamBuf(std::size_t buff_sz = LOGAPI_MSG_MAX_LEN);
                LOGAPI_PUBLIC void setOpts(LogAPI_Levels level, const char *sourceFile, unsigned int sourceLine);
                LOGAPI_PUBLIC int isInFilterList( const char * tag );

        private:
            virtual int_type overflow(int_type ch);
            virtual int sync();

            MsgStreamBuf(const MsgStreamBuf &);
            MsgStreamBuf &operator= (const MsgStreamBuf &);

        private:
            bool cap_next_;
            std::vector<char> buffer_;
            std::string source_file_;
            size_t source_line_;
            LogAPI_Levels level_;
    };

    // Helper class to implement Base-from-Member idiom
    struct log_ostream_base
    {
        public:
            log_ostream_base() : mBuffer() {}
        protected:
            MsgStreamBuf mBuffer;
    };

    class log_ostream: protected log_ostream_base, public std::ostream
    {
        public:
            // This call provides a correct init order: buffer then stream
            log_ostream(): log_ostream_base(), std::ostream(&mBuffer) {}
            MsgStreamBuf& getBuffer() { return mBuffer; }
    };

    LOGSTREAM_API_PUBLIC extern log_ostream g_stream;
}
#endif //_LOGAPI_STREAM_BASE_H_
