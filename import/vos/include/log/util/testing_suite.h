#ifndef TESTING_SUITE_H
#define TESTING_SUITE_H

#include <sys/time.h>
#include <string>
#include <string.h>
#include <vector>
#include <stdio.h>
#ifdef _VRXEVO
#include <SVC.H>
#define sleep(x) SVC_WAIT(x*1000)
#endif
#ifdef _x86
#include <unistd.h>
#define sleep(x) usleep(x*1000*1000)
#endif

inline unsigned timestamp()
{
   struct timeval tv;
   gettimeofday(&tv,0);
   return tv.tv_sec*1000+tv.tv_usec/1000;
}
#define LOG printf
#define ASSERT(x) \
    {\
        asserts++;\
        if(x) \
        {\
        }\
        else \
        {\
            passed = false;\
            LOG(#x": ASSERT FAILED on %s : %d",__FILE__,__LINE__);\
        }\
    }

#define TEST_FIXTURE_START(fixture, summary)\
    bool fixture()\
    {\
        static const char *FIXTURE_NAME= #fixture;\
        static int total_cases = 0;\
        static int passed_cases = 0;\
		static bool mResult = false;\
		std::string message;\
		std::vector<std::string> messages;\
        void (*cleanup_function)() = 0;\
        LOG("\nFixture: %s", FIXTURE_NAME);\
        LOG("Summary: %s", summary);

#define TEST_FIXTURE_START_CLEANUP(fixture, summary, cleanup_func) \
    TEST_FIXTURE_START(fixture, summary)\
    cleanup_function = cleanup_func;\

#define TEST_CASE_START_SUMMARY(summary) \
{\
    int asserts = 0;\
    bool passed = true;\
    void (*startup)() = 0;\
    void (*cleanup)() = 0;\
    LOG("\n\nCase %d) Summary: %s", ++total_cases, summary);

#define TEST_CASE_START_SUMMARY_FUNC(summary, startup_f, cleanup_f) \
    TEST_CASE_START_SUMMARY(summary) \
    startup = startup_f;\
    cleanup = cleanup_f;\
    startup();

#define TEST_CASE_END \
    if(cleanup)\
        cleanup();\
    if(!passed)\
    {\
        LOG("\nCase %d FAILED\n",total_cases);\
		sleep(5);\
    }\
    else\
    {\
        ++passed_cases;\
        LOG("Case %d PASSED\n",total_cases);\
    }\
}

#define TEST_FIXTURE_END \
    if(cleanup_function)\
    {\
         cleanup_function();\
    }\
    LOG("\nFixture %s ended. [%d/%d] test cases passed.\n", FIXTURE_NAME, passed_cases, total_cases); \
    return (passed_cases == total_cases);\
}

#endif //TESTING_SUITE_H
