/**
 * \file msr.h
 * Interface definitions for libmsr.
 * This file defines the API for the magnetic card reader library.
 * \author Thomas Buening, GSS
 */

#ifndef __MSR_LIB_INTERFACE__
#define __MSR_LIB_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

#include "msr_common.h"

/**
 * Type of function that is called after magnetic card has been swiped, see MSR_Activate().
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* MSR_Callback) (void *data);

/**
 * Set options.
 * This function has to be called while MSR is deactivated.
 * MSR_Activate()/MSR_Deactivate() do not discard the options previously set, so setting the options once at terminal start is sufficient.
 * \param[in] options : data pointer, see \ref MSR_OPTIONS
 * \param[in] options_len : length of options
 * \return
 * - \ref MSR_OK : No error
 * - \ref MSR_PARAM : options == NULL or options_len == 0
 * - \ref MSR_ERROR : MSR is activated, call MSR_Deactivate() first.
 */
int MSR_SetOptions(unsigned char *options, unsigned char options_len);

/**
 * Activate magnetic card reader interface.
 * After successful execution of this function every swiped card is read and its data is stored internally.
 * Upon storing card data any previously stored data is erased, so only the data of the last swiped card remains available.
 * To obtain the stored data, call MSR_GetData(). After doing so, the stored data is erased as well, so that card data can only be obtained once.
 * \param[in] cbf : Callback function that is called every time a card has been swiped, may be NULL. Within the callback function card data can be obtained by MSR_GetData().
 * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
 * \return
 * - \ref MSR_OK : Successfully activated.
 * - \ref MSR_ERROR : No reader present, device could not be opened, or internal error.
 * - \ref MSR_ACTIVE : Interface is already activated, nothing done (especially cbf and cb_data are not taken over, these will only be set if function returns \ref MSR_OK).
 */
int MSR_Activate(MSR_Callback cbf, void *cb_data);

/**
 * Deactivate magnetic card reader interface.
 * After deactivating the card reader interface no more swiped cards are read and no data is stored. Any previously stored card data will be erased.
 * \return
 * - \ref MSR_OK : Successfully deactivated or not activated before.
 * - \ref MSR_ERROR : Internal error.
 */
int MSR_Deactivate(void);

/**
 * Check if magnetic card has been swiped and its data is stored. After obtaining the stored data with MSR_GetData()
 * or deactivating the card reader interface with MSR_Deactivate() the stored data is erased and not available any longer.
 * \return
 * - 1 : Data available.
 * - 0 : No data available.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained by MSR_GetData(). Reactivation needed before next card swipe.
 */
int MSR_DataAvailable(void);

/**
 * Obtain magnetic card data.
 * With this function the internally stored card data can be obtained. In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * @n Remark:
 * In case the terminal has two magnetic heads the data from the first head is populated.
 * If there is no data from first head the data from the second head is returned.
 * If you need data from both heads you shall use MSR_GetData4().
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
int MSR_GetData(int timeout_msec, MSR_TrackData *tracks, MSR_DecodedData *data);

/**
 * Obtain magnetic card data including card type.
 * This is the same as MSR_GetData() but additionally offers \ref MSR_CARD_CODES.
 * With this function the internally stored card data can be obtained. In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData2() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
int MSR_GetData2(int timeout_msec, MSR_TrackData2 *tracks, MSR_DecodedData *data);

/**
 * Obtain magnetic card data including additional information.
 * This is the same as MSR_GetData() but additionally offers \ref MSR_CARD_CODES and \ref MSR_TRACK_TYPES.
 * With this function the internally stored card data can be obtained. In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData3() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
int MSR_GetData3(int timeout_msec, MSR_TrackData3 *tracks, MSR_DecodedData *data);

/**
 * Obtain magnetic card data including additional information.
 * This is the same as MSR_GetData() but additionally offers \ref MSR_CARD_CODES, \ref MSR_TRACK_TYPES and second magstripe data (if present).
 * With this function the internally stored card data can be obtained.
 * In doing so, the internally stored data is erased, so that data of each card swipe can only be obtained once.
 * If no card data is stored at the moment, the function waits for a specified time for a card swipe.
 * If a timeout is supplied, the function is blocking and can be aborted by MSR_AbortGetData().
 * If a callback function is supplied to MSR_Activate(), setting a timeout != 0 is not allowed.
 * MSR_GetData4() can be called from within a callback function supplied to MSR_Activate() but there's no need to do so.
 * \param[in] timeout_msec : Time (in milliseconds, < 0 : infinite) to wait for a card swipe if no card data has been stored previously.
 * \param[out] tracks : Track data, may be NULL.
 * \param[out] data : Decoded track data, may be NULL.
 * \return
 * - \ref MSR_OK : Magnetic card data is available of became available within the supplied timeout period.
 * - \ref MSR_ERROR : Magnetic card reader interface not activated or internal error.
 * - \ref MSR_TIMEOUT : No magnetic card data available within the specified timeout.
 * - \ref MSR_ABORTED : Aborted by MSR_AbortGetData().
 * - \ref MSR_PARAM : Callback function supplied to MSR_Activate() and timeout != 0.
 * - \ref MSR_PROCESS : MSR_ONE_SWIPE was set and card data has already been obtained. Reactivation needed before next card swipe.
 */
int MSR_GetData4(int timeout_msec, MSR_TrackData4 *tracks, MSR_DecodedData4 *data);

/**
 * Abort blocking function MSR_GetData().
 * If MSR_GetData() is waiting for a card swipe, the blocking function can be aborted by invoking this function from another thread.
 */
void MSR_AbortGetData(void);

/**
 * Set state of MSR LEDs.
 * \param[in] led1 : state of LED 1 (top), see \ref LED_STATES.
 * \param[in] led2 : state of LED 2 (mid), see \ref LED_STATES
 * \param[in] led3 : state of LED 3 (bottom), see \ref LED_STATES
 * \param[in] duration : if > 0: all LEDs are switched off after this time (in seconds).
 * \return
 * - \ref MSR_OK    : Successfully set LED state.
 * - \ref MSR_PARAM : Invalid parameter.
 * - \ref MSR_ERROR : Internal error.
 */
int MSR_SwitchLeds(int led1, int led2, int led3, int duration);


#ifdef __cplusplus
}
#endif

#endif  // avoid double include
