/**
 * \file msr.h
 * Interface definitions for libmsr - part which is common for direct linking and client/server.
 * This file defines the API for the magnetic card reader library.
 * \author Dirk Germann, GSS
 */

#ifndef __MSR_LIB_INTERFACE_COMMON__
#define __MSR_LIB_INTERFACE_COMMON__


/** \defgroup MSR_ERROR_CODES Error codes
 * These error codes are returned by the various libmsr functions.
 * \{ */
#define MSR_OK       0  /**< No error, function executed successfully. */
#define MSR_ERROR   -1  /**< Device error, magnetic card reader not activated, or internal error. */
#define MSR_TIMEOUT -2  /**< Timeout occurred (returned by MSR_GetData()). */
#define MSR_ABORTED -3  /**< Aborted by user (returned by MSR_GetData()). */
#define MSR_ACTIVE  -4  /**< Already activated (returned by MSR_Activate()). */
#define MSR_PARAM   -5  /**< Parameter error (returned by MSR_GetData(), MSR_SetOptions() and MSR_SwitchLeds()). */
#define MSR_PROCESS -6  /**< Process error (returned by MSR_GetData() and MSR_DataAvailable()). */
/** \} */

/** \defgroup MSR_STATUS_CODES Status code
 * These values indicate the status of each read track.
 * \{ */
#define MSR_STATUS_NOERR  0  /**< Valid data. */
#define MSR_STATUS_NODATA 1  /**< No data. */
#define MSR_STATUS_NOSTX  2  /**< Missing start sentinel or insufficient data. */
#define MSR_STATUS_NOETX  3  /**< Missing end sentinel or excessive data. */
#define MSR_STATUS_BADLRC 4  /**< Missing LRC or LRC error. */
#define MSR_STATUS_PARITY 5  /**< Parity error. */
#define MSR_STATUS_REVETX 6  /**< Reversed end sentinel found. */
#define MSR_STATUS_BADJIS 7  /**< Insufficient printable characters. */
#define MSR_STATUS_BADTRK 8  /**< Insufficient characters. */
/** \} */

/** \defgroup MSR_CARD_CODES Card Type Code
 * These values indicate the type of msr read.
 * \{ */
#define MSR_TYPE_PHYSICAL  0  /**< Normal magstripe swipe. */
#define MSR_TYPE_SAMSUNG   1  /**< LoopPay or Samsung Phone swipe. */
/** \} */

/** \defgroup MSR_TRACK_TYPES Track Type Code
 * These values indicate the type of track data.
 * \{ */
#define MSR_CARD_UNKNOWN  0x00  /**< Unknown track data. */
#define MSR_CARD_ISO      0x01  /**< ISO track data. */
#define MSR_CARD_JIS_II   0x02  /**< JIS-II track data. */
/** \} */

/**
 * \defgroup MSR_OPTIONS Options
 * These options configure the behavior of magnetic card reader library, set by MSR_SetOptions().
 * A combination is possible using | operator.
 * \{ */
#define MSR_UX_ENHANCEMENTS       0x01 /**< options[0] Relevant for UX, V/OS only:
                                            use improved noise filter and ignore card insertion, only read on card removal. */

#define MSR_SAMSUNG_TRACK1        0x02 /**< options[0] Relevant for Samsung LoopPay only:
                                            continuous polling of Samsung LoopPay device to fetch track 1 data, if not set will deliver track 2 as soon as any data is read. */

#define MSR_ONE_SWIPE             0x04 /**< options[0] In opposite to default behavior only read first swiped card after each call of MSR_Activate().
                                            Subsequently swiped cards will not be read and will not overwrite the stored data.
                                            The stored data is deleted as usually upon call of MSR_GetData() or MSR_Deactivate().
                                            To read the next card application has to call MSR_Deactivate() and MSR_Activate(). */

#define MSR_LEDS                  0x08 /**< options[0] Light MSR LEDs while card swipe is possible. */

#define MSR_ENABLE_LICENSE_DECODE 0x10 /**< options[0] Enable the decoding of California Drivers License and American Association of Motor Vehicle Administrators (AAMVA) Drivers License. By default, this option is disabled. */

#define MSR_DETECT_ISO            0x01 /**< options[1] Detect ISO cards, this is the default and presumed if options[1] == 0. */

#define MSR_DETECT_JIS_II         0x02 /**< options[1] Detect JIS-II cards. Currently only supported on V/OS2. */

#define MSR_DETECT_ALL            0x03 /**< options[1] Detect both ISO and JIS-II cards. JIS-II cards currently only supported on V/OS2. */
/** \} */

/**
 * \defgroup LED_STATES LED states
 * These are the states of the three MSR LEDs which can be set by MSR_SwitchLeds().
 * On devices that don't have multicolored LEDs, all "color defines" are equivalent and imply "LED on".
 * As there is currently no device with multicolored LEDs, multicolor feature is not implemented yet.
 * \{ */
#define MSR_LED_OFF     0x0000 /**< LED is switched off. */
#define MSR_LED_WHITE   0x0001 /**< LED shines white. */
#define MSR_LED_RED     0x0002 /**< LED shines red. */
#define MSR_LED_GREEN   0x0003 /**< LED shines green. */
#define MSR_LED_BLUE    0x0004 /**< LED shines blue. */
#define MSR_LED_YELLOW  0x0005 /**< LED shines yellow. */
#define MSR_LED_BLINK   0x0100 /**< LED blinks (500ms on, 500ms off), must be combined with one of the "color defines". */
/** \} */


/** Track data of track 1. */
typedef struct
{
  unsigned char status;     /**< \ref MSR_STATUS_CODES. */
           char data[80];   /**< Track data including start sentinel, end sentinel and LRC, ASCII null-terminated. */
} MSR_Track_1;

/** Track data of track 2. */
typedef struct
{
  unsigned char status;     /**< \ref MSR_STATUS_CODES. */
           char data[41];   /**< Track data including start sentinel, end sentinel and LRC, ASCII null-terminated. */
} MSR_Track_2;

/** Track data of track 3. */
typedef struct
{
  unsigned char status;     /**< \ref MSR_STATUS_CODES. */
           char data[108];  /**< Track data, ASCII null-terminated. */
} MSR_Track_3;

/** Track data. */
typedef struct
{
  MSR_Track_1 t1;  /**< Track data of track 1. */
  MSR_Track_2 t2;  /**< Track data of track 2. */
  MSR_Track_3 t3;  /**< Track data of track 3. */
} MSR_TrackData;

/** Track data including card type. */
typedef struct
{
  unsigned char card_type; /**< \ref MSR_CARD_CODES. */
  MSR_Track_1   t1;        /**< Track data of track 1. */
  MSR_Track_2   t2;        /**< Track data of track 2. */
  MSR_Track_3   t3;        /**< Track data of track 3. */
} MSR_TrackData2;

/** Track data including additional information. */
typedef struct
{
  unsigned char add_info[8]; /**< additional information, add_info[0]: \ref MSR_CARD_CODES, add_info[1]: \ref MSR_TRACK_TYPES, add_info[2..7]: RFU. */
  MSR_Track_1   t1;          /**< Track data of track 1. */
  MSR_Track_2   t2;          /**< Track data of track 2. */
  MSR_Track_3   t3;          /**< Track data of track 3. */
} MSR_TrackData3;

/** Track data including additional information. */
typedef struct
{
  unsigned char add_info[8];   /**< Additional information, add_info[0]: \ref MSR_CARD_CODES, add_info[1]: \ref MSR_TRACK_TYPES, add_info[2..7]: RFU. */
  MSR_Track_1   t1;            /**< Track data of track 1. */
  MSR_Track_2   t2;            /**< Track data of track 2. */
  MSR_Track_3   t3;            /**< Track data of track 3. */
  unsigned char add_info2[8];  /**< Second magstripe: Additional information, add_info[0]: \ref MSR_CARD_CODES, add_info[1]: \ref MSR_TRACK_TYPES, add_info[2..7]: RFU. */
  MSR_Track_1   t21;           /**< Second magstripe: Track data of track 1. */
  MSR_Track_2   t22;           /**< Second magstripe: Track data of track 2. */
  MSR_Track_3   t23;           /**< Second magstripe: Track data of track 3. */
} MSR_TrackData4;

/** Decoded data of track 1. */
typedef struct
{
  unsigned char valid;            /**< 1: data valid, 0: data invalid. */
           char pan[20];          /**< PAN, ASCII null-terminated. */
           char name[27];         /**< Cardholder name, ASCII null-terminated. */
           char exp_date[5];      /**< Expiry date, YYMM, ASCII null terminated. */
           char service_code[4];  /**< Service code, ASCII null terminated. */
           char disc_data[72];    /**< Discretionary data, ASCII null terminated. */
} MSR_Decoded_Track_1;

/** Decoded data of track 2. */
typedef struct
{
  unsigned char valid;            /**< 1: data valid, 0: data invalid. */
           char pan[20];          /**< PAN, ASCII null-terminated. */
           char exp_date[5];      /**< Expiry date, YYMM, ASCII null terminated. */
           char service_code[4];  /**< Service code, ASCII null terminated. */
           char disc_data[35];    /**< Discretionary data, ASCII null terminated. */
} MSR_Decoded_Track_2;

/** Decoded track data. */
typedef struct
{
  MSR_Decoded_Track_1 t1;  /**< Decoded data of track 1. */
  MSR_Decoded_Track_2 t2;  /**< Decoded data of track 2. */
} MSR_DecodedData;

/** Decoded track data. */
typedef struct
{
  MSR_Decoded_Track_1 t1;   /**< Decoded data of track 1. */
  MSR_Decoded_Track_2 t2;   /**< Decoded data of track 2. */
  MSR_Decoded_Track_1 t21;  /**< Second magstripe: Decoded data of track 1. */
  MSR_Decoded_Track_2 t22;  /**< Second magstripe: Decoded data of track 2. */
} MSR_DecodedData4;


/**
 * Type of function that is called for traces, see MSR_SetTraceCallback()
 * \param[in] str : Trace message.
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* MSR_TraceCallback) (const char *str, void *data);

/**
 * Get version of libmsr.
 * \param[out] version : Buffer to store null-terminated version string.
 * \param[in] len : Size of buffer version.
 */
void MSR_Version(char *version, unsigned char len);

/**
 * Set callback function for trace output.
 * \param[in] cbf : Callback function for trace messages, may be NULL.
 * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
 */
void MSR_SetTraceCallback(MSR_TraceCallback cbf, void *cb_data);


#endif  // avoid double include
