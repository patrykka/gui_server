/*
 * VASkernel.h
 *
 *  Created on: 30 Jul 2015
 *      Author: SagiT1
 */

#ifndef SRC_VASKERNEL_H_
#define SRC_VASKERNEL_H_

#define MAX_APDU_BUFF_SIZE 255
#define DEFAULT_CHUNK_SIZE	0xFF

#include "exp_imp_vas_kernel.h"

typedef enum
{
	VASKERNEL_STATUS_SUCCESS	= 0,
	VASKERNEL_STATUS_DATA_ERROR	= -1,
	VASKERNEL_STATUS_COMM_ERROR = -2,
	VASKERNEL_STATUS_ERROR      = -3
}VASKERNEL_STATUS;

typedef enum
{
	TRANSMIT_APDU,

}VASKERNEL_ACTIONS;


class VAS_KRNL_DECL_SPEC actionParams
{
public:
	byte	m_maxAttempts;
	rawData	m_rdInput;
	virtual ~actionParams();
};

class VAS_KRNL_DECL_SPEC actionResults
{
public:
	buffData	m_bfResult;
	virtual ~actionResults();
};

class VAS_KRNL_DECL_SPEC APDUparams: public actionParams
{

public:
	APDUparams(const byte Class, const byte Inst, const byte P1, const byte P2, const rawData rdData, const int expectedRetSize, const byte maxAtempts = 1);
	byte	m_Class;
	byte	m_Instruction;
	byte	m_P1;
	byte	m_P2;
	int		m_expectedSize;
	byte	chunkSize;
};

class VAS_KRNL_DECL_SPEC APDUresults: public actionResults
{
public:
	APDUresults();
	void operator=(APDU::APDU_RESPONSE& apduRes);
	byte		m_sw1;
	byte		m_sw2;
	uint16_t	sw;
};

VAS_KRNL_DECL_SPEC VASKERNEL_STATUS	vasKernelDoAction(VASKERNEL_ACTIONS action, actionParams& indata, actionResults& outdata);

VAS_KRNL_DECL_SPEC VASKERNEL_STATUS	vasKernelDoAction(VASKERNEL_ACTIONS action, actionParams& actionParams,rawData& rd_dataToSend, buffData& bddataReseived, actionResults& outdata);

VAS_KRNL_DECL_SPEC VASKERNEL_STATUS	transmitAPDU(APDUparams& inParams, APDUresults& outResults);

//NFC_FUNCTION_ATTRIBUTE void getVersion(rawData* rdOutVersion);

#endif /* SRC_VASKERNEL_H_ */
