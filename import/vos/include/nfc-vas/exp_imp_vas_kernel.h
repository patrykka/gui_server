
#ifndef EXPORT_IMPORT_VAS_KERNEL_H_
#define EXPORT_IMPORT_VAS_KERNEL_H_


#if defined(__GNUC__) || defined(__GNUG__)

#define VAS_KRNL_DECL_SPEC

#else

#ifdef _BUILDING_NFC_VERIX_VAS_KERENL_LIB_

#define VAS_KRNL_DECL_SPEC   __declspec(dllexport)

#else

#define VAS_KRNL_DECL_SPEC   __declspec(dllimport)

#endif

#endif

#endif // EXPORT_IMPORT_VAS_KERNEL_H_
