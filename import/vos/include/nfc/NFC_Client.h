/*************************************************************
* FILE NAME:   NFC_Client.h                                  *
* MODULE NAME: NFC Client                                    *
* PROGRAMMER:  Evgeny Sokolovsky                             *
* DESCRIPTION:                                               *
* REVISION:                                                  *
*************************************************************/
#ifndef NFCCLIENT_H_
#define NFCCLIENT_H_

#include "export_import.h"

typedef enum
{
	CL_STATUS_SUCCESS = 0x00,
	CL_STATUS_CARD_ERROR,	    		//0x01
	CL_STATUS_NOT_SUPPORTED,    		//0x02
	CL_STATUS_TIMEOUT,		    		//0x03
	CL_STATUS_DATA_ERROR,	    		//0x04
	CL_STATUS_SIZE_ERROR,	    		//0x05
	CL_STATUS_INIT_ERROR,	    		//0x06
	CL_STATUS_IPC_ERROR,	    		//0x07
	CL_STATUS_GENERAL_ERROR,    		//0x08
	CL_STATUS_PROT_ERROR,	    		//0x09
	CL_STATUS_INTERNAL_ERROR,   		//0x0A
	CL_STATUS_NFC_INCOMPATABLE,  		//0x0B
	CL_STATUS_NFC_INITILIZED_ALREADY  	//0x0C
}CL_STATUS;

typedef enum
{
	CL_TYPE_CLIENT_SERVER =0,
	CL_TYPE_FUNCTION
}CONNECTION_TYPE;

#ifdef __cplusplus
extern "C" {
#endif
/* --------------------------------------------------------------------------
 * FUNCTION NAME: NFC_Client_Init
 * DESCRIPTION:
 * PARAMETERS:
 * RETURN:
 * NOTES: establish a connection according with CONNECTION_TYPE
 * ------------------------------------------------------------------------ */
NFC_FUNCTION_ATTRIBUTE CL_STATUS NFC_Client_Init(CONNECTION_TYPE type);
NFC_FUNCTION_ATTRIBUTE CL_STATUS NFC_Client_Init_CheckVer(CONNECTION_TYPE type, int maj, int min, int bld); 
/* --------------------------------------------------------------------------
 * FUNCTION NAME: NFC_SerialOpen
 * DESCRIPTION:
 * PARAMETERS:
 * RETURN:
 * NOTES:
 * ------------------------------------------------------------------------ */
NFC_FUNCTION_ATTRIBUTE CL_STATUS NFC_SerialOpen(void);

/* --------------------------------------------------------------------------
 * FUNCTION NAME: NFC_SerialClose
 * DESCRIPTION:
 * PARAMETERS:
 * RETURN:
 * NOTES:
 * ------------------------------------------------------------------------ */
NFC_FUNCTION_ATTRIBUTE void 	NFC_SerialClose(void);

#ifdef __cplusplus
}
#endif

#endif //NFCCLIENT_H_
