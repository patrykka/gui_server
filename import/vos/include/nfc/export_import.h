
#ifndef EXPORT_IMPORT_H_
#define EXPORT_IMPORT_H_

#ifndef _VERIX_
	#ifdef _VRXEVO
		#define _VERIX_
	#endif
#endif

#ifdef _VERIX_

	#ifdef _BUILDING_NFC_VERIX_LIBRARIES_
		#define NFC_CLASS_ATTRIBUTE     __declspec(dllexport)
		#define NFC_FUNCTION_ATTRIBUTE  __declspec(dllexport)
		#define NFC_VARIABLE_ATTRIBUTE  __declspec(dllexport)
	#else
		#define NFC_CLASS_ATTRIBUTE     __declspec(dllimport)
		#define NFC_FUNCTION_ATTRIBUTE  __declspec(dllimport)
		#define NFC_VARIABLE_ATTRIBUTE  __declspec(dllimport)
	#endif

	#ifdef _BUILD_NFC_UTILS_VERIX_LIBRARIES_
		#define NFC_UTILS_CLASS_ATTRIBUTE     __declspec(dllexport)
		#define NFC_UTILS_FUNCTION_ATTRIBUTE  __declspec(dllexport)
		#define NFC_UTILS_VARIABLE_ATTRIBUTE  __declspec(dllexport)
	#else
		#define NFC_UTILS_CLASS_ATTRIBUTE     __declspec(dllimport)
		#define NFC_UTILS_FUNCTION_ATTRIBUTE  __declspec(dllimport)
		#define NFC_UTILS_VARIABLE_ATTRIBUTE  __declspec(dllimport)
	#endif

#elif _TRINITY

	#if defined _WIN32 
		#define NFC_CLASS_ATTRIBUTE     __declspec(dllexport)
		#define NFC_FUNCTION_ATTRIBUTE  __declspec(dllexport)
		#define NFC_VARIABLE_ATTRIBUTE  __declspec(dllexport)

		#define NFC_UTILS_CLASS_ATTRIBUTE     __declspec(dllexport)
		#define NFC_UTILS_FUNCTION_ATTRIBUTE  __declspec(dllexport)
		#define NFC_UTILS_VARIABLE_ATTRIBUTE  __declspec(dllexport)
	#elif defined __GNUC__
		#define NFC_CLASS_ATTRIBUTE				__attribute__((visibility ("default")))
		#define NFC_FUNCTION_ATTRIBUTE			__attribute__((visibility ("default")))
		#define NFC_VARIABLE_ATTRIBUTE			__attribute__((visibility ("default")))

		#define NFC_UTILS_CLASS_ATTRIBUTE		__attribute__((visibility ("default")))
		#define NFC_UTILS_FUNCTION_ATTRIBUTE	__attribute__((visibility ("default")))
		#define NFC_UTILS_VARIABLE_ATTRIBUTE	__attribute__((visibility ("default")))
	#else
		#define NFC_CLASS_ATTRIBUTE
		#define NFC_FUNCTION_ATTRIBUTE
		#define NFC_VARIABLE_ATTRIBUTE

		#define NFC_UTILS_CLASS_ATTRIBUTE
		#define NFC_UTILS_FUNCTION_ATTRIBUTE
		#define NFC_UTILS_VARIABLE_ATTRIBUTE

	#endif
#else
	#define NFC_CLASS_ATTRIBUTE
	#define NFC_FUNCTION_ATTRIBUTE
	#define NFC_VARIABLE_ATTRIBUTE

	#define NFC_UTILS_CLASS_ATTRIBUTE
	#define NFC_UTILS_FUNCTION_ATTRIBUTE
	#define NFC_UTILS_VARIABLE_ATTRIBUTE
#endif


#endif // EXPORT_IMPORT_H_
