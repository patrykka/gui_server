/*
 * log_adk.h
 *
 *  Created on: Mar 19, 2018
 *      Author: IdanO2
 */

#ifndef SRC_LOG_ADK_H_
#define SRC_LOG_ADK_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdarg.h>
#include "export_import.h"

#define	LOG_ERROR_MASK		0x00000001
#define	LOG_WARNING_MASK	0x00000002
#define	LOG_INFO_MASK		0x00000004
#define	LOG_DEBUG_MASK		0x00000008
#define	LOG_PERFORMANCE_MASK	0x00000010

NFC_UTILS_FUNCTION_ATTRIBUTE int adk_log_init(void);
NFC_UTILS_FUNCTION_ATTRIBUTE void log_message(unsigned int log_level, const char* log_prefix, unsigned int log_bit_map, const char* file_name,  int line, const char* format, ...);
NFC_UTILS_FUNCTION_ATTRIBUTE void dump_message(char* title, void* buff, unsigned int len);
NFC_UTILS_FUNCTION_ATTRIBUTE int is_trace(unsigned int log_mask, unsigned int log_bit_map);
NFC_UTILS_FUNCTION_ATTRIBUTE void log_message_internal(unsigned int log_level, const char* file_name,  int line, const char* format, va_list args);

#ifdef __cplusplus
}
#endif

#endif /* SRC_LOG_ADK_H_ */
