/*
 * utils.h
 *
 *  Created on: Mar 10, 2016
 *      Author: idano2
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include <time.h>
#include "export_import.h"

#include "log_adk.h"
//#include "liblog2.h"
//extern LibLogHandle logHandle;

#define	JASON_KEY_TERM_CONF		"Terminal_Configuration"
#define JSON_KEY_LOG			"Log"
#define JSON_KEY_LOG_LEVEL		"Log_Level_Bit_Map"
#define DUMP_WIDTH 16

#define	LOG_ERROR_MASK		0x00000001
#define	LOG_WARNING_MASK	0x00000002
#define	LOG_INFO_MASK		0x00000004
#define	LOG_DEBUG_MASK		0x00000008
#define	LOG_PERFORMANCE_MASK	0x00000010

extern NFC_UTILS_VARIABLE_ATTRIBUTE usint logBitMap;

#define LOG_PREFIX_ERR 	"ERROR-"
#define LOG_PREFIX_WAR	"WARNING-"
#define LOG_PREFIX_DBG 	"DEBUG-"
#define LOG_PREFIX_INFO "INFO-"
#define LOG_PREFIX_PERF "PERFORMANCE-"

//Macro
#ifndef MAX
	#define MAX(a,b) (a > b ? a : b)
#endif

#ifndef MIN
	#define MIN(a,b) (a < b ? a : b)
#endif

#define	IS_DEBUG() is_trace(LOG_DEBUG_MASK, logBitMap)

#define	LOG_ERR_W_NAME(...)     log_message(LOG_ERROR_MASK,  	LOG_PREFIX_ERR, 	logBitMap, __FILE__, __LINE__, __VA_ARGS__); 		//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_ERR, __VA_ARGS__);
#define	LOG_DBG_W_NAME(...) 	log_message(LOG_DEBUG_MASK,  	LOG_PREFIX_DBG, 	logBitMap, __FILE__, __LINE__,__VA_ARGS__);		//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_DBG, __VA_ARGS__);
#define	LOG_INFO_W_NAME(...) 	log_message(LOG_INFO_MASK,    	LOG_PREFIX_INFO, 	logBitMap, __FILE__, __LINE__,__VA_ARGS__);		//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_INFO, __VA_ARGS__);
#define	LOG_WRN_W_NAME(...) 	log_message(LOG_WARNING_MASK, 	LOG_PREFIX_WAR, 	logBitMap, __FILE__, __LINE__,__VA_ARGS__);		//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_WAR, __VA_ARGS__);
#define	LOG_PERF_W_NAME(...) 	log_message(LOG_PERFORMANCE_MASK, LOG_PREFIX_PERF, 	logBitMap, __FILE__, __LINE__,__VA_ARGS__);	//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_PERF, __VA_ARGS__);
#define	DEBUG_START_FUNC() 		log_message(LOG_DEBUG_MASK,  	LOG_PREFIX_DBG, 	logBitMap, __FILE__, __LINE__,"start\n");			//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_DBG, "start\n");
#define	DEBUG_END_FUNC()		log_message(LOG_DEBUG_MASK,  	LOG_PREFIX_DBG, 	logBitMap, __FILE__, __LINE__,"finish\n");			//LOG_W_NAME(LOG_ERROR_MASK, LOG_PREFIX_DBG, "finish\n");

#define	LOG_DEBUG(...) 			LOG_DBG_W_NAME(__VA_ARGS__)//log_message(LOG_DEBUG_MASK, "",  __LINE__, __VA_ARGS__);
#define	LOG_ERR(...) 			LOG_ERR_W_NAME(__VA_ARGS__)//log_message(LOG_ERROR_MASK, __func__,   __LINE__,__VA_ARGS__);

//#define	DUMP_BUFF(buff, title) dumpBuff(buff, (char*)title)
#define	DEBUG_DUMP_BUFF(buff, title) dumpBuff(buff, (char*)title)
#define	DEBUG_DUMP_BUFF_LEN(buff, len, title) dumpBuff(buff, len, (char*)title)

#define	SET_T(t) if ((LOG_PERFORMANCE_MASK&logBitMap) != 0) {t=clock();}
//#define	PRINT_PERF(str, t1, t2)  if ((LOG_PERFORMANCE_MASK&logBitMap) != 0) {myPrintf("%s\n", str); myPrintf("clocks %d\n", t2-t1) ; myPrintf(" time: %d") << (((float)(t2-t1))/1000.0) << "ms"<< std::endl;}

#define FOR_EACH_IN_VEC(index, vector) for(usint index=0; index < vector.size(); index++)
#define FOR_EACH_IN_VEC_COND(index, vector, cond) for(usint index=0;(index < vector.size())&&(cond); index++)

#define FOR_EACH(index, size) for(usint index=0; index < size; index++)
#define	IS_VAL_IN_VECTOR(vec, val) (std::find(vec.begin(),vec.end(),val)!=vec.end() ? true:false)
#define ARRAY_ENTRIES(a) (sizeof(a)/sizeof(a[0]))

//exported function in Verix
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(rawData& buff, char* title=NULL);
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(buffData& buff, char* title=NULL);
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(char* buff, usint len, char* title=NULL);
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(byte* buff, usint len, char* title);
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(std::string& str, char* title);
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(std::vector<uint8_t>& vector, char* title);
NFC_UTILS_FUNCTION_ATTRIBUTE void 	dumpBuff(std::vector<int>& vector, char* title);

NFC_UTILS_FUNCTION_ATTRIBUTE void 	SetCommHandle( int hdl );
NFC_UTILS_FUNCTION_ATTRIBUTE int  	getCommHandle();
//NFC_UTILS_FUNCTION_ATTRIBUTE FILE 	*popen ( char *__command, char *__modes);
//NFC_UTILS_FUNCTION_ATTRIBUTE int 		pclose (void *__stream);
NFC_UTILS_FUNCTION_ATTRIBUTE char* 	myGets(char* s, int maxSize);
NFC_UTILS_FUNCTION_ATTRIBUTE int		myPrintf( const char* fmt, va_list argList);
NFC_UTILS_FUNCTION_ATTRIBUTE int 		myPrintf(const char * fmt, ...);
NFC_UTILS_FUNCTION_ATTRIBUTE time_t 	time_iso8601_to_epoch(std::string& iso8601TimeStr);
NFC_UTILS_FUNCTION_ATTRIBUTE int 		getFileSize(std::string& fileName);
NFC_UTILS_FUNCTION_ATTRIBUTE int 		utilOpenFile(std::string& fileName, int flags);
NFC_UTILS_FUNCTION_ATTRIBUTE int 		utilCloseFile(int fd);
NFC_UTILS_FUNCTION_ATTRIBUTE std::string prv_lltoa(unsigned long long val, int base);

NFC_UTILS_FUNCTION_ATTRIBUTE std::vector<std::string> getFileNameFormDir(std::string& dirPath);
NFC_UTILS_FUNCTION_ATTRIBUTE int utils_init_adk_log();

//un exported function in Verix
NFC_UTILS_FUNCTION_ATTRIBUTE void 			buffToHexString(byte* buff, usint len, std::string& outStr);
NFC_UTILS_FUNCTION_ATTRIBUTE void 			buffToHexString(buffData& buff, std::string& outStr);
NFC_UTILS_FUNCTION_ATTRIBUTE void 			buffToHexString(usint& buff, std::string& outStr);

NFC_UTILS_FUNCTION_ATTRIBUTE buffData 		stringHexToBuffData(const std::string& stringIn);
NFC_UTILS_FUNCTION_ATTRIBUTE buffData 		stringHexToBuffData(const char* stringIn, const usint stringInLen);

NFC_UTILS_FUNCTION_ATTRIBUTE bool			strToUsint(const std::string& stringIn, usint& outVal);
NFC_UTILS_FUNCTION_ATTRIBUTE bool			strToBcd(const std::string& stringIn, buffData& outVal);

NFC_UTILS_FUNCTION_ATTRIBUTE bool 			configUtils(void* configData);
NFC_UTILS_FUNCTION_ATTRIBUTE bool 			configUtils(rawData* configData);
NFC_UTILS_FUNCTION_ATTRIBUTE usint 			getLogBitMap();

NFC_UTILS_FUNCTION_ATTRIBUTE unsigned long long int binaryBuffToUllint(buffData& binaryBuff);
NFC_UTILS_FUNCTION_ATTRIBUTE unsigned long long int bcdToUllint(buffData& bcdBuff);
NFC_UTILS_FUNCTION_ATTRIBUTE std::string  	ullintToString(unsigned long long int val);

NFC_UTILS_FUNCTION_ATTRIBUTE usint 			binaryBuffToUsint(char* data, usint len);
NFC_UTILS_FUNCTION_ATTRIBUTE usint 			binaryBuffToUsint (buffData& binaryBuff);

NFC_UTILS_FUNCTION_ATTRIBUTE std::string  	usintToString(usint val);

NFC_UTILS_FUNCTION_ATTRIBUTE std::string& 	IntToHexStr(int val, std::string& outStr);
NFC_UTILS_FUNCTION_ATTRIBUTE bool 			isvalidBcd(buffData& bcdBuff);

NFC_UTILS_FUNCTION_ATTRIBUTE ulint 			get_ms();
NFC_UTILS_FUNCTION_ATTRIBUTE char* 			prv_itoa(int num, char* str, int base);
NFC_UTILS_FUNCTION_ATTRIBUTE void* 			prv_dlopen(const char *file, int mode);
NFC_UTILS_FUNCTION_ATTRIBUTE void* 			prv_dlsym (void *handle, const char *name);
NFC_UTILS_FUNCTION_ATTRIBUTE std::string 	get_dlerror (void);

NFC_UTILS_FUNCTION_ATTRIBUTE bool 			isHexString(std::string& str);


#ifndef _VERIX_
unsigned long read_ticks(void);
#endif // _VERIX_

#endif /* SRC_UTILS_H_ */
