/******************************************************************
 Copyright (C) 2015 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in
 any form or by any means  whether electronic, mechanical,  magnetic,
 optical, or otherwise, without the express prior written permission
 of VeriFone, Inc.
 *******************************************************************/
#ifndef _PP1000_INTERFACE_H
#define _PP1000_INTERFACE_H

#ifdef _VRXEVO
  #if defined(PP1000_LIB_BUILD)
    #define VX_DECL_SPEC	__declspec(dllexport)
  #else
    #define VX_DECL_SPEC	__declspec(dllimport)
  #endif
#else
  #define VX_DECL_SPEC
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef U8
typedef unsigned char U8;
#endif

enum pp1000_ErrorCodes
{
	PP1000_ERR_NONE 			= 0, 
	PP1000_ERR_INVALID_PARAMS 	= -1, 
	PP1000_ERR_INVALID_PKT 		= -2, 
	PP1000_ERR_INVALID_RES 		= -3, 
	PP1000_ERR_INVALID_STATE 	= -4, 
	PP1000_ERR_NOT_SUPPORTED 	= -5, 
	PP1000_ERR_COM 				= -6,
	PP1000_ERR_RECV_EOT 		= -7,
	PP1000_ERR_PROTOCOL 		= -8,
	PP1000_ERR_DATA_LENGTH 		= -9,
	PP1000_ERR_APP_ABORT 		= -10
};

/* Callback function pointer for COM send */
typedef int (*comSendCallback)(void *handle, const void *buffer, int size);

/* Callback function pointer for COM receive */
typedef int (*comReceiveCallback)(void *handle, void *buffer, int size);

/* Callback function pointer for notification */
typedef int (*notifyCallback)(int state, void **, int *size);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_readCertificate
 * DESCRIPTION:   	Wrapper to OS function pp_readCertificate.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_readCertificate(int level, U8* data, int max);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_loadCertificate
 * DESCRIPTION:   	Wrapper to OS function pp_loadCertificate.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_loadCertificate(int level, const U8* data, int len);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_resolveNS
 * DESCRIPTION:   	Wrapper to OS function pp_resolveNS.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_resolveNS(int mode, const U8 *crypto1, int crypto1Len, U8 *crypto2, int *crypto2Len);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_verifyNS
 * DESCRIPTION:   	Wrapper to OS function pp_verifyNS.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_verifyNS(int mode, const U8 *crypto3);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_generateChallenge
 * DESCRIPTION:   	Wrapper to OS function pp_generateChallenge.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_generateChallenge(int mode, U8 *challenge, U8 *workingKey);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_resolveChallenge
 * DESCRIPTION:   	Wrapper to OS function pp_resolveChallenge.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_resolveChallenge(int mode, const U8 *challenge, const U8 *workingKey, U8 *result);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_verifyChallenge
 * DESCRIPTION:   	Wrapper to OS function pp_verifyChallenge.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_verifyChallenge(int mode, const U8 *challenge);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_acceptPin
 * DESCRIPTION:   	Accepts PIN from the user and stores it in OS buffer.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */

VX_DECL_SPEC int pp1000_acceptPin(int mode, char pinflag, int minpinlen, int maxpinlen, char nullpinflag);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_getPin
 * DESCRIPTION:   	Returns PIN block encrypted with working key.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */

VX_DECL_SPEC int pp1000_getPin(int mode, const U8 *workingKey, U8 *pinBlock);
/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_cancelPin
 * DESCRIPTION:   	Cancels PIN entry session on PP1000
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */

VX_DECL_SPEC int pp1000_cancelPin(void);
/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_registerComs
 * DESCRIPTION:   	Registers Send and Recieve callbacks with PP1000 library.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */

VX_DECL_SPEC int pp1000_registerComs(comSendCallback sendcb, comReceiveCallback receivecb);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_registerNotify
 * DESCRIPTION:   	Registers notifiers to the PP1000 library.
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC int pp1000_registerNotify(notifyCallback notifycb);

/* --------------------------------------------------------------------------
 * FUNCTION NAME:	pp1000_libVersion
 * DESCRIPTION:   	Returns the library version
 * PARAMETERS:    	
 * RETURN:        	
 * ------------------------------------------------------------------------ */
VX_DECL_SPEC const char *pp1000_libVersion(void);


#ifdef __cplusplus
}
#endif

#endif //_PPCOM_INTERFACE_ H

