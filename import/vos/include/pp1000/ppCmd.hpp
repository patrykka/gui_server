/******************************************************************
 Copyright (C) 2008-2010 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in
 any form or by any means  whether electronic, mechanical,	magnetic,
 optical, or otherwise, without the express prior written permission
 of VeriFone, Inc.
 *******************************************************************/

#ifndef PPCMD_H
#define PPCMD_H

#include <ppPkt.hpp>
#include <pp1000_interface.h>

// COM size buffer is increased from 8KB to 32KB to support RKL.
// PPSENG-759
#define MAX_PKT_SIZE 32768

/* Since d_ptr is a pointer and is never referended in header file
(it would cause a compile error) ppCmdData doesn't have to be included,
but forward-declared instead.
 */
 
class ppCmdData;

class VX_DECL_SPEC ppCmd
{
public:
	// This ENUM is for type of protocol we support.
	// In Generall PP1000 protocol packets are of different types
	// Categorized based on the way we receive the responses
	// Some packets receive an ACK some dont etc.
	// As of today we dont use these but they are defined for future use. For extending the classes
	enum protocolType
	{
		PP_RECV_ACK_ONLY,    // Packet request will be ACKed back, no response
		PP_RECV_RESP_ACK,    // Packet has a response and need to ACK back
		PP_RECV_RESP_EOT,    // Packet has a response and receives EOT
        PP_RECV_CUSTOM       // Need defined by user
	};

	// Constructor
    ppCmd(enum protocolType);
	// Destructor
	virtual ~ppCmd(void);
	// Will be called by the application. Will pass the structure to be filled with the response
	virtual int execute(void *data, int size);
	// Registers notify callback method
	static int registerNotifyCB(notifyCallback cb);
	// Get reference to requets type
	ppPkt * getRequestPkt();
	// Get reference to requets type
	ppPkt * getResponsePkt();
	// API to set the protocol type
	void setProtocolType(enum protocolType);
	// API to get the protocol type
	enum protocolType getProtocolType() const;
	
protected:
	// Enum for identifying the protocol states
	enum protocolStates {
		PP_CMD_STATE_SENT_REQ = 0,	// After the request is been sent to PP
		PP_CMD_STATE_RECD_ACK = 1,	// After receving an ACK for the request sent
		PP_CMD_STATE_RECD_RES = 2,	// After receiving a response 
		PP_CMD_STATE_SENT_ACK = 3,	// After sending an ACK for the response
		PP_CMD_STATE_RESP_END = 4	// End of state machine. Either after reciving EOT or error
	};
		
	// API to create a packet
	virtual int createPkt();
	
	// API to create a packet with input data
	virtual int createPkt(void *data, int size);
	virtual int sendPkt();
	virtual int receivePkt();
	virtual int processPkt(void *data, int size);
	
private:
	// d_ptr to access ppCmdPrivate class memebers
	ppCmdData *m_ppCmdPtr;
};

#endif // PPCMD_H

