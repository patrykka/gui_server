/******************************************************************
 Copyright (C) 2008-2010 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in
 any form or by any means  whether electronic, mechanical,	magnetic,
 optical, or otherwise, without the express prior written permission
 of VeriFone, Inc.
 *******************************************************************/

#ifndef PPPKT_H
#define PPPKT_H

// Standard Includes
#include <string>

// PPSecure Pin Includes
#include <pp1000_interface.h>

using std::string;

enum pktType
{
	STX_ETX = 0, SI_SO = 1
};

// This is an ENUM which identifies a variable in a string
enum pktFieldType
{
	// This is the field name of the packet. It is of length 3 bytes. Eg : S01, S02, Z2, M04 etc
	FLD_HDR,
	// This is a ASCII Hex string which needs to be set/get to/from the packet
	FLD_DATA_STR,
	// This is a Binary data which needs to be converted to Ascii Hex set/get to/from packet
	FLD_DATA_BIN,
	//This field used to add one control byte. Eg : <FS>
	FLD_BYTE, 
	//This field defines number to be added to the packet stream. The number is converted to ascii and added 
	FLD_DATA_NUM,
	//This field defines the end of the packet and appends LRC.
	FLD_PKT_END,
};

//This is an enum which defines Control bytes for Packet communication
enum pktControlBytes
{
	CTRL_NULL = 0x00, CTRL_SI = 0x0F, CTRL_SO = 0x0E, CTRL_STX = 0x02, 
	CTRL_ETX = 0x03, CTRL_ENQ = 0x05, CTRL_ACK = 0x06, CTRL_EOT = 0x04, 
	CTRL_NAK = 0x15, CTRL_SUB = 0x1A, CTRL_FS = 0x1C, CTRL_US = 0x1F,
	CTRL_DELIMTR = 0x2E
};

/* Since d_ptr is a pointer and is never referended in header file
(it would cause a compile error) ppPktData doesn't have to be included,
but forward-declared instead.
 */

class ppPktData;

class VX_DECL_SPEC ppPkt
{
	
public:

	// Default Constructor
	ppPkt(void);

	// Constructor accepts packet type
	ppPkt(enum pktType);

	// Destructor
	~ppPkt(void);

	// API to set the packet type. This is required only if you dont know the packet type at creation
	// This will create a packet based on type.
	void setPacketType(enum pktType type);

	// API to change the packet type.Default packet type is STX_ETX.
	// Note that this will not create any packet
	void setType(enum pktType type);

	// API to get the packet type.
	enum pktType getPacketType() const;
	
	// Sets the field in the string. All the data will be added as a ASCII Hex only
	int setField(enum pktFieldType fieldtype, void *data = NULL, int insize = 0);

	//Return const pointer to the packet stream
	const char * getData();
	
	// Returns the complete data as a string
	int getData(string &reqdata);

	//Returns chunks of code in case you need to extract only the part of the buffer
	int getData(void *data, int start, int end);

	// Appends the data passed to a string
	int setData(void *data, int insize);

	//Validates packet format and LRC
	bool isValid();

	//Get Packet length
	int getPacketLength() const;

	//Clears the packet data
	void clear();
	
private:
	// d_ptr to access ppPktPrivate class memebers
	ppPktData *m_ppPktPtr;

	
};

#endif // PPPKT_H

