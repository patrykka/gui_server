/****************************************************************************
*  Product:     ADK Secure Data Interface (SDI)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Client (structure) interface - Data functions
****************************************************************************/

#ifndef _SDI_DATA_H_   /* avoid double interface-includes */
#define _SDI_DATA_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "emv/EMV_CT_Interface.h"
#include "emv/EMV_CTLS_Interface.h"


/// @defgroup FETCH_TAGS_MODES
/// @brief Modes for handling of sensitive tags by SDI_fetchTxnTags()
#define SENSITIVE_TAG_OMIT          0  // Sensitive tags are not returned in the response
#define SENSITIVE_TAG_TRUNCATE      1  // Sensitive tags are returned with zero length
#define SENSITIVE_TAG_REPLACE       2  // Sensitive tags are returned with data replaced by 0xFF bytes and original length


/*****************************************************************************
*  SDI_fetchTxnTags
*****************************************************************************/
/// @brief Fetch one or several tags at the end of the transaction, excluding sensitive cardholder data.
///
/// One common function for all technologies: CT, CTLS, MSR and manual entry.
/// Sensitive tags are excluded from the returned data in the way defined by the parameter @mode.
/// The SDI Server has a predefined set of sensitive tags. Additionally, the user can configure additional
/// sensitive tags using the Software and Configuration Upload command set 20-14, 20-15 and 20-16 as described
/// in the SDI Server documentation.
/// The user can also configure a white list using the same Software and Configuration Upload commands. If a
/// card is in the configured white list, all data is returned in clear text, including also sensitive tags.
///
/// @author GSS R&D Germany
///
/// @param[in]     mode               ... Sensitive tags mode, see @ref FETCH_TAGS_MODES
/// @param[in]     requestedTags      ... requested tags
/// @param[in]     noOfRequestedTags  ... number of requested tags
/// @param[in,out] tlvBuffer          ... buffer for TLV output, allocated by calling application
/// @param[in]     bufferLength       ... number of bytes allocated for tlvBuffer
/// @param[out]    tlvDataLength      ... number of bytes written to tlvBuffer
///
/// @return #EMV_ADK_OK, #EMV_ADK_PARAM, #EMV_ADK_TLV_BUILD_ERR, #EMV_ADK_INTERNAL
///
EMV_ADK_INFO SDI_fetchTxnTags(unsigned char mode, unsigned long* requestedTags, unsigned short noOfRequestedTags, unsigned char* tlvBuffer, unsigned short bufferLength, unsigned short* tlvDataLength);


#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _SDI_DATA_H_
