/****************************************************************************
*  Product:     ADK Secure Data Interface (SDI)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Client (structure) interface - EMV functions
****************************************************************************/

#ifndef _SDI_CLIENT_EMV_INTERFACE_H_   /* avoid double interface-includes */
#define _SDI_CLIENT_EMV_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "emv/EMV_SDI.h"
#include "emv/EMV_CT_Interface.h"
#include "emv/EMV_CT_Link.h"
#include "emv/EMV_CTLS_Interface.h"
#include "emv/EMV_CTLS_Link.h"


// ==================================================================================================================================================
/// @brief Initialize SDI client
///
/// @author GSS R&D Germany
///
/// @param[in]  options  future use
///
/// @return 0=okay, else error
///
int SDI_Client_Init(const char* options);

// ==================================================================================================================================================
EMV_ADK_INFO SDI_CT_Init_Framework_Client(const char *version, unsigned char numberOfAIDs, EMV_CT_CALLBACK_FnT EMV_Callback, void* externalData, unsigned long options);
#define SDI_CT_Init_Framework(numberOfAIDs, EMV_Callback, externalData, options) SDI_CT_Init_Framework_Client(EMV_CT_FRAMEWORK_VERSION, (numberOfAIDs), (EMV_Callback), (externalData), (options))
void SDI_CT_Exit_Framework(void);
void SDI_CT_Exit_Framework_extended(unsigned char options);

const char* SDI_CT_CLIENT_GetVersion(void);
const char* SDI_CT_FRAMEWORK_GetVersion(void);

EMV_ADK_INFO SDI_CT_MapVirtualTerminal(EMV_ADK_VIRTUALTERMMAP_TYPE VirtualTermMapType, unsigned char *TLVSwitchValue, unsigned int TLVBufLen, unsigned char VirtualTerminal);

EMV_ADK_INFO SDI_CT_StoreCAPKey(EMV_ADK_HANDLE_RECORD_TYPE eHandleCAPKeyType, const EMV_CT_CAPKEY_TYPE *pxKeyData);
EMV_ADK_INFO SDI_CT_ReadCAPKeys(EMV_CT_CAPREAD_TYPE *pxKeyData, unsigned char *pucMaxnum);
EMV_ADK_INFO SDI_CT_SetTermData(EMV_CT_TERMDATA_TYPE* pxTermData);
EMV_ADK_INFO SDI_CT_GetTermData(EMV_CT_TERMDATA_TYPE* pxTermData);
EMV_ADK_INFO SDI_CT_SetAppliData(EMV_ADK_HANDLE_RECORD_TYPE eHandleAppliType, EMV_CT_APPLI_TYPE* pxAID, EMV_CT_APPLIDATA_TYPE* pxAppliData);
EMV_ADK_INFO SDI_CT_GetAppliData(EMV_ADK_READAPPLI_TYPE eReadAppliType, EMV_CT_APPLI_TYPE* pxAID, EMV_CT_APPLIDATA_TYPE* pxAppliData);
EMV_ADK_INFO SDI_CT_ApplyConfiguration(unsigned long options);

EMV_ADK_INFO SDI_CT_StartTransaction(EMV_CT_SELECT_TYPE *pxSelectInput, EMV_CT_SELECTRES_TYPE* pxSelectRes);
EMV_ADK_INFO SDI_CT_GetCandidateData(EMV_CT_CANDIDATE_DATA_TYPE *candidateData);
EMV_ADK_INFO SDI_CT_ContinueOffline(EMV_CT_TRANSAC_TYPE *pxTransactionInput, EMV_CT_TRANSRES_TYPE *pxTransRes, EMV_SDI_CT_TRANSRES_TYPE *pxSdiTransRes);
EMV_ADK_INFO SDI_CT_ContinueOnline(EMV_CT_HOST_TYPE *pxOnlineInput, EMV_CT_TRANSRES_TYPE *pxTransRes, EMV_SDI_CT_TRANSRES_TYPE *pxSdiTransRes);
EMV_ADK_INFO SDI_CT_updateTxnTags(unsigned long options, unsigned char *tlvBuffer, unsigned short bufferLength);
EMV_ADK_INFO SDI_CT_CheckSupportedAID(const EMV_CT_APPLI_TYPE* aid, unsigned char ASI, const unsigned char* defaultLabel, EMV_CT_CandListType* pCandList,
                                      unsigned char MaxCand, unsigned char* pCandidateCount, unsigned short* sw12, const unsigned char* adtCardTagList);
EMV_ADK_INFO SDI_CT_EndTransaction(unsigned long options);

unsigned char SDI_CT_SmartISO (unsigned char ucOptions, unsigned short usInDataLen, unsigned char *pucDataIn, unsigned short *pusOutDataLen, unsigned char *pucDataOut, unsigned short usOutBufferLength);
unsigned char SDI_CT_SmartDetect(unsigned char ucOptions);
unsigned char SDI_CT_SmartReset(unsigned char ucOptions, unsigned char* pucATR, unsigned long* pnATRLength);
unsigned char SDI_CT_SmartPowerOff(unsigned char ucOptions);

unsigned char SDI_CT_Send_PIN_Offline(unsigned char *pucPINResultData);

unsigned char SDI_CT_LED(unsigned char ucLedId, unsigned char ucLedState, unsigned char ucLedColor, unsigned long ulTimeoutMs);


// ==================================================================================================================================================
EMV_ADK_INFO  SDI_CT_SER_Init_Framework    (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
void          SDI_CT_SER_Exit_Framework    (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_MapVirtualTerminal(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_StoreCAPKey       (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_ReadCAPKeys       (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_SetTermData       (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_GetTermData       (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_SetAppliData      (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_GetAppliData      (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_StartTransaction  (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_GetCandidateData  (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_ContinueOffline   (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_ContinueOnline    (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_updateTxnTags     (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_CheckSupportedAID (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO  SDI_CT_SER_EndTransaction    (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CT_SER_SmartISO          (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CT_SER_SmartDetect       (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CT_SER_SmartReset        (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CT_SER_SmartPowerOff     (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CT_SER_SmartPIN          (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CT_SER_Send_PIN_Offline  (const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);


EMV_ADK_INFO SDI_CTLS_Init_Framework_Client(const char *version, unsigned char numberOfAIDs, EMV_CTLS_CALLBACK_FnT EMV_Callback, void* externalData, unsigned long options, unsigned long *ulResult);
#define SDI_CTLS_Init_Framework(numberOfAIDs, EMV_Callback, externalData, options,ulResult) SDI_CTLS_Init_Framework_Client(EMV_CTLS_FRAMEWORK_VERSION, (numberOfAIDs), (EMV_Callback), (externalData), (options),(ulResult))
void SDI_CTLS_Exit_Framework(void);
void SDI_CTLS_Exit_Framework_extended(unsigned char options);
const char* SDI_CTLS_CLIENT_GetVersion(void);
const char* SDI_CTLS_FRAMEWORK_GetVersion(void);
EMV_ADK_INFO SDI_CTLS_MapVirtualTerminal(EMV_ADK_VIRTUALTERMMAP_TYPE VirtualTermMapType, unsigned char *TLVSwitchValue, unsigned int TLVBufLen, unsigned char VirtualTerminal);
EMV_ADK_INFO SDI_CTLS_StoreCAPKey(EMV_ADK_HANDLE_RECORD_TYPE eHandleCAPKeyType, const EMV_CTLS_CAPKEY_TYPE *pxKeyData);
EMV_ADK_INFO SDI_CTLS_ReadCAPKeys(EMV_CTLS_CAPREAD_TYPE *pxKeyData, unsigned char *pucMaxnum);
EMV_ADK_INFO SDI_CTLS_SetTermData(EMV_CTLS_TERMDATA_TYPE* pxTermData);
EMV_ADK_INFO SDI_CTLS_GetTermData(EMV_CTLS_TERMDATA_TYPE* pxTermData);
EMV_ADK_INFO SDI_CTLS_SetAppliDataSchemeSpecific(EMV_ADK_HANDLE_RECORD_TYPE eHandleAppliType, EMV_CTLS_APPLI_KERNEL_TYPE* pxAID, EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_TYPE* pxAppliData);
EMV_ADK_INFO SDI_CTLS_GetAppliDataSchemeSpecific(EMV_ADK_READAPPLI_TYPE eReadAppliType, EMV_CTLS_APPLI_KERNEL_TYPE* pxAID, EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_TYPE* pxAppliData);
EMV_ADK_INFO SDI_CTLS_ApplyConfiguration(unsigned long options);
EMV_ADK_INFO SDI_CTLS_SetupTransaction(EMV_CTLS_START_TYPE *pxStartInput, EMV_CTLS_STARTRES_TYPE* pxStartRes);
EMV_ADK_INFO SDI_CTLS_ContinueOffline(EMV_CTLS_TRANSRES_TYPE *pxTransRes, EMV_SDI_CTLS_TRANSRES_TYPE *pxSdiTransRes);
EMV_ADK_INFO SDI_CTLS_ContinueOfflineExt(EMV_CTLS_CONT_OFFL_TYPE *pxContOfflInput, EMV_CTLS_TRANSRES_TYPE *pxTransRes, EMV_SDI_CTLS_TRANSRES_TYPE *pxSdiTransRes);
EMV_ADK_INFO SDI_CTLS_ContinueOnline(EMV_CTLS_HOST_TYPE *pxOnlineInput, EMV_CTLS_TRANSRES_TYPE *pxTransRes, EMV_SDI_CTLS_TRANSRES_TYPE *pxSdiTransRes);
EMV_ADK_INFO SDI_CTLS_EndTransaction(unsigned long options);
unsigned char SDI_CTLS_SmartISO(unsigned char ucOptions, unsigned short usInDataLen, unsigned char *pucDataIn, unsigned short *pusOutDataLen, unsigned char *pucDataOut, unsigned short usOutBufferLength);
unsigned char SDI_CTLS_SmartReset(unsigned char ucOptions, unsigned char* pucCardInfo, unsigned long* pnInfoLength);
unsigned char SDI_CTLS_SmartPowerOff(unsigned char ucOptions);
unsigned char SDI_CTLS_CardRemoval(long timeoutMillis);
unsigned char SDI_CTLS_LED(unsigned char ucLedId, unsigned char ucLedState);
unsigned char SDI_CTLS_LED_SetMode(unsigned char ucLedMode);
unsigned char SDI_CTLS_Break(void);
EMV_ADK_INFO SDI_CTLS_GetCandidateData(EMV_CTLS_CANDIDATE_DATA_TYPE *candidateData);

// ==================================================================================================================================================

EMV_ADK_INFO SDI_CTLS_SER_Init_Framework(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
void SDI_CTLS_SER_Exit_Framework(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_MapVirtualTerminal(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_StoreCAPKey(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_ReadCAPKeys(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_SetTermData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_GetTermData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_SetAppliDataSchemeSpecific(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_GetAppliDataSchemeSpecific(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_SetupTransaction(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_ContinueOffline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_ContinueOnline(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_EndTransaction(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_SmartISO(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_SmartReset(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_SmartPowerOff(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_CardRemoval(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_LED_SetMode(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_LED(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
unsigned char SDI_CTLS_SER_Break(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);
EMV_ADK_INFO SDI_CTLS_SER_GetCandidateData(const unsigned char *dataIn, unsigned short dataInLen, unsigned char *dataOut, unsigned short *dataOutLen);


#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef _SDI_CLIENT_EMV_INTERFACE_H_
