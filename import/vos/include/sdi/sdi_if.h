/****************************************************************************
*  Product:     ADK Secure Data Interface (SDI)
*  Company:     Verifone
*  Author:      GSS R&D Germany
*  Content:     Client (structure) interface - non-EMV part
****************************************************************************/
#ifndef CLIENT_SDI_IF_H_
#define CLIENT_SDI_IF_H_

#ifdef __cplusplus
#include <vector>
#include <string>
#include <stdint.h>
#include <emv/EMV_Common_Interface.h> // for EMV_ADK_INFO
#include <sysinfo/sysinfo.h> // for property enumerations

namespace libsdi {

enum SDI_SW12
{
  SDI_SW12_SUCCESS = 0x9000,
  SDI_SW12_TIMEOUT = 0x6600, // card detection/removal
  SDI_SW12_NOT_ALLOWED = 0x64FD, // not allowed for the moment, e.g. card removal
  SDI_SW12_ERROR = 0x6400, // any other error
  SDI_SW12_PARAMETER_ERROR = 0x90E6,
};

class SdiBase
{
protected:
  class SdiMessage;
  unsigned short sw12;
  int additionalResultValue;

public:
  SdiBase() : sw12(0), additionalResultValue(0){}
  enum SDI_SW12 getSdiSw12();
  int getAdditionalResultValue();

  /**
   * @brief Receive SDI server response with no data
   * To be called when data available callback was invoked and the command was
   * - sendWaitCardRemoval()
   * - startPinInput()
   * - ...
   *
   * @return return SW12
   */
  enum SDI_SW12 receiveSW12();

private:
  // make SdiBase non-copyable because some subclasses hold pointers and should not be copied
  SdiBase(const SdiBase&);
  SdiBase& operator=(const SdiBase&);
};

class SDI: public SdiBase
{
public:

  /**
   * @brief Read numeric date/time
   * Call Sys Get Status (20-04), tag 'D0', DOL approach. This is an alternative
   * to getProperty(vfisysinfo::SYS_PROP_RTC) which returns a string.
   * @param[out] buffer for date/time n14 (length 7, 'yyyymmddHHMMSS')
   * @return true in case of success
   */
  bool getDateTime(unsigned char* buffer);

  /**
   * @brief Get Integer Property by SYSINFO-ADK (20-1A, P2 = 00)
   */
  int getProperty(enum vfisysinfo::SYSPropertyInt p, bool epp = false); // 20 1A 00 00
  enum vfisysinfo::sysError getProperty(enum vfisysinfo::SYSPropertyInt p, int& value, bool epp = false); // 20 1A 00 00

  /**
   * @brief Set Integer Property by SYSINFO-ADK (20-19, P2 = 00)
   */
  enum vfisysinfo::sysError setProperty(enum vfisysinfo::SYSPropertyInt p, int value, bool epp = false); // 20 19 00 00

  /**
   * @brief Get String Property by SYSINFO-ADK (20-1A, P2 = 01)
   */
  std::string getProperty(enum vfisysinfo::SYSPropertyString p, bool epp = false); // 20 1A 00 01
  enum vfisysinfo::sysError getProperty(enum vfisysinfo::SYSPropertyString p, std::string& value, bool epp = false);

  /**
   * @brief Set Integer Property by SYSINFO-ADK (20-19, P2 = 01)
   */
  enum vfisysinfo::sysError setProperty(enum vfisysinfo::SYSPropertyString p, const std::string& value, bool epp = false); // 20 19 00 01

  /**
   * @brief read SDI version info via Get SDI version info (20-1C)
   * @return versions a single string "<component A>:<version A>;<component B>:version b>;..."
   */
  std::string getVersionInfo(bool epp = false);

  /**
   * @brief Check if update available on Android platform and execute (20-1D).
   * @param[in] updateKind e.g. 6 for SDI user config. See SDI server documentation.
   * @return true for success (SW12 = '9000'
   */
  bool checkForUpdate(unsigned char updateKind);

  /**
   * @brief Wait Card Removal (23-02)
   * @param[in] wait time in seconds
   * @return true if card is absent
   */
  bool waitCardRemoval(unsigned seconds);

  /**
   * @brief Wait Card Removal (23-02), send only
   * Send command Wait Card Removal. To be used with the data available callback.
   * Once data is available call receiveSW12() to get the result.
   * @param[in] wait time in seconds
   * @return return result of SDI_Send()
   */
  int sendWaitCardRemoval(unsigned seconds);

  // Data Interface (without crypto handle)
  void clearDataStore();
}; // class SDI

class CardDetection: public SdiBase
{
public:
  const static unsigned char SDI_TEC_CHIP = 1; // ISO 7816
  const static unsigned char SDI_TEC_MAGN = 2; // ISO 7810
  const static unsigned char SDI_TEC_CTLS = 4; // ISO 14443
  const static unsigned char SDI_TEC_MANU = 8; // ITU-T E.161

  void setTecStartOptions(const std::vector<unsigned char>& opts); // see cts_StartSelection
  void setTecConfig(const std::vector<unsigned char>& opts); // see cts_SetOptions
  void setCancelButton(bool enable);
  void setCardEntryValueDeativation(unsigned char b);
  void setAlternativeInputFormat(const char* f);

  /**
   * @brief Start Technology Selection
   * To be with with SDI_SetDataAvailableCallback. Once response is available
   * use receiveTechnology() to get the output.
   * @return return value from SDI_Send
   */
  int startSelection(unsigned char supportedTechnologies, unsigned seconds);

  /**
   * @brief Get the Technology Selection Result
   * In case of problem, when '0' is returned use getSW12() to retrieve the
   * SDI Server SW1 SW2.
   * @return selected technology SDI_TEC_CHIP, SDI_TEC_MAGN, SDI_TEC_CTLS,
   *         SDI_TEC_MANU or 0 in case of failure
   */
  unsigned char receiveTechnology(); // returns technology or 0 in case of error

  /**
   * @brief Access PAN from mag. stripe of manual entry
   * @return an ..19
   */
  std::string getPan();

  /**
   * @brief Access PAN from mag. stripe
   * @return an ..40
   */
  std::string getTrack2();

  /**
   * @brief Access obfuscated track 2 data
   * Note: Data is appended to existing content
   * @return length of appended data ..20, -1 if not present
   */
  int getTrack2Bin(std::vector<unsigned char>& data);

  /**
   * @brief Access card holder name from track 1
   * @return Name/Christian name according ISO 7810 (ans ..26)
   */
  std::string getCardholderName();

  /**
   * @brief Access service code from track 3
   * @return an 3
   */
  std::string getServiceCode();

  /**
   * @brief Access plugin response data
   * Note: Data is appended to existing content
   * @return length of appended data, -1 if not present
   */
  int getPluginResponseData(std::vector<unsigned char>& data);

  /**
   * @brief Access cts_WaitSelection result data
   * CTS_DATA_TAG_VAS_DATA: JSON string returned by NFC_VAS_Activate or NFC_VAS_Decrypt
   * @return JSON string from NFC ADK
   */
  std::string getString(unsigned CTS_DATA_TAG);

  /**
   * @brief Access cts_WaitSelection result data
   * CTS_DATA_TAG_NFC_RESULT
   * CTS_DATA_TAG_CARDS_TOTAL_COUNT
   * CTS_DATA_TAG_CARDS_A
   * CTS_DATA_TAG_CARDS_B
   * CTS_DATA_TAG_CARDS_F
   * CTS_DATA_TAG_CARD_TYPE
   * CTS_DATA_TAG_VAS_RESULT
   * CTS_DATA_TAG_EMV_RESULT
   * CTS_DATA_TAG_VAS_DECRYPT_DATA_RESULT
   */
  unsigned char getValue(unsigned CTS_DATA_TAG, unsigned char defaultValue);

  /**
   * @brief Access cts_WaitSelection result data
   * CTS_DATA_TAG_CARD_INFO
   * Note: Data is appended to existing content
   * @return length of appended data, -1 if not present
   */
  int getData(unsigned CTS_DATA_TAG, std::vector<unsigned char>& data);

  CardDetection();
  ~CardDetection();

private:
  void* dataIn;
  void* dataOut;
};

class PED: public SdiBase
{
public:
  enum NavigatorMode
  {
    NAVIGATOR_MODE_OFF,
    NAVIGATOR_MODE_DOUBLE_TAB,
    NAVICATOR_MODE_TACTILE_BUTTON
  };

  /**
   * @brief Configure PIN input dialog timeout
   * This timeout will be used for PIN entries started with this object and
   * replaces the SDI services default timeout configured via setDefaultTimeout.
   * @param[in] seconds PIN input dialog timeout
   * @return true in case of successful execution
   */
  void setTimeout(unsigned seconds);

  /**
   * @brief Configure PIN input dialog default timeout
   * This timeout will be applied in case setTimeout() has not been invoked.
   * @param[in] seconds PIN input dialog timeout
   * @return true in case of successful execution
   */
  bool setDefaultTimeout(unsigned seconds);

  /**
   * brief configure touch coordinates
   * Each key consists of
   * x-position b2
   * y-position b2
   * button width b2
   * button height b2
   * associated key ASCII code b1 ("0" .. "9" = '30' .. '31', cancel = '1B',
   * correction = '08', enter = '0D')
   * @param[in] array coordinates
   * @param[in] size size of coordinates array (n*9)
   */
  void setTouchCoordinates(const unsigned char* array, unsigned size);

  /**
   * @brief Activate Navigator Mode
   */
  void setNavigatorMode(enum NavigatorMode mode);

  /**
   * @brief Change PIN digit count limits
   * The default values are 4 and 12. Calling this method is only required
   * if other values shall be applied.
   * @param[in] min minimal number of PIN digits [4..12]
   * @param[in] max maximal number of PIN digits [4..12]
   */
  void setPinDigitCountMinMax(unsigned char min, unsigned char max);

  /**
   * @brief Send command for PIN input (22-01)
   * The response should be received with receiveSW12(). SDI_SetSdiCallback()
   * can be used to receive PIN input status messages, i.e. the digit count.
   * @return return value from SDI_Send()
   */
  int startPinInput(bool enablePinBypass = false);

  PED();
  ~PED();

private:
  void* dataIn;
};

/**
 * SdiCrypt holding the crypto handle from Crypto Open.
 * General notes for most functions in this class:
 * - In case of error the SDI server's SW1 SW2 can be read with getSdiSw12()
 * - If present additional error info can be read with getAdditionalResultValue()
 * - If a Initial Vector is required or received (random mode) it is managed by getInitialVector() and setInitialVector().
 * - If an KSN has been returned by the SDI server it can be read with getKeySerialNumber()
 */
class SdiCrypt: public SdiBase
{
public:

  // SDI Crypto Interface
  SdiCrypt();
  ~SdiCrypt();

  /**
   * @brief call SDI Crypto Open (70-00)
   * The crypto handle received from SDI server is stored and used inside
   * this object.
   * @param[in] hostName host name
   * @return true in case of success
   */
  bool open(const char* hostName);

  /**
   * @brief call SDI Crypto Open Close (70-01)
   * NOTE: This funtion is called by SdiCrypt's destructor and open()
   * @return true in case of success
   */
  bool close();

  /**
   * @brief Check if a crypto handle is present
   * @return false if crypto handle has not been obtain or if close has been called
   */
  bool isOpen() const;

  /**
   * @brief Read the crypto handle obtained by SDI Crypto Open
   * This method might be required if required for commands that are not handled
   * by this object.
   * @return crypto handle, -1 when open() was not called or failed
   */
  uint32_t getCryptoHandle();

  /**
   * @brief SDI Crypto Encrypt (70-02)
   * @param[in] data data for encryption
   * @param[out] encrypted buffer for encrypted data
   * @return true in case of success
   */
  bool encrypt(const std::vector<unsigned char>& data,
          std::vector<unsigned char>& encrypted);

  bool decrypt(const std::vector<unsigned char>& encrypted,
          std::vector<unsigned char>& decrypted);
  bool sign(const std::vector<unsigned char>& data,
          std::vector<unsigned char>& signature);
  bool verify(const std::vector<unsigned char>& data,
          const std::vector<unsigned char>& signature);
  bool updateKey(unsigned char keyType, const std::vector<unsigned char>& keyData,
          std::vector<unsigned char>* proprietaryData = NULL);
  bool setKeySetId(uint32_t ksid);
  bool getEncryptedPin(unsigned char pinBlockFormat,
          std::vector<unsigned char>& pinBlock,
          bool requestZeroPinBlock = false);
  std::string getKeyInventory();
  bool getKeyData(unsigned char keyType, std::vector<unsigned char>& keyData);
  std::string getStatus();
  static std::string getVersions(int& additionalResult);

  void setInitialVector(const std::vector<unsigned char>& iv);
  void getInitialVector(std::vector<unsigned char>& iv) const;
  void getKeySerialNumber(std::vector<unsigned char>& ksn) const;

  // SDI Data Interface
  struct Placeholder
  {
    std::vector<unsigned char> tagList;
    std::vector<unsigned char> applicationData;
    std::vector<unsigned char> dataOptions;
  };

  bool getEncData(const Placeholder& descriptor,
          std::vector<unsigned char>& encrypted,
          bool useStoredData = false);

  bool getEncMsgData(const std::vector<unsigned char>& messageTemplate,
          const std::vector<Placeholder>& placeholder,
          std::vector<unsigned char>& encrypted,
          bool useStoredData = false);

  bool getMsgSignature(const std::vector<unsigned char>& messageTemplate,
          const std::vector<Placeholder>& placeholder,
          std::vector<unsigned char>& signature,
          bool useStoredData = false);

private:
  unsigned char cryptoHandle[4];
  std::vector<unsigned char> initialVector;
  std::vector<unsigned char> keySerialNumber;
};

} // namespace sdi

#endif // C++
#endif /* CLIENT_SDI_IF_H_ */
