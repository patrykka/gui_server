// -*- Mode: C++ -*-
#ifndef SDI_PLUGIN_H_20181001
#define SDI_PLUGIN_H_20181001

/** \file sdiplugin.h */

#include <vector>


#define CLA_PLUGIN_CMD                          0x26 ///< Class for plugin commands


/** Abstract SDIPlugin base class */
class SDIPlugin
{
  public:

    /** buffer type */
    typedef std::vector<unsigned char> Buffer;

    /** SDI plugin trigger IDs in the form 1<<n*/
    enum SDITrigger
    {
      SDITrigger_CardDetected  =  1 << 0, /**< Card has been inserted, swiped or tapped after a card request was sent (2301) */
      SDITrigger_EMVConfig     =  1 << 1, /**< EMV config command issued by the application */
      SDITrigger_SDIConfig     =  1 << 2, /**< SDI config file upload command issued by the application */
      SDITrigger_CardDataAvail =  1 << 3, /**< SDI server has detected track equivalent 2 data */
      SDITrigger_MsgDataAvail  =  1 << 4, /**< SDI server has created host message data */
      SDITrigger_EmvCndListCbk =  1 << 5, /**< EMV Reduce Candidate List callback has been called */
      SDITrigger_CardId        =  1 << 6  /**< SDI server has delivers a Pan to generate a unique Card Id */
    };


    /** Destructor */
    virtual ~SDIPlugin() {}

    /** \return String containing the name of the plugin */
    virtual const char *name() const = 0;

    /** obtain version information
     *  \param[out] major major version
     *  \param[out] minor minor version
     *  \param[out] patch patch version
     */
    virtual void version(int &major, int &minor, int &patch) const = 0;

    /** obtain module ID that identifies the plugin (parameter p1 in the SDI interface)
     * \return module ID
     */
    virtual int moduleID() const = 0;

    /** \return trigger ID mask that are supported by this plugin. This is the or-ed value of all supported SDITriggers.
     */
    virtual unsigned supportedTriggers() const = 0;

    /** process SDI message sent by application
     * \param[out] result buffer containing the result of the operation, recommended is to
     *                           use BER-TLV format for encoding the content of the buffer
     * \param[in] cmd P1 parameter in the SDI interface that identifies the function
     * \param[in] message input message recommended is to
     *                           use BER-TLV format for encoding the content of the buffer
     * \return result code, e.g. 0x9000 for OK
     */
    virtual int processMessage(Buffer &result, int cmd, const Buffer &message) = 0;


    /** process trigger ID invoked from SDI server
     * \param[out] result buffer containing the result of the operation, recommended is to
     *                           use BER-TLV format for encoding the content of the buffer
     * \param[in] triggerID trigger ID
     * \param[in] message input input format depends on the triggerID.
     * \return result code, e.g. 0x9000 for OK
     */
    virtual int processTrigger(Buffer &result, unsigned triggerID, const Buffer &message) = 0;

    //
    //virtual int cardInsert(Buffer &result, unsigned cardType, const Buffer &atr, const Buffer &track1, const Buffer &track2, const Buffer &track3);
    //

    /** open extension. Extensions need to be closed using closeExtension() when no longer needed.
     * \param[in] ext_number extension number
     * \return pointer to extension or NULL if the extension is not known
     */
    virtual void *openExtension(int ext_number) = 0;

    /** close extension
     * \param[in] extension pointer as returned from openExtension
     */
    virtual void closeExtension(void *extension) = 0;
};


/** SDI plugin entry function
 * \return SDIPlugin object
 */
extern "C" {
  __attribute__((visibility("default"))) SDIPlugin *getSDIPlugin();
}

#endif
