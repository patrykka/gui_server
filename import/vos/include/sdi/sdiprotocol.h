/**
* @file
* @brief SDI protocol library interface
*
*/
#ifndef _SDIPROTOCOL_H_
#define _SDIPROTOCOL_H_

#ifdef __cplusplus
extern "C" {
#endif

/// @defgroup SDI_PROTOCOL_ERROR_CODES
/// @brief SDI protocol error codes
//@{
#define SDI_PROTOCOL_ERR_IO             -1 ///< read, write or protocol error
#define SDI_PROTOCOL_ERR_CONCURRENT_USE -2 ///< SDI protocol library currently used by another thread, maybe try later
#define SDI_PROTOCOL_ERR_CONNECT        -3 ///< connection establishment to SDI server failed
#define SDI_PROTOCOL_ERR_OVERFLOW       -4 ///< response buffer too small, response has been dropped
#define SDI_PROTOCOL_ERR_PARAM          -5 ///< wrong formal parameter like NULL pointer
#define SDI_PROTOCOL_ERR_OTHER          -6 ///< any other problem like thread creation, memory allocation, etc.
#define SDI_PROTOCOL_ERR_NO_RECEIVE     -7 ///< returned by SDI_Send(): command successfully sent, but response for this
///< command is suppressed, therefore, no SDI_Receive() must be called afterwards
//@}


/// @defgroup SDI_PROTOCOL_INIT_OPTIONS
/// @brief SDI protocol configuration flags
//@{
#define SDI_PROTOCOL_INIT_OPT_TRACE_EMV_CBK           0x01 ///< trace using the EMV callback
#define SDI_PROTOCOL_INIT_OPT_TRACE_SYSLOG            0x02 ///< use syslog instead of EMV callback for trace
#define SDI_PROTOCOL_INIT_OPT_TRACE_ADK_LOG           0x04 ///< use ADK-LOG instead of EMV callback for trace
//@}


/*****************************************************************************
* SDI_ProtocolInit
*****************************************************************************/
/// @brief Configure the library
///
/// @param[in] options bit mask for configuration flags @ref SDI_PROTOCOL_INIT_OPTIONS
/// @param[in] settings configuration parameters JSON string
///
/// Settings (all optional):
/// {"server": {"host":<string>, "port":<integer>}}
///
/// Default values:
/// {"server": {"host":"127.0.0.1", "port":12000}}
///
/// @return 0 for success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_ProtocolInit(unsigned options, const char *settings);

/*****************************************************************************
* SDI_Send
*****************************************************************************/
/// @brief Send message to SDI server
///
/// Will establish connection if necessary
///
/// @param[in] data data buffer with payload
/// @param[in] dataLength length of payload data
///
/// @return 0 for success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_Send(const unsigned char *data, int dataLength);

/*****************************************************************************
* SDI_Receive
*****************************************************************************/
/// @brief Receive response from SDI server
///
/// For instance uses infinite timeout
///
/// @param[in] responseBuffer buffer to take the payload of SDI server response
/// @param[in] responseBufferSize length of reponseBuffer
///
/// @return length of response in case of success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_Receive(unsigned char *responseBuffer, int responseBufferSize);

/*****************************************************************************
* SDI_SendReceive
*****************************************************************************/
/// @brief Send message to SDI server and receive the response
///
/// Combines SDI_Send and SDI_Receive
///
/// @param[in] data data buffer with payload
/// @param[in] dataLength length of payload data
/// @param[in] responseBuffer buffer to take the payload of SDI server response
/// @param[in] responseBufferSize length of reponseBuffer
///
/// @return length of response in case of success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_SendReceive(const unsigned char *data, int dataLength, unsigned char *responseBuffer, int responseBufferSize);

/*****************************************************************************
* SDI_Disconnect
*****************************************************************************/
/// @brief Shut down the connection to SDI server
///
/// Might be called if the connection shall be provided to another process,
/// for shutdown or in case of unrecoverable communication errors.
///
/// @return 0 for success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_Disconnect(void);

/// @brief callback function type declaration for SDI_SetDataAvailableCallback()
typedef void (*SDI_DATA_AVAILABLE_CALLBACK_TYPE)(void *context);

/*****************************************************************************
* SDI_SetDataAvailableCallback
*****************************************************************************/
/// @brief Set a callback to be called if a SDI server response is pending
///
/// Set a callback that will be invoked the callback when SDIresponse data
/// is pending after SDI_Send().
///
/// @return 0 for success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_SetDataAvailableCallback(SDI_DATA_AVAILABLE_CALLBACK_TYPE cb, void *context);

/// @brief callback function type declaration for SDI_SetSdiCallback()
///
/// @param[in]      dataIn   Input data to send to the application: complete SDI Server callback message
/// @param[in]      sizeIn   Length of input data
/// @param[out]     dataOut  TLV stream of output data (callback response) to send back to the SDI Server
/// @param[in,out]  sizeOut  Input: size of dataOut buffer; Output: size of output data. Must be set to zero for one-way callbacks
/// @param[out]     context  Application context data pointer
typedef void (*SDI_CALLBACK_TYPE)(unsigned char *dataIn, unsigned short sizeIn, unsigned char *dataOut, unsigned short *sizeOut, void *context);

/***************************************************************************************
* SDI_SetSdiCallback
****************************************************************************************/
/// @brief Set a callback function to be called when SDI server sends a callback message
///
/// The SDI Callback is called in the receiver thread and should not block.
/// @return 0 for success else SDI protocol error code, see @ref SDI_PROTOCOL_ERROR_CODES
///
int SDI_SetSdiCallback(SDI_CALLBACK_TYPE cb, void *context);

/***************************************************************************************
 * SDI_SendSysAbort
 ****************************************************************************************/
/// @brief Send SYS ABORT command '20 02'
///
/// This command is available even when SendReceive is in progress. This is useful if
/// a dialog during a callback needs to be aborted.
///
void SDI_SendSysAbort(void);

#ifdef __cplusplus
}
#endif

#endif // _SDIPROTOCOL_H_
