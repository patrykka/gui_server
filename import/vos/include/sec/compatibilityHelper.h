/**
* @file compatibilityHelper.h
*
 * @author	JensW
*
* @date    06.06.2016
*
* @brief ADK Security Service Compatibility Helper
*/

#ifndef API_EXPORT_COMPATIBILITYHELPER_H_
#define API_EXPORT_COMPATIBILITYHELPER_H_

#include <stdlib.h>
#include "libsec.h"

#if (defined _VRXEVO || defined _WIN32)
#  if defined VFI_SEC_SHARED_EXPORT
#    define DllSpecSEC __declspec(dllexport)
#  elif defined VFI_SEC_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpecSEC
#  else
#    define DllSpecSEC __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_SEC_SHARED_EXPORT
#  define DllSpecSEC  __attribute__((visibility ("default")))
#else
#  define DllSpecSEC
#endif


DllSpecSEC secError secCompatiblity_getHandle(u_char oldHandle, secHandle_t& newHandle);

DllSpecSEC u_char secCompatiblity_registerHandle(secHandle_t handle, u_char hash = 0);

DllSpecSEC secError secCompatiblity_deregisterHandle(u_char oldHandle);

#undef DllSpecSEC // important to avoid duplicated definitions of DllSpec with other component header files

#endif /* API_EXPORT_COMPATIBILITYHELPER_H_ */
