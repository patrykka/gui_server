#ifndef _LIBSEC_H_
#define _LIBSEC_H_

/**  @file libsec.h
*
*   @brief ADK Security Service API
*
*   This header file contains information about the ADK security service API functions.
*/

/***************************************************************************
 * Includes
 **************************************************************************/
//#include <stdlib.h>
#include "sec2.h"
#include "CTransactionData.h"

#ifdef _VRXEVO
#include <unistd.h>
#include <string>
//#include <SVC_NET.H>
#endif

#ifdef _VOS
#include <string>
#include <stdint.h>
#endif

#include <vector>
#include <map>
#include "secError.h"


#if (defined _VRXEVO || defined _WIN32)
#  if defined VFI_SEC_SHARED_EXPORT
#    define DllSpecSEC __declspec(dllexport)
#  elif defined VFI_SEC_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpecSEC
#  else
#    define DllSpecSEC __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_SEC_SHARED_EXPORT
#  define DllSpecSEC  __attribute__((visibility ("default")))
#else
#  define DllSpecSEC
#endif


/***************************************************************************
 * Namespace
 **************************************************************************/
//using namespace com_verifone_host;
using namespace com_adksec_cmd;

namespace com_verifone_seccmd
{

/**
 * Generic interface to establish an IPC communication to Security Service
 * @return
 */
DllSpecSEC secError secInit();

/**
 * Generic interface to close IPC connection to Security Service
 * @return
 */
DllSpecSEC secError secDestroy();

///** Open a Security Module.
// * For compatibility only: hostname will be handled as Transaction value
// */
DllSpecSEC secError secOpen(const std::string& , secHandle_t& handle);

DllSpecSEC secError secOpen(const SecTransactionData_t& td, secHandle_t& handle);

/**
 * Close a Security Module
 */
DllSpecSEC secError secClose(secHandle_t handle);

/**
 * Set the Key Set ID
 */
DllSpecSEC secError secSetKSId(secHandle_t handle, uint32_t ksid);

/**
 * Get JSON formatted information about keys related to an opened Security Module (available keys, creation date, ...)
 * @param[in]  handle
 * @param[out] out json of all available keys depending on transaction. \n\n
 *             Output examples can be obtained in the documentation reference part at \ref Output-Example-VSS-MSK, \ref Output-Example-VSS-DUKPT, \ref Output-Example-IPP-MSK, \ref Output-Example-IPP-DUKPT, \n
 *             \ref Output-Example-ADE, \ref Output-Example-RSA and \ref Output-Example-VisaDSP.
 *             \n
 *             The corresponding JSON schema can be found at \ref sec_json_secGetKeyInventory_schema.
 * @return
 */
DllSpecSEC secError secGetKeyInventory(secHandle_t handle, std::string& out);

/**
 * Get key relevant information related to an opened Security Module (key file path, ...)
 * @param[in]  handle
 * @param[in]  keyType
 * @param[out] out TLV if keyType is a Bendigo-type
 * @return
 */
DllSpecSEC secError secGetKeyData(secHandle_t handle, com_verifone_host::key_type_t keyType, std::vector<uint8_t>& out);

/**
 * Load a key
 * @param[in]     handle
 * @param[in]     keyType
 * @param[in]     keyData  - binary data (e.g. 8Byte for 1DES, 16Byte for 3DES and 2TDEA-ADE-DUKPT key, 120Byte for GISKE)
 *                         - TLV if keyType is a Bendigo-type
 *                         - 'KSN incrementation' with keyType=KEY_TYPE_DUKPT effects KSN incrementation
 * @param[in,out] propData - proprietary data
 *                           - in:  in case of DUKPT (modules \ref VSS, \ref IPP DUKPT part, \ref ADE): KSN binary data (maybe padded with 0xFF, 0xFF))
 *                           - out: in case of VSS-MSK and VSS-DUKPT (module \ref VSS): received data from an assigned Update Finalize macro
 * @return
 *
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secUpdateKey(secHandle_t handle, com_verifone_host::key_type_t keyType, const std::vector<uint8_t>& keyData,
    std::vector<uint8_t>& propData);

inline secError secUpdateKey(secHandle_t handle, com_verifone_host::key_type_t keyType, const std::vector<uint8_t>& keyData)
		{
			std::vector<uint8_t> dummyKsn = std::vector<uint8_t>();
			return secUpdateKey(handle, keyType, keyData, dummyKsn);
		}


/**
 * Encrypt data
 * @param[in]     handle
 * @param[in]     plainData
 * @param[out]    encData the encrypted result of encryption
 * @param[in,out] iv try to use this IV, empty for configured default. get used IV back. Relevant for modules \ref VSS and \ref ADE
 * @param[out]    ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema, like \ref VSS DUKPT, \ref ADE
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secEncryptData(secHandle_t handle, const std::vector<uint8_t>& plainData, std::vector<uint8_t>& encData,
        std::vector<uint8_t>& iv, std::vector<uint8_t>& ksn);


/**
 * Encrypt TransactionData referenced by index
 * @param[in]     handle
 * @param[in]     TDindex index-key of TransactionData to reference plainData
 * @param[out]    encData the encrypted result of encryption
 * @param[in,out] iv try to use this IV, empty for configured default. get used IV back. Relevant for modules \ref VSS and \ref ADE
 * @param[out]    ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema, like \ref VSS DUKPT, \ref ADE
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secEncryptTransactionData(secHandle_t handle, const secIndex_t& TDindex, std::vector<uint8_t>& encData,
        std::vector<uint8_t>& iv, std::vector<uint8_t>& ksn);

/**
 * Decrypt data
 * @param[in]     handle
 * @param[in]     encData
 * @param[out]    plainData
 * @param[in,out] iv try to use this IV, empty for configured default. get used IV back. Relevant for module \ref VSS
 * @param[out]    ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema \ref VSS
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secDecryptData(secHandle_t handle, const std::vector<uint8_t>& encData, std::vector<uint8_t>& plainData,
        std::vector<uint8_t>& iv, std::vector<uint8_t>& ksn);

/**
 * Generate a cryptographic signature of given data (in most cases this will be MAC generation)
 * @param[in]     handle
 * @param[in]     data           data to be signed
 * @param[out]    signature      MAC
 * @param[in,out] iv try to use this IV, empty for configured default. get used IV back. Relevant for modules \ref VSS and \ref SRED
 * @param[out]    ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema, like \ref VSS DUKPT and \ref SRED
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secSign(secHandle_t handle, const std::vector<uint8_t>& data, std::vector<uint8_t>& signature,
        std::vector<uint8_t>& iv, std::vector<uint8_t>& ksn);

/**
 * Verify a cryptographic signature of given data (in most cases this will be MAC verification)
 * @param[in]     handle
 * @param[in]     data
 * @param[in]     signature
 * @param[in,out] iv try to use this IV, empty for configured default. get used IV back. Relevant for modules \ref VSS and \ref SRED
 * @param[out]    ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema, like \ref VSS DUKPT and \ref SRED
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secVerify(secHandle_t handle, const std::vector<uint8_t>& data, const std::vector<uint8_t>& signature,
        std::vector<uint8_t>& iv, std::vector<uint8_t>& ksn);

/**
 * Retrieve an encrypted PIN block
 * @param[in]  handle
 * @param[in]  pinBlockFormat value of pin_block_format_t
 * @param[out] pinBlk
 * @param[out] ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema, like \ref VSS DUKPT and \ref IPP DUKPT
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secRetrieveEncryptedPIN(secHandle_t handle, const uint8_t pinBlockFormat, std::vector<unsigned char>& pinBlk,
        std::vector<uint8_t>& ksn);

/**
 * Increment the KSN
 * @param[in] handle
 * @param[out] ksn 10 bytes binary data (maybe padded with 0xFF, 0xFF). Relevant for DUKPT encryption schema, like \ref VSS DUKPT and \ref IPP DUKPT
 * @return
 * Module related parameter descriptions can be found in chapter \ref subsubsec_security_Security_Modules.
 */
DllSpecSEC secError secIncrementKSN(secHandle_t handle, std::vector<uint8_t>& ksn);

/**
 * Get JSON formatted information about status of Security Component (version, information of existing host configurations, available Security Modules)
 * @param[out] jsonString \n
 *            An output example can be found at \ref sec_json_secGetStatus_output_example. \n
 *            The corresponding JSON schema can be found at \ref sec_json_secGetStatus_schema. \n
 * @param[in] hostName configuration name, optional. Provide only defined host configuration.
 * @return
 */
DllSpecSEC secError secGetStatus(std::string& jsonString, std::string hostName = "");

/**
 * Get JSON formatted information about status of Security Component (version, information of existing host configurations, available Security Modules)
 * @param[out] jsonString \n
 *            An output example can be found at \ref sec_json_secGetStatus_output_example. \n
 *            The corresponding JSON schema can be found at \ref sec_json_secGetStatus_schema. \n
 * @param[in] handle
 * @return
 */
DllSpecSEC secError secGetStatus(std::string& jsonString, secHandle_t handle);

/**
 * Get versions of components (API library, Security Component, Host Names/VSS)
 * @param[out] versions - version string in format (component separator:';'):
 * @return
 */
DllSpecSEC secError secGetVersions(std::string& versions);

/** Get API library version
 *
 * @return version string: \<major>.\<minor>.\<patch>-\<build>
 */
DllSpecSEC std::string secGetApiVersion(void);


/** Get Security Service version
 *
 * @return version string: \<major>.\<minor>.\<patch>-\<build>
 */
DllSpecSEC std::string secGetSvcVersion(void);


/**
 * Clear all TransactionData tags except TagHostName and TagKeySetID
 * @param[in] handle
 * @return
 */
DllSpecSEC secError secClearTransactionData(secHandle_t handle);


/**
 * Insert additionally TransactionData of transaction of a given handle (merge)
 *
 * h3. Permissions
 * for each Value of given Data Writing Permissions will be checked:
 * # Configuration will be checked
 * # if there is an existing entry it will be checked if overwriting is allowed
 *
 * @param[in] handle
 * @param[in] taData
 * @return EsecTAWriteProtected if writing not allowed
 */
DllSpecSEC secError secPutTransactionData(secHandle_t handle, const SecTransactionData_t& taData);

/**
 * Get TransactionData of transaction of a given handle
 * * h3. Permissions
 * Reading Permissions will be checked
 * They can be reduced by configuration or during putting the data (see SecTransactionValue::restrictPermissions)
 *
 * @param[in]  handle
 * @param[in]  key
 * @param[out]  date
 * @return
 * - EsecTANoEntry if no Value with key can be found
 * - EsecTAReadProtected if permissions are reduced and reading not allowed
 */
DllSpecSEC secError secGetTransactionValue(secHandle_t handle, const secIndex_t& key, SecTransactionValue& date);

/**
 * Get Permissions of an TransactionData entry
 * @param[in]  handle
 * @param[in]  key index - Which entry of TransactionData
 * @param[out]  permissionFlags values of CTransactionValue::ePermissionFlags
 * @return
 */
DllSpecSEC secError secGetPermission(secHandle_t handle, const secIndex_t& key, bitMask_t& permissionFlags);

/**
 * Get N bytes of random data
 * @param[in] data: buffer for random data
 * @param[in] count: number of random bytes to generate
 * @param[out] data: buffer with random data. Vector is resized to count bytes and filled with random data
 * @return
 *
 */
DllSpecSEC secError secGenerateRandom( std::vector<uint8_t>& data, int count );

/**
 * Perform digest operations
 * @param[in] type of digests algorithm. Supported types defined as SEC_DIG_... in sec2.h
 * @param[in] data
 * @param[out] digest: buffer with calculated digits
 * @return
 *
 */
DllSpecSEC secError secDigest(int type, std::vector<uint8_t>& data, std::vector<uint8_t>& digest);

}

#undef DllSpecSEC // important to avoid duplicated definitions of DllSpec with other component header files

#endif //_LIBSEC_H_

