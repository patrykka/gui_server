#ifndef SEC_H
#define SEC_H

#include <stdint.h>

/** @file sec2.h
*
*   @brief ADK Security Definitions
*
*   This header file contains preprocessor and data definitions of the ADK security service.
*/

/** @{ */

/***************************************************************************
 * Includes
 **************************************************************************/

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/

/***************************************************************************
* Preprocessor constant definitions
**************************************************************************/

#define CODE_UNLOCK  (0)
#define CODE_LOCK    (1)

#define CODE_ACTION  (0x80)

#define RESP_CODE_SIZE   ( 1)

#define RESP_CODE_SUCCESS                    (  0)
#define RESP_CODE_FAILED                     (  1)
#define RESP_CODE_TIMEOUT                    (  2)
#define RESP_CODE_PIN_EQUAL                  (  3)
#define RESP_CODE_HOST_NOT_FOUND             (  4)

#define RESP_CODE_PIN_CANCEL                 ( 20)
#define RESP_CODE_PIN_BYPASS                 ( 21)
#define RESP_CODE_PIN_CANCELED_BY_CLEAR_KEY  ( 22)
#define RESP_CODE_SIZE_ERROR                 ( 23)
#define RESP_CODE_MAC_VER_FAILED             ( 24)
#define RESP_CODE_MSG_SIZE_TO_LONG           ( 25)
#define RESP_CODE_CMD_SEQ_ERROR              ( 26)
#define RESP_CODE_RETRY_LIMIT                ( 31)
#define RESP_CODE_INVALID_REQ                ( 35)
#define RESP_CODE_API_REQ                    ( 36)
#define RESP_CODE_INVALID_CMD                ( 37)
#define RESP_CODE_INVALID_LOCAL_KEY_IDX      ( 40)
#define RESP_CODE_UPDATE_KEY_FAILED          ( 41)
#define RESP_CODE_TRANSPORT_KEYS_CHANGED     ( 42)
#define RESP_CODE_KEY_UPDATE_LIMIT           ( 43)
#define RESP_CODE_INVALID_KEY                ( 45)
#define RESP_CODE_PIN_BLOCKED                ( 90)
#define RESP_CODE_PIN_GET_CHALLENGE_ERROR    ( 91)
#define RESP_CODE_PIN_ENCIPHER_ERROR         ( 92)
#define RESP_CODE_PIN_VERIFY_FAILED          ( 93)
#define RESP_CODE_PIN_MISSING_PIN_BLOCK_DATA ( 94)
#define RESP_CODE_INVALID_KEY_LENGTH         ( 95)
#define RESP_CODE_MISSING_TMK_OR_DUKPT_KEY   ( 96)
#define RESP_CODE_MISSING_TPK_OR_TAK         ( 97)
#define RESP_CODE_MISSING_STAN_OR_PAN        ( 98)
#define RESP_CODE_ADE_NOT_ACTIVE             ( 99)
#define RESP_CODE_CARD_REMOVED               (100)
#define RESP_CODE_TAMPERED                   (101)
#define RESP_CODE_NOT_SUPPORTED_KSID         (102)
#define RESP_CODE_NOT_SUPPORTED_KEY_TYPE     (103)
#define RESP_CODE_ASYNC_PIN_NOT_READY        (104)
#define RESP_CODE_CRYPTO_DEVICE_BLOCKED      (105)
#define RESP_CODE_KSN_INCREMENTATION_ERROR   (106)

#define RESP_CODE_CMD_BUSY                   (249)
#define RESP_CODE_INVALID_APP_ID             (250)
#define RESP_CODE_POS_INVALID_FIELD_LEN      (251)
#define RESP_CODE_POS_MSG_LEN                (252)
#define RESP_CODE_UNKNOWN_CMD                (253)
#define RESP_CODE_INVAL                      (254)

#define RESP_ADE_NOT_ACTIVE                  RESP_CODE_ADE_NOT_ACTIVE



#define ADD_SCREEN_TEXT          (16)
#define ACTION_CODE_SIZE         ( 1)
#define COMMAND_SIZE             ( 1)
#define DATA_ALGORITHM_SIZE      ( 1)
#define PAN_SIZE                 (10)
#define STAN_SIZE                ( 6)
#define TRANS_AMOUNT_SIZE        ( 6)
#define TRANS_CURR_CODE_SIZE     ( 2)
#define ONLINE_PIN_CIPHER_BLOCK_SIZE    ( 8)
#define KSN_SIZE                 (10)
#define AES_KSN_SIZE             (12)
#define POS_TIMEOUT_SIZE         ( 1)
#define HOST_ID_SIZE             ( 1)
#define KEY_TYPE_SIZE            ( 1)
#define LOCK_SIZE                ( 1)
#define PIN_ENTRY_TYPE_SIZE      ( 1)
#define PIN_BLOCK_FORMAT_SIZE    ( 1)
#define KEY_MANAGEMENT_SIZE      ( 1)
#define PIN_ALGORITHM_SIZE       ( 1)
#define PIN_TRY_FLAG_SIZE        ( 1)
#define TRANS_CURR_EXPONENT_SIZE ( 1)
#define MAX_PIN_LENGTH_SIZE      ( 1)
#define MIN_PIN_LENGTH_SIZE      ( 1)
#define PIN_ENTRY_TIMEOUT_SIZE   ( 2)
#define PIN_CANCEL_SIZE          ( 1)
#define USE_DEFAULT_CV_FLAG_SIZE ( 1)
#define MAC_MODE_SIZE            ( 1)
#define HTML_FILE_PATH_SIZE      (128)

#define MACRO_PAN_SIZE           ( 6)

#define KEK_FLAG_SIZE            ( 1)
#define AS2805_FUNC_SIZE         ( 1)
#define DES_IV_SIZE              ( 8)

#define POS_TIMEOUT_TS         (2)
#define ACTION_CODE_TS         (2)
#define HOST_KEY_FILE_TS       (3)
#define CODE_TS                (2)
#define HOST_ID_TS             (2)
#define KEY_TYPE_TS            (2)
#define KEY_DATA_TS            (2)
#define LOCK_TS                (3)
#define PAN_TS                 (2)
#define STAN_TS                (2)
#define TRANS_AMOUNT_TS        (2)
#define TRANS_CURR_CODE_TS     (2)
#define TRANS_CURR_EXP_TS      (2)
#define PIN_BLOCK_FORMAT_TS    (3)
#define PIN_TRY_FLAG_TS        (2)
#define ADD_SCREEN_TEXT_TS     (3)
#define ADD_SCREEN_TEXT_2_TS   (3)
#define MIN_PIN_LENGTH_TS      (3)
#define MAX_PIN_LENGTH_TS      (3)
#define PIN_ENTRY_TIMEOUT_TS   (3)
#define MESS_FOR_MAC_TS        (2)
#define MAC_TS                 (2)
#define MESS_FOR_ENC_TS        (2)
#define PIN_ENTRY_TYPE_TS      (2)
#define KEY_MANAGEMENT_TS      (3)
#define PIN_ALGORITHM_TS       (3)
#define DATA_ALGORITHM_TS      (3)
#define KSN_TS                 (3)
#define TRANS_CAT_EXP_TS       (2)
#define PLAIN_TXT_PIN_BLOCK_TS (3)
#define SCRIPT_NAME_TS         (3)
#define PIN_CANCEL_TS          (3)
#define ONLINE_PIN_CIPHER_TS   (2)
#define MESS_FOR_DEC_TS        (3)
#define FLAGS_TS               (3)
#define MSG_ENTER_PIN_TS       (3)
#define MSG_AMOUNT_TS          (3)
#define MSG_FONT_FILE_TS       (3)
#define MONITOR_PERIOD_TS      (3)
#define SCREEN_LINES_TS        (3)
#define PROMPT_FILL_TS         (3)
#define DUKPT_MASK_TS          (3)


/*
#define ENTER_AND_ENCRYPT_PIN_COM  (24)
#define ENTER_AND_HOLD_PIN_COM     (25)

#define CANCEL_PIN_COM             (27)
//#define CANCEL_PROCESS_COM         (36)
//#define LOCK_CANCEL_PROCESS_COM    (37)
#define LOAD_DUKPT_COM             (41)
#define SELECT_HOST_CONF_COM       (42)

#define SET_SECURITY_CONF_COM      (48)
#define GET_SECURITY_CONF_COM      (49)
#define INCREMENT_KSN_COM          (50)
//#define DECRYPT_STREAM_COM         (51)
//#define ENCRYPT_STREAM_COM         (52)
//#define WAIT_FOR_CARD              (53)
//#define RESET_EXTERNAL_READER      (54)
#define AS2805_KEY_MGMENT_COM      (55)
#define SET_DUKPT_MASK_COM         (59)
#define GET_VSS_FILE_NAMES_COM     (61)
*/

enum sec_cmd_code
{
    UPDATE_KEY_COM =43,
    RETRIEVE_ENCRYPTED_PIN_COM = 26,
    GENERATE_MAC_COM = 44,
    VERIFY_MAC_COM = 45,
    ENCRYPT_DATA_COM=46,
    DECRYPT_DATA_COM=47,
    GET_KEY_DATA_COM = 54,
    GET_SVC_VERSION_COM = 60,
    OPEN_SCHEMA_COM = 62,
    CLOSE_SCHEMA_COM = 63,
    PUT_TRANSACTIONDATA_COM = 64,
    GET_KEY_INVENTORY_COM = 65,
    GET_TRANSACTION_VALUE = 66,
    GET_PERMISSION = 67,
    CLEAR_TRANSACTIONDATA_COM = 68,
    GET_STATUS_COM = 69,

    CRT_CHECK_CONN_COM = 70,
    CRT_GET_CERT_CHAIN_COM = 71,
    CRT_GET_KEY_COM = 72,
    CRT_UPDATE_CERT_COM = 73,
    CRT_UPDATE_KEY_COM = 74,
    CRT_GET_SVC_VERSION_COM = 75,
};


#define AMOUNT_EMV_TS         (2)
#define CURR_CODE_EMV_TS      (2)
#define CURR_EXP_EMV_TS       (2)

#define DOUBLE_KEY_SIZE (32)


#define MAPP_UNLOCK_DISPLAY   (57)
#define MAPP_LOCK_DISPLAY     (58)

#define INSTALL_VSS_COM       (160)
#define EXECUTE_VSS_MACRO_COM   (161)

#ifndef _VRXEVO
  #define SC_PIPE_NAME          "/tmp/scdata";
  #define CERTSTORE_PIPE_NAME   "/tmp/csdata";
#else
  #define SC_PIPE_NAME          "scdata";
  #define CERTSTORE_PIPE_NAME   "csdata";
  #define DRIVE_RAM         "I:"
  #define DRIVE_FLASH       "F:"
#endif

// environment variables
//#define LOGMASK_VALUE       "SEC_LOGMASK"
#define IPP_SET_SPEED       "IPP19K"
#define CFG_FILE_NAME       "SCAPPCFG"
#define CFG_FILE_NAME_DEF   "sccfg.json"


// Calculation basic parameters used by secDigits() for LRC/CRC:
//    Width of crc in bits (8, 16 or 32)
//    Generator polynomial (poly)
//    Initialization value (init=0 or -1 here)
//    Whether input bytes are reflected (refin=1) or not (refin=0) before being used
//    Whether the output crc is reflected (refot=1) or not (refot=0) at end of calculation
//    A value to exclusive-or to the final crc (xorot=0 or -1 here)
//
// Finally a check value (chk) is supplied to show the result of applying the alogorithm
// to the 9 byte ascii test message "123456789"

#define SEC_DIG_LRC            0x00 // width=8, poly=0x01,      init=0, refin=0,refot=0,xorot=0, chk=0x31
#define SEC_DIG_CRC_CRC16LSB   0x01 // width=16,poly=0xa001,    init=0, refin=0,refot=0,xorot=0, chk=0xa47b
#define SEC_DIG_CRC_CRC16MSB   0x02 // width=16,poly=0x8005,    init=0, refin=0,refot=0,xorot=0, chk=0xfee8;      CRC-16/BUYPASS
#define SEC_DIG_CRC_CCITTLSB   0x03 // width=16,poly=0x8408,    init=-1,refin=0,refot=0,xorot=0, chk=0x0520
#define SEC_DIG_CRC_CCITTMSB   0x04 // width=16,poly=0x1021,    init=-1,refin=0,refot=0,xorot=0, chk=0x29b1;      CRC-16/CCITT-FALSE
#define SEC_DIG_CRC_CRC32LSB_0 0x05 // width=32,poly=0x04c11db7,init=0, refin=1,refot=1,xorot=0, chk=0x2dfd2d88
#define SEC_DIG_CRC_CRC32LSB   0x06 // width=32,poly=0x04c11db7,init=-1,refin=1,refot=1,xorot=-1,chk=0xcbf43926;  CRC-32
#define SEC_DIG_CRC_CRC16MSB_1 0x07 // width=16,poly=0x8005,    init=0, refin=1,refot=1,xorot=0, chk=0xbb3d;      CRC-16/ARC
#define SEC_DIG_CRC_CCITTMSB_1 0x08 // width=16,poly=0x1021,    init=-1,refin=1,refot=1,xorot=0, chk=0x6f91;      CRC-16/MCRF4XX

#define SEC_DIG_SHA1           0x10
#define SEC_DIG_SHA256         0x11
//#define SEC_DIG_SHA512         0x12

namespace com_verifone_host
{

/***************************************************************************
 * Macro definitions
 **************************************************************************/

/***************************************************************************
 * Data type definitions
 **************************************************************************/
  /**
   * Data type definitions
   **/

  // for backward compatibility
  #define KEY_TYPE_TPK_FOR_PIN       KEY_TYPE_PPK
  #define KEY_TYPE_TAK_FOR_GEN_MAC   KEY_TYPE_MGK
  #define KEY_TYPE_TPK_FOR_ENC_DATA  KEY_TYPE_DEK
  #define KEY_TYPE_TAK_FOR_VER_MAC   KEY_TYPE_MVK
  #define KEY_TYPE_TPK_FOR_DEC_DATA  KEY_TYPE_DDK
  #define KEY_TYPE_TPK_FOR_KEK       KEY_TYPE_KEK

  /** Key types */
  enum key_type_e
  {
    KEY_TYPE__INVALID_E        = -1,  /**< Value not set */
    KEY_TYPE_TMK               = 0,  /**< Termina Master Key */
    KEY_TYPE_PPK               = 1,  /**< PIN  Protection Key */
    KEY_TYPE_MGK               = 2,  /**< MAC  Generation Key*/
    KEY_TYPE_DEK               = 3,  /**< Data Encryption Key*/
    KEY_TYPE_TCU_PK            = 4,  /**< Bendigo only: TCU RSA Public Key */
    KEY_TYPE_SPONSOR_PK        = 5,  /**< Bendigo only: Sponsor RSA Public Key */
    KEY_TYPE_SPONSOR_MK        = 6,  /**< Bendigo only: Sponsor Master Keys */
    KEY_TYPE_SPONSOR_KI        = 7,  /**< Bendigo only: Sponsor Initialisation Key */
    KEY_TYPE_SEC_ACQ_KI        = 8,  /**< Bendigo only: Secondary Acquirer Initialisation Key */
    KEY_TYPE_ACQUIRER_MK       = 9,  /**< Bendigo only: Acquirer Master Keys */
    KEY_TYPE_SEC_ACQ_MK        = 10, /**< Bendigo only: Secondary Acquirer Master Keys */
    KEY_TYPE_ACQ_SESSION_KEYS  = 11, /**< Bendigo only: Acquirer Session Keys */
    KEY_TYPE_ENC_KI_FMT_0      = 12, /**< Bendigo only: Encrypted KI (Format 0) */
    KEY_TYPE_ENC_KI_FMT_1      = 13, /**< Bendigo only: Encrypted KI (Format 1) */
    KEY_TYPE_ENC_KI_FMT_2      = 14, /**< Bendigo only: Encrypted KI (Format 2) */
    KEY_TYPE_ENC_PPID          = 15, /**< Bendigo only: Encrypted PPID */
    KEY_TYPE_ENC_PPASN         = 16, /**< Bendigo only: Encrypted PPASN */
    KEY_TYPE_KVC_KIA           = 17, /**< Bendigo only: KVC of KIA */
    KEY_TYPE_KVC_KEK1          = 18, /**< Bendigo only: KVC of KEK1 */
    KEY_TYPE_MVK               = 19, /**< MAC  Verification Key*/
    KEY_TYPE_DDK               = 20, /**< Data Decryption Key*/
    KEY_TYPE_KEK               = 21, /**< Key  Encryption Key*/
    KEY_TYPE_DUKPT             = 22, /**< DUKPT key*/
    KEY_TYPE_CERT              = 23, /**< Certificate */
    KEY_TYPE_DIRECT_MK_PLAIN   = 24, /**< iPS_LoadMasterClearKey replacement.
                                          Loads the security script’s master key. The values are sent in the clear, but must all be loaded in the same session. \n
                                          Before loading the first key after a power cycle, all previously loaded keys (including the system keys) are erased.\n\n
                                          <b>This function should be used exclusively in a secure environment.</b>*/
    KEY_TYPE_DIRECT_MK_ENC     = 25, /**< iPS_LoadMasterEncKey replacement.
                                          Loads the security script’s master key without deleting the keys already loaded. The key value must be encrypted with VSS_KLK*/
    KEY_TYPE_DIRECT_SYSK_PLAIN = 26, /**< iPS_LoadSysClearKey replacement.
                                          Loads the VSS_KLK (system key) in the clear. \n
                                          Before writing the new value of the key, all other keys in the terminal are erased. \n\n
                                          <b>This function should be used exclusively in a secure environment.</b> */
    KEY_TYPE_DIRECT_SYSK_ENC   = 27, /**< iPS_LoadSysEncKey replacement.
                                          Loads the system key. The new values must be encrypted under the current value of VSS_KLK.*/
    KEY_TYPE_DELETE_ALL_KEYS   = 28, /**< iPS_DeleteKey replacement.*/
    KEY_TYPE_DELETE_SYS_KEY    = 29, /**< iPS_DeleteKey replacement.*/
#if defined VFI_PLATFORM_VOS || defined linux
    KEY_TYPE_DIRECT_AUTHEX_ENC = 30, /**< rsaAuthexVssKeyInstall replacement.
                                          Loads the VSS key. The new values must be encrypted under AuthEx.*/
    KEY_TYPE_DIRECT_AUTHEX_SYSK= 31, /**< rsaAuthexVssKeyInstall replacement.
                                          Loads the VSS_KLK. The new values must be encrypted under AuthEx.*/
#endif
    KEY_TYPE_DELETE_ALL_CUST_KEYS = 32, /**< iPS_DeleteKey replacement for DEL_ALL - clears all OS-managed customer keys like VSS, IPP and ADE.*/
  };
  typedef uint8_t  key_type_t;



  /** PIN Entry Type */
  typedef enum pin_entry_type_e
  {
    PIN_ENTRY_TYPE__INVALID_E             = -1, /**< Value not set */
    PIN_ENTRY_TYPE_MANDATORY_E            = 0,  /**< PIN Mandatory  */
    PIN_ENTRY_TYPE_OPTIONAL_E             = 1,  /**< PIN Optional */
    PIN_ENTRY_TYPE_OPTIONAL_0LN_PIN_ENC_E = 2,  /**< PIN Optional 0 length PIN Encryption */
    PIN_ENTRY_TYPE_OPTIONAL_0LN_PIN_E     = 3   /**< 0 Length PIN */

  } pin_entry_type_t;


  /** PIN Block Format */
  typedef enum pin_block_format_e
  {
    PIN_BLOCK_FORMAT__INVALID_E = -1,   /**< Value not set */
    PIN_BLOCK_FORMAT_ISO0_E     = 0,    /**< ISO-0 */
    PIN_BLOCK_FORMAT_ISO1_E     = 1,    /**< ISO-1 */
    PIN_BLOCK_FORMAT_ISO2_E     = 2,    /**< ISO-2 */
    PIN_BLOCK_FORMAT_ISO3_E     = 3,    /**< ISO-3 */
    PIN_BLOCK_FORMAT_INC_KSN    = 4,    /** increment KSN only - no pin entry request*/
  } pin_block_format_t;

#define PIN_BLOCK_FORMAT_MIN (PIN_BLOCK_FORMAT_ISO0_E)
#define PIN_BLOCK_FORMAT_MAX (PIN_BLOCK_FORMAT_INC_KSN)


  /** KSN key state definition */
#define NEXT_KSN      1  /**< KSN key state: get next KSN/key */
#define SAME_KSN      2  /**< KSN key state: use same KSN/key */
#define VISADSP_NEXT_KSN      NEXT_KSN
#define VISADSP_SAME_KSN      SAME_KSN

#define DO_KSN_INCREMENTATION   "KSN incrementation"


/***************************************************************************
 * Exported variable declarations
 **************************************************************************/

/***************************************************************************
 * Exported class declarations
 **************************************************************************/

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

#endif // SEC_H

