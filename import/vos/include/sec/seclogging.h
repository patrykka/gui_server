#ifndef _SECLOGGING_H_
#define _SECLOGGING_H_

#include <string>

#if (defined _VRXEVO || defined _WIN32)
#  if defined VFI_SEC_SHARED_EXPORT
#    define DllSpecSEC __declspec(dllexport)
#  elif defined VFI_SEC_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpecSEC
#  else
#    define DllSpecSEC __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_SEC_SHARED_EXPORT
#  define DllSpecSEC  __attribute__((visibility ("default")))
#else
#  define DllSpecSEC
#endif

#define LOGNAME_SECDAEMON "SEC"
#define LOGNAME_SECLIB "SECLIB"
#define LOGNAME_CERTSTORE "CERTSTORE"

/*  API to be used only in this header file for macros */
DllSpecSEC void SECINTERNAL_LOG_EMERG(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_ALERT(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_CRIT(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_ERROR(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_WARN(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_NOTICE(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_INFO(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_TRACE(const char *file, unsigned int line, const char *fmt, ...) __attribute__((format(printf, 3, 4)));
DllSpecSEC void SECINTERNAL_LOG_HEXDUMP_INFO(const char *title, const void *data, unsigned int size);
DllSpecSEC void SECINTERNAL_LOG_HEXDUMP_NOTICE(const char *title, const void *data, unsigned int size);
DllSpecSEC void SECINTERNAL_LOG_HEXDUMP_TRACE(const char *title, const void *data, unsigned int size);
DllSpecSEC void SECINTERNAL_PRINT_CALLSTACK();

DllSpecSEC /*  API to be used in ADK SEC source files */
DllSpecSEC void SEC_LOG_INIT(const char *name);
DllSpecSEC void SEC_LOG_DEINIT();

#define SEC_LOG_EMERG(...) SECINTERNAL_LOG_EMERG(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_ALERT(...) SECINTERNAL_LOG_ALERT(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_CRIT(...) SECINTERNAL_LOG_CRIT(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_ERROR(...) SECINTERNAL_LOG_ERROR(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_WARN(...) SECINTERNAL_LOG_WARN(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_NOTICE(...) SECINTERNAL_LOG_NOTICE(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_INFO(...) SECINTERNAL_LOG_INFO(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_TRACE(...) SECINTERNAL_LOG_TRACE(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_LOG_HEXDUMP_TRACE(a, b, c) SECINTERNAL_LOG_HEXDUMP_TRACE(a, b, c)
#define SEC_LOG_PRINT_CALLSTACK  SECINTERNAL_PRINT_CALLSTACK

#ifdef _DEBUG
#define SEC_DBG_ERROR(...) SECINTERNAL_LOG_ERROR(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_DBG_WARN(...) SECINTERNAL_LOG_WARN(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_DBG_INFO(...) SECINTERNAL_LOG_INFO(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_DBG_NOTICE(...) SECINTERNAL_LOG_NOTICE(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_DBG_TRACE(...) SECINTERNAL_LOG_TRACE(__FILE__, __LINE__, __VA_ARGS__)
#define SEC_DBG_HEXDUMP_INFO(a, b, c) SECINTERNAL_LOG_HEXDUMP_INFO(a, b, c)
#define SEC_DBG_HEXDUMP_NOTICE(a, b, c) SECINTERNAL_LOG_HEXDUMP_NOTICE(a, b, c)
#define SEC_DBG_HEXDUMP_TRACE(a, b, c) SECINTERNAL_LOG_HEXDUMP_TRACE(a, b, c)
#else
#define SEC_DBG_ERROR(...)
#define SEC_DBG_WARN(...)
#define SEC_DBG_INFO(...)
#define SEC_DBG_NOTICE(...)
#define SEC_DBG_TRACE(...)
#define SEC_DBG_HEXDUMP_INFO(a, b, c)
#define SEC_DBG_HEXDUMP_NOTICE(a, b, c)
#define SEC_DBG_HEXDUMP_TRACE(a, b, c)
#endif

#undef DllSpecSEC // important to avoid duplicated definitions of DllSpec with other component header files

#endif //_SECLOGGING_H_
