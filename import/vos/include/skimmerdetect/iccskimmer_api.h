/** @addtogroup */
/** @{ */
/*****************************************************************************/
/* This source code is confidential proprietary information of VeriFone Inc. */
/* and is an unpublished work or authorship protected by the copyright laws  */
/* of the United States. Unauthorized copying or use of this document or the */
/* program contained herein, in original or modified form, is a violation of */
/* Federal and State law.                                                    */
/*                                                                           */
/*                         Verifone Inc.                                     */
/*                        Copyright 2019                                     */
/*****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | iccskimmer_api.h
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | Header File for the Probe Card API Library
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Contains equates, definitions and function prototypes to
;|             | interface with Skimmer probe.
;|-------------+-------------------------------------------------------------+
;| TYPE:       |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|===========================================================================+
*/

#ifndef __ICCSKIMMER_API_H__
#define __ICCSKIMMER_API_H__

#ifdef __cplusplus
extern "C" {
#endif

//
// Skimmer Card API Response Code definitions
//
typedef int SKIMRESPCODE;               // Response code type definition

#define SKIMRC_Success               0  // Operation was successful
#define SKIMRC_Failure               1  // Operation failed
#define SKIMRC_Error_Power_Action    2  // Power-Up operation failed
#define SKIMRC_Error_Not_Supported   3  // Skimmer probe type not supported
#define SKIMRC_Error_BadParam        4  // A function parameter is invalid
#define SKIMRC_Error_Protocol        5  // Communication error with skimmer probe

#define SKIM_DEFAULT_LED_ON_TIME     3000000  // 3 seconds
#define ICC_SKIM_PASS_ADDR           0xA0
#define ICC_SKIM_FAIL_ADDR           0xA2

//
// skimmer probe Types
//
#if 0
enum
{
    SKIMCT_AT24C01SC = 0,
    SKIMCT_AT24C02SC,
    SKIMCT_LAST_DEVICE
} ENUM_PROBE_TYPE;
#endif

#if 1
#define SKIMCT_AT24C01SC             1  // Atmel AT24C01SC
#define SKIMCT_AT24C02SC             2  // Atmel AT24C02SC
#define SKIMCT_AT24C04SC             3  // Atmel AT24C04SC
#define SKIMCT_AT24C08SC             4  // Atmel AT24C08SC
#define SKIMCT_AT24C16SC             5  // Atmel AT24C16SC
#define SKIMCT_ST14C02SC             6  // STMicro ST14C02SC
#define SKIMCT_AT24C32SC             7  // Atmel AT24C16SC
#define SKIMCT_AT24C64SC             8  // Atmel AT24C16SC
#endif

enum
{
    EEPROM_AREA = 0,
    SERIAL_NUM_AREA,
    SKIMCT_LAST_AREA
} ENUM_PROBE_AREA;

#define ADDRESS_SIZE                 0x01
#define SERIAL_NUMBER_SIZE           0x10

// This global variable can be changed to set the duration LEDs are on (microseconds).
// Default duration is SKIM_DEFAULT_LED_ON_TIME (3 seconds).
extern unsigned long g_ulIccSkimLedOnDelayUs;

//****************************************************************************
/// @brief Open the skimmer probe driver @n
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SKIMRC_Success - Probe card driver opened.
/// @returns SKIMRC_Failure - Error, probe card driver is already opened.
//****************************************************************************
SKIMRESPCODE IccSkimmer_Open(unsigned long Options);


//****************************************************************************
/// @brief Close the skimmer probe driver @n
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SKIMRC_Success - Probe card driver closed.
/// @returns SKIMRC_Failure - Error, probe card driver is not opened.
//****************************************************************************
SKIMRESPCODE IccSkimmer_Close(unsigned long Options);


//****************************************************************************
/// @brief Check if a probe is inserted in the full-sized card reader.  @n
///
/// @param   None
///
/// @returns SKIMRC_Success - A probe is inserted.
/// @returns SKIMRC_Failure - No probe inserted.
//****************************************************************************
SKIMRESPCODE IccSkimmer_Detect( void );


//****************************************************************************
/// @brief Power up the probe @n
///
/// @param[in]     probeType   - probe card type
///
/// @returns SKIMRC_Success              - No probe
/// @returns SKIMRC_Failure              - Error, driver not opened or card not inserted
/// @returns SKIMRC_Error_Power_Action   - Error, unable to power up card
/// @returns SKIMRC_Error_Not_Supported  - Invalid CardType
//****************************************************************************
SKIMRESPCODE IccSkimmer_Activate(long           probeType);


//****************************************************************************
/// @brief Power down the probe card @n
///
/// @param   None
///
/// @returns SKIMRC_Success - Probe powered down successfully.
/// @returns SKIMRC_Failure - Error powering down card - not powered up.
//****************************************************************************
SKIMRESPCODE IccSkimmer_Deactivate( void );


//****************************************************************************
/// @brief Read data from probe @n
///
/// @param[in]     MemoryArea       - memory type to start reading from
///                                   either EEPROM_AREA or SERIAL_NUM_AREA
/// @param[in]     MemoryAddress    - memory address where to start reading from
///                                   if -1, start at current location
/// @param[in/out] RxBuf            - Pointer to buffer where read data is stored
///                                   Must be large enough to hold 'RxLen' bytes
/// @param[in]     RxLen            - Number of bytes to read from card
///
/// @returns SKIMRC_Success         - Operation successful
/// @returns SKIMRC_Failure         - Error - driver not opened, probe not inserted or powered up
/// @returns SKIMRC_Error_BadParam  - Invalid parameter, RxLen is zero, or RxBuf is NULL
/// @returns SKIMRC_Error_Protocol  - Error communicating with the card - wrong ProbeType
//****************************************************************************
SKIMRESPCODE IccSkimmer_ReadProbe(long          MemoryAddress,
                              unsigned char     AccessArea,
                              unsigned char     RxBuf[],
                              unsigned short    RxLen);


//****************************************************************************
/// @brief Write data to the probe @n
///
/// @param[in]     MemoryAddress    - memory address where to start reading from
/// @param[in/out] TxBuf            - Pointer to buffer where data to write is stored
/// @param[in]     TxLen            - Number of bytes to write to the card
///
/// @returns SKIMRC_Success         - Operation successful
/// @returns SKIMRC_Failure         - Error - driver not opened, card not inserted or powered up
/// @returns SKIMRC_Error_BadParam  - Invalid parameter, TxLen is zero, or TxBuf is NULL
/// @returns SKIMRC_Error_Protocol  - Error communicating with the probe - wrong ProbeType
//****************************************************************************
SKIMRESPCODE IccSkimmer_WriteProbe(long            MemoryAddress,
                               const unsigned char TxBuf[],
                               unsigned short      TxLen);


//****************************************************************************
/// @brief Read serial number from probe @n
///
/// @param[in/out] RxBuf                - Pointer to buffer where read data is stored
///                                       Must be large enough to hold result + 'RxLen' bytes
///                                       out = 1 byte result + 16 bytes serial number
/// @param[in/out] SerialLen            - Pointer to serial number length
///                                       0 if not able operation fails
///
/// @returns SKIMRC_Success         - Operation successful
/// @returns SKIMRC_Failure         - Error - driver not opened, probe not inserted or powered up
/// @returns SKIMRC_Error_BadParam  - Invalid parameter, RxLen is zero, or RxBuf is NULL
/// @returns SKIMRC_Error_Protocol  - Error communicating with the card - wrong ProbeType
//****************************************************************************
SKIMRESPCODE IccSkimmer_ReadSerialNum( unsigned char  RxBuf[],  int * SerialNumLen);

//****************************************************************************
/// @brief Get the NULL-terminated version string for this library @n
///
/// @param[in/out] pVersion        - Pointer to buffer to store version string
/// @param[in]     nSize           - Size of the buffer
///
/// @returns SKIMRC_Success        - Operation successful
/// @returns SKIMRC_Error_BadParam - Invalid parameter, nSize is too small, or pVersion is NULL
//****************************************************************************
SKIMRESPCODE IccSkimmer_GetVersion(char *pVersion,
                                int   nSize);


//****************************************************************************
/// @brief Set C4 High
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SKIMRC_Success - A probe is inserted.
/// @returns SKIMRC_Failure - No probe inserted.
//****************************************************************************
SKIMRESPCODE IccSkimmer_SetC4(unsigned long Options);


//****************************************************************************
/// @brief Set C4 Low
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SKIMRC_Success - A probe is inserted.
/// @returns SKIMRC_Failure - No probe inserted.
//****************************************************************************
SKIMRESPCODE IccSkimmer_ClrC4(unsigned long Options);


//****************************************************************************
/// @brief Set C8 High
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SKIMRC_Success - A probe is inserted.
/// @returns SKIMRC_Failure - No probe inserted.
//****************************************************************************
SKIMRESPCODE IccSkimmer_SetC8(unsigned long Options);


//****************************************************************************
/// @brief Set C8 Low
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SKIMRC_Success - A probe is inserted.
/// @returns SKIMRC_Failure - No probe inserted.
//****************************************************************************
SKIMRESPCODE IccSkimmer_ClrC8(unsigned long Options);


#ifdef __cplusplus
}
#endif

#endif // __ICCSKIMMER_API_H__
/// @} */
