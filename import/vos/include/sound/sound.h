#ifndef SOUND_H_20170901
#define SOUND_H_20170901

/** \file sound.h */

#include <stdio.h>

#if defined VFI_SOUND_EXPORT  // Export
#  if defined _VRXEVO || defined _WIN32
#    define DllSpec __declspec(dllexport)
#  elif defined __GNUC__
#    define DllSpec  __attribute__((visibility ("default")))
#  else
#    define DllSpec
#  endif
#else                         // Import
#  if defined _VRXEVO
#    define DllSpec __declspec(dllimport)
#  else
#    define DllSpec
#  endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if 0 // fix for automatic indentation
}
#endif

struct SND;

/** SND handle identifying a sound file */
typedef struct SND SND;

/** error codes */
enum SNDError {
   SND_OK=0,              /**< no error */
   SND_ERR_FAIL=-1,       /**< generic error */
   SND_ERR_PARAMETER=-2,  /**< invalid parameters */
   SND_ERR_NOT_FOUND=-3,  /**< file not found or cannot be accessed */
   SND_ERR_MEMORY=-4,     /**< out of memory */
   SND_ERR_UNSUPPORTED=-5,/**< unsupported format */
   SND_ERR_READ=-6        /**< read error */
};


/** open file and return handle. Playback does not start automatically, use sndStart() to start playing the file.
 * \param[in] filename file name of a sound file.
 * \return handle or NULL if the file cannot be found or the file format is not supported or if the sound device 
 * cannot be accessed.
 */
DllSpec struct SND* sndOpenFile(const char *filename);

/** close the file opened using ::sndOpenFile
 * \param[in] handle handle obtained by sndOpenFile()
 */
DllSpec void sndClose(struct SND *handle);

/** start playback. Playback starts at the current position.
 * \param[in] handle handle obtained by sndOpenFile()
 * \return error code
 */
DllSpec enum SNDError sndStart(struct SND *handle);

/** stop playback. Stopping does not reset the current position, i.e. invoking sndStart() after sndStop()
 * resumes playback at the position when sndStop() was invoked.
 * \param[in] handle handle obtained by sndOpenFile()
 * \return error code
 */
DllSpec enum SNDError sndStop(struct SND *handle);

/** change the current position
 * \param[in] handle handle obtained by sndOpenFile()
 * \param[in] pos position in seconds
 * \param[in] whence one of SEEK_SET, SEEK_CUR, SEEK_END to specify whether the position \a pos is
 *            with respect to the file start, the current position or file end.
 * \return error code
 */
DllSpec enum SNDError sndSeek(struct SND *handle, double pos, int whence);

/** read the current position
 * \param[in] handle handle obtained by sndOpenFile()
 * \return position in seconds from the file start or error code if less than 0.
 */
DllSpec double sndTell(struct SND *handle);

/** read the estimated length of the file
 * \param[in] handle handle obtained by sndOpenFile()
 * \return estimated length in seconds or error code if less than 0.
 * \note The returned length is an estimate only, that can be obtained by reading the header of the file
 * and may not represent the exact length. Obtaining the exact length may not be possible without scanning
 * the whole file (e.g. for MP3).
 */
DllSpec double sndLength(struct SND *handle);

/** sound callback function
 * \param[in] data data pointer
 * \param[in] handle handle obtained by sndOpenFile()
 * \param[in] reason reason why the callback was invoked: At end of file, this is SND_OK, in case of error it is the error code
 */
typedef void (*SNDCallback)(void *data, struct SND *handle, enum SNDError reason);

/** set callback that gets invoked when playback stops due to end of file or due to error.
 * \param[in] handle handle obtained by sndOpenFile()
 * \param[in] cb function pointer, use NULL to delete callback
 * \param[in] data data pointer that gets passed on to the callback
 */
DllSpec void sndSetCallback(struct SND *handle,
                            SNDCallback cb,
                            void *data);


/** check whether the device has sound support for playback of MP3 or WAV files
 * \return 1 if sound support is present, else 0
 */
DllSpec int sndSoundSupport();

/** set master volume in percent
 * \param[in] volume_percent volume in the range [0..100]
 * return error code
 */
DllSpec enum SNDError sndSetMasterVolume(int volume_percent);

/** read master volume
 * \return volume in percent (range [0..100]) or error code if less than 0.
 */
DllSpec int sndGetMasterVolume(void);

#ifdef __cplusplus
}
#endif

#undef DllSpec
#endif
