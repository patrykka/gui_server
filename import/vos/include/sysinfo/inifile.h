#pragma once

#include "dllspec.h"
#include <map>
#include <vector>
#include <string>
 
#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo 
{
#endif

/** class for accessing INI files */
class IniFile 
{
        std::string filename;
        std::map<std::string,std::string> config;
        int error_line;

    public:
        std::vector<std::string> section_names;
        enum Mode { READONLY, READWRITE, WRITEONLY };

        /** constructor, open INI file 
        * \param[in] filename_ file name
        */
        SYS_INFO_API IniFile(const std::string &filename_, Mode mode );

        /** constructor, open INI file 
        * \param[in] in file content
        */ 
        SYS_INFO_API IniFile( const std::string & content );

        /** constructor, open INI file 
        * \param[in] filename_ file name
        * \param[in] in file content
        */ 
        SYS_INFO_API IniFile(const std::string &filename_, const std::string &content, Mode mode=READWRITE);

        /** read content of section
        * \param[in] section section to be read
        * \return section content 
        */
        SYS_INFO_API std::string operator()(const std::string &section) const;

        /** read key 
        * \param[in] section section that contains the key
        * \param[in] var key name
        * \param[in] defaultvalue default value that is returned if the key does not exist
        * \return key value 
        */
        SYS_INFO_API std::string operator()(const std::string &section,const std::string &var, const std::string &defaultvalue="") const;

        /** reads all values from \a section to map \a values as key-value pairs
        *  \param[in] section section that contains the key value pairs
        *  \param[in] values
        */
        SYS_INFO_API void get_section(const std::string &section, std::map<std::string,std::string> &values);

        /** remove key from memory
        * \param[in] section section that contains the key
        * \param[in] var key name
        */
        SYS_INFO_API void remove(const std::string &section, const std::string &var);

        /** set key in memory
        * \param[in] section section that contains the key
        * \param[in] var key name
        * \param[in] value key value
        */
        SYS_INFO_API void set(const std::string &section, const std::string &var, const std::string &value);

        /** write memory content back to file 
        * \return true in case of success, else false
        */
        SYS_INFO_API bool write() const;

        /** Check if the constructor has read a file or if a new configuration file is to be created
        *  \return -1 in case the file did not exist, 0 in case the file could be read without error, >0 line number of syntax error  */
        SYS_INFO_API int error() const { return error_line; }

        /** clear content, reset error */ 
        SYS_INFO_API void clear() { config.clear(); error_line=0; }

        std::string getstring( 
        std::map<std::string,std::string> & values,
        const std::string & id, 
        const std::string & alternative = "" ); 
};

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

