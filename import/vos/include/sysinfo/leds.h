#pragma once

/** \file led.h
 * \defgroup led System LED functions
 * @{
 */

#include "dllspec.h"

namespace vfisysinfo {

// System LEDs
enum LEDs
{
    CTLS_LED_NONE   = 0,
    MSR_LED_NONE    = 0,

    MSR_LED_0       = (1<<0),
    MSR_LED_1       = (1<<1),
    MSR_LED_2       = (1<<2),
    KEYPAD_LED      = (1<<3),
    SMARTCARD_LED   = (1<<4),
    CTLS_LED_LOGO   = (1<<5),
    CTLS_LED_0      = (1<<6),
    CTLS_LED_1      = (1<<7),
    CTLS_LED_2      = (1<<8),
    CTLS_LED_3      = (1<<9),
    SYSTEM_LED      = (1<<10),
    POWER_LED       = (1<<11),
    BLUETOOTH_LED   = (1<<12),
    MODEM_LED       = (1<<13),

    MSR_LED_ALL     = MSR_LED_0 | MSR_LED_1 | MSR_LED_2,
    CTLS_LED_ALL    = CTLS_LED_0 | CTLS_LED_1 | CTLS_LED_2 | CTLS_LED_3 | CTLS_LED_LOGO
};

// LEDs states
enum LedStates
{
	SWITCH_OFF	= 0,
	SWITCH_ON	= 1
};

// LED brightness
enum LedBrightness
{
    MAX_BRIGHTNESS  = 255,
    MIN_BRIGHTNESS  = 0
};

/** Switch on of off Contactless LEDs
 * \param[in] ledMap
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = ctlsLedsChangeState( CTLS_LED_0 | CTLS_LED_1 | CTLS_LED_2 | CTLS_LED_3 ).
*/
SYS_INFO_API int ctlsLedsChangeState( int ledMap );

/** Switch on of off Magnetic Card Reader LEDs
 * \param[in] ledMap
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = msrLedsChangeState( MSR_LED_ALL ).
*/
SYS_INFO_API int msrLedsChangeState( int ledMap );

/** Switch on of off Secure Card Reader LEDs
 * \param[in] state
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = scrLedChangeState( SWITCH_ON ).
*/
SYS_INFO_API int scrLedChangeState( int state );

/** Run the MSR LEDs runway show
 * \param[in] repeatCount; 0 - for infinite repeat times ( until msrShowCancel() )
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = msrShowRunway( 5 ).
*/
SYS_INFO_API int msrShowRunway( int repeatCount );

/** Cancel the MSR LED show runway
  * \return 0 - On success
 */
SYS_INFO_API int msrShowCancel( void );

/** Switch on or off Logo LED
 * \param[in] state
 * \return 0 - On success. Otherwise - error code
 * \note Verix not supported.
 * Example: status = logoLedChangeState( SWITCH_ON ).
*/
SYS_INFO_API int logoLedChangeState( int state );

/** Set LEDs brightness
 * \param[in] ledMap
 * \param[in] Brightness
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = setBrightness( MSR_LED_0 | MSR_LED_1 | MSR_LED_2 ).
*/
SYS_INFO_API int setBrightness( unsigned int ledMap, int brightness );

/** Check if LED brightness adjustable
 * \param[in] LED ID
 * \return true - brightness adjustable. false - otherwise
 * \note Example: status = isBrightnessAdjustable( CTLS_LED_LOGO ).
*/
SYS_INFO_API bool isBrightnessAdjustable( unsigned int ledId );
}

/** @}*/

