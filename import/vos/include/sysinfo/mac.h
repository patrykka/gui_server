#pragma once

// STL
#include <string>
#include <vector>
#include <map>

// ADK
#include <ipc/jsobject.h>

/** \file mac.h
 * \defgroup mac Multi Application Controller functions
 * @{
 */

#ifndef DOXYGEN
/** Verifone Multi Application Controller (vfimac) namespace */
namespace vfimac 
{
#endif

#if defined VFI_MAC_DLL_EXPORT
    #define MAC_API  __attribute__((visibility ("default")))
    /*!< Marks public MAC API */
#else
    #define MAC_API
    /*!< Marks public MAC API */
#endif

/** error codes returned by MAC API */
enum macError
{
    MAC_ERR_OK                 =   0, /**< Success */
    MAC_ERR_FAIL               =  -1, /**< Generic error */
    MAC_ERR_UNSUPPORTED        =  -2, /**< Function is not supported */
    MAC_ERR_LOCKED             =  -3, /**< Layout is locked */
    MAC_ERR_TIMEOUT            =  -4, /**< Timeout */
    MAC_ERR_LIBNOTIFY          =  -5, /**< IPC error */
    MAC_ERR_APP_NOT_RUNNING    =  -6, /**< Application is not running */
    MAC_ERR_WRONG_APPID        =  -7, /**< Wrong/unknown application id */
    MAC_ERR_NOT_CP_APP         =  -8, /**< No matching CP application */
    MAC_ERR_UNKNOWN            =  -9, /**< Unknown command */
    MAC_ERR_CYCLE              = -10, /**< Manifest dependency cycle detected */
    MAC_ERR_DEPENDS            = -11, /**< Manifest has missing dependency */
    MAC_ERR_MANIFEST           = -12, /**< Manifest validation error */
    MAC_ERR_IPC                = -13, /**< IPC error */
    MAC_ERR_NO_APP             = -14, /**< No matching application */ 
    MAC_ERR_READY              = -15, /**< MAC is not ready */
    MAC_ERR_ACTIVE             = -16, /**< Application is not activate on this terminal */
    MAC_ERR_STOP               = -17, /**< Can't stop application */
    MAC_ERR_NO_APPID           = -18, /**< No matching appid */
    MAC_ERR_PERMISSION         = -19, /**< Permission error */
    MAC_ERR_SERVICE            = -20, /**< Application is a service */
    MAC_ERR_VISIBLE            = -21, /**< Action not allowed while application is not visible */
    MAC_ERR_DISABLED           = -22, /**< This feature is disabled in MAC configuration file */
    MAC_ERR_LAUNCH             = -23, /**< Error during application launch */
    MAC_ERR_RUNNING            = -24, /**< MAC is not running */
    MAC_ERR_LAYOUT             = -25, /**< Layout doesn't exist */
    MAC_ERR_PARAMETER          = -26, /**< Parameter error */
    MAC_ERR_IGNORE             = -27, /**< Command was ignored due to configuration */
    MAC_ERR_CANCELED           = -28, /**< Canceled by user */
    MAC_ERR_EXISTS             = -29, /**< Doesn't exists */
    MAC_ERR_NO_KEYS            = -30, /**< Keys are not available */
    MAC_ERR_EXPECTED_KEYS      = -31, /**< No information on expected keys */
    MAC_ERR_ACCESS             = -32, /**< No access */
    MAC_ERR_AVAILIBLE          = -33, /**< Not available */
};

enum macLaunchParams
{
    NONE                       = 0, /**< No special flags */
    KEEP_IN_BACKGROUND         = 1, /**< Keep application in background */
};

/** virtual keyboard modes */
enum macKeyboard
{
    MAC_KBD_OFF                = 0, /**< enable virtual keyboard */
    MAC_KBD_ON                 = 1, /**< disable virtual keyboard */
    MAC_KBD_AUTO               = 2, /**< virtual keyboard will appear automatically in case of active input */
};

/** MAC events */
enum macEvent
{
    EVENT_NONE = 0,           /**< no events occurred */                          
    EVENT_MANIFEST_UPDATE,    /**< manifest list was updated */                  
    EVENT_ACTIVATION_UPDATE,  /**< activation list was updated */                
    EVENT_PARAMETER_UPDATE,   /**< parameter list was updated */                 
    EVENT_FOCUS_UPDATE,       /**< focus switched to different application */                                
    EVENT_FOREGROUND,         /**< application transitioned to foreground */                                
    EVENT_BACKGROUND,         /**< application transitioned to background */                                
    EVENT_CP_STARTED,         /**< CP application started */                     
    EVENT_CP_STOPPED,         /**< CP application stopped */                     
    EVENT_LAST,               /**< placeholder */                                
};

/** notifications */
enum macNotifications
{
    NOTIFICATION_NONE                  = (0<<0), /**< no events occurred */ 
    NOTIFICATION_MANIFEST_RESCAN       = (1<<0), /**< manifest list was updated */  
    NOTIFICATION_ACTIVATION_CFG_UPDATE = (1<<1), /**< activation list was updated */
    NOTIFICATION_PARAMETER_CFG_UPDATE  = (1<<2), /**< parameter list was updated */ 
    NOTIFICATION_CP_START              = (1<<3), /**< CP application started */
    NOTIFICATION_CP_STOP               = (1<<4), /**< CP application stopped */
    NOTIFICATION_LAST                  = (1<<6), /**< placeholder */
    NOTIFICATION_ALL                   = 0xFFFF  /**< register callback for all available events */
};

/** MAC event callback. 
 * This callback gets invoked when corresponding event occurs
 * \param[in] json JSON encoded event information 
 * \note Callback will be invoked sequentially for every event one by one in sequence
 */
typedef void (*macEventCallback)( macEvent event, const vfiipc::JSObject & json );

/** Notification callback. 
 * This callback gets invoked when corresponding event occurs
 * \param[in] notification macNotifications event identifier
 * \note Callback will be invoked sequentially for every event one by one in sequence
 * \note please see macNotifications for reference
 */
typedef void (*macNotificationCallback)(macNotifications notification);

/** Register notification callback.
 * \param[in] cb notification callback to be invoked 
 * \param[in] event requested event
 * \note Set callback to NULL to disable callback 
 * \note please see macNotifications for reference
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */
MAC_API macError sysSetEventCallback( macEvent event, macEventCallback cb );
 
/** Register notification callback.
 * \param[in] cb notification callback to be invoked 
 * \param[in] notification_mask mask with requested events 
 * \note Set callback to NULL to disable callback 
 * \note please see macNotifications for reference
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */
MAC_API macError sysNotificationCallback( macNotificationCallback cb, long notification_mask );

/** Register notification callback.
 * \param[in] cb notification callback to be invoked 
 * \param[in] notification mask with requested events 
 * \note Set callback to NULL to disable callback 
 * \note please see macNotifications for reference
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */   
MAC_API macError sysNotificationCallback( macNotificationCallback cb = NULL, 
        macNotifications notification = NOTIFICATION_ALL );

/** Show MAC desktop
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                
 */
MAC_API macError sysShowDesktop();

/** Show MAC Control Panel
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                
 */  
MAC_API macError sysShowControlPanel();

/** Show MAC Power Control Panel
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */ 
MAC_API macError sysShowPowerControlPanel();

/** Show previous visible application
 * \note Will show previously visible application.
 * \note will show desktop if visibility stack is empty
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */  
MAC_API macError sysShowPrevApp();

/** Launch an application by application id
 * \param[in] appid  application id from manifest file
 * \param[in] args   arguments to be passed to application
 * \param[in] envs   environments to be passed to application
 * \param[in] params mask with launching parameters from #macLaunchParams enum
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */
MAC_API macError sysLaunchAppid( const std::string & appid, 
        const std::vector< std::string > & args, 
        const std::map< std::string,std::string > & envs, int params = 0 );

/** Launch an application by application id
 * \param[in] appid  application id from manifest file
 * \param[in] args   arguments to be passed to application
 * \param[in] params mas with launching parameters from #macLaunchParams enum
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */ 
MAC_API macError sysLaunchAppid( const std::string & appid, 
        const std::vector<std::string> & args, 
        int params );

/** Launch an application by application id
 * \param[in] appid  application id from manifest file
 * \param[in] args   arguments to be passed to application
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */
MAC_API macError sysLaunchAppid( const std::string & appid, 
        const std::vector<std::string> & args);

/** Launch an application by application id
 * \param[in] appid  application id from manifest file
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                 
 */ 
MAC_API macError sysLaunchAppid( const std::string & appid );

/** Launch an application by application name
 * \param[in] appname application name from manifest file
 * \param[in] args   arguments to be passed to application
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                  
*/
MAC_API macError sysLaunchApp( const std::string & appname, 
        const std::vector<std::string> & args );

/** Launch an application by application name
 * \param[in] appname application name from manifest file
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_IPC communication error                  
*/  
MAC_API macError sysLaunchApp( const std::string & appname );
 
/** Initialize relaunch process
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                   
 * \note after invoking this API, MAC will first stop all applications. 
 * After that all applications with autolaunch sections in manifest files, will be launched.
 * \note this API should bring terminal to clean state (same as after boot)
 */ 
MAC_API macError sysRelaunchApps(); 
 
/** Check if application is currently running
 * \param[in]  appid application id from manifest file 
 * \param[out] running running/not running
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                   
 * \return #MAC_ERR_NO_APPID unknown application id           
*/
MAC_API macError sysIsRunning( const std::string & appid, bool & running );

/** Shows progress screen during application startup
 * \param[in] info     message to be reported on screen
 * \param[in] percent  boot progress (0-100%)
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                   
 * \return #MAC_ERR_VISIBLE application is not currently visible
 */    
MAC_API macError sysShowProgress( const std::string & info , size_t percent );

/** This API is used to register you application in MAC.
 * After invokig this API, MAC will create new GUI region for application and set correct environment variables.
 * \param[in]  appid application id from manifest file 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \note This API is needed for specific usecase. Not intended for general use.
 * \note This API is usefull when application was launched by SI, but it still wants to interract with MAC.
 * \note Application needs to call sysUnSetAppid before exiting, to avoid resource leek.
 * \note Be sure to invoke this API before using any ADK-GUI API calls.
 */       
MAC_API macError sysSetAppid( const std::string & appid );

/** This API will unregister your application from MAC. 
 * MAC will destroy used GUI region and free all resources.
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \note This API is needed for specific usecase. Not intended for general use.
 * \note Be sure, to invoke this API before your application exits.
 */       
MAC_API macError sysUnSetAppid();

/** Change layout 
 * \param[in] layoutname layout name from layout.ini file 
 * \param[in] statusbar  layout name with -sb suffix
 * \param[in] keyboard   layout name with -kb suffix 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
*/
MAC_API macError sysChangeLayout( const std::string & layoutname, bool statusbar = true, bool keyboard = false );

/** Change layout 
 * \param[in] layoutname layout name from layout.ini file 
 * \param[in] statusbar  layout name with -sb suffix
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
*/  
MAC_API macError sysChangeLayout( const std::string & layoutname, bool statusbar ); 

/** Assign application to certain region in layout
 * \param[in] appid  application id from manifest file 
 * \param[in] region region name from layout file
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_NO_APPID unknown application id           
 * \note This API is needed for split screen mode
*/ 
MAC_API macError sysAssignToLayout( const std::string & appid, const std::string & region);

/** Enter/leave full screen mode
 * \param[in] enable
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_VISIBLE application is not currently visible
 * \note Will switch layout to full screen one
*/
MAC_API macError sysFullscreenMode( bool enable );

/** Set statusbar state
 * \param[in] visible
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                   
 * \note This configuration is stored per application
*/
MAC_API macError sysStatusbar( bool visible );

/** Get current statusbar state
 * \param[out] visible
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                   
 * \note This configuration is stored per application 
*/
MAC_API macError sysStatusbarState( bool & visible );

/** Sets value map to statusbar.html
 * \param[in] values value map, which will be passed to statusbar.html
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                    
 * \note This API can be used to add extra icons to statusbar.
 * \note This API is useful in case custom MAC resources are used.
*/ 
MAC_API macError sysSetStatusbarValues( const std::map<std::string,std::string> & values );

/** Obtain value map from statusbar.html
 * \param[out] values value map from statusbar
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                    
 * \note This API can be used to add extra icons to statusbar.
 * \note This API is useful in case custom MAC resources are used.
*/  
MAC_API macError sysGetStatusbarValues( std::map<std::string,std::string> & values );

/** Enforce MAC statusbar refresh
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                    
*/ 
MAC_API macError sysStatusbarRefresh();

/** Show/hide virtual keyboard
 * \param[in] mode keyboard mode from #macKeyboard enum
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                    
 * \return #MAC_ERR_LOCKED layout is currently locked             
 * \return #MAC_ERR_VISIBLE application is not currently visible  
 * \note Please see #macKeyboard enum for reference.
 * \note This configuration is stored per application.
*/ 
MAC_API macError sysVirtualKeyboard( macKeyboard mode );

/** Lock/unlock the layout.
 * \param[in] enable enable/disable layout lock 
 * \note Prevents other application from changing the layout 
 * (e.g. requesting full screen or virtual keyboard).
 *  Prevents from showing the application desktop.
 *  Typically this is being called when applications do user 
 *  processing and don't want the layout to be changed.
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                    
 * \return #MAC_ERR_LOCKED layout is already locked             
 * \return #MAC_ERR_VISIBLE application is not currently visible (in this case it can't lock layout)   
 */
MAC_API macError sysLockLayout( bool enable );

/** Returns appid of currently visible application
 * \param[out] appid 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error
 */
MAC_API macError sysInForeground( std::string & appid );

/** Allows to check if application is currently visible
 * \param[out] visible
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error
 */
MAC_API macError sysInForeground( bool & visible );

/** Bring application to foreground.
 * \param[in] appid application id from manifest file 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_LOCKED layout is already locked
 * \return #MAC_ERR_IPC communication error
 * \return #MAC_ERR_SERVICE application is service (no GUI)
 */
MAC_API macError sysToForeground( const std::string & appid );
MAC_API macError sysToForground(  const std::string & appid );

/** Stops application by appid
 * \param[in] appid 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                      
 * \return #MAC_ERR_APP_NOT_RUNNING application is not running
 */  
MAC_API macError sysStopAppid( const std::string & appid );

/** Stops application by application name
 * \param[in] appname 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                      
 * \return #MAC_ERR_APP_NOT_RUNNING application is not running 
 */  
MAC_API macError sysStopAppname( const std::string & appname );

/** Stop all user applications
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                       
 * \note All applications started by MAC (users usr1-usr15) will be stopped.
 */ 
MAC_API macError sysStopAllUserApps(); 

/** Stop all user applications, except for calling application
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                       
 * \note All applications started by MAC (users usr1-usr15) will be stopped.
 * \note Calling application will remain active.
 * \note This API is useful if you want to avoid any kind of interference from other applications.
 */ 
MAC_API macError sysStopOtherApps(); 

/** Get information about available application
 * \param[out] applist  
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error
 * \note payload example: 
 * \code 
 * {"appList":[ 
 *  {"Active":true,"AppId":"ccp","AppLabel":"Com Control" ,"AppName":"ccp","AppVersion":"1.8.0-pre-103","CPApp":false,"Desktop":{"AppLabel":"Com Control","Type":"app"},"Running":false,"Type":"app"}, 
 *  {"Active":true,"AppId":"cpdownloader","AppLabel":"cpdownloader","AppName":"cpdownloader","AppVersion":"2.31.0","CPApp":false,"Desktop":{"AppLabel":"cpdownloader","Type":"app"},"Running":false,"Type":"app"}, 
 *  {"Active":true,"AppId":"demo1","AppLabel":"demo","AppName":"demo1","AppVersion":"3.59.0","Autolaunch":{"Type":"app"},"CPApp":false,"Desktop":{"AppLabel":"demo","Type":"app"},"Running":true ,"Type":"app"}, 
 *  {"Active":true,"AppId":"lcp","AppLabel":"Log Control" ,"AppName":"lcp","AppVersion":"2.16.7","CPApp":false,"Desktop":{"AppLabel":"Log Control","Type":"app"} ,"Running":false,"Type":"app"} ],"result":"OK","status":0}"
 * \endcode
 */ 
MAC_API macError sysGetAppList( vfiipc::JSObject & applist );

/** Request MAC to rescan all folders for new manifest files
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                         
 * \return #MAC_ERR_CYCLE manifest dependency cycle is detected
 * \return #MAC_ERR_DEPENDS manifests are missing dependencies
 * \note Typically this is done after a SW update and new application installation.
 */
MAC_API macError sysScanManifests();

/** Set environment values for CPR applications
 * \param[in] envs environment values  
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK success
 * \return #MAC_ERR_IPC communication error                          
 * \note Values will be accessible to CP application via ARGV[“cp_envData”].
 * \note Values are available only to CPR applications.
 */
MAC_API macError sysSetCPEnvData( const vfiipc::JSObject & envs );

/** Install Commerce Platform application
 * \param[in] path path to CPA package which should be installed 
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK     success
 * \return #MAC_ERR_IPC    communication error
 * \return #MAC_ERR_PARAMETER wrong parameter passed
 * \return #MAC_ERR_ACCESS installation package is not accessible
 * \return #MAC_ERR_NOT_CP_APP no CP application to install
 * \note Enable CPR logging channel to see installation log.
 */
MAC_API macError sysInstallCPApp( const std::string & path );

/** MAC will generate desktop with icons for every CP application, which has matching trigger.
 * \param[in] trigger_id trigger identifier
 * \param[in] param parameters will be passed to CP application in cp_notifyParam field
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK     success
 * \return #MAC_ERR_IPC    communication error
 * \return #MAC_ERR_PARAMETER wrong parameter passed
 * \return #MAC_ERR_NOT_CP_APP no matching CP application
 * \note This API is asynchronous.
 */
MAC_API macError sysLaunchCPAppFromDesktop( const std::string & trigger_id, const std::vector<std::string> & param);

/** MAC will generate desktop with icons for every CP application, which has matching trigger.
 * \param[in] trigger_id trigger identifier
 * \param[in] param parameters will be passed to CP application in cp_notifyParam field
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK     success
 * \return #MAC_ERR_IPC    communication error
 * \return #MAC_ERR_PARAMETER wrong parameter passed
 * \return #MAC_ERR_NOT_CP_APP no matching CP application 
 * \note This API is asynchronous.
 */
MAC_API macError sysLaunchCPAppFromDesktop( const std::string & trigger_id, const vfiipc::JSObject & param );

/** MAC will generate desktop with icons for every CP application, which has matching trigger.
 * \param[in] trigger_id trigger identifier
 * \param[in] param parameters will be passed to CP application in cp_notifyParam field
 * \param[in] flags flags passed to CP app in cp_notifyFlags field
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK application was launched successfully
 * \return #MAC_ERR_CANCELED desktop was canceled by user
 * \return #MAC_ERR_IPC IPC error
 * \note This API is synchronous. It will exit only after application is launched or desktop is canceled.
 */
MAC_API macError sysLaunchCPAppFromDesktopSync( const std::string & trigger_id, 
        const vfiipc::JSObject & param, int flags = 0 );

/** Will execute all CP applications, which are registered for certain trigger identifier
 * \param[in] appid application id from manifest file (can be "*" for broadcast mode or "1" for unicast mode)
 * \param[in] trigger_id trigger identifier
 * \param[in] param parameters will be passed to CP application in cp_notifyParam field
 * \param[in] flags flags passed to CP app in cp_notifyFlags field
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK     success
 * \return #MAC_ERR_IPC    communication error
 * \return #MAC_ERR_PARAMETER wrong parameter passed
 * \return #MAC_ERR_NOT_CP_APP no CP application to launch 
 * \return #MAC_ERR_ACTIVE CP application is not activated 
 * \note if appid is equal to "*", this will invoke broadcast mode.
 * In this case all CP applications with matching trigger identifier will be launched one by one.
 * Launching sequence happens according to CP application priorities.
 * \note if appid is equal to "1", this will invoke unicast mode.
 * In this case only one CP applications with highest priority and matching trigger identifier will be launched.
 * \note in all other cases only one application with matching appid will be launched.
 */
MAC_API macError sysLaunchCPAppByTrigger(
        const std::string & appid,
        const std::string & trigger_id, 
        const vfiipc::JSObject & param,
        unsigned int flags);

/** Stop all Commerce Platform applications
 * \return Returns an #macError value enum 
 * \return #MAC_ERR_OK     success
 * \return #MAC_ERR_IPC    communication error 
 */ 
MAC_API macError sysStopAllCPApps();

/** returns a zero-terminated string with version and build information of MAC
 *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
 * \return version string */
MAC_API const char * sysMac_GetVersion();

/** returns a zero-terminated string with version and build information of MAC library
 *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
 * \return version string */    
MAC_API const char * sysLibMac_GetVersion();

#ifndef DOXYGEN
} // namespace vfimac
#endif

/** @}*/

