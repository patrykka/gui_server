#pragma once
 
/** \file netloader.h 
 * \defgroup netloader
 * @{
 */

#include "dllspec.h"
#include <map>
#include <string>

#ifndef DOXYGEN
/** Verifone netloader (vfinetloader) namespace */
namespace vfinetloader 
{
#endif

enum sysStatus 
{
    // Default messages:
    SYS_ERR = 0,                    /**< Error return (errno will be non-zero) msg is empty */
    SYS_MSG = 1,                    /**< Status message available. msg member is not empty */
    // Type 1 messages: 
    SYS_DNLD_FILE = 2,              /**< 'msg' member will return the filename of downloading file */
    SYS_DNLD_DONE = 3,              /**< 'msg' member returns "DNLD DONE" */
    SYS_DNLD_ERR = 4,               /**< 'msg' member returns "DNLD ERROR" */
    SYS_DNLD_INSTALL = 5,           /**< 'msg' member will return the filename of the file being installed*/
    SYS_DNLD_INSTALL_DONE = 6,      /**< 'msg' member returns "INSTL DONE" */
    SYS_DNLD_INSTALL_FAIL = 7,      /**< 'msg' member returns "INSTL FAIL" */
    SYS_REBOOT = 8,                 /**< 'msg' member returns "REBOOT" */
    SYS_FILE_FINISH = 9,            /**< 'msg' member will return the filename of the finished file */
    SYS_REBOOT_REQUIRED = 10,       /**< 'msg' member returns "REBOOT REQUIRED" */
    SYS_BASE_INSTALL_FAIL = 11,     /**< 'msg' member returns "BASE INSTL FAIL" */
    // Type 2 messages: 
    SYS_NETLOADER_READY = 100,   /**< 'msg' member returns "", netloader ready to connect on socket I/O */
    SYS_NETLOADER_EXIT = 200,    /**< 'msg' member returns "", netloader stopping and closing socket I/O */
};

enum sysMsgMode
{
    SYS_EXT_MSG_DISABLED = 0, /**< Disable extended messages */
    SYS_EXT_MSG          = 1, /**< Default messages */
    SYS_EXT_MSG_1_2      = 2, /**< Enable extended type 1 and 2 messages */
    SYS_EXT_MSG_1        = 9, /**< Enable extended type 1 and don't auto-reboot after install */
};

enum sysReadMode
{
    SYS_BLOCKING         = 0, /**< Blocking */
    SYS_NON_BLOCKING     = 1, /**< Non-blocking */
    SYS_LAST             = 2, /**< Return last event */
    SYS_CANCEL           = 9, /**< Cancel */
};

/** Start netloader (listen on socket for download request)
 */  
SYS_INFO_API int sysNetloaderStart();

/** Starts netloader on terminal base 
 */  
SYS_INFO_API int sysNetloaderStartBase();

/** Starts netloader on terminal base 
 */  
SYS_INFO_API int sysNetloaderStopBase();
 
/** Stop netloader (stop listening on socket for download request)
 */
SYS_INFO_API int sysNetloaderStop();

/** Get version of netloader service
 */
SYS_INFO_API int sysNetloaderVersion(std::string & version);

/** Set extended messages
 *
 * @param[in] type  flags from sysMsgMode enum
 */
SYS_INFO_API int sysNetloaderSetMsgMode(int type);

/** Read a netloader event.
 *
 * @param[in] flags  flags from sysReadMode enum
 * \return value from sysStatus enum 
 */  
SYS_INFO_API int sysNetloaderGetMsg(std::string & msg, int flags);
 
#ifndef DOXYGEN
} // namespace vfinetloader
#endif
/** @}*/

