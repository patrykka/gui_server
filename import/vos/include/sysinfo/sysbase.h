#pragma once

#include "dllspec.h"

/** \file dock.h 
 * \defgroup syspm Low level system power management functions. Please use vfisyspm.h instead.
 * @{
 */

#include <map>
#include <string>

#ifndef DOXYGEN
/** Verifone system base (vfisysbase) namespace */
namespace vfisysbase 
{
#endif

/** Transfer "dl" file to base
 * \return error code
 */
SYS_INFO_API int sysTransferToBase(const std::string & ip_addr, const std::string & filename);

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

