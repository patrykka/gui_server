#pragma once

#include "dllspec.h"

/** \file sysbeep.h 
 * \defgroup sysbeep System beep functions
 * @{
 */

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo {
#endif

/** Plays a 50 ms tone at 1245 Hz (normal beep tone, same as normal_tone() on Verix)
 * \param[in] volume volume in percent, value range: 0 (low) ... 50 (middle) ... 100 (high)
 * \return SYS_ERR_OK for success, else error code
 * \note Volume \a volume is not supported on Verix eVo. Beep is played with a system specified volume.
 */
SYS_INFO_API int sysBeepNormal(int volume);

/** Plays a 100 ms tone at 880 Hz (error beep tone, same as error_tone() on Verix)
 * \param[in] volume volume in percent, value range: 0 (low) ... 50 (middle) ... 100 (high)
 * \return SYS_ERR_OK for success, else error code
 * \note Volume \a volume is not supported on Verix eVo. Beep is played with a system specified volume.
 */
SYS_INFO_API int sysBeepError(int volume);

/** Plays an arbitrary tone with arbitrary duration and volume.
 * \param[in] note value specifying a note listed in the table below
 * \param[in] duration duration of the beep in milliseconds. value range: 0 ... 5000 (max)
 * \param[in] volume volume in percent, value range: 0 (low) ... 50 (middle) ... 100 (high)
 * \return SYS_ERR_OK for success, else error code
 * \note \li Verix eVo: Volume \a volume is not supported on Verix eVo. Beep is played with a system specified volume.
 *
 * &nbsp;<br>
 * Supported values for parameter \a note
 *   Value |  Note | Frequency   
 * --------|-------|------------
 *     0   |   A   |  55.00
 *     1   |   A#  |  58.27
 *     2   |   B   |  61.74
 *     3   |   C   |  65.41
 *     4   |   C#  |  69.30
 *     5   |   D   |  73.42
 *     6   |   D#  |  77.78
 *     7   |   E   |  82.41
 *     8   |   F   |  87.31
 *     9   |   F#  |  92.50
 *    10   |   G   |  98.00
 *    11   |   G#  |  103.83
 *    12   |   A   |  110.00
 *    13   |   A#  |  116.54
 *    14   |   B   |  123.47
 *    15   |   C   |  130.81
 *    16   |   C#  |  138.59
 *    17   |   D   |  146.83
 *    18   |   D#  |  155.56
 *    19   |   E   |  164.81
 *    20   |   F   |  174.61
 *    21   |   F#  |  185.00
 *    22   |   G   |  196.00
 *    23   |   G#  |  207.65
 *    24   |   A   |  220.00
 *    25   |   A#  |  233.08
 *    26   |   B   |  246.94
 *    27   |   C   |  261.63
 *    28   |   C#  |  277.18
 *    29   |   D   |  293.66
 *    30   |   D#  |  311.13
 *    31   |   E   |  329.63
 *    32   |   F   |  349.23
 *    33   |   F#  |  369.99
 *    34   |   G   |  392.00
 *    35   |   G#  |  415.30
 *    36   |   A   |  440.00
 *    37   |   A#  |  466.16
 *    38   |   B   |  493.88
 *    39   |   C   |  523.25
 *    40   |   C#  |  554.37
 *    41   |   D   |  587.33
 *    42   |   D#  |  622.25
 *    43   |   E   |  659.26
 *    44   |   F   |  698.46
 *    45   |   F#  |  739.99
 *    46   |   G   |  783.99
 *    47   |   G#  |  830.61
 *    48   |   A   |  880 
 *    49   |   A#  |  932 
 *    50   |   B   |  988 
 *    51   |   C   |  1047
 *    52   |   C#  |  1109
 *    53   |   D   |  1175
 *    54   |   D#  |  1245
 *    55   |   E   |  1319
 *    56   |   F   |  1397
 *    57   |   F#  |  1480
 *    58   |   G   |  1568
 *    59   |   G#  |  1661
 *    60   |   A   |  1760
 *    61   |   A#  |  1865
 *    62   |   B   |  1976
 *    63   |   C   |  2093
 *    64   |   C#  |  2217
 *    65   |   D   |  2349 
 *    66   |   D#  |  2489 
 *    67   |   E   |  2637 
 *    68   |   F   |  2794 
 *    69   |   F#  |  2960 
 *
 *    70   |   G   |  3136 
 *    71   |   G#  |  3322 
 *    72   |   A   |  3520 
 *    73   |   A#  |  3729 
 *    74   |   B   |  3951 
 *    75   |   C   |  4186 
 *    76   |   C#  |  4435 
 *    77   |   D   |  4699 
 *    78   |   D#  |  4978 
 *    79   |   E   |  5274 
 *    80   |   F   |  5588 
 *    81   |   F#  |  5920 
 *    82   |   G   |  6272 
 *    83   |   G#  |  6645 
 *    84   |   A   |  7040 
 *    85   |   A#  |  7459 
 *    86   |   B   |  7902 
 *    87   |   C   |  8372 
 *    88   |   C#  |  8870 
 *    89   |   D   |  9397 
 *    90   |   D#  |  9956 
 *    91   |   E   |  10548
 *    92   |   F   |  11175
 *    93   |   F#  |  11840
 *    94   |   G   |  12544
 *    95   |   G#  |  13290
 *
 */
SYS_INFO_API int sysPlaySound(int note, int duration, int volume);

/** Plays an arbitrary frequency with arbitrary duration and volume.
 * \param[in] frequency in Hz, value range: 400 (min) ... 3000(max)
 * \param[in] duration duration of the beep in milliseconds. value range: 0 ... 5000 (max)
 * \param[in] volume volume in percent, value range: 0 (low) ... 50 (middle) ... 100 (high)
 * \return SYS_ERR_OK for success, else error code
 * \note \li Verix eVo: Volume \a volume is not supported on Verix eVo. Beep is played with a system specified volume.
 */
SYS_INFO_API int sysPlaySoundFreq(int frequency, int duration, int volume);

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

