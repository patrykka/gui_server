#pragma once

/** \file syserror.h 
 * \defgroup syserro System property functions
 * @{
 */

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo 
{
#endif

/** error codes returned by the functions */
enum sysError
{
    SYS_ERR_OK                 =  0,         /**< no error */
    SYS_ERR_PROP_UNSUPPORTED   = -1,         /**< the property is not supported by the platform */
    SYS_ERR_PROP_READ_ONLY     = -2,         /**< the property value cannot be set */
    SYS_ERR_PROP_WRITE_ONLY    = -3,         /**< the property value cannot be obtained */
    SYS_ERR_PARAMETER          = -4,         /**< wrong parameter */
    SYS_ERR_PERMISSION         = -5,         /**< insufficient permissions */
    SYS_ERR_REGION             = -6,         /**< wrong region ID */
    SYS_ERR_FAIL               = -7,         /**< generic error */
    SYS_ERR_UNSUPPORTED        = -8,         /**< the function requested is not supported on the current platform/device/... */
    SYS_ERR_ACTIVE_TIMEZONE    = -9,         /**< the time zone is active already */
    SYS_ERR_INSTALL_LIMIT      = -10,        /**< No more bundles can be installed. Reboot needed */
    SYS_ERR_REBOOT_NEEDED      = -11,        /**< Reboot needed to install bundle */
    SYS_ERR_FILE_DOES_NOT_EXIST= -12,        /**< File doesn't exist */
    SYS_ERR_BUNDLE_VERSION     = -13,        /**< Higher bundle version already installed */
    SYS_ERR_EXISTS             = -14,        /**< the property does not exist */ 
    SYS_ERR_ACCESS             = -15,        /**< Not enough privileges */
    SYS_ERR_NOT_DOCKED         = -16,        /**< Terminal isn't docked */
    SYS_ERR_KEY_MISSING        = -17,        /**< Keys are missing */
    SYS_ERR_EXPECTED_KEY       = -18,        /**< No information on expected keys */
    SYS_ERR_CONFIGURATIION     = -19,        /**< Not configured properly */ 
    SYS_ERR_PATHNAME_TOO_LONG  = -20,        /**< Pathname is too long */
    SYS_ERR_GET_CWD            = -21,        /**< Not possible to get pathname of the curent directory */
    SYS_ERR_IPC                = -22,        /**< IPC error */
    SYS_ERR_RUNNING            = -23,        /**< Daemon is not running */
};

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

