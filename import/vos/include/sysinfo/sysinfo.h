#pragma once

/** \file sysinfo.h 
 * \defgroup sysinfo System property functions
 * @{
 */

#include <string>
#include "dllspec.h"
#include "syserror.h"

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo 
{
#endif


/** Display contrast constants for SYS_PROP_DISP_CONTRAST property. */
enum displayContrastConstants
{
    ContrastMin     = 1,  /**< Minimal display contrast */
    ContrastDefault = 8,  /**< Default display contrast */
    ContrastMax     = 15, /**< Maximal display contrast */
    ContrastUp      = 64, /**< Increase display contrast */
    ContrastDown    = 65  /**< Decrease display contrast */
};

/** Battery type used for SYS_PROP_BATTERY_TECHNOLOGY property. */
enum batteryTechnology
{
    BattTUnknown = 0,     /**< Battery type unknown */
    BattTNiMH    = 1,     /**< Battery type NiMH */
    BattTLiIon   = 2,     /**< Battery type LiIon */
    BattTLiPoly  = 3,     /**< Battery type LiPoly */
    BattTLiFe    = 4,     /**< Battery type LiFe */
    BattTNiCd    = 5,     /**< Battery type NiCd */
    BattTLiMn    = 6      /**< Battery type LiMn */
};

/** Device mode used in SYS_PROP_DEVICE_MODE property. */
enum deviceMode
{   
    DEVICE_MANUF  = 0,    /**< Device is in manufacturing mode */
    DEVICE_PROD   = 1,    /**< Device is in production mode */
    DEVICE_OS_DEV = 2,    /**< Device is in OS development mode */
    DEVICE_CP_DEV = 3     /**< Device is in test mode (CP development mode) */
};

/** Battery charge mode used in SYS_PROP_BATTERY_CHARGE_MODE property. */
enum batteryChargeMode
{
    UNRESTRICTED_CHARGE_MODE = 1, /**< Unrestricted charge mode */
    CAR_CHARGER_MODE         = 2, /**< Car charger mode */
};

/** USB port modes. */
enum sysUSBmode
{
    SYS_USB_UNKNOWN = 0,  /**< Unknown USB mode */
    SYS_USB_DEFAULT = 1,  /**< Default USB mode */
    SYS_USB_HOST    = 2,  /**< Host USB mode */
    SYS_USB_DEVICE  = 3,  /**< Device USB mode */
};

/** Device mode used in SYS_PROP_DEVICE_TYPE property. */
enum deviceType
{
    DEVICE_NON_PORTABLE = 0, /**< Non portable device (non dockable unit) */
    DEVICE_HANDSET      = 1, /**< Device is portable (dockable unit) */ 
    DEVICE_BASE         = 2, /**< Device is base */  
    DEVICE_MOBILE       = 3  /**< Mobile device (non dockable unit) */   
};

/** Battery type used in SYS_PROP_BATTERY_TYPE property. */
enum batteryType 
{
    BATTERY_TYPE_SMART  = 1, /**< Smart battery */ 
    BATTERY_TYPE_SIMPLE = 2  /**< Simple battery (No fuel gauge) */ 
};
 
/** Integer type system properties */
enum SYSPropertyInt 
{
    SYS_PROP_BATTERY_AVAILABLE=0,            /**< read-only,  Battery exists: 1: battery available, else 0*/
    SYS_PROP_BATTERY_STATUS_OK=1,            /**< read-only,  Battery status: 1: ok, else 0 */
    SYS_PROP_BATTERY_DEVICE=2,               /**< read-only,  Device can be battery powered: 1: Portable device, else 0 */
    SYS_PROP_BATTERY_VOLTAGE=3,              /**< read-only,  Battery voltage in units of mV */
    SYS_PROP_BATTERY_CAPACITY=4,             /**< read-only,  Battery capacity in mAh */
    SYS_PROP_BATTERY_CHARGE_LEVEL=5,         /**< read-only,  Battery charge level in percent (0-100)*/
    SYS_PROP_BATTERY_CHARGING=10,            /**< read-only,  Battery charging status: 1: charging, else 0 */
    SYS_PROP_BATTERY_FULL=11,                /**< read-only,  Battery full: 1: battery fully charged, else 0 */ 
    SYS_PROP_BATTERY_TEMP=13,                /**< read-only,  Battery temperature in degree Celsius */
    SYS_PROP_BATTERY_CURRENT=14,             /**< read-only,  Battery current in in units of 0.01A */
    SYS_PROP_BATTERY_CRITICAL=18,            /**< read-only,  Battery critical: 1: battery level is critical, else 0 */ 
    SYS_PROP_BATTERY_LOW=19,                 /**< read-only,  Battery low: 1: battery level is low, else 0 */ 
    SYS_PROP_BATTERY_NORMAL=20,              /**< read-only,  Battery normal: 1: battery level is normal, else 0 */ 
    SYS_PROP_BATTERY_TECHNOLOGY=700,         /**< read-only,  Possible values are enumerated in batteryTechnology enum */
    SYS_PROP_BATTERY_FCC=701,                /**< read-only,  Percent Battery FCC (Full Charge Capacity) in mAh */
    SYS_PROP_BATTERY_SOH=702,                /**< read-only,  Percent Battery SOH (State Of Health) in % */ 
    SYS_PROP_BATTERY_CHARGE_IN_SLEEP=21,     /**< read-only,  Battery can be charged when unit is in power saving state, 1: battery can be charged, 0: can't be charged */
    SYS_PROP_BATTERY_CALIBRATION=705,        /**< read-only,  Battery calibration status 1:Calibrated 0:not calibrated */
    SYS_PROP_BATTERY_TYPE=706,               /**< read-only,  Battery type, batteryType enum value */ 
    SYS_PROP_CHARGE_CYCLE_COUNT=704,         /**< read-only,  Battery charge cycle count */ 
    SYS_PROP_BATTERY_CHARGE_MODE=707,        /**< read/write, Battery charge mode, batteryChargeMode enum value */ 
    SYS_PROP_UX100_AVAILABLE=22,             /**< read-only,  presence of UX100 device */
    SYS_PROP_UX400_AVAILABLE=23,             /**< read-only,  presence of UX400 device */
    SYS_PROP_UX100_OPERATIONAL_MODE=24,      /**< read-only,  Get PINPAD Operational Mode */
    SYS_PROP_UX115_UNIT=26,                  /**< read-only,  If it is UX115 unit, 1: Ux115 unit, else 0 */
    SYS_PROP_UX_UNIT=25,                     /**< read-only,  If it is UX unit, 1: Ux unit, else 0 */
    SYS_PROP_INTERNAL_BATTERY_STATUS=7,      /**< read-only,  Internal battery battery status, 1: ok, else 0 */
    SYS_PROP_INTERNAL_BATTERY_VOLTAGE=8,     /**< read-only,  Internal battery voltage in units of mV, possible values: 3000 = ideal, 2600 = good, 2300 = low*/
    SYS_PROP_DOCKING_STATUS=6,               /**< read-only,  Docking status, 1:docked, 0:undocked */
    SYS_PROP_DOCK_SUPPORT=15,                /**< read-only,  Docking supports dock station, 1: dock station supported, 0: not supported */
    SYS_PROP_USB1_MODE=16,                   /**< read/write, USB1 mode, sysUSBmode enum value */
    SYS_PROP_USB2_MODE=17,                   /**< read/write, USB2 mode, sysUSBmode enum value */
    SYS_PROP_USB1_CONFIGURABLE=27,           /**< read-only,  USB1 configurable, 1: configurable, else 0 */
    SYS_PROP_USB2_CONFIGURABLE=28,           /**< read-only,  USB2 configurable, 1: configurable, else 0 */
    SYS_PROP_USB1_DEFAULT=29,                /**< read-only,  USB1 default mode: sysUSBmode enum value */   
    SYS_PROP_USB2_DEFAULT=30,                /**< read-only,  USB2 default mode: sysUSBmode enum value */   
    SYS_PROP_USB1_CHARGE=31,                 /**< read-only,  USB1 charge port, 1: charge port, else 0 */ 
    SYS_PROP_USB2_CHARGE=32,                 /**< read-only,  USB2 charge port, 1: charge port, else 0 */ 
    SYS_PROP_PRINTER_AVAILABLE=9,            /**< read-only,  Printer exists: 1: printer available, else 0*/
    SYS_PROP_POWER_STATUS=12,                /**< read-only,  Power status, 1: unit is line powered, else 0 */
    SYS_PROP_POWER_ON=39,                    /**< read-only,  1: Unit can be powered on by green key, else 0 */
    SYS_PROP_CHARGE_ON_SAVING=703,           /**< read-only,  Unit can be charged during power saving state (during sleep) */
    SYS_PROP_KEYBOARD_BEEP=100,              /**< write-only, Keyboard beep on key press, 1: enabled, 0: disabled */
    SYS_PROP_KEYB_BACKLIGHT=101,             /**< read/write, Keyboard backlight */
    SYS_PROP_KEYBOARD_HW=102,                /**< read-only,  HW keyboard exists, 1: exists, 0: touch only unit */
    SYS_PROP_KEYBOARD_SIZE=103,              /**< read-only,  Number of keys on HW keyboard */
    SYS_PROP_DISP_CONTRAST=200,              /**< read/write, Display contrast  */
    SYS_PROP_DISP_BACKLIGHT=201,             /**< read/write, Display backlight */
    SYS_PROP_DISP=202,                       /**< write-only, Turn display on/off, 1: turn on, 0: turn off */
    SYS_PROP_DISP_HW=203,                    /**< read-only,  Display exists, 1: exists, 0: no display available */
    SYS_PROP_DISP_COLOR=205,                 /**< read-only,  Color display exists, 1: color display, 0: monochrome display */
    SYS_PROP_DISP_HEIGHT=206,                /**< read-only,  Color display height in pixels */
    SYS_PROP_DISP_WIDTH=207,                 /**< read-only,  Color display width in pixels */
    SYS_PROP_DISP_TOUCH=208,                 /**< read-only,  Is touchscreen available, 1: touchscreen, else 0 */
    SYS_PROP_ANDROID=204,                    /**< read-only,  Android side available 1: available, 0: non Android unit */
    SYS_PROP_HW_RAM_SIZE=300,                /**< read-only,  RAM size KB*/
    SYS_PROP_HW_FLASH_SIZE=301,              /**< read-only,  Flash size KB*/
    SYS_PROP_HW_RAM_USED=302,                /**< read-only,  Used ram size KB */
    SYS_PROP_HW_FLASH_USED=303,              /**< read-only,  Used flash size KB */
    SYS_PROP_HW_TOUCH_GROUNDED=306,          /**< read/write, Touch screen grounded, 1: grounded, 0: not grounded */
    SYS_PROP_LED=304,                        /**< read-only,  Returns mask with available LEDs. Please see enum LEDs from leds.h */
    SYS_PROP_PCI=305,                        /**< read-only,  Returns terminal PCI version */
    SYS_PROP_VOLUME=400,                     /**< read/write, speaker volume (0-100) (Engage only) */
    SYS_PROP_SPEAKER_AVAILABLE=401,          /**< read-only,  speaker exists, 1: exists, 0: no speaker (Engage only) */
    SYS_PROP_BUZZER_AVAILABLE=402,           /**< read-only,  buzzer exists, 1: exists, 0: no buzzer */
    SYS_PROP_ADE_STATUS=500,                 /**< read-only,  ADE status 1: active, 0: inactive */
    SYS_PROP_CERT_SPONSOR_PROD=501,          /**< read-only,  sponsor certificate 1: prod, 0: test */
    SYS_PROP_ATTACK_STATUS=502,              /**< read-only,  Tamper status 1: device tampered, else 0 */
    SYS_PROP_VCL_ENCRYPTION_STATUS=503,      /**< read-only,  VCL encryption status 1: enabled, else 0 */
    SYS_PROP_VOLTAGE_ENCRYPTION_STATUS=504,  /**< read-only,  VOLTAGE encryption status 1: enabled, else 0 */
    SYS_PROP_POSEIDON_ENCRYPTION_STATUS=505, /**< read-only,  POSEIDON encryption status 1: enabled, else 0 */
    SYS_PROP_NAVIGATOR_STATUS=506,           /**< read-only,  NAVIGATOR status 1: enabled, else 0 */
    SYS_PROP_SERVICE_SWITCH_STATUS=507,      /**< read-only,  Service switch status: 1: service switch triggered, else 0 */
    SYS_PROP_DEVICE_MODE=508,                /**< read-only,  Device mode from deviceMode enum */
    SYS_PROP_SYSMODE_BUTTON=509,             /**< write-only, Enable/disable sysmode button, 1: enable, 0: disable */
    SYS_PROP_DEVICE_TYPE=510,                /**< read-only,  Device type from deviceType enum */
    SYS_PROP_VATS=512,                       /**< read-only,  VATS, 1: VATS OS 0: else */
    SYS_PROP_UX100_ANTI_REMOVAL_SWITCH=511,  /**< read-only,  Status of the anti-removal switch of the ux100. 1: triggered, 0: armed */
    SYS_PROP_UPTIME=600,                     /**< read-only,  System uptime in seconds */
    SYS_PROP_BARCODE_SCANNER=33,             /**< read-only,  Barcode scanner exists: 1: barcode scanner available, else 0*/
    SYS_PROP_CAMERA=34,                      /**< read-only,  Camera exists: 1: camera available, else 0*/
    SYS_PROP_SWITCH_THRESHOLD_VOLTAGE=35,    /**< read/write, Threshold voltage for successful switchback to HOST mode (Default: 3900000) (E285/E280 only) */
    SYS_PROP_LOW_THRESHOLD_VOLTAGE=36,       /**< read/write, Low threshold voltage (Default: 3600000) (E285/E280 only) */
    SYS_PROP_CONSOLE_STATE=37,               /**< read/write, Enable/disable console: 1 - console enabled; 0 - console disabled */
    SYS_PROP_USB_HIGH_POWER_MODE=38,         /**< read/write, Enable/disable high power mode  1 - mode enabled; 0 - disabled*/
};

/** String type system properties */
enum SYSPropertyString 
{
    SYS_PROP_HW_MODEL_NAME=0,                /**< read-only,  Hardware model name  */
    SYS_PROP_HW_MODEL_NAME_SHORT=54,         /**< read-only,  Short model name (without information on modifications), e.g. "P400" */
    SYS_PROP_HW_SERIALNO=1,                  /**< read-only,  Hardware serial number */
    SYS_PROP_OS_VERSION=2,                   /**< read-only,  OS version */
    SYS_PROP_HW_PTID=3,                      /**< read-only,  Terminal unit identification number */
    SYS_PROP_HW_VARIANT_NAME=4,              /**< read-only,  Hardware variant name*/
    SYS_PROP_HW_PART_NO=5,                   /**< read-only,  Hardware part number*/
    SYS_PROP_HW_VERSION=6,                   /**< read-only,  Hardware version*/
    SYS_PROP_HW_LOT_NO=7,                    /**< read-only,  Hardware LOT number*/
    SYS_PROP_BOOT_VERSION=8,                 /**< read-only,  Boot(SBI) version*/
    SYS_PROP_TERMINAL_SPONSOR=9,             /**< read-only,  Sponsor certificate name*/
    SYS_PROP_MANUF_DATE=10,                  /**< read-only,  manufacturing date/time in format "yyyymmddhhmmss" */
    SYS_PROP_CERT_SPONSOR_SN=11,             /**< read-only,  Sponsor certificate serial number*/
    SYS_PROP_CERT_SPONSOR_MODE=12,           /**< read-only,  Sponsor certificate mode: "test" or "prod" */
    SYS_PROP_UX100_SERIALNO=13,              /**< read-only,  UX100 Hardware serial number (UX100 only) */
    SYS_PROP_ADK_VERSION=14,                 /**< read-only,  ADK version */
    SYS_PROP_UX100_FW_VERSION=15,            /**< read-only,  UX100 firmware version (UX100 only) */
    SYS_PROP_SBI_VERSION=16,                 /**< read-only,  Secure Boot Image version */
    SYS_PROP_VAULT_VERSION=17,               /**< read-only,  Vault version */
    SYS_PROP_CIB_VERSION=18,                 /**< read-only,  Control Information Block version */
    SYS_PROP_MIB_VERSION=19,                 /**< read-only,  Machine Information Block version */
    SYS_PROP_KERNEL_VERSION=20,              /**< read-only,  Linux Kernel version */
    SYS_PROP_RFS_VERSION=21,                 /**< read-only,  Overall RFS version */
    SYS_PROP_RELEASE_VERSION=22,             /**< read-only,  Build Release version */
    SYS_PROP_RFS_SECURITY_VERSION=23,        /**< read-only,  Application Manager version */
    SYS_PROP_SRED_VERSION=24,                /**< read-only,  SRED version */
    SYS_PROP_OPENPROTOCOL_VERSION=25,        /**< read-only,  OpenProtocol version */
    SYS_PROP_PCI_REBOOT_TIME=26,             /**< read/write, PCI 24h reboot time "hh:mm:ss" */
    SYS_PROP_FILE_NAME_TO_INSTALL=27,        /**< write-only, Deprecated: please use API from sysinstall.h header instead */
    SYS_PROP_MSR_COUNTERS=29,                /**< read-only,  MSR counters  */
    SYS_PROP_SCR_COUNTERS=30,                /**< read-only,  SCR counters */
    SYS_PROP_CTLS_COUNTERS=58,               /**< read-only,  CTLS counters */
    SYS_PROP_DOCK_STATION_SN=31,             /**< read-only,  Docking station serial number */
    SYS_PROP_DOCK_STATION_PN=32,             /**< read-only,  Docking station product number */
    SYS_PROP_DOCK_STATION_MODEL=33,          /**< read-only,  Docking station model */
    SYS_PROP_DOCK_STATION_MAC=34,            /**< read-only,  Docking station MAC address */
    SYS_PROP_DOCK_STATION_HW_REV=35,         /**< read-only,  Docking station hardware revision */
    SYS_PROP_DOCK_STATION_INSTALL=36,        /**< write-only, Deprecated: Please use API from from sysinstall.h instead */
    SYS_PROP_GSM_FIRMWARE_VERSION=37,        /**< read-only,  GSM firmware version */
    SYS_PROP_DOCK_STATION_OS_VERSION=38,     /**< read-only,  Docking station OS version */
    SYS_PROP_DOCK_STATION_IP_ADDRESS=39,     /**< read-only,  Docking station IP V4 address corresponding to SYS_PROP_DOCK_STATION_MAC*/
    SYS_PROP_DOCK_STATION_IP_V6_ADDRESS=51,  /**< read-only,  Docking station IP V6 address corresponding to SYS_PROP_DOCK_STATION_MAC*/
    SYS_PROP_DOCK_STATION_USB1_IP_ADDRESS=57,/**< read-only,  Returns usb1 local IP address for base */
    SYS_PROP_WARRANTIED_KEYS=53,             /**< read-only,  JSON encoded information on warrantied keys (warrantied keys/missing keys) */
    SYS_PROP_SECAPP_VERSION=40,              /**< read-only,  Security application version */
    SYS_PROP_VSS_VERSION=41,                 /**< read-only,  VSS version */
    SYS_PROP_STBY_MCU_VERSION=42,            /**< read-only,  Standby MCU firmware version */
    SYS_PROP_EXT_STORAGE=43,                 /**< read-only,  JSON encoded information on external storage devices */
    SYS_PROP_INSTALLED_PCKG_LIST=28,         /**< read-only,  JSON encoded information on installed packages */
    SYS_PROP_PKG_LIST=28,                    /**< read-only,  JSON encoded information on installed packages */
    SYS_PROP_APP_PKG_LIST=44,                /**< read-only,  JSON encoded information on installed application packages */
    SYS_PROP_BUNDLE_LIST=45,                 /**< read-only,  JSON encoded information on bundles */
    SYS_PROP_APP_BUNDLE_LIST=49,             /**< read-only,  JSON encoded information on installed application bundles */
    SYS_PROP_MSR_INFO=46,                    /**< read-only,  JSON encoded information on MSR card reader */
    SYS_PROP_SCR_INFO=47,                    /**< read-only,  JSON encoded information on SCR card reader */
    SYS_PROP_CTLS_INFO=48,                   /**< read-only,  JSON encoded information on CTLS card reader */
    SYS_PROP_HW_STRING=50,                   /**< read-only,  Hardware description string. Screen resolution / Colored or BW / Keypad size / Touch or non touch. Example: 320x480C15T */
    SYS_PROP_DEV_ID=52,                      /**< read-only,  Device id string e.g.: 6E00F942-C5949001-41A97FA3-2F396409-F0896835-6898E7D4-142B43B7-105FEEB0 */
    SYS_PROP_FET=55,                         /**< read-only,  JSON encoded information on Feature licenses (FET) */
    SYS_PROP_BT_MAC=60,                      /**< read-only,  BT MAC address */ 
    SYS_PROP_WIFI_MAC=61,                    /**< read-only,  WIFI MAC address*/ 
    SYS_PROP_ETH_MAC=62,                     /**< read-only,  ETH MAC address */ 
    SYS_PROP_ETH2_MAC=63,                    /**< read-only,  ETH2 MAC address */ 
    SYS_PROP_COUNTRY=64,                     /**< read-only,  Radio module country code*/ 
    SYS_PROP_RTC=100,                        /**< read/write, Real-Time-Clock date/time in format "yyyymmddhhmmss" */
    SYS_PROP_NTP_SERVER=101,                 /**< write-only, Network Time Protocol host name (if value is empty, pool.ntp.org is used) */
    SYS_PROP_BATTERY_SERIALNO=200,           /**< read-only,  Deprecated */
    SYS_PROP_TIME_ZONE=300,                  /**< read-only,  JSON encoded information on current time zone, e.g. {"doff":"0","dst":"0","end":"0","soff":"UTC+08","start":"0","std":"CST"} */
    SYS_PROP_TIME_ZONE_LIST=301,             /**< read-only,  JSON encoded information on all available timezones */
    SYS_PROP_TIME_ZONE_FROM_FILE=302,        /**< write-only, Set time zone using path, e.g. /usr/share/zoneinfo/US/Pacific */
    SYS_PROP_TIME_ZONE_NAME=304,             /**< write-only, Set time zone using name, e.g. US/Hawaii  */
    SYS_PROP_TIME_ISO8601=303,               /**< read/write, Set time according to ISO8601 standard. Format "YYYY-MM-ddTHH:mm:ss.SSSzzz", e.g. 2020-09-02T07:39:31.000+02:00 */
    SYS_PROP_REBOOT_REASON=400,              /**< read-only,  Returns reason for last reboot */
    SYS_PROP_ALL_LOADED_KEYS=56,             /**< read-only,  JSON encoded information on all keys */
};

/** get int property 
 * \param[in] property property to be get
 * \param[out] value property value
 * \return error code
 */
SYS_INFO_API int sysGetPropertyInt(enum SYSPropertyInt property, int & value);
 
/** get int property 
 * \param[in] property property to be get
 * \param[out] value property value
 * \return error code
 */
SYS_INFO_API int sysGetPropertyInt(enum SYSPropertyInt property, int * value);

/** set int property 
 * \param[in] property property to be set
 * \param[in] value property value
 * \return error code
 */
SYS_INFO_API int sysSetPropertyInt(enum SYSPropertyInt property, int value);

/** get string property 
 * \param[in] property property to be get
 * \param[out] value current value
 * \param[in] len size of output buffer \a value in bytes
 * \return error code
 */
SYS_INFO_API int sysGetPropertyString(enum SYSPropertyString property, char * value, int len);

/** set string property 
 * \param[in] property property to be set
 * \param[in] value new value, C-string
 * \return error code
 */
SYS_INFO_API int sysSetPropertyString(enum SYSPropertyString property, const char * value);

/** get string property 
 * \param[in] property property to be get
 * \param[out] value current value
 * \return error code
 */
SYS_INFO_API int sysGetPropertyString(enum SYSPropertyString property, std::string & value);

/** set string property 
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code
 */
SYS_INFO_API int sysSetPropertyString(enum SYSPropertyString property, const std::string & value);

/** returns a zero-terminated string with version and build information of sysinfo daemon
 *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
 * \return version string */
SYS_INFO_API const char *sysInfo_GetVersion();

/** returns a zero-terminated string with version and build information of libvfisysinfo
 *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
 * \return version string */ 
SYS_INFO_API const char *sysLibInfo_GetVersion();

/** returns a zero-terminated string with version and build information of libvfisysinfo
  *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
  * \return version string */
SYS_INFO_API const char *sysGetVersion();

/** Reboot terminal
 * \return error code
 */
SYS_INFO_API int sysReboot();

/** Puts terminal in sleep mode
 * \return error code
 */
SYS_INFO_API int sysSleep();

/** Puts terminal in deep sleep mode
 * \return error code
 */
SYS_INFO_API int sysDeepSleep();
 
/** Puts terminal in hibernate mode
 * \return error code
 */
SYS_INFO_API int sysHibernate();

/** Shut down terminal
 * \return error code
 */
SYS_INFO_API int sysShutdown();

/** Reboot terminal dock station
 * \return error code
 */
SYS_INFO_API int sysRebootDock();

/** Return error code description
 * \return Error message
 */ 
SYS_INFO_API std::string sysErrorMsg( enum sysError code );

/** Deprecated */
inline const char * sysSDKVersion()
{
   return "UNSUPPORTED";
}

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

