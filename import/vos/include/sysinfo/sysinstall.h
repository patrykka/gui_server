#pragma once

/** \file sysinstall.h 
 * \defgroup vfisysinstall installation functions
 * @{
 */

#include <map>
#include <string>
#include "sysinfo.h"
#include "syserror.h"
#include "dllspec.h"
#include "ipc/jsobject.h"

#ifndef DOXYGEN
/** Verifone vfisysinstall namespace */
namespace vfisysinstall 
{
#endif

/** Authenticate file
 * \return SYS_ERR_OK - file is authenticated
 * \return SYS_ERR_FAIL - authentication error
 */
SYS_INFO_API vfisysinfo::sysError sysAuthenticateFile(const std::string & path);

/** Install bundle
 * \return SYS_ERR_OK - 
 * \return SYS_ERR_FAIL - authentication error
 * \return SYS_ERR_PARAMETER
 * \return SYS_ERR_FILE_DOES_NOT_EXIST 
 * \return SYS_ERR_PERMISSION
 * \return SYS_ERR_REBOOT_NEEDED 
 * \return SYS_ERR_BUNDLE_VERSION - higher version already installed
 * 
 */
SYS_INFO_API vfisysinfo::sysError sysInstall(const std::string & path);

/** Install bundle on base
 * \return SYS_ERR_OK
 * \return SYS_ERR_FAIL 
 * \return SYS_ERR_PROP_UNSUPPORTED
 * \return SYS_ERR_NOT_DOCKED 
 * \return SYS_ERR_REBOOT_NEEDED
 * \return SYS_ERR_BUNDLE_VERSION - higher version already installed
 */   
SYS_INFO_API vfisysinfo::sysError sysBaseInstall();

/** Installs packages from the filepath passed in the function. 
 * \note Expect that the caller of this API is killed during installtion.  
 * Depending on what action is needed (reboot or restart apps) to extract the
 * packages into the filesystem, the installer will do the needful.
 * The caller upon reboot/restart should call the new status API to find out
 * the status of the installation. (VOS2 only)
 * \return SYS_ERR_OK
 * \return SYS_ERR_PARAMETER
 * \return SYS_ERR_FILE_DOES_NOT_EXIST
 * \return SYS_ERR_PATHNAME_TOO_LONG
 * \return SYS_ERR_GET_CWD
 * \return SYS_ERR_FAIL
 */
SYS_INFO_API vfisysinfo::sysError sysFreeResourcesInstall( const std::string & value );


/** Get JSON formatted status for the install API sysFreeResourcesInstall()
 * (VOS2 only)
 * \return SYS_ERR_OK
 * \return SYS_ERR_FAIL
 */
SYS_INFO_API vfisysinfo::sysError sysGetInstallStatus( vfiipc::JSObject & json );

#ifndef DOXYGEN
} // namespace vfisysinstall
#endif
/** @}*/

