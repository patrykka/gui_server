#pragma once

/** \file syspm.h 
 * \defgroup syspm Low level system power management functions. Please use vfisyspm.h instead.
 * @{
 */

#include <map>
#include <string>
#include "dllspec.h"

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo {
#endif

/** reboot terminal
 * \return error code
 */
SYS_INFO_API int sysReboot();

/** reboot terminal dock
 * \return error code
 */
SYS_INFO_API int sysRebootDock();

/** Puts terminal in sleep mode 
 * \return error code
 */
SYS_INFO_API int sysSleep();

/** Puts terminal in deep slee mode
 * \return error code 
 */
SYS_INFO_API int sysDeepSleep();

/** Puts terminal in hibernate mode
 * \return error code 
 */
SYS_INFO_API int sysHibernate();

/** Shut down terminal
 * \return error code
 */
SYS_INFO_API int sysShutdown();

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

