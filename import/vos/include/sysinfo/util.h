#pragma once

/** \file util.h
 * \defgroup util Utility functions
 * @{
 */

#include <string>
#include "dllspec.h"

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo 
{
#endif

/** Converts ASCII hexadecimal data to binary.
 * \param[in] dsp The input in dsp consists of 2 × n characters, one per hex digit. If dsp contains invalid hex digits, the result is undefined.
 * \param[in] hex The output in hex is n bytes.
 * \param[in] n of bytes to convert.
 * \return void
 * \note dsp and hex should not overlap. 
 * \note Hex digits in the input can be either upper or lowercase.
 */  
SYS_INFO_API void Dsp2Hex(char *dsp, long *hex, unsigned int n);

/** Stores value in system ini file 
 * \param[in] section
 * \param[in] label
 * \param[in] value
 * \return 
 */ 
SYS_INFO_API int sysPutEnv( const std::string & section, const std::string & label, const std::string & value );

/** Retrieves value in system ini file 
 * \param[in] section
 * \param[in] label
 * \param[out] value
 * \return 
 */ 
SYS_INFO_API int sysGetEnv( const std::string & section, const std::string & label, std::string & value );

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

