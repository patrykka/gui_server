#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ipc/jsobject.h"

/** \file vfisyspm.h
 * \defgroup Power management functions
 * @{
 */

#if defined VFI_SYS_PM_DLL_EXPORT
    #define VFI_SYS_PM_API  __attribute__((visibility ("default")))
    /*!< Marks public PM API */
#else
    #define VFI_SYS_PM_API
    /*!< Marks public PM API */
#endif

// fix for typo in enum
#define PM_SUPRESS_NONE PM_SUPPRESS_NONE
#define PM_SUPRESS_CRITICAL_SECTION PM_SUPPRESS_CRITICAL_SECTION
#define PM_SUPRESS_CHARGE_CHECK PM_SUPPRESS_CHARGE_CHECK
#define PM_SUPRESS_ALL PM_SUPPRESS_ALL
 
#ifndef DOXYGEN
/** Verifone Power management (vfisyspm) namespace */
namespace vfisyspm
{
#endif

/** error codes returned by the functions */
enum vfisyspmError
{
    SYS_PM_ERR_OK                 =   0,  /**< no error */
    SYS_PM_ERR_FAIL               =  -1,  /**< generic error */
    SYS_PM_ERR_UNSUPPORTED        =  -2,  /**< function is not supported by current platform */
    SYS_PM_ERR_UNKNOWN            =  -3,  /**< unknown command */
    SYS_PM_ERR_DAEMON_COM         =  -4,  /**< communication error with the power management daemon */
    SYS_PM_ERR_TIMEOUT            =  -5,  /**< return code timeout */
    SYS_PM_ERR_LIBNOTIFY          =  -6,  /**< can't obtain appid */
    SYS_PM_ERR_IPC                =  -7,  /**< IPC call failed */
    SYS_PM_ERR_NOT_RUNNING        =  -8,  /**< PM daemon is not running */
    SYS_PM_ERR_PARAM              =  -9,  /**< incorrect parameter is passed */
    SYS_PM_ERR_INIT               = -10,  /**< library is not initialized, run sys_Init() */
    SYS_PM_ERR_IN_CRIT_SECTION    = -11,  /**< not allowed in critical section */
    SYS_PM_ERR_POWER              = -12,  /**< not allowed while unit is powered */
    SYS_PM_ERR_CHARGING           = -13,  /**< not allowed while unit is charging */
    SYS_PM_ERR_CONFIG             = -14,  /**< disabled in current in configuration */
    SYS_PM_ERR_NON_EXISTING       = -15,  /**< requested parameter doesn't exists */
    SYS_PM_ERR_NOTIFY_SVC         = -16,  /**< notification service error */
    SYS_PM_ERR_TIMER              = -17,  /**< generic timer subsystem error */
    SYS_PM_ERR_ALREADY_INIT       = -18,  /**< library is already initialized */
    SYS_PM_ERR_ALLOWED            = -19,  /**< operation is not allowed */
};

/** notification type mask */
enum vfisyspmNotificationType
{
    PM_NOTIFY_TYPE_NONE             = 0,
    PM_NOTIFY_TYPE_POWER_MODE       = (1 << 0),
    PM_NOTIFY_TYPE_DOCK_STATE       = (1 << 1),
    PM_NOTIFY_TYPE_POWER_SOURCE     = (1 << 2),
    PM_NOTIFY_TYPE_WAKEUP_SOURCE    = (1 << 3),
    PM_NOTIFY_TYPE_CRITICAL_SECTION = (1 << 4),
    PM_NOTIFY_TYPE_PCI_REBOOT       = (1 << 5),
    PM_NOTIFY_TYPE_LAST             = (1 << 6),
    PM_NOTIFY_TYPE_ALL              = 0xFFFF,
};

/** timer flags */
enum vfisyspmTimerFlags
{
    PM_TIMER_NONE = 0,
    PM_TIMER_NO_WAKE_UP = (1 << 0), /**< Timer will not wake up unit */
};

/** charging modes */
enum vfisyspmChargningModes
{   
    PM_CHARGING_MODE_UNRESTRICTED = 1, /**< Regular battery charging mode */
    PM_CHARGING_MODE_CAR_CHARGER  = 2, /**< Car charger mode */
}; 


/** transition flags */
enum vfisyspmTransitionFlags
{
    PM_SUPPRESS_NONE              = 0,
    PM_SUPPRESS_CRITICAL_SECTION  = (1 << 0), /**< Ignore critical section */ 
    PM_SUPPRESS_CHARGE_CHECK      = (1 << 1), /**< Ignore charging */ 
    PM_SUPPRESS_BOOT_STATE        = (1 << 2), /**< Ignore boot state */ 
    PM_SYNC                       = (1 << 3), /**< Synchronous API call */ 
    PM_SUPPRESS_ALL = 
        PM_SUPPRESS_CRITICAL_SECTION | PM_SUPPRESS_CHARGE_CHECK | PM_SUPPRESS_BOOT_STATE /**< Ignore all checks */
};

/** critical section parameters */
enum vfisyspmCritSectionParameters
{
    PM_CRIT_NONE = 0,
    PM_CRIT_SUPRESS_STANDBY = (1 << 0),    /**< Standby is not allowed in critical section */
};

/** Possible ADK-PM states */
enum powermngtState
{
    BOOT = -1,       /**< System is booting */
    ACTIVE = 0,      /**< System is active (normal state) */
    STANDBY = 1,     /**< Standby mode (screen is dimmed) */
    SLEEP = 2,       /**< Power saving sate */  
    DEEP_SLEEP = 3,  /**< Power saving sate */   
    HIBERNATE = 4,   /**< Power saving sate */   
    OFF = 5,         /**< Shutdown */
    REBOOT = 6,      /**< System is rebooting */
    LAST = 9         /**< Placeholder */ 
};   

/** notifications returned by callback */
enum vfisyspmNotifications
{
    ENTER_ACTIVE =                0,  /**< Unit entered Active state. Type: PM_NOTIFY_TYPE_POWER_MODE */
    ENTER_STANDBY =               1,  /**< Unit entered Standby state. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    ENTER_SLEEP =                 2,  /**< Unit entered Sleep state. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    ENTER_DEEP_SLEEP =           13,  /**< Unit entered Deep Sleep state. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    ENTER_HIBERNATE =            10,  /**< Unit entered Hibernate state. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    ENTER_SHUTDOWN =             12,  /**< Unit will now shutdown. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    ENTER_REBOOT =               11,  /**< Unit will now reboot. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    ENTER_SYSMODE =              27,  /**< Sysmode is starting. Type: PM_NOTIFY_TYPE_POWER_MODE */
    ENTER_DOCKED =                3,  /**< Unit is docked. Type: PM_NOTIFY_TYPE_DOCK_STATE */
    ENTER_UNDOCKED =              4,  /**< Unit is undocked. Type: PM_NOTIFY_TYPE_DOCK_STATE */
    ENTER_BATTERY  =              5,  /**< Unit runs on battery. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    ENTER_EXTERNAL_POWER =        6,  /**< Unit runs on external power source. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    ENTER_BATTERY_NORMAL =       26,  /**< Battery charge is normal. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    ENTER_BATTERY_FULL =          7,  /**< Battery is fully charged. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    ENTER_BATTERY_LOW =           8,  /**< Battery charge is low. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    ENTER_BATTERY_CRITICAL =      9,  /**< Battery charge is critical. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    BATTERY_FAULT =              35,  /**< Faulty battery detected. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    BUTTON_X_PRESSED =           14,  /**< User is pressing X key. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    BUTTON_X_RELEASED =          15,  /**< User released X key. Type: PM_NOTIFY_TYPE_POWER_MODE  */
    PCI_REBOOT_WARNING =         17,  /**< Unit will reboot soon. Type: PM_NOTIFY_TYPE_PCI_REBOOT */
    WAKEUP =                     16,  /**< Unit is awaken from power saving state. Type: PM_NOTIFY_TYPE_POWER_MODE */
    WAKEUP_COMM =                18,  /**< Unit was awaken by communication interface. Type: PM_NOTIFY_TYPE_WAKEUP_SOURCE */
    WAKEUP_USB =                 19,  /**< Unit was awaken by USB interface. Type: PM_NOTIFY_TYPE_WAKEUP_SOURCE */
    WAKEUP_CONSOLE =             20,  /**< Unit was awaken by serial interface. Type: PM_NOTIFY_TYPE_WAKEUP_SOURCE */
    WAKEUP_KEY_PRESS =           21,  /**< Unit was awaken by keyboard interface. Type: PM_NOTIFY_TYPE_WAKEUP_SOURCE */
    WAKEUP_TIMER =               22,  /**< Unit was awaken by HW timer. Type: PM_NOTIFY_TYPE_WAKEUP_SOURCE */
    WAKEUP_EXTERNAL =            23,  /**< Unit was awaken by external power cable. Type: PM_NOTIFY_TYPE_WAKEUP_SOURCE */
    CRITICAL_SECTION_ENABLED =   24,  /**< Unit entered critical section. Type: PM_NOTIFY_TYPE_CRITICAL_SECTION */
    CRITICAL_SECTION_DISABLED =  25,  /**< Unit exited critical section. Type: PM_NOTIFY_TYPE_CRITICAL_SECTION */
    ABORT_PINPAD_ENABLE =        28,  /**< Abort due to secure keypad (PINPAD) enabled. Type: PM_NOTIFY_TYPE_POWER_MODE */
    ABORT_EARLY_WAKEUP =         29,  /**< Abort due to pending wakeup event. Type: PM_NOTIFY_TYPE_POWER_MODE */
    ABORT_HMAC_PROCESSING =      30,  /**< Abort due to HMAC computation in progress. Type: PM_NOTIFY_TYPE_POWER_MODE */
    ABORT_CRITICAL_SECTION =     31,  /**< Abort due to critical section ON. Type: PM_NOTIFY_TYPE_POWER_MODE */
    ABORT_SYSTEM_ERROR =         32,  /**< Abort due to sysem error. Type: PM_NOTIFY_TYPE_POWER_MODE */
    CHARGING_MODE_UNRESTRICTED = 33,  /**< Regular charging mode. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
    CHARGING_MODE_CAR_CHARGER  = 34,  /**< Car charger mode. Type: PM_NOTIFY_TYPE_POWER_SOURCE */
};

typedef int wakeupHandle; // wakeup handle
typedef int timerHandle;  // timer handle

// Callback will receive JSON encoded timer information.
// JSON will include: time, timer handle and passed message.
// example:
// {"handle":11,"message":"timer for 90 seconds","time":1510696636}
typedef void (*timerNotificationCallback)( const vfiipc::JSObject & json ); // timer callback
  
/** Notify callback. The callback gets invoked when a matching notification has been sent.
 * \param[in] type vfisyspmNotificationType of received notification
 * \param[in] notification vfisyspmNotifications received notification
 * \note All notification and observer callbacks share one thread, therefore, at most one callback can be active at the same time.
 */
typedef void (*vfisyspmCallback)(vfisyspmNotificationType type, vfisyspmNotifications notification);

/*  Replica of the Linux structure tm in time.h */
struct vfisyspm_DateTime
{
    int tm_sec;     /**< seconds */
    int tm_min;     /**< minutes */
    int tm_hour;    /**< hours */
    int tm_mday;    /**< day of the month */
    int tm_mon;     /**< month */
    int tm_year;    /**< year */
    int tm_wday;    /**< day of the week */
    int tm_yday;    /**< day in the year */
    int tm_isdst;   /**< daylight saving time */
};

/** Library initialization function
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_IPC - can't init IPC
 * \return SYS_PM_ERR_TIMER - can't init timer thread
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Init();

/** Library initialization function
 * \param[in] cb notification callback
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_IPC - can't init IPC
 * \return SYS_PM_ERR_TIMER - can't init timer thread
 * \return SYS_PM_ERR_OK - success
 */ 
VFI_SYS_PM_API vfisyspmError sys_Init( vfisyspmCallback cb );

/** Library initialization function
 * \param[in] cb notification callback
 * \param[in] typeMask vfisyspmNotificationType mask, to register for specific notifications
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_IPC - can't init IPC
 * \return SYS_PM_ERR_TIMER - can't init timer thread
 * \return SYS_PM_ERR_OK - success
 */ 
VFI_SYS_PM_API vfisyspmError sys_Init( vfisyspmCallback cb, int typeMask );
VFI_SYS_PM_API vfisyspmError sys_Init( vfisyspmCallback cb, vfisyspmNotificationType typeMask );

/** Deinit library, disable notification callback
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Deinit();

/** Set notification callback
 * \param[in] cb notification callback
 * \param[in] typeMask vfisyspmNotificationType mask, to register for specific notifications
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_IPC - can't init IPC
 * \return SYS_PM_ERR_TIMER - can't init timer thread
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetNotificationCB( vfisyspmCallback cb, vfisyspmNotificationType typeMask );

/** Set charging mode
 * \param[in] mode charging mode from vfisyspmChargningModes enum
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_IPC - can't init IPC
 * \return SYS_PM_ERR_TIMER - can't init timer thread
 * \return SYS_PM_ERR_OK - success
 */ 
VFI_SYS_PM_API vfisyspmError sys_SetChargingMode( vfisyspmChargningModes mode );

/** Get charging mode
 * \param[in] mode charging mode from vfisyspmChargningModes enum
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_IPC - can't init IPC
 * \return SYS_PM_ERR_TIMER - can't init timer thread
 * \return SYS_PM_ERR_OK - success
 */ 
VFI_SYS_PM_API vfisyspmError sys_GetChargingMode( vfisyspmChargningModes & mode );

/** Rescans available PM configuration files and configures PM accordingly
 * \note if no configuration file is available on system, default values are set
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_RefreshConfig();

/** Enter active mode
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Active(int flags);
VFI_SYS_PM_API vfisyspmError sys_Active();

/** Enter standby mode
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Standby(int flags);
VFI_SYS_PM_API vfisyspmError sys_Standby();

/** Enter sleep mode
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Sleep(int flags);
VFI_SYS_PM_API vfisyspmError sys_Sleep();

/** Enter deep sleep mode
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_DeepSleep(int flags);
VFI_SYS_PM_API vfisyspmError sys_DeepSleep();

/** Enter hibernate mode
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Hibernate(int flags);
VFI_SYS_PM_API vfisyspmError sys_Hibernate();

/** Shut down device
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Shutdown(int flags);
VFI_SYS_PM_API vfisyspmError sys_Shutdown();

/** Reboot device
 * \param[in] flags - vfisyspmTransitionFlags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_Reboot(int flags);
VFI_SYS_PM_API vfisyspmError sys_Reboot();

/** Reboot dock station
 * \note Can be called only if terminal is docked
 * \return SYS_PM_ERR_UNSUPPORTED - current platform is not supported
 * \return SYS_PM_ERR_NON_EXISTING - unit is not docked
 * \return SYS_PM_ERR_FAIL - failure
 * \return SYS_PM_ERR_OK - success
 * \note When dock station is rebooting or not booted yet, e.g. when calling this function twice within short time interval on VOS2, then return code might be SYS_PM_ERR_FAIL or SYS_PM_ERR_NON_EXISTING
 */
VFI_SYS_PM_API vfisyspmError sys_RebootDock();

/** Enters PM critical section. Allows Standby
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CriticalSectionEnter();

/** Enters PM critical section
 * \param[in] params vfisyspmCritSectionParameters bitmask
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CriticalSectionEnter(int params);

/** Exits PM critical section
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CriticalSectionExit();

/** Exits PM critical section
 * \param[out] state if critical section is active or not
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CriticalSectionState( bool & state );

/** Sets time to wake up from sleep
 * \param[in] *wakeupTime wake up time from Sleep
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetWakeupTime( struct vfisyspm_DateTime * wakeupTime );

/** Sets time to wake up from sleep
 * \param[out] handle
 * \param[in] wakeup_time wake up time from Sleep
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetWakeupTime( wakeupHandle & handle,
            const vfisyspm_DateTime & wakeup_time );

/** Sets time to wake up from sleep
 * \param[out] handle
 * \param[in] time wake up time from Sleep
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetWakeupTime( wakeupHandle & handle, time_t time );

/** Sets time to wake up from sleep
 * \param[in] time wake up time from Sleep
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetWakeupTime( time_t time );
 
/** Clears wake up time list
 * \param[in] handle
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_NON_EXISTING - non existing handle
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CancelWakeupTime( const wakeupHandle & handle );

/** Clears all wakeup times set by application
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_ClearWakeupTimes();

/** Sets precise time for 24 hour PCI reboot
 * \param[in] rebootTime reboot time
 * \return SYS_PM_ERR_OK - success
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_UNSUPPORTED - PCI reboot is not supported by platform
 * \note only hour, minute and second fields will be used
 * \note Terminal should be rebooted for new value to be active
 */
VFI_SYS_PM_API vfisyspmError sys_SetPciRebootTime(struct vfisyspm_DateTime * rebootTime);

/** Get time for 24 hour PCI reboot
 * \param[out] rebootTime reboot time
 * \return SYS_PM_ERR_OK - success
 * \return SYS_PM_ERR_PARAM - invalid rebbotTime parameter
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_UNSUPPORTED - PCI reboot is not supported by platform
 * \note only hour, minute and second fields are used
 */
VFI_SYS_PM_API vfisyspmError sys_GetPciRebootTime(struct vfisyspm_DateTime * rebootTime);

/** Clears PCI reboot time
 * \return SYS_PM_ERR_OK - success
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_UNSUPPORTED - PCI reboot is not supported by platform
 */
VFI_SYS_PM_API vfisyspmError sys_ClearPciRebootTime();

/** Returns current PM configuration
 * \param[out] config json encoded PM configuration
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_GetConfig( vfiipc::JSObject & config );

/** Sets passed configuration
 * \param[in] config json encoded PM configuration
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 * \note JSON structure is similar to configuration file structure
 */
VFI_SYS_PM_API vfisyspmError sys_SetConfig( const vfiipc::JSObject & config );

/** Set timer for certain time
 * \param[out] handle
 * \param[in] time
 * \param[in] payload
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 * \note timer will invoke callback set by sys_SetTimerCallback API
 */
VFI_SYS_PM_API vfisyspmError sys_SetTimer( timerHandle & handle,
            const vfisyspm_DateTime & time,
            const std::string & payload = "" );

/** Set timer for certain time
 * \param[out] handle
 * \param[in] time
 * \param[in] payload
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 * \note timer will invoke callback set by sys_SetTimerCallback API
 */
VFI_SYS_PM_API vfisyspmError sys_SetTimer( timerHandle & handle,
            const time_t time,
            const std::string & payload = "" );

/** Set timer for certain time
 * \param[out] handle
 * \param[in] time
 * \param[in] payload
 * \param[in] cb
 * \param[in] flags
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetTimer( timerHandle & handle,
            const time_t time,
            const std::string & payload,
            timerNotificationCallback cb,
            int flags = 0 );

/** Cancel set timer
 * \param[in] handle
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_NON_EXISTING - non existing handle
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CancelTimer( const timerHandle & handle );

/** Cancel all timers set by application
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CancelTimers();

/** Register default timer callback
 *  This callback will be invoked in timer API if no timer specific callback is set
 * \param[in] cb notification callback to be invoked
 * \note only one default callback can be set
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_SetTimerCallback( timerNotificationCallback cb );

/** Request last reboot reason
 * \param[out] reason reason of last reboot
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_UNSUPPORTED - Functionality is not supported        
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_GetRebootReason( std::string & reason );

/** Will start sysmode
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_UNSUPPORTED - Functionality is not supported        
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_StartSysmode();
 
/**
 * @brief Get list of applications currently holding critical section
 * \param[out] list json encoded application list
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_CriticalSectionList( vfiipc::JSObject &list );

/**
 * @brief Get current PM daemon state
 * \param[in] handle
 * \return SYS_PM_ERR_INIT - library is not initialized
 * \return SYS_PM_ERR_TIMEOUT - PM daemon is not responding
 * \return SYS_PM_ERR_IPC - IPC library is not initialized
 * \return SYS_PM_ERR_NOTIFY_SVC - Notification service error
 * \return SYS_PM_ERR_LIBNOTIFY - Notification service error
 * \return SYS_PM_ERR_NON_EXISTING - non existing handle
 * \return SYS_PM_ERR_OK - success
 */
VFI_SYS_PM_API vfisyspmError sys_GetCurrentState( powermngtState & state );
 
/**
 * @brief Get vfisyspm daemon version
 * \return "N/A" - can't obtain version
 * \return daemon version
 */
VFI_SYS_PM_API const char *sys_GetSvcVersion();

/**
 * @brief Get vfisyspm library version
 * \return "N/A" - can't obtain version
 * \return daemon version
 */
VFI_SYS_PM_API const char *sys_GetVersion();

/**
 * @brief Get error description message
 * \return Error message
 */  
VFI_SYS_PM_API const char * sys_StrError( vfisyspmError error );


#ifndef DOXYGEN
} // namespace vfisyspm
#endif

/** @}*/
