#pragma once

#include "dllspec.h"
 
/** \file zontalk.h 
 * \defgroup zontalk zontalk protocal APIs
 * @{
 */

#include <map>
#include <string>

#ifndef DOXYGEN
/** Verifone vfizontalk namespace */
namespace vfizontalk 
{
#endif

enum download_type
{
    PARTIAL = 0,
    FULL = 1,
};

typedef void (*recieve_callback)(char *);
typedef void (*end_callback)(int);

/**
 * Allows an application to perform a local DL/Zontalk/VeriTalk download. 
 * The application must open and configure the port before calling this function.
 *
 * @param[in] com_port Communication port where the download is received.
 * @param[in] *rc_cb Callback function called when an information/status message is received from the Zontalk/DL server.
 * @param[in] type download type
 * @param[in] *end_cb Callback function called when the download completes successfully or failed
 *
 * @return 
 * @li 0 = Success
 * @li < 0 = Download failed
 *
 */  
SYS_INFO_API int sysZontalkReceive(short com_port, recieve_callback rc_cb, end_callback end_cb, download_type type );

/**
 * Cancel a Zontalk/DL download (initiated by sysZontalkRecieve)
 *
 * @return
 * @li 0 = Success
 * @li < 0 = Error occurred or a download is not in progress
 */  
SYS_INFO_API int sysZontalkCancel();
  
#ifndef DOXYGEN
} // namespace vfizontalk
#endif
/** @}*/
 
