/**
 * \file tec.h
 * Interface definitions for libtec.
 * This file defines the API for the technology selection library.
 * \author Thomas Buening, GSS
 */

#ifndef __TEC_LIB_INTERFACE__
#define __TEC_LIB_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif

#include "tec/tec_common.h"

/**
 *  Type of function that is called after technology selection has been finished (see cts_StartSelection()) or removed (see cts_WaitCardRemoval()).
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* cts_Callback) (void *data);

/**
 * Set additional options.
 * This function must not be called while technology selection is running.
 * \param[in] options : data pointer, TLV format, see \ref TEC_OPTION_TAGS
 * \param[in] options_len : length of options
 * \return
 * - \ref CTS_OK : No error
 * - \ref CTS_API_NOT_ALLOWED : API not allowed because TEC-callback is still in progress.
 * - \ref CTS_PARAM : options == NULL or options_len == 0 or TLV error
 * - \ref CTS_ERROR : technology selection is currently running.
 */
int cts_SetOptions(const unsigned char *options, unsigned char options_len);

/**
 * This function starts an asynchronous card reader monitoring. The monitoring ends if
 * - magstripe card is swiped or inserted
 * - or chip card is inserted
 * - or contactless card is tapped
 * - or the timeout occurs
 * - or cts_StopSelection() is called
 * - or error occurred.
 *
 * \param[in] supportedTechnologies : supported technologies: combination of \ref TEC_TECHNOLOGIES, any additional bits are reserved for future use and are currently ignored.
 *                                    Supplying none of \ref TEC_TECHNOLOGIES is allowed. In this case cts_WaitSelection() will never return \ref CTS_OK or \ref CTS_NO_CHIP of course.
 * \param[in] timeout_sec           : main timeout in seconds to wait for card insertion/swipe/tap, min=0 (makes no sense), max=65535s, infinite timeout not possible; if this timeout expires while a timeout defined in options[6..7] or options[8..9] is running, latter timeouts have higher priority, they are not aborted.
 * \param[in] cbf                   : callback function that is called when technology selection has been finished, that means a (positive or negative) result != \ref CTS_IN_PROGRESS can be obtained with cts_WaitSelection(), may be NULL. \n
 *                                    The following APIs are not allowed to be called while the callback function is still in progress: \n
 *                                    - cts_SetTraceCallback() \n
 *                                    - cts_SetOptions() \n
 *                                    - cts_StartSelection() \n
 *                                    - cts_RemoveTechnologies() \n
 *                                    - cts_WaitCardRemoval() \n
 *                                    - cts_WaitCardRemoval2() \n
 *                                    - cts_StopSelection() \n
 * \param[in] cb_data               : data pointer that is passed on to the callback function, may be NULL.
 * \param[in] options               : data pointer:\n
 *                                    <b>options[0..1]</b>: see \ref TEC_START_OPTIONS \n
 *                                    <b>options[2..3]</b>: reserved.\n
 *                                    <b>options[4]</b> is passed to CT ICC functions.\n
 *                                    <b>options[5]</b> is passed to CTLS ICC functions.\n
 *                                    <b>options[6..7]</b>: is a 2-byte binary parameter in big-endian format. min = 0x0000, max = 0xFFFF = 65535ms. options[6..7] is the time in milliseconds to wait for MSR-Data after CTLS has been detected. If options[6..7] == 0x0000, means do not wait for MSR after CTLS detection.\n
 *                                    <b>options[8..9]</b> is a 2-byte binary parameter in big-endian format, which is relevant for UX-Devices only. min = 0x0000, max = 0xFFFF = 65535ms. This parameter has two uses depending on the \ref TEC_TECHNOLOGIES \ref CTS_CHIP \n
 *                                    1. \ref CTS_CHIP is set. options[8..9] is the time in milliseconds to wait for MSR-Data after a card without chip or with broken chip is inserted. If options[8..9] == 0x0000, means do not wait for MSR-Data after card insertion and report \ref CTS_NO_CHIP immediately.\n
 *                                    2. \ref CTS_CHIP is disable. options[8..9] sets a time window for reading MSR-Data in order to avoid the pitfall in UX-Devices of getting MSR-Data when inserting the card. options[8..9] is the time in milliseconds to wait for MSR-Data after a card is inserted. If MSR-Data is not read during this time window, MSR_Deactivate() shall be called, technology selection shall be terminated and cts_WaitSelection() shall return the value \ref CTS_UX_MSRDATA_NOT_AVAILABLE_TIMEOUT. If options[8..9] == 0x0000, means wait for MSR-Data after card insertion until technology selection finishes.\n
 *                                    .
 *                                    <b>options[10..11]</b>: is a 2-byte binary parameter in big-endian format, which becomes relevant only if \ref CTS_VAS_ENABLE and \ref CTS_EMV_CTLS_TIMEOUT_AFTER_VAS are set. min = 0x0000 = default value, max = 0xFFFF = 65535ms. options[10..11] sets a permissible time window in milliseconds to perform a contactless payment after either VAS_DO_PAY or VAS_DO_PAY_DECRYPT_REQ has been detected.  If a contactless-payment-medium is not detected during this time window, technology selection shall be terminated and cts_WaitSelection() shall return the value \ref CTS_CTLS_EMV_NO_CARD. If options[10..11] == 0x0000, technology selection shall discard options[10..11] and shall wait for a contactless payment until the main timeout set in the input parameter timeout_sec expires.\n
 *                                    <b>options[12..15]</b>: NFC technologies, this parameter is passed to NFC poll function, 4 byte binary, big-endian, only relevant if \ref CTS_NFC_ENABLE is set, may be NULL (default = {0}).\n
 * \param[in] options_len           : length of options.
 * \return
 * - \ref CTS_OK : Success.
 * - \ref CTS_IN_PROGRESS : Monitoring is already active. Call cts_WaitSelection() until it returns != \ref CTS_IN_PROGRESS first.
 * - \ref CTS_API_NOT_ALLOWED : API not allowed because TEC-callback is still in progress.
 * - \ref CTS_PARAM : both \ref CTS_NFC_ENABLE and \ref CTS_VAS_ENABLE are set.
 * - \ref CTS_ERR_LOAD : \ref CTS_NFC_ENABLE or \ref CTS_VAS_ENABLE are set but NFC client library could not be loaded.
 * - \ref CTS_ERROR : Failure.
 */
int cts_StartSelection(unsigned char supportedTechnologies, unsigned short timeout_sec,
                                    cts_Callback cbf, void *cb_data,
                                    unsigned char *options, unsigned char options_len);

/**
 * This function stops a technology selection started via cts_StartSelection().
 * It will be called by application if waiting for a card is canceled by user or ECR break.
 * Keep in mind that the technology selection may not be stopped immediately.
 * Call cts_WaitSelection() to wait for termination of technology selection.
 * After cts_WaitSelection() returns != \ref CTS_IN_PROGRESS, it is safe to call cts_StartSelection() again.
 * \return
 * - \ref CTS_OK : Stopping technology selection is successfully requested
 * - \ref CTS_NOT_STARTED : Technology selection is not running
 * - \ref CTS_API_NOT_ALLOWED : API not allowed because TEC-callback is still in progress.
 */
int cts_StopSelection (void);

/**
 * This function waits for technology selection to finish.
 * \param[out]    usedTechnology   : technology that has been selected, only set if \ref CTS_OK is returned.
 *                                   See \ref TEC_TECHNOLOGIES .
 *                                   If \ref CTS_DATA_TLV is set dataBuffer is in TLV format (this is the case if NFC or VAS was detected).
 *                                   In certain circumstances ('MSR after CTLS timeout' is set; UX MSR enhancements not enabled) it is possible that usedTechnology contains more than one technology at once, see documentation.
 * \param[out]    dataBuffer       : reference to buffer for output data, only filled if \ref CTS_OK is returned.
 *                                   \n Recommended allocation size: 11264 bytes if NFC/VAS is used, else 256 bytes
 *                                   \n a) \ref CTS_DATA_TLV is set in usedTechnology:
 *                                   \n   contains tags, see \ref TEC_DATA_TAGS.
 *                                   \n b) \ref CTS_DATA_TLV is not set in usedTechnology:
 *                                   \n   If *usedTechnology & \ref CTS_CHIP : contains ATR.
 *                                   \n   If *usedTechnology & \ref CTS_CTLS and \ref CTS_PURE_CARD_DETECTION was set as option to cts_StartSelection() : contains card info delivered by EMV_CTLS_SmartReset().
 *                                   \n   If *usedTechnology & \ref CTS_CTLS and \ref CTS_PURE_CARD_DETECTION was not set : contains return value of EMV_CTLS_ContinueOffline().
 * \param[in,out] dataBufferLength : buffer size for output data, return data length;
 *                                   if the size of dataBuffer is too small to hold the whole output data,
 *                                   no special error code is returned, the return code is as usual, but the output buffer will be empty, dataBufferLength is set to 0.
 *                                   If return value is != \ref CTS_OK, dataBufferLength is set to 0 to indicate that there is no data written in dataBuffer.
 * \param[in]     timeout_msec     : timeout in milliseconds to wait for technology selection to finish, min=0, max=65535ms.
 *                                   If technology selection is not finished after this timeout has expired, \ref CTS_IN_PROGRESS is returned. In this case cts_WaitSelection() has to be called again.
 *                                   If a callback function is supplied to cts_StartSelection(), setting a timeout != 0 is not allowed.
 * \return
 * - \ref CTS_OK               : Successful completion, card was detected. Not possible if \ref CTS_NO_POWERON is set.
 * - \ref CTS_NO_CHIP          : Card without chip or with broken chip is inserted or card is inserted and \ref CTS_NO_POWERON is set.
 * - \ref CTS_IN_PROGRESS      : Technology selection not completed. Another call of cts_StartSelection() will return \ref CTS_IN_PROGRESS in this case.
 * - \ref CTS_TIMEOUT          : Timeout occurred, no card detected.
 * - \ref CTS_PARAM            : - usedTechnology is NULL pointer; - callback function supplied to cts_StartSelection() and timeout != 0.
 * - \ref CTS_NOT_STARTED      : cts_StartSelection() was not called previously.
 * - \ref CTS_STOPPED          : Technology selection was aborted by cts_StopSelection().
 * - \ref CTS_CTLS_INIT        : Contactless transaction was not set up correctly.
 * - \ref CTS_ERROR            : Internal error occurred.
 * - \ref CTS_CTLS_NOT_ALLOWED : VFI-Reader has not yet finished previous transaction, e.g. still waiting for ContinueOnline.
 * - \ref CTS_CTLS_EMV_NO_CARD : ADK-EMV has detected no medium to perform a contactless payment.
 * - \ref CTS_UX_MSRDATA_NOT_AVAILABLE_TIMEOUT : Technology selection has been terminated because MSR-Data was not read during the time window set in the input parameter options[8..9] of the function cts_StartSelection(). This return code is relevant and possible for UX-Devices only.
 */
int cts_WaitSelection(unsigned char *usedTechnology, unsigned char *dataBuffer,
                                   unsigned short *dataBufferLength, unsigned short timeout_msec);

/**
 * This function removes technologies from currently running technology selection.
 * This can be useful to remove contact and magstripe technologies after the application was informed by EMV-ADK that a ctls retap scenario is running.
 * \param[in] technologies : technologies to remove from running technology selection: combination of \ref TEC_TECHNOLOGIES, any additional bits are reserved for future use and are currently ignored.
 *                           Supplying none of \ref TEC_TECHNOLOGIES is allowed. In this case this function does actually nothing.
 * \return
 * - \ref CTS_OK : Removing technologies is successfully requested.
 * - \ref CTS_NOT_STARTED : Technology selection is not running.
 * - \ref CTS_API_NOT_ALLOWED : API not allowed because TEC-callback is still in progress.
 */
int cts_RemoveTechnologies(unsigned char technologies);

/**
 * This function registers callback for card removal.
 * The function returns immediately with one of the return values stated below.
 * The callback is invoked as soon as the inserted card is removed or immediately if no card is inserted.
 * Keep in mind that the callback is only invoked once. If you want to be informed about the next card removal as well, call this function again, even from within the callback function.
 * Attention: Do not call this function as long as other TEC/EMV functions are running and do not call other TEC/EMV functions until the callback has been invoked!
 * \param[in] cbf : callback function that is called when a card has been removed, must not be NULL.
 * \param[in] cb_data : data pointer that is passed on to the callback function.
 * \return
 * - \ref CTS_OK : Success.
 * - \ref CTS_IN_PROGRESS : Waiting for card removal is already active.
 * - \ref CTS_API_NOT_ALLOWED : API not allowed because TEC-callback is still in progress.
 * - \ref CTS_PARAM : Missing parameter.
 * - \ref CTS_ERROR : Internal error.
 */
int cts_WaitCardRemoval(cts_Callback cbf, void *cb_data);

/**
 * This function waits for card removal.
 * The function does not return until card has been removed or timeout has occurred.
 * Attention: Do not call this function as long as other TEC/EMV functions are running and do not call other TEC/EMV functions until the function has returned!
 * \param[in] timeout_sec : timeout in seconds to wait for card removal.
 * \return
 * - \ref CTS_OK : Card has been removed.
 * - \ref CTS_TIMEOUT : Timeout occurred.
 * - \ref CTS_API_NOT_ALLOWED : API not allowed because TEC-callback is still in progress.
 * - \ref CTS_ERROR : Internal error.
 */
int cts_WaitCardRemoval2(unsigned short timeout_sec);


#ifdef __cplusplus
}
#endif

#endif
