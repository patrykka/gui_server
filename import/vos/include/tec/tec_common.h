/**
 * \file tec_common.h
 * Interface definitions for libtec, common part for tec.h and tecclient.h.
 * This file defines the API for the technology selection library.
 * \author Dirk Germann, GSS
 */

#ifndef __TEC_COMMON_INTERFACE__
#define __TEC_COMMON_INTERFACE__


/**
 * \defgroup TEC_TECHNOLOGIES TEC technology code
 * These codes are used to specify supported technologies.
 * A combination is possible using | operator.
 * \{ */
#define CTS_CHIP 1  /**< Contact Chip (asynchronous card). */
#define CTS_MSR  2  /**< Magstripe. */
#define CTS_CTLS 4  /**< Contactless (EMV, NFC, and/or VAS). */
#define CTS_SYNC 8  /**< Contact Chip, synchronous card. */
/** \} */

#define CTS_DATA_TLV 0x80 /**< if set in usedTechnology parameter of cts_WaitSelection(), dataBuffer is in TLV format (this is only the case if NFC or VAS are used) */

/**
 * \defgroup TEC_RETURN_CODES TEC return codes
 * These return codes are returned by the various libtec functions.
 * \{ */
#define CTS_OK                0  /**< No error, function executed successfully. */
#define CTS_NO_CHIP           1  /**< Card without chip or with broken chip is inserted or card is inserted and \ref CTS_NO_POWERON is set (returned by cts_WaitSelection()). */
#define CTS_IN_PROGRESS       2  /**< Operation in progress (returned by cts_StartSelection(), cts_WaitSelection(), cts_WaitCardRemoval()). */
#define CTS_TIMEOUT           3  /**< Timeout occurred (returned by cts_WaitSelection(), cts_WaitCardRemoval2()). */
#define CTS_PARAM             4  /**< Parameter error (returned by cts_SetOptions(), cts_WaitSelection(), cts_WaitCardRemoval()). */
#define CTS_NOT_STARTED       5  /**< Technology selection is not running (returned by cts_WaitSelection(), cts_StopSelection(), cts_RemoveTechnologies()). */
#define CTS_STOPPED           6  /**< Technology selection was aborted by cts_StopSelection() (returned by cts_WaitSelection()). */
#define CTS_CTLS_INIT         7  /**< Contactless transaction was not set up correctly (returned by cts_WaitSelection()). */
#define CTS_ERROR             8  /**< Internal error. */
#define CTS_CTLS_NOT_ALLOWED  9  /**< VFI-Reader has not yet finished previous transaction (returned by cts_WaitSelection()). */
#define CTS_CTLS_EMV_NO_CARD 10  /**< ADK-EMV has detected no medium to perform a contactless payment (returned by cts_WaitSelection()). */
#define CTS_ERR_LOAD         11  /**< Error loading dynamic library (NFC_Client.so/NFC_Client.vsl). */
#define CTS_VAS_DECRYPTION_NOT_REQUIRED      12  /**< VAS output data is not encrypted (returned by TEC if NFC_VAS_Activate() reports data-not-encrypted). @n TLV tag #CTS_DATA_TAG_VAS_DECRYPT_DATA_RESULT */
#define CTS_VAS_DATA_DECRYPTED_OK            13  /**< VAS output data decrypted successfully (returned by TEC if NFC_VAS_Data_Decrypt() == VAS_OK). @n TLV tag #CTS_DATA_TAG_VAS_DECRYPT_DATA_RESULT */
#define CTS_VAS_DATA_DECRYPTED_FAILED        14  /**< VAS output data decryption failed (returned by TEC if NFC_VAS_Data_Decrypt() == VAS_FAIL). @n TLV tag #CTS_DATA_TAG_VAS_DECRYPT_DATA_RESULT */
#define CTS_UX_MSRDATA_NOT_AVAILABLE_TIMEOUT 15  /**< Technology selection has been terminated because MSR-Data was not read during the time window set in the input parameter options[8..9] of the function cts_StartSelection(). This return code, which is returned by cts_WaitSelection(), is relevant and possible for UX-Devices only. It can be interpreted by the application as "card inserted incorrectly" or "magstripe reader malfunction".*/
#define CTS_NFC_INIT                         16  /**< NFC not initialized (returned by cts_WaitSelection()). Application has to call NFC_Client_Init before. */
#define CTS_CHIP_WITH_WRONG_ATR              17  /**< Chip-Card is inserted but its ATR is not EMV conform. This value may be returned by cts_WaitSelection() only if EMV_CT_DETECT_WRONG_ATR is set in the input parameter options[4] of the function cts_StartSelection().*/
#define CTS_API_NOT_ALLOWED                  18  /**< API not allowed because TEC-callback is still in progress. */
#define CTS_VAS_DATA_ENCRYPTED_OK            19  /**< Encrypted VAS output data was NOT decrypted. Reason: #CTS_VAS_DONT_DECRYPT was put in by application. @n TLV tag #CTS_DATA_TAG_VAS_DECRYPT_DATA_RESULT */
/** \} */

/**
 * \defgroup TEC_START_OPTIONS TEC start options
 * These options configure the behavior of technology selection, set by cts_StartSelection().
 * A combination is possible using | operator.
 * \{ */
#define CTS_PURE_CARD_DETECTION                 0x01 /**< options[0] Relevant for CTLS only: only detect card, do not perform transaction */
#define CTS_NO_POWERON                          0x02 /**< options[0] Relevant for CT only (must not be set in conjunction with \ref CTS_POWERON_AFTER_CTLS_MSR_DEACTIVATION): do not power on inserted card. */
#define CTS_POWERON_AFTER_CTLS_MSR_DEACTIVATION 0x04 /**< options[0] Relevant for CT only (must not be set in conjunction with \ref CTS_NO_POWERON): deactivate CTLS and MRS once chip-card is detected and then power on the chip-card. */

#define CTS_NFC_ENABLE                  0x01 /**< options[1] CTLS, only allowed if \ref CTS_VAS_ENABLE is NOT set: ADK-NFC is used for detection of contactless cards, options[12..15] determine which NFC technology is enabled. */
#define CTS_VAS_ENABLE                  0x02 /**< options[1] CTLS, only allowed if \ref CTS_NFC_ENABLE is NOT set: ADK-NFC is used for Wallet processing. */
#define CTS_EMV_AFTER_NFC_ISO           0x04 /**< options[1] Relevant only if \ref CTS_NFC_ENABLE is set. Perform an EMV transaction if NFC has detected one of the following cards:
                                                             I_ISO14443A, I_ISO14443B */
#define CTS_VAS_HANDLE_LED_BUZZ         0x08 /**< options[1] Relevant only if \ref CTS_VAS_ENABLE is set: Handle LEDs and buzzer if VAS processing failed or VAS only (no subsequent EMV transaction). */
#define CTS_EMV_CTLS_TIMEOUT_AFTER_VAS  0x10 /**< options[1] Relevant only if \ref CTS_VAS_ENABLE is set: This option enables the use of a permissible time window to perform a contactless payment after either VAS_DO_PAY or VAS_DO_PAY_DECRYPT_REQ has been detected. The dimension of this time window is configured with the input parameter options[10..11] of the function  \ref  cts_StartSelection(). */
#define CTS_VAS_DONT_DECRYPT            0x20 /**< options[1] Relevant only if \ref CTS_VAS_ENABLE is set: TEC will NOT decrypt the resulting VAS data. */
/** \} */

/**
 * \defgroup TEC_DATA_TAGS TEC result data tags
 * These tags are used in dataBuffer of cts_WaitSelection() if \ref CTS_DATA_TLV is set in usedTechnology
 * \{ */
#define CTS_DATA_TAG_NFC_RESULT               0xDFDB20 /**< return code of either NFC_PT_Polling() or NFC_PT_PollingFull(). */
#define CTS_DATA_TAG_CARD                     0xFFDB20 /**< card detected by either NFC_PT_Polling() or NFC_PT_PollingFull(), may occur several times. */
#define CTS_DATA_TAG_CARD_TYPE                0xDFDB21 /**< card type, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_CARD_INFO                0xDFDB22 /**< card info, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_VAS_RESULT               0xDFDB23 /**< return code of NFC_VAS_Activate(). */
#define CTS_DATA_TAG_VAS_DATA                 0xDFDB24 /**< output data of NFC_VAS_Activate(). */
#define CTS_DATA_TAG_EMV_RESULT               0xDFDB25 /**< return code of EMV_CTLS_ContinueOffline() / EMV_CTLS_SmartReset(). */
#define CTS_DATA_TAG_VAS_DECRYPT_DATA_RESULT  0xDFDB26 /**< return code of TEC (\ref CTS_VAS_DECRYPTION_NOT_REQUIRED, \ref CTS_VAS_DATA_DECRYPTED_OK, \ref CTS_VAS_DATA_DECRYPTED_FAILED, or \ref CTS_VAS_DATA_ENCRYPTED_OK) based on the return responses of NFC_VAS_Activate() and NFC_VAS_Data_Decrypt(). */
#define CTS_DATA_TAG_CARD_TYPE_FULL           0xDFDB27 /**< nfc-card-type-full, 4-byte binary array in big-endian format, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_CARDS_TOTAL_COUNT        0xDFDB28 /**< total number of cards found by nfc, 1 byte binary. */
#define CTS_DATA_TAG_CARDS_A                  0xDFDB29 /**< total number of cards of type A found by nfc, 1 binary byte. */
#define CTS_DATA_TAG_CARDS_B                  0xDFDB2A /**< total number of cards of type B found by nfc, 1 binary byte. */
#define CTS_DATA_TAG_CARDS_F                  0xDFDB2B /**< total number of cards of type F found by nfc, 1 binary byte. */
#define CTS_DATA_TAG_CUSTOM_POLL_RESULT       0xDFDB2C /**< custom-poll-result of either NFC_PT_Polling() or NFC_PT_PollingFull(), n-byte binary array. */
#define CTS_DATA_TAG_CARD_SAK                 0xDFDB2D /**< SAK of the card found by nfc, 1 binary byte, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_CARD_ATQ                 0xDFDB2E /**< ATQ of the card found by nfc, 2-byte binary array, included in \ref CTS_DATA_TAG_CARD. */
#define CTS_DATA_TAG_CARD_RFU                 0xDFDB2F /**< RFU of the card found by nfc, n-byte binary array, included in \ref CTS_DATA_TAG_CARD. */
/** \} */

/**
 * \defgroup TEC_OPTION_TAGS TEC option tags
 * These options can be set by cts_SetOptions().
 * \{ */
#define CTS_OPTION_TAG_APP_ID           0xDFDB40 /**< application ID, passed on to NFC_VAS_Activate(). */
#define CTS_OPTION_TAG_SYNC_CARD_TYPE   0xDFDB41 /**< Card Type for synchroneous (contact) Chip, passed on to crdSync_PowerUp(). 1-byte value.*/
/** \} */


/**
 * Type of function that is called for traces, see cts_SetTraceCallback();
 * \param[in] str : Trace message.
 * \param[in] data : Data pointer provided by the application.
 */
typedef void (* cts_TraceCallback) (const char *str, void *data);

 /**
  * Get version of libtec.
  * \param[out] version : Buffer to store null-terminated version string.
  * \param[in] len : Size of buffer version.
  */
 void cts_Version(char *version, unsigned char len);

 /**
  * Set callback function for trace output.
  * \param[in] cbf : Callback function for trace messages, may be NULL.
  * \param[in] cb_data : Data pointer that is passed on to the callback function cbf, may be NULL.
  */
 void cts_SetTraceCallback(cts_TraceCallback cbf, void *cb_data);


#endif  // avoid double include
