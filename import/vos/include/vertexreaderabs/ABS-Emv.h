/*****************************************************************************/
/* This source code is confidential proprietary information of VeriFone Inc. */
/* and is an unpublished work or authorship protected by the copyright laws  */
/* of the United States. Unauthorized copying or use of this document or the */
/* program contained herein, in original or modified form, is a violation of */
/* Federal and State law.                                                    */
/*                                                                           */
/*                         Verifone Inc.                                     */
/*                        Copyright 2015                                     */
/*****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | ABS-Emv.h
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | EMV Abstraction API defintion
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| VERTEX EMV application interface
;|             |
;|-------------+-------------------------------------------------------------+
;| TYPE:       |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|===========================================================================+
*/

#ifndef __ABS_EMV_H__
#define __ABS_EMV_H__

#ifdef __cplusplus
extern "C"
{
#endif

#undef EmvAbsImportExport
#ifdef VERIX_COMPILE
#ifdef VERIX_EMVABS_LIBRARY
#define EmvAbsImportExport __declspec(dllexport)
#else
#define EmvAbsImportExport __declspec(dllimport)
#endif
#else
#define EmvAbsImportExport
#endif

/// @addtogroup  ABSTRACTION_LAYER Vertex Abstraction Layer
/// @{


//---------------------------------------------------------------------------------
//   Abstraction Layer Definitions
//---------------------------------------------------------------------------------

typedef enum {
    CRD_STATUS_OK = 0,                          ///< Good Completion
    CRD_STATUS_ABSENT,                          ///< Card not present
    CRD_STATUS_PRESENT,                         ///< Card is present
    CRD_STATUS_ACTIVE,                          ///< Card is active
    CRD_STATUS_NOT_ACTIVE,                      ///< Unable to activate
    CRD_STATUS_REMOVED,                         ///< Card was removed
    CRD_STATUS_UNSUPPORTED,                     ///< Card is not supported
    CRD_ERR_EXCHG,                              ///< Error during exchange
    CRD_ERR_MULT_CARDS,                         ///< Mulitple cards in field
    CRD_ERR_READER_ID,                          ///< Invalid Reader Id
    CRD_ERR_POWER,                              ///< Error during activation
    CRD_ERR_INVALID_OPTION,                     ///< Invalid option selected
    CRD_ERR_OPTION_READ_ONLY,                   ///< Object is read only
    CRD_ERR_TIMEOUT,                            ///< Exchanged timed out
    CRD_ERR_INTERNAL_ERROR,                     ///< Failed during attempt to execute
    CRD_ERR_DATA_TOO_LARGE,                     ///< Data length parameter too large
    CRD_ERR_RESP_OVERFLOW,                      ///< Response buffer too small
    CRD_ERR_INSUFFICIENT_POWER,                 ///< Not enough power/battery to perform transactions
    CRD_ERR_INVALID_APDU,                       ///< APDU format is invalid
    CRD_CTLS_NOT_PRESENT,                       ///< CTLS reader not present
    CRD_CONTACT_NOT_PRESENT,                    ///< Contact reader is not present
} CRD_READER_STATUS_t;


///< CRD_Initialize options flags
#define CRD_INIT_LEGACY         0               ///< Initialize the legacy value initializes both CTLS and Contact
#define CRD_INIT_CTLS_RDR       1               ///< Initialize the contactless Reader
#define CRD_INIT_CONTACT_RDR    2               ///< Initialize the contact card reader

///< CRD_DETECT options flags
#define CRD_DET_ACTIVATE        1               ///< check immediate for card present
#define CRD_DET_NO_ACTIVATE     2               ///< check card present but don't activate

///< CRC_Ident return values
#define CRD_IDENT_ERR_PARAM     -1              ///< Invalid parameter
#define CRD_IDENT_ERR_INIT      -2              ///< Readers not yet intialized

///< CRD_PowerStatus return Values
typedef enum {
    CRD_POWER_STATUS_LOW = -3,                  ///< Insufficient Battery level
    CRD_POWER_STATUS_MINIMAL = 0,               ///< Minimal battery for 1 - 10 transactions.
    CRD_POWER_STATUS_NOMINAL,                   ///< Nominal power 10+ transactions
    CRD_POWER_STATUS_EXT_POWER = 100,           ///< External power source.  Unlimited transactions
} CRD_POWER_STATUS_t;


typedef struct STRUCT_CRD_CARD_PROTOCOL {
    unsigned long EMV_Contactless : 1;          ///< Bit Field CTLS - EMV
    unsigned long ISO_14443 : 1;                ///< Bit Field CTLS - ISO_14443
    unsigned long ISO_18092 : 1;                ///< Bit Field CTLS - ISO_18092bit field
    unsigned long Mifare : 1;                   ///< Bit Field CTLS - Mifare
    unsigned long EMV_SmartCard : 1;            ///< Bit Field Contact - EMV
    unsigned long ISO_Smartcard : 1;            ///< Bit Field Contact - ISO
    unsigned long SAM_slots : 1;                ///< Bit Field Contact - SAM Slots Present
    unsigned long ExternalReader : 1;           ///< Bit Field Contact - External Reader
} CRD_CARD_PROTOCOL_t;

typedef struct STRUCT_SW_STRINGS {
    unsigned long readerId;                     ///< Reader Indentifier
    unsigned long pcd;                          ///< PCD identifier
    CRD_CARD_PROTOCOL_t interfaceType;          ///< interfaceType
    unsigned long samSlots;                     ///< Number of SAM Slots (SAM_slots==1)
    unsigned char* emvVersionStr;               ///< Version String for EMV Driver
    unsigned char* osDriverVersionStr;          ///< Version string for the OS Driver
    unsigned char* fwNameStr;                   ///< Firmware Name String
    unsigned char* VendorVersionStr;            ///< Vendor Version String
    unsigned char* VendorNameStr;               ///< Vendor Name String
}CRD_IDENT_t;

// Global list of configuration files
#define CRD_MAX_PCD_SIZE 64
#define CRD_MAX_IFM_SIZE 64
typedef struct
{
    char tcCfgFileName[CRD_MAX_PCD_SIZE];       // name of the configuration file
    char tcCfgPlatform[CRD_MAX_PCD_SIZE];       // platform config file supports.
    char tcCfgPcdName[CRD_MAX_PCD_SIZE];        // PCD Name.
    int  iMatch;                                // set to 1 if config file PCD matches platform PCD
} CRD_PCD_AVAIL_STRUCT_t;

typedef enum ENUM_CRD_PCD_OPERATION {           ///<
    PCD_GET = 0,                                ///< Get the current PCD name
    PCD_SET,                                    ///< Set the current PCD name
}CRD_PCD_OP_t;

typedef enum ENUM_CRD_IFM_OPERATION {           ///<
    IFM_GET = 0,                                ///< Get the current IFM name
    IFM_SET,                                    ///< Set the current IFM name(NOT SUPPORTED)
}CRD_IFM_OP_t;

typedef enum ENUM_CRD_EXCHANGE_STATUS {
    CRD_EXCHG_OK = 0,                           ///< Good Status
    CRD_EXCHG_ERR,                              ///< Error During Exchange
    CRD_EXCHG_BAD_HANDLE,                       ///< Bad Reader Id Provided
    CRD_EXCHG_NOT_POWERED,                      ///< Card not active
    CRD_EXCHG_INVALID_OPTION,                   ///< Invalid Options
    CRD_EXCHG_DATA_TOO_LARGE,                   ///< Request message too large
    CRD_EXCHG_RESP_OVERFLOW,                    ///< Response buffer too small
    CRD_EXCHG_INSUFFICIENT_POWER,               ///< Not enough power/battery to perform transactions
    CRD_EXCHG_INVALID_APDU,                     ///< Bad format for APDU
    CRD_EXCHG_BAD_PARAM                         ///< Invalid/NULL parameter
} CRD_EXCHANGE_STATUS_t;

typedef enum ENUM_CRD_EXCHAGE_OPTIONS {
    CRD_CARDTYPE_A = 0,                         ///< EMV CTLS Card Type A
    CRD_CARDTYPE_B,                             ///< EMV CTLS Card Type B
    CRD_EMV_APDU,                               ///< EMV Type APDU
    CRD_ISO_APDU,                               ///< ISO type APDU
    CRD_VENDOR_APDU                             ///< Vendor specific command
}CRD_EXCHANGE_OPTIONS_t;

typedef enum ENUM_CRD_POWER_OPTIONS {           ///< In CRD_POWER_t
    CRD_VCC_FIELD_OFF = 0,                      ///< Power off card Contact/CTLS
    CRD_ACTIVATE,                               ///< CTLS Field On/Contact Cold Reset
    CRD_STATUS,                                 ///< Activation status Active or not active
    CRD_REMOVE,                                 ///< Prepare card for removal
    CRD_CONTACT_WARM_RESET                      ///< Contact Warm Reset function
}CRD_POWER_OPT_t;

typedef enum ENUM_CRD_CONTACT_STANDARD {        ///< In CRD_POWER_t
    STD_EMV = 0,                                ///< Contact EMV Cold Reset
    STD_ISO,                                    ///< Contact ISO Mode Cold Reset
    STD_VISACASH,                               ///< Contact VISA Cash mode -VISA cash card type
    STD_NO_STANDARD,                            ///< Contact No protocol standard - deviating cards
    STD_NOT_ISO_38400,                          ///< Contact No Standard with 38400 Bit rate (special card)
}CRD_CT_STANDARD;

typedef enum ENUM_CRD_CONTACT_CLASS {           ///< In CRD_POWER_t
    CLASS_TYPE_A = 0,                           ///< 4.5V - 5.5V 60mA
    CLASS_TYPE_B,                               ///< 2.7V - 3.3V 50mA
    CLASS_TYPE_AB,                              ///< Automatic Class support
    VOLT_1v8,                                   ///< 1.8V
}CRD_CT_CLASS_t;

typedef enum ENUM_CRD_PTS_MGT {                 ///< In CRD_POWER_t
    PTS_MANUAL = 0,                             ///< PTS request is initiated by application
    PTS_AUTO,                                   ///< PTS request is automatic by reader (default)
    PTS_NOT_ALLOWED                             ///< PTS not allowed
}CRD_CT_PTS_t;

typedef struct {
    CRD_POWER_OPT_t pwrAction;                  ///< Power action to take
    CRD_CT_STANDARD contact_standard;           ///< Contact card protocol standard for power up
    CRD_CT_CLASS_t  contact_class;              ///< Contact card class management type
    CRD_CT_PTS_t    contact_pts_mode;           ///< Contact PTS management type
    unsigned long   card_remove_timeout;        ///< card remove wait period in microseconds.
}CRD_POWER_t;

///< STRUCT_PPS_REQUEST ProtocolType parameter definition
#define CRD_PROTOCOL_T0 1                       ///< ProtocolType - T=0 protocol selection
#define CRD_PROTOCOL_T1 2                       ///< ProtocolType - T=1 protocol selection
#define CRD_OVERRIDE_ATR 3                      ///< Override ATR to string at atrPtr

///< STRUCT_PPS_REQUEST Selection Flags parameter definition.  Bit ORed flags for each PPS byte
#define CRD_SELECT_PPS1 1                       ///< SelectionFlags - PPS1 byte present
#define CRD_SELECT_PPS2 2                       ///< SelectionFlags - PPS2 byte present
#define CRD_SELECT_PPS3 4                       ///< SelectionFlags - PPS3 byte present

typedef struct STRUCT_PPS_REQUEST {
    unsigned long ProtocolType;                 ///< ProtocolType Set CRD_PROTOCOL_T0, CRD_PROTOCOL_T1
    unsigned char SelectionFlags;               ///< SelectionFlags Bit ORed flags determine which PPS bytes are present
    unsigned char pps1;                         ///< PTS1 PPS1 parameter character
    unsigned char pps2;                         ///< PTS2 PPS2 parameter character
    unsigned char pps3;                         ///< PTS3 PPS2 parameter character
    unsigned char* atrPtr;                      ///< If ProtocolType set to CRD_OVERRIDE_ATR this points to ATR string.
}CRD_PPSREQ_t;

#define CRD_CTLS_NO_TYPE                    0x00000000
#define CRD_CTLS_ISO14443A                  0x00000001
#define CRD_CTLS_ISO14443B                  0x00000002
#define CRD_CTLS_NFC_F212                   0x00000004
#define CRD_CTLS_NFC_F424                   0x00000008
#define CRD_CTLS_MIFARE_1K                  0x00010000
#define CRD_CTLS_MIFARE_UL                  0x00020000
#define CRD_CTLS_MIFARE_AND_ISO14443A       0x00040000
#define CRD_CTLS_MIFARE_4K_AND_ISO14443A    0x00080000
#define CRD_CTLS_MIFARE_4K                  0x00100000
#define CRD_CONTACT_DEVICE                  0xFFFFFFFF



typedef struct {
    char apiName[48];                           ///< API Function Name for shared interface module
} CDR_RDR_API_t;


// Supported Readers
enum
{
    CRD_CTLS_READER = 0,                        ///< Contactless Reader
    CRD_CONTACT_SLOT1,                          ///< Customer facing card slot
    CRD_CONTACT_SLOT2,                          ///< MSAM 1
    CRD_CONTACT_SLOT3,                          ///< MSAM 2
    CRD_CONTACT_SLOT4,                          ///< MSAM 3
    CRD_CONTACT_SLOT5,                          ///< MSAM 4
    CRD_NUM_SUPPORTED_READERS
};


//---------------------------------------------------------------------------------
//   EMV Abstraction API
//---------------------------------------------------------------------------------

//****************************************************************************
/// @brief Identify reader support on this platform
/// Return reader information in the array of strutures pointed to by pRdr.
/// CRD_NUM_SUPPORTED_READERS defines the maximum number of readers supported.
///
/// @param[out] pRdr                - Where reader information is stored
/// @param[in]  iMaxReaders         - is the number of elements in pRdr
///
/// @returns >0                     - Number of readers resident
/// @returns =0                     - None avaiable
/// @returns CRD_IDENT_ERR_PARAM    - Invalid Parameter
/// @returns CRD_IDENT_ERR_INIT     - Must call CRD_Initialize first.
//****************************************************************************
EmvAbsImportExport int CRD_Ident(CRD_IDENT_t* pRdr, int iMaxReaders);

//****************************************************************************
/// @brief Returns the number of readers for CRD_ReadConfig and CRD_Ident
///  functions
///
/// @returns Number of readers resident
//****************************************************************************
EmvAbsImportExport int CRD_NumberConfigs(void);

//****************************************************************************
/// @brief Initialize Card Reader Abstraction
///
/// @param[in] pcPCDname            - string holding PCD Name to install
///
/// @returns CRD_STATUS_OK          - PCD load successful
/// @returns CRD_STATUS_ACTIVE      - Error - PCD already loaded and opened
/// @returns CRD_ERR_INVALID_OPTION - Error - Invalid PCD Name
/// @returns CRD_STATUS_UNSUPPORTED - Error - PCD Selection Not Available
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_SelectPCD(char *pcPCDname);

//****************************************************************************
/// @brief Initialize Card Reader Abstraction
///
/// @param[in] ulOptions            - bit mask initialization flags
///                                 - CRD_INIT_CTLS_RDR = initialize CTLS reader
///                                 - CRD_INIT_CONTACT_RDR = initialize Contact reader
///                                 - CRD_INIT_LEGACY = initialize all readers
/// *Note for legacy support if ulOptions = 0 then both readers are initialized.
///
/// @returns CRD_STATUS_OK          - Reader initialization successful
/// @returns CRD_STATUS_UNSUPPORTED - Error Intializing Reader(s)
/// @returns CRD_CONTACT_NOT_PRESENT - Contact reader is not present
///                                    -or- both readers are not present
/// @returns CRD_CTLS_NOT_PRESENT   - CTLS reader is not present
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_Initialize(unsigned long ulOptions);

//****************************************************************************
/// @brief Close all open readers
///
/// @param None
///
/// @returns None
//****************************************************************************
EmvAbsImportExport void CRD_Shutdown(void);

//****************************************************************************
/// @brief Closes Reader specified in ulOptions.
///
/// @param[in] ulOptions            - bit mask reader close flags
///                                 - CRD_INIT_CTLS_RDR = close CTLS reader
///                                 - CRD_INIT_CONTACT_RDR = close Contact reader
///                                 - CRD_INIT_LEGACY = close all readers
///
//****************************************************************************
EmvAbsImportExport void CRD_CloseReader(unsigned long ulOptions);

//****************************************************************************
/// @brief Return Device present
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Ident
/// @param[in] ulOptions            - Options for Check Present (not currently used)
///
/// @returns CRD_STATUS_ABSENT      - Card Not found
/// @returns CRD_STATUS_PRESENT     - Card found
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INTERNAL_ERROR - Error during detect or activation
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_DevicePresent(unsigned long ulReaderId, unsigned long ulOptions);


//****************************************************************************
/// @brief Detect card holder device
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Ident
/// @param[in] ulOptions            - Options for detect @n
///            CRD_DET_ACTIVATE     - CTLS Leave field active if card not found @n
///            CRD_DET_NO_ACTIVATE  - Deactivate field if card not found @n
/// @param[in]  ulActTimoutUs       - Activation timeout (microseconds to wait for card)
///
/// @returns CRD_STATUS_ABSENT      - No card
/// @returns CRD_STATUS_PRESENT     - Card found
/// @returns CRD_STATUS_ACTIVE      - Card found and active
/// @returns CRD_STATUS_REMOVED     - Card removed
/// @returns CRD_ERR_EXCHG          - Exchange error
/// @returns CRD_ERR_MULT_CARDS     - Multiple cards
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_POWER          - Reader not powered
/// @returns CRD_ERR_TIMEOUT        - Wait for Card Present time out
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_Detect(unsigned long ulReaderId, unsigned long ulOptions, unsigned long ulActTimoutUs);


//****************************************************************************
/// @brief Activate card holder device @n
/// CTLS: @n
/// - If not activated, activate device @n
/// - Perform Remove operation @n
/// - Perform Technology poll on activated Technology @n
/// - Perform AnitCollision @n
/// - Perform Technology Activate (Protocol Activation) - Device ready for APDU Exchange. @n
/// @n
/// Contact: @n
/// - Activate VCC and perform Reset sequence @n
/// - Return Answer To Reset (ATR) from card. @n
/// If previously activated then ATR is returned from prevous Reset. @n
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Open
/// @param[in]  ulOptions           - Not currently Used
/// @param[out] pulCardType         - Card type Activated bit mask returned
///                                     CTLS_TECHTYPE_NONE
///                                     CTLS_TECHTYPE_A
///                                     CTLS_TECHTYPE_B
///                                     CTLS_TECHTYPE_F212
///                                     CTLS_TECHTYPE_F424
///                                     CTLS_TECHTYPE_MIFARE
///                                     CRD_CONTACT_DEVICE
/// @param[out] pucCardInfo         - ATR or CTLS UID info
/// @param[in] pulInfoLength        - Size of CardInfo buffer
/// @param[out] pulInfoLength       - Size of ATR or CTLS UID data
/// @param[in]  ulActTimoutUs       - Activation timeout (microseconds to wait for card)
///
/// @returns CRD_STATUS_ABSENT      - No card
/// @returns CRD_STATUS_PRESENT     - Card was found
/// @returns CRD_STATUS_ACTIVE      - CTLS Card found and active
/// @returns CRD_STATUS_REMOVED     - Card was removed
/// @returns CRD_EXCHG_ERR          - Error executing protocol
/// @returns CRD_TWO_CARDS          - CTLS multiple cards detected
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_POWER          - Error during power up sequence
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t  CRD_Activate(unsigned long ulReaderId, unsigned long ulOptions,
                                             unsigned long* pulCardType, unsigned char* pucCardInfo,
                                             unsigned long* pulInfoLength , unsigned long ulActTimoutUs);


//****************************************************************************
/// @brief Exchange data with card holder device
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Open
/// @param[in] eOptions             - Type of Exchange @n
///                                     CRD_CARDTYPE_A @n
///                                     CRD_CARDTYPE_B @n
///                                     CRD_EMV_APDU @n
///                                     CRD_ISO_APDU @n
///                                     CRD_VENDOR_APDU @n
/// @param[in] usInDataLen          - Length of transmit data_exch_buff
/// @param[in] pucDataIn            - Transmit data pointer
/// @param[out] pusOutDataLen       - (Response Length)
/// @param[out] pucDataOut          - Response is stored at this location
/// @param[out] usDataOutSize       - Data out buffer size.  Maximum response size.
///
/// @returns CRD_EXCHG_OK           - No error
/// @returns CRD_EXCHG_ERR          - Exchange error
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_EXCHG_INVALID_OPTION - Invalid parameter
/// @returns CRD_ERR_POWER          - Error card not powered
//****************************************************************************
EmvAbsImportExport CRD_EXCHANGE_STATUS_t CRD_Exchange(unsigned long ulReaderId, CRD_EXCHANGE_OPTIONS_t eOptions,
                                              unsigned short usInDataLen, unsigned char* pucDataIn,
                                              unsigned short* pusOutDataLen, unsigned char* pucDataOut,
                                              unsigned short usDataOutSize);


//****************************************************************************
/// @brief Present Encrypted verify command using PIN from secure module @n
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open @n
/// @param[in]  eOptions            - Unused @n
/// @param[in]  pucRndData          - Random data pointer @n
/// @param[in]  pucPubKey           - Public Key data pointer @n
/// @param[in]  lPubKeyLen          - Public Key data length @n
/// @param[in]  lExponent           - Algorithm Exponent @n
/// @param[out] pucRespData         - Response Data buffer ponter @n
/// @param[in]  plRespDataLen       - Response buffer size @n
/// @param[out] plRespDataLen       - Response Data length @n
///
/// @returns CRD_EXCHG_OK           - No error @n
/// @returns CRD_EXCHG_ERR          - Exchange error @n
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID @n
/// @returns CRD_EXCHG_INVALID_OPTION - Invalid parameter @n
/// @returns CRD_ERR_POWER          - Error card not powered @n
//****************************************************************************
EmvAbsImportExport CRD_EXCHANGE_STATUS_t CRD_PresentEncryptEmvPin(
   unsigned long            ulReaderId,
   CRD_EXCHANGE_OPTIONS_t   eOptions,
   unsigned char*           pucRndData,
   unsigned char*           pucPubKeyData,
   long                     lPubKeyLen,
   long                     lExponent,
   unsigned char*           pucResponseData,
   long*                    plRespDataLen
   );

//****************************************************************************
/// @brief Present Clear Text verify command using PIN from secure module @n
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open @n
/// @param[in]  eOptions            - Unused @n
/// @param[out] pucRespData         - Response Data buffer ponter @n
/// @param[in]  plRespDataLen       - Response buffer size @n
/// @param[out] plRespDataLen       - Response Data length @n
///
/// @returns CRD_EXCHG_OK           - No error @n
/// @returns CRD_EXCHG_ERR          - Exchange error @n
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID @n
/// @returns CRD_EXCHG_INVALID_OPTION - Invalid parameter @n
/// @returns CRD_ERR_POWER          - Error card not powered @n
//****************************************************************************
EmvAbsImportExport CRD_EXCHANGE_STATUS_t CRD_PresentClearTextPin(
   unsigned long            ulReaderId,
   CRD_EXCHANGE_OPTIONS_t   eOptions,
   unsigned char*           pucResponseData,
   long*                    plRespDataLen
   );

//****************************************************************************
/// @brief Modify power to card holder device
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Initialize
/// @param[in] pOptions             - Power options for reader @n
///     pOPtions->pwrAction         - Power action to carry out @n
///                                     CRD_VCC_FIELD_OFF @n
///                                     CRD_ACTIVATE @n
///                                     CRD_STATUS @n
///                                     CRD_REMOVE: Performs an EMV Picc Removal procedure.@n
///                                         @note If PICC is not removed from the field,
///                                         will return with a timeout status after a
///                                         card_remove_timeout period in pOptions. @n
///                                     CRD_CONTACT_WARM_RESET @n
///     pOptions->contact_standard  - Contact reader protocol standard @n
///                                     STD_EMV @n
///                                     STD_ISO @n
///                                     STD_VISACASH @n
///                                     STD_NO_STANDARD @n
///                                     STD_NOT_ISO_38400 @n
///     pOptions->contact_class     - Contact reader power class @n
///                                     CLASS_TYPE_A @n
///                                     CLASS_TYPE_B @n
///                                     CLASS_TYPE_AB @n
///                                     VOLT_1v8 @n
///     pOptions->contact_pts_mode  - Contact reader PTS negotiaion mode @n
///                                     PTS_MANUAL @n
///                                     PTS_AUTO @n
///                                     PTS_NOT_ALLOWED @n
///
/// @returns CRD_STATUS_OK          - pOption CRD_ACTIVATE, CRD_REMOVE, CRD_VCC_FIELD_OFF: No error
/// @returns CRD_STATUS_ACTIVE      - pOption CRD_STATUS: Reader is active
/// @returns CRD_STATUS_NOT_ACTIVE  - pOption CRD_STATUS: Reader is inactive
/// @returns CRD_EXCHG_ERR          - Exchange error
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INVALID_OPTION - Invalid pOptions parameter
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t  CRD_Power(unsigned long ulReaderId, CRD_POWER_t* pOptions);


//****************************************************************************
/// @brief Request PPS exchange with current active card.
/// If Manual PPS request mode is required the PPS request can be generated
/// using this API.  PPS Request must immediately follow ATR.
///
/// @param[in] ulReaderId           - Reader ID recevied by CRD_Ident
/// @param[in] pPpsParams           - Pointer to parameters needed for PPS exchange
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_ERR_EXCHG          - Protocol error
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t  CRD_CtPpsRequest(unsigned long ulReaderId, CRD_PPSREQ_t* ppsParams);

//****************************************************************************
/// @brief Request Card Activation Information from specified reader.
///
/// Returns device/card information from prior activation
/// Expectation is that CRD_Activate was used.
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open
/// @param[in]  ulOptions           - Not currently Used
/// @param[out] pulCardType         - Report Card type
/// @param[out] pucCardInfo         - ATR or Ctls card info
/// @param[in]  pulInfoLength       - Size of CardInfo buffer
/// @param[out] pulInfoLength       - Size of CardInfo returned data
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_ERR_EXCHG          - Card data not available
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INVALID_OPTION - Invalid parameter
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_GetCardInfo(unsigned long ulReaderId, unsigned long ulOptions,
                                    unsigned long* pulCardType, unsigned char* pucCardInfo,
                                    unsigned long* pulInfoLength);

//****************************************************************************
/// @brief Request Power Status for platform.
///
/// Returns information in the form of number of transaction capablity.
///
/// @returns CRD_POWER_STATUS_LOW   - Insufficient Battery level
/// @returns CRD_POWER_STATUS_MINIMAL - Minimal battery for 1 - 10 transactions.
/// @returns CRD_POWER_STATUS_NOMINAL - Nominal power 10+ transactions
/// @returns CRD_POWER_STATUS_EXT_POWER - External power. Unlimited transactions
//****************************************************************************
EmvAbsImportExport CRD_POWER_STATUS_t CRD_PowerStatus(void);

//****************************************************************************
/// @brief IFM name management for CTLS. (only Get supported)
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open
/// @param[in]  tOperation          - Operation to perform
///                                     IFM_GET
/// @param[in]  pcName              - Unused
/// @param[out] pcName              - pointer to storage for IFM name
/// @param[in]  lNameSize           - size of pcName storage
///                                    Maximum = CRD_MAX_IFM_SIZE
///
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_STATUS_UNSUPPORTED - Not support for requested reader.
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_EXCHG_BAD_HANDLE   - Invalid Reader ID (Verix)
/// @returns CRD_ERR_INVALID_OPTION - Invalid tOperation parameter
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_IfmName(unsigned long ulReaderId, CRD_IFM_OP_t tOperation, char* pcName, long lNameSize);

//****************************************************************************
/// @brief PCD Information
/// Returns PCD SW ID, HW ID, and checksum.
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open
/// @param[in]  pcPcdSwId           - pointer to storage for PCD SW ID storage
/// @param[out] pcPcdHwId           - pointer to storage for PCD HW ID storage
/// @param[in]  pulChecksum         - pointer to checksum storage
/// @param[in]  lSize               - storage size for both SW and HW ID strings
///
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_STATUS_UNSUPPORTED - Not support for requested reader.
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_EXCHG_BAD_HANDLE   - Invalid Reader ID (Verix)
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_PcdInfo(unsigned long ulReaderId
                                                   , char* pcPcdSwId
                                                   , char* pcPcdHwId
                                                   , unsigned long* pulChecksum
                                                   , long lSize);

//****************************************************************************
/// @brief PCD name management for CTLS.
///
/// Set or Get PCD name.
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open
/// @param[in]  tOperation          - Operation to perform
///                                     PCD_GET
///                                     PCD_SET
/// @param[in]  pcName              - pointer to PCD name
/// @param[out] pcName              - pointer to storage for PCD name
/// @param[in]  lNameSize           - size of pcName storage
///                                    Maximum = CRD_MAX_PCD_SIZE
///
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_STATUS_UNSUPPORTED - Not support for requested reader
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_EXCHG_BAD_HANDLE   - Invalid Reader ID (Verix)
/// @returns CRD_ERR_INVALID_OPTION - Invalid tOperation parameter
/// @returns CRD_ERR_DATA_TOO_LARGE - PCD name is larger than size (PCD_GET)
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_PcdName(unsigned long ulReaderId, CRD_PCD_OP_t tOperation, char* pcName, long lNameSize);

//****************************************************************************
/// @brief Get a list of PCDs for CTLS.
///
/// @param[in]  ulReaderId          - Reader ID recevied by CRD_Open
/// @param[in]  ptPcdArray          - pointer to CRD_PCD_AVAIL_STRUCT_t array
/// @param[out] ptPcdArray          - where available PCD data is stored
/// @param[in]  lPcdArraySize       - size of pcName storage
/// @param[out] lPcdArraySize       - number of array elements copied
///
///
/// @returns CRD_STATUS_OK          - No error
/// @returns CRD_STATUS_UNSUPPORTED - Not support for requested reader
/// @returns CRD_ERR_READER_ID      - Invalid Reader ID
/// @returns CRD_ERR_INVALID_OPTION - Parameter is invalid
/// @returns CRD_ERR_DATA_TOO_LARGE - Unable to malloc working buffer
/// @returns CRD_ERR_INTERNAL_ERROR - Unexpected response from CTLS L1
//****************************************************************************
EmvAbsImportExport CRD_READER_STATUS_t CRD_AvailablePcds(unsigned long ulReaderId
                                      , CRD_PCD_AVAIL_STRUCT_t* ptPcdArray
                                      , long* plPcdArraySize);

/// @}

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif
