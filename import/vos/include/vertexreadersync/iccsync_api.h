/** @addtogroup */
/** @{ */
/*****************************************************************************/
/* This source code is confidential proprietary information of VeriFone Inc. */
/* and is an unpublished work or authorship protected by the copyright laws  */
/* of the United States. Unauthorized copying or use of this document or the */
/* program contained herein, in original or modified form, is a violation of */
/* Federal and State law.                                                    */
/*                                                                           */
/*                         Verifone Inc.                                     */
/*                        Copyright 2019                                     */
/*****************************************************************************/

/*
;|===========================================================================+
;| FILE NAME:  | iccsync_api.h
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | Header File for the Synchronous Card API Library
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Contains equates, definitions and function prototypes to
;|             | interface with Synchronous contact cards.
;|-------------+-------------------------------------------------------------+
;| TYPE:       |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|===========================================================================+
*/

#ifndef __ICCSYNC_API_H__
#define __ICCSYNC_API_H__

#ifdef __cplusplus
extern "C" {
#endif

//
// Synchronous Card API Response Code definitions
//
typedef int SYNCRESPCODE;               // Response code type definition

#define SYNCRC_Success               0  // Operation was successful
#define SYNCRC_Failure               1  // Operation failed
#define SYNCRC_Error_Power_Action    2  // Power-Up operation failed
#define SYNCRC_Error_Not_Supported   3  // Synchronous card type not supported
#define SYNCRC_Error_BadParam        4  // A function parameter is invalid
#define SYNCRC_Error_Protocol        5  // Communication error with synchronous card
#define SYNCRC_Error_Mem_Protected   6  // SLE Memory protected
#define SYNCRC_Error_PSC_Required    7  // PSC Validation Required
#define SYNCRC_Error_PSC             8  // PSC Validation Failed

//
// Synchronous Card Types
//
#define SYNCCT_AT24C01SC             1  // Atmel AT24C01SC
#define SYNCCT_AT24C02SC             2  // Atmel AT24C02SC
#define SYNCCT_AT24C04SC             3  // Atmel AT24C04SC
#define SYNCCT_AT24C08SC             4  // Atmel AT24C08SC
#define SYNCCT_AT24C16SC             5  // Atmel AT24C16SC
#define SYNCCT_ST14C02SC             6  // STMicro ST14C02SC
#define SYNCCT_AT24C32SC             7  // Atmel AT24C16SC
#define SYNCCT_AT24C64SC             8  // Atmel AT24C16SC

#define SYNCCT_SLE4418               9  // Siemens SLE4418
#define SYNCCT_SLE4428               10 // Siemens SLE4428

#define SYNCCT_SLE4432               11 // Siemens SLE4432
#define SYNCCT_SLE4442               12 // Siemens SLE4442

//
// PSC usages
//
#define SYNCPSC_NONE                 0 // Card doesn't supports PSC Validation
#define SYNCPSC_TWO_BYTES            2 // Card supports Two bytes PSC Validation
#define SYNCPSC_THREE_BYTES          3 // Card supports Tree bytes PSC Validation
#define SYNCPSC_MAX_NUMBER           4


//****************************************************************************
/// @brief Open the synchronous card driver @n
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SYNCRC_Success - Synchronous card driver opened.
/// @returns SYNCRC_Failure - Error, synchronous card driver is already opened.
//****************************************************************************
SYNCRESPCODE IccSync_Open(unsigned long Options);

//****************************************************************************
/// @brief Close the synchronous card driver @n
///
/// @param[in] Options      - Reserved for future use
///
/// @returns SYNCRC_Success - Synchronous card driver closed.
/// @returns SYNCRC_Failure - Error, synchronous card driver is not opened.
//****************************************************************************
SYNCRESPCODE IccSync_Close(unsigned long Options);


//****************************************************************************
/// @brief Check if a card is inserted in the full-sized card reader.  @n
///
/// @param   None
///
/// @returns SYNCRC_Success - A card is inserted.
/// @returns SYNCRC_Failure - No card inserted.
//****************************************************************************
SYNCRESPCODE IccSync_IsCardPresent();

//****************************************************************************
/// @brief Power up the synchronous card @n
///
/// @param[in]     CardType   - Synchronous card type
/// @param[in/out] AtrBuf     - Pointer to buffer to hold any response from the card (optional)
/// @param[in/out] AtrLen     - Pointer to 8-bit variable to hold length of card response (optional)
///
/// @returns SYNCRC_Success              - No card
/// @returns SYNCRC_Failure              - Error, driver not opened or card not inserted
/// @returns SYNCRC_Error_Power_Action   - Error, unable to power up card
/// @returns SYNCRC_Error_Not_Supported  - Invalid CardType
//****************************************************************************
SYNCRESPCODE IccSync_PowerUp(long           CardType,
                             unsigned char  AtrBuf[],
                             unsigned char *AtrLen);

//****************************************************************************
/// @brief Power down the synchronous card @n
///
/// @param   None
///
/// @returns SYNCRC_Success - Card powered down successfully.
/// @returns SYNCRC_Failure - Error powering down card - not powered up.
//****************************************************************************
SYNCRESPCODE IccSync_PowerDown();

//****************************************************************************
/// @brief Read data from the synchronous card @n
///
/// @param[in]     MemoryAddress    - memory address where to start reading from
///                                   if -1, start at current location
/// @param[in/out] RxBuf            - Pointer to buffer where read data is stored
///                                   Must be large enough to hold 'RxLen' bytes
/// @param[in]     RxLen            - Number of bytes to read from card
///
/// @returns SYNCRC_Success         - Operation successful
/// @returns SYNCRC_Failure         - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam  - Invalid parameter, RxBuf is NULL or undefined CardType
/// @returns SYNCRC_Error_Protocol  - Error communicating with the card - wrong CardType
//****************************************************************************
SYNCRESPCODE IccSync_ReadData(long           MemoryAddress,
                              unsigned char  RxBuf[],
                              unsigned short RxLen);

//****************************************************************************
/// @brief Write data to the synchronous card @n
///
/// @param[in]     MemoryAddress    - memory address where to start reading from
/// @param[in/out] TxBuf            - Pointer to buffer where data to write is stored
/// @param[in]     TxLen            - Number of bytes to write to the card
///
/// @returns SYNCRC_Success         - Operation successful
/// @returns SYNCRC_Failure         - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam  - Invalid parameter, TxLen is zero, or TxBuf is NULL
/// @returns SYNCRC_Error_Protocol  - Error communicating with the card - wrong CardType
//****************************************************************************
SYNCRESPCODE IccSync_WriteData(long                MemoryAddress,
                               const unsigned char TxBuf[],
                               unsigned short      TxLen);

//****************************************************************************
/// @brief Get the NULL-terminated version string for this library @n
///
/// @param[in/out] pVersion        - Pointer to buffer to store version string
/// @param[in]     nSize           - Size of the buffer
///
/// @returns SYNCRC_Success        - Operation successful
/// @returns SYNCRC_Error_BadParam - Invalid parameter, nSize is too small, or pVersion is NULL
//****************************************************************************
SYNCRESPCODE IccSync_GetVersion(char *pVersion,
                                int   nSize);

//****************************************************************************
/// @brief Present Secret code to the synchronous card @n
///
/// @param[in] PinCode                  - Pointer to buffer where Pin Code is stored
/// @param[in] nSize                    - PinCode size
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, nSize is wrong, or PinCode is NULL
/// @returns SYNCRC_Error_Protocol      - Error communicating with the card
//****************************************************************************
SYNCRESPCODE IccSync_PresentSecretCode(const unsigned char PinCode[],
                                       unsigned short      nSize);

//****************************************************************************
/// @brief Read Security Memory from synchronous card @n
///
/// @param[in/out] RxBuff                - Pointer to buffer where Pin Code is stored
/// @param[in/out] SecLen                - Pin Code length returned
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, TxLen is zero, or TxBuf is NULL
/// @returns SYNCRC_Error_Protocol      - Error communicating with the card
//****************************************************************************
SYNCRESPCODE IccSync_ReadSecurityMem(unsigned char RxBuff[],
                                     unsigned char  * BufLen);

//****************************************************************************
/// @brief Update PIN Code in synchronous card @n
///
/// @param[in/out]TxBuf                 - Pointer to buffer where Pin Code is stored
/// @param[in] TxLen                    - Number of bytes to write to the card
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, TxLen is zero, or TxBuf is NULL
/// @returns SYNCRC_Error_Protocol      - Error communicating with the card
//****************************************************************************
SYNCRESPCODE IccSync_WriteSecurityMem(const unsigned char TxBuf[],
                                      unsigned short TxLen);

//****************************************************************************
/// @brief Read Protection Memory from synchronous card @n
///
/// @param[out]RxBuf                    - Pointer to buffer where Pin Code is stored
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, RxBuf is NULL
//****************************************************************************
SYNCRESPCODE IccSync_ReadProtectionMem(unsigned char RxBuf[]);

//****************************************************************************
/// @brief Write Protection Memory into synchronous card @n
///
/// @param[in] TxBuf                    - Pointer to buffer where Pin Code is stored
/// @param[in] TxLen                    - Number of bytes to write to the card
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, TxLen is zero, or TxBuf is NULL
/// @returns SYNCRC_Error_Protocol      - Error communicating with the card
//****************************************************************************
SYNCRESPCODE IccSync_WriteProtectionMem(unsigned short MemoryAddress,
                                        const unsigned char TxBuf[],
                                        unsigned short TxLen);

//****************************************************************************
/// @brief Read data with protection bit from SLE synchronous card @n
///
///
/// @param[in]     MemoryAddress        - memory address where to start reading from
///                                       if -1, start at current location
/// @param[out]    RxBuf                - Pointer to buffer where read data is stored
///                                       Must be large enough to hold 'RxLen' bytes
/// @param[in]     TxLen                - Number of bytes to protect
///                                       if 0, operation effectively sets the current address
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, RxLen is zero, or RxBuf is NULL
/// @returns SYNCRC_Error_Protocol      - Error communicating with the card
//****************************************************************************
SYNCRESPCODE IccSync_ReadWithProtectionBit(long MemoryAddress,
                                           unsigned char  RxBuf[],
                                           unsigned short RxLen);

//****************************************************************************
/// @brief Write data with protection bit to SLE synchronous card @n
///
///
/// @param[in]     MemoryAddress        - memory address where to start reading from
///                                       if -1, start at current location
/// @param[in]     TxBuf                - Pointer to buffer where read data is stored
///                                       Must be large enough to hold 'RxLen' bytes
/// @param[in]     TxLen                - Number of bytes to protect
///
/// @returns SYNCRC_Success             - Operation successful
/// @returns SYNCRC_Failure             - Error - driver not opened, card not inserted or powered up
/// @returns SYNCRC_Error_BadParam      - Invalid parameter, RxLen is zero, or RxBuf is NULL
/// @returns SYNCRC_Error_Protocol      - Error communicating with the card
/// @returns SYNCRC_Error_Not_Supported - Bad Card Type
//****************************************************************************
SYNCRESPCODE IccSync_WriteWithProtectionBit(long MemoryAddress,
                                            unsigned char  TxBuf[],
                                            unsigned short TxLen);




#ifdef __cplusplus
}
#endif

#endif // __ICCSYNC_API_H__
/// @} */
