#ifndef __FPS_H__
#define __FPS_H__

#include <stdint.h>
#include <string>
#include <vector>

#if (defined _VRXEVO || defined _WIN32)
#  if defined VFI_FPS_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_FPS_STATIC_EXPORT || defined _WIN32 // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_FPS_SHARED_EXPORT
#  define DllSpec __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

#if defined VRX_IMPORT
#  define DllSpec __declspec(dllimport)
#endif

namespace vfifps
{
  /** enumeration of result types
   */
  enum FPS_RES_Type
  {
    /** function was successful */
    FPS_RES_SUCCESS = 0,
    /** function returned with failure */
    FPS_RES_FAILURE = -1,
    /** function returned with no such property */
    FPS_RES_NO_PROP = -2,
    /** function returned fpsInfo not read */
    FPS_RES_INFO_NOT_READ = -3
  };

  /** enumeration of callback types
   */
  enum FPS_CB_Type
  {
    /** data read successfully */
    FPS_CB_DATA,
    /** error occurred, see details in the data vector of the callback */
    FPS_CB_ERROR,
    /** backward movement */
    FPS_CB_INFO_BACKWARD_MOVEMENT,
    /** bad quality, try again */
    FPS_CB_INFO_BAD_QUALITY,
    /** calib start */
    FPS_CB_INFO_CALIB_START,
    /** center and press harder */
    FPS_CB_INFO_CENTER_AND_PRESS_HARDER,
    /** clean the sensor */
    FPS_CB_INFO_CLEAN_SENSOR,
    /** consolidation fail */
    FPS_CB_INFO_CONSOLIDATION_FAIL,
    /** consolidation succeed */
    FPS_CB_INFO_CONSOLIDATION_SUCCEED,
    /** enroll start */
    FPS_CB_INFO_ENROLL_START,
    /** enroll progress */
    FPS_CB_INFO_ENROLL_PROGRESS,
    /** detect start */
    FPS_CB_INFO_FINGER_DETECT_START,
    /** position finger : exceeding left boundaries */
    FPS_CB_INFO_FINGERLEFT,
    /** position finger : exceeding right boundaries */
    FPS_CB_INFO_FINGERRIGHT,
    /** position finger : exceeding top boundaries */
    FPS_CB_INFO_FINGERTOP,
    /** position finger : exceeding bottom boundaries */
    FPS_CB_INFO_FINGERBOTTOM,
    /** good image */
    FPS_CB_INFO_GOOD_IMAGE,
    /** gui finish */
    FPS_CB_INFO_GUI_FINISH,
    /** gui finish detect start */
    FPS_CB_INFO_GUI_FINISH_DETECT_START,
    /** gui finish fail*/
    FPS_CB_INFO_GUI_FINISH_FAIL,
    /** gui finished succeed */
    FPS_CB_INFO_GUI_FINISH_SUCCEED,
    /** image processed */
    FPS_CB_INFO_IMAGE_PROCESSED,
    /** joint detected */
    FPS_CB_INFO_JOINT_DETECTED,
    /** keep finger on sensor */
    FPS_CB_INFO_KEEP_FINGER,
    /** no finger */
    FPS_CB_INFO_NO_FINGER,
    /** processing image */
    FPS_CB_INFO_PROCESSING_IMAGE,
    /** put finger number 1 */
    FPS_CB_INFO_PUT_FINGER,
    /** put finger number 2 */
    FPS_CB_INFO_PUT_FINGER2,
    /** put finger number 3 */
    FPS_CB_INFO_PUT_FINGER3,
    /** put finger number 4 */
    FPS_CB_INFO_PUT_FINGER4,
    /** put finger number 5 */
    FPS_CB_INFO_PUT_FINGER5,
    /** remove finger */
    FPS_CB_INFO_REMOVE_FINGER,
    /** start */
    FPS_CB_INFO_START,
    /** swipe in progress */
    FPS_CB_INFO_SWIPE_IN_PROGRESS,
    /** image too dark */
    FPS_CB_INFO_TOO_DARK,
    /** too dry */
    FPS_CB_INFO_TOO_DRY,
    /** too fast */
    FPS_CB_INFO_TOO_FAST,
    /** too hight */
    FPS_CB_INFO_TOO_HIGH,
    /** too left */
    FPS_CB_INFO_TOO_LEFT,
    /** image too light */
    FPS_CB_INFO_TOO_LIGHT,
    /** too low */
    FPS_CB_INFO_TOO_LOW,
    /** too right */
    FPS_CB_INFO_TOO_RIGHT,
    /** too short */
    FPS_CB_INFO_TOO_SHORT,
    /** too skewed */
    FPS_CB_INFO_TOO_SKEWED,
    /** too small */
    FPS_CB_INFO_TOO_SMALL,
    /** too strange */
    FPS_CB_INFO_TOO_STRANGE,
    /** touch sensor */
    FPS_CB_INFO_TOUCH_SENSOR,
    /** verify start */
    FPS_CB_INFO_VERIFY_START,
    /** picture in raw data format */
    FPS_CB_PICTURE,
    /** picture in ansi format */
    FPS_CB_ANSI_DATA,
    /** picture in iso format */
    FPS_CB_ISO_DATA,
    /** picture in pgm format */
    FPS_CB_PGM_DATA
  };

  /** enumeration of integer properties
   */
  enum FPS_PROP_INT_Type
  {
    /** property of version informations */
    FPS_PROP_INT_VERSION,
    /** property of general informations */
    FPS_PROP_INT_INFO,
    /** property of minimal next version */
    FPS_PROP_INT_MINNEXTVERSION,
    /** property of variant */
    FPS_PROP_INT_VARIANT,
    /** property of functionality */
    FPS_PROP_INT_FUNCTIONALITY,
    /** property of configuration */
    FPS_PROP_INT_CONFIG,
    /** property of id */
    FPS_PROP_INT_ID,
    /** property of authentify id */
    FPS_PROP_INT_AUTHENTIFYID,
    /** property of usage */
    FPS_PROP_INT_USAGE,
    /** property of sensortype */
    FPS_PROP_INT_SENSORTYPE,
    /** property of max image width */
    FPS_PROP_INT_IMAGEWIDTH,
    /** property of max image height */
    FPS_PROP_INT_IMAGEHEIGHT,
    /** property of max grab window */
    FPS_PROP_INT_MAXGRABWINDOW,
    /** property of companion vendor code */
    FPS_PROP_INT_COMPANIONVENDORCODE,
    /** property of version extension */
    FPS_PROP_INT_VERSIONEXT,
    /** property of system id */
    FPS_PROP_INT_SYSTEMID,
    /** property of datatype */
    FPS_PROP_INT_DATATYPE,
    /** property of last image quality */
    FPS_PROP_INT_IMAGE_QUALITY_LAST,
    /** property of number of minutiae in last scan */
    FPS_PROP_INT_IMAGE_NUMBER_OF_MINUTIAE_LAST,
    /** property of image size for iso/ansi */
    FPS_PROP_INT_IMAGE_SIZE
  };

  /** enumeration of string properties */
  enum FPS_PROP_STRING_Type
  {
    /** f.e. extended info from sensor in json format */
    FPS_PROP_STRING_EXTINFO,
    /** f.e. raw data from sensor in json format */
    FPS_PROP_STRING_INFO,
    /** f.e. sensor type */
    FPS_PROP_STRING_TYPE,
    /** f.e. extended informations of used hardware */
    FPS_PROP_STRING_HWINFO,
    /** f.e. guid delivered by sensor */
    FPS_PROP_STRING_GUID,
    /** f.e. system name delivered by sensor */
    FPS_PROP_STRING_SYSTEMNAME
  };

  /** enumeration of desired image type, standard is RAW */
  enum FPS_IMG_Type
  {
    /** Image will be scanned in RAW format of sensor */
    FPS_IMG_RAW,
    /** Image will be converted to PGM format */
    FPS_IMG_PGM,
    /** Image will be converted to ANSI format */
    FPS_IMG_ANSI,
    /** Image will be converted to ISO format */
    FPS_IMG_ISO,
    /** All image formats will be delivered in callback */
    FPS_IMG_ALL
  };

  /** callback method of capture_start
   *  @param dataptr - reference to sender
   *  @param msgType - identifies the kind of callback data
   *  @param width - information about the picture width
   *  @param rawData - raw data of the message delivered as vector of unsigned chars
   */
  typedef void (*resultCallbackType)(void *dataptr, FPS_CB_Type msgType, int width, std::vector<unsigned char> rawData);


  // normal functions


  /** open connection to fingerprint sensor
   *  @param device - device identifier to open. If empty automatic device detection.
   *  @result handle to opened finger print sensor
   */
  DllSpec int16_t fps_open(std::string &device);

  /** close connection to fingerprint sensor
   *  @param handle - handle to fingerprint sensor object
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_close(int16_t handle);

  /** read property of integer type from fingerprint sensor
   *  @param handle - handle to fingerprint sensor object
   *  @param prop - integer property to read
   *  @param propValue - out variable where the read result is stored
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_getPropInt(int16_t handle, FPS_PROP_INT_Type prop, int &propValue);

  /** read property of string type from fingerprint sensor
   *  @param handle - handle to fingerprint sensor object
   *  @param prop - string property to read
   *  @param propValue - out variable where the read result is stored
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_getPropString(int16_t handle, FPS_PROP_STRING_Type prop, std::string &propValue);

  /** write property of integer type to fingerprint sensor
   *  @param handle - handle to fingerprint sensor object
   *  @param prop - integer property to write
   *  @param newPropValue - in variable where the data to write is stored
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_setPropInt(int16_t handle, FPS_PROP_INT_Type prop, int &newPropValue);

  /** write property of string type from fingerprint sensor
   *  @param handle - handle to fingerprint sensor object
   *  @param prop - string property to write
   *  @param newPropValue - in variable where the data to write is stored
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_setPropString(int16_t handle, FPS_PROP_STRING_Type prop, std::string &newPropValue);

  /** start captureing fingerprint from device (asyncron). Install a callback that indicates successful read, errors or user information.
   *  @param handle - handle to fingerprint sensor object
   *  @param cb - callback for user instruction, results
   *  @param dataptr - ptr to datacontext of caller
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_capture_start(int16_t handle, resultCallbackType cb, void *dataptr);

  /** stop captureing fingerprint from device
   *  @param handle - handle to fingerprint sensor object
   *  @result success indicator cf. FPS_RES_Type
   */
  DllSpec FPS_RES_Type fps_capture_stop(int16_t handle);

  /**
   * get version of adk library
   */
  DllSpec std::string fps_GetVersion();
};

#endif // __FPS_H__
