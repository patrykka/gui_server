// -*- Mode: C++; -*-
#ifndef GUI_H_2013_03_14
#define GUI_H_2013_03_14

/** \file gui.h
 * \addtogroup vfigui Graphical User Interface
 * \{
 */

#include <stdarg.h>
#include <pthread.h>
#include "jsobject.h"
#include "timestamp.h"
#include "gui_error.h"
#include "types.h"

#if defined _WIN32 && defined VFI_GUIPRT_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#elif defined __GNUC__ && defined VFI_GUIPRT_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

/** Verifone GUI namespace */
#ifndef DOXYGEN
namespace vfigui {
#endif
#if 0
} // just to satisfy automatic indentation of the editor
#endif

using namespace vfihtml; // import common interfaces

/** \section gui_overloading Parameter Overloading
 * Most functions take a \c display and \c region_id parameter. For convenience these functions
 * are overloaded so that these parameters may be omitted when using the default display
 * and region:
 *   - When the default display is to be used, \c display may be omitted
 *   - When the default region on the default display is to be used \c display and \c region_id
 *     may be omitted.
 *
 * If the key-value map is not required for a specific purpose, an overloaded version
 * is provided so that the key-value map can be omitted.
 */

/** callback function that is called at regular intervals while the dialog is displayed.
 * If the callback returns false, the dialog is cancelled;
 * \param[in] data data pointer provided by the application
 */
typedef bool (*uiCallback)(void *data);

/** Reason why the callback was invoked */
enum UICBType { UI_CB_RESULT,     /**< callback was invoked due to receiving the result */
                UI_CB_UPDATE,     /**< callback was invoked due to receiving an update event */
                UI_CB_LOAD,       /**< callback was invoked due to switching to a new dialog */
                UI_CB_ERROR_LIST, /**< callback was invoked due to receiving error data */
                UI_CB_STATISTICS, /**< callback was invoked due to receiving statistics data */
                UI_CB_EVENT       /**< callback was invoked due to receiving a custom event */
};

struct UIErrorEntry;
struct UIStatistics;

/** class for accessing data of the current transaction */
class UICBData {
 public:

   /** Destructor */
   virtual ~UICBData() {}

   /** \return result result code of the dialog or event code in case of custom event */
   virtual int &result()=0;

   /** \return transaction ID */
   virtual int txn_id()=0;

   /** \return key value map containing data received from the dialog */
   virtual stringmap &value()=0;

   /** \return url of the dialog as provided by the application. */
   virtual std::string &url()=0;

   /** \return current status of error list */
   virtual std::vector<UIErrorEntry> &error()=0;

   /** \return script error message */
   virtual std::string &script_error()=0;

   /** \return statistics data */
   virtual UIStatistics &statistics()=0;

};


/** callback function that is called when a dialog has returned some data. In case of dialog sequences
 * (e.g. a dialog uses the load-URL action) the callback is invoked after each step of the sequence.
 * \param[in] data data pointer provided by the application
 * \param[in] type type / reason why the callback was invoked
 * \param[in] uidata object to access data of the current transaction
 * \note The callback will be run within a different thread context, so make sure to use appropriate
 *  locking if required. All callback functions share the same thread. Therefore, do not use functions
 *  that block for a long time, in particular uiInvokeWait() must not be called.
 */
typedef void (*uiAsyncCallback)(void *data, UICBType type, UICBData &uidata);

/** structure describing the position and size of an output region
 */
struct UIRegion {
   int id; /**< region ID, 0 is the default region */
   int left;   /**< left position in pixels (+=width if negative) */
   int top;    /**< top position in pixels (+=height if negative) */
   int right;  /**< right position in pixels (+=width if negative) */
   int bottom; /**< bottom position in pixels (+=height if negative) */
   int flags;  /**< flags: UI_REGION_FLAG_HIDDEN */
};

enum { UI_REGION_DEFAULT=0      /**< default region ID */
};

enum {
   UI_REGION_FLAG_HIDDEN=1         /**< do not show region: Hiding a region does not terminate the contained dialog */

};


/** set the number of attempts to contact the server. After a failed attempt the library waits for about 1s.
 * \param[in] num number of attempts, if 0 use an infinite number of attempts (default: 30).
 * \note This takes effect on the next attempt to connect to the server, therefore, it should be invoked before any other function.
 */
DllSpec void uiSetConnectAttempts(int num);

/** get the number of attempts to contact the server
 * \return Number of attempts
 */
DllSpec int uiGetConnectAttempts();

/** callback function that is called when the contact to a server is lost. It runs in parallel
 * to UI functions that will return UI_ERR_CONNECTION_LOST so the order of returning the error and
 * invocation of the callback is unspecified.
 * \param[in] display display number
 */
typedef void (*uiDisconnectCallback)(int display);

/** set callback that gets invoked if the contact to the server is lost
 * \param[in] cb callback function or NULL
 * .
 * The callback is invoked with the display number.
 */
DllSpec void uiSetDisconnectCallback(uiDisconnectCallback cb);

/** get callback that gets invoked if the contact to the server is lost
 * \return callback function as set with uiSetDisconnectCallback();
 */
DllSpec uiDisconnectCallback uiGetDisconnectCallback();


/** perform printf-like formatting.
 * \param[in] format printf-like format string. It supports the commonly known format specifiers
 *                   's', 'i', 'd', 'u', 'o', 'x', 'X', 'p', 'c', 'f', 'e', 'g'. In addition the
 *                   following format specifiers are supported:
 *                     - 'S': format as string and substitute the special HTML characters '&', '<', '>',
 *                       '&quot;' and '\'' by character references (e.g. '&amp;amp;', '&amp;lt;' ...).
 *                     - 'C': format as character and sustitute HTML characters
 * \return formatted string */
DllSpec std::string uiPrint(const char *format, ...);

/** perfom printf-like formatting. This is the same as uiPrint() just taking a va_list
 * instead of a variable number of arguments */
DllSpec std::string uiPrintV(const char *format, va_list va);


/** determine the number of displays
 * \return number of displays
 */
DllSpec int uiDisplayCount();


/** Display an HTML document on screen
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \return error code (see ::UIError)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 */
DllSpec int uiDisplay(int display, int region_id, const std::string &text);

DllSpec int uiDisplay(int region_id, const std::string &text);

inline int uiDisplay(const std::string &text) { return uiDisplay(0,UI_REGION_DEFAULT,text); }


/** Invoke an HTML dialog.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in,out] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing. On return \a value contains the updated input values.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or return value specified in the HTML fragment
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 */
DllSpec int uiInvoke(int display, int region_id, stringmap &value, const std::string &text, uiCallback cb=0, void *cbdata=0);

DllSpec int uiInvoke(int region_id, stringmap &value, const std::string &text, uiCallback cb=0, void *cbdata=0);

inline int uiInvoke(stringmap &value, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ return uiInvoke(0,UI_REGION_DEFAULT,value,text,cb,cbdata); }

inline int uiInvoke(int display, int region_id, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvoke(display, region_id,value,text,cb,cbdata); }

inline int uiInvoke(int region_id, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvoke(0,region_id,value,text,cb,cbdata); }

inline int uiInvoke(const std::string &text, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvoke(0,UI_REGION_DEFAULT,value,text,cb,cbdata); }

/** Invoke an HTML dialog with template: The provided text is inserted into the template file and then displayed.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in,out] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing. On return \a value contains the updated input values.
 * \param[in] template_name file name of template into which \a text is inserted
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or return value specified in the HTML fragment
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 */
DllSpec int uiInvokeTemplate(int display, int region_id, stringmap &value, const std::string &template_name, const std::string &text, uiCallback cb=0, void *cbdata=0);

DllSpec int uiInvokeTemplate(int region_id, stringmap &value, const std::string &template_name, const std::string &text, uiCallback cb=0, void *cbdata=0);

inline int uiInvokeTemplate(stringmap &value, const std::string &template_name, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ return uiInvokeTemplate(0,UI_REGION_DEFAULT,value,template_name,text,cb,cbdata); }

inline int uiInvokeTemplate(int display, int region_id, const std::string &template_name, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeTemplate(display,region_id,value,template_name,text,cb,cbdata); }

inline int uiInvokeTemplate(int region_id, const std::string &template_name, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeTemplate(0,region_id,value,template_name,text,cb,cbdata); }

inline int uiInvokeTemplate(const std::string &template_name, const std::string &text, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeTemplate(0,UI_REGION_DEFAULT,value,template_name,text,cb,cbdata); }


/** display a dialog
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in,out] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing. On return \a value contains the updated input values.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX)
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or return value specified in the HTML fragment
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 */
DllSpec int uiInvokeURL(int display, int region_id, stringmap &value, const char *url, uiCallback cb=0, void *cbdata=0);

/** shortcut for uiInvokeURL(0,region_id,value,url,cb,cbdata); */
DllSpec int uiInvokeURL(int region_id, stringmap &value, const char *url, uiCallback cb=0, void *cbdata=0);

/** shortcut for uiInvokeURL(0,UI_REGION_DEFAULT,value,url,cb,cbdata); */
inline int uiInvokeURL(stringmap &value, const char *url, uiCallback cb=0, void *cbdata=0)
{ return uiInvokeURL(0,UI_REGION_DEFAULT,value,url,cb,cbdata); }

/** shortcut for uiInvokeURL(display, region_id, dummy_value,url,cb,cbdata); */
inline int uiInvokeURL(int display, int region_id, const char *url, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeURL(display, region_id,value,url,cb,cbdata); }

/** shortcut for uiInvokeURL(0,region_id,dummy_value,url,cb,cbdata); */
inline int uiInvokeURL(int region_id, const char *url, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeURL(0,region_id,value,url,cb,cbdata); }

/** shortcut for uiInvokeURL(0,UI_REGION_DEFAULT,dummy_value,url,cb,cbdata); */
inline int uiInvokeURL(const char *url, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeURL(0,UI_REGION_DEFAULT,value,url,cb,cbdata); }


/** display a dialog
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in,out] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing. On return \a value contains the updated input values.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX)
 * \param[in] cb optional callback function
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code (see ::UIError) or return value specified in the HTML fragment
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 */
DllSpec int uiInvokeURL(int display, int region_id, stringmap &value, const std::string &url, uiCallback cb=0, void *cbdata=0);

/** shortcut for iInvokeURL(0,region_id,value,url,cb,cbdata); */
inline int uiInvokeURL(int region_id, stringmap &value, const std::string &url, uiCallback cb=0, void *cbdata=0)
{ return uiInvokeURL(0,region_id,value,url,cb,cbdata); }

/** shortcut for iInvokeURL(0,UI_REGION_DEFAULT,value,url,cb,cbdata); */
inline int uiInvokeURL(stringmap &value, const std::string &url, uiCallback cb=0, void *cbdata=0)
{ return uiInvokeURL(0,UI_REGION_DEFAULT,value,url,cb,cbdata); }

/** shortcut for uiInvokeURL(display,region_id,dummy_value,url,cb,cbdata); */
inline int uiInvokeURL(int display, int region_id, const std::string &url, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeURL(display,region_id,value,url,cb,cbdata); }

/** shortcut for uiInvokeURL(0,region_id,dummy_value,url,cb,cbdata); */
inline int uiInvokeURL(int region_id, const std::string &url, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeURL(0,region_id,value,url,cb,cbdata); }

/** shortcut for uiInvokeURL(0,UI_REGION_DEFAULT, dummy_value,url,cb,cbdata); */
inline int uiInvokeURL(const std::string &url, uiCallback cb=0, void *cbdata=0)
{ stringmap value; return uiInvokeURL(0,UI_REGION_DEFAULT, value,url,cb,cbdata); }


/** Asynchronously invoke an HTML dialog.
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return error code (see ::UIError) or transaction ID. The transaction ID is required for obtaining the dialog result using uiInvokeWait()
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 * \note If no callback was provided, uiInvokeWait() must be called to obtain the result and release allocated memory or uiInvokeCancel() needs to be called to cancel the dialog. If a callback was provided uiInvokeWait() cannot be used and will return UI_ERR_INVALID.
 */
DllSpec int uiInvokeAsync(int display, int region_id, const stringmap &value, const std::string &text, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeAsync(0,region_id,value,text,cb,data); */
DllSpec int uiInvokeAsync(int region_id, const stringmap &value, const std::string &text, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeAsync(0,UI_REGION_DEFAULT, value, text,cb,data); */
inline int uiInvokeAsync(const stringmap &value, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ return uiInvokeAsync(0,UI_REGION_DEFAULT, value, text,cb,data); }

/** shortcut for uiInvokeAsync(display, region_id,dummy_value,text,cb,data);*/
inline int uiInvokeAsync(int display, int region_id, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeAsync(display, region_id,value,text,cb,data); }

/** shortcut for uiInvokeAsync(0,region_id,dummy_value,text,cb,data); */
inline int uiInvokeAsync(int region_id, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeAsync(0,region_id,value,text,cb,data); }

/** shortcut for uiInvokeAsync(0,UI_REGION_DEFAULT, dummy_value, text,cb,data); */
inline int uiInvokeAsync(const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeAsync(0,UI_REGION_DEFAULT, value, text,cb,data); }

/** Asynchronously invoke an HTML dialog. The provided text is inserted into the template file and then the dialog is displayed
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] template_name template file
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return error code (see ::UIError) or transaction ID. The transaction ID is required for obtaining the dialog result using uiInvokeWait()
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 * \note If no callback was provided, uiInvokeWait() must be called to obtain the result and release allocated memory or uiInvokeCancel() needs to be called to cancel the dialog. If a callback was provided uiInvokeWait() cannot be used and will return UI_ERR_INVALID.
 */
DllSpec int uiInvokeTemplateAsync(int display, int region_id, const stringmap &value, const std::string &template_name, const std::string &text, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeTemplateAsync(0,region_id,value,template_name,text,cb,data); */
DllSpec int uiInvokeTemplateAsync(int region_id, const stringmap &value, const std::string &template_name, const std::string &text, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeTemplateAsync(0, UI_REGION_DEFAULT, value, template_name, text, cb,data); */
inline int uiInvokeTemplateAsync(const stringmap &value, const std::string &template_name, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ return uiInvokeTemplateAsync(0, UI_REGION_DEFAULT, value, template_name, text, cb,data); }

/** shortcut for uiInvokeTemplateAsync(display,region_id,dummy_value,template_name,text, cb,data); */
inline int uiInvokeTemplateAsync(int display, int region_id, const std::string &template_name, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeTemplateAsync(display,region_id,value,template_name,text, cb,data); }

/** shortcut for uiInvokeTemplateAsync(0,region_id,dummy_value,template_name,text, cb,data); */
inline int uiInvokeTemplateAsync(int region_id, const std::string &template_name, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeTemplateAsync(0,region_id,value,template_name,text, cb,data); }

/** shortcut for uiInvokeTemplateAsync(0,UI_REGION_DEFAULT, dummy_value, template_name, text, cb,data); */
inline int uiInvokeTemplateAsync(const std::string &template_name, const std::string &text, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeTemplateAsync(0,UI_REGION_DEFAULT, value, template_name, text, cb,data); }


/** Asynchronously invoke an HTML dialog
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX)
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return error code (see ::UIError) or transaction ID. The transaction ID is required for obtaining the dialog result using uiInvokeWait()
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 * \note If no callback was provided, uiInvokeWait() must be called to obtain the result and release allocated memory or uiInvokeCancel() needs to be called to cancel the dialog. If a callback was provided uiInvokeWait() cannot be used and will return UI_ERR_INVALID.
 */
DllSpec int uiInvokeURLAsync(int display, int region_id, const stringmap &value, const char *url, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeURLAsync(0,region_id,value,url,cb,data); */
DllSpec int uiInvokeURLAsync(int region_id, const stringmap &value, const char *url, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeURLAsync(0, UI_REGION_DEFAULT, value, url,cb,data); */
inline int uiInvokeURLAsync(const stringmap &value, const char *url, uiAsyncCallback cb=0, void *data=0)
{ return uiInvokeURLAsync(0, UI_REGION_DEFAULT, value, url,cb,data); }

/** shortcut for uiInvokeURLAsync(display, region_id,dummy_value,url,cb,data); */
inline int uiInvokeURLAsync(int display, int region_id,const char *url, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeURLAsync(display, region_id,value,url,cb,data); }

/** shortcut for uiInvokeURLAsync(0,region_id,dummy_value,url,cb,data); */
inline int uiInvokeURLAsync(int region_id,const char *url, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeURLAsync(0,region_id,value,url,cb,data); }

/** shortcut for uiInvokeURLAsync(0, UI_REGION_DEFAULT, dummy_value, url,cb,data); */
inline int uiInvokeURLAsync(const char *url, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeURLAsync(0, UI_REGION_DEFAULT, value, url,cb,data); }


/** Asynchronously invoke an HTML dialog
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX)
 * \param[in] cb optional callback function that is called when the dialog has returned
 * \param[in] data data pointer that is passed on to the callback function
 * \return error code (see ::UIError) or transaction ID. The transaction ID is required for obtaining the dialog result using uiInvokeWait()
 * \note \a display, \a region_id and \a value are optional, see \ref gui_overloading
 * \note If no callback was provided, uiInvokeWait() must be called to obtain the result and release allocated memory or uiInvokeCancel() needs to be called to cancel the dialog. If a callback was provided uiInvokeWait() cannot be used and will return UI_ERR_INVALID.
 */
DllSpec int uiInvokeURLAsync(int display, int region_id, const stringmap &value, const std::string &url, uiAsyncCallback cb=0, void *data=0);

/** shortcut for uiInvokeURLAsync(0, region_id, value, url,cb,data); */
inline int uiInvokeURLAsync(int region_id, const stringmap &value, const std::string &url, uiAsyncCallback cb=0, void *data=0)
{ return uiInvokeURLAsync(0, region_id, value, url,cb,data); }

/** shortcut for uiInvokeURLAsync(0, UI_REGION_DEFAULT, value, url,cb,data); */
inline int uiInvokeURLAsync(const stringmap &value, const std::string &url, uiAsyncCallback cb=0, void *data=0)
{ return uiInvokeURLAsync(0, UI_REGION_DEFAULT, value, url,cb,data); }

/** shortcut for uiInvokeURLAsync(display, region_id,dummy_value,url,cb,data); */
inline int uiInvokeURLAsync(int display, int region_id,const std::string &url, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeURLAsync(display, region_id,value,url,cb,data); }

/** shortcut for uiInvokeURLAsync(0,region_id,dummy_value,url,cb,data); */
inline int uiInvokeURLAsync(int region_id,const std::string &url, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeURLAsync(0,region_id,value,url,cb,data); }

/** shortcut for uiInvokeURLAsync(0, UI_REGION_DEFAULT, dummy_value, url,cb,data); */
inline int uiInvokeURLAsync(const std::string &url, uiAsyncCallback cb=0, void *data=0)
{ stringmap value; return uiInvokeURLAsync(0, UI_REGION_DEFAULT, value, url,cb,data); }


/** Wait for a dialog to finish
 * \param[in] txn_id Request ID of the dialog
 * \param[out] value entered values are added to this map
 * \param[in] timeout_msec timeout in milliseconds. In case of a negative timeout wait forever.
 * \return error code (see ::UIError) or return value specified in the HTML fragment. In case of a timeout UI_ERR_WAIT_TIMEOUT is returned and
 * uiInvokeWait() has to be called again or memory may be leaked.
 * As an alternative uiInvokeCancel() may be called to cancel the dialog.
 * \note uiInvokeWait cannot be called if a callback function has been provided when invoking the dialog.
 * \note When displaying dialog sequences (e.g. using load url in an action) uiInvokeWait() handles the switching
 * to the next dialog in the sequence.
 */
DllSpec int uiInvokeWait(int txn_id, std::map<std::string,std::string> &value, int timeout_msec=-1);


/** the same as uiInvokeWait(int txn_id, std::map<std::string,std::string> &value, int timeout_msec=-1) but discards returned input values */
inline int uiInvokeWait(int txn_id, int timeout_msec=-1)
{
   std::map<std::string,std::string> value;
   return uiInvokeWait(txn_id,value,timeout_msec);
}

/** Cancel dialog. This may be called whenever uiInvokeWait() can be called and cancels the dialog.
 * \param[in] txn_id Request ID of the dialog
 * \param[out] value entered values are added to this map
 * \return error code (see ::UIError). For asynchronous calls with callback function the return value
 * cannot be obtained since it is delivered to the callback function. In this case always UI_ERR_OK is returned.
 * Normally the error code will be UI_ERR_CANCELLED, however, the returncode reflects the true result of
 * the dialog, i.e. if the user terminated the dialog just before cancelling it, the returncode will contain
 * the result of the dialog.
 */
DllSpec int uiInvokeCancel(int txn_id, std::map<std::string,std::string> &value);

/** the same as uiInvokeCancel(int txn_id, std::map<std::string,std::string> &value) but discards returned input values */
inline int uiInvokeCancel(int txn_id)
{
   std::map<std::string,std::string> value;
   return uiInvokeCancel(txn_id,value);
}

/** Cancel dialog without reading the result, i.e. uiInvokeWait() will have to be called afterwards.
 * \param[in] txn_id Request ID of the dialog
 * \return error code */
DllSpec int uiInvokeCancelAsync(int txn_id);

/** Create a new thread and display a dialog from within this thread. The thread terminates when the dialog terminates.
 *  Dialog return values are discarded.
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX)
 * \return error code (see ::UIError) (but no result from dialog)
 * \note \a display and \a region_id are optional, see \ref gui_overloading
 * \note This function is useful for example for displaying some kind of virtual keyboard.
 */
DllSpec int uiInvokeURLDetached(int display, int region_id, const char *url);

/** shortcut for uiInvokeURLDetached(0,region_id,url); */
DllSpec int uiInvokeURLDetached(int region_id, const char *url);

/** shortcut for uiInvokeURLDetached(0,UI_REGION_DEFAULT,url); */
inline int uiInvokeURLDetached(const char *url)
{ return uiInvokeURLDetached(0, UI_REGION_DEFAULT,url); }


/** Remove a dialog or sub-regions from a region. If the dialog is still active, it is cancelled. After the the dialog has been removed,
 * the region is empty, i.e. it displays nothing and does not clear the region.
 * \param[in] display display
 * \param[in] region_id region id
 * \return error code (see ::UIError)
 */
DllSpec int uiClear(int display, int region_id);

/** shortcut for uiClear(0,region_id); */
inline int uiClear(int region_id) { return uiClear(0,region_id); }

/** Int properties for configuring global GUI options */
enum UIPropertyInt {
   UI_PROP_TIMEOUT=0,                /**< Idle timeout in milliseconds, set to -1 to deactivate */
   UI_PROP_PASSWORD_CHAR,            /**< Password masking character */
   UI_PROP_DEFAULT_FONT_SIZE,        /**< Default font size */
   UI_PROP_PIN_AUTO_ENTER,           /**< Automatically terminate the PIN entry when the maximum number of digits has been reached */
   UI_PROP_PIN_CLEAR_ALL,            /**< The clear key deletes all entered digits instead of only the last one */
   UI_PROP_PIN_BYPASS_KEY,           /**< Key code that is used for PIN bypass. Setting it to 0 disables PIN bypass.
                                      *   Supported bypass keys are: 13 (enter/confirm key), 8 (clear/backspace key)
                                      *   Add 1000 to disable bypass as soon as a digit has been entered, e.g. using 1013
                                      *   will use the enter key as bypass key but only if no digit has been entered in between.
                                      */
   UI_PROP_PIN_ALGORITHM,            /**< value used for iPS_SelectPINAlgo(), e.g. EMV_PIN, see
                                      *   "MX 900 Series Programmers Guide" */
   UI_PROP_PIN_INTERCHAR_TIMEOUT,    /**< if >0: inter character timeout for PIN entry in milliseconds */
   UI_PROP_PASSWORD_SHOW_CHAR,       /**< Time (in ms) to show last entered password character before masking it (ignored for PIN entry) */
   UI_PROP_KEEP_DISPLAY,             /**< Determines whether the display is cleared (0) or whether the display keeps displaying the old
                                      *  content (1) when updating the layout using uiLayout. Keeping the display on layout updates does
                                      *  not work on all platforms, therefore, default is to clear. */
   UI_PROP_UPDATE_EVENTS,            /**< if not 0 send update events each time the content of an input field changes or if the URL
                                      *   changes due to a load action (default: 0) */
   UI_PROP_CIRCULAR_MENU,            /**< if not 0 the actions "up" and "down" actions of the menu will not stop at the start/end of the menu but
                                      * jump to the last/first element instead. (default: 0) */
   UI_PROP_TOUCH_ACTION_BEEP,        /**< if set (1) beep when an action was triggered from the touch screen. (default: 0) */
   UI_PROP_STATISTICS,               /**< if not 0 update statistics information that can be read using uiStatistics. (default: 0) */
   UI_PROP_RESTRICTED,               /**< if set (1) only up to three digit keys may be entered in a row. (default: 0) */
   UI_PROP_CP_APP_MODE,              /**< if set (1) activate CP app specific restrictions, (default: 0) */
   UI_PROP_SCROLL_COLOR,             /**< RGBA color value used for the scrollbar overlays, the value is an unsigned cast to an int (default: 0x00000060) */
   UI_PROP_INPUT_ERROR_BEEP,         /**< if set (1) beep when an input character cannot be inserted into an input field
                                      *   (input field full or invalid character) (default: 0)*/
   UI_PROP_ZOOM_MAX,                 /**< maximum zoom in percent (multitouch enabled ADKGUI only, default:400, minimum:100) */
   UI_PROP_SCROLL_DISPLAY,           /**< time to show the scrollbar overlays in milliseconds. Scrollbar overlays are disabled by setting
                                      *   this to 0. When set to a negative value, overlays are displayed permanently */
   UI_PROP_JS_QUOTA_SIZE,               /**< If >0 filesystem quota in kilobytes for use by JavaScript, see also ::UI_PROP_JS_QUOTA_ROOT  */

   // Readonly properties
   UI_DEVICE_WIDTH=1000,             /**< width of the display in pixels */
   UI_DEVICE_HEIGHT,                 /**< height of the display in pixels */
   UI_DEVICE_COLOR_DEPTH,            /**< color depth in bits  */
   UI_DEVICE_SUPPORTS_TOUCH,         /**< 1 if the device supports touch, else 0 */
   UI_DEVICE_SUPPORTS_BEEPER,        // not supported, do not use. Left here for compatibility reasons.
   UI_DEVICE_SUPPORTS_AUDIO,         /**< 1 if the device supports audio output, else 0 */
   UI_DEVICE_SUPPORTS_VIDEO,         /**< 1 if the device supports video output, else 0 */
   UI_DEVICE_SUPPORTS_SIGCAP,        /**< 1 if the device supports signature capture, else 0 */
   UI_DEVICE_SUPPORTS_COLOR_DISPLAY, /**< 1 if the device supports color display, else 0 (B/W display) */
   UI_DEVICE_HAS_CAP_TOUCH_KEYPAD,   /**< 1 if the device has a capacitive touch keypad, else 0 */
   UI_DEVICE_SUPPORTS_NAVIGATOR_MODE,/**< 1 if the device supports navigator mode */
   UI_DEVICE_KEY_COUNT               /**< number of keypad keys */
};

/** String properties for configuring global GUI options */
enum UIPropertyString {
   UI_PROP_DECIMAL_SEPARATOR=2000, /**< Decimal separator  */
   UI_PROP_THOUSANDS_SEPARATOR,    /**< Thousands separator  */
   UI_PROP_DEFAULT_FONT,           /**< Default font name  */
   UI_PROP_RESOURCE_PATH,          /**< Resource path, default is www/&lt;platform> */
   UI_PROP_FILE_PREFIX,            /**< prefix that is added in front of the URL in uiInvokeURL and to the template names,
                                    *   e.g. using "en/" would access the files in the subdirectory "en". */
   UI_PROP_KEYMAP,                 /**< Keymap string of the format: "<key>=<char><char>...<char>\n<key>=<char>..." */
   UI_PROP_CSS,                    /**< name of a CSS file */
   UI_PROP_RESOURCE_DEFAULT_PATH,  /**< Resource default path, default is www/default (or resource/default) */
   UI_PROP_GUI_EXTENSIONS,         /**< (removed) */
   UI_PROP_HTTP_HEADER_FIELDS,     /**< (removed) */

   UI_PROP_CP_APP_DIR,             /**< CP app directory, if set only dialogs from the app dir can be shown.
                                    *   It should point to the base app directory without platform, e.g. /home/sys14/www/<app-id> on V/OS */
   UI_PROP_LANGUAGE,               /**< ISO 639-1 language code (2 bytes, lowercase) or BCP 47 locale
                                    *   (5 bytes, 2 bytes lowercase language code, '-', 2 bytes uppercase country code, e.g. "en-US").
                                    *   Setting the language loads catalog file \<language>.ctlg.
                                    *   It is searched in the resource folder (e.g. www/VX820) and in the default resource folder (www/default)
                                    *   In case of a 5-byte locale if the file cannot be found, the country code is stripped and a catalog file
                                    *   just using the language code is searched. For example, using "de-AT", it would search for de-AT.ctlg
                                    *   in the resource and default resource folder and then it would search for de.ctlg in those two folders.
                                    */
   UI_PROP_JS_ROOT,                /**< Setting this path activates the JavaScript filesystem module. I/O is restricted to happen inside this path.
                                    *   Use "$APPDIR" to refer to this path from within JavaScript. Images and videos may also use "$APPDIR"
                                    *   to refer to files in this path. */
   UI_PROP_PRINT_ROOT,             /**< Setting this path activates JavaScript printing support. Receipts to be printed are looked
                                    *   up relative to this path. \note JavaScript printing may not be available for some platforms. */
   UI_PROP_PRINT_CSS,              /**< Name of the CSS file used for JavaScript printing. Default is "print.css" */

   UI_PROP_JS_QUOTA_ROOT,          /**< If not empty base directory for JavaScript quota calculation, default is "", see also ::UI_PROP_JS_QUOTA_SIZE */

   // Readonly properties
   UI_DEVICE_MODEL=3000            /**< Terminal model name, e.g. "Razor" or "VX820" */
};

/** set int property
 * \param[in] display display
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiSetPropertyInt(int display, enum UIPropertyInt property, int value);

/** shortcut for uiSetPropertyInt(0,property,value); */
DllSpec int uiSetPropertyInt(enum UIPropertyInt property, int value);

/** get int property
 * \param[in] display display
 * \param[in] property property to be set
 * \param[out] value current value
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiGetPropertyInt(int display, enum UIPropertyInt property, int *value);

/** shortcut for uiGetPropertyInt(0,property,value); */
DllSpec int uiGetPropertyInt(enum UIPropertyInt property, int *value);

/** set string property
 * \param[in] display display
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiSetPropertyString(int display, enum UIPropertyString property, const char *value);

/** shortcut for uiSetPropertyString(0,property,value); */
DllSpec int uiSetPropertyString(enum UIPropertyString property, const char *value);

/** set string property
 * \param[in] display display
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
inline int uiSetPropertyString(int display, enum UIPropertyString property, const std::string &value)
{ return uiSetPropertyString(display,property,value.c_str()); }

/** shortcut for uiSetPropertyString(0,property,value); */
inline int uiSetPropertyString(enum UIPropertyString property, const std::string &value)
{ return uiSetPropertyString(0,property,value.c_str()); }

/** get string property
 * \param[in] display display
 * \param[in] property property to be set
 * \param[out] value current value
 * \param[in] len size ouf output buffer \a value in bytes
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiGetPropertyString(int display, enum UIPropertyString property, char* value, int len);

/** shortcut for uiGetPropertyString(0,property,value,len); */
DllSpec int uiGetPropertyString(enum UIPropertyString property, char* value, int len);

/** get string property
 * \param[in] display display
 * \param[in] property property to be set
 * \param[out] value current value
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiGetPropertyString(int display, enum UIPropertyString property, std::string &value);

/** shortcut for uiGetPropertyString(0,property,value); */
DllSpec int uiGetPropertyString(enum UIPropertyString property, std::string &value);

/** determine the HW dependent terminal specific subdirectory inside the 'www' directory
 * \param[in] display display
 * \param[in] www_path path of the www directory containing the resource files
 * \return path to the terminal specific subdirectory
 * \note \a display is optional, see \ref gui_overloading
 * \note The environment variable GUIPRT_APPNAME is not considered.
 */
DllSpec std::string uiGetTerminalResourceDir(int display, const std::string &www_path);

/** shortcut for uiGetTerminalResourceDir(0,www_path); */
DllSpec std::string uiGetTerminalResourceDir(const std::string &www_path);

/** de-/activate thread local properties for the current thread. Activating thread
 * local properties initially copies the global properties to the current thread.
 * \param[in] local if true activate thread local properties, if false discard them
 * \note Switching affects all displays.
 */
DllSpec void uiSetLocalProperties(bool local);

/** \return true if thread local properties are used, else return false */
DllSpec bool uiGetLocalProperties();

/** define output regions on the display. Region with id==0 is the default region.
 * For incremental layout updates the previous layout may be provided to be checked before applying the new layout.
 * If the previous layout does not match the current layout in guiserver, UI_ERR_MODIFIED is returned. This feature
 * can be used to detect and prevent race conditions if several processes concurrently try to update a layout.
 * \param[in] display display
 * \param[in] region array containing the regions, at least one region must be provided
 * \param[in] regcnt number of entries in \a region
 * \param[in] oldreg optional: if not NULL array containing the previous set of regions
 * \param[in] oldregcnt optional: number of entries in \a oldreg
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note If a region is removed that still contains a dialog this dialog is cancelled.
 */
DllSpec int uiLayout(int display, const UIRegion *region, int regcnt, const UIRegion *oldreg, int oldregcnt);

/** shortcut for uiLayout(0,region,regcnt,oldreg,oldregcnt); */
DllSpec int uiLayout(const UIRegion *region, int regcnt, const UIRegion *oldreg, int oldregcnt);

/** shortcut for uiLayout(display, region,regcnt,0,0); */
DllSpec int uiLayout(int display, const UIRegion *region, int regcnt);

/** shortcut for uiLayout(0,region,regcnt,0,0); */
DllSpec int uiLayout(const UIRegion *region, int regcnt);

/** define output regions on the display. Region with id==0 is the default region.
 * \param[in] display display
 * \param[in] reg vector containing the regions, at least one region must be provided
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note If a region is removed that still contains a dialog this dialog is cancelled.
 */
inline int uiLayout(int display, const std::vector<UIRegion> &reg) {
   UIRegion dummy;
   return uiLayout(display, reg.size() ? &reg[0] : &dummy,reg.size());
}

/** shortcut for uiLayout(0,reg); */
inline int uiLayout(const std::vector<UIRegion> &reg) {
   UIRegion dummy;
   return uiLayout(0, reg.size() ? &reg[0] : &dummy,reg.size());
}

/** define output regions on the display. Region with id==0 is the default region.
 * The old layout provided in \a oldreg must match the current layout in guiserver or the layout
 * will not be updated and UI_ERR_MODIFIED will be returned.
 * \param[in] display display
 * \param[in] reg vector containing the regions, at least one region must be provided
 * \param[in] oldreg vector containing the previous set of regions.
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note If a region is removed that still contains a dialog this dialog is cancelled.
 */
inline int uiLayout(int display, const std::vector<UIRegion> &reg, const std::vector<UIRegion> &oldreg)
{
   UIRegion dummy;
   return uiLayout(display, reg.size() ? &reg[0] : &dummy,reg.size(), oldreg.size() ? &oldreg[0] : &dummy, oldreg.size());
}

/** shortcut for uiLayout(0,reg,oldreg); */
inline int uiLayout(const std::vector<UIRegion> &reg, const std::vector<UIRegion> &oldreg)
{
   UIRegion dummy;
   return uiLayout(0,reg.size() ? &reg[0] : &dummy,reg.size(), oldreg.size() ? &oldreg[0] : &dummy, oldreg.size());
}

/** define output regions of the display. The layout definition is read from section \a name of gui.ini
 * \param[in] display display
 * \param[in] name name of the layout section in gui.ini
 * \return error code (see ::UIError), in case the section is not found in gui.ini or it is does not contain a valid configuration, then UI_ERR_REGION is returned.
 * \note \a display is optional, see \ref gui_overloading
 * \note changing the layout may cancel all currently active dialogs
 */
DllSpec int uiLayout(int display, const char *name);

/** shortcut for uiLayout(0,name); */
DllSpec int uiLayout(const char *name);

/** read the current layout setting
 * \param[in] display display
 * \param[out] region returned region layout
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiGetLayout(int display, std::vector<UIRegion> &region);

/** shortcut for uiGetLayout(0,region); */
DllSpec int uiGetLayout(std::vector<UIRegion> &region);

/** Determine size of a region in pixels
 * \param[in] display display;
 * \param[in] region_id region ID;
 * \param[out] width width in pixels
 * \param[out] height height in pixels
 * \return error code
 */
DllSpec int uiGetRegionSize(int display, int region_id, int &width, int &height);

/** shortcut for uiGetRegionSize(0,region_id,width,height); */
inline int uiGetRegionSize(int region_id, int &width, int &height)
{
   return uiGetRegionSize(0,region_id,width,height);
}

/** Enter region \a region_id making it the parent region for all successive uiLayout() and uiGetLayout() commands.
 * This is used to create a tree of regions.
 * \param[in] display display
 * \param[in] region_id region ID
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note a new default region is created if required.
 */
DllSpec int uiEnterRegion(int display, int region_id);

/** shortcut for uiEnterRegion(0,region_id); */
DllSpec int uiEnterRegion(int region_id);

/** Ascend one level in the tree of regions.
 * \return error code: UI_ERR_OK if region was left, UI_ERR_REGION if already at the highest level
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiLeaveRegion(int display);

/** shortcut for uiLeaveRegion(0); */
DllSpec int uiLeaveRegion();

/** Return the current region path that was constructed using uiEnterRegion(). The region path contains the regions passed
 * on to successive calls of uiEnterRegion() separated by '/', e.g calling the sequence uiEnterRegion(1); uiEnterRegion(2);
 * uiEnterRegion(3); the region path would be "1/2/3".
 * If no region path is used, the empty string is returned.
 * \param[in] display display
 * \return current region path
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec std::string uiCurrentRegionPath(int display);

/** shortcut for uiCurrentRegionPath(0); */
DllSpec std::string uiCurrentRegionPath();

/** set region path.
 * \param[in] display display
 * \param[in] path absolute region path to be set
 * \return error code
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiSetCurrentRegionPath(int display, const std::string &path);

/** shortcut for uiSetCurrentRegionPath(0,path); */
DllSpec int uiSetCurrentRegionPath(const std::string &path);


/** check if region is visible on screen or hidden
 * \param[in] display display
 * \param[in] region_id region ID
 * \return 1 if visible, 0 if not visible, <0 if error
 */
DllSpec int uiRegionIsVisible(int display, int region_id);

/** check if region is visible on screen or hidden
 * \param[in] region_id region ID
 * \return 1 if visible, 0 if not visible, <0 if error
 */
inline int uiRegionIsVisible(int region_id)
{return uiRegionIsVisible(0,region_id); }


/** re-read configuration from file gui.ini from within the resource directory associated with a display
 * \param[in] display display
 * \note \a display is optional, see \ref gui_overloading
 * \note gui.ini is read automatically on startup. Invoking uiReadConfig() is
 *  required if gui.ini has been modified by the application or the resource path
 *  has been changed by updating UI_PROP_RESOURCE_PATH and re-reading of the file
 *  is required.
 */
DllSpec void uiReadConfig(int display);

/** shortcut for uiReadConfig(0); */
DllSpec void uiReadConfig();


/** load and set a catalog file containing name-value text pairs to be inserted with HTML placeholder <?text name?>.
 *  The current catalog is unloaded with filename=="" or by loading another catalog file.
 *  \param[in] display display
 *  \param[in] filename of the catalog, empty string to unload the current dialog
 *  \return UI_ERR_OK if file was successfully loaded, else error code (see ::UIError)
 *  \note \a display is optional, see \ref gui_overloading
 *  \note Setting the catalog file resets UI_PROP_LANGUAGE
 */
DllSpec int uiSetCatalog(int display, const std::string &filename);

/** shortcut for uiSetCatalog(0,filename); */
DllSpec int uiSetCatalog(const std::string &filename);

/**
 *  \param[in] display display
 *  \return file name of the current catalog file or empty string in case none has been loaded
 *  \note \a display is optional, see \ref gui_overloading
 */
DllSpec std::string uiGetCatalog(int display);

/** shortcut for uiGetCatalog(0); */
DllSpec std::string uiGetCatalog();

/** Set the maximum number of catalogs that are kept in memory although being unused.
 * This improves load time when a catalog is used again, since when still in memory it
 * does not need to be read from disk (default: 0)
 * \param[in] num number of catalogs
 * \note Catalog storage is shared with the printer module, i.e. this setting may be
 * changed by calls to prtCatalogSetDelayedRelease()
 */
DllSpec void uiCatalogSetDelayedRelease(unsigned num);

/** Get the maximum number of unused catalogs that are kept in memory.
 * \return number of catalogs
 * \note Catalog storage is shared with the printer module, i.e. this setting may be
 * changed by calls to prtCatalogSetDelayedRelease()
 */
DllSpec unsigned uiCatalogGetDelayedRelease();


/** lookup a text from current loaded catalog by \a key name. If text is not found in catalog or
 *  no catalog is loaded the function returns value in in parameter \a default.
 *  \param[in] display display
 *  \param[in] name of key used to lookup the text in calalog
 *  \param[in] deflt text that is returned, if text is not found in calalog
 *  \return text from catalog for success, else value in parameter \a default
 *  \note \a display is optional, see \ref gui_overloading
 */
DllSpec std::string uiGetText(int display, const std::string &name, const std::string &deflt="");

/** shortcut for uiGetText(0,name,deflt); */
DllSpec std::string uiGetText(const std::string &name, const std::string &deflt="");


/** preprocess HTML code and return the resulting string
 * \param[in] display display whose settings should be used (e.g. text catalog, css file, etc.)
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[out] html resulting HTML code
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiGetHtml(int display, const std::map<std::string,std::string> &value, const std::string &text, std::string &html);

/** shortcut for uiGetHtml(0,value,text,html); */
DllSpec int uiGetHtml(const std::map<std::string,std::string> &value, const std::string &text, std::string &html);

/** preprocess HTML code and return the resulting string
 * \param[in] display display whose settings should be used (e.g. text catalog, css file, etc.)
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX) unless an absolute path was provided.
 * \param[out] html resulting HTML code
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec int uiGetHtmlURL(int display, const std::map<std::string,std::string> &value, const std::string &url, std::string &html);

/** shortcut for uiGetHtmlURL(0,value,url,html); */
DllSpec int uiGetHtmlURL(const std::map<std::string,std::string> &value, const std::string &url, std::string &html);


/** determine the full path of an url taking into account all settings and rules for looking up files
 * \param[in] display display whose settings should be used (e.g. text catalog, css file, etc.)
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX) unless an absolute path was provided.
 * \return full path of the HTML file or empty string if not found.
 * \note \a display is optional, see \ref gui_overloading
 */
DllSpec std::string uiGetURLPath(int display, const std::string &url);

/** shortcut for uiGetURLPath(0,url); */
DllSpec std::string uiGetURLPath(const std::string &url);


/** take a screenshot and save as PNG file
 * \param[in] display display
 * \param[in] filename file name
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note Only supported by DirectGUI
 */
DllSpec int uiScreenshotPNG(int display, const char *filename);

/** shortcut for uiScreenshotPNG(0,filename); */
DllSpec int uiScreenshotPNG(const char *filename);

/** take a screenshot of a screen area and save as PNG file
 * \param[in] display display
 * \param[in] filename file name
 * \param[in] x x-coordinate of the screen area
 * \param[in] y y-coordinate of the screen area
 * \param[in] w width of the screen area
 * \param[in] h height of the screen area
 * \return error code (see ::UIError)
 * \note If the position/size does not fit on the screen it will be cropped
 * \note \a display is optional, see \ref gui_overloading
 * \note Only supported by DirectGUI
 */
DllSpec int uiScreenshotPNG(int display, const char *filename, int x, int y, int w, int h);

/** shortcut for uiScreenshotPNG(0,filename,x,y,w,h); */
inline int uiScreenshotPNG(const char *filename, int x, int y, int w, int h)
{
   return uiScreenshotPNG(0,filename,x,y,w,h);
}


/** take a screenshot and return PNG data in vector
 * \param[in] display display
 * \param[out] data data of PNG file
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note Only supported by DirectGUI
 */
DllSpec int uiScreenshotPNGData(int display, std::vector<unsigned char> &data);

/** shortcut for uiScreenshotPNGData(0,data); */
DllSpec int uiScreenshotPNGData(std::vector<unsigned char> &data);

/** take a screenshot of a screen area and return PNG data in vector
 * \param[in] display display
 * \param[out] data data of PNG file
 * \param[in] x x-coordinate of the screen area
 * \param[in] y y-coordinate of the screen area
 * \param[in] w width of the screen area
 * \param[in] h height of the screen area
 * \return error code (see ::UIError)
 * \note If the position/size does not fit on the screen it will be cropped
 * \note \a display is optional, see \ref gui_overloading
 * \note Only supported by DirectGUI
 */
DllSpec int uiScreenshotPNGData(int display, std::vector<unsigned char> &data, int x, int y, int w, int h);

/** shortcut for uiScreenshotPNGData(0,data,x,y,w,h); */
inline int uiScreenshotPNGData(std::vector<unsigned char> &data, int x, int y, int w, int h)
{
   return uiScreenshotPNGData(0,data,x,y,w,h);
}


/** convert a dialog to a PNG image
 * \param[in] filename file name of the PNG file
 * \param[in] width width of the image
 * \param[in] height height of the image
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX) unless an absolute path was provided.
 * \return error code (see ::UIError)
 */
DllSpec int uiURL2PNG(const char *filename, int width, int height, const std::map<std::string,std::string> &value, const std::string &url);

/** convert a dialog to a PNG image
 * \param[out] data data of PNG file
 * \param[in] width width of the image
 * \param[in] height height of the image
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX) unless an absolute path was provided.
 * \return error code (see ::UIError)
 */
DllSpec int uiURL2PNGData(std::vector<unsigned char> &data, int width, int height, const std::map<std::string,std::string> &value, const std::string &url);

/** convert a dialog to a PNG image
 * \param[in] filename file name of the PNG file
 * \param[in] width width of the image
 * \param[in] height height of the image
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] text HTML code
 * \return error code (see ::UIError)
 */
DllSpec int uiHTML2PNG(const char *filename, int width, int height, const std::map<std::string,std::string> &value, const std::string &text);

/** convert a dialog to a PNG image
 * \param[out] data data of PNG file
 * \param[in] width width of the image
 * \param[in] height height of the image
 * \param[in] value name value pairs that are used as initial value of input fields, checkboxes, etc. and that are
 *                      used during script processing.
 * \param[in] text HTML code
 * \return error code (see ::UIError)
 */
DllSpec int uiHTML2PNGData(std::vector<unsigned char> &data, int width, int height, const std::map<std::string,std::string> &value, const std::string &text);

/** read library version
 * \return string containing the version
 */
DllSpec const char *uiLibVersion();

/** returns a zero-terminated string with version and build information of libvfiguiprt
 *  in ADK version string format: \<major>.\<minor>.\<patch>-\<build>, e.g. "1.2.3-4"
 * \return version string */
DllSpec const char *gui_GetVersion();


/** returns a zero-terminated string with version and build information of guiserver/guiprtserver
 *  in ADK version string format: \<major>.\<minor>.\<patch>-\<build>, e.g. "1.2.3-4"
 * \param[in] display display
 * \return version string */
DllSpec const char *gui_GetSvcVersion(int display);
DllSpec const char *gui_GetSvcVersion();

/** bit mask constants for describing the transition */
enum {
   // lowest 4 bits: direction flags
   UI_TRANS_UP     =0x1,  /**< movement direction: up, depending on the transition type horizontal an vertical movement may be combined.*/
   UI_TRANS_RIGHT  =0x2,  /**< movement direction: right, depending on the transition type horizontal an vertical movement may be combined.*/
   UI_TRANS_DOWN   =0x4,  /**< movement direction: down, depending on the transition type horizontal an vertical movement may be combined.*/
   UI_TRANS_LEFT   =0x8,  /**< movement direction: left, depending on the transition type horizontal an vertical movement may be combined.*/

   UI_TRANS_DIRECTION_MASK=0xf, /**< mask containing the movement direction bits */

   // 2 bits affecting speed:
   UI_TRANS_EASE_IN    =0x10, /**< transition accelerates after the start */
   UI_TRANS_EASE_OUT   =0x20, /**< transition decelerates before the end  */
   UI_TRANS_EASE_INOUT =0x30, /**< combination of UI_TRANS_EASE_IN and UI_TRANS_EASE_OUT */

   UI_TRANS_EASE_MASK=0x30, /**< mask containing the acceleration bits */

   // Transition type
   UI_TRANS_NONE     =0x000, /**< No transition */
   UI_TRANS_SLIDE    =0x100, /**< The dialog slides to the screen shifting out the old one */
   UI_TRANS_SLIDEON  =0x200, /**< The dialog slides to the screen covering the old one */
   UI_TRANS_SWAP     =0x300, /**< The new dialog starts behind the old one. Then both slide to
                              *   opposite sides and then back, this time with the new dialog in front.*/
   UI_TRANS_CROSSFADE=0x400, /**< Crossfade between old and new dialog. The movement direction flags are ignored.*/

   UI_TRANS_TYPE_MASK=0xfff00 /**< mask containing the transition type bits */
};

/** set the transition type for a region
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] transtype transition type
 * \param[in] duration_ms duration for the transition in milliseconds, ignored for UI_TRANS_NONE which is the default transition type.
 * \return error code (see ::UIError)
 * \note \a display is optional, see \ref gui_overloading
 * \note Configured transitions are reset when changing the layout
 */
DllSpec int uiSetTransition(int display, int region_id, unsigned transtype, int duration_ms);

/** shortcut for accessing display 0 */
DllSpec int uiSetTransition(int region_id, unsigned transtype, int duration_ms);

/** shortcut for accessing UI_REGION_DEFAULT on display 0 */
inline int uiSetTransition(unsigned transtype, int duration_ms)
{ return uiSetTransition(0,UI_REGION_DEFAULT,transtype,duration_ms); }


/** error entry */
struct UIErrorEntry {
   /** error type */
   enum ErrorType { NOT_FOUND,   /**< resource not found*/
                    UNSUPPORTED, /**< unsupported data format */
                    CORRUPT      /**< corrupt file */
   } type; /**< type of the error */
   std::string name; /**< involved resource name or error message in case of SCRIPT_ERROR */
   /** constructor */
   UIErrorEntry(ErrorType t, const std::string &n) :type(t),name(n) {}
};

/** obtain list of non-fatal errors that happened while displaying a dialog. In case of synchronous
 * dialogs the error list is reset as part of displaying the dialog. In case of asynchronous
 * dialogs this list is reset when displaying the dialog using one of the ui*Async functions and when
 * calling uiInvokeWait (or one of the other ui*Wait functions)
 * \return reference to the error list of the last dialog shown in this thread.
 */
DllSpec const std::vector<UIErrorEntry> &uiErrorList();

/** Statistics information */
struct UIStatistics {
   vfigui::TimeStamp start;  /**< starting time */
   vfigui::TimeStamp css;    /**< css loading and parsing has finished */
   vfigui::TimeStamp html;   /**< html loading, parsing and layout has finished */
   vfigui::TimeStamp draw;   /**< sending drawing commands to gfx system has finished (initial drawing of the HTML page) */
};


/** obtain statistics information for the last completed dialog. In case of synchronous
 * dialogs the statistics information is reset as part of displaying the dialog. In case of asynchronous
 * dialogs this information is reset as part of calling uiInvokeWait (or one of the other ui*Wait functions)
 * \return reference to statistics information of the last dialog shown in this thread.
 */
DllSpec const UIStatistics &uiStatistics();


/** Obtain script error message
 * \return string containing information about errors that were reported during last script processing in this thread.
 */
DllSpec std::string uiScriptError();


/** Determine the amount of time passed since the last user interaction.
 * \return Idle time in milliseconds (>=0) or error code (<0).
 */
DllSpec int uiGetIdleTime(int display=0);

/** Set log mask
 * \param[in] mask logging mask (see GUI_LOGMASK)
 */
DllSpec void uiSetLogMask(unsigned mask);

/** Get log mask
 * \return current log mask
 */
DllSpec unsigned uiGetLogMask(void);

/** LED shape */
enum UILEDShape {
   UI_SHAPE_RECTANGLE,  /**< rectangular shape */
   UI_SHAPE_ELLIPSE     /**< elliptic shape */
};

/** set LED configuration. If present the LED area is cleared with
 * the background color and the LEDs are drawn in off-state.
 * \param[in] display display
 * \param[in] shape LED shape
 * \param[in] width  width of a single LED
 * \param[in] height height of a single LED
 * \param[in] off_rgba color of the LED when switched off
 * \param[in] on0_rgba color of LED 0 when switched on
 * \param[in] on1_rgba color of LED 1 when switched on
 * \param[in] on2_rgba color of LED 2 when switched on
 * \param[in] on3_rgba color of LED 3 when switched on
 * \return error code (see ::UIError)
 */
DllSpec int uiConfigLEDs(int display,
                         UILEDShape shape, int width, int height,
                         unsigned off_rgba,
                         unsigned on0_rgba, unsigned on1_rgba, unsigned on2_rgba, unsigned on3_rgba);

/** shortcut for accessing display 0 */
DllSpec int uiConfigLEDs(UILEDShape shape, int width, int height,
                         unsigned off_rgba,
                         unsigned on0_rgba, unsigned on1_rgba, unsigned on2_rgba, unsigned on3_rgba);


/** set LED configuration from PNG file. The PNG file has to contain 5 LEDs next to each other.
 * The first LED shows the off state, the remaining 4 LEDs show the on state of the 4 LEDs on screen.
 * The alpha channel is supported to display non-rectangular LEDs.
 * If present the LED area is cleared with
 * the background color and the LEDs are drawn in off-state.
 * \param[in] display display
 * \param[in] filename file name of the PNG image
 * \return error code (see ::UIError)
 */
DllSpec int uiConfigLEDs(int display, const char *filename);

/** shortcut for accessing display 0 */
DllSpec int uiConfigLEDs(const char *filename);

/** Display edge selection */
enum UIEdge {
   UI_EDGE_TOP,  /**< top edge of display */
   UI_EDGE_RIGHT,  /**< right edge of display */
   UI_EDGE_BOTTOM, /**< bottom edge of display */
   UI_EDGE_LEFT    /**< left edge of display */
};

/** show virtual LEDs on the display and/or increment the LED area reference counter. The LED area is cleared with
 * the background color and the LEDs are drawn in off-state.
 * \param[in] display display
 * \param[in] edge selects edge of the display at which to reserve some room. UI_EDGE_OFF removes the LED area
 * \param[in] width width of the area
 * \param[in] height height of the area
 * \param[in] bg_rgba background color in RGBA format (alpha channel is ignored)
 * \return error code (see ::UIError)
 * \note The size of the LED area determines how much room is available for distributing the contactless LEDs.
 * It does not need to fit the width (or height depending on the edge) of the display, clearing the background
 * happens using the full width (height).
 * \note If uiShowLEDArea is called more than once, then the later
 * calls do not update the parameters but only increase a reference counter.
 * uiHideLEDArea() has to be called the same number of times to remove the area.
 * \note uiConfigLEDs should be called before calling uiShowLEDArea().
 */
DllSpec int uiShowLEDArea(int display, UIEdge edge, int width, int height, unsigned bg_rgba);

/** shortcut for accessing display 0 */
DllSpec int uiShowLEDArea(UIEdge edge, int width, int height, unsigned bg_rgba);

/** Decrement the LED area reference counter and hide the LED area if the counter reaches 0.
 * \param[in] display display
 * \return error code (see ::UIError)
 */
DllSpec int uiHideLEDArea(int display);

/** shortcut for accessing display 0 */
DllSpec int uiHideLEDArea();

/** switch led on/off
 * \param[in] display display
 * \param[in] led led number (0..3)
 * \param[in] state on (true) or off (false)
 * \return error code (see ::UIError)
 */
DllSpec int uiSetLED(int display, unsigned led, bool state);

/** shortcut for accessing display 0 */
DllSpec int uiSetLED(unsigned led, bool state);


/** backlight brightness level */
struct UIBacklight {
   unsigned brightness; /**< brightness level (0..100) */
   unsigned timeout;    /**< timeout in milliseconds after which to switch to
                         *   the next level (if there is another one) */
};

/** set up backlight parameters
 * \param[in] display display
 * \param[in] levels backlight brightness levels and timeouts. The entries in the vector
 *                   are considered to represent decreasing brightness, i.e. levels[0]
 *                   represents maximum brighness and levels[levels.size()-1] represents
 *                   minimum brightness.
 * \return error code (see ::UIError)
 * \note If more than one brightness level is configured, the first level is considered to be
 * the on state. This level is activated as soon as there is some user interaction. The
 * last level is considered to be the off state. In this state, if the brighness is 0, the first touch or
 * keypress is used to wake up the device and is otherwise dropped to not inadvertedly
 * trigger an action.
 */
DllSpec int uiSetBacklightConfig(int display, const std::vector<UIBacklight> &levels);

/** read backlight parameters
 * \param[in] display display
 * \param[out] levels backlight brightness levels and timeouts
 * \return error code (see ::UIError)
 */
DllSpec int uiGetBacklightConfig(int display, std::vector<UIBacklight> &levels);

/** set backlight level to one of the levels defined using uiSetBacklightParam
 * \param[in] display display
 * \param[in] level new level in the range 0..levels.size()-1
 * \return error code (see ::UIError)
 * \note The level is evaluated modulus the number of configured levels, i.e. setting it to -1 can be used
 * to select the level with lowest brightness.
 */
DllSpec int uiSetBacklightLevel(int display, int level);

/** get backlight level
 * \param[in] display display
 * \param[out] level current level in the range 0..levels.size()-1
 * \return error code (see ::UIError)
 */
DllSpec int uiGetBacklightLevel(int display, int &level);


/** set the value of an input element on screen. This covers \<input> and \<select> elements
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] name name of the input element
 * \param[in] value new value
 * \return error code
 */
DllSpec int uiSetInput(int display, int region_id, const std::string &name, const std::string &value);

/** shortcut for uiSetInput(0,region_id,name,value); */
inline int uiSetInput(int region_id, const std::string &name, const std::string &value)
{
   return uiSetInput(0,region_id,name,value);
}

/** Run the action of an input element on screen. This covers \<input>, \<select> and \<button>
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] name name of the input element
 * \return error code
 */
DllSpec int uiRunAction(int display, int region_id, const std::string &name);

/** shortcut for uiRunAction(0,region_id,name); */
inline int uiRunAction(int region_id, const std::string &name)
{
   return uiRunAction(0,region_id,name);
}


/** Run the action2 of an input element on screen. This covers \<input>, \<select> and \<button>
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] name name of the input element
 * \return error code
 */
DllSpec int uiRunAction2(int display, int region_id, const std::string &name);

/** shortcut for uiRunAction2(0,region_id,name); */
inline int uiRunAction2(int region_id, const std::string &name)
{
   return uiRunAction2(0,region_id,name);
}

/** Add an option to a \<select> element. The option is added at the end of the list.
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] name name of the \<select> element
 * \param[in] value value of the element to be added
 * \param[in] text text to be displayed
 * \param[in] style CSS style, if empty use default style
 * \param[in] action optional action
 * \param[in] position position at which to insert the entry. Negative values refer to the
 *            position from the end, e.g. -1 adds the element to the end.
 * \return error code
 * \note Only a subset of the style settings is supported (setting style and color of text).
 */
DllSpec int uiAddOption(int display, int region_id, const std::string &name, const std::string &value, const std::string &text, const std::string &style="", const std::string &action="", int position=-1);

/** shortcut for uiAddOption(0,region_id,name,value,text,style,action,position); */
inline int uiAddOption(int region_id, const std::string &name, const std::string &value, const std::string &text, const std::string &style="", const std::string &action="", int position=-1)
{
   return uiAddOption(0,region_id,name,value,text,style,action,position);
}

/** scroll position for uiScrollOption */
enum UIScrollPosition { UI_SCROLL_TOP,   /**< scroll element to top of menu */
                        UI_SCROLL_BOTTOM /**< scroll element to bottom of menu */
};


/** scroll an entry in a \<select> element to the top or bottom position
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] name name of the \<select> element
 * \param[in] value value of the element to be scrolled
 * \param[in] pos position
 * \note Scrolling is only supported if the content is larger than the containing element.
 * Scrolling is limited to the amount the user could scroll using the touch screen.
 */
DllSpec int uiScrollOption(int display, int region_id, const std::string &name, const std::string &value, UIScrollPosition pos);

/** shortcut for uiScrollOption(0,region_id,name,value,pos) */
inline int uiScrollOption(int region_id, const std::string &name, const std::string &value, UIScrollPosition pos)
{
   return uiScrollOption(0,region_id,name,value,pos);
}

/** Remove an option from a \<select> element
 * \param[in] display display
 * \param[in] region_id region id
 * \param[in] name name of the \<select> element
 * \param[in] value value of the element to be removed
 * \return error code
 */
DllSpec int uiRemoveOption(int display, int region_id, const std::string &name, const std::string &value);

/** shortcut for uiRemoveOption(0,region_id,name,value); */
inline int uiRemoveOption(int region_id, const std::string &name, const std::string &value)
{
   return uiRemoveOption(0,region_id,name,value);
}

/*****************************************************************************/
/*****************************************************************************/
/**************************** Canvas interface *******************************/
/*****************************************************************************/
/*****************************************************************************/

/** Canvas events */
enum UICanvasEvent {
   UI_PUSH,   /**< The user has touched the screen */
   UI_DRAG,   /**< The user is dragging the finger on the screen */
   UI_RELEASE,/**< The user has released the screen */
   UI_KEYDOWN,/**< The user has pressed a button on the keypad */
   UI_KEYUP,  /**< The user has released a button on the keypad */
   UI_TERMINATE /**< The dialog has terminated, the return code can be found in \a key */
};

/** callback function for receiving Canvas events.
 * \param[in] data data pointer provided by the application
 * \param[in] event event type
 * \param[in] x x-coordinate (for ::UI_PUSH, ::UI_DRAG and ::UI_RELEASE)
 * \param[in] y y-coordinate (for ::UI_PUSH, ::UI_DRAG and ::UI_RELEASE)
 * \param[in] key key code (for ::UI_KEYDOWN and ::UI_KEYUP) or return code (UI_TERMINATE)
 * \param[in] flags reserved for future use, currently always 0
 * \note The callback will be run within a different thread context, so make sure to use appropriate
 *  locking if required. All callback functions share the same thread. Therefore, do not use functions
 *  that block for a long time, in particular uiInvokeWait() must not be called.
 */
typedef void (*uiEventCallback)(void *data, UICanvasEvent event, int x, int y, int key, unsigned flags);

/** class for creating a drawing list */
class DllSpec UIDrawing {

 public:
   vfihtml::JSObject drawlist;

   /** font style, values can be combined by or-ing them together */
   enum {
      FNT_NORMAL=0x0, /**< normal font */
      FNT_ITALIC=0x1, /**< italic style */
      FNT_BOLD=0x700  /**< bold style */
   };

   /** image type */
   enum ImageType {
      IMG_BMP,  /**< BMP image */
      IMG_GIF,  /**< GIF image */
      IMG_JPEG, /**< JPEG image */
      IMG_PAM,  /**< PAM image */
      IMG_PBM,  /**< PBM image */
      IMG_PNG,  /**< PNG image */
      IMG_RAW   /**< raw image data, the format is inferred from the size information of the data. */
   };

   /** constructor */
   UIDrawing();

   /** destructor */
   ~UIDrawing();

   /** reset drawing */
   void reset();

   /** clear canvas
    * \param[in] rgb 24-bit color value to be used for clearing the canvas */
   void clear(unsigned rgb);

   /** set current color for successive drawing commands
    * \param[in] rgb 24-bit color value */
   void color(unsigned rgb);

   /** set line width for line and rect
    * \param[in] w line width in pixels, 0 is default */
   void linewidth(int w);

   /** draw line between the given points
    * \param[in] x x-coodinate first point
    * \param[in] y y-coodinate first point
    * \param[in] x2 x-coodinate second point
    * \param[in] y2 y-coodinate second point */
   void line(int x, int y, int x2, int y2);

   /** draw single pixel pixel
    * \param[in] x x-coodinate
    * \param[in] y y-coodinate  */
   void pixel(int x, int y);

   /** draw rectangle
    * \param[in] x x-coodinate
    * \param[in] y x-coodinate
    * \param[in] w width
    * \param[in] h height  */
   void rect(int x, int y, int w, int h);

   /** draw filled rectangle
    * \param[in] x x-coodinate
    * \param[in] y x-coodinate
    * \param[in] w width
    * \param[in] h height  */
   void rectf(int x, int y, int w, int h);

   /** draw filled triangle given by three points
    * \param[in] x x-coodinate first point
    * \param[in] y y-coodinate first point
    * \param[in] x2 x-coodinate second point
    * \param[in] y2 y-coodinate second point
    * \param[in] x3 x-coodinate third point
    * \param[in] y3 y-coodinate third point */
   void trif(int x, int y, int x2, int y2, int x3, int y3);

   /** set the current font
    * \param[in] name font name
    * \param[in] size nominal font height
    * \param[in] style font style */
   void font(const char *name, int size, unsigned style=FNT_NORMAL);

   /** set the current font
    * \param[in] name font name
    * \param[in] size nominal font height
    * \param[in] style font style */
   void font(const std::string &name, int size, unsigned style=FNT_NORMAL);

   /** draw text using current font and color
    * \param[in] text text
    * \param[in] x x-coodinate first point
    * \param[in] y y-coodinate first point
    * \note Coordinates refer to the starting point of the base line, e.g.
    * drawing an 'A', x/y refers to the leftmost bottom pixel of 'A'.
    */
   void text(const char *text, int x, int y);

   /** \copydoc UIDrawing::text(const char *text, int x, int y) */
   void text(const std::string &text, int x, int y);


   /** draw image data to screen
    * \param[in] type image type
    * \param[in] data image data (format depends on type)
    * \param[in] size number of bytes in data
    * \param[in] x x-coordinate
    * \param[in] y y-coordinate
    * \param[in] w width of the image, only used for IMG_RAW
    * \param[in] h height of the image, only used for IMG_RAW
    * \note In case of IMG_RAW the number of bytes per pixel is determined from size and w*h.
    * Depending on the number of bytes per pixel the image format is considered to be as follows:
    * - 1: Grayscale image
    * - 2: Grayscale image with alpha
    * - 3: RGB image
    * - 4: RGB image with alpha
    */
   void image(ImageType type, const void *data, unsigned size, int x, int y, int w=0, int h=0);

   /** draw image data to screen
    * \param[in] filename image file name
    * \param[in] x x-coordinate
    * \param[in] y y-coordinate
    */
   void image(const char *filename, int x, int y);

   /** \copydoc UIDrawing::image(const char *filename, int x, int y) */
   void image(const std::string &filename, int x, int y);

};


/** install a canvas for drawing in the given region
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] cb event callback function, it is invoked for touch and keyboard events.
 * \param[in] data data pointer that is passed on to the callback function
 * \return error code (see ::UIError) or transaction ID.
 */
DllSpec int uiCanvas(int display, int region_id, uiEventCallback cb, void *data=0);

/** shortcut for uiCanvas(0,region_id,cb,data); */
inline int uiCanvas(int region_id, uiEventCallback cb, void *data=0)
{
   return uiCanvas(0,region_id,cb,data);
}

/** draw to canvas
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] drawing set of drawing primitives
 * \return error code (see ::UIError)
 */
DllSpec int uiDraw(int display, int region_id, const UIDrawing &drawing);

/** shortcut for iDraw(0,region_id,drawing); */
inline int uiDraw(int region_id, const UIDrawing &drawing)
{
   return uiDraw(0,region_id,drawing);
}

/** structure for determining the text width */
struct UITextWidth {
   std::string text; /**< text */
   int width;        /**< text width in pixels */

   /** constructor \param[in] s text */
   UITextWidth(const std::string &s="") { text=s; width=0; }
};


/** determine size of text in pixels
 * \param[in] display display
 * \param[in] region_id region ID
 * \param[in] font font name
 * \param[in] size nominal font height
 * \param[in] style font style
 * \param[in,out] width array with the texts for which the width should be determined, the widths are filled in on return.
 * \param[out] height font height, may be different from nominal font height
 * \param[out] descent number of pixels below line
 * \return error code (see ::UIError)
 */
DllSpec int uiTextMetrics(int display, int region_id, const std::string &font, int size, unsigned style, std::vector<UITextWidth> &width, int &height, int &descent);

/** shortcut for uiTextMetrics(0,region_id,font,size,style,width,height,descent); */
inline int uiTextMetrics(int region_id, const std::string &font, int size, unsigned style, std::vector<UITextWidth> &width, int &height, int &descent)
{
   return uiTextMetrics(0,region_id,font,size,style,width,height,descent);
}


/** check if a string contains a data URL and return the type as string (e.g. "png" for a PNG image)
 * \param[in] s string containing data
 * \return file type as string or NULL if \a s does not contain a known data URL type.
 */
DllSpec const char *uiDataURLType(const std::string &in);

/** extract data from data URL to string
 * \param[out] out extracted data
 * \param[in] in data URL
 * \return true in case of success, else false */
DllSpec bool uiDataURLToString(std::string &out,const std::string &in);

/** extract data from data URL to file
 * \param[in] filename file name
 * \param[in] in data URL
 * \return true in case of success, else false */
DllSpec bool uiDataURLToFile(const char *filename,const std::string &in);


#ifndef DOXYGEN
} // namespace vfigui
#endif


/** \}*/

#undef DllSpec


#include "gui_template.h" // add "old" template functions

#endif
