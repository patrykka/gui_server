#ifndef INPUT_PLUGIN_H_20160330
#define INPUT_PLUGIN_H_20160330

/** \file input_plugin.h
 * Interface for input plugin modules
 */

#if defined _WIN32 && defined VFI_INPUTPLUGIN_SHARED_EXPORT
#  define DllSpec __declspec(dllexport)
#elif defined __GNUC__ && defined VFI_INPUTPLUGIN_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** start the input
 * \param[in] parameter string containing the parameters for the input plugin
 * \param[out] log_msg pointer to log message. If not NULL ADKGUI will log
 *             the message at error level and release the returned string using free().
 * \return handle handle that will be passed to input_poll and input_end or
 *              NULL in case of error
 */
DllSpec void *input_start(const char *parameter, char **log_msg);

/** poll for input. If input is ready it is returned as string.
 * \param[in] handle handle as returned by input_start()
 * \return Input string or NULL if there is no input. The returned input string will be released using free().
 * \note The format of the returned string is plugin-specific.
 */
DllSpec char *input_poll(void *handle);

/** stop input and release all internal resources
 * \param[in] handle handle as returned by input_start(). After input_end()
 * returns the handle is no longer valid.
 */
DllSpec void input_end(void *handle);


#ifdef __cplusplus
}
#endif

#undef DllSpec

#endif
