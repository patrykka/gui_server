// -*- Mode: C++; -*-
#ifndef JSPROC_H_20150609
#define JSPROC_H_20150609

/** \file jsproc.h script processor */

#include <string>
#include <vector>
#include <map>

#if defined _WIN32 && defined VFI_JSPROC_SHARED_EXPORT
#  define DllSpec __declspec(dllexport)
#elif defined __GNUC__ && defined VFI_JSPROC_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

namespace vfihtml { class ExtContext; }

#ifndef DOXYGEN
namespace js {
#endif

/** Process JavaScript script and return the data sent to stdout and stderr
 * \param[in] data this parameter is unused, it is just there for compatibility with the ADKGUI scripting interface
 * \param[in] sourcecode string containing the Js source code
 * \param[in] arg key value map, can be accessed as table 'ARG' from within the JavaScript script
 * \param[out] out data sent to stdout
 * \param[out] err data sent to stderr
 * \param[out] exitcode parameter that was passed to exit()
 * \return true if processing was successful, false if not
 */
DllSpec bool jsProcessor(void *data,
                         const std::string &sourcecode,
                         std::map<std::string,std::string> &arg,
                         std::string &out,
                         std::string &err,
                         std::string &exitcode);

/** Process JavaScript script and return the data sent to stdout and stderr. This is the variant with ADKGUI extensions support.
 * \param[in] data this parameter is unused, it is just there for compatibility with the ADKGUI scripting interface
 * \param[in] sourcecode string containing the Js source code
 * \param[in] arg key value map, can be accessed as table 'ARG' from within the JavaScript script
 * \param[out] out data sent to stdout
 * \param[out] err data sent to stderr
 * \param[out] exitcode parameter that was passed to exit()
 * \param[in,out] context extended context, if NULL GUI extensions are disabled.
 * \return true if processing was successful, false if not
 */
DllSpec bool jsProcessorExt(void *data,
                            const std::string &sourcecode,
                            std::map<std::string,std::string> &arg,
                            std::string &out,
                            std::string &err,
                            std::string &exitcode,
                            vfihtml::ExtContext *context);

/** \return version information */
DllSpec const char *jsProcVersion();


/** Set Http-Proxy
 * \param[in] proxy Proxy setting, e.g. http://localhost:8888 or NULL
 */
DllSpec void jsSetHttpProxy(const char *proxy);

/** Read current proxy setting
 * \return proxy setting.
 */
DllSpec const char *jsGetHttpProxy();

/** set callback for console output
 * \param[in] cb callback function pointer or NULL
 * \param[in] data data pointer provided to callback as first parameter
 *
 * The callback takes the following parameters:
 * \param[in] data data pointer
 * \param[in] text string to be sent to console
 *
 * \note The console callback is global, only one callback may be set. Setting the
 * callback function is not thread safe, i.e. appropriate actions have to be taken
 * to protect against race conditions.
 */
DllSpec void jsSetConsole( void (*cb)(void *data, const char *text), void *data);

/** get current setting of callback for console output
 * \param[out] cb callback function pointer or NULL
 * \param[out] data data pointer provided to callback as first parameter
 */
DllSpec void jsGetConsole( void (*&cb)(void *, const char *), void *&data);

/** set callback for sending notifications
 * \param[in] cb callback function pointer or NULL
 * \param[in] data data pointer provided to callback as first parameter
 *
 * The callback takes the following parameters:
 * \param[in] data data pointer
 * \param[in] to destination address
 * \param[in] notification_id notification id
 * \param[in] json_param string containing the JSON encoded parameters
 * \param[in] flags optional flags
 * \param[in] from optional source address, if NULL use default source address
 * \return 0 in case of success or negative error code
 *
 * \note The notification callback is global, only one callback may be set. Setting the
 * callback function is not thread safe, i.e. appropriate actions have to be taken
 * to protect against race conditions.
 */
DllSpec void jsSetNotify( int (*cb)(void *data,
                                    const char *to,
                                    const char *notification_id,
                                    const char *json_param,
                                    unsigned flags,
                                    const char *from), void *data);

/** get current setting of notification callback
 * \param[out] cb callback function pointer or NULL
 * \param[out] data data pointer provided to callback as first parameter
 */
DllSpec void jsGetNotify( int (*&cb)(void *data,
                                      const char *to,
                                      const char *notification_id,
                                      const char *json_param,
                                      unsigned flags,
                                      const char *from), void *&data);

/** set callback for sending notifications
 * \param[in] cb callback function pointer or NULL
 * \param[in] data data pointer provided to callback as first parameter
 *
 * The callback takes the following parameters:
 * \param[in] data data pointer
 * \param[in] to destination address
 * \param[in] notification_id notification id
 * \param[in] json_param string containing the JSON encoded parameters
 * \param[in] flags optional flags
 * \param[in] from optional source address, if NULL use default source address
 * \param[in] wait_id wait for notification with this id
 * \param[out] result received notification
 * \param[in] timeout_msec timeout in milliseconds
 * \return 0 in case of success or negative error code
 *
 * \note The notification callback is global, only one callback may be set. Setting the
 * callback function is not thread safe, i.e. appropriate actions have to be taken
 * to protect against race conditions.
 */
DllSpec void jsSetNotifyAndWait( int (*cb)(void *data,
                                           const char *to,
                                           const char *notification_id,
                                           const char *json_param,
                                           unsigned flags,
                                           const char *from,
                                           const char *wait_id,
                                           std::string &result,
                                           int timeout_msec), void *data);

/** get current setting of notification callback
 * \param[out] cb callback function pointer or NULL
 * \param[out] data data pointer provided to callback as first parameter
 */
DllSpec void jsGetNotifyAndWait( int (*&cb)(void *data,
                                            const char *to,
                                            const char *notification_id,
                                            const char *json_param,
                                            unsigned flags,
                                            const char *from,
                                            const char *wait_id,
                                            std::string &result,
                                            int timeout_msec), void *&data);

/** trace type */
enum JSTraceType {
   JST_HTTPGET,   /**< trace of an HTTP GET request, data provided: "url" */
   JST_HTTPPOST,  /**< trace of an HTTP POST request, data provided: "url" */
   JST_HTTPRESULT /**< trace of the HTTP result, data provided: "status"  */
};

/** install HTTP trace callback
 * \param[in] cb callback function pointer or NULL
 * \param[in] data data pointer provided to callback as first parameter
 *
 * The callback takes the following parameters:
 * \param[in] data data pointer
 * \param[in] type trace type
 * \param[in] app_id application ID (as found in ARGV["cp_appId"])
 * \param[in] trace key value map containing trace parameters
 *
 * \note The trace callback is global, only one callback may be set. Setting the
 * callback function is not thread safe, i.e. appropriate actions have to be taken
 * to protect against race conditions.
 */
DllSpec void jsSetTrace( void (*cb)(void *data,
                                    JSTraceType type,
                                    const std::string &app_id,
                                    std::map<std::string,std::string> &trace), void *data);

/** read HTTP trace callback
 * \param[out] cb callback function pointer or NULL
 * \param[out] data data pointer provided to callback as first parameter
 */
DllSpec void jsGetTrace( void (*&cb)(void *data,
                                     JSTraceType type,
                                     const std::string &app_id,
                                     std::map<std::string,std::string> &trace), void *&data);


enum JSLogLevel {
   JSL_EMERGENCY=0,
   JSL_ALERT=1,
   JSL_CRITICAL=2,
   JSL_ERROR=3,
   JSL_WARNING=4,
   JSL_NOTICE=5,
   JSL_INFO=6,
   JSL_DEBUG=7
};

/** install logging callback
 * \param[in] cb callback function pointer or NULL
 * \param[in] data data pointer provided to callback as first parameter
 *
 * The callback takes the following parameters:
 * \param[in] data data pointer
 * \param[in] log_level logging level 0-7 (emergency,alert,critical,
 * \param[in] app_id application ID (as found in ARGV["cp_appId"])
 * \param[in] msg log message parameters in the order they were provided to log.info()/log.debug(),log.error()
 *
 * \note The log callback is global, only one callback may be set. Setting the
 * callback function is not thread safe, i.e. appropriate actions have to be taken
 * to protect against race conditions.
 */
DllSpec void jsSetLog( void (*cb)(void *data,
                                  const std::string &app_id,
                                  JSLogLevel log_level,
                                  const std::vector<std::string> &msg), void *data);

/** read logging callback
 * \param[out] cb callback function pointer or NULL
 * \param[out] data data pointer provided to callback as first parameter
 */
DllSpec void jsGetLog( void (*&cb)(void *data,
                                  const std::string &app_id,
                                  JSLogLevel log_level,
                                  const std::vector<std::string> &msg), void *&data);

#ifndef DOXYGEN
} // namespace
#endif

#undef DllSpec

#endif
