#ifndef IPC_TLS_H_20200130
#define IPC_TLS_H_20200130

/** \file tls.h */

#include <ipc/ipc.h>


#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_IPCTLS_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_IPCTLS_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_IPCTLS_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

namespace vfiipc {


/** Class for inter process communication via SSL/TLS */
class DllSpec TLS: public IPC {

 private:
   // prevent usage of copy constructor and assignment operator
   TLS(const TLS &o);
   void operator=(const TLS &o);

 protected:
   friend class TLSPrivate;
   void setLink(void *l);
   void getAddr(char *&remote, char *&local);

 public:

   /** TLS error codes */
   enum Error {
      OK,               /**< no error */
      ERR_HOST,         /**< invalid host name */
      ERR_PORT,         /**< invalid port */
      ERR_SOCKET,       /**< failed to allocate socket */
      ERR_UNKNOWN_HOST, /**< unknown host */
      ERR_CONNECT,      /**< failed to connect */
      ERR_TIMEOUT,      /**< timeout */
      ERR_AUTHENTICATION, /**< failed authentication */
      ERR_FAIL          /**< other error */
   };

   /** Certificate checking configuration */
   enum CheckConfig {
      IGNORE_NONE=0,         /**< default: all checks are performed */
      IGNORE_ALL=1,          /**< disable all server certificate checks */
      IGNORE_SELF_SIGNED=2,  /**< allow self-signed server certificates */
      IGNORE_EXPIRED=4,      /**< allow expired certificates */
      IGNORE_NOT_YET_VALID=8,/**< allow not yet valid certificates */
      IGNORE_HOSTNAME=16     /**< disable hostname check on the server certificate */
   };

   /** constructor */
   TLS();
   /** destructor */
   ~TLS();

   virtual bool eof();
   virtual bool error();

   /** obtain last TLS error
    * \param[out] err if not NULL, fill in the error description
    * \return error code;
    */
   Error TLSError(std::string *err);

   /** set key and certificate to be used for the connection.
    * \param[in] key file name containing the SSL key or NULL
    * \param[in] cert file containing the SSL certificate or NULL
   */
   void setKeyCert(const char *key, const char *cert);

   /** add CA used for verifying certificates
    * \param[in] filename file name containing CA certificates or NULL
    * \param[in] path directory containing CA certificates or NULL
    * \note The CA file may contain more than once CA certificate.
    */
   void addCA(const char *filename, const char *path);

   /** same as addCA, just provided for backwards compatibility */
   void setCA(const char *filename, const char *path);

   /** add system default CA paths for verifying certificates */
   void addDefaultCA();

   /** enable/disable client authentication
    * \param[in] on true to enable, false to disable client authentication
    */
   void setClientAuth(bool on);

   /** set certificate check configuration
    * \param[in] check_flags bitwise or-ed flags from CheckConfig
    */
   void setCheckConfig(unsigned check_flags);

   /** test for valid AuthEx key/certificate
    * \return true if a valid AuthEx key/certificate is available, false if not
    */
   static bool hasAuthEx();

   /** initiate a connection to a remote IPC server via a TLS socket
    * \param[in] hostname name or IP address of the destination host with an optional port number (<name:port> or <[IPV6]:port>)
    * \param[in] port port of the destination host. If 0, get the port from hostname.
    * \param[in] timeout_msec connect timeout in milliseconds. A negative timeout means using default OS timeout.
    * \return true in case that connection was established successfully, else false
    * \note Providing two port numbers, one in \a hostname and one in \a port is not supported.
    * \par
    * \note There is a difference in the flow of operation between TLS 1.2 and TLS 1.3 when using client authentication.
    * With TLS up to 1.2 connect() fails if a client certificate is missing or is invalid. This is no longer the case with TLS 1.3.
    * With TLS 1.3 connect() will return success but successive calls to read/write will fail with an error. This is due to
    * changes in the control flow of the TLS handshake and the failed client certificate check cannot be detected in connect().
    */
   bool connect(const char *hostname, unsigned short port,int timeout_msec=-1);

   /** creates a TLS listen socket for server mode to accept incomming connections with TLS::accept().
    * \param[in] port port of the TLS listen socket. If port is 0, then the system chooses one, the application can
    *                      determine it using local_addr().
    * \param[in] listen_address IP address of adapter that should be used to accept the connection
    *                           Default listen_address=0 means acceptance of connections on all available adapters.
    *                           Use "127.0.0.1" for local loopback device to avoid that external connections are accepted.
    * \return true in case that the TLS listen socket was created successfully, else false
    */
   bool listen(unsigned short port, const char *listen_address=0);

   IPC * accept(int timeout_msec=-1);

   /** closes the session and all TLS socket sockets for this object */
   void close();

   virtual bool write(const void *data, int size);
   virtual int read(void *data, int maxsize, int timeout_msec, int timeout_msec2);

   virtual bool write_msg(unsigned prefix, int msg_id, const void *msg, int size);
   virtual bool write_msg(unsigned prefix, int msg_id, const std::vector<unsigned char> &msg)
      { return write_msg(prefix,msg_id,msg.size()?&msg[0]:0,msg.size()); }
   virtual bool write_msg(unsigned prefix, int msg_id, const std::vector<char> &msg)
      { return write_msg(prefix,msg_id,msg.size()?&msg[0]:0,msg.size()); }
   virtual bool write_msg(unsigned prefix, int msg_id, const std::string &msg)
      { return write_msg(prefix,msg_id,msg.data(),msg.size()); }

   virtual bool read_msg(unsigned prefix, int &msg_id, std::vector<unsigned char> &msg, int size_limit, int timeout_msec=-1);
   virtual bool read_msg(unsigned prefix, int &msg_id, std::vector<char> &msg, int size_limit, int timeout_msec=-1);

   virtual bool poll_in(int timeout_msec=-1);
   virtual const char *remote_addr() const;

   /** Get file descriptor, only supported when listening for incoming connections
    * \return listening file descriptor */
   int getFD() const;
   virtual bool set_callback(ipcCallback cb, void *data=0);
protected:
   /** base function for reading messages */
   virtual bool read_msg(unsigned prefix, int &msg_id, IPC::IpcBuffer &msg, int size_limit, int timeout_msec=-1);

public:
     virtual bool read_msg(unsigned prefix, int &msg_id, std::string &msg, int size_limit, int timeout_msec=-1);
   virtual bool is_server();
   virtual const char *local_addr() const;

};



} // namespace

#undef DllSpec

#endif
