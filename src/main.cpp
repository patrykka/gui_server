
#include <vector>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <string>
#include <list>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "html/gui.h"
#include "ipc/notify.h"
#include "ipc/tls.h"
#include <log/liblog.h>
#include <sysinfo/vfisyspm.h>


#include "ipc/jsobject.h"
#include "ipc/ipc.h"
#include "html/prt.h"
#include "html/jsobject.h"


#ifdef _WIN32
#  include <winsock2.h>
#  include <windows.h>
#  define sleep(x) Sleep(1000*(x))
#  define usleep(x) Sleep((x)/1000)
#endif

#include <sys/types.h>
#include <sys/timeb.h>
#if defined (linux) || defined (__CYGWIN__)
#   include <unistd.h>
#   include <sys/wait.h>
#endif

using namespace std;
using namespace vfigui;
using namespace vfiprt;
using namespace vfiipc;

#define PREFIX 0x16475549
#define TCP_PORT 5900
#define SINGLE_SHOT_CLIENT 1
#define SINGLE_CONNECT_CLIENT 0
#define USE_CALLBACK_CLIENT 0
#define SIZELIMIT (1024*1024)
#define RESP_OK1 "{\"result\":\"OK\"}"
#define RESP_OK2 "{\"result\":0}"

/* Set to 1 to build a single threaded server.
 * Set to 0 to create a seperate thread for each new connection. */
#define SINGLE_THEADED_SERVER 1

/* Set to 1 to build a server reading one request and sending one response before the connection is closed.
 * Set to 0 to let the connection open and do a request/responce loop. */
#define SINGLE_SHOT_SERVER    0

static void notification_cb(vfisyspm::vfisyspmNotificationType type, vfisyspm::vfisyspmNotifications notification);
static bool pmInited = false;


static void timer_cb(const vfiipc::JSObject &json)
{
   LOGF_ERROR("callback timer sleep");
   int i = 0;
   //try 5 times to enter sleep
   vfisyspm::vfisyspmError returnCode = vfisyspm::SYS_PM_ERR_FAIL ;
   do
   {
      returnCode = vfisyspm::sys_Sleep();
      LOGF_ERROR("Enter Sleep result:%d", returnCode);
      if (i==10) break;
      i++;
      sleep(1);
   }
   while(vfisyspm::SYS_PM_ERR_OK!=returnCode);

   if(vfisyspm::SYS_PM_ERR_OK == returnCode)
   {
      LOGF_TRACE("Sleeeeping....");
   }
   else
   {
      LOGF_ERROR("fail to sleep....");
   }
}

static int response(IPC *r, int timeout_msec)
{
   vector<char> buf;
   vector<char> buf_response;
   int id;
   bool res;
   res=r->read_msg(PREFIX,id,buf,SIZELIMIT,timeout_msec);
   if(!res) {
      if(r->error() || r->eof()) {
         return -1;
      }
      return 0;
   }
 
   buf.push_back(0);
   LOGF_TRACE("msg_id:%d read_msg(): %s",id,&buf[0]);

   sleep(1);

   if (id == 0) 
   {
      if(!r->write_msg(PREFIX,id,"{\"audio\":0,\"display\":{\"depth\":1,\"height\":64,\"touch\":0,\"width\":128},\"model\":\"UX300\",\"navigator\":0,\"num_keys\":16,\"result\":0,\"version\":\"2.45.3-1\",\"video\":0}"))
      {
         return -1;
      }   
   }
   else 
   {
      if(!r->write_msg(PREFIX,id,"0")) 
      {
         return -1;
      }
      if (!pmInited)
      {
         vfisyspm::vfisyspmError returnCode = vfisyspm::sys_Init();
         LOGF_TRACE("vfisyspm::sys_Init:%d", returnCode);
         if(vfisyspm::SYS_PM_ERR_OK == returnCode)
         {
            vfisyspm::sys_SetNotificationCB( notification_cb, vfisyspm::PM_NOTIFY_TYPE_ALL );
            pmInited = true;
         }
      }
   }
   return 1;
}

void *server(void *p)
{
   IPC *r=(IPC *)p;

   do
   {
     int ret=response(r,2000);
     if(ret<0) break; // leave loop on error
   }
   while(!SINGLE_SHOT_SERVER);

   delete r; // closes the session and does not require an explicit call of r->close()
   return 0;
}

static void notification_cb(vfisyspm::vfisyspmNotificationType type, vfisyspm::vfisyspmNotifications notification)
{
   if (type == vfisyspm::PM_NOTIFY_TYPE_CRITICAL_SECTION)
   {
      vfisyspm::sys_CancelTimers();
      LOGF_ERROR("CRITICAL SECTION");
      if (notification == vfisyspm:: CRITICAL_SECTION_DISABLED)
      {        
         LOGF_ERROR("CRITICAL SECTION OFF");
         vfisyspm::timerHandle handle;
         time_t requested_time;
         time(&requested_time);
         requested_time += 60;
         const std::string message("timer for 30 seconds");
         vfisyspm::vfisyspmError returnCode = vfisyspm::sys_SetTimer( handle, requested_time, message, timer_cb );
         LOGF_TRACE("Timer is set 90 sec:%d", returnCode);
      }
      else
      {
         LOGF_ERROR("CRITICAL SECTION ON");
      }
   }
}


int main(int argc, char *argv[])
{
   LOGAPI_INIT("GUI");
   LOGAPI_SETLEVEL(LOGAPI_TRACE);
   LOGAPI_SETOUTPUT(LOGAPI_ALL);

   LOGF_TRACE("application starts now");
   LOGF_ERROR("APPLICATION START ver 0.0.8");

   TCP s;
   LOGF_TRACE("listening.....");

   if (s.listen(TCP_PORT, "127.0.0.1"))
   {
      LOGF_TRACE("listening started.....");
   }
   else
   {
      LOGF_TRACE("not able to start listening");
      return 0;
   }
   
   while(1)
   {
      LOGF_TRACE("....loop.... before accept");
      IPC *r=s.accept();
      LOGF_TRACE("after accept");
      if( !r ) 
      {
         sleep(1);
         LOGF_TRACE("looping");
         continue;
      }
      LOGF_TRACE("local: %s",r->local_addr());
      LOGF_TRACE("remote: %s",r->remote_addr());

      if(SINGLE_THEADED_SERVER) {
         LOGF_TRACE("Single-threaded server");
         server(r);
      }
      else {
         LOGF_TRACE("Multi-threaded server");
         pthread_t server_thread;
         pthread_attr_t attr;
         pthread_attr_init(&attr);
         pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
         pthread_attr_setstacksize(&attr,32*1024); // use 32 KB as default stack size for session threads
         if(!pthread_create(&server_thread,&attr,server,(void *)r)) {
            LOGF_TRACE("New session thread with ipc=%lx successfully started",(long)r);
         } else {
            LOGF_TRACE("!!!!!! Could NOT start session thread with ipc=%lx !!!!!!",(long)r);
         }
         pthread_attr_destroy(&attr);
      }     
   }
   return 0;
}